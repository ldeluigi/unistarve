<?php
require("base.php");
$result = [];
session_start();
$conn = connectDB();
if ($conn->connect_error) {
    $result[OK] = 401;
    $result[ERROR] = "Connessione al DB fallita";
} else {
    if (isset($_GET["new"]) and isset($_SESSION[ACCOUNT])) {
        if (isset($_GET["name"]) and is_string($_GET["name"]) and strlen($_GET["name"]) > 0 and strlen($_GET["name"]) < 200 and isset($_GET["sd"]) and
         is_string($_GET["sd"]) and strlen($_GET["sd"]) > 0 and strlen($_GET["sd"]) < 300 and isset($_GET["cost"]) and is_numeric($_GET["cost"])) {
            $stmt = $conn->prepare("INSERT INTO ORDINABLES (name, short_description, available, cost, isMenu, productor, description, img_link)
                                    SELECT ?, ?, ?, ?, 0, P.ID, ?, ?
                                    FROM PRODUCTORS AS P JOIN ACCOUNTS AS A ON A.productor=P.ID
                                    WHERE A.ID=?");
            if ($stmt === false) {
                $result[OK] = 409;
                $result[ERROR] = "Preparazione query di inserimento non riuscita";
            } else {
                $d = (isset($_GET["d"]) and is_string($_GET["d"]) and strlen($_GET["d"]) > 0 and strlen($_GET["d"]) < 2048) ? $_GET["d"] : NULL;
                $l = (isset($_GET["img"]) and is_string($_GET["img"]) and strlen($_GET["img"]) > 0 and strlen($_GET["img"]) < 2048) ? $_GET["img"] : NULL;
                $available = (isset($_GET["av"]) and ($_GET["av"] == "1" or $_GET["av"] == "0")) ? $_GET["av"] : 0;
                if ($stmt->bind_param("ssidssi", $_GET["name"], $_GET["sd"], $available, $_GET["cost"], $d, $l, $_SESSION[ACCOUNT])) {
                    if ($stmt->execute() and $stmt->affected_rows > 0) {
                        $result[OK] = true;
                        $result[RESULT] = "Oggetto aggiunto con successo";
                        $result[ID] = $stmt->insert_id;
                    } else {
                        $result[OK] = 411;
                        $result[ERROR] = "Esecuzione query di inserimento non riuscita. ".$stmt->error;
                    }
                } else {
                    $result[OK] = 410;
                    $result[ERROR] = "Preparazione query di inserimento non riuscita";
                }
            }
        } else {
            $result[OK] = 408;
            $result[ERROR] = "Minimo set di valori in input non rispettato";
        }
    } else if (isset($_GET["update"]) and isset($_SESSION[ACCOUNT]) and isset($_GET["prodid"]) and is_numeric($_GET["prodid"])) {
        $name = (isset($_GET["name"]) and is_string($_GET["name"]) and strlen($_GET["name"]) > 0 and strlen($_GET["name"]) < $PNMAX) ? $_GET["name"] : NULL;
        $short_description = (isset($_GET["sd"]) and is_string($_GET["sd"]) and strlen($_GET["sd"]) > 0 and strlen($_GET["sd"]) < $SDMAX) ? $_GET["sd"] : NULL;
        $cost = (isset($_GET["cost"]) and is_numeric($_GET["cost"])) ? $_GET["cost"] : NULL;
        $description = (isset($_GET["d"]) and is_string($_GET["d"])) ? $_GET["d"] : NULL;
        $img_link = (isset($_GET["img"]) and is_string($_GET["img"])) ? $_GET["img"] : NULL;
        $available = (isset($_GET["av"]) and ($_GET["av"] == "1" or $_GET["av"] == "0")) ? $_GET["av"] : NULL;
        if ($name == NULL or $short_description == NULL or $cost == NULL) {
            $result[OK] = 415;
            $result[ERROR] = "I parametri non permetto una modifica ragionevole";
        } else {
            $stmt = $conn->prepare("UPDATE ORDINABLES SET name=?, short_description=?, cost=?, description=?, img_link=?, available=? WHERE ID IN ( SELECT O.ID FROM (SELECT * FROM ORDINABLES) AS O JOIN PRODUCTORS AS P ON O.productor=P.ID JOIN ACCOUNTS AS A ON A.productor=P.ID WHERE O.ID=? AND A.ID=?)");
            if ($stmt !== false) {
                if ($stmt->bind_param("ssdssiii", $name, $short_description, $cost, $description, $img_link, $available, $_GET["prodid"], $_SESSION[ACCOUNT])) {
                    if ($stmt->execute() and $stmt->affected_rows > 0) {
                        $result[OK] = true;
                        $result[RESULT] = "Oggetto modificato con successo";
                    } else {
                        $result[OK] = 412;
                        $result[ERROR] = "Errore nella query di modifica (".$stmt->affected_rows." righe modificate). ".$conn->error;
                    }
                } else {
                    $result[OK] = 413;
                    $result[ERROR] = "Errore nella preparazione della query per la modifica: ".$conn->error;
                }
            } else {
                $result[OK] = 414;
                $result[ERROR] = "Errore nella preparazione della query per la modifica: ".$conn->error;
            }
        }
    } else if (isset($_GET["delete"]) and isset($_SESSION[ACCOUNT]) and isset($_GET["prodid"]) and is_numeric($_GET["prodid"])) {
        $stmt = $conn->prepare("UPDATE ORDINABLES SET elimination_datetime=NOW() WHERE ID IN ( SELECT O.ID FROM (SELECT * FROM ORDINABLES) AS O JOIN PRODUCTORS AS P ON O.productor=P.ID JOIN ACCOUNTS AS A ON A.productor=P.ID WHERE O.ID=? AND A.ID=?)");
        if ($stmt !== false) {
            if ($stmt->bind_param("ii", $_GET["prodid"], $_SESSION[ACCOUNT])) {
                if ($stmt->execute() and $stmt->affected_rows > 0) {
                    $result[OK] = true;
                    $result[RESULT] = "Oggetto cancellato con successo";
                } else {
                    $result[OK] = 416;
                    $result[ERROR] = "Errore nella query di cancellazione (".$stmt->affected_rows." righe cancellate). ".$conn->error;
                }
            } else {
                $result[OK] = 417;
                $result[ERROR] = "Errore nella preparazione della query per la cancellazione: ".$conn->error;
            }
        } else {
            $result[OK] = 418;
            $result[ERROR] = "Errore nella preparazione della query per la cancellazione: ".$conn->error;
        }
    } else if (isset($_GET["prodid"]) and is_numeric($_GET["prodid"])) {
        $stmt = $conn->prepare("SELECT O.name AS name, O.description AS descr, O.img_link AS img, O.available AS av, O.cost AS cost, O.isMenu AS isM, P.name AS pName, P.description AS pDescr, P.address AS pAddr, P.ID AS PID, P.P_IVA AS IVA, A.ID AS owner, O.short_description AS sd FROM ORDINABLES AS O 
                            JOIN PRODUCTORS AS P ON O.productor=P.ID
                            JOIN ACCOUNTS AS A ON A.productor=P.ID
                            WHERE O.ID=? AND O.elimination_datetime IS NULL AND P.unsubscription IS NULL");
        if(!$stmt){
            $result[OK] = 402;
            $result[ERROR] = "Errore nella preparazione della query di ricerca";
        } else {
            if ($stmt->bind_param("i",$_GET['prodid'] )) {
                if ($stmt->execute() === false) {
                    $result[OK] = 407;
                    $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                } else {
                    $res = $stmt->get_result();
                    if ($res === false) {
                        $result[OK] = 406;
                        $result[ERROR] = "Errore nella query per i top: ".$conn->error;
                    } else {
                        if ($elem = $res->fetch_assoc()) {
                            $ownerMode = (isset($_SESSION[ACCOUNT]) and $elem["owner"] == $_SESSION[ACCOUNT] and isset($_GET["edit"]));
                            $cartMode = !(isset($_SESSION[ACCOUNT]) and $elem["owner"] == $_SESSION[ACCOUNT]);

                            $page = new DOMDocument();
                            $page->normalizeDocument();
                            $page->formatOutput = true;
                            class br extends DOMElement {
                                function __construct() {
                                    parent::__construct('br');
                                }
                            }

                            $main = $page->createElement('div');
                            $main->setAttribute('class', 'modal fade');
                            $main->setAttribute('id', 'productModal');
                            $main->setAttribute('tabindex', '-1');
                            $main->setAttribute('role', 'dialog');

                            $modalDialog = $page->createElement('div');
                            $modalDialog->setAttribute('class', 'modal-dialog');
                            $modalDialog->setAttribute('role', 'document');
                            
                            $modalContent = $page->createElement('div');
                            $modalContent->setAttribute('class', 'modal-content');

                            $modalHeader = $page->createElement('div');
                            $modalHeader->setAttribute('class', 'modal-header');

                            $closeButton = $page->createElement('button');
                            $closeButton->setAttribute('type', 'button');
                            $closeButton->setAttribute('class', 'close');
                            $closeButton->setAttribute('data-dismiss', 'modal');
                            $closeButton->setAttribute('title', 'Chiudi');

                            $closeIcon = $page->createElement('i');
                            $closeIcon->setAttribute('aria-hidden', 'true');
                            $closeIcon->setAttribute('class', 'fas fa-times');

                            if ($ownerMode) {
                                $modalTitle = $page->createElement('input');
                                $modalTitle->setAttribute('class', 'form-control font-weight-bold');
                                $modalTitle->setAttribute('type', 'text');
                                $modalTitle->setAttribute('id', 'Pname');
                                $modalTitle->setAttribute('name', 'name');
                                $modalTitle->setAttribute('value', $elem["name"]);
                            } else {
                                $modalTitle = $page->createElement('h5');
                                $modalTitle->appendChild($page->createTextNode($elem["name"]));
                            }
                            
                            $modalBody = $page->createElement('div');
                            $modalBody->setAttribute('class', 'modal-body');

                            $modalFooter = $page->createElement('div');
                            $modalFooter->setAttribute('class', 'modal-footer');

                            
                            $closeFooter = $page->createElement('button');
                            $closeFooter->setAttribute('class', 'btn btn-secondary');
                            $closeFooter->setAttribute('type', 'button');
                            $closeFooter->setAttribute('data-dismiss', 'modal');
                            if ($ownerMode) {
                                $closeFooter->appendChild($page->createTextNode('Annulla'));
                            } else {
                                $closeFooter->appendChild($page->createTextNode('Chiudi'));
                            }

                            $modalFooter->appendChild($closeFooter);

                            $banner = $page->createElement('div');
                            $banner->setAttribute('class', 'bannerM banner');
                            $alert = $page->createElement('div');
                            $alert->setAttribute('class', 'alertM alert alert-danger alert-dismissible fade show');
                            $alert->setAttribute('role', 'alert');
                            $label = $page->createElement('label');
                            $label->setAttribute('class', 'msgM msg');
                            $label->appendChild($page->createTextNode('Errore:'));
                            $closeBanner = $page->createElement('button');
                            $closeBanner->setAttribute('type', 'button');
                            $closeBanner->setAttribute('class', 'close');
                            $closeBanner->setAttribute('data-dismiss', 'alert');
                            $closeBanner->setAttribute('title', 'Chiudi');
                            $closeX = $page->createElement('span');
                            $closeX->setAttribute('aria-hidden', 'true');
                            $closeX->appendChild($page->createTextNode('&times;'));
                            $closeBanner->appendChild($closeX);
                            $alert->appendChild($label);
                            $alert->appendChild($closeBanner);
                            $banner->appendChild($alert);
                            $modalBody->appendChild($banner);

                            if ($ownerMode) {
                                $dimg = $page->createElement('div');
                                $dimg->setAttribute('class', 'prodImageC mt-3');
                                $dimg->setAttribute('id', 'imgDiv');
                                $limg = $page->createElement('label');
                                $limg->setAttribute('for', 'Pimg');
                                $limg->appendChild($page->createTextNode('Link all\'immagine:'));
                                $limg->setAttribute('class', 'font-italic');
                                $img = $page->createElement('input');
                                $img->setAttribute('class', 'form-control');
                                $img->setAttribute('type', 'url');
                                $img->setAttribute('id', 'Pimg');
                                $img->setAttribute('name', 'image_link');
                                $img->setAttribute('value', $elem["img"]);
                                $dimg->appendChild($limg);
                                $dimg->appendChild($img);
                                $modalBody->appendChild($dimg);
                            } else if ($elem["img"] and strlen($elem["img"]) > 0) {
                                $dimg = $page->createElement('div');
                                $dimg->setAttribute('class', 'prodImageC');
                                $dimg->setAttribute('id', 'imgDiv');
                                $img = $page->createElement('img');
                                $img->setAttribute('class', 'img-thumbnail rounded lazy mw-100');
                                $img->setAttribute('style', 'max-height: 40vh;');
                                $img->setAttribute('alt', $elem["pName"]);
                                $img->setAttribute('src', $elem["img"]);
                                $img->setAttribute('id', 'modalImg');

                                $reportbutton = $page->createElement('button');
                                $reportbutton->setAttribute('class', 'btn btn-primary position-absolute');
                                $reportbutton->setAttribute('type', "button");
                                $reportbutton->setAttribute('onclick', "report(".$_GET["prodid"].");");
                                $reportbutton->setAttribute('style', "display: block;");
                                $reportbutton->setAttribute('data-toggle', "tooltip");
                                $reportbutton->setAttribute('data-placement', "right");
                                $reportbutton->setAttribute('title', "Segnala contenuto inappropriato");
                                $icon = $page->createElement("i");
                                $icon->setAttribute("class", "fas fa-exclamation-circle");
                                $icon->setAttribute("aria-hidden", "true");
                                $reportbutton->appendChild($icon);

                                $dimg->appendChild($reportbutton);
                                $dimg->appendChild($img);

                                $modalBody->appendChild($dimg);
                            
                            }
                            if ($ownerMode) {
                                $switchDiv = $page->createElement('div');
                                $switchDiv->setAttribute('class', 'float-right mr-5 switch round '.($elem["av"] == 0 ? 'Off' : 'On' ));
                                $switchDiv->setAttribute('id', 'Pavailable');
                                $avToggle = $page->createElement('div');
                                $avToggle->setAttribute('class', 'toggle');
                                $avLabel = $page->createElement('div');
                                $avLabel->setAttribute('class', 'w-100 align-middle my-4');
                                $avLabel->appendChild($page->createTextNode('Disponibilità: '));
                                $switchDiv->appendChild($avToggle);
                                $avLabel->appendChild($switchDiv);
                                $modalBody->appendChild($avLabel);

                                $del = $page->createElement('button');
                                $del->setAttribute('class', 'submit btn btn-outline-light');
                                $del->setAttribute('type', 'submit');
                                $del->setAttribute('onclick', 'updateItem('.$_GET["prodid"].'); return false;');
                                $del->setAttribute('title', 'Aggiorna questo oggetto');
                                $icon = $page->createElement('i');
                                $icon->setAttribute('class', "fas fa-check");
                                $icon->setAttribute('aria-hidden', "true");
                                $del->appendChild($icon);
                                $modalFooter->appendChild($del);
                            } else {
                                if ($elem["av"] == 0) {
                                    $alert = $page->createElement('div');
                                    $alert->setAttribute('class', 'alert alert-danger w-100 my-2');
                                    $alert->setAttribute('role', 'alert');
                                    $alert->appendChild($page->createTextNode('Prodotto non acquistabile al momento.'));
                                    $modalBody->appendChild($alert);
                                } else if ($cartMode) {
                                    $del = $page->createElement('button');
                                    $del->setAttribute('class', 'add btn btn-outline-light');
                                    $del->setAttribute('type', 'button');
                                    $del->setAttribute('onclick', isset($_SESSION[ACCOUNT]) ? 'addToCart('.$_GET['prodid'].');' : 'window.location="/login/login.html";');
                                    $del->setAttribute('title', 'Aggiungi al carrello');
                                    $icon = $page->createElement('i');
                                    $icon->setAttribute('class', "fas fa-plus");
                                    $icon->setAttribute('aria-hidden', "true");
                                    $del->appendChild($icon);
                                    $modalFooter->appendChild($del);
                                }
                            }


                            $content = $page->createElement('div');
                            $content->setAttribute('class', 'container mt-3');

                            $firstRow = $page->createElement('div');
                            $firstRow->setAttribute('class', 'row justify-content-center mt-4');
                            $secondRow = $page->createElement('div');
                            $secondRow->setAttribute('class', 'row justify-content-center');
                            $thirdRow = $page->createElement('div');
                            $thirdRow->setAttribute('class', 'row justify-content-between mt-2');
                            $fourthRow = $page->createElement('div');
                            $fourthRow->setAttribute('class', 'row justify-content-center mt-3');
                            $fifthRow = $page->createElement('div');
                            $fifthRow->setAttribute('class', 'row');

                            $sectionTitle = $page->createElement('h4');
                            $sectionTitle->appendChild($page->createTextNode('Informazioni'));
                            $firstRow->appendChild($sectionTitle);

                            if ($ownerMode) {
                                $description = $page->createElement('textarea');
                                $description->setAttribute('form', 'form');
                                $description->setAttribute('maxlength', $DMAX);
                                $description->appendChild($page->createTextNode($elem["descr"]));
                                $description->setAttribute('class', 'w-100 text-area-desc form-control');
                                $description->setAttribute('id', 'Pdescription');
                                $secondRow->appendChild($description);
                                $sH1 = $page->createElement('h5');
                                $sH1->appendChild($page->createTextNode("Descrizione breve ($SDMAX caratteri)"));
                                $sdescription = $page->createElement('textarea');
                                $sdescription->setAttribute('form', 'form');
                                $sdescription->setAttribute('maxlength', $SDMAX);
                                $sdescription->appendChild($page->createTextNode($elem["sd"]));
                                $sdescription->setAttribute('class', 'w-100 text-area-desc form-control');
                                $sdescription->setAttribute('id', 'Psd');
                                $secondRow->appendChild($sH1);
                                $secondRow->appendChild($sdescription);
                            } else {
                                $description = $page->createElement('div');
                                $description->setAttribute('class', 'col');
                                $description->appendChild($page->createTextNode($elem["sd"]));
                                $description->appendChild(new br());
                                $description->appendChild(new br());
                                $description->appendChild($page->createTextNode($elem["descr"]));
                                $secondRow->appendChild($description);
                            }

                            if ($ownerMode) {
                                $priceTag1 = $page->createElement('span');
                                $priceTag1->setAttribute('class', 'font-weight-light cost col');
                                $priceTag = $page->createElement('span');
                                $priceTag->setAttribute('class', 'font-weight-light cost col');
                                $priceInput = $page->createElement('input');
                                $priceInput->setAttribute('type', 'number');
                                $priceInput->setAttribute('class', 'form-control font-weight-light cost col text-right');
                                $priceInput->setAttribute('value', $elem["cost"]);
                                $priceInput->setAttribute('id', 'Pcost');
                                $priceTag->appendChild($page->createTextNode(" €"));
                                $priceTag1->appendChild($page->createTextNode('Prezzo: '));
                                $thirdRow->appendChild($priceTag1);
                                $thirdRow->appendChild($priceInput);
                            } else {
                                $priceTag = $page->createElement('p');
                                $priceTag->setAttribute('class', 'font-weight-light cost text-right col');
                                $priceTag->appendChild($page->createTextNode($elem["cost"]." €"));
                            }
                            $thirdRow->appendChild($priceTag);

                            $sectionTitle2 = $page->createElement('h5');
                            $sectionTitle2->appendChild($page->createTextNode('Informazioni sul produttore'));
                            $fourthRow->appendChild($sectionTitle2);

                            $leftProductorColumn = $page->createElement('div');
                            $leftProductorColumn->setAttribute('class', 'col-4');

                            $productorName = $page->createElement('span');
                            $productorName->setAttribute('class', 'row text-left font-weight-bold');
                            $productorName->appendChild($page->createTextNode($elem["pName"]));
                            $leftProductorColumn->appendChild($productorName);

                            $productorAddress = $page->createElement('span');
                            $productorAddress->setAttribute('class', 'row text-left font-weight-light mt-3');
                            $productorAddress->appendChild($page->createTextNode($elem["pAddr"]));
                            $leftProductorColumn->appendChild($productorAddress);

                            $pid = $page->createElement('small');
                            $pid->setAttribute('class', 'productor row text-left mt-1');
                            $pid->appendChild($page->createTextNode("ID produttore: ".$elem["PID"]));
                            $leftProductorColumn->appendChild($pid);
                            $pid = $page->createElement('small');
                            $pid->setAttribute('class', 'productor row text-left mt-1');
                            $pid->appendChild($page->createTextNode("IVA: ".$elem["IVA"]));
                            $leftProductorColumn->appendChild($pid);

                            $rightProducerColumn = $page->createElement('div');
                            $rightProducerColumn->setAttribute('class', 'col-8 text-right font-italic overflow-auto');
                            $rightProducerColumn->appendChild($page->createTextNode($elem["pDescr"]));

                            $fifthRow->appendChild($leftProductorColumn);
                            $fifthRow->appendChild($rightProducerColumn);

                            $content->appendChild($firstRow);
                            $content->appendChild($secondRow);
                            $content->appendChild($thirdRow);
                            $content->appendChild($fourthRow);
                            $content->appendChild($fifthRow);
                            $modalBody->appendChild($content);

                            $closeButton->appendChild($closeIcon);
                            $modalHeader->appendChild($modalTitle);
                            $modalHeader->appendChild($closeButton);

                            $modalContent->appendChild($modalHeader);
                            $modalContent->appendChild($modalBody);
                            $modalContent->appendChild($modalFooter);

                            $modalDialog->appendChild($modalContent);

                            $main->appendChild($modalDialog);

                            $page->appendChild($main);

                            $res = html_entity_decode($page->saveHTML());
                            $result[OK] = true;
                            $result[HTML] = $res;
                        } else {
                            $result[OK] = 404;
                            $result[ERROR] = "Elemento non trovato";
                        }
                    }
                }
            } else {
                $result[OK] = 405;
                $result[ERROR] = "Errore nella preparazione della query: ".$stmt->error;
            }
        }
    } else {
        $result[OK] = 404;
        $result[ERROR] = "Errore input not found";
    }
}
header('Content-Type: application/json');
echo(json_encode($result));
?>