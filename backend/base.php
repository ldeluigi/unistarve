<?php
define("OK", "ok");
define("ERROR", "message");
define("RESULT", "result");
define("REDIRECT", "redirect");
define("ID", "id");
define("ACCOUNT", "account");
define("TOKEN", "token");
define("HTML", "html");
define("REPORT", "report");
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "my_unistarve";
$EMAX = 50; // email
$PMAX = 128; // password
$NMAX = 30; // name
$CMAX = 30; // surname
$CFMAX = 20; // fc
$PNMAX = 200; // prod name
$SDMAX = 300; //short desc
$PIVAMAX = 20; // PIVA
$AMAX = 200; // address
$DMAX = 2048; // prod description
$TMAX = 20; // telephone
$TNMAX = 200; // transp max
$TMIN = 5; // telephone
$COMAX = 50; //code
$PTMAX = 20; //payment_type
$PIMAX = 20; //pin
mysqli_report(MYSQLI_REPORT_OFF);
$SESSION_DURATION = 3600; // seconds
ini_set('session.gc_maxlifetime', $SESSION_DURATION);
session_set_cookie_params($SESSION_DURATION, "/");
mb_internal_encoding("UTF-8");

function refresh_session() {
    global $SESSION_DURATION;
    if (session_status() == PHP_SESSION_ACTIVE) {
        setcookie(session_name(), session_id(), time() + $SESSION_DURATION, "/"); // REFRESH
    }
}
function connectDB(): mysqli {
    global $servername;
    global $username;
    global $password;
    global $dbname;
    $db = new mysqli($servername, $username, $password, $dbname);
    $db->set_charset("utf8");
    return $db;
}
?>