<?php
require("base.php");
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 301;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $go = true;
        if (isset($_GET["delete"]) or isset($_GET["update"]) or isset($_GET["set"])) {
            $go = false;
            if (!(isset($_GET["prodid"]) and is_numeric($_GET["prodid"]))) {
                $result[OK] = 302;
                $result[ERROR] = "I parametri non sono numerici o sono mancanti.";
            } else if (isset($_GET["delete"])) {
                $stmt = $conn -> prepare("DELETE FROM ON_CART WHERE product=? AND cart IN (SELECT C.ID AS cart FROM CART AS C
                                            JOIN CONSUMERS AS CO ON C.ID=CO.cart
                                            JOIN ACCOUNTS AS A ON A.consumer=CO.CF
                                            WHERE A.ID=?)");
                if ($stmt === false) {
                    $result[OK] = 303;
                    $result[ERROR] = "Errore durante la preparazione della query.";
                } else {
                    if ($stmt -> bind_param ("ii",$_GET["prodid"], $_SESSION[ACCOUNT])) {
                        if ($stmt->execute() and $stmt->affected_rows > 0) {
                            $go = true;
                        } else {
                            $result[OK] = 305;
                            $result[ERROR] = "Errore durante l'esecuzione della query.".$stmt->error."(".$stmt->affected_rows." righe eliminate)";
                        }
                    } else {
                        $result[OK] = 304;
                        $result[ERROR] = "Errore durante la preparazione della query.";
                    }
                }
            } else if (isset($_GET["update"]) and isset($_GET["quantity"]) and is_numeric($_GET["quantity"])) {
                $stmt = $conn -> prepare("UPDATE ON_CART SET quantity=quantity+(?) WHERE product=? AND quantity+(?)>0 AND cart IN (SELECT C.ID AS cart FROM CART AS C
                                            JOIN CONSUMERS AS CO ON C.ID=CO.cart
                                            JOIN ACCOUNTS AS A ON A.consumer=CO.CF
                                            WHERE A.ID=?)");
                if ($stmt === false) {
                    $result[OK] = 306;
                    $result[ERROR] = "Errore durante la preparazione della query.";
                } else {
                    if ($stmt -> bind_param ("iiii", $_GET["quantity"], $_GET["prodid"], $_GET["quantity"], $_SESSION[ACCOUNT])) {
                        if ($stmt->execute() and $stmt->affected_rows > 0) {
                            $go = true;
                        } else {
                            $result[OK] = 307;
                            $result[ERROR] = "Errore durante l'esecuzione della query.".$stmt->error."(".$stmt->affected_rows." righe modificate)";
                        }
                    } else {
                        $result[OK] = 308;
                        $result[ERROR] = "Errore durante la preparazione della query.";
                    }
                }
            } else if (isset($_GET["quantity"]) and is_numeric($_GET["quantity"]) and $_GET["quantity"] > 0) {
                $stmt = $conn -> prepare("REPLACE INTO ON_CART (product, cart, quantity) SELECT ?, C.ID AS cart, ? FROM CART AS C
                                            JOIN CONSUMERS AS CO ON C.ID=CO.cart
                                            JOIN ACCOUNTS AS A ON A.consumer=CO.CF
                                            WHERE A.ID=?");
                if ($stmt === false) {
                    $result[OK] = 314;
                    $result[ERROR] = "Errore durante la preparazione della query.";
                } else {
                    if ($stmt -> bind_param ("iii", $_GET["prodid"], $_GET["quantity"], $_SESSION[ACCOUNT])) {
                        if ($stmt->execute() and $stmt->affected_rows > 0) {
                            $go = true;
                        } else {
                            $result[OK] = 312;
                            $result[ERROR] = "Errore durante l'esecuzione della query.".$stmt->error."(".$stmt->affected_rows." righe modificate)";
                        }
                    } else {
                        $result[OK] = 313;
                        $result[ERROR] = "Errore durante la preparazione della query.";
                    }
                }
            } else {
                $result[OK] = 315;
                $result[ERROR] = "La quantita iniziale deve essere positiva.";
            }
        }
        if ($go) {
            $stmt = $conn->prepare("SELECT O.name AS name, O.img_link AS img, O.short_description AS description, O.cost AS price, OC.quantity AS quantity, OC.product AS ID, OC.cart AS cart, O.productor AS PID FROM ORDINABLES AS O
                                    JOIN ON_CART AS OC ON OC.product=O.ID
                                    JOIN CART AS C ON OC.cart=C.ID
                                    JOIN CONSUMERS AS CS ON CS.cart=C.ID
                                    JOIN ACCOUNTS AS A ON A.consumer=CS.CF
                                    WHERE A.ID=?");
            if (!$stmt) {
                $result[OK] = 309;
                $result[ERROR] = "Errore durante la preparazione della query";
            } else {
                if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                    if ($stmt->execute()) {
                        $cart = $stmt->get_result();
                        if ($cart->num_rows === 0) {
                            $page = new DOMDocument();
                            $page->normalizeDocument();
                            $page->formatOutput = true;
                            $p = $page->createElement('p');
                            $p->setAttribute('class', 'no-messages font-italic text-center lead w-50 mx-auto border rounded');
                            $p->appendChild($page->createTextNode("Carrello vuoto"));
                            $page->appendChild($p);
                            $res = html_entity_decode($page->saveHTML());
                        } else {
                            $res = generate($cart, PHP_INT_MAX);
                        }
                        $result[OK] = true;
                        $result[HTML] = $res;
                    } else {
                        $result[OK] = 310;
                        $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                    } 
                } else {
                    $result[OK] = 311;
                    $result[ERROR] = "Errore nella creazione della query per i messaggi";
                }
            }
        }
    }
} else {
    $result[OK] = 300;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));


function generate($res, $RESULT_NUM, $resultMultiplier = 1, $pageIndex = 0) {
    $page = new DOMDocument();
    $page->normalizeDocument();
    $page->formatOutput = true;

    $list = $page->createElement('ul');
    $list->setAttribute('class', 'cartproducts list-unstyled');
    $moreResults = $res->num_rows > ($pageIndex + 1) * $RESULT_NUM * $resultMultiplier;
    if ($pageIndex > 0 and $RESULT_NUM * $resultMultiplier > 0) {
        $res->data_seek($pageIndex * $RESULT_NUM * $resultMultiplier);
    }
    $total = 0;
    for ($i = 0; $i < $RESULT_NUM * $resultMultiplier and $elem = $res->fetch_assoc(); $i++) {
        $onProductClick = 'if (typeof showProduct == "function") {
                            showProduct('.$elem["ID"].'); 
                           } else {
                               window.location.href="/product/product.html?id=' . $elem["ID"].'";
                           }';

        $el = $page->createElement('li');
        $el->setAttribute('class', 'cartproduct media border rounded w-100 mb-1 cartprod-'.$elem["ID"]);

        $h = $page->createElement('div');
        $h->setAttribute('class', 'm-3 media-body w-50');
        $h->setAttribute('onclick', $onProductClick);
        
        $ee = $page->createElement('h5');
        $ee->setAttribute('class', 'mt-0 mb-1');
        
        $id = $page->createElement('p');
        $id->setAttribute('class', 'font-weight-light cost row text-right mt-5');
        $id->appendChild($page->createTextNode($elem["quantity"]." x ".$elem["price"]."€ = ".((int)$elem["quantity"] * $elem["price"])."€"));
        
        $rdiv = $page->createElement('div');
        $rdiv->setAttribute('class', 'float-right rdiv w-25 container mt-4');

        if ($elem["img"] and strlen($elem["img"]) > 0) {
            $dimg = $page->createElement('div');
            $dimg->setAttribute('class', 'ml-2 my-auto prodImageC');
            $dimg->setAttribute('onclick', $onProductClick);
            $img = $page->createElement('img');
            $img->setAttribute('class', 'my-auto prodImage img-thumbnail rounded mw-25 lazy');
            $img->setAttribute('alt', $elem["name"]);
            $img->setAttribute('src', '/img/unibologostarve.gif');
            $img->setAttribute('data-src', $elem["img"]);
            
            $dimg->appendChild($img);
            $el->appendChild($dimg);
        }
        
        $pid = $page->createElement('small');
        $pid->setAttribute('class', 'productor');
        $pid->appendChild($page->createTextNode("ID produttore: ".$elem["PID"]));

        $descrip = $page->createElement('div');
        $descrip->setAttribute('class', 'description mb-0 text-truncate mw-100');
        $pd = $page->createElement('p');
        $pd->appendChild($page->createTextNode($elem["description"]));
        $pd->setAttribute('class', 'text-truncate');
        $descrip->appendChild($pd);
        $descrip->appendChild($pid);

        $ee->appendChild($page->createTextNode($elem["name"]));
        $h->appendChild($ee);
        $h->appendChild($descrip);

        $el->appendChild($h);

        //buttons

        $buttons = $page->createElement('div');
        $buttons->setAttribute('class', 'row justify-content-center');

        $plus = $page->createElement('button');
        $plus->setAttribute('class', 'add btn btn-outline-light col-auto');
        $plus->setAttribute('type', 'button');
        $plus->setAttribute('onclick', 'plusCart('.$elem["ID"].');');
        $plus->setAttribute('title', 'Aggiungi 1 '.$elem["name"].' al carrello');
        $icon = $page->createElement('i');
        $icon->setAttribute('class', "fas fa-plus");
        $icon->setAttribute('aria-hidden', "true");
        $plus->appendChild($icon);
        $buttons->appendChild($plus);

        $del = $page->createElement('button');
        $del->setAttribute('class', 'add btn btn-outline-light col-auto');
        $del->setAttribute('type', 'button');
        $del->setAttribute('onclick', 'minusCart('.$elem["ID"].');');
        $del->setAttribute('title', 'Sottrai 1 '.$elem["name"].' dal carrello');
        $icon = $page->createElement('i');
        $icon->setAttribute('class', "fas fa-minus");
        $icon->setAttribute('aria-hidden', "true");
        $del->appendChild($icon);
        $buttons->appendChild($del);

        $trash = $page->createElement('button');
        $trash->setAttribute('class', 'add btn btn-outline-light col-auto');
        $trash->setAttribute('type', 'button');
        $trash->setAttribute('onclick', 'removeCart('.$elem["ID"].');');
        $trash->setAttribute('title', 'Rimuovi completamente '.$elem["name"].' dal carrello');
        $icon = $page->createElement('i');
        $icon->setAttribute('class', "fas fa-trash-alt");
        $icon->setAttribute('aria-hidden', "true");
        $trash->appendChild($icon);
        $buttons->appendChild($trash);

        $rdiv->appendChild($buttons);

        $rdiv->appendChild($id);
        $el->appendChild($rdiv);

        $list->appendChild($el);
        $total += ((int)$elem["quantity"] * $elem["price"]);
    }
    if ($i === 0) {
        $p = $page->createElement('p');
        $p->setAttribute('class', 'no-messages font-italic text-center lead');
        $p->appendChild($page->createTextNode("Carrello vuoto"));
        $page->appendChild($p);
    } else {
        $page->appendChild($list);
        if ($moreResults) {
            $mDiv = $page->createElement('div');
            $mDiv->setAttribute('class', 'mx-auto w-50 my-2 text-center');
            $more = $page->createElement('button');
            $more->setAttribute('class', 'btn btn-outline-light mx-auto');
            $more->setAttribute('type', 'button');
            $more->setAttribute('onclick', 'moreItems('.($pageIndex + 1).','.($resultMultiplier + 1).');');
            $more->appendChild($page->createTextNode('Altri Risultati'));
            $mDiv->appendChild($more);
            $page->appendChild($mDiv);
        }
        $okDiv = $page->createElement('div');
        $okDiv->setAttribute('class', 'mx-auto w-100 my-2 text-center');
        $buy = $page->createElement('button');
        $buy->setAttribute('class', 'btn btn-outline-light mx-auto btn-lg');
        $buy->setAttribute('type', 'button');
        $buy->setAttribute('onclick', 'buyCart();');
        $buy->appendChild($page->createTextNode('Ordina questi prodotti'));
        $okDiv->appendChild($buy);
        $totalP = $page->createElement('span');
        $totalP->setAttribute('class', 'font-weight-bold text-right float-right total p-3 mb-2 bg-dark text-white rounded mr-3 mt-5');
        $totalP->appendChild($page->createTextNode('Totale: '.$total.'€'));
        $okDiv->appendChild($totalP);
        $page->appendChild($okDiv);
    }
    $res = html_entity_decode($page->saveHTML());
    return $res;
}
?>