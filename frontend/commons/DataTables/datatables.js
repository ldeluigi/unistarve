/*
 * This combined file was created by the DataTables downloader builder:
 *   https://datatables.net/download
 *
 * To rebuild or modify this file with the latest versions of the included
 * software please visit:
 *   https://datatables.net/download/#bs4/dt-1.10.18/e-1.8.1/r-2.2.2
 *
 * Included libraries:
 *   DataTables 1.10.18, Editor 1.8.1, Responsive 2.2.2
 */

/*! DataTables 1.10.18
 * ©2008-2018 SpryMedia Ltd - datatables.net/license
 */

/**
 * @summary     DataTables
 * @description Paginate, search and order HTML tables
 * @version     1.10.18
 * @file        jquery.dataTables.js
 * @author      SpryMedia Ltd
 * @contact     www.datatables.net
 * @copyright   Copyright 2008-2018 SpryMedia Ltd.
 *
 * This source file is free software, available under the following license:
 *   MIT license - http://datatables.net/license
 *
 * This source file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 *
 * For details please refer to: http://www.datatables.net
 */

/*jslint evil: true, undef: true, browser: true */
/*globals $,require,jQuery,define,_selector_run,_selector_opts,_selector_first,_selector_row_indexes,_ext,_Api,_api_register,_api_registerPlural,_re_new_lines,_re_html,_re_formatted_numeric,_re_escape_regex,_empty,_intVal,_numToDecimal,_isNumber,_isHtml,_htmlNumeric,_pluck,_pluck_order,_range,_stripHtml,_unique,_fnBuildAjax,_fnAjaxUpdate,_fnAjaxParameters,_fnAjaxUpdateDraw,_fnAjaxDataSrc,_fnAddColumn,_fnColumnOptions,_fnAdjustColumnSizing,_fnVisibleToColumnIndex,_fnColumnIndexToVisible,_fnVisbleColumns,_fnGetColumns,_fnColumnTypes,_fnApplyColumnDefs,_fnHungarianMap,_fnCamelToHungarian,_fnLanguageCompat,_fnBrowserDetect,_fnAddData,_fnAddTr,_fnNodeToDataIndex,_fnNodeToColumnIndex,_fnGetCellData,_fnSetCellData,_fnSplitObjNotation,_fnGetObjectDataFn,_fnSetObjectDataFn,_fnGetDataMaster,_fnClearTable,_fnDeleteIndex,_fnInvalidate,_fnGetRowElements,_fnCreateTr,_fnBuildHead,_fnDrawHead,_fnDraw,_fnReDraw,_fnAddOptionsHtml,_fnDetectHeader,_fnGetUniqueThs,_fnFeatureHtmlFilter,_fnFilterComplete,_fnFilterCustom,_fnFilterColumn,_fnFilter,_fnFilterCreateSearch,_fnEscapeRegex,_fnFilterData,_fnFeatureHtmlInfo,_fnUpdateInfo,_fnInfoMacros,_fnInitialise,_fnInitComplete,_fnLengthChange,_fnFeatureHtmlLength,_fnFeatureHtmlPaginate,_fnPageChange,_fnFeatureHtmlProcessing,_fnProcessingDisplay,_fnFeatureHtmlTable,_fnScrollDraw,_fnApplyToChildren,_fnCalculateColumnWidths,_fnThrottle,_fnConvertToWidth,_fnGetWidestNode,_fnGetMaxLenString,_fnStringToCss,_fnSortFlatten,_fnSort,_fnSortAria,_fnSortListener,_fnSortAttachListener,_fnSortingClasses,_fnSortData,_fnSaveState,_fnLoadState,_fnSettingsFromNode,_fnLog,_fnMap,_fnBindAction,_fnCallbackReg,_fnCallbackFire,_fnLengthOverflow,_fnRenderer,_fnDataSource,_fnRowAttributes*/

(function( factory ) {
	"use strict";

	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				// CommonJS environments without a window global must pass a
				// root. This will give an error otherwise
				root = window;
			}

			if ( ! $ ) {
				$ = typeof window !== 'undefined' ? // jQuery's factory checks for a global window
					require('jquery') :
					require('jquery')( root );
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}
(function( $, window, document, undefined ) {
	"use strict";

	/**
	 * DataTables is a plug-in for the jQuery Javascript library. It is a highly
	 * flexible tool, based upon the foundations of progressive enhancement,
	 * which will add advanced interaction controls to any HTML table. For a
	 * full list of features please refer to
	 * [DataTables.net](href="http://datatables.net).
	 *
	 * Note that the `DataTable` object is not a global variable but is aliased
	 * to `jQuery.fn.DataTable` and `jQuery.fn.dataTable` through which it may
	 * be  accessed.
	 *
	 *  @class
	 *  @param {object} [init={}] Configuration object for DataTables. Options
	 *    are defined by {@link DataTable.defaults}
	 *  @requires jQuery 1.7+
	 *
	 *  @example
	 *    // Basic initialisation
	 *    $(document).ready( function {
	 *      $('#example').dataTable();
	 *    } );
	 *
	 *  @example
	 *    // Initialisation with configuration options - in this case, disable
	 *    // pagination and sorting.
	 *    $(document).ready( function {
	 *      $('#example').dataTable( {
	 *        "paginate": false,
	 *        "sort": false
	 *      } );
	 *    } );
	 */
	var DataTable = function ( options )
	{
		/**
		 * Perform a jQuery selector action on the table's TR elements (from the tbody) and
		 * return the resulting jQuery object.
		 *  @param {string|node|jQuery} sSelector jQuery selector or node collection to act on
		 *  @param {object} [oOpts] Optional parameters for modifying the rows to be included
		 *  @param {string} [oOpts.filter=none] Select TR elements that meet the current filter
		 *    criterion ("applied") or all TR elements (i.e. no filter).
		 *  @param {string} [oOpts.order=current] Order of the TR elements in the processed array.
		 *    Can be either 'current', whereby the current sorting of the table is used, or
		 *    'original' whereby the original order the data was read into the table is used.
		 *  @param {string} [oOpts.page=all] Limit the selection to the currently displayed page
		 *    ("current") or not ("all"). If 'current' is given, then order is assumed to be
		 *    'current' and filter is 'applied', regardless of what they might be given as.
		 *  @returns {object} jQuery object, filtered by the given selector.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Highlight every second row
		 *      oTable.$('tr:odd').css('backgroundColor', 'blue');
		 *    } );
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Filter to rows with 'Webkit' in them, add a background colour and then
		 *      // remove the filter, thus highlighting the 'Webkit' rows only.
		 *      oTable.fnFilter('Webkit');
		 *      oTable.$('tr', {"search": "applied"}).css('backgroundColor', 'blue');
		 *      oTable.fnFilter('');
		 *    } );
		 */
		this.$ = function ( sSelector, oOpts )
		{
			return this.api(true).$( sSelector, oOpts );
		};
		
		
		/**
		 * Almost identical to $ in operation, but in this case returns the data for the matched
		 * rows - as such, the jQuery selector used should match TR row nodes or TD/TH cell nodes
		 * rather than any descendants, so the data can be obtained for the row/cell. If matching
		 * rows are found, the data returned is the original data array/object that was used to
		 * create the row (or a generated array if from a DOM source).
		 *
		 * This method is often useful in-combination with $ where both functions are given the
		 * same parameters and the array indexes will match identically.
		 *  @param {string|node|jQuery} sSelector jQuery selector or node collection to act on
		 *  @param {object} [oOpts] Optional parameters for modifying the rows to be included
		 *  @param {string} [oOpts.filter=none] Select elements that meet the current filter
		 *    criterion ("applied") or all elements (i.e. no filter).
		 *  @param {string} [oOpts.order=current] Order of the data in the processed array.
		 *    Can be either 'current', whereby the current sorting of the table is used, or
		 *    'original' whereby the original order the data was read into the table is used.
		 *  @param {string} [oOpts.page=all] Limit the selection to the currently displayed page
		 *    ("current") or not ("all"). If 'current' is given, then order is assumed to be
		 *    'current' and filter is 'applied', regardless of what they might be given as.
		 *  @returns {array} Data for the matched elements. If any elements, as a result of the
		 *    selector, were not TR, TD or TH elements in the DataTable, they will have a null
		 *    entry in the array.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Get the data from the first row in the table
		 *      var data = oTable._('tr:first');
		 *
		 *      // Do something useful with the data
		 *      alert( "First cell is: "+data[0] );
		 *    } );
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Filter to 'Webkit' and get all data for
		 *      oTable.fnFilter('Webkit');
		 *      var data = oTable._('tr', {"search": "applied"});
		 *
		 *      // Do something with the data
		 *      alert( data.length+" rows matched the search" );
		 *    } );
		 */
		this._ = function ( sSelector, oOpts )
		{
			return this.api(true).rows( sSelector, oOpts ).data();
		};
		
		
		/**
		 * Create a DataTables Api instance, with the currently selected tables for
		 * the Api's context.
		 * @param {boolean} [traditional=false] Set the API instance's context to be
		 *   only the table referred to by the `DataTable.ext.iApiIndex` option, as was
		 *   used in the API presented by DataTables 1.9- (i.e. the traditional mode),
		 *   or if all tables captured in the jQuery object should be used.
		 * @return {DataTables.Api}
		 */
		this.api = function ( traditional )
		{
			return traditional ?
				new _Api(
					_fnSettingsFromNode( this[ _ext.iApiIndex ] )
				) :
				new _Api( this );
		};
		
		
		/**
		 * Add a single new row or multiple rows of data to the table. Please note
		 * that this is suitable for client-side processing only - if you are using
		 * server-side processing (i.e. "bServerSide": true), then to add data, you
		 * must add it to the data source, i.e. the server-side, through an Ajax call.
		 *  @param {array|object} data The data to be added to the table. This can be:
		 *    <ul>
		 *      <li>1D array of data - add a single row with the data provided</li>
		 *      <li>2D array of arrays - add multiple rows in a single call</li>
		 *      <li>object - data object when using <i>mData</i></li>
		 *      <li>array of objects - multiple data objects when using <i>mData</i></li>
		 *    </ul>
		 *  @param {bool} [redraw=true] redraw the table or not
		 *  @returns {array} An array of integers, representing the list of indexes in
		 *    <i>aoData</i> ({@link DataTable.models.oSettings}) that have been added to
		 *    the table.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    // Global var for counter
		 *    var giCount = 2;
		 *
		 *    $(document).ready(function() {
		 *      $('#example').dataTable();
		 *    } );
		 *
		 *    function fnClickAddRow() {
		 *      $('#example').dataTable().fnAddData( [
		 *        giCount+".1",
		 *        giCount+".2",
		 *        giCount+".3",
		 *        giCount+".4" ]
		 *      );
		 *
		 *      giCount++;
		 *    }
		 */
		this.fnAddData = function( data, redraw )
		{
			var api = this.api( true );
		
			/* Check if we want to add multiple rows or not */
			var rows = $.isArray(data) && ( $.isArray(data[0]) || $.isPlainObject(data[0]) ) ?
				api.rows.add( data ) :
				api.row.add( data );
		
			if ( redraw === undefined || redraw ) {
				api.draw();
			}
		
			return rows.flatten().toArray();
		};
		
		
		/**
		 * This function will make DataTables recalculate the column sizes, based on the data
		 * contained in the table and the sizes applied to the columns (in the DOM, CSS or
		 * through the sWidth parameter). This can be useful when the width of the table's
		 * parent element changes (for example a window resize).
		 *  @param {boolean} [bRedraw=true] Redraw the table or not, you will typically want to
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable( {
		 *        "sScrollY": "200px",
		 *        "bPaginate": false
		 *      } );
		 *
		 *      $(window).on('resize', function () {
		 *        oTable.fnAdjustColumnSizing();
		 *      } );
		 *    } );
		 */
		this.fnAdjustColumnSizing = function ( bRedraw )
		{
			var api = this.api( true ).columns.adjust();
			var settings = api.settings()[0];
			var scroll = settings.oScroll;
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw( false );
			}
			else if ( scroll.sX !== "" || scroll.sY !== "" ) {
				/* If not redrawing, but scrolling, we want to apply the new column sizes anyway */
				_fnScrollDraw( settings );
			}
		};
		
		
		/**
		 * Quickly and simply clear a table
		 *  @param {bool} [bRedraw=true] redraw the table or not
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Immediately 'nuke' the current rows (perhaps waiting for an Ajax callback...)
		 *      oTable.fnClearTable();
		 *    } );
		 */
		this.fnClearTable = function( bRedraw )
		{
			var api = this.api( true ).clear();
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw();
			}
		};
		
		
		/**
		 * The exact opposite of 'opening' a row, this function will close any rows which
		 * are currently 'open'.
		 *  @param {node} nTr the table row to 'close'
		 *  @returns {int} 0 on success, or 1 if failed (can't find the row)
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable;
		 *
		 *      // 'open' an information row when a row is clicked on
		 *      $('#example tbody tr').click( function () {
		 *        if ( oTable.fnIsOpen(this) ) {
		 *          oTable.fnClose( this );
		 *        } else {
		 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
		 *        }
		 *      } );
		 *
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnClose = function( nTr )
		{
			this.api( true ).row( nTr ).child.hide();
		};
		
		
		/**
		 * Remove a row for the table
		 *  @param {mixed} target The index of the row from aoData to be deleted, or
		 *    the TR element you want to delete
		 *  @param {function|null} [callBack] Callback function
		 *  @param {bool} [redraw=true] Redraw the table or not
		 *  @returns {array} The row that was deleted
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Immediately remove the first row
		 *      oTable.fnDeleteRow( 0 );
		 *    } );
		 */
		this.fnDeleteRow = function( target, callback, redraw )
		{
			var api = this.api( true );
			var rows = api.rows( target );
			var settings = rows.settings()[0];
			var data = settings.aoData[ rows[0][0] ];
		
			rows.remove();
		
			if ( callback ) {
				callback.call( this, settings, data );
			}
		
			if ( redraw === undefined || redraw ) {
				api.draw();
			}
		
			return data;
		};
		
		
		/**
		 * Restore the table to it's original state in the DOM by removing all of DataTables
		 * enhancements, alterations to the DOM structure of the table and event listeners.
		 *  @param {boolean} [remove=false] Completely remove the table from the DOM
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      // This example is fairly pointless in reality, but shows how fnDestroy can be used
		 *      var oTable = $('#example').dataTable();
		 *      oTable.fnDestroy();
		 *    } );
		 */
		this.fnDestroy = function ( remove )
		{
			this.api( true ).destroy( remove );
		};
		
		
		/**
		 * Redraw the table
		 *  @param {bool} [complete=true] Re-filter and resort (if enabled) the table before the draw.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Re-draw the table - you wouldn't want to do it here, but it's an example :-)
		 *      oTable.fnDraw();
		 *    } );
		 */
		this.fnDraw = function( complete )
		{
			// Note that this isn't an exact match to the old call to _fnDraw - it takes
			// into account the new data, but can hold position.
			this.api( true ).draw( complete );
		};
		
		
		/**
		 * Filter the input based on data
		 *  @param {string} sInput String to filter the table on
		 *  @param {int|null} [iColumn] Column to limit filtering to
		 *  @param {bool} [bRegex=false] Treat as regular expression or not
		 *  @param {bool} [bSmart=true] Perform smart filtering or not
		 *  @param {bool} [bShowGlobal=true] Show the input global filter in it's input box(es)
		 *  @param {bool} [bCaseInsensitive=true] Do case-insensitive matching (true) or not (false)
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Sometime later - filter...
		 *      oTable.fnFilter( 'test string' );
		 *    } );
		 */
		this.fnFilter = function( sInput, iColumn, bRegex, bSmart, bShowGlobal, bCaseInsensitive )
		{
			var api = this.api( true );
		
			if ( iColumn === null || iColumn === undefined ) {
				api.search( sInput, bRegex, bSmart, bCaseInsensitive );
			}
			else {
				api.column( iColumn ).search( sInput, bRegex, bSmart, bCaseInsensitive );
			}
		
			api.draw();
		};
		
		
		/**
		 * Get the data for the whole table, an individual row or an individual cell based on the
		 * provided parameters.
		 *  @param {int|node} [src] A TR row node, TD/TH cell node or an integer. If given as
		 *    a TR node then the data source for the whole row will be returned. If given as a
		 *    TD/TH cell node then iCol will be automatically calculated and the data for the
		 *    cell returned. If given as an integer, then this is treated as the aoData internal
		 *    data index for the row (see fnGetPosition) and the data for that row used.
		 *  @param {int} [col] Optional column index that you want the data of.
		 *  @returns {array|object|string} If mRow is undefined, then the data for all rows is
		 *    returned. If mRow is defined, just data for that row, and is iCol is
		 *    defined, only data for the designated cell is returned.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    // Row data
		 *    $(document).ready(function() {
		 *      oTable = $('#example').dataTable();
		 *
		 *      oTable.$('tr').click( function () {
		 *        var data = oTable.fnGetData( this );
		 *        // ... do something with the array / object of data for the row
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Individual cell data
		 *    $(document).ready(function() {
		 *      oTable = $('#example').dataTable();
		 *
		 *      oTable.$('td').click( function () {
		 *        var sData = oTable.fnGetData( this );
		 *        alert( 'The cell clicked on had the value of '+sData );
		 *      } );
		 *    } );
		 */
		this.fnGetData = function( src, col )
		{
			var api = this.api( true );
		
			if ( src !== undefined ) {
				var type = src.nodeName ? src.nodeName.toLowerCase() : '';
		
				return col !== undefined || type == 'td' || type == 'th' ?
					api.cell( src, col ).data() :
					api.row( src ).data() || null;
			}
		
			return api.data().toArray();
		};
		
		
		/**
		 * Get an array of the TR nodes that are used in the table's body. Note that you will
		 * typically want to use the '$' API method in preference to this as it is more
		 * flexible.
		 *  @param {int} [iRow] Optional row index for the TR element you want
		 *  @returns {array|node} If iRow is undefined, returns an array of all TR elements
		 *    in the table's body, or iRow is defined, just the TR element requested.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Get the nodes from the table
		 *      var nNodes = oTable.fnGetNodes( );
		 *    } );
		 */
		this.fnGetNodes = function( iRow )
		{
			var api = this.api( true );
		
			return iRow !== undefined ?
				api.row( iRow ).node() :
				api.rows().nodes().flatten().toArray();
		};
		
		
		/**
		 * Get the array indexes of a particular cell from it's DOM element
		 * and column index including hidden columns
		 *  @param {node} node this can either be a TR, TD or TH in the table's body
		 *  @returns {int} If nNode is given as a TR, then a single index is returned, or
		 *    if given as a cell, an array of [row index, column index (visible),
		 *    column index (all)] is given.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      $('#example tbody td').click( function () {
		 *        // Get the position of the current data from the node
		 *        var aPos = oTable.fnGetPosition( this );
		 *
		 *        // Get the data array for this row
		 *        var aData = oTable.fnGetData( aPos[0] );
		 *
		 *        // Update the data array and return the value
		 *        aData[ aPos[1] ] = 'clicked';
		 *        this.innerHTML = 'clicked';
		 *      } );
		 *
		 *      // Init DataTables
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnGetPosition = function( node )
		{
			var api = this.api( true );
			var nodeName = node.nodeName.toUpperCase();
		
			if ( nodeName == 'TR' ) {
				return api.row( node ).index();
			}
			else if ( nodeName == 'TD' || nodeName == 'TH' ) {
				var cell = api.cell( node ).index();
		
				return [
					cell.row,
					cell.columnVisible,
					cell.column
				];
			}
			return null;
		};
		
		
		/**
		 * Check to see if a row is 'open' or not.
		 *  @param {node} nTr the table row to check
		 *  @returns {boolean} true if the row is currently open, false otherwise
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable;
		 *
		 *      // 'open' an information row when a row is clicked on
		 *      $('#example tbody tr').click( function () {
		 *        if ( oTable.fnIsOpen(this) ) {
		 *          oTable.fnClose( this );
		 *        } else {
		 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
		 *        }
		 *      } );
		 *
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnIsOpen = function( nTr )
		{
			return this.api( true ).row( nTr ).child.isShown();
		};
		
		
		/**
		 * This function will place a new row directly after a row which is currently
		 * on display on the page, with the HTML contents that is passed into the
		 * function. This can be used, for example, to ask for confirmation that a
		 * particular record should be deleted.
		 *  @param {node} nTr The table row to 'open'
		 *  @param {string|node|jQuery} mHtml The HTML to put into the row
		 *  @param {string} sClass Class to give the new TD cell
		 *  @returns {node} The row opened. Note that if the table row passed in as the
		 *    first parameter, is not found in the table, this method will silently
		 *    return.
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable;
		 *
		 *      // 'open' an information row when a row is clicked on
		 *      $('#example tbody tr').click( function () {
		 *        if ( oTable.fnIsOpen(this) ) {
		 *          oTable.fnClose( this );
		 *        } else {
		 *          oTable.fnOpen( this, "Temporary row opened", "info_row" );
		 *        }
		 *      } );
		 *
		 *      oTable = $('#example').dataTable();
		 *    } );
		 */
		this.fnOpen = function( nTr, mHtml, sClass )
		{
			return this.api( true )
				.row( nTr )
				.child( mHtml, sClass )
				.show()
				.child()[0];
		};
		
		
		/**
		 * Change the pagination - provides the internal logic for pagination in a simple API
		 * function. With this function you can have a DataTables table go to the next,
		 * previous, first or last pages.
		 *  @param {string|int} mAction Paging action to take: "first", "previous", "next" or "last"
		 *    or page number to jump to (integer), note that page 0 is the first page.
		 *  @param {bool} [bRedraw=true] Redraw the table or not
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      oTable.fnPageChange( 'next' );
		 *    } );
		 */
		this.fnPageChange = function ( mAction, bRedraw )
		{
			var api = this.api( true ).page( mAction );
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw(false);
			}
		};
		
		
		/**
		 * Show a particular column
		 *  @param {int} iCol The column whose display should be changed
		 *  @param {bool} bShow Show (true) or hide (false) the column
		 *  @param {bool} [bRedraw=true] Redraw the table or not
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Hide the second column after initialisation
		 *      oTable.fnSetColumnVis( 1, false );
		 *    } );
		 */
		this.fnSetColumnVis = function ( iCol, bShow, bRedraw )
		{
			var api = this.api( true ).column( iCol ).visible( bShow );
		
			if ( bRedraw === undefined || bRedraw ) {
				api.columns.adjust().draw();
			}
		};
		
		
		/**
		 * Get the settings for a particular table for external manipulation
		 *  @returns {object} DataTables settings object. See
		 *    {@link DataTable.models.oSettings}
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      var oSettings = oTable.fnSettings();
		 *
		 *      // Show an example parameter from the settings
		 *      alert( oSettings._iDisplayStart );
		 *    } );
		 */
		this.fnSettings = function()
		{
			return _fnSettingsFromNode( this[_ext.iApiIndex] );
		};
		
		
		/**
		 * Sort the table by a particular column
		 *  @param {int} iCol the data index to sort on. Note that this will not match the
		 *    'display index' if you have hidden data entries
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Sort immediately with columns 0 and 1
		 *      oTable.fnSort( [ [0,'asc'], [1,'asc'] ] );
		 *    } );
		 */
		this.fnSort = function( aaSort )
		{
			this.api( true ).order( aaSort ).draw();
		};
		
		
		/**
		 * Attach a sort listener to an element for a given column
		 *  @param {node} nNode the element to attach the sort listener to
		 *  @param {int} iColumn the column that a click on this node will sort on
		 *  @param {function} [fnCallback] callback function when sort is run
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *
		 *      // Sort on column 1, when 'sorter' is clicked on
		 *      oTable.fnSortListener( document.getElementById('sorter'), 1 );
		 *    } );
		 */
		this.fnSortListener = function( nNode, iColumn, fnCallback )
		{
			this.api( true ).order.listener( nNode, iColumn, fnCallback );
		};
		
		
		/**
		 * Update a table cell or row - this method will accept either a single value to
		 * update the cell with, an array of values with one element for each column or
		 * an object in the same format as the original data source. The function is
		 * self-referencing in order to make the multi column updates easier.
		 *  @param {object|array|string} mData Data to update the cell/row with
		 *  @param {node|int} mRow TR element you want to update or the aoData index
		 *  @param {int} [iColumn] The column to update, give as null or undefined to
		 *    update a whole row.
		 *  @param {bool} [bRedraw=true] Redraw the table or not
		 *  @param {bool} [bAction=true] Perform pre-draw actions or not
		 *  @returns {int} 0 on success, 1 on error
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      oTable.fnUpdate( 'Example update', 0, 0 ); // Single cell
		 *      oTable.fnUpdate( ['a', 'b', 'c', 'd', 'e'], $('tbody tr')[0] ); // Row
		 *    } );
		 */
		this.fnUpdate = function( mData, mRow, iColumn, bRedraw, bAction )
		{
			var api = this.api( true );
		
			if ( iColumn === undefined || iColumn === null ) {
				api.row( mRow ).data( mData );
			}
			else {
				api.cell( mRow, iColumn ).data( mData );
			}
		
			if ( bAction === undefined || bAction ) {
				api.columns.adjust();
			}
		
			if ( bRedraw === undefined || bRedraw ) {
				api.draw();
			}
			return 0;
		};
		
		
		/**
		 * Provide a common method for plug-ins to check the version of DataTables being used, in order
		 * to ensure compatibility.
		 *  @param {string} sVersion Version string to check for, in the format "X.Y.Z". Note that the
		 *    formats "X" and "X.Y" are also acceptable.
		 *  @returns {boolean} true if this version of DataTables is greater or equal to the required
		 *    version, or false if this version of DataTales is not suitable
		 *  @method
		 *  @dtopt API
		 *  @deprecated Since v1.10
		 *
		 *  @example
		 *    $(document).ready(function() {
		 *      var oTable = $('#example').dataTable();
		 *      alert( oTable.fnVersionCheck( '1.9.0' ) );
		 *    } );
		 */
		this.fnVersionCheck = _ext.fnVersionCheck;
		

		var _that = this;
		var emptyInit = options === undefined;
		var len = this.length;

		if ( emptyInit ) {
			options = {};
		}

		this.oApi = this.internal = _ext.internal;

		// Extend with old style plug-in API methods
		for ( var fn in DataTable.ext.internal ) {
			if ( fn ) {
				this[fn] = _fnExternApiFunc(fn);
			}
		}

		this.each(function() {
			// For each initialisation we want to give it a clean initialisation
			// object that can be bashed around
			var o = {};
			var oInit = len > 1 ? // optimisation for single table case
				_fnExtend( o, options, true ) :
				options;

			/*global oInit,_that,emptyInit*/
			var i=0, iLen, j, jLen, k, kLen;
			var sId = this.getAttribute( 'id' );
			var bInitHandedOff = false;
			var defaults = DataTable.defaults;
			var $this = $(this);
			
			
			/* Sanity check */
			if ( this.nodeName.toLowerCase() != 'table' )
			{
				_fnLog( null, 0, 'Non-table node initialisation ('+this.nodeName+')', 2 );
				return;
			}
			
			/* Backwards compatibility for the defaults */
			_fnCompatOpts( defaults );
			_fnCompatCols( defaults.column );
			
			/* Convert the camel-case defaults to Hungarian */
			_fnCamelToHungarian( defaults, defaults, true );
			_fnCamelToHungarian( defaults.column, defaults.column, true );
			
			/* Setting up the initialisation object */
			_fnCamelToHungarian( defaults, $.extend( oInit, $this.data() ) );
			
			
			
			/* Check to see if we are re-initialising a table */
			var allSettings = DataTable.settings;
			for ( i=0, iLen=allSettings.length ; i<iLen ; i++ )
			{
				var s = allSettings[i];
			
				/* Base check on table node */
				if (
					s.nTable == this ||
					(s.nTHead && s.nTHead.parentNode == this) ||
					(s.nTFoot && s.nTFoot.parentNode == this)
				) {
					var bRetrieve = oInit.bRetrieve !== undefined ? oInit.bRetrieve : defaults.bRetrieve;
					var bDestroy = oInit.bDestroy !== undefined ? oInit.bDestroy : defaults.bDestroy;
			
					if ( emptyInit || bRetrieve )
					{
						return s.oInstance;
					}
					else if ( bDestroy )
					{
						s.oInstance.fnDestroy();
						break;
					}
					else
					{
						_fnLog( s, 0, 'Cannot reinitialise DataTable', 3 );
						return;
					}
				}
			
				/* If the element we are initialising has the same ID as a table which was previously
				 * initialised, but the table nodes don't match (from before) then we destroy the old
				 * instance by simply deleting it. This is under the assumption that the table has been
				 * destroyed by other methods. Anyone using non-id selectors will need to do this manually
				 */
				if ( s.sTableId == this.id )
				{
					allSettings.splice( i, 1 );
					break;
				}
			}
			
			/* Ensure the table has an ID - required for accessibility */
			if ( sId === null || sId === "" )
			{
				sId = "DataTables_Table_"+(DataTable.ext._unique++);
				this.id = sId;
			}
			
			/* Create the settings object for this table and set some of the default parameters */
			var oSettings = $.extend( true, {}, DataTable.models.oSettings, {
				"sDestroyWidth": $this[0].style.width,
				"sInstance":     sId,
				"sTableId":      sId
			} );
			oSettings.nTable = this;
			oSettings.oApi   = _that.internal;
			oSettings.oInit  = oInit;
			
			allSettings.push( oSettings );
			
			// Need to add the instance after the instance after the settings object has been added
			// to the settings array, so we can self reference the table instance if more than one
			oSettings.oInstance = (_that.length===1) ? _that : $this.dataTable();
			
			// Backwards compatibility, before we apply all the defaults
			_fnCompatOpts( oInit );
			_fnLanguageCompat( oInit.oLanguage );
			
			// If the length menu is given, but the init display length is not, use the length menu
			if ( oInit.aLengthMenu && ! oInit.iDisplayLength )
			{
				oInit.iDisplayLength = $.isArray( oInit.aLengthMenu[0] ) ?
					oInit.aLengthMenu[0][0] : oInit.aLengthMenu[0];
			}
			
			// Apply the defaults and init options to make a single init object will all
			// options defined from defaults and instance options.
			oInit = _fnExtend( $.extend( true, {}, defaults ), oInit );
			
			
			// Map the initialisation options onto the settings object
			_fnMap( oSettings.oFeatures, oInit, [
				"bPaginate",
				"bLengthChange",
				"bFilter",
				"bSort",
				"bSortMulti",
				"bInfo",
				"bProcessing",
				"bAutoWidth",
				"bSortClasses",
				"bServerSide",
				"bDeferRender"
			] );
			_fnMap( oSettings, oInit, [
				"asStripeClasses",
				"ajax",
				"fnServerData",
				"fnFormatNumber",
				"sServerMethod",
				"aaSorting",
				"aaSortingFixed",
				"aLengthMenu",
				"sPaginationType",
				"sAjaxSource",
				"sAjaxDataProp",
				"iStateDuration",
				"sDom",
				"bSortCellsTop",
				"iTabIndex",
				"fnStateLoadCallback",
				"fnStateSaveCallback",
				"renderer",
				"searchDelay",
				"rowId",
				[ "iCookieDuration", "iStateDuration" ], // backwards compat
				[ "oSearch", "oPreviousSearch" ],
				[ "aoSearchCols", "aoPreSearchCols" ],
				[ "iDisplayLength", "_iDisplayLength" ]
			] );
			_fnMap( oSettings.oScroll, oInit, [
				[ "sScrollX", "sX" ],
				[ "sScrollXInner", "sXInner" ],
				[ "sScrollY", "sY" ],
				[ "bScrollCollapse", "bCollapse" ]
			] );
			_fnMap( oSettings.oLanguage, oInit, "fnInfoCallback" );
			
			/* Callback functions which are array driven */
			_fnCallbackReg( oSettings, 'aoDrawCallback',       oInit.fnDrawCallback,      'user' );
			_fnCallbackReg( oSettings, 'aoServerParams',       oInit.fnServerParams,      'user' );
			_fnCallbackReg( oSettings, 'aoStateSaveParams',    oInit.fnStateSaveParams,   'user' );
			_fnCallbackReg( oSettings, 'aoStateLoadParams',    oInit.fnStateLoadParams,   'user' );
			_fnCallbackReg( oSettings, 'aoStateLoaded',        oInit.fnStateLoaded,       'user' );
			_fnCallbackReg( oSettings, 'aoRowCallback',        oInit.fnRowCallback,       'user' );
			_fnCallbackReg( oSettings, 'aoRowCreatedCallback', oInit.fnCreatedRow,        'user' );
			_fnCallbackReg( oSettings, 'aoHeaderCallback',     oInit.fnHeaderCallback,    'user' );
			_fnCallbackReg( oSettings, 'aoFooterCallback',     oInit.fnFooterCallback,    'user' );
			_fnCallbackReg( oSettings, 'aoInitComplete',       oInit.fnInitComplete,      'user' );
			_fnCallbackReg( oSettings, 'aoPreDrawCallback',    oInit.fnPreDrawCallback,   'user' );
			
			oSettings.rowIdFn = _fnGetObjectDataFn( oInit.rowId );
			
			/* Browser support detection */
			_fnBrowserDetect( oSettings );
			
			var oClasses = oSettings.oClasses;
			
			$.extend( oClasses, DataTable.ext.classes, oInit.oClasses );
			$this.addClass( oClasses.sTable );
			
			
			if ( oSettings.iInitDisplayStart === undefined )
			{
				/* Display start point, taking into account the save saving */
				oSettings.iInitDisplayStart = oInit.iDisplayStart;
				oSettings._iDisplayStart = oInit.iDisplayStart;
			}
			
			if ( oInit.iDeferLoading !== null )
			{
				oSettings.bDeferLoading = true;
				var tmp = $.isArray( oInit.iDeferLoading );
				oSettings._iRecordsDisplay = tmp ? oInit.iDeferLoading[0] : oInit.iDeferLoading;
				oSettings._iRecordsTotal = tmp ? oInit.iDeferLoading[1] : oInit.iDeferLoading;
			}
			
			/* Language definitions */
			var oLanguage = oSettings.oLanguage;
			$.extend( true, oLanguage, oInit.oLanguage );
			
			if ( oLanguage.sUrl )
			{
				/* Get the language definitions from a file - because this Ajax call makes the language
				 * get async to the remainder of this function we use bInitHandedOff to indicate that
				 * _fnInitialise will be fired by the returned Ajax handler, rather than the constructor
				 */
				$.ajax( {
					dataType: 'json',
					url: oLanguage.sUrl,
					success: function ( json ) {
						_fnLanguageCompat( json );
						_fnCamelToHungarian( defaults.oLanguage, json );
						$.extend( true, oLanguage, json );
						_fnInitialise( oSettings );
					},
					error: function () {
						// Error occurred loading language file, continue on as best we can
						_fnInitialise( oSettings );
					}
				} );
				bInitHandedOff = true;
			}
			
			/*
			 * Stripes
			 */
			if ( oInit.asStripeClasses === null )
			{
				oSettings.asStripeClasses =[
					oClasses.sStripeOdd,
					oClasses.sStripeEven
				];
			}
			
			/* Remove row stripe classes if they are already on the table row */
			var stripeClasses = oSettings.asStripeClasses;
			var rowOne = $this.children('tbody').find('tr').eq(0);
			if ( $.inArray( true, $.map( stripeClasses, function(el, i) {
				return rowOne.hasClass(el);
			} ) ) !== -1 ) {
				$('tbody tr', this).removeClass( stripeClasses.join(' ') );
				oSettings.asDestroyStripes = stripeClasses.slice();
			}
			
			/*
			 * Columns
			 * See if we should load columns automatically or use defined ones
			 */
			var anThs = [];
			var aoColumnsInit;
			var nThead = this.getElementsByTagName('thead');
			if ( nThead.length !== 0 )
			{
				_fnDetectHeader( oSettings.aoHeader, nThead[0] );
				anThs = _fnGetUniqueThs( oSettings );
			}
			
			/* If not given a column array, generate one with nulls */
			if ( oInit.aoColumns === null )
			{
				aoColumnsInit = [];
				for ( i=0, iLen=anThs.length ; i<iLen ; i++ )
				{
					aoColumnsInit.push( null );
				}
			}
			else
			{
				aoColumnsInit = oInit.aoColumns;
			}
			
			/* Add the columns */
			for ( i=0, iLen=aoColumnsInit.length ; i<iLen ; i++ )
			{
				_fnAddColumn( oSettings, anThs ? anThs[i] : null );
			}
			
			/* Apply the column definitions */
			_fnApplyColumnDefs( oSettings, oInit.aoColumnDefs, aoColumnsInit, function (iCol, oDef) {
				_fnColumnOptions( oSettings, iCol, oDef );
			} );
			
			/* HTML5 attribute detection - build an mData object automatically if the
			 * attributes are found
			 */
			if ( rowOne.length ) {
				var a = function ( cell, name ) {
					return cell.getAttribute( 'data-'+name ) !== null ? name : null;
				};
			
				$( rowOne[0] ).children('th, td').each( function (i, cell) {
					var col = oSettings.aoColumns[i];
			
					if ( col.mData === i ) {
						var sort = a( cell, 'sort' ) || a( cell, 'order' );
						var filter = a( cell, 'filter' ) || a( cell, 'search' );
			
						if ( sort !== null || filter !== null ) {
							col.mData = {
								_:      i+'.display',
								sort:   sort !== null   ? i+'.@data-'+sort   : undefined,
								type:   sort !== null   ? i+'.@data-'+sort   : undefined,
								filter: filter !== null ? i+'.@data-'+filter : undefined
							};
			
							_fnColumnOptions( oSettings, i );
						}
					}
				} );
			}
			
			var features = oSettings.oFeatures;
			var loadedInit = function () {
				/*
				 * Sorting
				 * @todo For modularisation (1.11) this needs to do into a sort start up handler
				 */
			
				// If aaSorting is not defined, then we use the first indicator in asSorting
				// in case that has been altered, so the default sort reflects that option
				if ( oInit.aaSorting === undefined ) {
					var sorting = oSettings.aaSorting;
					for ( i=0, iLen=sorting.length ; i<iLen ; i++ ) {
						sorting[i][1] = oSettings.aoColumns[ i ].asSorting[0];
					}
				}
			
				/* Do a first pass on the sorting classes (allows any size changes to be taken into
				 * account, and also will apply sorting disabled classes if disabled
				 */
				_fnSortingClasses( oSettings );
			
				if ( features.bSort ) {
					_fnCallbackReg( oSettings, 'aoDrawCallback', function () {
						if ( oSettings.bSorted ) {
							var aSort = _fnSortFlatten( oSettings );
							var sortedColumns = {};
			
							$.each( aSort, function (i, val) {
								sortedColumns[ val.src ] = val.dir;
							} );
			
							_fnCallbackFire( oSettings, null, 'order', [oSettings, aSort, sortedColumns] );
							_fnSortAria( oSettings );
						}
					} );
				}
			
				_fnCallbackReg( oSettings, 'aoDrawCallback', function () {
					if ( oSettings.bSorted || _fnDataSource( oSettings ) === 'ssp' || features.bDeferRender ) {
						_fnSortingClasses( oSettings );
					}
				}, 'sc' );
			
			
				/*
				 * Final init
				 * Cache the header, body and footer as required, creating them if needed
				 */
			
				// Work around for Webkit bug 83867 - store the caption-side before removing from doc
				var captions = $this.children('caption').each( function () {
					this._captionSide = $(this).css('caption-side');
				} );
			
				var thead = $this.children('thead');
				if ( thead.length === 0 ) {
					thead = $('<thead/>').appendTo($this);
				}
				oSettings.nTHead = thead[0];
			
				var tbody = $this.children('tbody');
				if ( tbody.length === 0 ) {
					tbody = $('<tbody/>').appendTo($this);
				}
				oSettings.nTBody = tbody[0];
			
				var tfoot = $this.children('tfoot');
				if ( tfoot.length === 0 && captions.length > 0 && (oSettings.oScroll.sX !== "" || oSettings.oScroll.sY !== "") ) {
					// If we are a scrolling table, and no footer has been given, then we need to create
					// a tfoot element for the caption element to be appended to
					tfoot = $('<tfoot/>').appendTo($this);
				}
			
				if ( tfoot.length === 0 || tfoot.children().length === 0 ) {
					$this.addClass( oClasses.sNoFooter );
				}
				else if ( tfoot.length > 0 ) {
					oSettings.nTFoot = tfoot[0];
					_fnDetectHeader( oSettings.aoFooter, oSettings.nTFoot );
				}
			
				/* Check if there is data passing into the constructor */
				if ( oInit.aaData ) {
					for ( i=0 ; i<oInit.aaData.length ; i++ ) {
						_fnAddData( oSettings, oInit.aaData[ i ] );
					}
				}
				else if ( oSettings.bDeferLoading || _fnDataSource( oSettings ) == 'dom' ) {
					/* Grab the data from the page - only do this when deferred loading or no Ajax
					 * source since there is no point in reading the DOM data if we are then going
					 * to replace it with Ajax data
					 */
					_fnAddTr( oSettings, $(oSettings.nTBody).children('tr') );
				}
			
				/* Copy the data index array */
				oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
			
				/* Initialisation complete - table can be drawn */
				oSettings.bInitialised = true;
			
				/* Check if we need to initialise the table (it might not have been handed off to the
				 * language processor)
				 */
				if ( bInitHandedOff === false ) {
					_fnInitialise( oSettings );
				}
			};
			
			/* Must be done after everything which can be overridden by the state saving! */
			if ( oInit.bStateSave )
			{
				features.bStateSave = true;
				_fnCallbackReg( oSettings, 'aoDrawCallback', _fnSaveState, 'state_save' );
				_fnLoadState( oSettings, oInit, loadedInit );
			}
			else {
				loadedInit();
			}
			
		} );
		_that = null;
		return this;
	};

	
	/*
	 * It is useful to have variables which are scoped locally so only the
	 * DataTables functions can access them and they don't leak into global space.
	 * At the same time these functions are often useful over multiple files in the
	 * core and API, so we list, or at least document, all variables which are used
	 * by DataTables as private variables here. This also ensures that there is no
	 * clashing of variable names and that they can easily referenced for reuse.
	 */
	
	
	// Defined else where
	//  _selector_run
	//  _selector_opts
	//  _selector_first
	//  _selector_row_indexes
	
	var _ext; // DataTable.ext
	var _Api; // DataTable.Api
	var _api_register; // DataTable.Api.register
	var _api_registerPlural; // DataTable.Api.registerPlural
	
	var _re_dic = {};
	var _re_new_lines = /[\r\n]/g;
	var _re_html = /<.*?>/g;
	
	// This is not strict ISO8601 - Date.parse() is quite lax, although
	// implementations differ between browsers.
	var _re_date = /^\d{2,4}[\.\/\-]\d{1,2}[\.\/\-]\d{1,2}([T ]{1}\d{1,2}[:\.]\d{2}([\.:]\d{2})?)?$/;
	
	// Escape regular expression special characters
	var _re_escape_regex = new RegExp( '(\\' + [ '/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\', '$', '^', '-' ].join('|\\') + ')', 'g' );
	
	// http://en.wikipedia.org/wiki/Foreign_exchange_market
	// - \u20BD - Russian ruble.
	// - \u20a9 - South Korean Won
	// - \u20BA - Turkish Lira
	// - \u20B9 - Indian Rupee
	// - R - Brazil (R$) and South Africa
	// - fr - Swiss Franc
	// - kr - Swedish krona, Norwegian krone and Danish krone
	// - \u2009 is thin space and \u202F is narrow no-break space, both used in many
	// - Ƀ - Bitcoin
	// - Ξ - Ethereum
	//   standards as thousands separators.
	var _re_formatted_numeric = /[',$£€¥%\u2009\u202F\u20BD\u20a9\u20BArfkɃΞ]/gi;
	
	
	var _empty = function ( d ) {
		return !d || d === true || d === '-' ? true : false;
	};
	
	
	var _intVal = function ( s ) {
		var integer = parseInt( s, 10 );
		return !isNaN(integer) && isFinite(s) ? integer : null;
	};
	
	// Convert from a formatted number with characters other than `.` as the
	// decimal place, to a Javascript number
	var _numToDecimal = function ( num, decimalPoint ) {
		// Cache created regular expressions for speed as this function is called often
		if ( ! _re_dic[ decimalPoint ] ) {
			_re_dic[ decimalPoint ] = new RegExp( _fnEscapeRegex( decimalPoint ), 'g' );
		}
		return typeof num === 'string' && decimalPoint !== '.' ?
			num.replace( /\./g, '' ).replace( _re_dic[ decimalPoint ], '.' ) :
			num;
	};
	
	
	var _isNumber = function ( d, decimalPoint, formatted ) {
		var strType = typeof d === 'string';
	
		// If empty return immediately so there must be a number if it is a
		// formatted string (this stops the string "k", or "kr", etc being detected
		// as a formatted number for currency
		if ( _empty( d ) ) {
			return true;
		}
	
		if ( decimalPoint && strType ) {
			d = _numToDecimal( d, decimalPoint );
		}
	
		if ( formatted && strType ) {
			d = d.replace( _re_formatted_numeric, '' );
		}
	
		return !isNaN( parseFloat(d) ) && isFinite( d );
	};
	
	
	// A string without HTML in it can be considered to be HTML still
	var _isHtml = function ( d ) {
		return _empty( d ) || typeof d === 'string';
	};
	
	
	var _htmlNumeric = function ( d, decimalPoint, formatted ) {
		if ( _empty( d ) ) {
			return true;
		}
	
		var html = _isHtml( d );
		return ! html ?
			null :
			_isNumber( _stripHtml( d ), decimalPoint, formatted ) ?
				true :
				null;
	};
	
	
	var _pluck = function ( a, prop, prop2 ) {
		var out = [];
		var i=0, ien=a.length;
	
		// Could have the test in the loop for slightly smaller code, but speed
		// is essential here
		if ( prop2 !== undefined ) {
			for ( ; i<ien ; i++ ) {
				if ( a[i] && a[i][ prop ] ) {
					out.push( a[i][ prop ][ prop2 ] );
				}
			}
		}
		else {
			for ( ; i<ien ; i++ ) {
				if ( a[i] ) {
					out.push( a[i][ prop ] );
				}
			}
		}
	
		return out;
	};
	
	
	// Basically the same as _pluck, but rather than looping over `a` we use `order`
	// as the indexes to pick from `a`
	var _pluck_order = function ( a, order, prop, prop2 )
	{
		var out = [];
		var i=0, ien=order.length;
	
		// Could have the test in the loop for slightly smaller code, but speed
		// is essential here
		if ( prop2 !== undefined ) {
			for ( ; i<ien ; i++ ) {
				if ( a[ order[i] ][ prop ] ) {
					out.push( a[ order[i] ][ prop ][ prop2 ] );
				}
			}
		}
		else {
			for ( ; i<ien ; i++ ) {
				out.push( a[ order[i] ][ prop ] );
			}
		}
	
		return out;
	};
	
	
	var _range = function ( len, start )
	{
		var out = [];
		var end;
	
		if ( start === undefined ) {
			start = 0;
			end = len;
		}
		else {
			end = start;
			start = len;
		}
	
		for ( var i=start ; i<end ; i++ ) {
			out.push( i );
		}
	
		return out;
	};
	
	
	var _removeEmpty = function ( a )
	{
		var out = [];
	
		for ( var i=0, ien=a.length ; i<ien ; i++ ) {
			if ( a[i] ) { // careful - will remove all falsy values!
				out.push( a[i] );
			}
		}
	
		return out;
	};
	
	
	var _stripHtml = function ( d ) {
		return d.replace( _re_html, '' );
	};
	
	
	/**
	 * Determine if all values in the array are unique. This means we can short
	 * cut the _unique method at the cost of a single loop. A sorted array is used
	 * to easily check the values.
	 *
	 * @param  {array} src Source array
	 * @return {boolean} true if all unique, false otherwise
	 * @ignore
	 */
	var _areAllUnique = function ( src ) {
		if ( src.length < 2 ) {
			return true;
		}
	
		var sorted = src.slice().sort();
		var last = sorted[0];
	
		for ( var i=1, ien=sorted.length ; i<ien ; i++ ) {
			if ( sorted[i] === last ) {
				return false;
			}
	
			last = sorted[i];
		}
	
		return true;
	};
	
	
	/**
	 * Find the unique elements in a source array.
	 *
	 * @param  {array} src Source array
	 * @return {array} Array of unique items
	 * @ignore
	 */
	var _unique = function ( src )
	{
		if ( _areAllUnique( src ) ) {
			return src.slice();
		}
	
		// A faster unique method is to use object keys to identify used values,
		// but this doesn't work with arrays or objects, which we must also
		// consider. See jsperf.com/compare-array-unique-versions/4 for more
		// information.
		var
			out = [],
			val,
			i, ien=src.length,
			j, k=0;
	
		again: for ( i=0 ; i<ien ; i++ ) {
			val = src[i];
	
			for ( j=0 ; j<k ; j++ ) {
				if ( out[j] === val ) {
					continue again;
				}
			}
	
			out.push( val );
			k++;
		}
	
		return out;
	};
	
	
	/**
	 * DataTables utility methods
	 * 
	 * This namespace provides helper methods that DataTables uses internally to
	 * create a DataTable, but which are not exclusively used only for DataTables.
	 * These methods can be used by extension authors to save the duplication of
	 * code.
	 *
	 *  @namespace
	 */
	DataTable.util = {
		/**
		 * Throttle the calls to a function. Arguments and context are maintained
		 * for the throttled function.
		 *
		 * @param {function} fn Function to be called
		 * @param {integer} freq Call frequency in mS
		 * @return {function} Wrapped function
		 */
		throttle: function ( fn, freq ) {
			var
				frequency = freq !== undefined ? freq : 200,
				last,
				timer;
	
			return function () {
				var
					that = this,
					now  = +new Date(),
					args = arguments;
	
				if ( last && now < last + frequency ) {
					clearTimeout( timer );
	
					timer = setTimeout( function () {
						last = undefined;
						fn.apply( that, args );
					}, frequency );
				}
				else {
					last = now;
					fn.apply( that, args );
				}
			};
		},
	
	
		/**
		 * Escape a string such that it can be used in a regular expression
		 *
		 *  @param {string} val string to escape
		 *  @returns {string} escaped string
		 */
		escapeRegex: function ( val ) {
			return val.replace( _re_escape_regex, '\\$1' );
		}
	};
	
	
	
	/**
	 * Create a mapping object that allows camel case parameters to be looked up
	 * for their Hungarian counterparts. The mapping is stored in a private
	 * parameter called `_hungarianMap` which can be accessed on the source object.
	 *  @param {object} o
	 *  @memberof DataTable#oApi
	 */
	function _fnHungarianMap ( o )
	{
		var
			hungarian = 'a aa ai ao as b fn i m o s ',
			match,
			newKey,
			map = {};
	
		$.each( o, function (key, val) {
			match = key.match(/^([^A-Z]+?)([A-Z])/);
	
			if ( match && hungarian.indexOf(match[1]+' ') !== -1 )
			{
				newKey = key.replace( match[0], match[2].toLowerCase() );
				map[ newKey ] = key;
	
				if ( match[1] === 'o' )
				{
					_fnHungarianMap( o[key] );
				}
			}
		} );
	
		o._hungarianMap = map;
	}
	
	
	/**
	 * Convert from camel case parameters to Hungarian, based on a Hungarian map
	 * created by _fnHungarianMap.
	 *  @param {object} src The model object which holds all parameters that can be
	 *    mapped.
	 *  @param {object} user The object to convert from camel case to Hungarian.
	 *  @param {boolean} force When set to `true`, properties which already have a
	 *    Hungarian value in the `user` object will be overwritten. Otherwise they
	 *    won't be.
	 *  @memberof DataTable#oApi
	 */
	function _fnCamelToHungarian ( src, user, force )
	{
		if ( ! src._hungarianMap ) {
			_fnHungarianMap( src );
		}
	
		var hungarianKey;
	
		$.each( user, function (key, val) {
			hungarianKey = src._hungarianMap[ key ];
	
			if ( hungarianKey !== undefined && (force || user[hungarianKey] === undefined) )
			{
				// For objects, we need to buzz down into the object to copy parameters
				if ( hungarianKey.charAt(0) === 'o' )
				{
					// Copy the camelCase options over to the hungarian
					if ( ! user[ hungarianKey ] ) {
						user[ hungarianKey ] = {};
					}
					$.extend( true, user[hungarianKey], user[key] );
	
					_fnCamelToHungarian( src[hungarianKey], user[hungarianKey], force );
				}
				else {
					user[hungarianKey] = user[ key ];
				}
			}
		} );
	}
	
	
	/**
	 * Language compatibility - when certain options are given, and others aren't, we
	 * need to duplicate the values over, in order to provide backwards compatibility
	 * with older language files.
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnLanguageCompat( lang )
	{
		// Note the use of the Hungarian notation for the parameters in this method as
		// this is called after the mapping of camelCase to Hungarian
		var defaults = DataTable.defaults.oLanguage;
	
		// Default mapping
		var defaultDecimal = defaults.sDecimal;
		if ( defaultDecimal ) {
			_addNumericSort( defaultDecimal );
		}
	
		if ( lang ) {
			var zeroRecords = lang.sZeroRecords;
	
			// Backwards compatibility - if there is no sEmptyTable given, then use the same as
			// sZeroRecords - assuming that is given.
			if ( ! lang.sEmptyTable && zeroRecords &&
				defaults.sEmptyTable === "No data available in table" )
			{
				_fnMap( lang, lang, 'sZeroRecords', 'sEmptyTable' );
			}
	
			// Likewise with loading records
			if ( ! lang.sLoadingRecords && zeroRecords &&
				defaults.sLoadingRecords === "Loading..." )
			{
				_fnMap( lang, lang, 'sZeroRecords', 'sLoadingRecords' );
			}
	
			// Old parameter name of the thousands separator mapped onto the new
			if ( lang.sInfoThousands ) {
				lang.sThousands = lang.sInfoThousands;
			}
	
			var decimal = lang.sDecimal;
			if ( decimal && defaultDecimal !== decimal ) {
				_addNumericSort( decimal );
			}
		}
	}
	
	
	/**
	 * Map one parameter onto another
	 *  @param {object} o Object to map
	 *  @param {*} knew The new parameter name
	 *  @param {*} old The old parameter name
	 */
	var _fnCompatMap = function ( o, knew, old ) {
		if ( o[ knew ] !== undefined ) {
			o[ old ] = o[ knew ];
		}
	};
	
	
	/**
	 * Provide backwards compatibility for the main DT options. Note that the new
	 * options are mapped onto the old parameters, so this is an external interface
	 * change only.
	 *  @param {object} init Object to map
	 */
	function _fnCompatOpts ( init )
	{
		_fnCompatMap( init, 'ordering',      'bSort' );
		_fnCompatMap( init, 'orderMulti',    'bSortMulti' );
		_fnCompatMap( init, 'orderClasses',  'bSortClasses' );
		_fnCompatMap( init, 'orderCellsTop', 'bSortCellsTop' );
		_fnCompatMap( init, 'order',         'aaSorting' );
		_fnCompatMap( init, 'orderFixed',    'aaSortingFixed' );
		_fnCompatMap( init, 'paging',        'bPaginate' );
		_fnCompatMap( init, 'pagingType',    'sPaginationType' );
		_fnCompatMap( init, 'pageLength',    'iDisplayLength' );
		_fnCompatMap( init, 'searching',     'bFilter' );
	
		// Boolean initialisation of x-scrolling
		if ( typeof init.sScrollX === 'boolean' ) {
			init.sScrollX = init.sScrollX ? '100%' : '';
		}
		if ( typeof init.scrollX === 'boolean' ) {
			init.scrollX = init.scrollX ? '100%' : '';
		}
	
		// Column search objects are in an array, so it needs to be converted
		// element by element
		var searchCols = init.aoSearchCols;
	
		if ( searchCols ) {
			for ( var i=0, ien=searchCols.length ; i<ien ; i++ ) {
				if ( searchCols[i] ) {
					_fnCamelToHungarian( DataTable.models.oSearch, searchCols[i] );
				}
			}
		}
	}
	
	
	/**
	 * Provide backwards compatibility for column options. Note that the new options
	 * are mapped onto the old parameters, so this is an external interface change
	 * only.
	 *  @param {object} init Object to map
	 */
	function _fnCompatCols ( init )
	{
		_fnCompatMap( init, 'orderable',     'bSortable' );
		_fnCompatMap( init, 'orderData',     'aDataSort' );
		_fnCompatMap( init, 'orderSequence', 'asSorting' );
		_fnCompatMap( init, 'orderDataType', 'sortDataType' );
	
		// orderData can be given as an integer
		var dataSort = init.aDataSort;
		if ( typeof dataSort === 'number' && ! $.isArray( dataSort ) ) {
			init.aDataSort = [ dataSort ];
		}
	}
	
	
	/**
	 * Browser feature detection for capabilities, quirks
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnBrowserDetect( settings )
	{
		// We don't need to do this every time DataTables is constructed, the values
		// calculated are specific to the browser and OS configuration which we
		// don't expect to change between initialisations
		if ( ! DataTable.__browser ) {
			var browser = {};
			DataTable.__browser = browser;
	
			// Scrolling feature / quirks detection
			var n = $('<div/>')
				.css( {
					position: 'fixed',
					top: 0,
					left: $(window).scrollLeft()*-1, // allow for scrolling
					height: 1,
					width: 1,
					overflow: 'hidden'
				} )
				.append(
					$('<div/>')
						.css( {
							position: 'absolute',
							top: 1,
							left: 1,
							width: 100,
							overflow: 'scroll'
						} )
						.append(
							$('<div/>')
								.css( {
									width: '100%',
									height: 10
								} )
						)
				)
				.appendTo( 'body' );
	
			var outer = n.children();
			var inner = outer.children();
	
			// Numbers below, in order, are:
			// inner.offsetWidth, inner.clientWidth, outer.offsetWidth, outer.clientWidth
			//
			// IE6 XP:                           100 100 100  83
			// IE7 Vista:                        100 100 100  83
			// IE 8+ Windows:                     83  83 100  83
			// Evergreen Windows:                 83  83 100  83
			// Evergreen Mac with scrollbars:     85  85 100  85
			// Evergreen Mac without scrollbars: 100 100 100 100
	
			// Get scrollbar width
			browser.barWidth = outer[0].offsetWidth - outer[0].clientWidth;
	
			// IE6/7 will oversize a width 100% element inside a scrolling element, to
			// include the width of the scrollbar, while other browsers ensure the inner
			// element is contained without forcing scrolling
			browser.bScrollOversize = inner[0].offsetWidth === 100 && outer[0].clientWidth !== 100;
	
			// In rtl text layout, some browsers (most, but not all) will place the
			// scrollbar on the left, rather than the right.
			browser.bScrollbarLeft = Math.round( inner.offset().left ) !== 1;
	
			// IE8- don't provide height and width for getBoundingClientRect
			browser.bBounding = n[0].getBoundingClientRect().width ? true : false;
	
			n.remove();
		}
	
		$.extend( settings.oBrowser, DataTable.__browser );
		settings.oScroll.iBarWidth = DataTable.__browser.barWidth;
	}
	
	
	/**
	 * Array.prototype reduce[Right] method, used for browsers which don't support
	 * JS 1.6. Done this way to reduce code size, since we iterate either way
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnReduce ( that, fn, init, start, end, inc )
	{
		var
			i = start,
			value,
			isSet = false;
	
		if ( init !== undefined ) {
			value = init;
			isSet = true;
		}
	
		while ( i !== end ) {
			if ( ! that.hasOwnProperty(i) ) {
				continue;
			}
	
			value = isSet ?
				fn( value, that[i], i, that ) :
				that[i];
	
			isSet = true;
			i += inc;
		}
	
		return value;
	}
	
	/**
	 * Add a column to the list used for the table with default values
	 *  @param {object} oSettings dataTables settings object
	 *  @param {node} nTh The th element for this column
	 *  @memberof DataTable#oApi
	 */
	function _fnAddColumn( oSettings, nTh )
	{
		// Add column to aoColumns array
		var oDefaults = DataTable.defaults.column;
		var iCol = oSettings.aoColumns.length;
		var oCol = $.extend( {}, DataTable.models.oColumn, oDefaults, {
			"nTh": nTh ? nTh : document.createElement('th'),
			"sTitle":    oDefaults.sTitle    ? oDefaults.sTitle    : nTh ? nTh.innerHTML : '',
			"aDataSort": oDefaults.aDataSort ? oDefaults.aDataSort : [iCol],
			"mData": oDefaults.mData ? oDefaults.mData : iCol,
			idx: iCol
		} );
		oSettings.aoColumns.push( oCol );
	
		// Add search object for column specific search. Note that the `searchCols[ iCol ]`
		// passed into extend can be undefined. This allows the user to give a default
		// with only some of the parameters defined, and also not give a default
		var searchCols = oSettings.aoPreSearchCols;
		searchCols[ iCol ] = $.extend( {}, DataTable.models.oSearch, searchCols[ iCol ] );
	
		// Use the default column options function to initialise classes etc
		_fnColumnOptions( oSettings, iCol, $(nTh).data() );
	}
	
	
	/**
	 * Apply options for a column
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iCol column index to consider
	 *  @param {object} oOptions object with sType, bVisible and bSearchable etc
	 *  @memberof DataTable#oApi
	 */
	function _fnColumnOptions( oSettings, iCol, oOptions )
	{
		var oCol = oSettings.aoColumns[ iCol ];
		var oClasses = oSettings.oClasses;
		var th = $(oCol.nTh);
	
		// Try to get width information from the DOM. We can't get it from CSS
		// as we'd need to parse the CSS stylesheet. `width` option can override
		if ( ! oCol.sWidthOrig ) {
			// Width attribute
			oCol.sWidthOrig = th.attr('width') || null;
	
			// Style attribute
			var t = (th.attr('style') || '').match(/width:\s*(\d+[pxem%]+)/);
			if ( t ) {
				oCol.sWidthOrig = t[1];
			}
		}
	
		/* User specified column options */
		if ( oOptions !== undefined && oOptions !== null )
		{
			// Backwards compatibility
			_fnCompatCols( oOptions );
	
			// Map camel case parameters to their Hungarian counterparts
			_fnCamelToHungarian( DataTable.defaults.column, oOptions );
	
			/* Backwards compatibility for mDataProp */
			if ( oOptions.mDataProp !== undefined && !oOptions.mData )
			{
				oOptions.mData = oOptions.mDataProp;
			}
	
			if ( oOptions.sType )
			{
				oCol._sManualType = oOptions.sType;
			}
	
			// `class` is a reserved word in Javascript, so we need to provide
			// the ability to use a valid name for the camel case input
			if ( oOptions.className && ! oOptions.sClass )
			{
				oOptions.sClass = oOptions.className;
			}
			if ( oOptions.sClass ) {
				th.addClass( oOptions.sClass );
			}
	
			$.extend( oCol, oOptions );
			_fnMap( oCol, oOptions, "sWidth", "sWidthOrig" );
	
			/* iDataSort to be applied (backwards compatibility), but aDataSort will take
			 * priority if defined
			 */
			if ( oOptions.iDataSort !== undefined )
			{
				oCol.aDataSort = [ oOptions.iDataSort ];
			}
			_fnMap( oCol, oOptions, "aDataSort" );
		}
	
		/* Cache the data get and set functions for speed */
		var mDataSrc = oCol.mData;
		var mData = _fnGetObjectDataFn( mDataSrc );
		var mRender = oCol.mRender ? _fnGetObjectDataFn( oCol.mRender ) : null;
	
		var attrTest = function( src ) {
			return typeof src === 'string' && src.indexOf('@') !== -1;
		};
		oCol._bAttrSrc = $.isPlainObject( mDataSrc ) && (
			attrTest(mDataSrc.sort) || attrTest(mDataSrc.type) || attrTest(mDataSrc.filter)
		);
		oCol._setter = null;
	
		oCol.fnGetData = function (rowData, type, meta) {
			var innerData = mData( rowData, type, undefined, meta );
	
			return mRender && type ?
				mRender( innerData, type, rowData, meta ) :
				innerData;
		};
		oCol.fnSetData = function ( rowData, val, meta ) {
			return _fnSetObjectDataFn( mDataSrc )( rowData, val, meta );
		};
	
		// Indicate if DataTables should read DOM data as an object or array
		// Used in _fnGetRowElements
		if ( typeof mDataSrc !== 'number' ) {
			oSettings._rowReadObject = true;
		}
	
		/* Feature sorting overrides column specific when off */
		if ( !oSettings.oFeatures.bSort )
		{
			oCol.bSortable = false;
			th.addClass( oClasses.sSortableNone ); // Have to add class here as order event isn't called
		}
	
		/* Check that the class assignment is correct for sorting */
		var bAsc = $.inArray('asc', oCol.asSorting) !== -1;
		var bDesc = $.inArray('desc', oCol.asSorting) !== -1;
		if ( !oCol.bSortable || (!bAsc && !bDesc) )
		{
			oCol.sSortingClass = oClasses.sSortableNone;
			oCol.sSortingClassJUI = "";
		}
		else if ( bAsc && !bDesc )
		{
			oCol.sSortingClass = oClasses.sSortableAsc;
			oCol.sSortingClassJUI = oClasses.sSortJUIAscAllowed;
		}
		else if ( !bAsc && bDesc )
		{
			oCol.sSortingClass = oClasses.sSortableDesc;
			oCol.sSortingClassJUI = oClasses.sSortJUIDescAllowed;
		}
		else
		{
			oCol.sSortingClass = oClasses.sSortable;
			oCol.sSortingClassJUI = oClasses.sSortJUI;
		}
	}
	
	
	/**
	 * Adjust the table column widths for new data. Note: you would probably want to
	 * do a redraw after calling this function!
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnAdjustColumnSizing ( settings )
	{
		/* Not interested in doing column width calculation if auto-width is disabled */
		if ( settings.oFeatures.bAutoWidth !== false )
		{
			var columns = settings.aoColumns;
	
			_fnCalculateColumnWidths( settings );
			for ( var i=0 , iLen=columns.length ; i<iLen ; i++ )
			{
				columns[i].nTh.style.width = columns[i].sWidth;
			}
		}
	
		var scroll = settings.oScroll;
		if ( scroll.sY !== '' || scroll.sX !== '')
		{
			_fnScrollDraw( settings );
		}
	
		_fnCallbackFire( settings, null, 'column-sizing', [settings] );
	}
	
	
	/**
	 * Covert the index of a visible column to the index in the data array (take account
	 * of hidden columns)
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iMatch Visible column index to lookup
	 *  @returns {int} i the data index
	 *  @memberof DataTable#oApi
	 */
	function _fnVisibleToColumnIndex( oSettings, iMatch )
	{
		var aiVis = _fnGetColumns( oSettings, 'bVisible' );
	
		return typeof aiVis[iMatch] === 'number' ?
			aiVis[iMatch] :
			null;
	}
	
	
	/**
	 * Covert the index of an index in the data array and convert it to the visible
	 *   column index (take account of hidden columns)
	 *  @param {int} iMatch Column index to lookup
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {int} i the data index
	 *  @memberof DataTable#oApi
	 */
	function _fnColumnIndexToVisible( oSettings, iMatch )
	{
		var aiVis = _fnGetColumns( oSettings, 'bVisible' );
		var iPos = $.inArray( iMatch, aiVis );
	
		return iPos !== -1 ? iPos : null;
	}
	
	
	/**
	 * Get the number of visible columns
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {int} i the number of visible columns
	 *  @memberof DataTable#oApi
	 */
	function _fnVisbleColumns( oSettings )
	{
		var vis = 0;
	
		// No reduce in IE8, use a loop for now
		$.each( oSettings.aoColumns, function ( i, col ) {
			if ( col.bVisible && $(col.nTh).css('display') !== 'none' ) {
				vis++;
			}
		} );
	
		return vis;
	}
	
	
	/**
	 * Get an array of column indexes that match a given property
	 *  @param {object} oSettings dataTables settings object
	 *  @param {string} sParam Parameter in aoColumns to look for - typically
	 *    bVisible or bSearchable
	 *  @returns {array} Array of indexes with matched properties
	 *  @memberof DataTable#oApi
	 */
	function _fnGetColumns( oSettings, sParam )
	{
		var a = [];
	
		$.map( oSettings.aoColumns, function(val, i) {
			if ( val[sParam] ) {
				a.push( i );
			}
		} );
	
		return a;
	}
	
	
	/**
	 * Calculate the 'type' of a column
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnColumnTypes ( settings )
	{
		var columns = settings.aoColumns;
		var data = settings.aoData;
		var types = DataTable.ext.type.detect;
		var i, ien, j, jen, k, ken;
		var col, cell, detectedType, cache;
	
		// For each column, spin over the 
		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			col = columns[i];
			cache = [];
	
			if ( ! col.sType && col._sManualType ) {
				col.sType = col._sManualType;
			}
			else if ( ! col.sType ) {
				for ( j=0, jen=types.length ; j<jen ; j++ ) {
					for ( k=0, ken=data.length ; k<ken ; k++ ) {
						// Use a cache array so we only need to get the type data
						// from the formatter once (when using multiple detectors)
						if ( cache[k] === undefined ) {
							cache[k] = _fnGetCellData( settings, k, i, 'type' );
						}
	
						detectedType = types[j]( cache[k], settings );
	
						// If null, then this type can't apply to this column, so
						// rather than testing all cells, break out. There is an
						// exception for the last type which is `html`. We need to
						// scan all rows since it is possible to mix string and HTML
						// types
						if ( ! detectedType && j !== types.length-1 ) {
							break;
						}
	
						// Only a single match is needed for html type since it is
						// bottom of the pile and very similar to string
						if ( detectedType === 'html' ) {
							break;
						}
					}
	
					// Type is valid for all data points in the column - use this
					// type
					if ( detectedType ) {
						col.sType = detectedType;
						break;
					}
				}
	
				// Fall back - if no type was detected, always use string
				if ( ! col.sType ) {
					col.sType = 'string';
				}
			}
		}
	}
	
	
	/**
	 * Take the column definitions and static columns arrays and calculate how
	 * they relate to column indexes. The callback function will then apply the
	 * definition found for a column to a suitable configuration object.
	 *  @param {object} oSettings dataTables settings object
	 *  @param {array} aoColDefs The aoColumnDefs array that is to be applied
	 *  @param {array} aoCols The aoColumns array that defines columns individually
	 *  @param {function} fn Callback function - takes two parameters, the calculated
	 *    column index and the definition for that column.
	 *  @memberof DataTable#oApi
	 */
	function _fnApplyColumnDefs( oSettings, aoColDefs, aoCols, fn )
	{
		var i, iLen, j, jLen, k, kLen, def;
		var columns = oSettings.aoColumns;
	
		// Column definitions with aTargets
		if ( aoColDefs )
		{
			/* Loop over the definitions array - loop in reverse so first instance has priority */
			for ( i=aoColDefs.length-1 ; i>=0 ; i-- )
			{
				def = aoColDefs[i];
	
				/* Each definition can target multiple columns, as it is an array */
				var aTargets = def.targets !== undefined ?
					def.targets :
					def.aTargets;
	
				if ( ! $.isArray( aTargets ) )
				{
					aTargets = [ aTargets ];
				}
	
				for ( j=0, jLen=aTargets.length ; j<jLen ; j++ )
				{
					if ( typeof aTargets[j] === 'number' && aTargets[j] >= 0 )
					{
						/* Add columns that we don't yet know about */
						while( columns.length <= aTargets[j] )
						{
							_fnAddColumn( oSettings );
						}
	
						/* Integer, basic index */
						fn( aTargets[j], def );
					}
					else if ( typeof aTargets[j] === 'number' && aTargets[j] < 0 )
					{
						/* Negative integer, right to left column counting */
						fn( columns.length+aTargets[j], def );
					}
					else if ( typeof aTargets[j] === 'string' )
					{
						/* Class name matching on TH element */
						for ( k=0, kLen=columns.length ; k<kLen ; k++ )
						{
							if ( aTargets[j] == "_all" ||
							     $(columns[k].nTh).hasClass( aTargets[j] ) )
							{
								fn( k, def );
							}
						}
					}
				}
			}
		}
	
		// Statically defined columns array
		if ( aoCols )
		{
			for ( i=0, iLen=aoCols.length ; i<iLen ; i++ )
			{
				fn( i, aoCols[i] );
			}
		}
	}
	
	/**
	 * Add a data array to the table, creating DOM node etc. This is the parallel to
	 * _fnGatherData, but for adding rows from a Javascript source, rather than a
	 * DOM source.
	 *  @param {object} oSettings dataTables settings object
	 *  @param {array} aData data array to be added
	 *  @param {node} [nTr] TR element to add to the table - optional. If not given,
	 *    DataTables will create a row automatically
	 *  @param {array} [anTds] Array of TD|TH elements for the row - must be given
	 *    if nTr is.
	 *  @returns {int} >=0 if successful (index of new aoData entry), -1 if failed
	 *  @memberof DataTable#oApi
	 */
	function _fnAddData ( oSettings, aDataIn, nTr, anTds )
	{
		/* Create the object for storing information about this new row */
		var iRow = oSettings.aoData.length;
		var oData = $.extend( true, {}, DataTable.models.oRow, {
			src: nTr ? 'dom' : 'data',
			idx: iRow
		} );
	
		oData._aData = aDataIn;
		oSettings.aoData.push( oData );
	
		/* Create the cells */
		var nTd, sThisType;
		var columns = oSettings.aoColumns;
	
		// Invalidate the column types as the new data needs to be revalidated
		for ( var i=0, iLen=columns.length ; i<iLen ; i++ )
		{
			columns[i].sType = null;
		}
	
		/* Add to the display array */
		oSettings.aiDisplayMaster.push( iRow );
	
		var id = oSettings.rowIdFn( aDataIn );
		if ( id !== undefined ) {
			oSettings.aIds[ id ] = oData;
		}
	
		/* Create the DOM information, or register it if already present */
		if ( nTr || ! oSettings.oFeatures.bDeferRender )
		{
			_fnCreateTr( oSettings, iRow, nTr, anTds );
		}
	
		return iRow;
	}
	
	
	/**
	 * Add one or more TR elements to the table. Generally we'd expect to
	 * use this for reading data from a DOM sourced table, but it could be
	 * used for an TR element. Note that if a TR is given, it is used (i.e.
	 * it is not cloned).
	 *  @param {object} settings dataTables settings object
	 *  @param {array|node|jQuery} trs The TR element(s) to add to the table
	 *  @returns {array} Array of indexes for the added rows
	 *  @memberof DataTable#oApi
	 */
	function _fnAddTr( settings, trs )
	{
		var row;
	
		// Allow an individual node to be passed in
		if ( ! (trs instanceof $) ) {
			trs = $(trs);
		}
	
		return trs.map( function (i, el) {
			row = _fnGetRowElements( settings, el );
			return _fnAddData( settings, row.data, el, row.cells );
		} );
	}
	
	
	/**
	 * Take a TR element and convert it to an index in aoData
	 *  @param {object} oSettings dataTables settings object
	 *  @param {node} n the TR element to find
	 *  @returns {int} index if the node is found, null if not
	 *  @memberof DataTable#oApi
	 */
	function _fnNodeToDataIndex( oSettings, n )
	{
		return (n._DT_RowIndex!==undefined) ? n._DT_RowIndex : null;
	}
	
	
	/**
	 * Take a TD element and convert it into a column data index (not the visible index)
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iRow The row number the TD/TH can be found in
	 *  @param {node} n The TD/TH element to find
	 *  @returns {int} index if the node is found, -1 if not
	 *  @memberof DataTable#oApi
	 */
	function _fnNodeToColumnIndex( oSettings, iRow, n )
	{
		return $.inArray( n, oSettings.aoData[ iRow ].anCells );
	}
	
	
	/**
	 * Get the data for a given cell from the internal cache, taking into account data mapping
	 *  @param {object} settings dataTables settings object
	 *  @param {int} rowIdx aoData row id
	 *  @param {int} colIdx Column index
	 *  @param {string} type data get type ('display', 'type' 'filter' 'sort')
	 *  @returns {*} Cell data
	 *  @memberof DataTable#oApi
	 */
	function _fnGetCellData( settings, rowIdx, colIdx, type )
	{
		var draw           = settings.iDraw;
		var col            = settings.aoColumns[colIdx];
		var rowData        = settings.aoData[rowIdx]._aData;
		var defaultContent = col.sDefaultContent;
		var cellData       = col.fnGetData( rowData, type, {
			settings: settings,
			row:      rowIdx,
			col:      colIdx
		} );
	
		if ( cellData === undefined ) {
			if ( settings.iDrawError != draw && defaultContent === null ) {
				_fnLog( settings, 0, "Requested unknown parameter "+
					(typeof col.mData=='function' ? '{function}' : "'"+col.mData+"'")+
					" for row "+rowIdx+", column "+colIdx, 4 );
				settings.iDrawError = draw;
			}
			return defaultContent;
		}
	
		// When the data source is null and a specific data type is requested (i.e.
		// not the original data), we can use default column data
		if ( (cellData === rowData || cellData === null) && defaultContent !== null && type !== undefined ) {
			cellData = defaultContent;
		}
		else if ( typeof cellData === 'function' ) {
			// If the data source is a function, then we run it and use the return,
			// executing in the scope of the data object (for instances)
			return cellData.call( rowData );
		}
	
		if ( cellData === null && type == 'display' ) {
			return '';
		}
		return cellData;
	}
	
	
	/**
	 * Set the value for a specific cell, into the internal data cache
	 *  @param {object} settings dataTables settings object
	 *  @param {int} rowIdx aoData row id
	 *  @param {int} colIdx Column index
	 *  @param {*} val Value to set
	 *  @memberof DataTable#oApi
	 */
	function _fnSetCellData( settings, rowIdx, colIdx, val )
	{
		var col     = settings.aoColumns[colIdx];
		var rowData = settings.aoData[rowIdx]._aData;
	
		col.fnSetData( rowData, val, {
			settings: settings,
			row:      rowIdx,
			col:      colIdx
		}  );
	}
	
	
	// Private variable that is used to match action syntax in the data property object
	var __reArray = /\[.*?\]$/;
	var __reFn = /\(\)$/;
	
	/**
	 * Split string on periods, taking into account escaped periods
	 * @param  {string} str String to split
	 * @return {array} Split string
	 */
	function _fnSplitObjNotation( str )
	{
		return $.map( str.match(/(\\.|[^\.])+/g) || [''], function ( s ) {
			return s.replace(/\\\./g, '.');
		} );
	}
	
	
	/**
	 * Return a function that can be used to get data from a source object, taking
	 * into account the ability to use nested objects as a source
	 *  @param {string|int|function} mSource The data source for the object
	 *  @returns {function} Data get function
	 *  @memberof DataTable#oApi
	 */
	function _fnGetObjectDataFn( mSource )
	{
		if ( $.isPlainObject( mSource ) )
		{
			/* Build an object of get functions, and wrap them in a single call */
			var o = {};
			$.each( mSource, function (key, val) {
				if ( val ) {
					o[key] = _fnGetObjectDataFn( val );
				}
			} );
	
			return function (data, type, row, meta) {
				var t = o[type] || o._;
				return t !== undefined ?
					t(data, type, row, meta) :
					data;
			};
		}
		else if ( mSource === null )
		{
			/* Give an empty string for rendering / sorting etc */
			return function (data) { // type, row and meta also passed, but not used
				return data;
			};
		}
		else if ( typeof mSource === 'function' )
		{
			return function (data, type, row, meta) {
				return mSource( data, type, row, meta );
			};
		}
		else if ( typeof mSource === 'string' && (mSource.indexOf('.') !== -1 ||
			      mSource.indexOf('[') !== -1 || mSource.indexOf('(') !== -1) )
		{
			/* If there is a . in the source string then the data source is in a
			 * nested object so we loop over the data for each level to get the next
			 * level down. On each loop we test for undefined, and if found immediately
			 * return. This allows entire objects to be missing and sDefaultContent to
			 * be used if defined, rather than throwing an error
			 */
			var fetchData = function (data, type, src) {
				var arrayNotation, funcNotation, out, innerSrc;
	
				if ( src !== "" )
				{
					var a = _fnSplitObjNotation( src );
	
					for ( var i=0, iLen=a.length ; i<iLen ; i++ )
					{
						// Check if we are dealing with special notation
						arrayNotation = a[i].match(__reArray);
						funcNotation = a[i].match(__reFn);
	
						if ( arrayNotation )
						{
							// Array notation
							a[i] = a[i].replace(__reArray, '');
	
							// Condition allows simply [] to be passed in
							if ( a[i] !== "" ) {
								data = data[ a[i] ];
							}
							out = [];
	
							// Get the remainder of the nested object to get
							a.splice( 0, i+1 );
							innerSrc = a.join('.');
	
							// Traverse each entry in the array getting the properties requested
							if ( $.isArray( data ) ) {
								for ( var j=0, jLen=data.length ; j<jLen ; j++ ) {
									out.push( fetchData( data[j], type, innerSrc ) );
								}
							}
	
							// If a string is given in between the array notation indicators, that
							// is used to join the strings together, otherwise an array is returned
							var join = arrayNotation[0].substring(1, arrayNotation[0].length-1);
							data = (join==="") ? out : out.join(join);
	
							// The inner call to fetchData has already traversed through the remainder
							// of the source requested, so we exit from the loop
							break;
						}
						else if ( funcNotation )
						{
							// Function call
							a[i] = a[i].replace(__reFn, '');
							data = data[ a[i] ]();
							continue;
						}
	
						if ( data === null || data[ a[i] ] === undefined )
						{
							return undefined;
						}
						data = data[ a[i] ];
					}
				}
	
				return data;
			};
	
			return function (data, type) { // row and meta also passed, but not used
				return fetchData( data, type, mSource );
			};
		}
		else
		{
			/* Array or flat object mapping */
			return function (data, type) { // row and meta also passed, but not used
				return data[mSource];
			};
		}
	}
	
	
	/**
	 * Return a function that can be used to set data from a source object, taking
	 * into account the ability to use nested objects as a source
	 *  @param {string|int|function} mSource The data source for the object
	 *  @returns {function} Data set function
	 *  @memberof DataTable#oApi
	 */
	function _fnSetObjectDataFn( mSource )
	{
		if ( $.isPlainObject( mSource ) )
		{
			/* Unlike get, only the underscore (global) option is used for for
			 * setting data since we don't know the type here. This is why an object
			 * option is not documented for `mData` (which is read/write), but it is
			 * for `mRender` which is read only.
			 */
			return _fnSetObjectDataFn( mSource._ );
		}
		else if ( mSource === null )
		{
			/* Nothing to do when the data source is null */
			return function () {};
		}
		else if ( typeof mSource === 'function' )
		{
			return function (data, val, meta) {
				mSource( data, 'set', val, meta );
			};
		}
		else if ( typeof mSource === 'string' && (mSource.indexOf('.') !== -1 ||
			      mSource.indexOf('[') !== -1 || mSource.indexOf('(') !== -1) )
		{
			/* Like the get, we need to get data from a nested object */
			var setData = function (data, val, src) {
				var a = _fnSplitObjNotation( src ), b;
				var aLast = a[a.length-1];
				var arrayNotation, funcNotation, o, innerSrc;
	
				for ( var i=0, iLen=a.length-1 ; i<iLen ; i++ )
				{
					// Check if we are dealing with an array notation request
					arrayNotation = a[i].match(__reArray);
					funcNotation = a[i].match(__reFn);
	
					if ( arrayNotation )
					{
						a[i] = a[i].replace(__reArray, '');
						data[ a[i] ] = [];
	
						// Get the remainder of the nested object to set so we can recurse
						b = a.slice();
						b.splice( 0, i+1 );
						innerSrc = b.join('.');
	
						// Traverse each entry in the array setting the properties requested
						if ( $.isArray( val ) )
						{
							for ( var j=0, jLen=val.length ; j<jLen ; j++ )
							{
								o = {};
								setData( o, val[j], innerSrc );
								data[ a[i] ].push( o );
							}
						}
						else
						{
							// We've been asked to save data to an array, but it
							// isn't array data to be saved. Best that can be done
							// is to just save the value.
							data[ a[i] ] = val;
						}
	
						// The inner call to setData has already traversed through the remainder
						// of the source and has set the data, thus we can exit here
						return;
					}
					else if ( funcNotation )
					{
						// Function call
						a[i] = a[i].replace(__reFn, '');
						data = data[ a[i] ]( val );
					}
	
					// If the nested object doesn't currently exist - since we are
					// trying to set the value - create it
					if ( data[ a[i] ] === null || data[ a[i] ] === undefined )
					{
						data[ a[i] ] = {};
					}
					data = data[ a[i] ];
				}
	
				// Last item in the input - i.e, the actual set
				if ( aLast.match(__reFn ) )
				{
					// Function call
					data = data[ aLast.replace(__reFn, '') ]( val );
				}
				else
				{
					// If array notation is used, we just want to strip it and use the property name
					// and assign the value. If it isn't used, then we get the result we want anyway
					data[ aLast.replace(__reArray, '') ] = val;
				}
			};
	
			return function (data, val) { // meta is also passed in, but not used
				return setData( data, val, mSource );
			};
		}
		else
		{
			/* Array or flat object mapping */
			return function (data, val) { // meta is also passed in, but not used
				data[mSource] = val;
			};
		}
	}
	
	
	/**
	 * Return an array with the full table data
	 *  @param {object} oSettings dataTables settings object
	 *  @returns array {array} aData Master data array
	 *  @memberof DataTable#oApi
	 */
	function _fnGetDataMaster ( settings )
	{
		return _pluck( settings.aoData, '_aData' );
	}
	
	
	/**
	 * Nuke the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnClearTable( settings )
	{
		settings.aoData.length = 0;
		settings.aiDisplayMaster.length = 0;
		settings.aiDisplay.length = 0;
		settings.aIds = {};
	}
	
	
	 /**
	 * Take an array of integers (index array) and remove a target integer (value - not
	 * the key!)
	 *  @param {array} a Index array to target
	 *  @param {int} iTarget value to find
	 *  @memberof DataTable#oApi
	 */
	function _fnDeleteIndex( a, iTarget, splice )
	{
		var iTargetIndex = -1;
	
		for ( var i=0, iLen=a.length ; i<iLen ; i++ )
		{
			if ( a[i] == iTarget )
			{
				iTargetIndex = i;
			}
			else if ( a[i] > iTarget )
			{
				a[i]--;
			}
		}
	
		if ( iTargetIndex != -1 && splice === undefined )
		{
			a.splice( iTargetIndex, 1 );
		}
	}
	
	
	/**
	 * Mark cached data as invalid such that a re-read of the data will occur when
	 * the cached data is next requested. Also update from the data source object.
	 *
	 * @param {object} settings DataTables settings object
	 * @param {int}    rowIdx   Row index to invalidate
	 * @param {string} [src]    Source to invalidate from: undefined, 'auto', 'dom'
	 *     or 'data'
	 * @param {int}    [colIdx] Column index to invalidate. If undefined the whole
	 *     row will be invalidated
	 * @memberof DataTable#oApi
	 *
	 * @todo For the modularisation of v1.11 this will need to become a callback, so
	 *   the sort and filter methods can subscribe to it. That will required
	 *   initialisation options for sorting, which is why it is not already baked in
	 */
	function _fnInvalidate( settings, rowIdx, src, colIdx )
	{
		var row = settings.aoData[ rowIdx ];
		var i, ien;
		var cellWrite = function ( cell, col ) {
			// This is very frustrating, but in IE if you just write directly
			// to innerHTML, and elements that are overwritten are GC'ed,
			// even if there is a reference to them elsewhere
			while ( cell.childNodes.length ) {
				cell.removeChild( cell.firstChild );
			}
	
			cell.innerHTML = _fnGetCellData( settings, rowIdx, col, 'display' );
		};
	
		// Are we reading last data from DOM or the data object?
		if ( src === 'dom' || ((! src || src === 'auto') && row.src === 'dom') ) {
			// Read the data from the DOM
			row._aData = _fnGetRowElements(
					settings, row, colIdx, colIdx === undefined ? undefined : row._aData
				)
				.data;
		}
		else {
			// Reading from data object, update the DOM
			var cells = row.anCells;
	
			if ( cells ) {
				if ( colIdx !== undefined ) {
					cellWrite( cells[colIdx], colIdx );
				}
				else {
					for ( i=0, ien=cells.length ; i<ien ; i++ ) {
						cellWrite( cells[i], i );
					}
				}
			}
		}
	
		// For both row and cell invalidation, the cached data for sorting and
		// filtering is nulled out
		row._aSortData = null;
		row._aFilterData = null;
	
		// Invalidate the type for a specific column (if given) or all columns since
		// the data might have changed
		var cols = settings.aoColumns;
		if ( colIdx !== undefined ) {
			cols[ colIdx ].sType = null;
		}
		else {
			for ( i=0, ien=cols.length ; i<ien ; i++ ) {
				cols[i].sType = null;
			}
	
			// Update DataTables special `DT_*` attributes for the row
			_fnRowAttributes( settings, row );
		}
	}
	
	
	/**
	 * Build a data source object from an HTML row, reading the contents of the
	 * cells that are in the row.
	 *
	 * @param {object} settings DataTables settings object
	 * @param {node|object} TR element from which to read data or existing row
	 *   object from which to re-read the data from the cells
	 * @param {int} [colIdx] Optional column index
	 * @param {array|object} [d] Data source object. If `colIdx` is given then this
	 *   parameter should also be given and will be used to write the data into.
	 *   Only the column in question will be written
	 * @returns {object} Object with two parameters: `data` the data read, in
	 *   document order, and `cells` and array of nodes (they can be useful to the
	 *   caller, so rather than needing a second traversal to get them, just return
	 *   them from here).
	 * @memberof DataTable#oApi
	 */
	function _fnGetRowElements( settings, row, colIdx, d )
	{
		var
			tds = [],
			td = row.firstChild,
			name, col, o, i=0, contents,
			columns = settings.aoColumns,
			objectRead = settings._rowReadObject;
	
		// Allow the data object to be passed in, or construct
		d = d !== undefined ?
			d :
			objectRead ?
				{} :
				[];
	
		var attr = function ( str, td  ) {
			if ( typeof str === 'string' ) {
				var idx = str.indexOf('@');
	
				if ( idx !== -1 ) {
					var attr = str.substring( idx+1 );
					var setter = _fnSetObjectDataFn( str );
					setter( d, td.getAttribute( attr ) );
				}
			}
		};
	
		// Read data from a cell and store into the data object
		var cellProcess = function ( cell ) {
			if ( colIdx === undefined || colIdx === i ) {
				col = columns[i];
				contents = $.trim(cell.innerHTML);
	
				if ( col && col._bAttrSrc ) {
					var setter = _fnSetObjectDataFn( col.mData._ );
					setter( d, contents );
	
					attr( col.mData.sort, cell );
					attr( col.mData.type, cell );
					attr( col.mData.filter, cell );
				}
				else {
					// Depending on the `data` option for the columns the data can
					// be read to either an object or an array.
					if ( objectRead ) {
						if ( ! col._setter ) {
							// Cache the setter function
							col._setter = _fnSetObjectDataFn( col.mData );
						}
						col._setter( d, contents );
					}
					else {
						d[i] = contents;
					}
				}
			}
	
			i++;
		};
	
		if ( td ) {
			// `tr` element was passed in
			while ( td ) {
				name = td.nodeName.toUpperCase();
	
				if ( name == "TD" || name == "TH" ) {
					cellProcess( td );
					tds.push( td );
				}
	
				td = td.nextSibling;
			}
		}
		else {
			// Existing row object passed in
			tds = row.anCells;
	
			for ( var j=0, jen=tds.length ; j<jen ; j++ ) {
				cellProcess( tds[j] );
			}
		}
	
		// Read the ID from the DOM if present
		var rowNode = row.firstChild ? row : row.nTr;
	
		if ( rowNode ) {
			var id = rowNode.getAttribute( 'id' );
	
			if ( id ) {
				_fnSetObjectDataFn( settings.rowId )( d, id );
			}
		}
	
		return {
			data: d,
			cells: tds
		};
	}
	/**
	 * Create a new TR element (and it's TD children) for a row
	 *  @param {object} oSettings dataTables settings object
	 *  @param {int} iRow Row to consider
	 *  @param {node} [nTrIn] TR element to add to the table - optional. If not given,
	 *    DataTables will create a row automatically
	 *  @param {array} [anTds] Array of TD|TH elements for the row - must be given
	 *    if nTr is.
	 *  @memberof DataTable#oApi
	 */
	function _fnCreateTr ( oSettings, iRow, nTrIn, anTds )
	{
		var
			row = oSettings.aoData[iRow],
			rowData = row._aData,
			cells = [],
			nTr, nTd, oCol,
			i, iLen;
	
		if ( row.nTr === null )
		{
			nTr = nTrIn || document.createElement('tr');
	
			row.nTr = nTr;
			row.anCells = cells;
	
			/* Use a private property on the node to allow reserve mapping from the node
			 * to the aoData array for fast look up
			 */
			nTr._DT_RowIndex = iRow;
	
			/* Special parameters can be given by the data source to be used on the row */
			_fnRowAttributes( oSettings, row );
	
			/* Process each column */
			for ( i=0, iLen=oSettings.aoColumns.length ; i<iLen ; i++ )
			{
				oCol = oSettings.aoColumns[i];
	
				nTd = nTrIn ? anTds[i] : document.createElement( oCol.sCellType );
				nTd._DT_CellIndex = {
					row: iRow,
					column: i
				};
				
				cells.push( nTd );
	
				// Need to create the HTML if new, or if a rendering function is defined
				if ( (!nTrIn || oCol.mRender || oCol.mData !== i) &&
					 (!$.isPlainObject(oCol.mData) || oCol.mData._ !== i+'.display')
				) {
					nTd.innerHTML = _fnGetCellData( oSettings, iRow, i, 'display' );
				}
	
				/* Add user defined class */
				if ( oCol.sClass )
				{
					nTd.className += ' '+oCol.sClass;
				}
	
				// Visibility - add or remove as required
				if ( oCol.bVisible && ! nTrIn )
				{
					nTr.appendChild( nTd );
				}
				else if ( ! oCol.bVisible && nTrIn )
				{
					nTd.parentNode.removeChild( nTd );
				}
	
				if ( oCol.fnCreatedCell )
				{
					oCol.fnCreatedCell.call( oSettings.oInstance,
						nTd, _fnGetCellData( oSettings, iRow, i ), rowData, iRow, i
					);
				}
			}
	
			_fnCallbackFire( oSettings, 'aoRowCreatedCallback', null, [nTr, rowData, iRow, cells] );
		}
	
		// Remove once webkit bug 131819 and Chromium bug 365619 have been resolved
		// and deployed
		row.nTr.setAttribute( 'role', 'row' );
	}
	
	
	/**
	 * Add attributes to a row based on the special `DT_*` parameters in a data
	 * source object.
	 *  @param {object} settings DataTables settings object
	 *  @param {object} DataTables row object for the row to be modified
	 *  @memberof DataTable#oApi
	 */
	function _fnRowAttributes( settings, row )
	{
		var tr = row.nTr;
		var data = row._aData;
	
		if ( tr ) {
			var id = settings.rowIdFn( data );
	
			if ( id ) {
				tr.id = id;
			}
	
			if ( data.DT_RowClass ) {
				// Remove any classes added by DT_RowClass before
				var a = data.DT_RowClass.split(' ');
				row.__rowc = row.__rowc ?
					_unique( row.__rowc.concat( a ) ) :
					a;
	
				$(tr)
					.removeClass( row.__rowc.join(' ') )
					.addClass( data.DT_RowClass );
			}
	
			if ( data.DT_RowAttr ) {
				$(tr).attr( data.DT_RowAttr );
			}
	
			if ( data.DT_RowData ) {
				$(tr).data( data.DT_RowData );
			}
		}
	}
	
	
	/**
	 * Create the HTML header for the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnBuildHead( oSettings )
	{
		var i, ien, cell, row, column;
		var thead = oSettings.nTHead;
		var tfoot = oSettings.nTFoot;
		var createHeader = $('th, td', thead).length === 0;
		var classes = oSettings.oClasses;
		var columns = oSettings.aoColumns;
	
		if ( createHeader ) {
			row = $('<tr/>').appendTo( thead );
		}
	
		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			column = columns[i];
			cell = $( column.nTh ).addClass( column.sClass );
	
			if ( createHeader ) {
				cell.appendTo( row );
			}
	
			// 1.11 move into sorting
			if ( oSettings.oFeatures.bSort ) {
				cell.addClass( column.sSortingClass );
	
				if ( column.bSortable !== false ) {
					cell
						.attr( 'tabindex', oSettings.iTabIndex )
						.attr( 'aria-controls', oSettings.sTableId );
	
					_fnSortAttachListener( oSettings, column.nTh, i );
				}
			}
	
			if ( column.sTitle != cell[0].innerHTML ) {
				cell.html( column.sTitle );
			}
	
			_fnRenderer( oSettings, 'header' )(
				oSettings, cell, column, classes
			);
		}
	
		if ( createHeader ) {
			_fnDetectHeader( oSettings.aoHeader, thead );
		}
		
		/* ARIA role for the rows */
	 	$(thead).find('>tr').attr('role', 'row');
	
		/* Deal with the footer - add classes if required */
		$(thead).find('>tr>th, >tr>td').addClass( classes.sHeaderTH );
		$(tfoot).find('>tr>th, >tr>td').addClass( classes.sFooterTH );
	
		// Cache the footer cells. Note that we only take the cells from the first
		// row in the footer. If there is more than one row the user wants to
		// interact with, they need to use the table().foot() method. Note also this
		// allows cells to be used for multiple columns using colspan
		if ( tfoot !== null ) {
			var cells = oSettings.aoFooter[0];
	
			for ( i=0, ien=cells.length ; i<ien ; i++ ) {
				column = columns[i];
				column.nTf = cells[i].cell;
	
				if ( column.sClass ) {
					$(column.nTf).addClass( column.sClass );
				}
			}
		}
	}
	
	
	/**
	 * Draw the header (or footer) element based on the column visibility states. The
	 * methodology here is to use the layout array from _fnDetectHeader, modified for
	 * the instantaneous column visibility, to construct the new layout. The grid is
	 * traversed over cell at a time in a rows x columns grid fashion, although each
	 * cell insert can cover multiple elements in the grid - which is tracks using the
	 * aApplied array. Cell inserts in the grid will only occur where there isn't
	 * already a cell in that position.
	 *  @param {object} oSettings dataTables settings object
	 *  @param array {objects} aoSource Layout array from _fnDetectHeader
	 *  @param {boolean} [bIncludeHidden=false] If true then include the hidden columns in the calc,
	 *  @memberof DataTable#oApi
	 */
	function _fnDrawHead( oSettings, aoSource, bIncludeHidden )
	{
		var i, iLen, j, jLen, k, kLen, n, nLocalTr;
		var aoLocal = [];
		var aApplied = [];
		var iColumns = oSettings.aoColumns.length;
		var iRowspan, iColspan;
	
		if ( ! aoSource )
		{
			return;
		}
	
		if (  bIncludeHidden === undefined )
		{
			bIncludeHidden = false;
		}
	
		/* Make a copy of the master layout array, but without the visible columns in it */
		for ( i=0, iLen=aoSource.length ; i<iLen ; i++ )
		{
			aoLocal[i] = aoSource[i].slice();
			aoLocal[i].nTr = aoSource[i].nTr;
	
			/* Remove any columns which are currently hidden */
			for ( j=iColumns-1 ; j>=0 ; j-- )
			{
				if ( !oSettings.aoColumns[j].bVisible && !bIncludeHidden )
				{
					aoLocal[i].splice( j, 1 );
				}
			}
	
			/* Prep the applied array - it needs an element for each row */
			aApplied.push( [] );
		}
	
		for ( i=0, iLen=aoLocal.length ; i<iLen ; i++ )
		{
			nLocalTr = aoLocal[i].nTr;
	
			/* All cells are going to be replaced, so empty out the row */
			if ( nLocalTr )
			{
				while( (n = nLocalTr.firstChild) )
				{
					nLocalTr.removeChild( n );
				}
			}
	
			for ( j=0, jLen=aoLocal[i].length ; j<jLen ; j++ )
			{
				iRowspan = 1;
				iColspan = 1;
	
				/* Check to see if there is already a cell (row/colspan) covering our target
				 * insert point. If there is, then there is nothing to do.
				 */
				if ( aApplied[i][j] === undefined )
				{
					nLocalTr.appendChild( aoLocal[i][j].cell );
					aApplied[i][j] = 1;
	
					/* Expand the cell to cover as many rows as needed */
					while ( aoLocal[i+iRowspan] !== undefined &&
					        aoLocal[i][j].cell == aoLocal[i+iRowspan][j].cell )
					{
						aApplied[i+iRowspan][j] = 1;
						iRowspan++;
					}
	
					/* Expand the cell to cover as many columns as needed */
					while ( aoLocal[i][j+iColspan] !== undefined &&
					        aoLocal[i][j].cell == aoLocal[i][j+iColspan].cell )
					{
						/* Must update the applied array over the rows for the columns */
						for ( k=0 ; k<iRowspan ; k++ )
						{
							aApplied[i+k][j+iColspan] = 1;
						}
						iColspan++;
					}
	
					/* Do the actual expansion in the DOM */
					$(aoLocal[i][j].cell)
						.attr('rowspan', iRowspan)
						.attr('colspan', iColspan);
				}
			}
		}
	}
	
	
	/**
	 * Insert the required TR nodes into the table for display
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnDraw( oSettings )
	{
		/* Provide a pre-callback function which can be used to cancel the draw is false is returned */
		var aPreDraw = _fnCallbackFire( oSettings, 'aoPreDrawCallback', 'preDraw', [oSettings] );
		if ( $.inArray( false, aPreDraw ) !== -1 )
		{
			_fnProcessingDisplay( oSettings, false );
			return;
		}
	
		var i, iLen, n;
		var anRows = [];
		var iRowCount = 0;
		var asStripeClasses = oSettings.asStripeClasses;
		var iStripes = asStripeClasses.length;
		var iOpenRows = oSettings.aoOpenRows.length;
		var oLang = oSettings.oLanguage;
		var iInitDisplayStart = oSettings.iInitDisplayStart;
		var bServerSide = _fnDataSource( oSettings ) == 'ssp';
		var aiDisplay = oSettings.aiDisplay;
	
		oSettings.bDrawing = true;
	
		/* Check and see if we have an initial draw position from state saving */
		if ( iInitDisplayStart !== undefined && iInitDisplayStart !== -1 )
		{
			oSettings._iDisplayStart = bServerSide ?
				iInitDisplayStart :
				iInitDisplayStart >= oSettings.fnRecordsDisplay() ?
					0 :
					iInitDisplayStart;
	
			oSettings.iInitDisplayStart = -1;
		}
	
		var iDisplayStart = oSettings._iDisplayStart;
		var iDisplayEnd = oSettings.fnDisplayEnd();
	
		/* Server-side processing draw intercept */
		if ( oSettings.bDeferLoading )
		{
			oSettings.bDeferLoading = false;
			oSettings.iDraw++;
			_fnProcessingDisplay( oSettings, false );
		}
		else if ( !bServerSide )
		{
			oSettings.iDraw++;
		}
		else if ( !oSettings.bDestroying && !_fnAjaxUpdate( oSettings ) )
		{
			return;
		}
	
		if ( aiDisplay.length !== 0 )
		{
			var iStart = bServerSide ? 0 : iDisplayStart;
			var iEnd = bServerSide ? oSettings.aoData.length : iDisplayEnd;
	
			for ( var j=iStart ; j<iEnd ; j++ )
			{
				var iDataIndex = aiDisplay[j];
				var aoData = oSettings.aoData[ iDataIndex ];
				if ( aoData.nTr === null )
				{
					_fnCreateTr( oSettings, iDataIndex );
				}
	
				var nRow = aoData.nTr;
	
				/* Remove the old striping classes and then add the new one */
				if ( iStripes !== 0 )
				{
					var sStripe = asStripeClasses[ iRowCount % iStripes ];
					if ( aoData._sRowStripe != sStripe )
					{
						$(nRow).removeClass( aoData._sRowStripe ).addClass( sStripe );
						aoData._sRowStripe = sStripe;
					}
				}
	
				// Row callback functions - might want to manipulate the row
				// iRowCount and j are not currently documented. Are they at all
				// useful?
				_fnCallbackFire( oSettings, 'aoRowCallback', null,
					[nRow, aoData._aData, iRowCount, j, iDataIndex] );
	
				anRows.push( nRow );
				iRowCount++;
			}
		}
		else
		{
			/* Table is empty - create a row with an empty message in it */
			var sZero = oLang.sZeroRecords;
			if ( oSettings.iDraw == 1 &&  _fnDataSource( oSettings ) == 'ajax' )
			{
				sZero = oLang.sLoadingRecords;
			}
			else if ( oLang.sEmptyTable && oSettings.fnRecordsTotal() === 0 )
			{
				sZero = oLang.sEmptyTable;
			}
	
			anRows[ 0 ] = $( '<tr/>', { 'class': iStripes ? asStripeClasses[0] : '' } )
				.append( $('<td />', {
					'valign':  'top',
					'colSpan': _fnVisbleColumns( oSettings ),
					'class':   oSettings.oClasses.sRowEmpty
				} ).html( sZero ) )[0];
		}
	
		/* Header and footer callbacks */
		_fnCallbackFire( oSettings, 'aoHeaderCallback', 'header', [ $(oSettings.nTHead).children('tr')[0],
			_fnGetDataMaster( oSettings ), iDisplayStart, iDisplayEnd, aiDisplay ] );
	
		_fnCallbackFire( oSettings, 'aoFooterCallback', 'footer', [ $(oSettings.nTFoot).children('tr')[0],
			_fnGetDataMaster( oSettings ), iDisplayStart, iDisplayEnd, aiDisplay ] );
	
		var body = $(oSettings.nTBody);
	
		body.children().detach();
		body.append( $(anRows) );
	
		/* Call all required callback functions for the end of a draw */
		_fnCallbackFire( oSettings, 'aoDrawCallback', 'draw', [oSettings] );
	
		/* Draw is complete, sorting and filtering must be as well */
		oSettings.bSorted = false;
		oSettings.bFiltered = false;
		oSettings.bDrawing = false;
	}
	
	
	/**
	 * Redraw the table - taking account of the various features which are enabled
	 *  @param {object} oSettings dataTables settings object
	 *  @param {boolean} [holdPosition] Keep the current paging position. By default
	 *    the paging is reset to the first page
	 *  @memberof DataTable#oApi
	 */
	function _fnReDraw( settings, holdPosition )
	{
		var
			features = settings.oFeatures,
			sort     = features.bSort,
			filter   = features.bFilter;
	
		if ( sort ) {
			_fnSort( settings );
		}
	
		if ( filter ) {
			_fnFilterComplete( settings, settings.oPreviousSearch );
		}
		else {
			// No filtering, so we want to just use the display master
			settings.aiDisplay = settings.aiDisplayMaster.slice();
		}
	
		if ( holdPosition !== true ) {
			settings._iDisplayStart = 0;
		}
	
		// Let any modules know about the draw hold position state (used by
		// scrolling internally)
		settings._drawHold = holdPosition;
	
		_fnDraw( settings );
	
		settings._drawHold = false;
	}
	
	
	/**
	 * Add the options to the page HTML for the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnAddOptionsHtml ( oSettings )
	{
		var classes = oSettings.oClasses;
		var table = $(oSettings.nTable);
		var holding = $('<div/>').insertBefore( table ); // Holding element for speed
		var features = oSettings.oFeatures;
	
		// All DataTables are wrapped in a div
		var insert = $('<div/>', {
			id:      oSettings.sTableId+'_wrapper',
			'class': classes.sWrapper + (oSettings.nTFoot ? '' : ' '+classes.sNoFooter)
		} );
	
		oSettings.nHolding = holding[0];
		oSettings.nTableWrapper = insert[0];
		oSettings.nTableReinsertBefore = oSettings.nTable.nextSibling;
	
		/* Loop over the user set positioning and place the elements as needed */
		var aDom = oSettings.sDom.split('');
		var featureNode, cOption, nNewNode, cNext, sAttr, j;
		for ( var i=0 ; i<aDom.length ; i++ )
		{
			featureNode = null;
			cOption = aDom[i];
	
			if ( cOption == '<' )
			{
				/* New container div */
				nNewNode = $('<div/>')[0];
	
				/* Check to see if we should append an id and/or a class name to the container */
				cNext = aDom[i+1];
				if ( cNext == "'" || cNext == '"' )
				{
					sAttr = "";
					j = 2;
					while ( aDom[i+j] != cNext )
					{
						sAttr += aDom[i+j];
						j++;
					}
	
					/* Replace jQuery UI constants @todo depreciated */
					if ( sAttr == "H" )
					{
						sAttr = classes.sJUIHeader;
					}
					else if ( sAttr == "F" )
					{
						sAttr = classes.sJUIFooter;
					}
	
					/* The attribute can be in the format of "#id.class", "#id" or "class" This logic
					 * breaks the string into parts and applies them as needed
					 */
					if ( sAttr.indexOf('.') != -1 )
					{
						var aSplit = sAttr.split('.');
						nNewNode.id = aSplit[0].substr(1, aSplit[0].length-1);
						nNewNode.className = aSplit[1];
					}
					else if ( sAttr.charAt(0) == "#" )
					{
						nNewNode.id = sAttr.substr(1, sAttr.length-1);
					}
					else
					{
						nNewNode.className = sAttr;
					}
	
					i += j; /* Move along the position array */
				}
	
				insert.append( nNewNode );
				insert = $(nNewNode);
			}
			else if ( cOption == '>' )
			{
				/* End container div */
				insert = insert.parent();
			}
			// @todo Move options into their own plugins?
			else if ( cOption == 'l' && features.bPaginate && features.bLengthChange )
			{
				/* Length */
				featureNode = _fnFeatureHtmlLength( oSettings );
			}
			else if ( cOption == 'f' && features.bFilter )
			{
				/* Filter */
				featureNode = _fnFeatureHtmlFilter( oSettings );
			}
			else if ( cOption == 'r' && features.bProcessing )
			{
				/* pRocessing */
				featureNode = _fnFeatureHtmlProcessing( oSettings );
			}
			else if ( cOption == 't' )
			{
				/* Table */
				featureNode = _fnFeatureHtmlTable( oSettings );
			}
			else if ( cOption ==  'i' && features.bInfo )
			{
				/* Info */
				featureNode = _fnFeatureHtmlInfo( oSettings );
			}
			else if ( cOption == 'p' && features.bPaginate )
			{
				/* Pagination */
				featureNode = _fnFeatureHtmlPaginate( oSettings );
			}
			else if ( DataTable.ext.feature.length !== 0 )
			{
				/* Plug-in features */
				var aoFeatures = DataTable.ext.feature;
				for ( var k=0, kLen=aoFeatures.length ; k<kLen ; k++ )
				{
					if ( cOption == aoFeatures[k].cFeature )
					{
						featureNode = aoFeatures[k].fnInit( oSettings );
						break;
					}
				}
			}
	
			/* Add to the 2D features array */
			if ( featureNode )
			{
				var aanFeatures = oSettings.aanFeatures;
	
				if ( ! aanFeatures[cOption] )
				{
					aanFeatures[cOption] = [];
				}
	
				aanFeatures[cOption].push( featureNode );
				insert.append( featureNode );
			}
		}
	
		/* Built our DOM structure - replace the holding div with what we want */
		holding.replaceWith( insert );
		oSettings.nHolding = null;
	}
	
	
	/**
	 * Use the DOM source to create up an array of header cells. The idea here is to
	 * create a layout grid (array) of rows x columns, which contains a reference
	 * to the cell that that point in the grid (regardless of col/rowspan), such that
	 * any column / row could be removed and the new grid constructed
	 *  @param array {object} aLayout Array to store the calculated layout in
	 *  @param {node} nThead The header/footer element for the table
	 *  @memberof DataTable#oApi
	 */
	function _fnDetectHeader ( aLayout, nThead )
	{
		var nTrs = $(nThead).children('tr');
		var nTr, nCell;
		var i, k, l, iLen, jLen, iColShifted, iColumn, iColspan, iRowspan;
		var bUnique;
		var fnShiftCol = function ( a, i, j ) {
			var k = a[i];
	                while ( k[j] ) {
				j++;
			}
			return j;
		};
	
		aLayout.splice( 0, aLayout.length );
	
		/* We know how many rows there are in the layout - so prep it */
		for ( i=0, iLen=nTrs.length ; i<iLen ; i++ )
		{
			aLayout.push( [] );
		}
	
		/* Calculate a layout array */
		for ( i=0, iLen=nTrs.length ; i<iLen ; i++ )
		{
			nTr = nTrs[i];
			iColumn = 0;
	
			/* For every cell in the row... */
			nCell = nTr.firstChild;
			while ( nCell ) {
				if ( nCell.nodeName.toUpperCase() == "TD" ||
				     nCell.nodeName.toUpperCase() == "TH" )
				{
					/* Get the col and rowspan attributes from the DOM and sanitise them */
					iColspan = nCell.getAttribute('colspan') * 1;
					iRowspan = nCell.getAttribute('rowspan') * 1;
					iColspan = (!iColspan || iColspan===0 || iColspan===1) ? 1 : iColspan;
					iRowspan = (!iRowspan || iRowspan===0 || iRowspan===1) ? 1 : iRowspan;
	
					/* There might be colspan cells already in this row, so shift our target
					 * accordingly
					 */
					iColShifted = fnShiftCol( aLayout, i, iColumn );
	
					/* Cache calculation for unique columns */
					bUnique = iColspan === 1 ? true : false;
	
					/* If there is col / rowspan, copy the information into the layout grid */
					for ( l=0 ; l<iColspan ; l++ )
					{
						for ( k=0 ; k<iRowspan ; k++ )
						{
							aLayout[i+k][iColShifted+l] = {
								"cell": nCell,
								"unique": bUnique
							};
							aLayout[i+k].nTr = nTr;
						}
					}
				}
				nCell = nCell.nextSibling;
			}
		}
	}
	
	
	/**
	 * Get an array of unique th elements, one for each column
	 *  @param {object} oSettings dataTables settings object
	 *  @param {node} nHeader automatically detect the layout from this node - optional
	 *  @param {array} aLayout thead/tfoot layout from _fnDetectHeader - optional
	 *  @returns array {node} aReturn list of unique th's
	 *  @memberof DataTable#oApi
	 */
	function _fnGetUniqueThs ( oSettings, nHeader, aLayout )
	{
		var aReturn = [];
		if ( !aLayout )
		{
			aLayout = oSettings.aoHeader;
			if ( nHeader )
			{
				aLayout = [];
				_fnDetectHeader( aLayout, nHeader );
			}
		}
	
		for ( var i=0, iLen=aLayout.length ; i<iLen ; i++ )
		{
			for ( var j=0, jLen=aLayout[i].length ; j<jLen ; j++ )
			{
				if ( aLayout[i][j].unique &&
					 (!aReturn[j] || !oSettings.bSortCellsTop) )
				{
					aReturn[j] = aLayout[i][j].cell;
				}
			}
		}
	
		return aReturn;
	}
	
	/**
	 * Create an Ajax call based on the table's settings, taking into account that
	 * parameters can have multiple forms, and backwards compatibility.
	 *
	 * @param {object} oSettings dataTables settings object
	 * @param {array} data Data to send to the server, required by
	 *     DataTables - may be augmented by developer callbacks
	 * @param {function} fn Callback function to run when data is obtained
	 */
	function _fnBuildAjax( oSettings, data, fn )
	{
		// Compatibility with 1.9-, allow fnServerData and event to manipulate
		_fnCallbackFire( oSettings, 'aoServerParams', 'serverParams', [data] );
	
		// Convert to object based for 1.10+ if using the old array scheme which can
		// come from server-side processing or serverParams
		if ( data && $.isArray(data) ) {
			var tmp = {};
			var rbracket = /(.*?)\[\]$/;
	
			$.each( data, function (key, val) {
				var match = val.name.match(rbracket);
	
				if ( match ) {
					// Support for arrays
					var name = match[0];
	
					if ( ! tmp[ name ] ) {
						tmp[ name ] = [];
					}
					tmp[ name ].push( val.value );
				}
				else {
					tmp[val.name] = val.value;
				}
			} );
			data = tmp;
		}
	
		var ajaxData;
		var ajax = oSettings.ajax;
		var instance = oSettings.oInstance;
		var callback = function ( json ) {
			_fnCallbackFire( oSettings, null, 'xhr', [oSettings, json, oSettings.jqXHR] );
			fn( json );
		};
	
		if ( $.isPlainObject( ajax ) && ajax.data )
		{
			ajaxData = ajax.data;
	
			var newData = typeof ajaxData === 'function' ?
				ajaxData( data, oSettings ) :  // fn can manipulate data or return
				ajaxData;                      // an object object or array to merge
	
			// If the function returned something, use that alone
			data = typeof ajaxData === 'function' && newData ?
				newData :
				$.extend( true, data, newData );
	
			// Remove the data property as we've resolved it already and don't want
			// jQuery to do it again (it is restored at the end of the function)
			delete ajax.data;
		}
	
		var baseAjax = {
			"data": data,
			"success": function (json) {
				var error = json.error || json.sError;
				if ( error ) {
					_fnLog( oSettings, 0, error );
				}
	
				oSettings.json = json;
				callback( json );
			},
			"dataType": "json",
			"cache": false,
			"type": oSettings.sServerMethod,
			"error": function (xhr, error, thrown) {
				var ret = _fnCallbackFire( oSettings, null, 'xhr', [oSettings, null, oSettings.jqXHR] );
	
				if ( $.inArray( true, ret ) === -1 ) {
					if ( error == "parsererror" ) {
						_fnLog( oSettings, 0, 'Invalid JSON response', 1 );
					}
					else if ( xhr.readyState === 4 ) {
						_fnLog( oSettings, 0, 'Ajax error', 7 );
					}
				}
	
				_fnProcessingDisplay( oSettings, false );
			}
		};
	
		// Store the data submitted for the API
		oSettings.oAjaxData = data;
	
		// Allow plug-ins and external processes to modify the data
		_fnCallbackFire( oSettings, null, 'preXhr', [oSettings, data] );
	
		if ( oSettings.fnServerData )
		{
			// DataTables 1.9- compatibility
			oSettings.fnServerData.call( instance,
				oSettings.sAjaxSource,
				$.map( data, function (val, key) { // Need to convert back to 1.9 trad format
					return { name: key, value: val };
				} ),
				callback,
				oSettings
			);
		}
		else if ( oSettings.sAjaxSource || typeof ajax === 'string' )
		{
			// DataTables 1.9- compatibility
			oSettings.jqXHR = $.ajax( $.extend( baseAjax, {
				url: ajax || oSettings.sAjaxSource
			} ) );
		}
		else if ( typeof ajax === 'function' )
		{
			// Is a function - let the caller define what needs to be done
			oSettings.jqXHR = ajax.call( instance, data, callback, oSettings );
		}
		else
		{
			// Object to extend the base settings
			oSettings.jqXHR = $.ajax( $.extend( baseAjax, ajax ) );
	
			// Restore for next time around
			ajax.data = ajaxData;
		}
	}
	
	
	/**
	 * Update the table using an Ajax call
	 *  @param {object} settings dataTables settings object
	 *  @returns {boolean} Block the table drawing or not
	 *  @memberof DataTable#oApi
	 */
	function _fnAjaxUpdate( settings )
	{
		if ( settings.bAjaxDataGet ) {
			settings.iDraw++;
			_fnProcessingDisplay( settings, true );
	
			_fnBuildAjax(
				settings,
				_fnAjaxParameters( settings ),
				function(json) {
					_fnAjaxUpdateDraw( settings, json );
				}
			);
	
			return false;
		}
		return true;
	}
	
	
	/**
	 * Build up the parameters in an object needed for a server-side processing
	 * request. Note that this is basically done twice, is different ways - a modern
	 * method which is used by default in DataTables 1.10 which uses objects and
	 * arrays, or the 1.9- method with is name / value pairs. 1.9 method is used if
	 * the sAjaxSource option is used in the initialisation, or the legacyAjax
	 * option is set.
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {bool} block the table drawing or not
	 *  @memberof DataTable#oApi
	 */
	function _fnAjaxParameters( settings )
	{
		var
			columns = settings.aoColumns,
			columnCount = columns.length,
			features = settings.oFeatures,
			preSearch = settings.oPreviousSearch,
			preColSearch = settings.aoPreSearchCols,
			i, data = [], dataProp, column, columnSearch,
			sort = _fnSortFlatten( settings ),
			displayStart = settings._iDisplayStart,
			displayLength = features.bPaginate !== false ?
				settings._iDisplayLength :
				-1;
	
		var param = function ( name, value ) {
			data.push( { 'name': name, 'value': value } );
		};
	
		// DataTables 1.9- compatible method
		param( 'sEcho',          settings.iDraw );
		param( 'iColumns',       columnCount );
		param( 'sColumns',       _pluck( columns, 'sName' ).join(',') );
		param( 'iDisplayStart',  displayStart );
		param( 'iDisplayLength', displayLength );
	
		// DataTables 1.10+ method
		var d = {
			draw:    settings.iDraw,
			columns: [],
			order:   [],
			start:   displayStart,
			length:  displayLength,
			search:  {
				value: preSearch.sSearch,
				regex: preSearch.bRegex
			}
		};
	
		for ( i=0 ; i<columnCount ; i++ ) {
			column = columns[i];
			columnSearch = preColSearch[i];
			dataProp = typeof column.mData=="function" ? 'function' : column.mData ;
	
			d.columns.push( {
				data:       dataProp,
				name:       column.sName,
				searchable: column.bSearchable,
				orderable:  column.bSortable,
				search:     {
					value: columnSearch.sSearch,
					regex: columnSearch.bRegex
				}
			} );
	
			param( "mDataProp_"+i, dataProp );
	
			if ( features.bFilter ) {
				param( 'sSearch_'+i,     columnSearch.sSearch );
				param( 'bRegex_'+i,      columnSearch.bRegex );
				param( 'bSearchable_'+i, column.bSearchable );
			}
	
			if ( features.bSort ) {
				param( 'bSortable_'+i, column.bSortable );
			}
		}
	
		if ( features.bFilter ) {
			param( 'sSearch', preSearch.sSearch );
			param( 'bRegex', preSearch.bRegex );
		}
	
		if ( features.bSort ) {
			$.each( sort, function ( i, val ) {
				d.order.push( { column: val.col, dir: val.dir } );
	
				param( 'iSortCol_'+i, val.col );
				param( 'sSortDir_'+i, val.dir );
			} );
	
			param( 'iSortingCols', sort.length );
		}
	
		// If the legacy.ajax parameter is null, then we automatically decide which
		// form to use, based on sAjaxSource
		var legacy = DataTable.ext.legacy.ajax;
		if ( legacy === null ) {
			return settings.sAjaxSource ? data : d;
		}
	
		// Otherwise, if legacy has been specified then we use that to decide on the
		// form
		return legacy ? data : d;
	}
	
	
	/**
	 * Data the data from the server (nuking the old) and redraw the table
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} json json data return from the server.
	 *  @param {string} json.sEcho Tracking flag for DataTables to match requests
	 *  @param {int} json.iTotalRecords Number of records in the data set, not accounting for filtering
	 *  @param {int} json.iTotalDisplayRecords Number of records in the data set, accounting for filtering
	 *  @param {array} json.aaData The data to display on this page
	 *  @param {string} [json.sColumns] Column ordering (sName, comma separated)
	 *  @memberof DataTable#oApi
	 */
	function _fnAjaxUpdateDraw ( settings, json )
	{
		// v1.10 uses camelCase variables, while 1.9 uses Hungarian notation.
		// Support both
		var compat = function ( old, modern ) {
			return json[old] !== undefined ? json[old] : json[modern];
		};
	
		var data = _fnAjaxDataSrc( settings, json );
		var draw            = compat( 'sEcho',                'draw' );
		var recordsTotal    = compat( 'iTotalRecords',        'recordsTotal' );
		var recordsFiltered = compat( 'iTotalDisplayRecords', 'recordsFiltered' );
	
		if ( draw ) {
			// Protect against out of sequence returns
			if ( draw*1 < settings.iDraw ) {
				return;
			}
			settings.iDraw = draw * 1;
		}
	
		_fnClearTable( settings );
		settings._iRecordsTotal   = parseInt(recordsTotal, 10);
		settings._iRecordsDisplay = parseInt(recordsFiltered, 10);
	
		for ( var i=0, ien=data.length ; i<ien ; i++ ) {
			_fnAddData( settings, data[i] );
		}
		settings.aiDisplay = settings.aiDisplayMaster.slice();
	
		settings.bAjaxDataGet = false;
		_fnDraw( settings );
	
		if ( ! settings._bInitComplete ) {
			_fnInitComplete( settings, json );
		}
	
		settings.bAjaxDataGet = true;
		_fnProcessingDisplay( settings, false );
	}
	
	
	/**
	 * Get the data from the JSON data source to use for drawing a table. Using
	 * `_fnGetObjectDataFn` allows the data to be sourced from a property of the
	 * source object, or from a processing function.
	 *  @param {object} oSettings dataTables settings object
	 *  @param  {object} json Data source object / array from the server
	 *  @return {array} Array of data to use
	 */
	function _fnAjaxDataSrc ( oSettings, json )
	{
		var dataSrc = $.isPlainObject( oSettings.ajax ) && oSettings.ajax.dataSrc !== undefined ?
			oSettings.ajax.dataSrc :
			oSettings.sAjaxDataProp; // Compatibility with 1.9-.
	
		// Compatibility with 1.9-. In order to read from aaData, check if the
		// default has been changed, if not, check for aaData
		if ( dataSrc === 'data' ) {
			return json.aaData || json[dataSrc];
		}
	
		return dataSrc !== "" ?
			_fnGetObjectDataFn( dataSrc )( json ) :
			json;
	}
	
	/**
	 * Generate the node required for filtering text
	 *  @returns {node} Filter control element
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlFilter ( settings )
	{
		var classes = settings.oClasses;
		var tableId = settings.sTableId;
		var language = settings.oLanguage;
		var previousSearch = settings.oPreviousSearch;
		var features = settings.aanFeatures;
		var input = '<input type="search" class="'+classes.sFilterInput+'"/>';
	
		var str = language.sSearch;
		str = str.match(/_INPUT_/) ?
			str.replace('_INPUT_', input) :
			str+input;
	
		var filter = $('<div/>', {
				'id': ! features.f ? tableId+'_filter' : null,
				'class': classes.sFilter
			} )
			.append( $('<label/>' ).append( str ) );
	
		var searchFn = function() {
			/* Update all other filter input elements for the new display */
			var n = features.f;
			var val = !this.value ? "" : this.value; // mental IE8 fix :-(
	
			/* Now do the filter */
			if ( val != previousSearch.sSearch ) {
				_fnFilterComplete( settings, {
					"sSearch": val,
					"bRegex": previousSearch.bRegex,
					"bSmart": previousSearch.bSmart ,
					"bCaseInsensitive": previousSearch.bCaseInsensitive
				} );
	
				// Need to redraw, without resorting
				settings._iDisplayStart = 0;
				_fnDraw( settings );
			}
		};
	
		var searchDelay = settings.searchDelay !== null ?
			settings.searchDelay :
			_fnDataSource( settings ) === 'ssp' ?
				400 :
				0;
	
		var jqFilter = $('input', filter)
			.val( previousSearch.sSearch )
			.attr( 'placeholder', language.sSearchPlaceholder )
			.on(
				'keyup.DT search.DT input.DT paste.DT cut.DT',
				searchDelay ?
					_fnThrottle( searchFn, searchDelay ) :
					searchFn
			)
			.on( 'keypress.DT', function(e) {
				/* Prevent form submission */
				if ( e.keyCode == 13 ) {
					return false;
				}
			} )
			.attr('aria-controls', tableId);
	
		// Update the input elements whenever the table is filtered
		$(settings.nTable).on( 'search.dt.DT', function ( ev, s ) {
			if ( settings === s ) {
				// IE9 throws an 'unknown error' if document.activeElement is used
				// inside an iframe or frame...
				try {
					if ( jqFilter[0] !== document.activeElement ) {
						jqFilter.val( previousSearch.sSearch );
					}
				}
				catch ( e ) {}
			}
		} );
	
		return filter[0];
	}
	
	
	/**
	 * Filter the table using both the global filter and column based filtering
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} oSearch search information
	 *  @param {int} [iForce] force a research of the master array (1) or not (undefined or 0)
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterComplete ( oSettings, oInput, iForce )
	{
		var oPrevSearch = oSettings.oPreviousSearch;
		var aoPrevSearch = oSettings.aoPreSearchCols;
		var fnSaveFilter = function ( oFilter ) {
			/* Save the filtering values */
			oPrevSearch.sSearch = oFilter.sSearch;
			oPrevSearch.bRegex = oFilter.bRegex;
			oPrevSearch.bSmart = oFilter.bSmart;
			oPrevSearch.bCaseInsensitive = oFilter.bCaseInsensitive;
		};
		var fnRegex = function ( o ) {
			// Backwards compatibility with the bEscapeRegex option
			return o.bEscapeRegex !== undefined ? !o.bEscapeRegex : o.bRegex;
		};
	
		// Resolve any column types that are unknown due to addition or invalidation
		// @todo As per sort - can this be moved into an event handler?
		_fnColumnTypes( oSettings );
	
		/* In server-side processing all filtering is done by the server, so no point hanging around here */
		if ( _fnDataSource( oSettings ) != 'ssp' )
		{
			/* Global filter */
			_fnFilter( oSettings, oInput.sSearch, iForce, fnRegex(oInput), oInput.bSmart, oInput.bCaseInsensitive );
			fnSaveFilter( oInput );
	
			/* Now do the individual column filter */
			for ( var i=0 ; i<aoPrevSearch.length ; i++ )
			{
				_fnFilterColumn( oSettings, aoPrevSearch[i].sSearch, i, fnRegex(aoPrevSearch[i]),
					aoPrevSearch[i].bSmart, aoPrevSearch[i].bCaseInsensitive );
			}
	
			/* Custom filtering */
			_fnFilterCustom( oSettings );
		}
		else
		{
			fnSaveFilter( oInput );
		}
	
		/* Tell the draw function we have been filtering */
		oSettings.bFiltered = true;
		_fnCallbackFire( oSettings, null, 'search', [oSettings] );
	}
	
	
	/**
	 * Apply custom filtering functions
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterCustom( settings )
	{
		var filters = DataTable.ext.search;
		var displayRows = settings.aiDisplay;
		var row, rowIdx;
	
		for ( var i=0, ien=filters.length ; i<ien ; i++ ) {
			var rows = [];
	
			// Loop over each row and see if it should be included
			for ( var j=0, jen=displayRows.length ; j<jen ; j++ ) {
				rowIdx = displayRows[ j ];
				row = settings.aoData[ rowIdx ];
	
				if ( filters[i]( settings, row._aFilterData, rowIdx, row._aData, j ) ) {
					rows.push( rowIdx );
				}
			}
	
			// So the array reference doesn't break set the results into the
			// existing array
			displayRows.length = 0;
			$.merge( displayRows, rows );
		}
	}
	
	
	/**
	 * Filter the table on a per-column basis
	 *  @param {object} oSettings dataTables settings object
	 *  @param {string} sInput string to filter on
	 *  @param {int} iColumn column to filter
	 *  @param {bool} bRegex treat search string as a regular expression or not
	 *  @param {bool} bSmart use smart filtering or not
	 *  @param {bool} bCaseInsensitive Do case insenstive matching or not
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterColumn ( settings, searchStr, colIdx, regex, smart, caseInsensitive )
	{
		if ( searchStr === '' ) {
			return;
		}
	
		var data;
		var out = [];
		var display = settings.aiDisplay;
		var rpSearch = _fnFilterCreateSearch( searchStr, regex, smart, caseInsensitive );
	
		for ( var i=0 ; i<display.length ; i++ ) {
			data = settings.aoData[ display[i] ]._aFilterData[ colIdx ];
	
			if ( rpSearch.test( data ) ) {
				out.push( display[i] );
			}
		}
	
		settings.aiDisplay = out;
	}
	
	
	/**
	 * Filter the data table based on user input and draw the table
	 *  @param {object} settings dataTables settings object
	 *  @param {string} input string to filter on
	 *  @param {int} force optional - force a research of the master array (1) or not (undefined or 0)
	 *  @param {bool} regex treat as a regular expression or not
	 *  @param {bool} smart perform smart filtering or not
	 *  @param {bool} caseInsensitive Do case insenstive matching or not
	 *  @memberof DataTable#oApi
	 */
	function _fnFilter( settings, input, force, regex, smart, caseInsensitive )
	{
		var rpSearch = _fnFilterCreateSearch( input, regex, smart, caseInsensitive );
		var prevSearch = settings.oPreviousSearch.sSearch;
		var displayMaster = settings.aiDisplayMaster;
		var display, invalidated, i;
		var filtered = [];
	
		// Need to take account of custom filtering functions - always filter
		if ( DataTable.ext.search.length !== 0 ) {
			force = true;
		}
	
		// Check if any of the rows were invalidated
		invalidated = _fnFilterData( settings );
	
		// If the input is blank - we just want the full data set
		if ( input.length <= 0 ) {
			settings.aiDisplay = displayMaster.slice();
		}
		else {
			// New search - start from the master array
			if ( invalidated ||
				 force ||
				 prevSearch.length > input.length ||
				 input.indexOf(prevSearch) !== 0 ||
				 settings.bSorted // On resort, the display master needs to be
				                  // re-filtered since indexes will have changed
			) {
				settings.aiDisplay = displayMaster.slice();
			}
	
			// Search the display array
			display = settings.aiDisplay;
	
			for ( i=0 ; i<display.length ; i++ ) {
				if ( rpSearch.test( settings.aoData[ display[i] ]._sFilterRow ) ) {
					filtered.push( display[i] );
				}
			}
	
			settings.aiDisplay = filtered;
		}
	}
	
	
	/**
	 * Build a regular expression object suitable for searching a table
	 *  @param {string} sSearch string to search for
	 *  @param {bool} bRegex treat as a regular expression or not
	 *  @param {bool} bSmart perform smart filtering or not
	 *  @param {bool} bCaseInsensitive Do case insensitive matching or not
	 *  @returns {RegExp} constructed object
	 *  @memberof DataTable#oApi
	 */
	function _fnFilterCreateSearch( search, regex, smart, caseInsensitive )
	{
		search = regex ?
			search :
			_fnEscapeRegex( search );
		
		if ( smart ) {
			/* For smart filtering we want to allow the search to work regardless of
			 * word order. We also want double quoted text to be preserved, so word
			 * order is important - a la google. So this is what we want to
			 * generate:
			 * 
			 * ^(?=.*?\bone\b)(?=.*?\btwo three\b)(?=.*?\bfour\b).*$
			 */
			var a = $.map( search.match( /"[^"]+"|[^ ]+/g ) || [''], function ( word ) {
				if ( word.charAt(0) === '"' ) {
					var m = word.match( /^"(.*)"$/ );
					word = m ? m[1] : word;
				}
	
				return word.replace('"', '');
			} );
	
			search = '^(?=.*?'+a.join( ')(?=.*?' )+').*$';
		}
	
		return new RegExp( search, caseInsensitive ? 'i' : '' );
	}
	
	
	/**
	 * Escape a string such that it can be used in a regular expression
	 *  @param {string} sVal string to escape
	 *  @returns {string} escaped string
	 *  @memberof DataTable#oApi
	 */
	var _fnEscapeRegex = DataTable.util.escapeRegex;
	
	var __filter_div = $('<div>')[0];
	var __filter_div_textContent = __filter_div.textContent !== undefined;
	
	// Update the filtering data for each row if needed (by invalidation or first run)
	function _fnFilterData ( settings )
	{
		var columns = settings.aoColumns;
		var column;
		var i, j, ien, jen, filterData, cellData, row;
		var fomatters = DataTable.ext.type.search;
		var wasInvalidated = false;
	
		for ( i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
			row = settings.aoData[i];
	
			if ( ! row._aFilterData ) {
				filterData = [];
	
				for ( j=0, jen=columns.length ; j<jen ; j++ ) {
					column = columns[j];
	
					if ( column.bSearchable ) {
						cellData = _fnGetCellData( settings, i, j, 'filter' );
	
						if ( fomatters[ column.sType ] ) {
							cellData = fomatters[ column.sType ]( cellData );
						}
	
						// Search in DataTables 1.10 is string based. In 1.11 this
						// should be altered to also allow strict type checking.
						if ( cellData === null ) {
							cellData = '';
						}
	
						if ( typeof cellData !== 'string' && cellData.toString ) {
							cellData = cellData.toString();
						}
					}
					else {
						cellData = '';
					}
	
					// If it looks like there is an HTML entity in the string,
					// attempt to decode it so sorting works as expected. Note that
					// we could use a single line of jQuery to do this, but the DOM
					// method used here is much faster http://jsperf.com/html-decode
					if ( cellData.indexOf && cellData.indexOf('&') !== -1 ) {
						__filter_div.innerHTML = cellData;
						cellData = __filter_div_textContent ?
							__filter_div.textContent :
							__filter_div.innerText;
					}
	
					if ( cellData.replace ) {
						cellData = cellData.replace(/[\r\n]/g, '');
					}
	
					filterData.push( cellData );
				}
	
				row._aFilterData = filterData;
				row._sFilterRow = filterData.join('  ');
				wasInvalidated = true;
			}
		}
	
		return wasInvalidated;
	}
	
	
	/**
	 * Convert from the internal Hungarian notation to camelCase for external
	 * interaction
	 *  @param {object} obj Object to convert
	 *  @returns {object} Inverted object
	 *  @memberof DataTable#oApi
	 */
	function _fnSearchToCamel ( obj )
	{
		return {
			search:          obj.sSearch,
			smart:           obj.bSmart,
			regex:           obj.bRegex,
			caseInsensitive: obj.bCaseInsensitive
		};
	}
	
	
	
	/**
	 * Convert from camelCase notation to the internal Hungarian. We could use the
	 * Hungarian convert function here, but this is cleaner
	 *  @param {object} obj Object to convert
	 *  @returns {object} Inverted object
	 *  @memberof DataTable#oApi
	 */
	function _fnSearchToHung ( obj )
	{
		return {
			sSearch:          obj.search,
			bSmart:           obj.smart,
			bRegex:           obj.regex,
			bCaseInsensitive: obj.caseInsensitive
		};
	}
	
	/**
	 * Generate the node required for the info display
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {node} Information element
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlInfo ( settings )
	{
		var
			tid = settings.sTableId,
			nodes = settings.aanFeatures.i,
			n = $('<div/>', {
				'class': settings.oClasses.sInfo,
				'id': ! nodes ? tid+'_info' : null
			} );
	
		if ( ! nodes ) {
			// Update display on each draw
			settings.aoDrawCallback.push( {
				"fn": _fnUpdateInfo,
				"sName": "information"
			} );
	
			n
				.attr( 'role', 'status' )
				.attr( 'aria-live', 'polite' );
	
			// Table is described by our info div
			$(settings.nTable).attr( 'aria-describedby', tid+'_info' );
		}
	
		return n[0];
	}
	
	
	/**
	 * Update the information elements in the display
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnUpdateInfo ( settings )
	{
		/* Show information about the table */
		var nodes = settings.aanFeatures.i;
		if ( nodes.length === 0 ) {
			return;
		}
	
		var
			lang  = settings.oLanguage,
			start = settings._iDisplayStart+1,
			end   = settings.fnDisplayEnd(),
			max   = settings.fnRecordsTotal(),
			total = settings.fnRecordsDisplay(),
			out   = total ?
				lang.sInfo :
				lang.sInfoEmpty;
	
		if ( total !== max ) {
			/* Record set after filtering */
			out += ' ' + lang.sInfoFiltered;
		}
	
		// Convert the macros
		out += lang.sInfoPostFix;
		out = _fnInfoMacros( settings, out );
	
		var callback = lang.fnInfoCallback;
		if ( callback !== null ) {
			out = callback.call( settings.oInstance,
				settings, start, end, max, total, out
			);
		}
	
		$(nodes).html( out );
	}
	
	
	function _fnInfoMacros ( settings, str )
	{
		// When infinite scrolling, we are always starting at 1. _iDisplayStart is used only
		// internally
		var
			formatter  = settings.fnFormatNumber,
			start      = settings._iDisplayStart+1,
			len        = settings._iDisplayLength,
			vis        = settings.fnRecordsDisplay(),
			all        = len === -1;
	
		return str.
			replace(/_START_/g, formatter.call( settings, start ) ).
			replace(/_END_/g,   formatter.call( settings, settings.fnDisplayEnd() ) ).
			replace(/_MAX_/g,   formatter.call( settings, settings.fnRecordsTotal() ) ).
			replace(/_TOTAL_/g, formatter.call( settings, vis ) ).
			replace(/_PAGE_/g,  formatter.call( settings, all ? 1 : Math.ceil( start / len ) ) ).
			replace(/_PAGES_/g, formatter.call( settings, all ? 1 : Math.ceil( vis / len ) ) );
	}
	
	
	
	/**
	 * Draw the table for the first time, adding all required features
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnInitialise ( settings )
	{
		var i, iLen, iAjaxStart=settings.iInitDisplayStart;
		var columns = settings.aoColumns, column;
		var features = settings.oFeatures;
		var deferLoading = settings.bDeferLoading; // value modified by the draw
	
		/* Ensure that the table data is fully initialised */
		if ( ! settings.bInitialised ) {
			setTimeout( function(){ _fnInitialise( settings ); }, 200 );
			return;
		}
	
		/* Show the display HTML options */
		_fnAddOptionsHtml( settings );
	
		/* Build and draw the header / footer for the table */
		_fnBuildHead( settings );
		_fnDrawHead( settings, settings.aoHeader );
		_fnDrawHead( settings, settings.aoFooter );
	
		/* Okay to show that something is going on now */
		_fnProcessingDisplay( settings, true );
	
		/* Calculate sizes for columns */
		if ( features.bAutoWidth ) {
			_fnCalculateColumnWidths( settings );
		}
	
		for ( i=0, iLen=columns.length ; i<iLen ; i++ ) {
			column = columns[i];
	
			if ( column.sWidth ) {
				column.nTh.style.width = _fnStringToCss( column.sWidth );
			}
		}
	
		_fnCallbackFire( settings, null, 'preInit', [settings] );
	
		// If there is default sorting required - let's do it. The sort function
		// will do the drawing for us. Otherwise we draw the table regardless of the
		// Ajax source - this allows the table to look initialised for Ajax sourcing
		// data (show 'loading' message possibly)
		_fnReDraw( settings );
	
		// Server-side processing init complete is done by _fnAjaxUpdateDraw
		var dataSrc = _fnDataSource( settings );
		if ( dataSrc != 'ssp' || deferLoading ) {
			// if there is an ajax source load the data
			if ( dataSrc == 'ajax' ) {
				_fnBuildAjax( settings, [], function(json) {
					var aData = _fnAjaxDataSrc( settings, json );
	
					// Got the data - add it to the table
					for ( i=0 ; i<aData.length ; i++ ) {
						_fnAddData( settings, aData[i] );
					}
	
					// Reset the init display for cookie saving. We've already done
					// a filter, and therefore cleared it before. So we need to make
					// it appear 'fresh'
					settings.iInitDisplayStart = iAjaxStart;
	
					_fnReDraw( settings );
	
					_fnProcessingDisplay( settings, false );
					_fnInitComplete( settings, json );
				}, settings );
			}
			else {
				_fnProcessingDisplay( settings, false );
				_fnInitComplete( settings );
			}
		}
	}
	
	
	/**
	 * Draw the table for the first time, adding all required features
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} [json] JSON from the server that completed the table, if using Ajax source
	 *    with client-side processing (optional)
	 *  @memberof DataTable#oApi
	 */
	function _fnInitComplete ( settings, json )
	{
		settings._bInitComplete = true;
	
		// When data was added after the initialisation (data or Ajax) we need to
		// calculate the column sizing
		if ( json || settings.oInit.aaData ) {
			_fnAdjustColumnSizing( settings );
		}
	
		_fnCallbackFire( settings, null, 'plugin-init', [settings, json] );
		_fnCallbackFire( settings, 'aoInitComplete', 'init', [settings, json] );
	}
	
	
	function _fnLengthChange ( settings, val )
	{
		var len = parseInt( val, 10 );
		settings._iDisplayLength = len;
	
		_fnLengthOverflow( settings );
	
		// Fire length change event
		_fnCallbackFire( settings, null, 'length', [settings, len] );
	}
	
	
	/**
	 * Generate the node required for user display length changing
	 *  @param {object} settings dataTables settings object
	 *  @returns {node} Display length feature node
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlLength ( settings )
	{
		var
			classes  = settings.oClasses,
			tableId  = settings.sTableId,
			menu     = settings.aLengthMenu,
			d2       = $.isArray( menu[0] ),
			lengths  = d2 ? menu[0] : menu,
			language = d2 ? menu[1] : menu;
	
		var select = $('<select/>', {
			'name':          tableId+'_length',
			'aria-controls': tableId,
			'class':         classes.sLengthSelect
		} );
	
		for ( var i=0, ien=lengths.length ; i<ien ; i++ ) {
			select[0][ i ] = new Option(
				typeof language[i] === 'number' ?
					settings.fnFormatNumber( language[i] ) :
					language[i],
				lengths[i]
			);
		}
	
		var div = $('<div><label/></div>').addClass( classes.sLength );
		if ( ! settings.aanFeatures.l ) {
			div[0].id = tableId+'_length';
		}
	
		div.children().append(
			settings.oLanguage.sLengthMenu.replace( '_MENU_', select[0].outerHTML )
		);
	
		// Can't use `select` variable as user might provide their own and the
		// reference is broken by the use of outerHTML
		$('select', div)
			.val( settings._iDisplayLength )
			.on( 'change.DT', function(e) {
				_fnLengthChange( settings, $(this).val() );
				_fnDraw( settings );
			} );
	
		// Update node value whenever anything changes the table's length
		$(settings.nTable).on( 'length.dt.DT', function (e, s, len) {
			if ( settings === s ) {
				$('select', div).val( len );
			}
		} );
	
		return div[0];
	}
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Note that most of the paging logic is done in
	 * DataTable.ext.pager
	 */
	
	/**
	 * Generate the node required for default pagination
	 *  @param {object} oSettings dataTables settings object
	 *  @returns {node} Pagination feature node
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlPaginate ( settings )
	{
		var
			type   = settings.sPaginationType,
			plugin = DataTable.ext.pager[ type ],
			modern = typeof plugin === 'function',
			redraw = function( settings ) {
				_fnDraw( settings );
			},
			node = $('<div/>').addClass( settings.oClasses.sPaging + type )[0],
			features = settings.aanFeatures;
	
		if ( ! modern ) {
			plugin.fnInit( settings, node, redraw );
		}
	
		/* Add a draw callback for the pagination on first instance, to update the paging display */
		if ( ! features.p )
		{
			node.id = settings.sTableId+'_paginate';
	
			settings.aoDrawCallback.push( {
				"fn": function( settings ) {
					if ( modern ) {
						var
							start      = settings._iDisplayStart,
							len        = settings._iDisplayLength,
							visRecords = settings.fnRecordsDisplay(),
							all        = len === -1,
							page = all ? 0 : Math.ceil( start / len ),
							pages = all ? 1 : Math.ceil( visRecords / len ),
							buttons = plugin(page, pages),
							i, ien;
	
						for ( i=0, ien=features.p.length ; i<ien ; i++ ) {
							_fnRenderer( settings, 'pageButton' )(
								settings, features.p[i], i, buttons, page, pages
							);
						}
					}
					else {
						plugin.fnUpdate( settings, redraw );
					}
				},
				"sName": "pagination"
			} );
		}
	
		return node;
	}
	
	
	/**
	 * Alter the display settings to change the page
	 *  @param {object} settings DataTables settings object
	 *  @param {string|int} action Paging action to take: "first", "previous",
	 *    "next" or "last" or page number to jump to (integer)
	 *  @param [bool] redraw Automatically draw the update or not
	 *  @returns {bool} true page has changed, false - no change
	 *  @memberof DataTable#oApi
	 */
	function _fnPageChange ( settings, action, redraw )
	{
		var
			start     = settings._iDisplayStart,
			len       = settings._iDisplayLength,
			records   = settings.fnRecordsDisplay();
	
		if ( records === 0 || len === -1 )
		{
			start = 0;
		}
		else if ( typeof action === "number" )
		{
			start = action * len;
	
			if ( start > records )
			{
				start = 0;
			}
		}
		else if ( action == "first" )
		{
			start = 0;
		}
		else if ( action == "previous" )
		{
			start = len >= 0 ?
				start - len :
				0;
	
			if ( start < 0 )
			{
			  start = 0;
			}
		}
		else if ( action == "next" )
		{
			if ( start + len < records )
			{
				start += len;
			}
		}
		else if ( action == "last" )
		{
			start = Math.floor( (records-1) / len) * len;
		}
		else
		{
			_fnLog( settings, 0, "Unknown paging action: "+action, 5 );
		}
	
		var changed = settings._iDisplayStart !== start;
		settings._iDisplayStart = start;
	
		if ( changed ) {
			_fnCallbackFire( settings, null, 'page', [settings] );
	
			if ( redraw ) {
				_fnDraw( settings );
			}
		}
	
		return changed;
	}
	
	
	
	/**
	 * Generate the node required for the processing node
	 *  @param {object} settings dataTables settings object
	 *  @returns {node} Processing element
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlProcessing ( settings )
	{
		return $('<div/>', {
				'id': ! settings.aanFeatures.r ? settings.sTableId+'_processing' : null,
				'class': settings.oClasses.sProcessing
			} )
			.html( settings.oLanguage.sProcessing )
			.insertBefore( settings.nTable )[0];
	}
	
	
	/**
	 * Display or hide the processing indicator
	 *  @param {object} settings dataTables settings object
	 *  @param {bool} show Show the processing indicator (true) or not (false)
	 *  @memberof DataTable#oApi
	 */
	function _fnProcessingDisplay ( settings, show )
	{
		if ( settings.oFeatures.bProcessing ) {
			$(settings.aanFeatures.r).css( 'display', show ? 'block' : 'none' );
		}
	
		_fnCallbackFire( settings, null, 'processing', [settings, show] );
	}
	
	/**
	 * Add any control elements for the table - specifically scrolling
	 *  @param {object} settings dataTables settings object
	 *  @returns {node} Node to add to the DOM
	 *  @memberof DataTable#oApi
	 */
	function _fnFeatureHtmlTable ( settings )
	{
		var table = $(settings.nTable);
	
		// Add the ARIA grid role to the table
		table.attr( 'role', 'grid' );
	
		// Scrolling from here on in
		var scroll = settings.oScroll;
	
		if ( scroll.sX === '' && scroll.sY === '' ) {
			return settings.nTable;
		}
	
		var scrollX = scroll.sX;
		var scrollY = scroll.sY;
		var classes = settings.oClasses;
		var caption = table.children('caption');
		var captionSide = caption.length ? caption[0]._captionSide : null;
		var headerClone = $( table[0].cloneNode(false) );
		var footerClone = $( table[0].cloneNode(false) );
		var footer = table.children('tfoot');
		var _div = '<div/>';
		var size = function ( s ) {
			return !s ? null : _fnStringToCss( s );
		};
	
		if ( ! footer.length ) {
			footer = null;
		}
	
		/*
		 * The HTML structure that we want to generate in this function is:
		 *  div - scroller
		 *    div - scroll head
		 *      div - scroll head inner
		 *        table - scroll head table
		 *          thead - thead
		 *    div - scroll body
		 *      table - table (master table)
		 *        thead - thead clone for sizing
		 *        tbody - tbody
		 *    div - scroll foot
		 *      div - scroll foot inner
		 *        table - scroll foot table
		 *          tfoot - tfoot
		 */
		var scroller = $( _div, { 'class': classes.sScrollWrapper } )
			.append(
				$(_div, { 'class': classes.sScrollHead } )
					.css( {
						overflow: 'hidden',
						position: 'relative',
						border: 0,
						width: scrollX ? size(scrollX) : '100%'
					} )
					.append(
						$(_div, { 'class': classes.sScrollHeadInner } )
							.css( {
								'box-sizing': 'content-box',
								width: scroll.sXInner || '100%'
							} )
							.append(
								headerClone
									.removeAttr('id')
									.css( 'margin-left', 0 )
									.append( captionSide === 'top' ? caption : null )
									.append(
										table.children('thead')
									)
							)
					)
			)
			.append(
				$(_div, { 'class': classes.sScrollBody } )
					.css( {
						position: 'relative',
						overflow: 'auto',
						width: size( scrollX )
					} )
					.append( table )
			);
	
		if ( footer ) {
			scroller.append(
				$(_div, { 'class': classes.sScrollFoot } )
					.css( {
						overflow: 'hidden',
						border: 0,
						width: scrollX ? size(scrollX) : '100%'
					} )
					.append(
						$(_div, { 'class': classes.sScrollFootInner } )
							.append(
								footerClone
									.removeAttr('id')
									.css( 'margin-left', 0 )
									.append( captionSide === 'bottom' ? caption : null )
									.append(
										table.children('tfoot')
									)
							)
					)
			);
		}
	
		var children = scroller.children();
		var scrollHead = children[0];
		var scrollBody = children[1];
		var scrollFoot = footer ? children[2] : null;
	
		// When the body is scrolled, then we also want to scroll the headers
		if ( scrollX ) {
			$(scrollBody).on( 'scroll.DT', function (e) {
				var scrollLeft = this.scrollLeft;
	
				scrollHead.scrollLeft = scrollLeft;
	
				if ( footer ) {
					scrollFoot.scrollLeft = scrollLeft;
				}
			} );
		}
	
		$(scrollBody).css(
			scrollY && scroll.bCollapse ? 'max-height' : 'height', 
			scrollY
		);
	
		settings.nScrollHead = scrollHead;
		settings.nScrollBody = scrollBody;
		settings.nScrollFoot = scrollFoot;
	
		// On redraw - align columns
		settings.aoDrawCallback.push( {
			"fn": _fnScrollDraw,
			"sName": "scrolling"
		} );
	
		return scroller[0];
	}
	
	
	
	/**
	 * Update the header, footer and body tables for resizing - i.e. column
	 * alignment.
	 *
	 * Welcome to the most horrible function DataTables. The process that this
	 * function follows is basically:
	 *   1. Re-create the table inside the scrolling div
	 *   2. Take live measurements from the DOM
	 *   3. Apply the measurements to align the columns
	 *   4. Clean up
	 *
	 *  @param {object} settings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnScrollDraw ( settings )
	{
		// Given that this is such a monster function, a lot of variables are use
		// to try and keep the minimised size as small as possible
		var
			scroll         = settings.oScroll,
			scrollX        = scroll.sX,
			scrollXInner   = scroll.sXInner,
			scrollY        = scroll.sY,
			barWidth       = scroll.iBarWidth,
			divHeader      = $(settings.nScrollHead),
			divHeaderStyle = divHeader[0].style,
			divHeaderInner = divHeader.children('div'),
			divHeaderInnerStyle = divHeaderInner[0].style,
			divHeaderTable = divHeaderInner.children('table'),
			divBodyEl      = settings.nScrollBody,
			divBody        = $(divBodyEl),
			divBodyStyle   = divBodyEl.style,
			divFooter      = $(settings.nScrollFoot),
			divFooterInner = divFooter.children('div'),
			divFooterTable = divFooterInner.children('table'),
			header         = $(settings.nTHead),
			table          = $(settings.nTable),
			tableEl        = table[0],
			tableStyle     = tableEl.style,
			footer         = settings.nTFoot ? $(settings.nTFoot) : null,
			browser        = settings.oBrowser,
			ie67           = browser.bScrollOversize,
			dtHeaderCells  = _pluck( settings.aoColumns, 'nTh' ),
			headerTrgEls, footerTrgEls,
			headerSrcEls, footerSrcEls,
			headerCopy, footerCopy,
			headerWidths=[], footerWidths=[],
			headerContent=[], footerContent=[],
			idx, correction, sanityWidth,
			zeroOut = function(nSizer) {
				var style = nSizer.style;
				style.paddingTop = "0";
				style.paddingBottom = "0";
				style.borderTopWidth = "0";
				style.borderBottomWidth = "0";
				style.height = 0;
			};
	
		// If the scrollbar visibility has changed from the last draw, we need to
		// adjust the column sizes as the table width will have changed to account
		// for the scrollbar
		var scrollBarVis = divBodyEl.scrollHeight > divBodyEl.clientHeight;
		
		if ( settings.scrollBarVis !== scrollBarVis && settings.scrollBarVis !== undefined ) {
			settings.scrollBarVis = scrollBarVis;
			_fnAdjustColumnSizing( settings );
			return; // adjust column sizing will call this function again
		}
		else {
			settings.scrollBarVis = scrollBarVis;
		}
	
		/*
		 * 1. Re-create the table inside the scrolling div
		 */
	
		// Remove the old minimised thead and tfoot elements in the inner table
		table.children('thead, tfoot').remove();
	
		if ( footer ) {
			footerCopy = footer.clone().prependTo( table );
			footerTrgEls = footer.find('tr'); // the original tfoot is in its own table and must be sized
			footerSrcEls = footerCopy.find('tr');
		}
	
		// Clone the current header and footer elements and then place it into the inner table
		headerCopy = header.clone().prependTo( table );
		headerTrgEls = header.find('tr'); // original header is in its own table
		headerSrcEls = headerCopy.find('tr');
		headerCopy.find('th, td').removeAttr('tabindex');
	
	
		/*
		 * 2. Take live measurements from the DOM - do not alter the DOM itself!
		 */
	
		// Remove old sizing and apply the calculated column widths
		// Get the unique column headers in the newly created (cloned) header. We want to apply the
		// calculated sizes to this header
		if ( ! scrollX )
		{
			divBodyStyle.width = '100%';
			divHeader[0].style.width = '100%';
		}
	
		$.each( _fnGetUniqueThs( settings, headerCopy ), function ( i, el ) {
			idx = _fnVisibleToColumnIndex( settings, i );
			el.style.width = settings.aoColumns[idx].sWidth;
		} );
	
		if ( footer ) {
			_fnApplyToChildren( function(n) {
				n.style.width = "";
			}, footerSrcEls );
		}
	
		// Size the table as a whole
		sanityWidth = table.outerWidth();
		if ( scrollX === "" ) {
			// No x scrolling
			tableStyle.width = "100%";
	
			// IE7 will make the width of the table when 100% include the scrollbar
			// - which is shouldn't. When there is a scrollbar we need to take this
			// into account.
			if ( ie67 && (table.find('tbody').height() > divBodyEl.offsetHeight ||
				divBody.css('overflow-y') == "scroll")
			) {
				tableStyle.width = _fnStringToCss( table.outerWidth() - barWidth);
			}
	
			// Recalculate the sanity width
			sanityWidth = table.outerWidth();
		}
		else if ( scrollXInner !== "" ) {
			// legacy x scroll inner has been given - use it
			tableStyle.width = _fnStringToCss(scrollXInner);
	
			// Recalculate the sanity width
			sanityWidth = table.outerWidth();
		}
	
		// Hidden header should have zero height, so remove padding and borders. Then
		// set the width based on the real headers
	
		// Apply all styles in one pass
		_fnApplyToChildren( zeroOut, headerSrcEls );
	
		// Read all widths in next pass
		_fnApplyToChildren( function(nSizer) {
			headerContent.push( nSizer.innerHTML );
			headerWidths.push( _fnStringToCss( $(nSizer).css('width') ) );
		}, headerSrcEls );
	
		// Apply all widths in final pass
		_fnApplyToChildren( function(nToSize, i) {
			// Only apply widths to the DataTables detected header cells - this
			// prevents complex headers from having contradictory sizes applied
			if ( $.inArray( nToSize, dtHeaderCells ) !== -1 ) {
				nToSize.style.width = headerWidths[i];
			}
		}, headerTrgEls );
	
		$(headerSrcEls).height(0);
	
		/* Same again with the footer if we have one */
		if ( footer )
		{
			_fnApplyToChildren( zeroOut, footerSrcEls );
	
			_fnApplyToChildren( function(nSizer) {
				footerContent.push( nSizer.innerHTML );
				footerWidths.push( _fnStringToCss( $(nSizer).css('width') ) );
			}, footerSrcEls );
	
			_fnApplyToChildren( function(nToSize, i) {
				nToSize.style.width = footerWidths[i];
			}, footerTrgEls );
	
			$(footerSrcEls).height(0);
		}
	
	
		/*
		 * 3. Apply the measurements
		 */
	
		// "Hide" the header and footer that we used for the sizing. We need to keep
		// the content of the cell so that the width applied to the header and body
		// both match, but we want to hide it completely. We want to also fix their
		// width to what they currently are
		_fnApplyToChildren( function(nSizer, i) {
			nSizer.innerHTML = '<div class="dataTables_sizing">'+headerContent[i]+'</div>';
			nSizer.childNodes[0].style.height = "0";
			nSizer.childNodes[0].style.overflow = "hidden";
			nSizer.style.width = headerWidths[i];
		}, headerSrcEls );
	
		if ( footer )
		{
			_fnApplyToChildren( function(nSizer, i) {
				nSizer.innerHTML = '<div class="dataTables_sizing">'+footerContent[i]+'</div>';
				nSizer.childNodes[0].style.height = "0";
				nSizer.childNodes[0].style.overflow = "hidden";
				nSizer.style.width = footerWidths[i];
			}, footerSrcEls );
		}
	
		// Sanity check that the table is of a sensible width. If not then we are going to get
		// misalignment - try to prevent this by not allowing the table to shrink below its min width
		if ( table.outerWidth() < sanityWidth )
		{
			// The min width depends upon if we have a vertical scrollbar visible or not */
			correction = ((divBodyEl.scrollHeight > divBodyEl.offsetHeight ||
				divBody.css('overflow-y') == "scroll")) ?
					sanityWidth+barWidth :
					sanityWidth;
	
			// IE6/7 are a law unto themselves...
			if ( ie67 && (divBodyEl.scrollHeight >
				divBodyEl.offsetHeight || divBody.css('overflow-y') == "scroll")
			) {
				tableStyle.width = _fnStringToCss( correction-barWidth );
			}
	
			// And give the user a warning that we've stopped the table getting too small
			if ( scrollX === "" || scrollXInner !== "" ) {
				_fnLog( settings, 1, 'Possible column misalignment', 6 );
			}
		}
		else
		{
			correction = '100%';
		}
	
		// Apply to the container elements
		divBodyStyle.width = _fnStringToCss( correction );
		divHeaderStyle.width = _fnStringToCss( correction );
	
		if ( footer ) {
			settings.nScrollFoot.style.width = _fnStringToCss( correction );
		}
	
	
		/*
		 * 4. Clean up
		 */
		if ( ! scrollY ) {
			/* IE7< puts a vertical scrollbar in place (when it shouldn't be) due to subtracting
			 * the scrollbar height from the visible display, rather than adding it on. We need to
			 * set the height in order to sort this. Don't want to do it in any other browsers.
			 */
			if ( ie67 ) {
				divBodyStyle.height = _fnStringToCss( tableEl.offsetHeight+barWidth );
			}
		}
	
		/* Finally set the width's of the header and footer tables */
		var iOuterWidth = table.outerWidth();
		divHeaderTable[0].style.width = _fnStringToCss( iOuterWidth );
		divHeaderInnerStyle.width = _fnStringToCss( iOuterWidth );
	
		// Figure out if there are scrollbar present - if so then we need a the header and footer to
		// provide a bit more space to allow "overflow" scrolling (i.e. past the scrollbar)
		var bScrolling = table.height() > divBodyEl.clientHeight || divBody.css('overflow-y') == "scroll";
		var padding = 'padding' + (browser.bScrollbarLeft ? 'Left' : 'Right' );
		divHeaderInnerStyle[ padding ] = bScrolling ? barWidth+"px" : "0px";
	
		if ( footer ) {
			divFooterTable[0].style.width = _fnStringToCss( iOuterWidth );
			divFooterInner[0].style.width = _fnStringToCss( iOuterWidth );
			divFooterInner[0].style[padding] = bScrolling ? barWidth+"px" : "0px";
		}
	
		// Correct DOM ordering for colgroup - comes before the thead
		table.children('colgroup').insertBefore( table.children('thead') );
	
		/* Adjust the position of the header in case we loose the y-scrollbar */
		divBody.scroll();
	
		// If sorting or filtering has occurred, jump the scrolling back to the top
		// only if we aren't holding the position
		if ( (settings.bSorted || settings.bFiltered) && ! settings._drawHold ) {
			divBodyEl.scrollTop = 0;
		}
	}
	
	
	
	/**
	 * Apply a given function to the display child nodes of an element array (typically
	 * TD children of TR rows
	 *  @param {function} fn Method to apply to the objects
	 *  @param array {nodes} an1 List of elements to look through for display children
	 *  @param array {nodes} an2 Another list (identical structure to the first) - optional
	 *  @memberof DataTable#oApi
	 */
	function _fnApplyToChildren( fn, an1, an2 )
	{
		var index=0, i=0, iLen=an1.length;
		var nNode1, nNode2;
	
		while ( i < iLen ) {
			nNode1 = an1[i].firstChild;
			nNode2 = an2 ? an2[i].firstChild : null;
	
			while ( nNode1 ) {
				if ( nNode1.nodeType === 1 ) {
					if ( an2 ) {
						fn( nNode1, nNode2, index );
					}
					else {
						fn( nNode1, index );
					}
	
					index++;
				}
	
				nNode1 = nNode1.nextSibling;
				nNode2 = an2 ? nNode2.nextSibling : null;
			}
	
			i++;
		}
	}
	
	
	
	var __re_html_remove = /<.*?>/g;
	
	
	/**
	 * Calculate the width of columns for the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnCalculateColumnWidths ( oSettings )
	{
		var
			table = oSettings.nTable,
			columns = oSettings.aoColumns,
			scroll = oSettings.oScroll,
			scrollY = scroll.sY,
			scrollX = scroll.sX,
			scrollXInner = scroll.sXInner,
			columnCount = columns.length,
			visibleColumns = _fnGetColumns( oSettings, 'bVisible' ),
			headerCells = $('th', oSettings.nTHead),
			tableWidthAttr = table.getAttribute('width'), // from DOM element
			tableContainer = table.parentNode,
			userInputs = false,
			i, column, columnIdx, width, outerWidth,
			browser = oSettings.oBrowser,
			ie67 = browser.bScrollOversize;
	
		var styleWidth = table.style.width;
		if ( styleWidth && styleWidth.indexOf('%') !== -1 ) {
			tableWidthAttr = styleWidth;
		}
	
		/* Convert any user input sizes into pixel sizes */
		for ( i=0 ; i<visibleColumns.length ; i++ ) {
			column = columns[ visibleColumns[i] ];
	
			if ( column.sWidth !== null ) {
				column.sWidth = _fnConvertToWidth( column.sWidthOrig, tableContainer );
	
				userInputs = true;
			}
		}
	
		/* If the number of columns in the DOM equals the number that we have to
		 * process in DataTables, then we can use the offsets that are created by
		 * the web- browser. No custom sizes can be set in order for this to happen,
		 * nor scrolling used
		 */
		if ( ie67 || ! userInputs && ! scrollX && ! scrollY &&
		     columnCount == _fnVisbleColumns( oSettings ) &&
		     columnCount == headerCells.length
		) {
			for ( i=0 ; i<columnCount ; i++ ) {
				var colIdx = _fnVisibleToColumnIndex( oSettings, i );
	
				if ( colIdx !== null ) {
					columns[ colIdx ].sWidth = _fnStringToCss( headerCells.eq(i).width() );
				}
			}
		}
		else
		{
			// Otherwise construct a single row, worst case, table with the widest
			// node in the data, assign any user defined widths, then insert it into
			// the DOM and allow the browser to do all the hard work of calculating
			// table widths
			var tmpTable = $(table).clone() // don't use cloneNode - IE8 will remove events on the main table
				.css( 'visibility', 'hidden' )
				.removeAttr( 'id' );
	
			// Clean up the table body
			tmpTable.find('tbody tr').remove();
			var tr = $('<tr/>').appendTo( tmpTable.find('tbody') );
	
			// Clone the table header and footer - we can't use the header / footer
			// from the cloned table, since if scrolling is active, the table's
			// real header and footer are contained in different table tags
			tmpTable.find('thead, tfoot').remove();
			tmpTable
				.append( $(oSettings.nTHead).clone() )
				.append( $(oSettings.nTFoot).clone() );
	
			// Remove any assigned widths from the footer (from scrolling)
			tmpTable.find('tfoot th, tfoot td').css('width', '');
	
			// Apply custom sizing to the cloned header
			headerCells = _fnGetUniqueThs( oSettings, tmpTable.find('thead')[0] );
	
			for ( i=0 ; i<visibleColumns.length ; i++ ) {
				column = columns[ visibleColumns[i] ];
	
				headerCells[i].style.width = column.sWidthOrig !== null && column.sWidthOrig !== '' ?
					_fnStringToCss( column.sWidthOrig ) :
					'';
	
				// For scrollX we need to force the column width otherwise the
				// browser will collapse it. If this width is smaller than the
				// width the column requires, then it will have no effect
				if ( column.sWidthOrig && scrollX ) {
					$( headerCells[i] ).append( $('<div/>').css( {
						width: column.sWidthOrig,
						margin: 0,
						padding: 0,
						border: 0,
						height: 1
					} ) );
				}
			}
	
			// Find the widest cell for each column and put it into the table
			if ( oSettings.aoData.length ) {
				for ( i=0 ; i<visibleColumns.length ; i++ ) {
					columnIdx = visibleColumns[i];
					column = columns[ columnIdx ];
	
					$( _fnGetWidestNode( oSettings, columnIdx ) )
						.clone( false )
						.append( column.sContentPadding )
						.appendTo( tr );
				}
			}
	
			// Tidy the temporary table - remove name attributes so there aren't
			// duplicated in the dom (radio elements for example)
			$('[name]', tmpTable).removeAttr('name');
	
			// Table has been built, attach to the document so we can work with it.
			// A holding element is used, positioned at the top of the container
			// with minimal height, so it has no effect on if the container scrolls
			// or not. Otherwise it might trigger scrolling when it actually isn't
			// needed
			var holder = $('<div/>').css( scrollX || scrollY ?
					{
						position: 'absolute',
						top: 0,
						left: 0,
						height: 1,
						right: 0,
						overflow: 'hidden'
					} :
					{}
				)
				.append( tmpTable )
				.appendTo( tableContainer );
	
			// When scrolling (X or Y) we want to set the width of the table as 
			// appropriate. However, when not scrolling leave the table width as it
			// is. This results in slightly different, but I think correct behaviour
			if ( scrollX && scrollXInner ) {
				tmpTable.width( scrollXInner );
			}
			else if ( scrollX ) {
				tmpTable.css( 'width', 'auto' );
				tmpTable.removeAttr('width');
	
				// If there is no width attribute or style, then allow the table to
				// collapse
				if ( tmpTable.width() < tableContainer.clientWidth && tableWidthAttr ) {
					tmpTable.width( tableContainer.clientWidth );
				}
			}
			else if ( scrollY ) {
				tmpTable.width( tableContainer.clientWidth );
			}
			else if ( tableWidthAttr ) {
				tmpTable.width( tableWidthAttr );
			}
	
			// Get the width of each column in the constructed table - we need to
			// know the inner width (so it can be assigned to the other table's
			// cells) and the outer width so we can calculate the full width of the
			// table. This is safe since DataTables requires a unique cell for each
			// column, but if ever a header can span multiple columns, this will
			// need to be modified.
			var total = 0;
			for ( i=0 ; i<visibleColumns.length ; i++ ) {
				var cell = $(headerCells[i]);
				var border = cell.outerWidth() - cell.width();
	
				// Use getBounding... where possible (not IE8-) because it can give
				// sub-pixel accuracy, which we then want to round up!
				var bounding = browser.bBounding ?
					Math.ceil( headerCells[i].getBoundingClientRect().width ) :
					cell.outerWidth();
	
				// Total is tracked to remove any sub-pixel errors as the outerWidth
				// of the table might not equal the total given here (IE!).
				total += bounding;
	
				// Width for each column to use
				columns[ visibleColumns[i] ].sWidth = _fnStringToCss( bounding - border );
			}
	
			table.style.width = _fnStringToCss( total );
	
			// Finished with the table - ditch it
			holder.remove();
		}
	
		// If there is a width attr, we want to attach an event listener which
		// allows the table sizing to automatically adjust when the window is
		// resized. Use the width attr rather than CSS, since we can't know if the
		// CSS is a relative value or absolute - DOM read is always px.
		if ( tableWidthAttr ) {
			table.style.width = _fnStringToCss( tableWidthAttr );
		}
	
		if ( (tableWidthAttr || scrollX) && ! oSettings._reszEvt ) {
			var bindResize = function () {
				$(window).on('resize.DT-'+oSettings.sInstance, _fnThrottle( function () {
					_fnAdjustColumnSizing( oSettings );
				} ) );
			};
	
			// IE6/7 will crash if we bind a resize event handler on page load.
			// To be removed in 1.11 which drops IE6/7 support
			if ( ie67 ) {
				setTimeout( bindResize, 1000 );
			}
			else {
				bindResize();
			}
	
			oSettings._reszEvt = true;
		}
	}
	
	
	/**
	 * Throttle the calls to a function. Arguments and context are maintained for
	 * the throttled function
	 *  @param {function} fn Function to be called
	 *  @param {int} [freq=200] call frequency in mS
	 *  @returns {function} wrapped function
	 *  @memberof DataTable#oApi
	 */
	var _fnThrottle = DataTable.util.throttle;
	
	
	/**
	 * Convert a CSS unit width to pixels (e.g. 2em)
	 *  @param {string} width width to be converted
	 *  @param {node} parent parent to get the with for (required for relative widths) - optional
	 *  @returns {int} width in pixels
	 *  @memberof DataTable#oApi
	 */
	function _fnConvertToWidth ( width, parent )
	{
		if ( ! width ) {
			return 0;
		}
	
		var n = $('<div/>')
			.css( 'width', _fnStringToCss( width ) )
			.appendTo( parent || document.body );
	
		var val = n[0].offsetWidth;
		n.remove();
	
		return val;
	}
	
	
	/**
	 * Get the widest node
	 *  @param {object} settings dataTables settings object
	 *  @param {int} colIdx column of interest
	 *  @returns {node} widest table node
	 *  @memberof DataTable#oApi
	 */
	function _fnGetWidestNode( settings, colIdx )
	{
		var idx = _fnGetMaxLenString( settings, colIdx );
		if ( idx < 0 ) {
			return null;
		}
	
		var data = settings.aoData[ idx ];
		return ! data.nTr ? // Might not have been created when deferred rendering
			$('<td/>').html( _fnGetCellData( settings, idx, colIdx, 'display' ) )[0] :
			data.anCells[ colIdx ];
	}
	
	
	/**
	 * Get the maximum strlen for each data column
	 *  @param {object} settings dataTables settings object
	 *  @param {int} colIdx column of interest
	 *  @returns {string} max string length for each column
	 *  @memberof DataTable#oApi
	 */
	function _fnGetMaxLenString( settings, colIdx )
	{
		var s, max=-1, maxIdx = -1;
	
		for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
			s = _fnGetCellData( settings, i, colIdx, 'display' )+'';
			s = s.replace( __re_html_remove, '' );
			s = s.replace( /&nbsp;/g, ' ' );
	
			if ( s.length > max ) {
				max = s.length;
				maxIdx = i;
			}
		}
	
		return maxIdx;
	}
	
	
	/**
	 * Append a CSS unit (only if required) to a string
	 *  @param {string} value to css-ify
	 *  @returns {string} value with css unit
	 *  @memberof DataTable#oApi
	 */
	function _fnStringToCss( s )
	{
		if ( s === null ) {
			return '0px';
		}
	
		if ( typeof s == 'number' ) {
			return s < 0 ?
				'0px' :
				s+'px';
		}
	
		// Check it has a unit character already
		return s.match(/\d$/) ?
			s+'px' :
			s;
	}
	
	
	
	function _fnSortFlatten ( settings )
	{
		var
			i, iLen, k, kLen,
			aSort = [],
			aiOrig = [],
			aoColumns = settings.aoColumns,
			aDataSort, iCol, sType, srcCol,
			fixed = settings.aaSortingFixed,
			fixedObj = $.isPlainObject( fixed ),
			nestedSort = [],
			add = function ( a ) {
				if ( a.length && ! $.isArray( a[0] ) ) {
					// 1D array
					nestedSort.push( a );
				}
				else {
					// 2D array
					$.merge( nestedSort, a );
				}
			};
	
		// Build the sort array, with pre-fix and post-fix options if they have been
		// specified
		if ( $.isArray( fixed ) ) {
			add( fixed );
		}
	
		if ( fixedObj && fixed.pre ) {
			add( fixed.pre );
		}
	
		add( settings.aaSorting );
	
		if (fixedObj && fixed.post ) {
			add( fixed.post );
		}
	
		for ( i=0 ; i<nestedSort.length ; i++ )
		{
			srcCol = nestedSort[i][0];
			aDataSort = aoColumns[ srcCol ].aDataSort;
	
			for ( k=0, kLen=aDataSort.length ; k<kLen ; k++ )
			{
				iCol = aDataSort[k];
				sType = aoColumns[ iCol ].sType || 'string';
	
				if ( nestedSort[i]._idx === undefined ) {
					nestedSort[i]._idx = $.inArray( nestedSort[i][1], aoColumns[iCol].asSorting );
				}
	
				aSort.push( {
					src:       srcCol,
					col:       iCol,
					dir:       nestedSort[i][1],
					index:     nestedSort[i]._idx,
					type:      sType,
					formatter: DataTable.ext.type.order[ sType+"-pre" ]
				} );
			}
		}
	
		return aSort;
	}
	
	/**
	 * Change the order of the table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 *  @todo This really needs split up!
	 */
	function _fnSort ( oSettings )
	{
		var
			i, ien, iLen, j, jLen, k, kLen,
			sDataType, nTh,
			aiOrig = [],
			oExtSort = DataTable.ext.type.order,
			aoData = oSettings.aoData,
			aoColumns = oSettings.aoColumns,
			aDataSort, data, iCol, sType, oSort,
			formatters = 0,
			sortCol,
			displayMaster = oSettings.aiDisplayMaster,
			aSort;
	
		// Resolve any column types that are unknown due to addition or invalidation
		// @todo Can this be moved into a 'data-ready' handler which is called when
		//   data is going to be used in the table?
		_fnColumnTypes( oSettings );
	
		aSort = _fnSortFlatten( oSettings );
	
		for ( i=0, ien=aSort.length ; i<ien ; i++ ) {
			sortCol = aSort[i];
	
			// Track if we can use the fast sort algorithm
			if ( sortCol.formatter ) {
				formatters++;
			}
	
			// Load the data needed for the sort, for each cell
			_fnSortData( oSettings, sortCol.col );
		}
	
		/* No sorting required if server-side or no sorting array */
		if ( _fnDataSource( oSettings ) != 'ssp' && aSort.length !== 0 )
		{
			// Create a value - key array of the current row positions such that we can use their
			// current position during the sort, if values match, in order to perform stable sorting
			for ( i=0, iLen=displayMaster.length ; i<iLen ; i++ ) {
				aiOrig[ displayMaster[i] ] = i;
			}
	
			/* Do the sort - here we want multi-column sorting based on a given data source (column)
			 * and sorting function (from oSort) in a certain direction. It's reasonably complex to
			 * follow on it's own, but this is what we want (example two column sorting):
			 *  fnLocalSorting = function(a,b){
			 *    var iTest;
			 *    iTest = oSort['string-asc']('data11', 'data12');
			 *      if (iTest !== 0)
			 *        return iTest;
			 *    iTest = oSort['numeric-desc']('data21', 'data22');
			 *    if (iTest !== 0)
			 *      return iTest;
			 *    return oSort['numeric-asc']( aiOrig[a], aiOrig[b] );
			 *  }
			 * Basically we have a test for each sorting column, if the data in that column is equal,
			 * test the next column. If all columns match, then we use a numeric sort on the row
			 * positions in the original data array to provide a stable sort.
			 *
			 * Note - I know it seems excessive to have two sorting methods, but the first is around
			 * 15% faster, so the second is only maintained for backwards compatibility with sorting
			 * methods which do not have a pre-sort formatting function.
			 */
			if ( formatters === aSort.length ) {
				// All sort types have formatting functions
				displayMaster.sort( function ( a, b ) {
					var
						x, y, k, test, sort,
						len=aSort.length,
						dataA = aoData[a]._aSortData,
						dataB = aoData[b]._aSortData;
	
					for ( k=0 ; k<len ; k++ ) {
						sort = aSort[k];
	
						x = dataA[ sort.col ];
						y = dataB[ sort.col ];
	
						test = x<y ? -1 : x>y ? 1 : 0;
						if ( test !== 0 ) {
							return sort.dir === 'asc' ? test : -test;
						}
					}
	
					x = aiOrig[a];
					y = aiOrig[b];
					return x<y ? -1 : x>y ? 1 : 0;
				} );
			}
			else {
				// Depreciated - remove in 1.11 (providing a plug-in option)
				// Not all sort types have formatting methods, so we have to call their sorting
				// methods.
				displayMaster.sort( function ( a, b ) {
					var
						x, y, k, l, test, sort, fn,
						len=aSort.length,
						dataA = aoData[a]._aSortData,
						dataB = aoData[b]._aSortData;
	
					for ( k=0 ; k<len ; k++ ) {
						sort = aSort[k];
	
						x = dataA[ sort.col ];
						y = dataB[ sort.col ];
	
						fn = oExtSort[ sort.type+"-"+sort.dir ] || oExtSort[ "string-"+sort.dir ];
						test = fn( x, y );
						if ( test !== 0 ) {
							return test;
						}
					}
	
					x = aiOrig[a];
					y = aiOrig[b];
					return x<y ? -1 : x>y ? 1 : 0;
				} );
			}
		}
	
		/* Tell the draw function that we have sorted the data */
		oSettings.bSorted = true;
	}
	
	
	function _fnSortAria ( settings )
	{
		var label;
		var nextSort;
		var columns = settings.aoColumns;
		var aSort = _fnSortFlatten( settings );
		var oAria = settings.oLanguage.oAria;
	
		// ARIA attributes - need to loop all columns, to update all (removing old
		// attributes as needed)
		for ( var i=0, iLen=columns.length ; i<iLen ; i++ )
		{
			var col = columns[i];
			var asSorting = col.asSorting;
			var sTitle = col.sTitle.replace( /<.*?>/g, "" );
			var th = col.nTh;
	
			// IE7 is throwing an error when setting these properties with jQuery's
			// attr() and removeAttr() methods...
			th.removeAttribute('aria-sort');
	
			/* In ARIA only the first sorting column can be marked as sorting - no multi-sort option */
			if ( col.bSortable ) {
				if ( aSort.length > 0 && aSort[0].col == i ) {
					th.setAttribute('aria-sort', aSort[0].dir=="asc" ? "ascending" : "descending" );
					nextSort = asSorting[ aSort[0].index+1 ] || asSorting[0];
				}
				else {
					nextSort = asSorting[0];
				}
	
				label = sTitle + ( nextSort === "asc" ?
					oAria.sSortAscending :
					oAria.sSortDescending
				);
			}
			else {
				label = sTitle;
			}
	
			th.setAttribute('aria-label', label);
		}
	}
	
	
	/**
	 * Function to run on user sort request
	 *  @param {object} settings dataTables settings object
	 *  @param {node} attachTo node to attach the handler to
	 *  @param {int} colIdx column sorting index
	 *  @param {boolean} [append=false] Append the requested sort to the existing
	 *    sort if true (i.e. multi-column sort)
	 *  @param {function} [callback] callback function
	 *  @memberof DataTable#oApi
	 */
	function _fnSortListener ( settings, colIdx, append, callback )
	{
		var col = settings.aoColumns[ colIdx ];
		var sorting = settings.aaSorting;
		var asSorting = col.asSorting;
		var nextSortIdx;
		var next = function ( a, overflow ) {
			var idx = a._idx;
			if ( idx === undefined ) {
				idx = $.inArray( a[1], asSorting );
			}
	
			return idx+1 < asSorting.length ?
				idx+1 :
				overflow ?
					null :
					0;
		};
	
		// Convert to 2D array if needed
		if ( typeof sorting[0] === 'number' ) {
			sorting = settings.aaSorting = [ sorting ];
		}
	
		// If appending the sort then we are multi-column sorting
		if ( append && settings.oFeatures.bSortMulti ) {
			// Are we already doing some kind of sort on this column?
			var sortIdx = $.inArray( colIdx, _pluck(sorting, '0') );
	
			if ( sortIdx !== -1 ) {
				// Yes, modify the sort
				nextSortIdx = next( sorting[sortIdx], true );
	
				if ( nextSortIdx === null && sorting.length === 1 ) {
					nextSortIdx = 0; // can't remove sorting completely
				}
	
				if ( nextSortIdx === null ) {
					sorting.splice( sortIdx, 1 );
				}
				else {
					sorting[sortIdx][1] = asSorting[ nextSortIdx ];
					sorting[sortIdx]._idx = nextSortIdx;
				}
			}
			else {
				// No sort on this column yet
				sorting.push( [ colIdx, asSorting[0], 0 ] );
				sorting[sorting.length-1]._idx = 0;
			}
		}
		else if ( sorting.length && sorting[0][0] == colIdx ) {
			// Single column - already sorting on this column, modify the sort
			nextSortIdx = next( sorting[0] );
	
			sorting.length = 1;
			sorting[0][1] = asSorting[ nextSortIdx ];
			sorting[0]._idx = nextSortIdx;
		}
		else {
			// Single column - sort only on this column
			sorting.length = 0;
			sorting.push( [ colIdx, asSorting[0] ] );
			sorting[0]._idx = 0;
		}
	
		// Run the sort by calling a full redraw
		_fnReDraw( settings );
	
		// callback used for async user interaction
		if ( typeof callback == 'function' ) {
			callback( settings );
		}
	}
	
	
	/**
	 * Attach a sort handler (click) to a node
	 *  @param {object} settings dataTables settings object
	 *  @param {node} attachTo node to attach the handler to
	 *  @param {int} colIdx column sorting index
	 *  @param {function} [callback] callback function
	 *  @memberof DataTable#oApi
	 */
	function _fnSortAttachListener ( settings, attachTo, colIdx, callback )
	{
		var col = settings.aoColumns[ colIdx ];
	
		_fnBindAction( attachTo, {}, function (e) {
			/* If the column is not sortable - don't to anything */
			if ( col.bSortable === false ) {
				return;
			}
	
			// If processing is enabled use a timeout to allow the processing
			// display to be shown - otherwise to it synchronously
			if ( settings.oFeatures.bProcessing ) {
				_fnProcessingDisplay( settings, true );
	
				setTimeout( function() {
					_fnSortListener( settings, colIdx, e.shiftKey, callback );
	
					// In server-side processing, the draw callback will remove the
					// processing display
					if ( _fnDataSource( settings ) !== 'ssp' ) {
						_fnProcessingDisplay( settings, false );
					}
				}, 0 );
			}
			else {
				_fnSortListener( settings, colIdx, e.shiftKey, callback );
			}
		} );
	}
	
	
	/**
	 * Set the sorting classes on table's body, Note: it is safe to call this function
	 * when bSort and bSortClasses are false
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnSortingClasses( settings )
	{
		var oldSort = settings.aLastSort;
		var sortClass = settings.oClasses.sSortColumn;
		var sort = _fnSortFlatten( settings );
		var features = settings.oFeatures;
		var i, ien, colIdx;
	
		if ( features.bSort && features.bSortClasses ) {
			// Remove old sorting classes
			for ( i=0, ien=oldSort.length ; i<ien ; i++ ) {
				colIdx = oldSort[i].src;
	
				// Remove column sorting
				$( _pluck( settings.aoData, 'anCells', colIdx ) )
					.removeClass( sortClass + (i<2 ? i+1 : 3) );
			}
	
			// Add new column sorting
			for ( i=0, ien=sort.length ; i<ien ; i++ ) {
				colIdx = sort[i].src;
	
				$( _pluck( settings.aoData, 'anCells', colIdx ) )
					.addClass( sortClass + (i<2 ? i+1 : 3) );
			}
		}
	
		settings.aLastSort = sort;
	}
	
	
	// Get the data to sort a column, be it from cache, fresh (populating the
	// cache), or from a sort formatter
	function _fnSortData( settings, idx )
	{
		// Custom sorting function - provided by the sort data type
		var column = settings.aoColumns[ idx ];
		var customSort = DataTable.ext.order[ column.sSortDataType ];
		var customData;
	
		if ( customSort ) {
			customData = customSort.call( settings.oInstance, settings, idx,
				_fnColumnIndexToVisible( settings, idx )
			);
		}
	
		// Use / populate cache
		var row, cellData;
		var formatter = DataTable.ext.type.order[ column.sType+"-pre" ];
	
		for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
			row = settings.aoData[i];
	
			if ( ! row._aSortData ) {
				row._aSortData = [];
			}
	
			if ( ! row._aSortData[idx] || customSort ) {
				cellData = customSort ?
					customData[i] : // If there was a custom sort function, use data from there
					_fnGetCellData( settings, i, idx, 'sort' );
	
				row._aSortData[ idx ] = formatter ?
					formatter( cellData ) :
					cellData;
			}
		}
	}
	
	
	
	/**
	 * Save the state of a table
	 *  @param {object} oSettings dataTables settings object
	 *  @memberof DataTable#oApi
	 */
	function _fnSaveState ( settings )
	{
		if ( !settings.oFeatures.bStateSave || settings.bDestroying )
		{
			return;
		}
	
		/* Store the interesting variables */
		var state = {
			time:    +new Date(),
			start:   settings._iDisplayStart,
			length:  settings._iDisplayLength,
			order:   $.extend( true, [], settings.aaSorting ),
			search:  _fnSearchToCamel( settings.oPreviousSearch ),
			columns: $.map( settings.aoColumns, function ( col, i ) {
				return {
					visible: col.bVisible,
					search: _fnSearchToCamel( settings.aoPreSearchCols[i] )
				};
			} )
		};
	
		_fnCallbackFire( settings, "aoStateSaveParams", 'stateSaveParams', [settings, state] );
	
		settings.oSavedState = state;
		settings.fnStateSaveCallback.call( settings.oInstance, settings, state );
	}
	
	
	/**
	 * Attempt to load a saved table state
	 *  @param {object} oSettings dataTables settings object
	 *  @param {object} oInit DataTables init object so we can override settings
	 *  @param {function} callback Callback to execute when the state has been loaded
	 *  @memberof DataTable#oApi
	 */
	function _fnLoadState ( settings, oInit, callback )
	{
		var i, ien;
		var columns = settings.aoColumns;
		var loaded = function ( s ) {
			if ( ! s || ! s.time ) {
				callback();
				return;
			}
	
			// Allow custom and plug-in manipulation functions to alter the saved data set and
			// cancelling of loading by returning false
			var abStateLoad = _fnCallbackFire( settings, 'aoStateLoadParams', 'stateLoadParams', [settings, s] );
			if ( $.inArray( false, abStateLoad ) !== -1 ) {
				callback();
				return;
			}
	
			// Reject old data
			var duration = settings.iStateDuration;
			if ( duration > 0 && s.time < +new Date() - (duration*1000) ) {
				callback();
				return;
			}
	
			// Number of columns have changed - all bets are off, no restore of settings
			if ( s.columns && columns.length !== s.columns.length ) {
				callback();
				return;
			}
	
			// Store the saved state so it might be accessed at any time
			settings.oLoadedState = $.extend( true, {}, s );
	
			// Restore key features - todo - for 1.11 this needs to be done by
			// subscribed events
			if ( s.start !== undefined ) {
				settings._iDisplayStart    = s.start;
				settings.iInitDisplayStart = s.start;
			}
			if ( s.length !== undefined ) {
				settings._iDisplayLength   = s.length;
			}
	
			// Order
			if ( s.order !== undefined ) {
				settings.aaSorting = [];
				$.each( s.order, function ( i, col ) {
					settings.aaSorting.push( col[0] >= columns.length ?
						[ 0, col[1] ] :
						col
					);
				} );
			}
	
			// Search
			if ( s.search !== undefined ) {
				$.extend( settings.oPreviousSearch, _fnSearchToHung( s.search ) );
			}
	
			// Columns
			//
			if ( s.columns ) {
				for ( i=0, ien=s.columns.length ; i<ien ; i++ ) {
					var col = s.columns[i];
	
					// Visibility
					if ( col.visible !== undefined ) {
						columns[i].bVisible = col.visible;
					}
	
					// Search
					if ( col.search !== undefined ) {
						$.extend( settings.aoPreSearchCols[i], _fnSearchToHung( col.search ) );
					}
				}
			}
	
			_fnCallbackFire( settings, 'aoStateLoaded', 'stateLoaded', [settings, s] );
			callback();
		}
	
		if ( ! settings.oFeatures.bStateSave ) {
			callback();
			return;
		}
	
		var state = settings.fnStateLoadCallback.call( settings.oInstance, settings, loaded );
	
		if ( state !== undefined ) {
			loaded( state );
		}
		// otherwise, wait for the loaded callback to be executed
	}
	
	
	/**
	 * Return the settings object for a particular table
	 *  @param {node} table table we are using as a dataTable
	 *  @returns {object} Settings object - or null if not found
	 *  @memberof DataTable#oApi
	 */
	function _fnSettingsFromNode ( table )
	{
		var settings = DataTable.settings;
		var idx = $.inArray( table, _pluck( settings, 'nTable' ) );
	
		return idx !== -1 ?
			settings[ idx ] :
			null;
	}
	
	
	/**
	 * Log an error message
	 *  @param {object} settings dataTables settings object
	 *  @param {int} level log error messages, or display them to the user
	 *  @param {string} msg error message
	 *  @param {int} tn Technical note id to get more information about the error.
	 *  @memberof DataTable#oApi
	 */
	function _fnLog( settings, level, msg, tn )
	{
		msg = 'DataTables warning: '+
			(settings ? 'table id='+settings.sTableId+' - ' : '')+msg;
	
		if ( tn ) {
			msg += '. For more information about this error, please see '+
			'http://datatables.net/tn/'+tn;
		}
	
		if ( ! level  ) {
			// Backwards compatibility pre 1.10
			var ext = DataTable.ext;
			var type = ext.sErrMode || ext.errMode;
	
			if ( settings ) {
				_fnCallbackFire( settings, null, 'error', [ settings, tn, msg ] );
			}
	
			if ( type == 'alert' ) {
				alert( msg );
			}
			else if ( type == 'throw' ) {
				throw new Error(msg);
			}
			else if ( typeof type == 'function' ) {
				type( settings, tn, msg );
			}
		}
		else if ( window.console && console.log ) {
			console.log( msg );
		}
	}
	
	
	/**
	 * See if a property is defined on one object, if so assign it to the other object
	 *  @param {object} ret target object
	 *  @param {object} src source object
	 *  @param {string} name property
	 *  @param {string} [mappedName] name to map too - optional, name used if not given
	 *  @memberof DataTable#oApi
	 */
	function _fnMap( ret, src, name, mappedName )
	{
		if ( $.isArray( name ) ) {
			$.each( name, function (i, val) {
				if ( $.isArray( val ) ) {
					_fnMap( ret, src, val[0], val[1] );
				}
				else {
					_fnMap( ret, src, val );
				}
			} );
	
			return;
		}
	
		if ( mappedName === undefined ) {
			mappedName = name;
		}
	
		if ( src[name] !== undefined ) {
			ret[mappedName] = src[name];
		}
	}
	
	
	/**
	 * Extend objects - very similar to jQuery.extend, but deep copy objects, and
	 * shallow copy arrays. The reason we need to do this, is that we don't want to
	 * deep copy array init values (such as aaSorting) since the dev wouldn't be
	 * able to override them, but we do want to deep copy arrays.
	 *  @param {object} out Object to extend
	 *  @param {object} extender Object from which the properties will be applied to
	 *      out
	 *  @param {boolean} breakRefs If true, then arrays will be sliced to take an
	 *      independent copy with the exception of the `data` or `aaData` parameters
	 *      if they are present. This is so you can pass in a collection to
	 *      DataTables and have that used as your data source without breaking the
	 *      references
	 *  @returns {object} out Reference, just for convenience - out === the return.
	 *  @memberof DataTable#oApi
	 *  @todo This doesn't take account of arrays inside the deep copied objects.
	 */
	function _fnExtend( out, extender, breakRefs )
	{
		var val;
	
		for ( var prop in extender ) {
			if ( extender.hasOwnProperty(prop) ) {
				val = extender[prop];
	
				if ( $.isPlainObject( val ) ) {
					if ( ! $.isPlainObject( out[prop] ) ) {
						out[prop] = {};
					}
					$.extend( true, out[prop], val );
				}
				else if ( breakRefs && prop !== 'data' && prop !== 'aaData' && $.isArray(val) ) {
					out[prop] = val.slice();
				}
				else {
					out[prop] = val;
				}
			}
		}
	
		return out;
	}
	
	
	/**
	 * Bind an event handers to allow a click or return key to activate the callback.
	 * This is good for accessibility since a return on the keyboard will have the
	 * same effect as a click, if the element has focus.
	 *  @param {element} n Element to bind the action to
	 *  @param {object} oData Data object to pass to the triggered function
	 *  @param {function} fn Callback function for when the event is triggered
	 *  @memberof DataTable#oApi
	 */
	function _fnBindAction( n, oData, fn )
	{
		$(n)
			.on( 'click.DT', oData, function (e) {
					$(n).blur(); // Remove focus outline for mouse users
					fn(e);
				} )
			.on( 'keypress.DT', oData, function (e){
					if ( e.which === 13 ) {
						e.preventDefault();
						fn(e);
					}
				} )
			.on( 'selectstart.DT', function () {
					/* Take the brutal approach to cancelling text selection */
					return false;
				} );
	}
	
	
	/**
	 * Register a callback function. Easily allows a callback function to be added to
	 * an array store of callback functions that can then all be called together.
	 *  @param {object} oSettings dataTables settings object
	 *  @param {string} sStore Name of the array storage for the callbacks in oSettings
	 *  @param {function} fn Function to be called back
	 *  @param {string} sName Identifying name for the callback (i.e. a label)
	 *  @memberof DataTable#oApi
	 */
	function _fnCallbackReg( oSettings, sStore, fn, sName )
	{
		if ( fn )
		{
			oSettings[sStore].push( {
				"fn": fn,
				"sName": sName
			} );
		}
	}
	
	
	/**
	 * Fire callback functions and trigger events. Note that the loop over the
	 * callback array store is done backwards! Further note that you do not want to
	 * fire off triggers in time sensitive applications (for example cell creation)
	 * as its slow.
	 *  @param {object} settings dataTables settings object
	 *  @param {string} callbackArr Name of the array storage for the callbacks in
	 *      oSettings
	 *  @param {string} eventName Name of the jQuery custom event to trigger. If
	 *      null no trigger is fired
	 *  @param {array} args Array of arguments to pass to the callback function /
	 *      trigger
	 *  @memberof DataTable#oApi
	 */
	function _fnCallbackFire( settings, callbackArr, eventName, args )
	{
		var ret = [];
	
		if ( callbackArr ) {
			ret = $.map( settings[callbackArr].slice().reverse(), function (val, i) {
				return val.fn.apply( settings.oInstance, args );
			} );
		}
	
		if ( eventName !== null ) {
			var e = $.Event( eventName+'.dt' );
	
			$(settings.nTable).trigger( e, args );
	
			ret.push( e.result );
		}
	
		return ret;
	}
	
	
	function _fnLengthOverflow ( settings )
	{
		var
			start = settings._iDisplayStart,
			end = settings.fnDisplayEnd(),
			len = settings._iDisplayLength;
	
		/* If we have space to show extra rows (backing up from the end point - then do so */
		if ( start >= end )
		{
			start = end - len;
		}
	
		// Keep the start record on the current page
		start -= (start % len);
	
		if ( len === -1 || start < 0 )
		{
			start = 0;
		}
	
		settings._iDisplayStart = start;
	}
	
	
	function _fnRenderer( settings, type )
	{
		var renderer = settings.renderer;
		var host = DataTable.ext.renderer[type];
	
		if ( $.isPlainObject( renderer ) && renderer[type] ) {
			// Specific renderer for this type. If available use it, otherwise use
			// the default.
			return host[renderer[type]] || host._;
		}
		else if ( typeof renderer === 'string' ) {
			// Common renderer - if there is one available for this type use it,
			// otherwise use the default
			return host[renderer] || host._;
		}
	
		// Use the default
		return host._;
	}
	
	
	/**
	 * Detect the data source being used for the table. Used to simplify the code
	 * a little (ajax) and to make it compress a little smaller.
	 *
	 *  @param {object} settings dataTables settings object
	 *  @returns {string} Data source
	 *  @memberof DataTable#oApi
	 */
	function _fnDataSource ( settings )
	{
		if ( settings.oFeatures.bServerSide ) {
			return 'ssp';
		}
		else if ( settings.ajax || settings.sAjaxSource ) {
			return 'ajax';
		}
		return 'dom';
	}
	

	
	
	/**
	 * Computed structure of the DataTables API, defined by the options passed to
	 * `DataTable.Api.register()` when building the API.
	 *
	 * The structure is built in order to speed creation and extension of the Api
	 * objects since the extensions are effectively pre-parsed.
	 *
	 * The array is an array of objects with the following structure, where this
	 * base array represents the Api prototype base:
	 *
	 *     [
	 *       {
	 *         name:      'data'                -- string   - Property name
	 *         val:       function () {},       -- function - Api method (or undefined if just an object
	 *         methodExt: [ ... ],              -- array    - Array of Api object definitions to extend the method result
	 *         propExt:   [ ... ]               -- array    - Array of Api object definitions to extend the property
	 *       },
	 *       {
	 *         name:     'row'
	 *         val:       {},
	 *         methodExt: [ ... ],
	 *         propExt:   [
	 *           {
	 *             name:      'data'
	 *             val:       function () {},
	 *             methodExt: [ ... ],
	 *             propExt:   [ ... ]
	 *           },
	 *           ...
	 *         ]
	 *       }
	 *     ]
	 *
	 * @type {Array}
	 * @ignore
	 */
	var __apiStruct = [];
	
	
	/**
	 * `Array.prototype` reference.
	 *
	 * @type object
	 * @ignore
	 */
	var __arrayProto = Array.prototype;
	
	
	/**
	 * Abstraction for `context` parameter of the `Api` constructor to allow it to
	 * take several different forms for ease of use.
	 *
	 * Each of the input parameter types will be converted to a DataTables settings
	 * object where possible.
	 *
	 * @param  {string|node|jQuery|object} mixed DataTable identifier. Can be one
	 *   of:
	 *
	 *   * `string` - jQuery selector. Any DataTables' matching the given selector
	 *     with be found and used.
	 *   * `node` - `TABLE` node which has already been formed into a DataTable.
	 *   * `jQuery` - A jQuery object of `TABLE` nodes.
	 *   * `object` - DataTables settings object
	 *   * `DataTables.Api` - API instance
	 * @return {array|null} Matching DataTables settings objects. `null` or
	 *   `undefined` is returned if no matching DataTable is found.
	 * @ignore
	 */
	var _toSettings = function ( mixed )
	{
		var idx, jq;
		var settings = DataTable.settings;
		var tables = $.map( settings, function (el, i) {
			return el.nTable;
		} );
	
		if ( ! mixed ) {
			return [];
		}
		else if ( mixed.nTable && mixed.oApi ) {
			// DataTables settings object
			return [ mixed ];
		}
		else if ( mixed.nodeName && mixed.nodeName.toLowerCase() === 'table' ) {
			// Table node
			idx = $.inArray( mixed, tables );
			return idx !== -1 ? [ settings[idx] ] : null;
		}
		else if ( mixed && typeof mixed.settings === 'function' ) {
			return mixed.settings().toArray();
		}
		else if ( typeof mixed === 'string' ) {
			// jQuery selector
			jq = $(mixed);
		}
		else if ( mixed instanceof $ ) {
			// jQuery object (also DataTables instance)
			jq = mixed;
		}
	
		if ( jq ) {
			return jq.map( function(i) {
				idx = $.inArray( this, tables );
				return idx !== -1 ? settings[idx] : null;
			} ).toArray();
		}
	};
	
	
	/**
	 * DataTables API class - used to control and interface with  one or more
	 * DataTables enhanced tables.
	 *
	 * The API class is heavily based on jQuery, presenting a chainable interface
	 * that you can use to interact with tables. Each instance of the API class has
	 * a "context" - i.e. the tables that it will operate on. This could be a single
	 * table, all tables on a page or a sub-set thereof.
	 *
	 * Additionally the API is designed to allow you to easily work with the data in
	 * the tables, retrieving and manipulating it as required. This is done by
	 * presenting the API class as an array like interface. The contents of the
	 * array depend upon the actions requested by each method (for example
	 * `rows().nodes()` will return an array of nodes, while `rows().data()` will
	 * return an array of objects or arrays depending upon your table's
	 * configuration). The API object has a number of array like methods (`push`,
	 * `pop`, `reverse` etc) as well as additional helper methods (`each`, `pluck`,
	 * `unique` etc) to assist your working with the data held in a table.
	 *
	 * Most methods (those which return an Api instance) are chainable, which means
	 * the return from a method call also has all of the methods available that the
	 * top level object had. For example, these two calls are equivalent:
	 *
	 *     // Not chained
	 *     api.row.add( {...} );
	 *     api.draw();
	 *
	 *     // Chained
	 *     api.row.add( {...} ).draw();
	 *
	 * @class DataTable.Api
	 * @param {array|object|string|jQuery} context DataTable identifier. This is
	 *   used to define which DataTables enhanced tables this API will operate on.
	 *   Can be one of:
	 *
	 *   * `string` - jQuery selector. Any DataTables' matching the given selector
	 *     with be found and used.
	 *   * `node` - `TABLE` node which has already been formed into a DataTable.
	 *   * `jQuery` - A jQuery object of `TABLE` nodes.
	 *   * `object` - DataTables settings object
	 * @param {array} [data] Data to initialise the Api instance with.
	 *
	 * @example
	 *   // Direct initialisation during DataTables construction
	 *   var api = $('#example').DataTable();
	 *
	 * @example
	 *   // Initialisation using a DataTables jQuery object
	 *   var api = $('#example').dataTable().api();
	 *
	 * @example
	 *   // Initialisation as a constructor
	 *   var api = new $.fn.DataTable.Api( 'table.dataTable' );
	 */
	_Api = function ( context, data )
	{
		if ( ! (this instanceof _Api) ) {
			return new _Api( context, data );
		}
	
		var settings = [];
		var ctxSettings = function ( o ) {
			var a = _toSettings( o );
			if ( a ) {
				settings = settings.concat( a );
			}
		};
	
		if ( $.isArray( context ) ) {
			for ( var i=0, ien=context.length ; i<ien ; i++ ) {
				ctxSettings( context[i] );
			}
		}
		else {
			ctxSettings( context );
		}
	
		// Remove duplicates
		this.context = _unique( settings );
	
		// Initial data
		if ( data ) {
			$.merge( this, data );
		}
	
		// selector
		this.selector = {
			rows: null,
			cols: null,
			opts: null
		};
	
		_Api.extend( this, this, __apiStruct );
	};
	
	DataTable.Api = _Api;
	
	// Don't destroy the existing prototype, just extend it. Required for jQuery 2's
	// isPlainObject.
	$.extend( _Api.prototype, {
		any: function ()
		{
			return this.count() !== 0;
		},
	
	
		concat:  __arrayProto.concat,
	
	
		context: [], // array of table settings objects
	
	
		count: function ()
		{
			return this.flatten().length;
		},
	
	
		each: function ( fn )
		{
			for ( var i=0, ien=this.length ; i<ien; i++ ) {
				fn.call( this, this[i], i, this );
			}
	
			return this;
		},
	
	
		eq: function ( idx )
		{
			var ctx = this.context;
	
			return ctx.length > idx ?
				new _Api( ctx[idx], this[idx] ) :
				null;
		},
	
	
		filter: function ( fn )
		{
			var a = [];
	
			if ( __arrayProto.filter ) {
				a = __arrayProto.filter.call( this, fn, this );
			}
			else {
				// Compatibility for browsers without EMCA-252-5 (JS 1.6)
				for ( var i=0, ien=this.length ; i<ien ; i++ ) {
					if ( fn.call( this, this[i], i, this ) ) {
						a.push( this[i] );
					}
				}
			}
	
			return new _Api( this.context, a );
		},
	
	
		flatten: function ()
		{
			var a = [];
			return new _Api( this.context, a.concat.apply( a, this.toArray() ) );
		},
	
	
		join:    __arrayProto.join,
	
	
		indexOf: __arrayProto.indexOf || function (obj, start)
		{
			for ( var i=(start || 0), ien=this.length ; i<ien ; i++ ) {
				if ( this[i] === obj ) {
					return i;
				}
			}
			return -1;
		},
	
		iterator: function ( flatten, type, fn, alwaysNew ) {
			var
				a = [], ret,
				i, ien, j, jen,
				context = this.context,
				rows, items, item,
				selector = this.selector;
	
			// Argument shifting
			if ( typeof flatten === 'string' ) {
				alwaysNew = fn;
				fn = type;
				type = flatten;
				flatten = false;
			}
	
			for ( i=0, ien=context.length ; i<ien ; i++ ) {
				var apiInst = new _Api( context[i] );
	
				if ( type === 'table' ) {
					ret = fn.call( apiInst, context[i], i );
	
					if ( ret !== undefined ) {
						a.push( ret );
					}
				}
				else if ( type === 'columns' || type === 'rows' ) {
					// this has same length as context - one entry for each table
					ret = fn.call( apiInst, context[i], this[i], i );
	
					if ( ret !== undefined ) {
						a.push( ret );
					}
				}
				else if ( type === 'column' || type === 'column-rows' || type === 'row' || type === 'cell' ) {
					// columns and rows share the same structure.
					// 'this' is an array of column indexes for each context
					items = this[i];
	
					if ( type === 'column-rows' ) {
						rows = _selector_row_indexes( context[i], selector.opts );
					}
	
					for ( j=0, jen=items.length ; j<jen ; j++ ) {
						item = items[j];
	
						if ( type === 'cell' ) {
							ret = fn.call( apiInst, context[i], item.row, item.column, i, j );
						}
						else {
							ret = fn.call( apiInst, context[i], item, i, j, rows );
						}
	
						if ( ret !== undefined ) {
							a.push( ret );
						}
					}
				}
			}
	
			if ( a.length || alwaysNew ) {
				var api = new _Api( context, flatten ? a.concat.apply( [], a ) : a );
				var apiSelector = api.selector;
				apiSelector.rows = selector.rows;
				apiSelector.cols = selector.cols;
				apiSelector.opts = selector.opts;
				return api;
			}
			return this;
		},
	
	
		lastIndexOf: __arrayProto.lastIndexOf || function (obj, start)
		{
			// Bit cheeky...
			return this.indexOf.apply( this.toArray.reverse(), arguments );
		},
	
	
		length:  0,
	
	
		map: function ( fn )
		{
			var a = [];
	
			if ( __arrayProto.map ) {
				a = __arrayProto.map.call( this, fn, this );
			}
			else {
				// Compatibility for browsers without EMCA-252-5 (JS 1.6)
				for ( var i=0, ien=this.length ; i<ien ; i++ ) {
					a.push( fn.call( this, this[i], i ) );
				}
			}
	
			return new _Api( this.context, a );
		},
	
	
		pluck: function ( prop )
		{
			return this.map( function ( el ) {
				return el[ prop ];
			} );
		},
	
		pop:     __arrayProto.pop,
	
	
		push:    __arrayProto.push,
	
	
		// Does not return an API instance
		reduce: __arrayProto.reduce || function ( fn, init )
		{
			return _fnReduce( this, fn, init, 0, this.length, 1 );
		},
	
	
		reduceRight: __arrayProto.reduceRight || function ( fn, init )
		{
			return _fnReduce( this, fn, init, this.length-1, -1, -1 );
		},
	
	
		reverse: __arrayProto.reverse,
	
	
		// Object with rows, columns and opts
		selector: null,
	
	
		shift:   __arrayProto.shift,
	
	
		slice: function () {
			return new _Api( this.context, this );
		},
	
	
		sort:    __arrayProto.sort, // ? name - order?
	
	
		splice:  __arrayProto.splice,
	
	
		toArray: function ()
		{
			return __arrayProto.slice.call( this );
		},
	
	
		to$: function ()
		{
			return $( this );
		},
	
	
		toJQuery: function ()
		{
			return $( this );
		},
	
	
		unique: function ()
		{
			return new _Api( this.context, _unique(this) );
		},
	
	
		unshift: __arrayProto.unshift
	} );
	
	
	_Api.extend = function ( scope, obj, ext )
	{
		// Only extend API instances and static properties of the API
		if ( ! ext.length || ! obj || ( ! (obj instanceof _Api) && ! obj.__dt_wrapper ) ) {
			return;
		}
	
		var
			i, ien,
			j, jen,
			struct, inner,
			methodScoping = function ( scope, fn, struc ) {
				return function () {
					var ret = fn.apply( scope, arguments );
	
					// Method extension
					_Api.extend( ret, ret, struc.methodExt );
					return ret;
				};
			};
	
		for ( i=0, ien=ext.length ; i<ien ; i++ ) {
			struct = ext[i];
	
			// Value
			obj[ struct.name ] = typeof struct.val === 'function' ?
				methodScoping( scope, struct.val, struct ) :
				$.isPlainObject( struct.val ) ?
					{} :
					struct.val;
	
			obj[ struct.name ].__dt_wrapper = true;
	
			// Property extension
			_Api.extend( scope, obj[ struct.name ], struct.propExt );
		}
	};
	
	
	// @todo - Is there need for an augment function?
	// _Api.augment = function ( inst, name )
	// {
	// 	// Find src object in the structure from the name
	// 	var parts = name.split('.');
	
	// 	_Api.extend( inst, obj );
	// };
	
	
	//     [
	//       {
	//         name:      'data'                -- string   - Property name
	//         val:       function () {},       -- function - Api method (or undefined if just an object
	//         methodExt: [ ... ],              -- array    - Array of Api object definitions to extend the method result
	//         propExt:   [ ... ]               -- array    - Array of Api object definitions to extend the property
	//       },
	//       {
	//         name:     'row'
	//         val:       {},
	//         methodExt: [ ... ],
	//         propExt:   [
	//           {
	//             name:      'data'
	//             val:       function () {},
	//             methodExt: [ ... ],
	//             propExt:   [ ... ]
	//           },
	//           ...
	//         ]
	//       }
	//     ]
	
	_Api.register = _api_register = function ( name, val )
	{
		if ( $.isArray( name ) ) {
			for ( var j=0, jen=name.length ; j<jen ; j++ ) {
				_Api.register( name[j], val );
			}
			return;
		}
	
		var
			i, ien,
			heir = name.split('.'),
			struct = __apiStruct,
			key, method;
	
		var find = function ( src, name ) {
			for ( var i=0, ien=src.length ; i<ien ; i++ ) {
				if ( src[i].name === name ) {
					return src[i];
				}
			}
			return null;
		};
	
		for ( i=0, ien=heir.length ; i<ien ; i++ ) {
			method = heir[i].indexOf('()') !== -1;
			key = method ?
				heir[i].replace('()', '') :
				heir[i];
	
			var src = find( struct, key );
			if ( ! src ) {
				src = {
					name:      key,
					val:       {},
					methodExt: [],
					propExt:   []
				};
				struct.push( src );
			}
	
			if ( i === ien-1 ) {
				src.val = val;
			}
			else {
				struct = method ?
					src.methodExt :
					src.propExt;
			}
		}
	};
	
	
	_Api.registerPlural = _api_registerPlural = function ( pluralName, singularName, val ) {
		_Api.register( pluralName, val );
	
		_Api.register( singularName, function () {
			var ret = val.apply( this, arguments );
	
			if ( ret === this ) {
				// Returned item is the API instance that was passed in, return it
				return this;
			}
			else if ( ret instanceof _Api ) {
				// New API instance returned, want the value from the first item
				// in the returned array for the singular result.
				return ret.length ?
					$.isArray( ret[0] ) ?
						new _Api( ret.context, ret[0] ) : // Array results are 'enhanced'
						ret[0] :
					undefined;
			}
	
			// Non-API return - just fire it back
			return ret;
		} );
	};
	
	
	/**
	 * Selector for HTML tables. Apply the given selector to the give array of
	 * DataTables settings objects.
	 *
	 * @param {string|integer} [selector] jQuery selector string or integer
	 * @param  {array} Array of DataTables settings objects to be filtered
	 * @return {array}
	 * @ignore
	 */
	var __table_selector = function ( selector, a )
	{
		// Integer is used to pick out a table by index
		if ( typeof selector === 'number' ) {
			return [ a[ selector ] ];
		}
	
		// Perform a jQuery selector on the table nodes
		var nodes = $.map( a, function (el, i) {
			return el.nTable;
		} );
	
		return $(nodes)
			.filter( selector )
			.map( function (i) {
				// Need to translate back from the table node to the settings
				var idx = $.inArray( this, nodes );
				return a[ idx ];
			} )
			.toArray();
	};
	
	
	
	/**
	 * Context selector for the API's context (i.e. the tables the API instance
	 * refers to.
	 *
	 * @name    DataTable.Api#tables
	 * @param {string|integer} [selector] Selector to pick which tables the iterator
	 *   should operate on. If not given, all tables in the current context are
	 *   used. This can be given as a jQuery selector (for example `':gt(0)'`) to
	 *   select multiple tables or as an integer to select a single table.
	 * @returns {DataTable.Api} Returns a new API instance if a selector is given.
	 */
	_api_register( 'tables()', function ( selector ) {
		// A new instance is created if there was a selector specified
		return selector ?
			new _Api( __table_selector( selector, this.context ) ) :
			this;
	} );
	
	
	_api_register( 'table()', function ( selector ) {
		var tables = this.tables( selector );
		var ctx = tables.context;
	
		// Truncate to the first matched table
		return ctx.length ?
			new _Api( ctx[0] ) :
			tables;
	} );
	
	
	_api_registerPlural( 'tables().nodes()', 'table().node()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTable;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().body()', 'table().body()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTBody;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().header()', 'table().header()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTHead;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().footer()', 'table().footer()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTFoot;
		}, 1 );
	} );
	
	
	_api_registerPlural( 'tables().containers()', 'table().container()' , function () {
		return this.iterator( 'table', function ( ctx ) {
			return ctx.nTableWrapper;
		}, 1 );
	} );
	
	
	
	/**
	 * Redraw the tables in the current context.
	 */
	_api_register( 'draw()', function ( paging ) {
		return this.iterator( 'table', function ( settings ) {
			if ( paging === 'page' ) {
				_fnDraw( settings );
			}
			else {
				if ( typeof paging === 'string' ) {
					paging = paging === 'full-hold' ?
						false :
						true;
				}
	
				_fnReDraw( settings, paging===false );
			}
		} );
	} );
	
	
	
	/**
	 * Get the current page index.
	 *
	 * @return {integer} Current page index (zero based)
	 *//**
	 * Set the current page.
	 *
	 * Note that if you attempt to show a page which does not exist, DataTables will
	 * not throw an error, but rather reset the paging.
	 *
	 * @param {integer|string} action The paging action to take. This can be one of:
	 *  * `integer` - The page index to jump to
	 *  * `string` - An action to take:
	 *    * `first` - Jump to first page.
	 *    * `next` - Jump to the next page
	 *    * `previous` - Jump to previous page
	 *    * `last` - Jump to the last page.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'page()', function ( action ) {
		if ( action === undefined ) {
			return this.page.info().page; // not an expensive call
		}
	
		// else, have an action to take on all tables
		return this.iterator( 'table', function ( settings ) {
			_fnPageChange( settings, action );
		} );
	} );
	
	
	/**
	 * Paging information for the first table in the current context.
	 *
	 * If you require paging information for another table, use the `table()` method
	 * with a suitable selector.
	 *
	 * @return {object} Object with the following properties set:
	 *  * `page` - Current page index (zero based - i.e. the first page is `0`)
	 *  * `pages` - Total number of pages
	 *  * `start` - Display index for the first record shown on the current page
	 *  * `end` - Display index for the last record shown on the current page
	 *  * `length` - Display length (number of records). Note that generally `start
	 *    + length = end`, but this is not always true, for example if there are
	 *    only 2 records to show on the final page, with a length of 10.
	 *  * `recordsTotal` - Full data set length
	 *  * `recordsDisplay` - Data set length once the current filtering criterion
	 *    are applied.
	 */
	_api_register( 'page.info()', function ( action ) {
		if ( this.context.length === 0 ) {
			return undefined;
		}
	
		var
			settings   = this.context[0],
			start      = settings._iDisplayStart,
			len        = settings.oFeatures.bPaginate ? settings._iDisplayLength : -1,
			visRecords = settings.fnRecordsDisplay(),
			all        = len === -1;
	
		return {
			"page":           all ? 0 : Math.floor( start / len ),
			"pages":          all ? 1 : Math.ceil( visRecords / len ),
			"start":          start,
			"end":            settings.fnDisplayEnd(),
			"length":         len,
			"recordsTotal":   settings.fnRecordsTotal(),
			"recordsDisplay": visRecords,
			"serverSide":     _fnDataSource( settings ) === 'ssp'
		};
	} );
	
	
	/**
	 * Get the current page length.
	 *
	 * @return {integer} Current page length. Note `-1` indicates that all records
	 *   are to be shown.
	 *//**
	 * Set the current page length.
	 *
	 * @param {integer} Page length to set. Use `-1` to show all records.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'page.len()', function ( len ) {
		// Note that we can't call this function 'length()' because `length`
		// is a Javascript property of functions which defines how many arguments
		// the function expects.
		if ( len === undefined ) {
			return this.context.length !== 0 ?
				this.context[0]._iDisplayLength :
				undefined;
		}
	
		// else, set the page length
		return this.iterator( 'table', function ( settings ) {
			_fnLengthChange( settings, len );
		} );
	} );
	
	
	
	var __reload = function ( settings, holdPosition, callback ) {
		// Use the draw event to trigger a callback
		if ( callback ) {
			var api = new _Api( settings );
	
			api.one( 'draw', function () {
				callback( api.ajax.json() );
			} );
		}
	
		if ( _fnDataSource( settings ) == 'ssp' ) {
			_fnReDraw( settings, holdPosition );
		}
		else {
			_fnProcessingDisplay( settings, true );
	
			// Cancel an existing request
			var xhr = settings.jqXHR;
			if ( xhr && xhr.readyState !== 4 ) {
				xhr.abort();
			}
	
			// Trigger xhr
			_fnBuildAjax( settings, [], function( json ) {
				_fnClearTable( settings );
	
				var data = _fnAjaxDataSrc( settings, json );
				for ( var i=0, ien=data.length ; i<ien ; i++ ) {
					_fnAddData( settings, data[i] );
				}
	
				_fnReDraw( settings, holdPosition );
				_fnProcessingDisplay( settings, false );
			} );
		}
	};
	
	
	/**
	 * Get the JSON response from the last Ajax request that DataTables made to the
	 * server. Note that this returns the JSON from the first table in the current
	 * context.
	 *
	 * @return {object} JSON received from the server.
	 */
	_api_register( 'ajax.json()', function () {
		var ctx = this.context;
	
		if ( ctx.length > 0 ) {
			return ctx[0].json;
		}
	
		// else return undefined;
	} );
	
	
	/**
	 * Get the data submitted in the last Ajax request
	 */
	_api_register( 'ajax.params()', function () {
		var ctx = this.context;
	
		if ( ctx.length > 0 ) {
			return ctx[0].oAjaxData;
		}
	
		// else return undefined;
	} );
	
	
	/**
	 * Reload tables from the Ajax data source. Note that this function will
	 * automatically re-draw the table when the remote data has been loaded.
	 *
	 * @param {boolean} [reset=true] Reset (default) or hold the current paging
	 *   position. A full re-sort and re-filter is performed when this method is
	 *   called, which is why the pagination reset is the default action.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'ajax.reload()', function ( callback, resetPaging ) {
		return this.iterator( 'table', function (settings) {
			__reload( settings, resetPaging===false, callback );
		} );
	} );
	
	
	/**
	 * Get the current Ajax URL. Note that this returns the URL from the first
	 * table in the current context.
	 *
	 * @return {string} Current Ajax source URL
	 *//**
	 * Set the Ajax URL. Note that this will set the URL for all tables in the
	 * current context.
	 *
	 * @param {string} url URL to set.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'ajax.url()', function ( url ) {
		var ctx = this.context;
	
		if ( url === undefined ) {
			// get
			if ( ctx.length === 0 ) {
				return undefined;
			}
			ctx = ctx[0];
	
			return ctx.ajax ?
				$.isPlainObject( ctx.ajax ) ?
					ctx.ajax.url :
					ctx.ajax :
				ctx.sAjaxSource;
		}
	
		// set
		return this.iterator( 'table', function ( settings ) {
			if ( $.isPlainObject( settings.ajax ) ) {
				settings.ajax.url = url;
			}
			else {
				settings.ajax = url;
			}
			// No need to consider sAjaxSource here since DataTables gives priority
			// to `ajax` over `sAjaxSource`. So setting `ajax` here, renders any
			// value of `sAjaxSource` redundant.
		} );
	} );
	
	
	/**
	 * Load data from the newly set Ajax URL. Note that this method is only
	 * available when `ajax.url()` is used to set a URL. Additionally, this method
	 * has the same effect as calling `ajax.reload()` but is provided for
	 * convenience when setting a new URL. Like `ajax.reload()` it will
	 * automatically redraw the table once the remote data has been loaded.
	 *
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'ajax.url().load()', function ( callback, resetPaging ) {
		// Same as a reload, but makes sense to present it for easy access after a
		// url change
		return this.iterator( 'table', function ( ctx ) {
			__reload( ctx, resetPaging===false, callback );
		} );
	} );
	
	
	
	
	var _selector_run = function ( type, selector, selectFn, settings, opts )
	{
		var
			out = [], res,
			a, i, ien, j, jen,
			selectorType = typeof selector;
	
		// Can't just check for isArray here, as an API or jQuery instance might be
		// given with their array like look
		if ( ! selector || selectorType === 'string' || selectorType === 'function' || selector.length === undefined ) {
			selector = [ selector ];
		}
	
		for ( i=0, ien=selector.length ; i<ien ; i++ ) {
			// Only split on simple strings - complex expressions will be jQuery selectors
			a = selector[i] && selector[i].split && ! selector[i].match(/[\[\(:]/) ?
				selector[i].split(',') :
				[ selector[i] ];
	
			for ( j=0, jen=a.length ; j<jen ; j++ ) {
				res = selectFn( typeof a[j] === 'string' ? $.trim(a[j]) : a[j] );
	
				if ( res && res.length ) {
					out = out.concat( res );
				}
			}
		}
	
		// selector extensions
		var ext = _ext.selector[ type ];
		if ( ext.length ) {
			for ( i=0, ien=ext.length ; i<ien ; i++ ) {
				out = ext[i]( settings, opts, out );
			}
		}
	
		return _unique( out );
	};
	
	
	var _selector_opts = function ( opts )
	{
		if ( ! opts ) {
			opts = {};
		}
	
		// Backwards compatibility for 1.9- which used the terminology filter rather
		// than search
		if ( opts.filter && opts.search === undefined ) {
			opts.search = opts.filter;
		}
	
		return $.extend( {
			search: 'none',
			order: 'current',
			page: 'all'
		}, opts );
	};
	
	
	var _selector_first = function ( inst )
	{
		// Reduce the API instance to the first item found
		for ( var i=0, ien=inst.length ; i<ien ; i++ ) {
			if ( inst[i].length > 0 ) {
				// Assign the first element to the first item in the instance
				// and truncate the instance and context
				inst[0] = inst[i];
				inst[0].length = 1;
				inst.length = 1;
				inst.context = [ inst.context[i] ];
	
				return inst;
			}
		}
	
		// Not found - return an empty instance
		inst.length = 0;
		return inst;
	};
	
	
	var _selector_row_indexes = function ( settings, opts )
	{
		var
			i, ien, tmp, a=[],
			displayFiltered = settings.aiDisplay,
			displayMaster = settings.aiDisplayMaster;
	
		var
			search = opts.search,  // none, applied, removed
			order  = opts.order,   // applied, current, index (original - compatibility with 1.9)
			page   = opts.page;    // all, current
	
		if ( _fnDataSource( settings ) == 'ssp' ) {
			// In server-side processing mode, most options are irrelevant since
			// rows not shown don't exist and the index order is the applied order
			// Removed is a special case - for consistency just return an empty
			// array
			return search === 'removed' ?
				[] :
				_range( 0, displayMaster.length );
		}
		else if ( page == 'current' ) {
			// Current page implies that order=current and fitler=applied, since it is
			// fairly senseless otherwise, regardless of what order and search actually
			// are
			for ( i=settings._iDisplayStart, ien=settings.fnDisplayEnd() ; i<ien ; i++ ) {
				a.push( displayFiltered[i] );
			}
		}
		else if ( order == 'current' || order == 'applied' ) {
			if ( search == 'none') {
				a = displayMaster.slice();
			}
			else if ( search == 'applied' ) {
				a = displayFiltered.slice();
			}
			else if ( search == 'removed' ) {
				// O(n+m) solution by creating a hash map
				var displayFilteredMap = {};
	
				for ( var i=0, ien=displayFiltered.length ; i<ien ; i++ ) {
					displayFilteredMap[displayFiltered[i]] = null;
				}
	
				a = $.map( displayMaster, function (el) {
					return ! displayFilteredMap.hasOwnProperty(el) ?
						el :
						null;
				} );
			}
		}
		else if ( order == 'index' || order == 'original' ) {
			for ( i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
				if ( search == 'none' ) {
					a.push( i );
				}
				else { // applied | removed
					tmp = $.inArray( i, displayFiltered );
	
					if ((tmp === -1 && search == 'removed') ||
						(tmp >= 0   && search == 'applied') )
					{
						a.push( i );
					}
				}
			}
		}
	
		return a;
	};
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Rows
	 *
	 * {}          - no selector - use all available rows
	 * {integer}   - row aoData index
	 * {node}      - TR node
	 * {string}    - jQuery selector to apply to the TR elements
	 * {array}     - jQuery array of nodes, or simply an array of TR nodes
	 *
	 */
	var __row_selector = function ( settings, selector, opts )
	{
		var rows;
		var run = function ( sel ) {
			var selInt = _intVal( sel );
			var i, ien;
			var aoData = settings.aoData;
	
			// Short cut - selector is a number and no options provided (default is
			// all records, so no need to check if the index is in there, since it
			// must be - dev error if the index doesn't exist).
			if ( selInt !== null && ! opts ) {
				return [ selInt ];
			}
	
			if ( ! rows ) {
				rows = _selector_row_indexes( settings, opts );
			}
	
			if ( selInt !== null && $.inArray( selInt, rows ) !== -1 ) {
				// Selector - integer
				return [ selInt ];
			}
			else if ( sel === null || sel === undefined || sel === '' ) {
				// Selector - none
				return rows;
			}
	
			// Selector - function
			if ( typeof sel === 'function' ) {
				return $.map( rows, function (idx) {
					var row = aoData[ idx ];
					return sel( idx, row._aData, row.nTr ) ? idx : null;
				} );
			}
	
			// Selector - node
			if ( sel.nodeName ) {
				var rowIdx = sel._DT_RowIndex;  // Property added by DT for fast lookup
				var cellIdx = sel._DT_CellIndex;
	
				if ( rowIdx !== undefined ) {
					// Make sure that the row is actually still present in the table
					return aoData[ rowIdx ] && aoData[ rowIdx ].nTr === sel ?
						[ rowIdx ] :
						[];
				}
				else if ( cellIdx ) {
					return aoData[ cellIdx.row ] && aoData[ cellIdx.row ].nTr === sel ?
						[ cellIdx.row ] :
						[];
				}
				else {
					var host = $(sel).closest('*[data-dt-row]');
					return host.length ?
						[ host.data('dt-row') ] :
						[];
				}
			}
	
			// ID selector. Want to always be able to select rows by id, regardless
			// of if the tr element has been created or not, so can't rely upon
			// jQuery here - hence a custom implementation. This does not match
			// Sizzle's fast selector or HTML4 - in HTML5 the ID can be anything,
			// but to select it using a CSS selector engine (like Sizzle or
			// querySelect) it would need to need to be escaped for some characters.
			// DataTables simplifies this for row selectors since you can select
			// only a row. A # indicates an id any anything that follows is the id -
			// unescaped.
			if ( typeof sel === 'string' && sel.charAt(0) === '#' ) {
				// get row index from id
				var rowObj = settings.aIds[ sel.replace( /^#/, '' ) ];
				if ( rowObj !== undefined ) {
					return [ rowObj.idx ];
				}
	
				// need to fall through to jQuery in case there is DOM id that
				// matches
			}
			
			// Get nodes in the order from the `rows` array with null values removed
			var nodes = _removeEmpty(
				_pluck_order( settings.aoData, rows, 'nTr' )
			);
	
			// Selector - jQuery selector string, array of nodes or jQuery object/
			// As jQuery's .filter() allows jQuery objects to be passed in filter,
			// it also allows arrays, so this will cope with all three options
			return $(nodes)
				.filter( sel )
				.map( function () {
					return this._DT_RowIndex;
				} )
				.toArray();
		};
	
		return _selector_run( 'row', selector, run, settings, opts );
	};
	
	
	_api_register( 'rows()', function ( selector, opts ) {
		// argument shifting
		if ( selector === undefined ) {
			selector = '';
		}
		else if ( $.isPlainObject( selector ) ) {
			opts = selector;
			selector = '';
		}
	
		opts = _selector_opts( opts );
	
		var inst = this.iterator( 'table', function ( settings ) {
			return __row_selector( settings, selector, opts );
		}, 1 );
	
		// Want argument shifting here and in __row_selector?
		inst.selector.rows = selector;
		inst.selector.opts = opts;
	
		return inst;
	} );
	
	_api_register( 'rows().nodes()', function () {
		return this.iterator( 'row', function ( settings, row ) {
			return settings.aoData[ row ].nTr || undefined;
		}, 1 );
	} );
	
	_api_register( 'rows().data()', function () {
		return this.iterator( true, 'rows', function ( settings, rows ) {
			return _pluck_order( settings.aoData, rows, '_aData' );
		}, 1 );
	} );
	
	_api_registerPlural( 'rows().cache()', 'row().cache()', function ( type ) {
		return this.iterator( 'row', function ( settings, row ) {
			var r = settings.aoData[ row ];
			return type === 'search' ? r._aFilterData : r._aSortData;
		}, 1 );
	} );
	
	_api_registerPlural( 'rows().invalidate()', 'row().invalidate()', function ( src ) {
		return this.iterator( 'row', function ( settings, row ) {
			_fnInvalidate( settings, row, src );
		} );
	} );
	
	_api_registerPlural( 'rows().indexes()', 'row().index()', function () {
		return this.iterator( 'row', function ( settings, row ) {
			return row;
		}, 1 );
	} );
	
	_api_registerPlural( 'rows().ids()', 'row().id()', function ( hash ) {
		var a = [];
		var context = this.context;
	
		// `iterator` will drop undefined values, but in this case we want them
		for ( var i=0, ien=context.length ; i<ien ; i++ ) {
			for ( var j=0, jen=this[i].length ; j<jen ; j++ ) {
				var id = context[i].rowIdFn( context[i].aoData[ this[i][j] ]._aData );
				a.push( (hash === true ? '#' : '' )+ id );
			}
		}
	
		return new _Api( context, a );
	} );
	
	_api_registerPlural( 'rows().remove()', 'row().remove()', function () {
		var that = this;
	
		this.iterator( 'row', function ( settings, row, thatIdx ) {
			var data = settings.aoData;
			var rowData = data[ row ];
			var i, ien, j, jen;
			var loopRow, loopCells;
	
			data.splice( row, 1 );
	
			// Update the cached indexes
			for ( i=0, ien=data.length ; i<ien ; i++ ) {
				loopRow = data[i];
				loopCells = loopRow.anCells;
	
				// Rows
				if ( loopRow.nTr !== null ) {
					loopRow.nTr._DT_RowIndex = i;
				}
	
				// Cells
				if ( loopCells !== null ) {
					for ( j=0, jen=loopCells.length ; j<jen ; j++ ) {
						loopCells[j]._DT_CellIndex.row = i;
					}
				}
			}
	
			// Delete from the display arrays
			_fnDeleteIndex( settings.aiDisplayMaster, row );
			_fnDeleteIndex( settings.aiDisplay, row );
			_fnDeleteIndex( that[ thatIdx ], row, false ); // maintain local indexes
	
			// For server-side processing tables - subtract the deleted row from the count
			if ( settings._iRecordsDisplay > 0 ) {
				settings._iRecordsDisplay--;
			}
	
			// Check for an 'overflow' they case for displaying the table
			_fnLengthOverflow( settings );
	
			// Remove the row's ID reference if there is one
			var id = settings.rowIdFn( rowData._aData );
			if ( id !== undefined ) {
				delete settings.aIds[ id ];
			}
		} );
	
		this.iterator( 'table', function ( settings ) {
			for ( var i=0, ien=settings.aoData.length ; i<ien ; i++ ) {
				settings.aoData[i].idx = i;
			}
		} );
	
		return this;
	} );
	
	
	_api_register( 'rows.add()', function ( rows ) {
		var newRows = this.iterator( 'table', function ( settings ) {
				var row, i, ien;
				var out = [];
	
				for ( i=0, ien=rows.length ; i<ien ; i++ ) {
					row = rows[i];
	
					if ( row.nodeName && row.nodeName.toUpperCase() === 'TR' ) {
						out.push( _fnAddTr( settings, row )[0] );
					}
					else {
						out.push( _fnAddData( settings, row ) );
					}
				}
	
				return out;
			}, 1 );
	
		// Return an Api.rows() extended instance, so rows().nodes() etc can be used
		var modRows = this.rows( -1 );
		modRows.pop();
		$.merge( modRows, newRows );
	
		return modRows;
	} );
	
	
	
	
	
	/**
	 *
	 */
	_api_register( 'row()', function ( selector, opts ) {
		return _selector_first( this.rows( selector, opts ) );
	} );
	
	
	_api_register( 'row().data()', function ( data ) {
		var ctx = this.context;
	
		if ( data === undefined ) {
			// Get
			return ctx.length && this.length ?
				ctx[0].aoData[ this[0] ]._aData :
				undefined;
		}
	
		// Set
		var row = ctx[0].aoData[ this[0] ];
		row._aData = data;
	
		// If the DOM has an id, and the data source is an array
		if ( $.isArray( data ) && row.nTr.id ) {
			_fnSetObjectDataFn( ctx[0].rowId )( data, row.nTr.id );
		}
	
		// Automatically invalidate
		_fnInvalidate( ctx[0], this[0], 'data' );
	
		return this;
	} );
	
	
	_api_register( 'row().node()', function () {
		var ctx = this.context;
	
		return ctx.length && this.length ?
			ctx[0].aoData[ this[0] ].nTr || null :
			null;
	} );
	
	
	_api_register( 'row.add()', function ( row ) {
		// Allow a jQuery object to be passed in - only a single row is added from
		// it though - the first element in the set
		if ( row instanceof $ && row.length ) {
			row = row[0];
		}
	
		var rows = this.iterator( 'table', function ( settings ) {
			if ( row.nodeName && row.nodeName.toUpperCase() === 'TR' ) {
				return _fnAddTr( settings, row )[0];
			}
			return _fnAddData( settings, row );
		} );
	
		// Return an Api.rows() extended instance, with the newly added row selected
		return this.row( rows[0] );
	} );
	
	
	
	var __details_add = function ( ctx, row, data, klass )
	{
		// Convert to array of TR elements
		var rows = [];
		var addRow = function ( r, k ) {
			// Recursion to allow for arrays of jQuery objects
			if ( $.isArray( r ) || r instanceof $ ) {
				for ( var i=0, ien=r.length ; i<ien ; i++ ) {
					addRow( r[i], k );
				}
				return;
			}
	
			// If we get a TR element, then just add it directly - up to the dev
			// to add the correct number of columns etc
			if ( r.nodeName && r.nodeName.toLowerCase() === 'tr' ) {
				rows.push( r );
			}
			else {
				// Otherwise create a row with a wrapper
				var created = $('<tr><td/></tr>').addClass( k );
				$('td', created)
					.addClass( k )
					.html( r )
					[0].colSpan = _fnVisbleColumns( ctx );
	
				rows.push( created[0] );
			}
		};
	
		addRow( data, klass );
	
		if ( row._details ) {
			row._details.detach();
		}
	
		row._details = $(rows);
	
		// If the children were already shown, that state should be retained
		if ( row._detailsShow ) {
			row._details.insertAfter( row.nTr );
		}
	};
	
	
	var __details_remove = function ( api, idx )
	{
		var ctx = api.context;
	
		if ( ctx.length ) {
			var row = ctx[0].aoData[ idx !== undefined ? idx : api[0] ];
	
			if ( row && row._details ) {
				row._details.remove();
	
				row._detailsShow = undefined;
				row._details = undefined;
			}
		}
	};
	
	
	var __details_display = function ( api, show ) {
		var ctx = api.context;
	
		if ( ctx.length && api.length ) {
			var row = ctx[0].aoData[ api[0] ];
	
			if ( row._details ) {
				row._detailsShow = show;
	
				if ( show ) {
					row._details.insertAfter( row.nTr );
				}
				else {
					row._details.detach();
				}
	
				__details_events( ctx[0] );
			}
		}
	};
	
	
	var __details_events = function ( settings )
	{
		var api = new _Api( settings );
		var namespace = '.dt.DT_details';
		var drawEvent = 'draw'+namespace;
		var colvisEvent = 'column-visibility'+namespace;
		var destroyEvent = 'destroy'+namespace;
		var data = settings.aoData;
	
		api.off( drawEvent +' '+ colvisEvent +' '+ destroyEvent );
	
		if ( _pluck( data, '_details' ).length > 0 ) {
			// On each draw, insert the required elements into the document
			api.on( drawEvent, function ( e, ctx ) {
				if ( settings !== ctx ) {
					return;
				}
	
				api.rows( {page:'current'} ).eq(0).each( function (idx) {
					// Internal data grab
					var row = data[ idx ];
	
					if ( row._detailsShow ) {
						row._details.insertAfter( row.nTr );
					}
				} );
			} );
	
			// Column visibility change - update the colspan
			api.on( colvisEvent, function ( e, ctx, idx, vis ) {
				if ( settings !== ctx ) {
					return;
				}
	
				// Update the colspan for the details rows (note, only if it already has
				// a colspan)
				var row, visible = _fnVisbleColumns( ctx );
	
				for ( var i=0, ien=data.length ; i<ien ; i++ ) {
					row = data[i];
	
					if ( row._details ) {
						row._details.children('td[colspan]').attr('colspan', visible );
					}
				}
			} );
	
			// Table destroyed - nuke any child rows
			api.on( destroyEvent, function ( e, ctx ) {
				if ( settings !== ctx ) {
					return;
				}
	
				for ( var i=0, ien=data.length ; i<ien ; i++ ) {
					if ( data[i]._details ) {
						__details_remove( api, i );
					}
				}
			} );
		}
	};
	
	// Strings for the method names to help minification
	var _emp = '';
	var _child_obj = _emp+'row().child';
	var _child_mth = _child_obj+'()';
	
	// data can be:
	//  tr
	//  string
	//  jQuery or array of any of the above
	_api_register( _child_mth, function ( data, klass ) {
		var ctx = this.context;
	
		if ( data === undefined ) {
			// get
			return ctx.length && this.length ?
				ctx[0].aoData[ this[0] ]._details :
				undefined;
		}
		else if ( data === true ) {
			// show
			this.child.show();
		}
		else if ( data === false ) {
			// remove
			__details_remove( this );
		}
		else if ( ctx.length && this.length ) {
			// set
			__details_add( ctx[0], ctx[0].aoData[ this[0] ], data, klass );
		}
	
		return this;
	} );
	
	
	_api_register( [
		_child_obj+'.show()',
		_child_mth+'.show()' // only when `child()` was called with parameters (without
	], function ( show ) {   // it returns an object and this method is not executed)
		__details_display( this, true );
		return this;
	} );
	
	
	_api_register( [
		_child_obj+'.hide()',
		_child_mth+'.hide()' // only when `child()` was called with parameters (without
	], function () {         // it returns an object and this method is not executed)
		__details_display( this, false );
		return this;
	} );
	
	
	_api_register( [
		_child_obj+'.remove()',
		_child_mth+'.remove()' // only when `child()` was called with parameters (without
	], function () {           // it returns an object and this method is not executed)
		__details_remove( this );
		return this;
	} );
	
	
	_api_register( _child_obj+'.isShown()', function () {
		var ctx = this.context;
	
		if ( ctx.length && this.length ) {
			// _detailsShown as false or undefined will fall through to return false
			return ctx[0].aoData[ this[0] ]._detailsShow || false;
		}
		return false;
	} );
	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Columns
	 *
	 * {integer}           - column index (>=0 count from left, <0 count from right)
	 * "{integer}:visIdx"  - visible column index (i.e. translate to column index)  (>=0 count from left, <0 count from right)
	 * "{integer}:visible" - alias for {integer}:visIdx  (>=0 count from left, <0 count from right)
	 * "{string}:name"     - column name
	 * "{string}"          - jQuery selector on column header nodes
	 *
	 */
	
	// can be an array of these items, comma separated list, or an array of comma
	// separated lists
	
	var __re_column_selector = /^([^:]+):(name|visIdx|visible)$/;
	
	
	// r1 and r2 are redundant - but it means that the parameters match for the
	// iterator callback in columns().data()
	var __columnData = function ( settings, column, r1, r2, rows ) {
		var a = [];
		for ( var row=0, ien=rows.length ; row<ien ; row++ ) {
			a.push( _fnGetCellData( settings, rows[row], column ) );
		}
		return a;
	};
	
	
	var __column_selector = function ( settings, selector, opts )
	{
		var
			columns = settings.aoColumns,
			names = _pluck( columns, 'sName' ),
			nodes = _pluck( columns, 'nTh' );
	
		var run = function ( s ) {
			var selInt = _intVal( s );
	
			// Selector - all
			if ( s === '' ) {
				return _range( columns.length );
			}
	
			// Selector - index
			if ( selInt !== null ) {
				return [ selInt >= 0 ?
					selInt : // Count from left
					columns.length + selInt // Count from right (+ because its a negative value)
				];
			}
	
			// Selector = function
			if ( typeof s === 'function' ) {
				var rows = _selector_row_indexes( settings, opts );
	
				return $.map( columns, function (col, idx) {
					return s(
							idx,
							__columnData( settings, idx, 0, 0, rows ),
							nodes[ idx ]
						) ? idx : null;
				} );
			}
	
			// jQuery or string selector
			var match = typeof s === 'string' ?
				s.match( __re_column_selector ) :
				'';
	
			if ( match ) {
				switch( match[2] ) {
					case 'visIdx':
					case 'visible':
						var idx = parseInt( match[1], 10 );
						// Visible index given, convert to column index
						if ( idx < 0 ) {
							// Counting from the right
							var visColumns = $.map( columns, function (col,i) {
								return col.bVisible ? i : null;
							} );
							return [ visColumns[ visColumns.length + idx ] ];
						}
						// Counting from the left
						return [ _fnVisibleToColumnIndex( settings, idx ) ];
	
					case 'name':
						// match by name. `names` is column index complete and in order
						return $.map( names, function (name, i) {
							return name === match[1] ? i : null;
						} );
	
					default:
						return [];
				}
			}
	
			// Cell in the table body
			if ( s.nodeName && s._DT_CellIndex ) {
				return [ s._DT_CellIndex.column ];
			}
	
			// jQuery selector on the TH elements for the columns
			var jqResult = $( nodes )
				.filter( s )
				.map( function () {
					return $.inArray( this, nodes ); // `nodes` is column index complete and in order
				} )
				.toArray();
	
			if ( jqResult.length || ! s.nodeName ) {
				return jqResult;
			}
	
			// Otherwise a node which might have a `dt-column` data attribute, or be
			// a child or such an element
			var host = $(s).closest('*[data-dt-column]');
			return host.length ?
				[ host.data('dt-column') ] :
				[];
		};
	
		return _selector_run( 'column', selector, run, settings, opts );
	};
	
	
	var __setColumnVis = function ( settings, column, vis ) {
		var
			cols = settings.aoColumns,
			col  = cols[ column ],
			data = settings.aoData,
			row, cells, i, ien, tr;
	
		// Get
		if ( vis === undefined ) {
			return col.bVisible;
		}
	
		// Set
		// No change
		if ( col.bVisible === vis ) {
			return;
		}
	
		if ( vis ) {
			// Insert column
			// Need to decide if we should use appendChild or insertBefore
			var insertBefore = $.inArray( true, _pluck(cols, 'bVisible'), column+1 );
	
			for ( i=0, ien=data.length ; i<ien ; i++ ) {
				tr = data[i].nTr;
				cells = data[i].anCells;
	
				if ( tr ) {
					// insertBefore can act like appendChild if 2nd arg is null
					tr.insertBefore( cells[ column ], cells[ insertBefore ] || null );
				}
			}
		}
		else {
			// Remove column
			$( _pluck( settings.aoData, 'anCells', column ) ).detach();
		}
	
		// Common actions
		col.bVisible = vis;
		_fnDrawHead( settings, settings.aoHeader );
		_fnDrawHead( settings, settings.aoFooter );
	
		// Update colspan for no records display. Child rows and extensions will use their own
		// listeners to do this - only need to update the empty table item here
		if ( ! settings.aiDisplay.length ) {
			$(settings.nTBody).find('td[colspan]').attr('colspan', _fnVisbleColumns(settings));
		}
	
		_fnSaveState( settings );
	};
	
	
	_api_register( 'columns()', function ( selector, opts ) {
		// argument shifting
		if ( selector === undefined ) {
			selector = '';
		}
		else if ( $.isPlainObject( selector ) ) {
			opts = selector;
			selector = '';
		}
	
		opts = _selector_opts( opts );
	
		var inst = this.iterator( 'table', function ( settings ) {
			return __column_selector( settings, selector, opts );
		}, 1 );
	
		// Want argument shifting here and in _row_selector?
		inst.selector.cols = selector;
		inst.selector.opts = opts;
	
		return inst;
	} );
	
	_api_registerPlural( 'columns().header()', 'column().header()', function ( selector, opts ) {
		return this.iterator( 'column', function ( settings, column ) {
			return settings.aoColumns[column].nTh;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().footer()', 'column().footer()', function ( selector, opts ) {
		return this.iterator( 'column', function ( settings, column ) {
			return settings.aoColumns[column].nTf;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().data()', 'column().data()', function () {
		return this.iterator( 'column-rows', __columnData, 1 );
	} );
	
	_api_registerPlural( 'columns().dataSrc()', 'column().dataSrc()', function () {
		return this.iterator( 'column', function ( settings, column ) {
			return settings.aoColumns[column].mData;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().cache()', 'column().cache()', function ( type ) {
		return this.iterator( 'column-rows', function ( settings, column, i, j, rows ) {
			return _pluck_order( settings.aoData, rows,
				type === 'search' ? '_aFilterData' : '_aSortData', column
			);
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().nodes()', 'column().nodes()', function () {
		return this.iterator( 'column-rows', function ( settings, column, i, j, rows ) {
			return _pluck_order( settings.aoData, rows, 'anCells', column ) ;
		}, 1 );
	} );
	
	_api_registerPlural( 'columns().visible()', 'column().visible()', function ( vis, calc ) {
		var ret = this.iterator( 'column', function ( settings, column ) {
			if ( vis === undefined ) {
				return settings.aoColumns[ column ].bVisible;
			} // else
			__setColumnVis( settings, column, vis );
		} );
	
		// Group the column visibility changes
		if ( vis !== undefined ) {
			// Second loop once the first is done for events
			this.iterator( 'column', function ( settings, column ) {
				_fnCallbackFire( settings, null, 'column-visibility', [settings, column, vis, calc] );
			} );
	
			if ( calc === undefined || calc ) {
				this.columns.adjust();
			}
		}
	
		return ret;
	} );
	
	_api_registerPlural( 'columns().indexes()', 'column().index()', function ( type ) {
		return this.iterator( 'column', function ( settings, column ) {
			return type === 'visible' ?
				_fnColumnIndexToVisible( settings, column ) :
				column;
		}, 1 );
	} );
	
	_api_register( 'columns.adjust()', function () {
		return this.iterator( 'table', function ( settings ) {
			_fnAdjustColumnSizing( settings );
		}, 1 );
	} );
	
	_api_register( 'column.index()', function ( type, idx ) {
		if ( this.context.length !== 0 ) {
			var ctx = this.context[0];
	
			if ( type === 'fromVisible' || type === 'toData' ) {
				return _fnVisibleToColumnIndex( ctx, idx );
			}
			else if ( type === 'fromData' || type === 'toVisible' ) {
				return _fnColumnIndexToVisible( ctx, idx );
			}
		}
	} );
	
	_api_register( 'column()', function ( selector, opts ) {
		return _selector_first( this.columns( selector, opts ) );
	} );
	
	
	
	var __cell_selector = function ( settings, selector, opts )
	{
		var data = settings.aoData;
		var rows = _selector_row_indexes( settings, opts );
		var cells = _removeEmpty( _pluck_order( data, rows, 'anCells' ) );
		var allCells = $( [].concat.apply([], cells) );
		var row;
		var columns = settings.aoColumns.length;
		var a, i, ien, j, o, host;
	
		var run = function ( s ) {
			var fnSelector = typeof s === 'function';
	
			if ( s === null || s === undefined || fnSelector ) {
				// All cells and function selectors
				a = [];
	
				for ( i=0, ien=rows.length ; i<ien ; i++ ) {
					row = rows[i];
	
					for ( j=0 ; j<columns ; j++ ) {
						o = {
							row: row,
							column: j
						};
	
						if ( fnSelector ) {
							// Selector - function
							host = data[ row ];
	
							if ( s( o, _fnGetCellData(settings, row, j), host.anCells ? host.anCells[j] : null ) ) {
								a.push( o );
							}
						}
						else {
							// Selector - all
							a.push( o );
						}
					}
				}
	
				return a;
			}
			
			// Selector - index
			if ( $.isPlainObject( s ) ) {
				// Valid cell index and its in the array of selectable rows
				return s.column !== undefined && s.row !== undefined && $.inArray( s.row, rows ) !== -1 ?
					[s] :
					[];
			}
	
			// Selector - jQuery filtered cells
			var jqResult = allCells
				.filter( s )
				.map( function (i, el) {
					return { // use a new object, in case someone changes the values
						row:    el._DT_CellIndex.row,
						column: el._DT_CellIndex.column
	 				};
				} )
				.toArray();
	
			if ( jqResult.length || ! s.nodeName ) {
				return jqResult;
			}
	
			// Otherwise the selector is a node, and there is one last option - the
			// element might be a child of an element which has dt-row and dt-column
			// data attributes
			host = $(s).closest('*[data-dt-row]');
			return host.length ?
				[ {
					row: host.data('dt-row'),
					column: host.data('dt-column')
				} ] :
				[];
		};
	
		return _selector_run( 'cell', selector, run, settings, opts );
	};
	
	
	
	
	_api_register( 'cells()', function ( rowSelector, columnSelector, opts ) {
		// Argument shifting
		if ( $.isPlainObject( rowSelector ) ) {
			// Indexes
			if ( rowSelector.row === undefined ) {
				// Selector options in first parameter
				opts = rowSelector;
				rowSelector = null;
			}
			else {
				// Cell index objects in first parameter
				opts = columnSelector;
				columnSelector = null;
			}
		}
		if ( $.isPlainObject( columnSelector ) ) {
			opts = columnSelector;
			columnSelector = null;
		}
	
		// Cell selector
		if ( columnSelector === null || columnSelector === undefined ) {
			return this.iterator( 'table', function ( settings ) {
				return __cell_selector( settings, rowSelector, _selector_opts( opts ) );
			} );
		}
	
		// Row + column selector
		var columns = this.columns( columnSelector );
		var rows = this.rows( rowSelector );
		var a, i, ien, j, jen;
	
		this.iterator( 'table', function ( settings, idx ) {
			a = [];
	
			for ( i=0, ien=rows[idx].length ; i<ien ; i++ ) {
				for ( j=0, jen=columns[idx].length ; j<jen ; j++ ) {
					a.push( {
						row:    rows[idx][i],
						column: columns[idx][j]
					} );
				}
			}
		}, 1 );
	
	    // Now pass through the cell selector for options
	    var cells = this.cells( a, opts );
	
		$.extend( cells.selector, {
			cols: columnSelector,
			rows: rowSelector,
			opts: opts
		} );
	
		return cells;
	} );
	
	
	_api_registerPlural( 'cells().nodes()', 'cell().node()', function () {
		return this.iterator( 'cell', function ( settings, row, column ) {
			var data = settings.aoData[ row ];
	
			return data && data.anCells ?
				data.anCells[ column ] :
				undefined;
		}, 1 );
	} );
	
	
	_api_register( 'cells().data()', function () {
		return this.iterator( 'cell', function ( settings, row, column ) {
			return _fnGetCellData( settings, row, column );
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().cache()', 'cell().cache()', function ( type ) {
		type = type === 'search' ? '_aFilterData' : '_aSortData';
	
		return this.iterator( 'cell', function ( settings, row, column ) {
			return settings.aoData[ row ][ type ][ column ];
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().render()', 'cell().render()', function ( type ) {
		return this.iterator( 'cell', function ( settings, row, column ) {
			return _fnGetCellData( settings, row, column, type );
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().indexes()', 'cell().index()', function () {
		return this.iterator( 'cell', function ( settings, row, column ) {
			return {
				row: row,
				column: column,
				columnVisible: _fnColumnIndexToVisible( settings, column )
			};
		}, 1 );
	} );
	
	
	_api_registerPlural( 'cells().invalidate()', 'cell().invalidate()', function ( src ) {
		return this.iterator( 'cell', function ( settings, row, column ) {
			_fnInvalidate( settings, row, src, column );
		} );
	} );
	
	
	
	_api_register( 'cell()', function ( rowSelector, columnSelector, opts ) {
		return _selector_first( this.cells( rowSelector, columnSelector, opts ) );
	} );
	
	
	_api_register( 'cell().data()', function ( data ) {
		var ctx = this.context;
		var cell = this[0];
	
		if ( data === undefined ) {
			// Get
			return ctx.length && cell.length ?
				_fnGetCellData( ctx[0], cell[0].row, cell[0].column ) :
				undefined;
		}
	
		// Set
		_fnSetCellData( ctx[0], cell[0].row, cell[0].column, data );
		_fnInvalidate( ctx[0], cell[0].row, 'data', cell[0].column );
	
		return this;
	} );
	
	
	
	/**
	 * Get current ordering (sorting) that has been applied to the table.
	 *
	 * @returns {array} 2D array containing the sorting information for the first
	 *   table in the current context. Each element in the parent array represents
	 *   a column being sorted upon (i.e. multi-sorting with two columns would have
	 *   2 inner arrays). The inner arrays may have 2 or 3 elements. The first is
	 *   the column index that the sorting condition applies to, the second is the
	 *   direction of the sort (`desc` or `asc`) and, optionally, the third is the
	 *   index of the sorting order from the `column.sorting` initialisation array.
	 *//**
	 * Set the ordering for the table.
	 *
	 * @param {integer} order Column index to sort upon.
	 * @param {string} direction Direction of the sort to be applied (`asc` or `desc`)
	 * @returns {DataTables.Api} this
	 *//**
	 * Set the ordering for the table.
	 *
	 * @param {array} order 1D array of sorting information to be applied.
	 * @param {array} [...] Optional additional sorting conditions
	 * @returns {DataTables.Api} this
	 *//**
	 * Set the ordering for the table.
	 *
	 * @param {array} order 2D array of sorting information to be applied.
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'order()', function ( order, dir ) {
		var ctx = this.context;
	
		if ( order === undefined ) {
			// get
			return ctx.length !== 0 ?
				ctx[0].aaSorting :
				undefined;
		}
	
		// set
		if ( typeof order === 'number' ) {
			// Simple column / direction passed in
			order = [ [ order, dir ] ];
		}
		else if ( order.length && ! $.isArray( order[0] ) ) {
			// Arguments passed in (list of 1D arrays)
			order = Array.prototype.slice.call( arguments );
		}
		// otherwise a 2D array was passed in
	
		return this.iterator( 'table', function ( settings ) {
			settings.aaSorting = order.slice();
		} );
	} );
	
	
	/**
	 * Attach a sort listener to an element for a given column
	 *
	 * @param {node|jQuery|string} node Identifier for the element(s) to attach the
	 *   listener to. This can take the form of a single DOM node, a jQuery
	 *   collection of nodes or a jQuery selector which will identify the node(s).
	 * @param {integer} column the column that a click on this node will sort on
	 * @param {function} [callback] callback function when sort is run
	 * @returns {DataTables.Api} this
	 */
	_api_register( 'order.listener()', function ( node, column, callback ) {
		return this.iterator( 'table', function ( settings ) {
			_fnSortAttachListener( settings, node, column, callback );
		} );
	} );
	
	
	_api_register( 'order.fixed()', function ( set ) {
		if ( ! set ) {
			var ctx = this.context;
			var fixed = ctx.length ?
				ctx[0].aaSortingFixed :
				undefined;
	
			return $.isArray( fixed ) ?
				{ pre: fixed } :
				fixed;
		}
	
		return this.iterator( 'table', function ( settings ) {
			settings.aaSortingFixed = $.extend( true, {}, set );
		} );
	} );
	
	
	// Order by the selected column(s)
	_api_register( [
		'columns().order()',
		'column().order()'
	], function ( dir ) {
		var that = this;
	
		return this.iterator( 'table', function ( settings, i ) {
			var sort = [];
	
			$.each( that[i], function (j, col) {
				sort.push( [ col, dir ] );
			} );
	
			settings.aaSorting = sort;
		} );
	} );
	
	
	
	_api_register( 'search()', function ( input, regex, smart, caseInsen ) {
		var ctx = this.context;
	
		if ( input === undefined ) {
			// get
			return ctx.length !== 0 ?
				ctx[0].oPreviousSearch.sSearch :
				undefined;
		}
	
		// set
		return this.iterator( 'table', function ( settings ) {
			if ( ! settings.oFeatures.bFilter ) {
				return;
			}
	
			_fnFilterComplete( settings, $.extend( {}, settings.oPreviousSearch, {
				"sSearch": input+"",
				"bRegex":  regex === null ? false : regex,
				"bSmart":  smart === null ? true  : smart,
				"bCaseInsensitive": caseInsen === null ? true : caseInsen
			} ), 1 );
		} );
	} );
	
	
	_api_registerPlural(
		'columns().search()',
		'column().search()',
		function ( input, regex, smart, caseInsen ) {
			return this.iterator( 'column', function ( settings, column ) {
				var preSearch = settings.aoPreSearchCols;
	
				if ( input === undefined ) {
					// get
					return preSearch[ column ].sSearch;
				}
	
				// set
				if ( ! settings.oFeatures.bFilter ) {
					return;
				}
	
				$.extend( preSearch[ column ], {
					"sSearch": input+"",
					"bRegex":  regex === null ? false : regex,
					"bSmart":  smart === null ? true  : smart,
					"bCaseInsensitive": caseInsen === null ? true : caseInsen
				} );
	
				_fnFilterComplete( settings, settings.oPreviousSearch, 1 );
			} );
		}
	);
	
	/*
	 * State API methods
	 */
	
	_api_register( 'state()', function () {
		return this.context.length ?
			this.context[0].oSavedState :
			null;
	} );
	
	
	_api_register( 'state.clear()', function () {
		return this.iterator( 'table', function ( settings ) {
			// Save an empty object
			settings.fnStateSaveCallback.call( settings.oInstance, settings, {} );
		} );
	} );
	
	
	_api_register( 'state.loaded()', function () {
		return this.context.length ?
			this.context[0].oLoadedState :
			null;
	} );
	
	
	_api_register( 'state.save()', function () {
		return this.iterator( 'table', function ( settings ) {
			_fnSaveState( settings );
		} );
	} );
	
	
	
	/**
	 * Provide a common method for plug-ins to check the version of DataTables being
	 * used, in order to ensure compatibility.
	 *
	 *  @param {string} version Version string to check for, in the format "X.Y.Z".
	 *    Note that the formats "X" and "X.Y" are also acceptable.
	 *  @returns {boolean} true if this version of DataTables is greater or equal to
	 *    the required version, or false if this version of DataTales is not
	 *    suitable
	 *  @static
	 *  @dtopt API-Static
	 *
	 *  @example
	 *    alert( $.fn.dataTable.versionCheck( '1.9.0' ) );
	 */
	DataTable.versionCheck = DataTable.fnVersionCheck = function( version )
	{
		var aThis = DataTable.version.split('.');
		var aThat = version.split('.');
		var iThis, iThat;
	
		for ( var i=0, iLen=aThat.length ; i<iLen ; i++ ) {
			iThis = parseInt( aThis[i], 10 ) || 0;
			iThat = parseInt( aThat[i], 10 ) || 0;
	
			// Parts are the same, keep comparing
			if (iThis === iThat) {
				continue;
			}
	
			// Parts are different, return immediately
			return iThis > iThat;
		}
	
		return true;
	};
	
	
	/**
	 * Check if a `<table>` node is a DataTable table already or not.
	 *
	 *  @param {node|jquery|string} table Table node, jQuery object or jQuery
	 *      selector for the table to test. Note that if more than more than one
	 *      table is passed on, only the first will be checked
	 *  @returns {boolean} true the table given is a DataTable, or false otherwise
	 *  @static
	 *  @dtopt API-Static
	 *
	 *  @example
	 *    if ( ! $.fn.DataTable.isDataTable( '#example' ) ) {
	 *      $('#example').dataTable();
	 *    }
	 */
	DataTable.isDataTable = DataTable.fnIsDataTable = function ( table )
	{
		var t = $(table).get(0);
		var is = false;
	
		if ( table instanceof DataTable.Api ) {
			return true;
		}
	
		$.each( DataTable.settings, function (i, o) {
			var head = o.nScrollHead ? $('table', o.nScrollHead)[0] : null;
			var foot = o.nScrollFoot ? $('table', o.nScrollFoot)[0] : null;
	
			if ( o.nTable === t || head === t || foot === t ) {
				is = true;
			}
		} );
	
		return is;
	};
	
	
	/**
	 * Get all DataTable tables that have been initialised - optionally you can
	 * select to get only currently visible tables.
	 *
	 *  @param {boolean} [visible=false] Flag to indicate if you want all (default)
	 *    or visible tables only.
	 *  @returns {array} Array of `table` nodes (not DataTable instances) which are
	 *    DataTables
	 *  @static
	 *  @dtopt API-Static
	 *
	 *  @example
	 *    $.each( $.fn.dataTable.tables(true), function () {
	 *      $(table).DataTable().columns.adjust();
	 *    } );
	 */
	DataTable.tables = DataTable.fnTables = function ( visible )
	{
		var api = false;
	
		if ( $.isPlainObject( visible ) ) {
			api = visible.api;
			visible = visible.visible;
		}
	
		var a = $.map( DataTable.settings, function (o) {
			if ( !visible || (visible && $(o.nTable).is(':visible')) ) {
				return o.nTable;
			}
		} );
	
		return api ?
			new _Api( a ) :
			a;
	};
	
	
	/**
	 * Convert from camel case parameters to Hungarian notation. This is made public
	 * for the extensions to provide the same ability as DataTables core to accept
	 * either the 1.9 style Hungarian notation, or the 1.10+ style camelCase
	 * parameters.
	 *
	 *  @param {object} src The model object which holds all parameters that can be
	 *    mapped.
	 *  @param {object} user The object to convert from camel case to Hungarian.
	 *  @param {boolean} force When set to `true`, properties which already have a
	 *    Hungarian value in the `user` object will be overwritten. Otherwise they
	 *    won't be.
	 */
	DataTable.camelToHungarian = _fnCamelToHungarian;
	
	
	
	/**
	 *
	 */
	_api_register( '$()', function ( selector, opts ) {
		var
			rows   = this.rows( opts ).nodes(), // Get all rows
			jqRows = $(rows);
	
		return $( [].concat(
			jqRows.filter( selector ).toArray(),
			jqRows.find( selector ).toArray()
		) );
	} );
	
	
	// jQuery functions to operate on the tables
	$.each( [ 'on', 'one', 'off' ], function (i, key) {
		_api_register( key+'()', function ( /* event, handler */ ) {
			var args = Array.prototype.slice.call(arguments);
	
			// Add the `dt` namespace automatically if it isn't already present
			args[0] = $.map( args[0].split( /\s/ ), function ( e ) {
				return ! e.match(/\.dt\b/) ?
					e+'.dt' :
					e;
				} ).join( ' ' );
	
			var inst = $( this.tables().nodes() );
			inst[key].apply( inst, args );
			return this;
		} );
	} );
	
	
	_api_register( 'clear()', function () {
		return this.iterator( 'table', function ( settings ) {
			_fnClearTable( settings );
		} );
	} );
	
	
	_api_register( 'settings()', function () {
		return new _Api( this.context, this.context );
	} );
	
	
	_api_register( 'init()', function () {
		var ctx = this.context;
		return ctx.length ? ctx[0].oInit : null;
	} );
	
	
	_api_register( 'data()', function () {
		return this.iterator( 'table', function ( settings ) {
			return _pluck( settings.aoData, '_aData' );
		} ).flatten();
	} );
	
	
	_api_register( 'destroy()', function ( remove ) {
		remove = remove || false;
	
		return this.iterator( 'table', function ( settings ) {
			var orig      = settings.nTableWrapper.parentNode;
			var classes   = settings.oClasses;
			var table     = settings.nTable;
			var tbody     = settings.nTBody;
			var thead     = settings.nTHead;
			var tfoot     = settings.nTFoot;
			var jqTable   = $(table);
			var jqTbody   = $(tbody);
			var jqWrapper = $(settings.nTableWrapper);
			var rows      = $.map( settings.aoData, function (r) { return r.nTr; } );
			var i, ien;
	
			// Flag to note that the table is currently being destroyed - no action
			// should be taken
			settings.bDestroying = true;
	
			// Fire off the destroy callbacks for plug-ins etc
			_fnCallbackFire( settings, "aoDestroyCallback", "destroy", [settings] );
	
			// If not being removed from the document, make all columns visible
			if ( ! remove ) {
				new _Api( settings ).columns().visible( true );
			}
	
			// Blitz all `DT` namespaced events (these are internal events, the
			// lowercase, `dt` events are user subscribed and they are responsible
			// for removing them
			jqWrapper.off('.DT').find(':not(tbody *)').off('.DT');
			$(window).off('.DT-'+settings.sInstance);
	
			// When scrolling we had to break the table up - restore it
			if ( table != thead.parentNode ) {
				jqTable.children('thead').detach();
				jqTable.append( thead );
			}
	
			if ( tfoot && table != tfoot.parentNode ) {
				jqTable.children('tfoot').detach();
				jqTable.append( tfoot );
			}
	
			settings.aaSorting = [];
			settings.aaSortingFixed = [];
			_fnSortingClasses( settings );
	
			$( rows ).removeClass( settings.asStripeClasses.join(' ') );
	
			$('th, td', thead).removeClass( classes.sSortable+' '+
				classes.sSortableAsc+' '+classes.sSortableDesc+' '+classes.sSortableNone
			);
	
			// Add the TR elements back into the table in their original order
			jqTbody.children().detach();
			jqTbody.append( rows );
	
			// Remove the DataTables generated nodes, events and classes
			var removedMethod = remove ? 'remove' : 'detach';
			jqTable[ removedMethod ]();
			jqWrapper[ removedMethod ]();
	
			// If we need to reattach the table to the document
			if ( ! remove && orig ) {
				// insertBefore acts like appendChild if !arg[1]
				orig.insertBefore( table, settings.nTableReinsertBefore );
	
				// Restore the width of the original table - was read from the style property,
				// so we can restore directly to that
				jqTable
					.css( 'width', settings.sDestroyWidth )
					.removeClass( classes.sTable );
	
				// If the were originally stripe classes - then we add them back here.
				// Note this is not fool proof (for example if not all rows had stripe
				// classes - but it's a good effort without getting carried away
				ien = settings.asDestroyStripes.length;
	
				if ( ien ) {
					jqTbody.children().each( function (i) {
						$(this).addClass( settings.asDestroyStripes[i % ien] );
					} );
				}
			}
	
			/* Remove the settings object from the settings array */
			var idx = $.inArray( settings, DataTable.settings );
			if ( idx !== -1 ) {
				DataTable.settings.splice( idx, 1 );
			}
		} );
	} );
	
	
	// Add the `every()` method for rows, columns and cells in a compact form
	$.each( [ 'column', 'row', 'cell' ], function ( i, type ) {
		_api_register( type+'s().every()', function ( fn ) {
			var opts = this.selector.opts;
			var api = this;
	
			return this.iterator( type, function ( settings, arg1, arg2, arg3, arg4 ) {
				// Rows and columns:
				//  arg1 - index
				//  arg2 - table counter
				//  arg3 - loop counter
				//  arg4 - undefined
				// Cells:
				//  arg1 - row index
				//  arg2 - column index
				//  arg3 - table counter
				//  arg4 - loop counter
				fn.call(
					api[ type ](
						arg1,
						type==='cell' ? arg2 : opts,
						type==='cell' ? opts : undefined
					),
					arg1, arg2, arg3, arg4
				);
			} );
		} );
	} );
	
	
	// i18n method for extensions to be able to use the language object from the
	// DataTable
	_api_register( 'i18n()', function ( token, def, plural ) {
		var ctx = this.context[0];
		var resolved = _fnGetObjectDataFn( token )( ctx.oLanguage );
	
		if ( resolved === undefined ) {
			resolved = def;
		}
	
		if ( plural !== undefined && $.isPlainObject( resolved ) ) {
			resolved = resolved[ plural ] !== undefined ?
				resolved[ plural ] :
				resolved._;
		}
	
		return resolved.replace( '%d', plural ); // nb: plural might be undefined,
	} );

	/**
	 * Version string for plug-ins to check compatibility. Allowed format is
	 * `a.b.c-d` where: a:int, b:int, c:int, d:string(dev|beta|alpha). `d` is used
	 * only for non-release builds. See http://semver.org/ for more information.
	 *  @member
	 *  @type string
	 *  @default Version number
	 */
	DataTable.version = "1.10.18";

	/**
	 * Private data store, containing all of the settings objects that are
	 * created for the tables on a given page.
	 *
	 * Note that the `DataTable.settings` object is aliased to
	 * `jQuery.fn.dataTableExt` through which it may be accessed and
	 * manipulated, or `jQuery.fn.dataTable.settings`.
	 *  @member
	 *  @type array
	 *  @default []
	 *  @private
	 */
	DataTable.settings = [];

	/**
	 * Object models container, for the various models that DataTables has
	 * available to it. These models define the objects that are used to hold
	 * the active state and configuration of the table.
	 *  @namespace
	 */
	DataTable.models = {};
	
	
	
	/**
	 * Template object for the way in which DataTables holds information about
	 * search information for the global filter and individual column filters.
	 *  @namespace
	 */
	DataTable.models.oSearch = {
		/**
		 * Flag to indicate if the filtering should be case insensitive or not
		 *  @type boolean
		 *  @default true
		 */
		"bCaseInsensitive": true,
	
		/**
		 * Applied search term
		 *  @type string
		 *  @default <i>Empty string</i>
		 */
		"sSearch": "",
	
		/**
		 * Flag to indicate if the search term should be interpreted as a
		 * regular expression (true) or not (false) and therefore and special
		 * regex characters escaped.
		 *  @type boolean
		 *  @default false
		 */
		"bRegex": false,
	
		/**
		 * Flag to indicate if DataTables is to use its smart filtering or not.
		 *  @type boolean
		 *  @default true
		 */
		"bSmart": true
	};
	
	
	
	
	/**
	 * Template object for the way in which DataTables holds information about
	 * each individual row. This is the object format used for the settings
	 * aoData array.
	 *  @namespace
	 */
	DataTable.models.oRow = {
		/**
		 * TR element for the row
		 *  @type node
		 *  @default null
		 */
		"nTr": null,
	
		/**
		 * Array of TD elements for each row. This is null until the row has been
		 * created.
		 *  @type array nodes
		 *  @default []
		 */
		"anCells": null,
	
		/**
		 * Data object from the original data source for the row. This is either
		 * an array if using the traditional form of DataTables, or an object if
		 * using mData options. The exact type will depend on the passed in
		 * data from the data source, or will be an array if using DOM a data
		 * source.
		 *  @type array|object
		 *  @default []
		 */
		"_aData": [],
	
		/**
		 * Sorting data cache - this array is ostensibly the same length as the
		 * number of columns (although each index is generated only as it is
		 * needed), and holds the data that is used for sorting each column in the
		 * row. We do this cache generation at the start of the sort in order that
		 * the formatting of the sort data need be done only once for each cell
		 * per sort. This array should not be read from or written to by anything
		 * other than the master sorting methods.
		 *  @type array
		 *  @default null
		 *  @private
		 */
		"_aSortData": null,
	
		/**
		 * Per cell filtering data cache. As per the sort data cache, used to
		 * increase the performance of the filtering in DataTables
		 *  @type array
		 *  @default null
		 *  @private
		 */
		"_aFilterData": null,
	
		/**
		 * Filtering data cache. This is the same as the cell filtering cache, but
		 * in this case a string rather than an array. This is easily computed with
		 * a join on `_aFilterData`, but is provided as a cache so the join isn't
		 * needed on every search (memory traded for performance)
		 *  @type array
		 *  @default null
		 *  @private
		 */
		"_sFilterRow": null,
	
		/**
		 * Cache of the class name that DataTables has applied to the row, so we
		 * can quickly look at this variable rather than needing to do a DOM check
		 * on className for the nTr property.
		 *  @type string
		 *  @default <i>Empty string</i>
		 *  @private
		 */
		"_sRowStripe": "",
	
		/**
		 * Denote if the original data source was from the DOM, or the data source
		 * object. This is used for invalidating data, so DataTables can
		 * automatically read data from the original source, unless uninstructed
		 * otherwise.
		 *  @type string
		 *  @default null
		 *  @private
		 */
		"src": null,
	
		/**
		 * Index in the aoData array. This saves an indexOf lookup when we have the
		 * object, but want to know the index
		 *  @type integer
		 *  @default -1
		 *  @private
		 */
		"idx": -1
	};
	
	
	/**
	 * Template object for the column information object in DataTables. This object
	 * is held in the settings aoColumns array and contains all the information that
	 * DataTables needs about each individual column.
	 *
	 * Note that this object is related to {@link DataTable.defaults.column}
	 * but this one is the internal data store for DataTables's cache of columns.
	 * It should NOT be manipulated outside of DataTables. Any configuration should
	 * be done through the initialisation options.
	 *  @namespace
	 */
	DataTable.models.oColumn = {
		/**
		 * Column index. This could be worked out on-the-fly with $.inArray, but it
		 * is faster to just hold it as a variable
		 *  @type integer
		 *  @default null
		 */
		"idx": null,
	
		/**
		 * A list of the columns that sorting should occur on when this column
		 * is sorted. That this property is an array allows multi-column sorting
		 * to be defined for a column (for example first name / last name columns
		 * would benefit from this). The values are integers pointing to the
		 * columns to be sorted on (typically it will be a single integer pointing
		 * at itself, but that doesn't need to be the case).
		 *  @type array
		 */
		"aDataSort": null,
	
		/**
		 * Define the sorting directions that are applied to the column, in sequence
		 * as the column is repeatedly sorted upon - i.e. the first value is used
		 * as the sorting direction when the column if first sorted (clicked on).
		 * Sort it again (click again) and it will move on to the next index.
		 * Repeat until loop.
		 *  @type array
		 */
		"asSorting": null,
	
		/**
		 * Flag to indicate if the column is searchable, and thus should be included
		 * in the filtering or not.
		 *  @type boolean
		 */
		"bSearchable": null,
	
		/**
		 * Flag to indicate if the column is sortable or not.
		 *  @type boolean
		 */
		"bSortable": null,
	
		/**
		 * Flag to indicate if the column is currently visible in the table or not
		 *  @type boolean
		 */
		"bVisible": null,
	
		/**
		 * Store for manual type assignment using the `column.type` option. This
		 * is held in store so we can manipulate the column's `sType` property.
		 *  @type string
		 *  @default null
		 *  @private
		 */
		"_sManualType": null,
	
		/**
		 * Flag to indicate if HTML5 data attributes should be used as the data
		 * source for filtering or sorting. True is either are.
		 *  @type boolean
		 *  @default false
		 *  @private
		 */
		"_bAttrSrc": false,
	
		/**
		 * Developer definable function that is called whenever a cell is created (Ajax source,
		 * etc) or processed for input (DOM source). This can be used as a compliment to mRender
		 * allowing you to modify the DOM element (add background colour for example) when the
		 * element is available.
		 *  @type function
		 *  @param {element} nTd The TD node that has been created
		 *  @param {*} sData The Data for the cell
		 *  @param {array|object} oData The data for the whole row
		 *  @param {int} iRow The row index for the aoData data store
		 *  @default null
		 */
		"fnCreatedCell": null,
	
		/**
		 * Function to get data from a cell in a column. You should <b>never</b>
		 * access data directly through _aData internally in DataTables - always use
		 * the method attached to this property. It allows mData to function as
		 * required. This function is automatically assigned by the column
		 * initialisation method
		 *  @type function
		 *  @param {array|object} oData The data array/object for the array
		 *    (i.e. aoData[]._aData)
		 *  @param {string} sSpecific The specific data type you want to get -
		 *    'display', 'type' 'filter' 'sort'
		 *  @returns {*} The data for the cell from the given row's data
		 *  @default null
		 */
		"fnGetData": null,
	
		/**
		 * Function to set data for a cell in the column. You should <b>never</b>
		 * set the data directly to _aData internally in DataTables - always use
		 * this method. It allows mData to function as required. This function
		 * is automatically assigned by the column initialisation method
		 *  @type function
		 *  @param {array|object} oData The data array/object for the array
		 *    (i.e. aoData[]._aData)
		 *  @param {*} sValue Value to set
		 *  @default null
		 */
		"fnSetData": null,
	
		/**
		 * Property to read the value for the cells in the column from the data
		 * source array / object. If null, then the default content is used, if a
		 * function is given then the return from the function is used.
		 *  @type function|int|string|null
		 *  @default null
		 */
		"mData": null,
	
		/**
		 * Partner property to mData which is used (only when defined) to get
		 * the data - i.e. it is basically the same as mData, but without the
		 * 'set' option, and also the data fed to it is the result from mData.
		 * This is the rendering method to match the data method of mData.
		 *  @type function|int|string|null
		 *  @default null
		 */
		"mRender": null,
	
		/**
		 * Unique header TH/TD element for this column - this is what the sorting
		 * listener is attached to (if sorting is enabled.)
		 *  @type node
		 *  @default null
		 */
		"nTh": null,
	
		/**
		 * Unique footer TH/TD element for this column (if there is one). Not used
		 * in DataTables as such, but can be used for plug-ins to reference the
		 * footer for each column.
		 *  @type node
		 *  @default null
		 */
		"nTf": null,
	
		/**
		 * The class to apply to all TD elements in the table's TBODY for the column
		 *  @type string
		 *  @default null
		 */
		"sClass": null,
	
		/**
		 * When DataTables calculates the column widths to assign to each column,
		 * it finds the longest string in each column and then constructs a
		 * temporary table and reads the widths from that. The problem with this
		 * is that "mmm" is much wider then "iiii", but the latter is a longer
		 * string - thus the calculation can go wrong (doing it properly and putting
		 * it into an DOM object and measuring that is horribly(!) slow). Thus as
		 * a "work around" we provide this option. It will append its value to the
		 * text that is found to be the longest string for the column - i.e. padding.
		 *  @type string
		 */
		"sContentPadding": null,
	
		/**
		 * Allows a default value to be given for a column's data, and will be used
		 * whenever a null data source is encountered (this can be because mData
		 * is set to null, or because the data source itself is null).
		 *  @type string
		 *  @default null
		 */
		"sDefaultContent": null,
	
		/**
		 * Name for the column, allowing reference to the column by name as well as
		 * by index (needs a lookup to work by name).
		 *  @type string
		 */
		"sName": null,
	
		/**
		 * Custom sorting data type - defines which of the available plug-ins in
		 * afnSortData the custom sorting will use - if any is defined.
		 *  @type string
		 *  @default std
		 */
		"sSortDataType": 'std',
	
		/**
		 * Class to be applied to the header element when sorting on this column
		 *  @type string
		 *  @default null
		 */
		"sSortingClass": null,
	
		/**
		 * Class to be applied to the header element when sorting on this column -
		 * when jQuery UI theming is used.
		 *  @type string
		 *  @default null
		 */
		"sSortingClassJUI": null,
	
		/**
		 * Title of the column - what is seen in the TH element (nTh).
		 *  @type string
		 */
		"sTitle": null,
	
		/**
		 * Column sorting and filtering type
		 *  @type string
		 *  @default null
		 */
		"sType": null,
	
		/**
		 * Width of the column
		 *  @type string
		 *  @default null
		 */
		"sWidth": null,
	
		/**
		 * Width of the column when it was first "encountered"
		 *  @type string
		 *  @default null
		 */
		"sWidthOrig": null
	};
	
	
	/*
	 * Developer note: The properties of the object below are given in Hungarian
	 * notation, that was used as the interface for DataTables prior to v1.10, however
	 * from v1.10 onwards the primary interface is camel case. In order to avoid
	 * breaking backwards compatibility utterly with this change, the Hungarian
	 * version is still, internally the primary interface, but is is not documented
	 * - hence the @name tags in each doc comment. This allows a Javascript function
	 * to create a map from Hungarian notation to camel case (going the other direction
	 * would require each property to be listed, which would at around 3K to the size
	 * of DataTables, while this method is about a 0.5K hit.
	 *
	 * Ultimately this does pave the way for Hungarian notation to be dropped
	 * completely, but that is a massive amount of work and will break current
	 * installs (therefore is on-hold until v2).
	 */
	
	/**
	 * Initialisation options that can be given to DataTables at initialisation
	 * time.
	 *  @namespace
	 */
	DataTable.defaults = {
		/**
		 * An array of data to use for the table, passed in at initialisation which
		 * will be used in preference to any data which is already in the DOM. This is
		 * particularly useful for constructing tables purely in Javascript, for
		 * example with a custom Ajax call.
		 *  @type array
		 *  @default null
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.data
		 *
		 *  @example
		 *    // Using a 2D array data source
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "data": [
		 *          ['Trident', 'Internet Explorer 4.0', 'Win 95+', 4, 'X'],
		 *          ['Trident', 'Internet Explorer 5.0', 'Win 95+', 5, 'C'],
		 *        ],
		 *        "columns": [
		 *          { "title": "Engine" },
		 *          { "title": "Browser" },
		 *          { "title": "Platform" },
		 *          { "title": "Version" },
		 *          { "title": "Grade" }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using an array of objects as a data source (`data`)
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "data": [
		 *          {
		 *            "engine":   "Trident",
		 *            "browser":  "Internet Explorer 4.0",
		 *            "platform": "Win 95+",
		 *            "version":  4,
		 *            "grade":    "X"
		 *          },
		 *          {
		 *            "engine":   "Trident",
		 *            "browser":  "Internet Explorer 5.0",
		 *            "platform": "Win 95+",
		 *            "version":  5,
		 *            "grade":    "C"
		 *          }
		 *        ],
		 *        "columns": [
		 *          { "title": "Engine",   "data": "engine" },
		 *          { "title": "Browser",  "data": "browser" },
		 *          { "title": "Platform", "data": "platform" },
		 *          { "title": "Version",  "data": "version" },
		 *          { "title": "Grade",    "data": "grade" }
		 *        ]
		 *      } );
		 *    } );
		 */
		"aaData": null,
	
	
		/**
		 * If ordering is enabled, then DataTables will perform a first pass sort on
		 * initialisation. You can define which column(s) the sort is performed
		 * upon, and the sorting direction, with this variable. The `sorting` array
		 * should contain an array for each column to be sorted initially containing
		 * the column's index and a direction string ('asc' or 'desc').
		 *  @type array
		 *  @default [[0,'asc']]
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.order
		 *
		 *  @example
		 *    // Sort by 3rd column first, and then 4th column
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "order": [[2,'asc'], [3,'desc']]
		 *      } );
		 *    } );
		 *
		 *    // No initial sorting
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "order": []
		 *      } );
		 *    } );
		 */
		"aaSorting": [[0,'asc']],
	
	
		/**
		 * This parameter is basically identical to the `sorting` parameter, but
		 * cannot be overridden by user interaction with the table. What this means
		 * is that you could have a column (visible or hidden) which the sorting
		 * will always be forced on first - any sorting after that (from the user)
		 * will then be performed as required. This can be useful for grouping rows
		 * together.
		 *  @type array
		 *  @default null
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.orderFixed
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "orderFixed": [[0,'asc']]
		 *      } );
		 *    } )
		 */
		"aaSortingFixed": [],
	
	
		/**
		 * DataTables can be instructed to load data to display in the table from a
		 * Ajax source. This option defines how that Ajax call is made and where to.
		 *
		 * The `ajax` property has three different modes of operation, depending on
		 * how it is defined. These are:
		 *
		 * * `string` - Set the URL from where the data should be loaded from.
		 * * `object` - Define properties for `jQuery.ajax`.
		 * * `function` - Custom data get function
		 *
		 * `string`
		 * --------
		 *
		 * As a string, the `ajax` property simply defines the URL from which
		 * DataTables will load data.
		 *
		 * `object`
		 * --------
		 *
		 * As an object, the parameters in the object are passed to
		 * [jQuery.ajax](http://api.jquery.com/jQuery.ajax/) allowing fine control
		 * of the Ajax request. DataTables has a number of default parameters which
		 * you can override using this option. Please refer to the jQuery
		 * documentation for a full description of the options available, although
		 * the following parameters provide additional options in DataTables or
		 * require special consideration:
		 *
		 * * `data` - As with jQuery, `data` can be provided as an object, but it
		 *   can also be used as a function to manipulate the data DataTables sends
		 *   to the server. The function takes a single parameter, an object of
		 *   parameters with the values that DataTables has readied for sending. An
		 *   object may be returned which will be merged into the DataTables
		 *   defaults, or you can add the items to the object that was passed in and
		 *   not return anything from the function. This supersedes `fnServerParams`
		 *   from DataTables 1.9-.
		 *
		 * * `dataSrc` - By default DataTables will look for the property `data` (or
		 *   `aaData` for compatibility with DataTables 1.9-) when obtaining data
		 *   from an Ajax source or for server-side processing - this parameter
		 *   allows that property to be changed. You can use Javascript dotted
		 *   object notation to get a data source for multiple levels of nesting, or
		 *   it my be used as a function. As a function it takes a single parameter,
		 *   the JSON returned from the server, which can be manipulated as
		 *   required, with the returned value being that used by DataTables as the
		 *   data source for the table. This supersedes `sAjaxDataProp` from
		 *   DataTables 1.9-.
		 *
		 * * `success` - Should not be overridden it is used internally in
		 *   DataTables. To manipulate / transform the data returned by the server
		 *   use `ajax.dataSrc`, or use `ajax` as a function (see below).
		 *
		 * `function`
		 * ----------
		 *
		 * As a function, making the Ajax call is left up to yourself allowing
		 * complete control of the Ajax request. Indeed, if desired, a method other
		 * than Ajax could be used to obtain the required data, such as Web storage
		 * or an AIR database.
		 *
		 * The function is given four parameters and no return is required. The
		 * parameters are:
		 *
		 * 1. _object_ - Data to send to the server
		 * 2. _function_ - Callback function that must be executed when the required
		 *    data has been obtained. That data should be passed into the callback
		 *    as the only parameter
		 * 3. _object_ - DataTables settings object for the table
		 *
		 * Note that this supersedes `fnServerData` from DataTables 1.9-.
		 *
		 *  @type string|object|function
		 *  @default null
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.ajax
		 *  @since 1.10.0
		 *
		 * @example
		 *   // Get JSON data from a file via Ajax.
		 *   // Note DataTables expects data in the form `{ data: [ ...data... ] }` by default).
		 *   $('#example').dataTable( {
		 *     "ajax": "data.json"
		 *   } );
		 *
		 * @example
		 *   // Get JSON data from a file via Ajax, using `dataSrc` to change
		 *   // `data` to `tableData` (i.e. `{ tableData: [ ...data... ] }`)
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "dataSrc": "tableData"
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Get JSON data from a file via Ajax, using `dataSrc` to read data
		 *   // from a plain array rather than an array in an object
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "dataSrc": ""
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Manipulate the data returned from the server - add a link to data
		 *   // (note this can, should, be done using `render` for the column - this
		 *   // is just a simple example of how the data can be manipulated).
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "dataSrc": function ( json ) {
		 *         for ( var i=0, ien=json.length ; i<ien ; i++ ) {
		 *           json[i][0] = '<a href="/message/'+json[i][0]+'>View message</a>';
		 *         }
		 *         return json;
		 *       }
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Add data to the request
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "data": function ( d ) {
		 *         return {
		 *           "extra_search": $('#extra').val()
		 *         };
		 *       }
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Send request as POST
		 *   $('#example').dataTable( {
		 *     "ajax": {
		 *       "url": "data.json",
		 *       "type": "POST"
		 *     }
		 *   } );
		 *
		 * @example
		 *   // Get the data from localStorage (could interface with a form for
		 *   // adding, editing and removing rows).
		 *   $('#example').dataTable( {
		 *     "ajax": function (data, callback, settings) {
		 *       callback(
		 *         JSON.parse( localStorage.getItem('dataTablesData') )
		 *       );
		 *     }
		 *   } );
		 */
		"ajax": null,
	
	
		/**
		 * This parameter allows you to readily specify the entries in the length drop
		 * down menu that DataTables shows when pagination is enabled. It can be
		 * either a 1D array of options which will be used for both the displayed
		 * option and the value, or a 2D array which will use the array in the first
		 * position as the value, and the array in the second position as the
		 * displayed options (useful for language strings such as 'All').
		 *
		 * Note that the `pageLength` property will be automatically set to the
		 * first value given in this array, unless `pageLength` is also provided.
		 *  @type array
		 *  @default [ 10, 25, 50, 100 ]
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.lengthMenu
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
		 *      } );
		 *    } );
		 */
		"aLengthMenu": [ 10, 25, 50, 100 ],
	
	
		/**
		 * The `columns` option in the initialisation parameter allows you to define
		 * details about the way individual columns behave. For a full list of
		 * column options that can be set, please see
		 * {@link DataTable.defaults.column}. Note that if you use `columns` to
		 * define your columns, you must have an entry in the array for every single
		 * column that you have in your table (these can be null if you don't which
		 * to specify any options).
		 *  @member
		 *
		 *  @name DataTable.defaults.column
		 */
		"aoColumns": null,
	
		/**
		 * Very similar to `columns`, `columnDefs` allows you to target a specific
		 * column, multiple columns, or all columns, using the `targets` property of
		 * each object in the array. This allows great flexibility when creating
		 * tables, as the `columnDefs` arrays can be of any length, targeting the
		 * columns you specifically want. `columnDefs` may use any of the column
		 * options available: {@link DataTable.defaults.column}, but it _must_
		 * have `targets` defined in each object in the array. Values in the `targets`
		 * array may be:
		 *   <ul>
		 *     <li>a string - class name will be matched on the TH for the column</li>
		 *     <li>0 or a positive integer - column index counting from the left</li>
		 *     <li>a negative integer - column index counting from the right</li>
		 *     <li>the string "_all" - all columns (i.e. assign a default)</li>
		 *   </ul>
		 *  @member
		 *
		 *  @name DataTable.defaults.columnDefs
		 */
		"aoColumnDefs": null,
	
	
		/**
		 * Basically the same as `search`, this parameter defines the individual column
		 * filtering state at initialisation time. The array must be of the same size
		 * as the number of columns, and each element be an object with the parameters
		 * `search` and `escapeRegex` (the latter is optional). 'null' is also
		 * accepted and the default will be used.
		 *  @type array
		 *  @default []
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.searchCols
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "searchCols": [
		 *          null,
		 *          { "search": "My filter" },
		 *          null,
		 *          { "search": "^[0-9]", "escapeRegex": false }
		 *        ]
		 *      } );
		 *    } )
		 */
		"aoSearchCols": [],
	
	
		/**
		 * An array of CSS classes that should be applied to displayed rows. This
		 * array may be of any length, and DataTables will apply each class
		 * sequentially, looping when required.
		 *  @type array
		 *  @default null <i>Will take the values determined by the `oClasses.stripe*`
		 *    options</i>
		 *
		 *  @dtopt Option
		 *  @name DataTable.defaults.stripeClasses
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stripeClasses": [ 'strip1', 'strip2', 'strip3' ]
		 *      } );
		 *    } )
		 */
		"asStripeClasses": null,
	
	
		/**
		 * Enable or disable automatic column width calculation. This can be disabled
		 * as an optimisation (it takes some time to calculate the widths) if the
		 * tables widths are passed in using `columns`.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.autoWidth
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "autoWidth": false
		 *      } );
		 *    } );
		 */
		"bAutoWidth": true,
	
	
		/**
		 * Deferred rendering can provide DataTables with a huge speed boost when you
		 * are using an Ajax or JS data source for the table. This option, when set to
		 * true, will cause DataTables to defer the creation of the table elements for
		 * each row until they are needed for a draw - saving a significant amount of
		 * time.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.deferRender
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajax": "sources/arrays.txt",
		 *        "deferRender": true
		 *      } );
		 *    } );
		 */
		"bDeferRender": false,
	
	
		/**
		 * Replace a DataTable which matches the given selector and replace it with
		 * one which has the properties of the new initialisation object passed. If no
		 * table matches the selector, then the new DataTable will be constructed as
		 * per normal.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.destroy
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "srollY": "200px",
		 *        "paginate": false
		 *      } );
		 *
		 *      // Some time later....
		 *      $('#example').dataTable( {
		 *        "filter": false,
		 *        "destroy": true
		 *      } );
		 *    } );
		 */
		"bDestroy": false,
	
	
		/**
		 * Enable or disable filtering of data. Filtering in DataTables is "smart" in
		 * that it allows the end user to input multiple words (space separated) and
		 * will match a row containing those words, even if not in the order that was
		 * specified (this allow matching across multiple columns). Note that if you
		 * wish to use filtering in DataTables this must remain 'true' - to remove the
		 * default filtering input box and retain filtering abilities, please use
		 * {@link DataTable.defaults.dom}.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.searching
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "searching": false
		 *      } );
		 *    } );
		 */
		"bFilter": true,
	
	
		/**
		 * Enable or disable the table information display. This shows information
		 * about the data that is currently visible on the page, including information
		 * about filtered data if that action is being performed.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.info
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "info": false
		 *      } );
		 *    } );
		 */
		"bInfo": true,
	
	
		/**
		 * Allows the end user to select the size of a formatted page from a select
		 * menu (sizes are 10, 25, 50 and 100). Requires pagination (`paginate`).
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.lengthChange
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "lengthChange": false
		 *      } );
		 *    } );
		 */
		"bLengthChange": true,
	
	
		/**
		 * Enable or disable pagination.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.paging
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "paging": false
		 *      } );
		 *    } );
		 */
		"bPaginate": true,
	
	
		/**
		 * Enable or disable the display of a 'processing' indicator when the table is
		 * being processed (e.g. a sort). This is particularly useful for tables with
		 * large amounts of data where it can take a noticeable amount of time to sort
		 * the entries.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.processing
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "processing": true
		 *      } );
		 *    } );
		 */
		"bProcessing": false,
	
	
		/**
		 * Retrieve the DataTables object for the given selector. Note that if the
		 * table has already been initialised, this parameter will cause DataTables
		 * to simply return the object that has already been set up - it will not take
		 * account of any changes you might have made to the initialisation object
		 * passed to DataTables (setting this parameter to true is an acknowledgement
		 * that you understand this). `destroy` can be used to reinitialise a table if
		 * you need.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.retrieve
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      initTable();
		 *      tableActions();
		 *    } );
		 *
		 *    function initTable ()
		 *    {
		 *      return $('#example').dataTable( {
		 *        "scrollY": "200px",
		 *        "paginate": false,
		 *        "retrieve": true
		 *      } );
		 *    }
		 *
		 *    function tableActions ()
		 *    {
		 *      var table = initTable();
		 *      // perform API operations with oTable
		 *    }
		 */
		"bRetrieve": false,
	
	
		/**
		 * When vertical (y) scrolling is enabled, DataTables will force the height of
		 * the table's viewport to the given height at all times (useful for layout).
		 * However, this can look odd when filtering data down to a small data set,
		 * and the footer is left "floating" further down. This parameter (when
		 * enabled) will cause DataTables to collapse the table's viewport down when
		 * the result set will fit within the given Y height.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.scrollCollapse
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollY": "200",
		 *        "scrollCollapse": true
		 *      } );
		 *    } );
		 */
		"bScrollCollapse": false,
	
	
		/**
		 * Configure DataTables to use server-side processing. Note that the
		 * `ajax` parameter must also be given in order to give DataTables a
		 * source to obtain the required data for each draw.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverSide
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "serverSide": true,
		 *        "ajax": "xhr.php"
		 *      } );
		 *    } );
		 */
		"bServerSide": false,
	
	
		/**
		 * Enable or disable sorting of columns. Sorting of individual columns can be
		 * disabled by the `sortable` option for each column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.ordering
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "ordering": false
		 *      } );
		 *    } );
		 */
		"bSort": true,
	
	
		/**
		 * Enable or display DataTables' ability to sort multiple columns at the
		 * same time (activated by shift-click by the user).
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.orderMulti
		 *
		 *  @example
		 *    // Disable multiple column sorting ability
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "orderMulti": false
		 *      } );
		 *    } );
		 */
		"bSortMulti": true,
	
	
		/**
		 * Allows control over whether DataTables should use the top (true) unique
		 * cell that is found for a single column, or the bottom (false - default).
		 * This is useful when using complex headers.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.orderCellsTop
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "orderCellsTop": true
		 *      } );
		 *    } );
		 */
		"bSortCellsTop": false,
	
	
		/**
		 * Enable or disable the addition of the classes `sorting\_1`, `sorting\_2` and
		 * `sorting\_3` to the columns which are currently being sorted on. This is
		 * presented as a feature switch as it can increase processing time (while
		 * classes are removed and added) so for large data sets you might want to
		 * turn this off.
		 *  @type boolean
		 *  @default true
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.orderClasses
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "orderClasses": false
		 *      } );
		 *    } );
		 */
		"bSortClasses": true,
	
	
		/**
		 * Enable or disable state saving. When enabled HTML5 `localStorage` will be
		 * used to save table display information such as pagination information,
		 * display length, filtering and sorting. As such when the end user reloads
		 * the page the display display will match what thy had previously set up.
		 *
		 * Due to the use of `localStorage` the default state saving is not supported
		 * in IE6 or 7. If state saving is required in those browsers, use
		 * `stateSaveCallback` to provide a storage solution such as cookies.
		 *  @type boolean
		 *  @default false
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.stateSave
		 *
		 *  @example
		 *    $(document).ready( function () {
		 *      $('#example').dataTable( {
		 *        "stateSave": true
		 *      } );
		 *    } );
		 */
		"bStateSave": false,
	
	
		/**
		 * This function is called when a TR element is created (and all TD child
		 * elements have been inserted), or registered if using a DOM source, allowing
		 * manipulation of the TR element (adding classes etc).
		 *  @type function
		 *  @param {node} row "TR" element for the current row
		 *  @param {array} data Raw data array for this row
		 *  @param {int} dataIndex The index of this row in the internal aoData array
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.createdRow
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "createdRow": function( row, data, dataIndex ) {
		 *          // Bold the grade for all 'A' grade browsers
		 *          if ( data[4] == "A" )
		 *          {
		 *            $('td:eq(4)', row).html( '<b>A</b>' );
		 *          }
		 *        }
		 *      } );
		 *    } );
		 */
		"fnCreatedRow": null,
	
	
		/**
		 * This function is called on every 'draw' event, and allows you to
		 * dynamically modify any aspect you want about the created DOM.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.drawCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "drawCallback": function( settings ) {
		 *          alert( 'DataTables has redrawn the table' );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnDrawCallback": null,
	
	
		/**
		 * Identical to fnHeaderCallback() but for the table footer this function
		 * allows you to modify the table footer on every 'draw' event.
		 *  @type function
		 *  @param {node} foot "TR" element for the footer
		 *  @param {array} data Full table data (as derived from the original HTML)
		 *  @param {int} start Index for the current display starting point in the
		 *    display array
		 *  @param {int} end Index for the current display ending point in the
		 *    display array
		 *  @param {array int} display Index array to translate the visual position
		 *    to the full data array
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.footerCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "footerCallback": function( tfoot, data, start, end, display ) {
		 *          tfoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+start;
		 *        }
		 *      } );
		 *    } )
		 */
		"fnFooterCallback": null,
	
	
		/**
		 * When rendering large numbers in the information element for the table
		 * (i.e. "Showing 1 to 10 of 57 entries") DataTables will render large numbers
		 * to have a comma separator for the 'thousands' units (e.g. 1 million is
		 * rendered as "1,000,000") to help readability for the end user. This
		 * function will override the default method DataTables uses.
		 *  @type function
		 *  @member
		 *  @param {int} toFormat number to be formatted
		 *  @returns {string} formatted string for DataTables to show the number
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.formatNumber
		 *
		 *  @example
		 *    // Format a number using a single quote for the separator (note that
		 *    // this can also be done with the language.thousands option)
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "formatNumber": function ( toFormat ) {
		 *          return toFormat.toString().replace(
		 *            /\B(?=(\d{3})+(?!\d))/g, "'"
		 *          );
		 *        };
		 *      } );
		 *    } );
		 */
		"fnFormatNumber": function ( toFormat ) {
			return toFormat.toString().replace(
				/\B(?=(\d{3})+(?!\d))/g,
				this.oLanguage.sThousands
			);
		},
	
	
		/**
		 * This function is called on every 'draw' event, and allows you to
		 * dynamically modify the header row. This can be used to calculate and
		 * display useful information about the table.
		 *  @type function
		 *  @param {node} head "TR" element for the header
		 *  @param {array} data Full table data (as derived from the original HTML)
		 *  @param {int} start Index for the current display starting point in the
		 *    display array
		 *  @param {int} end Index for the current display ending point in the
		 *    display array
		 *  @param {array int} display Index array to translate the visual position
		 *    to the full data array
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.headerCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "fheaderCallback": function( head, data, start, end, display ) {
		 *          head.getElementsByTagName('th')[0].innerHTML = "Displaying "+(end-start)+" records";
		 *        }
		 *      } );
		 *    } )
		 */
		"fnHeaderCallback": null,
	
	
		/**
		 * The information element can be used to convey information about the current
		 * state of the table. Although the internationalisation options presented by
		 * DataTables are quite capable of dealing with most customisations, there may
		 * be times where you wish to customise the string further. This callback
		 * allows you to do exactly that.
		 *  @type function
		 *  @param {object} oSettings DataTables settings object
		 *  @param {int} start Starting position in data for the draw
		 *  @param {int} end End position in data for the draw
		 *  @param {int} max Total number of rows in the table (regardless of
		 *    filtering)
		 *  @param {int} total Total number of rows in the data set, after filtering
		 *  @param {string} pre The string that DataTables has formatted using it's
		 *    own rules
		 *  @returns {string} The string to be displayed in the information element.
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.infoCallback
		 *
		 *  @example
		 *    $('#example').dataTable( {
		 *      "infoCallback": function( settings, start, end, max, total, pre ) {
		 *        return start +" to "+ end;
		 *      }
		 *    } );
		 */
		"fnInfoCallback": null,
	
	
		/**
		 * Called when the table has been initialised. Normally DataTables will
		 * initialise sequentially and there will be no need for this function,
		 * however, this does not hold true when using external language information
		 * since that is obtained using an async XHR call.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} json The JSON object request from the server - only
		 *    present if client-side Ajax sourced data is used
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.initComplete
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "initComplete": function(settings, json) {
		 *          alert( 'DataTables has finished its initialisation.' );
		 *        }
		 *      } );
		 *    } )
		 */
		"fnInitComplete": null,
	
	
		/**
		 * Called at the very start of each table draw and can be used to cancel the
		 * draw by returning false, any other return (including undefined) results in
		 * the full draw occurring).
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @returns {boolean} False will cancel the draw, anything else (including no
		 *    return) will allow it to complete.
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.preDrawCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "preDrawCallback": function( settings ) {
		 *          if ( $('#test').val() == 1 ) {
		 *            return false;
		 *          }
		 *        }
		 *      } );
		 *    } );
		 */
		"fnPreDrawCallback": null,
	
	
		/**
		 * This function allows you to 'post process' each row after it have been
		 * generated for each table draw, but before it is rendered on screen. This
		 * function might be used for setting the row class name etc.
		 *  @type function
		 *  @param {node} row "TR" element for the current row
		 *  @param {array} data Raw data array for this row
		 *  @param {int} displayIndex The display index for the current table draw
		 *  @param {int} displayIndexFull The index of the data in the full list of
		 *    rows (after filtering)
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.rowCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "rowCallback": function( row, data, displayIndex, displayIndexFull ) {
		 *          // Bold the grade for all 'A' grade browsers
		 *          if ( data[4] == "A" ) {
		 *            $('td:eq(4)', row).html( '<b>A</b>' );
		 *          }
		 *        }
		 *      } );
		 *    } );
		 */
		"fnRowCallback": null,
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * This parameter allows you to override the default function which obtains
		 * the data from the server so something more suitable for your application.
		 * For example you could use POST data, or pull information from a Gears or
		 * AIR database.
		 *  @type function
		 *  @member
		 *  @param {string} source HTTP source to obtain the data from (`ajax`)
		 *  @param {array} data A key/value pair object containing the data to send
		 *    to the server
		 *  @param {function} callback to be called on completion of the data get
		 *    process that will draw the data on the page.
		 *  @param {object} settings DataTables settings object
		 *
		 *  @dtopt Callbacks
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverData
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"fnServerData": null,
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 *  It is often useful to send extra data to the server when making an Ajax
		 * request - for example custom filtering information, and this callback
		 * function makes it trivial to send extra information to the server. The
		 * passed in parameter is the data set that has been constructed by
		 * DataTables, and you can add to this or modify it as you require.
		 *  @type function
		 *  @param {array} data Data array (array of objects which are name/value
		 *    pairs) that has been constructed by DataTables and will be sent to the
		 *    server. In the case of Ajax sourced data with server-side processing
		 *    this will be an empty array, for server-side processing there will be a
		 *    significant number of parameters!
		 *  @returns {undefined} Ensure that you modify the data array passed in,
		 *    as this is passed by reference.
		 *
		 *  @dtopt Callbacks
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverParams
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"fnServerParams": null,
	
	
		/**
		 * Load the table state. With this function you can define from where, and how, the
		 * state of a table is loaded. By default DataTables will load from `localStorage`
		 * but you might wish to use a server-side database or cookies.
		 *  @type function
		 *  @member
		 *  @param {object} settings DataTables settings object
		 *  @param {object} callback Callback that can be executed when done. It
		 *    should be passed the loaded state object.
		 *  @return {object} The DataTables state object to be loaded
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateLoadCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoadCallback": function (settings, callback) {
		 *          $.ajax( {
		 *            "url": "/state_load",
		 *            "dataType": "json",
		 *            "success": function (json) {
		 *              callback( json );
		 *            }
		 *          } );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateLoadCallback": function ( settings ) {
			try {
				return JSON.parse(
					(settings.iStateDuration === -1 ? sessionStorage : localStorage).getItem(
						'DataTables_'+settings.sInstance+'_'+location.pathname
					)
				);
			} catch (e) {}
		},
	
	
		/**
		 * Callback which allows modification of the saved state prior to loading that state.
		 * This callback is called when the table is loading state from the stored data, but
		 * prior to the settings object being modified by the saved state. Note that for
		 * plug-in authors, you should use the `stateLoadParams` event to load parameters for
		 * a plug-in.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object that is to be loaded
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateLoadParams
		 *
		 *  @example
		 *    // Remove a saved filter, so filtering is never loaded
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoadParams": function (settings, data) {
		 *          data.oSearch.sSearch = "";
		 *        }
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Disallow state loading by returning false
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoadParams": function (settings, data) {
		 *          return false;
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateLoadParams": null,
	
	
		/**
		 * Callback that is called when the state has been loaded from the state saving method
		 * and the DataTables settings object has been modified as a result of the loaded state.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object that was loaded
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateLoaded
		 *
		 *  @example
		 *    // Show an alert with the filtering value that was saved
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateLoaded": function (settings, data) {
		 *          alert( 'Saved filter was: '+data.oSearch.sSearch );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateLoaded": null,
	
	
		/**
		 * Save the table state. This function allows you to define where and how the state
		 * information for the table is stored By default DataTables will use `localStorage`
		 * but you might wish to use a server-side database or cookies.
		 *  @type function
		 *  @member
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object to be saved
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateSaveCallback
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateSaveCallback": function (settings, data) {
		 *          // Send an Ajax request to the server with the state object
		 *          $.ajax( {
		 *            "url": "/state_save",
		 *            "data": data,
		 *            "dataType": "json",
		 *            "method": "POST"
		 *            "success": function () {}
		 *          } );
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateSaveCallback": function ( settings, data ) {
			try {
				(settings.iStateDuration === -1 ? sessionStorage : localStorage).setItem(
					'DataTables_'+settings.sInstance+'_'+location.pathname,
					JSON.stringify( data )
				);
			} catch (e) {}
		},
	
	
		/**
		 * Callback which allows modification of the state to be saved. Called when the table
		 * has changed state a new state save is required. This method allows modification of
		 * the state saving object prior to actually doing the save, including addition or
		 * other state properties or modification. Note that for plug-in authors, you should
		 * use the `stateSaveParams` event to save parameters for a plug-in.
		 *  @type function
		 *  @param {object} settings DataTables settings object
		 *  @param {object} data The state object to be saved
		 *
		 *  @dtopt Callbacks
		 *  @name DataTable.defaults.stateSaveParams
		 *
		 *  @example
		 *    // Remove a saved filter, so filtering is never saved
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateSave": true,
		 *        "stateSaveParams": function (settings, data) {
		 *          data.oSearch.sSearch = "";
		 *        }
		 *      } );
		 *    } );
		 */
		"fnStateSaveParams": null,
	
	
		/**
		 * Duration for which the saved state information is considered valid. After this period
		 * has elapsed the state will be returned to the default.
		 * Value is given in seconds.
		 *  @type int
		 *  @default 7200 <i>(2 hours)</i>
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.stateDuration
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "stateDuration": 60*60*24; // 1 day
		 *      } );
		 *    } )
		 */
		"iStateDuration": 7200,
	
	
		/**
		 * When enabled DataTables will not make a request to the server for the first
		 * page draw - rather it will use the data already on the page (no sorting etc
		 * will be applied to it), thus saving on an XHR at load time. `deferLoading`
		 * is used to indicate that deferred loading is required, but it is also used
		 * to tell DataTables how many records there are in the full table (allowing
		 * the information element and pagination to be displayed correctly). In the case
		 * where a filtering is applied to the table on initial load, this can be
		 * indicated by giving the parameter as an array, where the first element is
		 * the number of records available after filtering and the second element is the
		 * number of records without filtering (allowing the table information element
		 * to be shown correctly).
		 *  @type int | array
		 *  @default null
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.deferLoading
		 *
		 *  @example
		 *    // 57 records available in the table, no filtering applied
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "serverSide": true,
		 *        "ajax": "scripts/server_processing.php",
		 *        "deferLoading": 57
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // 57 records after filtering, 100 without filtering (an initial filter applied)
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "serverSide": true,
		 *        "ajax": "scripts/server_processing.php",
		 *        "deferLoading": [ 57, 100 ],
		 *        "search": {
		 *          "search": "my_filter"
		 *        }
		 *      } );
		 *    } );
		 */
		"iDeferLoading": null,
	
	
		/**
		 * Number of rows to display on a single page when using pagination. If
		 * feature enabled (`lengthChange`) then the end user will be able to override
		 * this to a custom setting using a pop-up menu.
		 *  @type int
		 *  @default 10
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.pageLength
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "pageLength": 50
		 *      } );
		 *    } )
		 */
		"iDisplayLength": 10,
	
	
		/**
		 * Define the starting point for data display when using DataTables with
		 * pagination. Note that this parameter is the number of records, rather than
		 * the page number, so if you have 10 records per page and want to start on
		 * the third page, it should be "20".
		 *  @type int
		 *  @default 0
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.displayStart
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "displayStart": 20
		 *      } );
		 *    } )
		 */
		"iDisplayStart": 0,
	
	
		/**
		 * By default DataTables allows keyboard navigation of the table (sorting, paging,
		 * and filtering) by adding a `tabindex` attribute to the required elements. This
		 * allows you to tab through the controls and press the enter key to activate them.
		 * The tabindex is default 0, meaning that the tab follows the flow of the document.
		 * You can overrule this using this parameter if you wish. Use a value of -1 to
		 * disable built-in keyboard navigation.
		 *  @type int
		 *  @default 0
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.tabIndex
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "tabIndex": 1
		 *      } );
		 *    } );
		 */
		"iTabIndex": 0,
	
	
		/**
		 * Classes that DataTables assigns to the various components and features
		 * that it adds to the HTML table. This allows classes to be configured
		 * during initialisation in addition to through the static
		 * {@link DataTable.ext.oStdClasses} object).
		 *  @namespace
		 *  @name DataTable.defaults.classes
		 */
		"oClasses": {},
	
	
		/**
		 * All strings that DataTables uses in the user interface that it creates
		 * are defined in this object, allowing you to modified them individually or
		 * completely replace them all as required.
		 *  @namespace
		 *  @name DataTable.defaults.language
		 */
		"oLanguage": {
			/**
			 * Strings that are used for WAI-ARIA labels and controls only (these are not
			 * actually visible on the page, but will be read by screenreaders, and thus
			 * must be internationalised as well).
			 *  @namespace
			 *  @name DataTable.defaults.language.aria
			 */
			"oAria": {
				/**
				 * ARIA label that is added to the table headers when the column may be
				 * sorted ascending by activing the column (click or return when focused).
				 * Note that the column header is prefixed to this string.
				 *  @type string
				 *  @default : activate to sort column ascending
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.aria.sortAscending
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "aria": {
				 *            "sortAscending": " - click/return to sort ascending"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sSortAscending": ": activate to sort column ascending",
	
				/**
				 * ARIA label that is added to the table headers when the column may be
				 * sorted descending by activing the column (click or return when focused).
				 * Note that the column header is prefixed to this string.
				 *  @type string
				 *  @default : activate to sort column ascending
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.aria.sortDescending
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "aria": {
				 *            "sortDescending": " - click/return to sort descending"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sSortDescending": ": activate to sort column descending"
			},
	
			/**
			 * Pagination string used by DataTables for the built-in pagination
			 * control types.
			 *  @namespace
			 *  @name DataTable.defaults.language.paginate
			 */
			"oPaginate": {
				/**
				 * Text to use when using the 'full_numbers' type of pagination for the
				 * button to take the user to the first page.
				 *  @type string
				 *  @default First
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.first
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "first": "First page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sFirst": "First",
	
	
				/**
				 * Text to use when using the 'full_numbers' type of pagination for the
				 * button to take the user to the last page.
				 *  @type string
				 *  @default Last
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.last
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "last": "Last page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sLast": "Last",
	
	
				/**
				 * Text to use for the 'next' pagination button (to take the user to the
				 * next page).
				 *  @type string
				 *  @default Next
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.next
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "next": "Next page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sNext": "Next",
	
	
				/**
				 * Text to use for the 'previous' pagination button (to take the user to
				 * the previous page).
				 *  @type string
				 *  @default Previous
				 *
				 *  @dtopt Language
				 *  @name DataTable.defaults.language.paginate.previous
				 *
				 *  @example
				 *    $(document).ready( function() {
				 *      $('#example').dataTable( {
				 *        "language": {
				 *          "paginate": {
				 *            "previous": "Previous page"
				 *          }
				 *        }
				 *      } );
				 *    } );
				 */
				"sPrevious": "Previous"
			},
	
			/**
			 * This string is shown in preference to `zeroRecords` when the table is
			 * empty of data (regardless of filtering). Note that this is an optional
			 * parameter - if it is not given, the value of `zeroRecords` will be used
			 * instead (either the default or given value).
			 *  @type string
			 *  @default No data available in table
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.emptyTable
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "emptyTable": "No data available in table"
			 *        }
			 *      } );
			 *    } );
			 */
			"sEmptyTable": "No data available in table",
	
	
			/**
			 * This string gives information to the end user about the information
			 * that is current on display on the page. The following tokens can be
			 * used in the string and will be dynamically replaced as the table
			 * display updates. This tokens can be placed anywhere in the string, or
			 * removed as needed by the language requires:
			 *
			 * * `\_START\_` - Display index of the first record on the current page
			 * * `\_END\_` - Display index of the last record on the current page
			 * * `\_TOTAL\_` - Number of records in the table after filtering
			 * * `\_MAX\_` - Number of records in the table without filtering
			 * * `\_PAGE\_` - Current page number
			 * * `\_PAGES\_` - Total number of pages of data in the table
			 *
			 *  @type string
			 *  @default Showing _START_ to _END_ of _TOTAL_ entries
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.info
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "info": "Showing page _PAGE_ of _PAGES_"
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
	
	
			/**
			 * Display information string for when the table is empty. Typically the
			 * format of this string should match `info`.
			 *  @type string
			 *  @default Showing 0 to 0 of 0 entries
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.infoEmpty
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "infoEmpty": "No entries to show"
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfoEmpty": "Showing 0 to 0 of 0 entries",
	
	
			/**
			 * When a user filters the information in a table, this string is appended
			 * to the information (`info`) to give an idea of how strong the filtering
			 * is. The variable _MAX_ is dynamically updated.
			 *  @type string
			 *  @default (filtered from _MAX_ total entries)
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.infoFiltered
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "infoFiltered": " - filtering from _MAX_ records"
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfoFiltered": "(filtered from _MAX_ total entries)",
	
	
			/**
			 * If can be useful to append extra information to the info string at times,
			 * and this variable does exactly that. This information will be appended to
			 * the `info` (`infoEmpty` and `infoFiltered` in whatever combination they are
			 * being used) at all times.
			 *  @type string
			 *  @default <i>Empty string</i>
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.infoPostFix
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "infoPostFix": "All records shown are derived from real information."
			 *        }
			 *      } );
			 *    } );
			 */
			"sInfoPostFix": "",
	
	
			/**
			 * This decimal place operator is a little different from the other
			 * language options since DataTables doesn't output floating point
			 * numbers, so it won't ever use this for display of a number. Rather,
			 * what this parameter does is modify the sort methods of the table so
			 * that numbers which are in a format which has a character other than
			 * a period (`.`) as a decimal place will be sorted numerically.
			 *
			 * Note that numbers with different decimal places cannot be shown in
			 * the same table and still be sortable, the table must be consistent.
			 * However, multiple different tables on the page can use different
			 * decimal place characters.
			 *  @type string
			 *  @default 
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.decimal
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "decimal": ","
			 *          "thousands": "."
			 *        }
			 *      } );
			 *    } );
			 */
			"sDecimal": "",
	
	
			/**
			 * DataTables has a build in number formatter (`formatNumber`) which is
			 * used to format large numbers that are used in the table information.
			 * By default a comma is used, but this can be trivially changed to any
			 * character you wish with this parameter.
			 *  @type string
			 *  @default ,
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.thousands
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "thousands": "'"
			 *        }
			 *      } );
			 *    } );
			 */
			"sThousands": ",",
	
	
			/**
			 * Detail the action that will be taken when the drop down menu for the
			 * pagination length option is changed. The '_MENU_' variable is replaced
			 * with a default select list of 10, 25, 50 and 100, and can be replaced
			 * with a custom select box if required.
			 *  @type string
			 *  @default Show _MENU_ entries
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.lengthMenu
			 *
			 *  @example
			 *    // Language change only
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "lengthMenu": "Display _MENU_ records"
			 *        }
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Language and options change
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "lengthMenu": 'Display <select>'+
			 *            '<option value="10">10</option>'+
			 *            '<option value="20">20</option>'+
			 *            '<option value="30">30</option>'+
			 *            '<option value="40">40</option>'+
			 *            '<option value="50">50</option>'+
			 *            '<option value="-1">All</option>'+
			 *            '</select> records'
			 *        }
			 *      } );
			 *    } );
			 */
			"sLengthMenu": "Show _MENU_ entries",
	
	
			/**
			 * When using Ajax sourced data and during the first draw when DataTables is
			 * gathering the data, this message is shown in an empty row in the table to
			 * indicate to the end user the the data is being loaded. Note that this
			 * parameter is not used when loading data by server-side processing, just
			 * Ajax sourced data with client-side processing.
			 *  @type string
			 *  @default Loading...
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.loadingRecords
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "loadingRecords": "Please wait - loading..."
			 *        }
			 *      } );
			 *    } );
			 */
			"sLoadingRecords": "Loading...",
	
	
			/**
			 * Text which is displayed when the table is processing a user action
			 * (usually a sort command or similar).
			 *  @type string
			 *  @default Processing...
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.processing
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "processing": "DataTables is currently busy"
			 *        }
			 *      } );
			 *    } );
			 */
			"sProcessing": "Processing...",
	
	
			/**
			 * Details the actions that will be taken when the user types into the
			 * filtering input text box. The variable "_INPUT_", if used in the string,
			 * is replaced with the HTML text box for the filtering input allowing
			 * control over where it appears in the string. If "_INPUT_" is not given
			 * then the input box is appended to the string automatically.
			 *  @type string
			 *  @default Search:
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.search
			 *
			 *  @example
			 *    // Input text box will be appended at the end automatically
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "search": "Filter records:"
			 *        }
			 *      } );
			 *    } );
			 *
			 *  @example
			 *    // Specify where the filter should appear
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "search": "Apply filter _INPUT_ to table"
			 *        }
			 *      } );
			 *    } );
			 */
			"sSearch": "Search:",
	
	
			/**
			 * Assign a `placeholder` attribute to the search `input` element
			 *  @type string
			 *  @default 
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.searchPlaceholder
			 */
			"sSearchPlaceholder": "",
	
	
			/**
			 * All of the language information can be stored in a file on the
			 * server-side, which DataTables will look up if this parameter is passed.
			 * It must store the URL of the language file, which is in a JSON format,
			 * and the object has the same properties as the oLanguage object in the
			 * initialiser object (i.e. the above parameters). Please refer to one of
			 * the example language files to see how this works in action.
			 *  @type string
			 *  @default <i>Empty string - i.e. disabled</i>
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.url
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "url": "http://www.sprymedia.co.uk/dataTables/lang.txt"
			 *        }
			 *      } );
			 *    } );
			 */
			"sUrl": "",
	
	
			/**
			 * Text shown inside the table records when the is no information to be
			 * displayed after filtering. `emptyTable` is shown when there is simply no
			 * information in the table at all (regardless of filtering).
			 *  @type string
			 *  @default No matching records found
			 *
			 *  @dtopt Language
			 *  @name DataTable.defaults.language.zeroRecords
			 *
			 *  @example
			 *    $(document).ready( function() {
			 *      $('#example').dataTable( {
			 *        "language": {
			 *          "zeroRecords": "No records to display"
			 *        }
			 *      } );
			 *    } );
			 */
			"sZeroRecords": "No matching records found"
		},
	
	
		/**
		 * This parameter allows you to have define the global filtering state at
		 * initialisation time. As an object the `search` parameter must be
		 * defined, but all other parameters are optional. When `regex` is true,
		 * the search string will be treated as a regular expression, when false
		 * (default) it will be treated as a straight string. When `smart`
		 * DataTables will use it's smart filtering methods (to word match at
		 * any point in the data), when false this will not be done.
		 *  @namespace
		 *  @extends DataTable.models.oSearch
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.search
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "search": {"search": "Initial search"}
		 *      } );
		 *    } )
		 */
		"oSearch": $.extend( {}, DataTable.models.oSearch ),
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * By default DataTables will look for the property `data` (or `aaData` for
		 * compatibility with DataTables 1.9-) when obtaining data from an Ajax
		 * source or for server-side processing - this parameter allows that
		 * property to be changed. You can use Javascript dotted object notation to
		 * get a data source for multiple levels of nesting.
		 *  @type string
		 *  @default data
		 *
		 *  @dtopt Options
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.ajaxDataProp
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"sAjaxDataProp": "data",
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * You can instruct DataTables to load data from an external
		 * source using this parameter (use aData if you want to pass data in you
		 * already have). Simply provide a url a JSON object can be obtained from.
		 *  @type string
		 *  @default null
		 *
		 *  @dtopt Options
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.ajaxSource
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"sAjaxSource": null,
	
	
		/**
		 * This initialisation variable allows you to specify exactly where in the
		 * DOM you want DataTables to inject the various controls it adds to the page
		 * (for example you might want the pagination controls at the top of the
		 * table). DIV elements (with or without a custom class) can also be added to
		 * aid styling. The follow syntax is used:
		 *   <ul>
		 *     <li>The following options are allowed:
		 *       <ul>
		 *         <li>'l' - Length changing</li>
		 *         <li>'f' - Filtering input</li>
		 *         <li>'t' - The table!</li>
		 *         <li>'i' - Information</li>
		 *         <li>'p' - Pagination</li>
		 *         <li>'r' - pRocessing</li>
		 *       </ul>
		 *     </li>
		 *     <li>The following constants are allowed:
		 *       <ul>
		 *         <li>'H' - jQueryUI theme "header" classes ('fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix')</li>
		 *         <li>'F' - jQueryUI theme "footer" classes ('fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix')</li>
		 *       </ul>
		 *     </li>
		 *     <li>The following syntax is expected:
		 *       <ul>
		 *         <li>'&lt;' and '&gt;' - div elements</li>
		 *         <li>'&lt;"class" and '&gt;' - div with a class</li>
		 *         <li>'&lt;"#id" and '&gt;' - div with an ID</li>
		 *       </ul>
		 *     </li>
		 *     <li>Examples:
		 *       <ul>
		 *         <li>'&lt;"wrapper"flipt&gt;'</li>
		 *         <li>'&lt;lf&lt;t&gt;ip&gt;'</li>
		 *       </ul>
		 *     </li>
		 *   </ul>
		 *  @type string
		 *  @default lfrtip <i>(when `jQueryUI` is false)</i> <b>or</b>
		 *    <"H"lfr>t<"F"ip> <i>(when `jQueryUI` is true)</i>
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.dom
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "dom": '&lt;"top"i&gt;rt&lt;"bottom"flp&gt;&lt;"clear"&gt;'
		 *      } );
		 *    } );
		 */
		"sDom": "lfrtip",
	
	
		/**
		 * Search delay option. This will throttle full table searches that use the
		 * DataTables provided search input element (it does not effect calls to
		 * `dt-api search()`, providing a delay before the search is made.
		 *  @type integer
		 *  @default 0
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.searchDelay
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "searchDelay": 200
		 *      } );
		 *    } )
		 */
		"searchDelay": null,
	
	
		/**
		 * DataTables features six different built-in options for the buttons to
		 * display for pagination control:
		 *
		 * * `numbers` - Page number buttons only
		 * * `simple` - 'Previous' and 'Next' buttons only
		 * * 'simple_numbers` - 'Previous' and 'Next' buttons, plus page numbers
		 * * `full` - 'First', 'Previous', 'Next' and 'Last' buttons
		 * * `full_numbers` - 'First', 'Previous', 'Next' and 'Last' buttons, plus page numbers
		 * * `first_last_numbers` - 'First' and 'Last' buttons, plus page numbers
		 *  
		 * Further methods can be added using {@link DataTable.ext.oPagination}.
		 *  @type string
		 *  @default simple_numbers
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.pagingType
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "pagingType": "full_numbers"
		 *      } );
		 *    } )
		 */
		"sPaginationType": "simple_numbers",
	
	
		/**
		 * Enable horizontal scrolling. When a table is too wide to fit into a
		 * certain layout, or you have a large number of columns in the table, you
		 * can enable x-scrolling to show the table in a viewport, which can be
		 * scrolled. This property can be `true` which will allow the table to
		 * scroll horizontally when needed, or any CSS unit, or a number (in which
		 * case it will be treated as a pixel measurement). Setting as simply `true`
		 * is recommended.
		 *  @type boolean|string
		 *  @default <i>blank string - i.e. disabled</i>
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.scrollX
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollX": true,
		 *        "scrollCollapse": true
		 *      } );
		 *    } );
		 */
		"sScrollX": "",
	
	
		/**
		 * This property can be used to force a DataTable to use more width than it
		 * might otherwise do when x-scrolling is enabled. For example if you have a
		 * table which requires to be well spaced, this parameter is useful for
		 * "over-sizing" the table, and thus forcing scrolling. This property can by
		 * any CSS unit, or a number (in which case it will be treated as a pixel
		 * measurement).
		 *  @type string
		 *  @default <i>blank string - i.e. disabled</i>
		 *
		 *  @dtopt Options
		 *  @name DataTable.defaults.scrollXInner
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollX": "100%",
		 *        "scrollXInner": "110%"
		 *      } );
		 *    } );
		 */
		"sScrollXInner": "",
	
	
		/**
		 * Enable vertical scrolling. Vertical scrolling will constrain the DataTable
		 * to the given height, and enable scrolling for any data which overflows the
		 * current viewport. This can be used as an alternative to paging to display
		 * a lot of data in a small area (although paging and scrolling can both be
		 * enabled at the same time). This property can be any CSS unit, or a number
		 * (in which case it will be treated as a pixel measurement).
		 *  @type string
		 *  @default <i>blank string - i.e. disabled</i>
		 *
		 *  @dtopt Features
		 *  @name DataTable.defaults.scrollY
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "scrollY": "200px",
		 *        "paginate": false
		 *      } );
		 *    } );
		 */
		"sScrollY": "",
	
	
		/**
		 * __Deprecated__ The functionality provided by this parameter has now been
		 * superseded by that provided through `ajax`, which should be used instead.
		 *
		 * Set the HTTP method that is used to make the Ajax call for server-side
		 * processing or Ajax sourced data.
		 *  @type string
		 *  @default GET
		 *
		 *  @dtopt Options
		 *  @dtopt Server-side
		 *  @name DataTable.defaults.serverMethod
		 *
		 *  @deprecated 1.10. Please use `ajax` for this functionality now.
		 */
		"sServerMethod": "GET",
	
	
		/**
		 * DataTables makes use of renderers when displaying HTML elements for
		 * a table. These renderers can be added or modified by plug-ins to
		 * generate suitable mark-up for a site. For example the Bootstrap
		 * integration plug-in for DataTables uses a paging button renderer to
		 * display pagination buttons in the mark-up required by Bootstrap.
		 *
		 * For further information about the renderers available see
		 * DataTable.ext.renderer
		 *  @type string|object
		 *  @default null
		 *
		 *  @name DataTable.defaults.renderer
		 *
		 */
		"renderer": null,
	
	
		/**
		 * Set the data property name that DataTables should use to get a row's id
		 * to set as the `id` property in the node.
		 *  @type string
		 *  @default DT_RowId
		 *
		 *  @name DataTable.defaults.rowId
		 */
		"rowId": "DT_RowId"
	};
	
	_fnHungarianMap( DataTable.defaults );
	
	
	
	/*
	 * Developer note - See note in model.defaults.js about the use of Hungarian
	 * notation and camel case.
	 */
	
	/**
	 * Column options that can be given to DataTables at initialisation time.
	 *  @namespace
	 */
	DataTable.defaults.column = {
		/**
		 * Define which column(s) an order will occur on for this column. This
		 * allows a column's ordering to take multiple columns into account when
		 * doing a sort or use the data from a different column. For example first
		 * name / last name columns make sense to do a multi-column sort over the
		 * two columns.
		 *  @type array|int
		 *  @default null <i>Takes the value of the column index automatically</i>
		 *
		 *  @name DataTable.defaults.column.orderData
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderData": [ 0, 1 ], "targets": [ 0 ] },
		 *          { "orderData": [ 1, 0 ], "targets": [ 1 ] },
		 *          { "orderData": 2, "targets": [ 2 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "orderData": [ 0, 1 ] },
		 *          { "orderData": [ 1, 0 ] },
		 *          { "orderData": 2 },
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"aDataSort": null,
		"iDataSort": -1,
	
	
		/**
		 * You can control the default ordering direction, and even alter the
		 * behaviour of the sort handler (i.e. only allow ascending ordering etc)
		 * using this parameter.
		 *  @type array
		 *  @default [ 'asc', 'desc' ]
		 *
		 *  @name DataTable.defaults.column.orderSequence
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderSequence": [ "asc" ], "targets": [ 1 ] },
		 *          { "orderSequence": [ "desc", "asc", "asc" ], "targets": [ 2 ] },
		 *          { "orderSequence": [ "desc" ], "targets": [ 3 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          { "orderSequence": [ "asc" ] },
		 *          { "orderSequence": [ "desc", "asc", "asc" ] },
		 *          { "orderSequence": [ "desc" ] },
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"asSorting": [ 'asc', 'desc' ],
	
	
		/**
		 * Enable or disable filtering on the data in this column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @name DataTable.defaults.column.searchable
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "searchable": false, "targets": [ 0 ] }
		 *        ] } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "searchable": false },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ] } );
		 *    } );
		 */
		"bSearchable": true,
	
	
		/**
		 * Enable or disable ordering on this column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @name DataTable.defaults.column.orderable
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderable": false, "targets": [ 0 ] }
		 *        ] } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "orderable": false },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ] } );
		 *    } );
		 */
		"bSortable": true,
	
	
		/**
		 * Enable or disable the display of this column.
		 *  @type boolean
		 *  @default true
		 *
		 *  @name DataTable.defaults.column.visible
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "visible": false, "targets": [ 0 ] }
		 *        ] } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "visible": false },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ] } );
		 *    } );
		 */
		"bVisible": true,
	
	
		/**
		 * Developer definable function that is called whenever a cell is created (Ajax source,
		 * etc) or processed for input (DOM source). This can be used as a compliment to mRender
		 * allowing you to modify the DOM element (add background colour for example) when the
		 * element is available.
		 *  @type function
		 *  @param {element} td The TD node that has been created
		 *  @param {*} cellData The Data for the cell
		 *  @param {array|object} rowData The data for the whole row
		 *  @param {int} row The row index for the aoData data store
		 *  @param {int} col The column index for aoColumns
		 *
		 *  @name DataTable.defaults.column.createdCell
		 *  @dtopt Columns
		 *
		 *  @example
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [3],
		 *          "createdCell": function (td, cellData, rowData, row, col) {
		 *            if ( cellData == "1.7" ) {
		 *              $(td).css('color', 'blue')
		 *            }
		 *          }
		 *        } ]
		 *      });
		 *    } );
		 */
		"fnCreatedCell": null,
	
	
		/**
		 * This parameter has been replaced by `data` in DataTables to ensure naming
		 * consistency. `dataProp` can still be used, as there is backwards
		 * compatibility in DataTables for this option, but it is strongly
		 * recommended that you use `data` in preference to `dataProp`.
		 *  @name DataTable.defaults.column.dataProp
		 */
	
	
		/**
		 * This property can be used to read data from any data source property,
		 * including deeply nested objects / properties. `data` can be given in a
		 * number of different ways which effect its behaviour:
		 *
		 * * `integer` - treated as an array index for the data source. This is the
		 *   default that DataTables uses (incrementally increased for each column).
		 * * `string` - read an object property from the data source. There are
		 *   three 'special' options that can be used in the string to alter how
		 *   DataTables reads the data from the source object:
		 *    * `.` - Dotted Javascript notation. Just as you use a `.` in
		 *      Javascript to read from nested objects, so to can the options
		 *      specified in `data`. For example: `browser.version` or
		 *      `browser.name`. If your object parameter name contains a period, use
		 *      `\\` to escape it - i.e. `first\\.name`.
		 *    * `[]` - Array notation. DataTables can automatically combine data
		 *      from and array source, joining the data with the characters provided
		 *      between the two brackets. For example: `name[, ]` would provide a
		 *      comma-space separated list from the source array. If no characters
		 *      are provided between the brackets, the original array source is
		 *      returned.
		 *    * `()` - Function notation. Adding `()` to the end of a parameter will
		 *      execute a function of the name given. For example: `browser()` for a
		 *      simple function on the data source, `browser.version()` for a
		 *      function in a nested property or even `browser().version` to get an
		 *      object property if the function called returns an object. Note that
		 *      function notation is recommended for use in `render` rather than
		 *      `data` as it is much simpler to use as a renderer.
		 * * `null` - use the original data source for the row rather than plucking
		 *   data directly from it. This action has effects on two other
		 *   initialisation options:
		 *    * `defaultContent` - When null is given as the `data` option and
		 *      `defaultContent` is specified for the column, the value defined by
		 *      `defaultContent` will be used for the cell.
		 *    * `render` - When null is used for the `data` option and the `render`
		 *      option is specified for the column, the whole data source for the
		 *      row is used for the renderer.
		 * * `function` - the function given will be executed whenever DataTables
		 *   needs to set or get the data for a cell in the column. The function
		 *   takes three parameters:
		 *    * Parameters:
		 *      * `{array|object}` The data source for the row
		 *      * `{string}` The type call data requested - this will be 'set' when
		 *        setting data or 'filter', 'display', 'type', 'sort' or undefined
		 *        when gathering data. Note that when `undefined` is given for the
		 *        type DataTables expects to get the raw data for the object back<
		 *      * `{*}` Data to set when the second parameter is 'set'.
		 *    * Return:
		 *      * The return value from the function is not required when 'set' is
		 *        the type of call, but otherwise the return is what will be used
		 *        for the data requested.
		 *
		 * Note that `data` is a getter and setter option. If you just require
		 * formatting of data for output, you will likely want to use `render` which
		 * is simply a getter and thus simpler to use.
		 *
		 * Note that prior to DataTables 1.9.2 `data` was called `mDataProp`. The
		 * name change reflects the flexibility of this property and is consistent
		 * with the naming of mRender. If 'mDataProp' is given, then it will still
		 * be used by DataTables, as it automatically maps the old name to the new
		 * if required.
		 *
		 *  @type string|int|function|null
		 *  @default null <i>Use automatically calculated column index</i>
		 *
		 *  @name DataTable.defaults.column.data
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Read table data from objects
		 *    // JSON structure for each row:
		 *    //   {
		 *    //      "engine": {value},
		 *    //      "browser": {value},
		 *    //      "platform": {value},
		 *    //      "version": {value},
		 *    //      "grade": {value}
		 *    //   }
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajaxSource": "sources/objects.txt",
		 *        "columns": [
		 *          { "data": "engine" },
		 *          { "data": "browser" },
		 *          { "data": "platform" },
		 *          { "data": "version" },
		 *          { "data": "grade" }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Read information from deeply nested objects
		 *    // JSON structure for each row:
		 *    //   {
		 *    //      "engine": {value},
		 *    //      "browser": {value},
		 *    //      "platform": {
		 *    //         "inner": {value}
		 *    //      },
		 *    //      "details": [
		 *    //         {value}, {value}
		 *    //      ]
		 *    //   }
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajaxSource": "sources/deep.txt",
		 *        "columns": [
		 *          { "data": "engine" },
		 *          { "data": "browser" },
		 *          { "data": "platform.inner" },
		 *          { "data": "details.0" },
		 *          { "data": "details.1" }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `data` as a function to provide different information for
		 *    // sorting, filtering and display. In this case, currency (price)
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": function ( source, type, val ) {
		 *            if (type === 'set') {
		 *              source.price = val;
		 *              // Store the computed dislay and filter values for efficiency
		 *              source.price_display = val=="" ? "" : "$"+numberFormat(val);
		 *              source.price_filter  = val=="" ? "" : "$"+numberFormat(val)+" "+val;
		 *              return;
		 *            }
		 *            else if (type === 'display') {
		 *              return source.price_display;
		 *            }
		 *            else if (type === 'filter') {
		 *              return source.price_filter;
		 *            }
		 *            // 'sort', 'type' and undefined all just use the integer
		 *            return source.price;
		 *          }
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using default content
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": null,
		 *          "defaultContent": "Click to edit"
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using array notation - outputting a list from an array
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": "name[, ]"
		 *        } ]
		 *      } );
		 *    } );
		 *
		 */
		"mData": null,
	
	
		/**
		 * This property is the rendering partner to `data` and it is suggested that
		 * when you want to manipulate data for display (including filtering,
		 * sorting etc) without altering the underlying data for the table, use this
		 * property. `render` can be considered to be the the read only companion to
		 * `data` which is read / write (then as such more complex). Like `data`
		 * this option can be given in a number of different ways to effect its
		 * behaviour:
		 *
		 * * `integer` - treated as an array index for the data source. This is the
		 *   default that DataTables uses (incrementally increased for each column).
		 * * `string` - read an object property from the data source. There are
		 *   three 'special' options that can be used in the string to alter how
		 *   DataTables reads the data from the source object:
		 *    * `.` - Dotted Javascript notation. Just as you use a `.` in
		 *      Javascript to read from nested objects, so to can the options
		 *      specified in `data`. For example: `browser.version` or
		 *      `browser.name`. If your object parameter name contains a period, use
		 *      `\\` to escape it - i.e. `first\\.name`.
		 *    * `[]` - Array notation. DataTables can automatically combine data
		 *      from and array source, joining the data with the characters provided
		 *      between the two brackets. For example: `name[, ]` would provide a
		 *      comma-space separated list from the source array. If no characters
		 *      are provided between the brackets, the original array source is
		 *      returned.
		 *    * `()` - Function notation. Adding `()` to the end of a parameter will
		 *      execute a function of the name given. For example: `browser()` for a
		 *      simple function on the data source, `browser.version()` for a
		 *      function in a nested property or even `browser().version` to get an
		 *      object property if the function called returns an object.
		 * * `object` - use different data for the different data types requested by
		 *   DataTables ('filter', 'display', 'type' or 'sort'). The property names
		 *   of the object is the data type the property refers to and the value can
		 *   defined using an integer, string or function using the same rules as
		 *   `render` normally does. Note that an `_` option _must_ be specified.
		 *   This is the default value to use if you haven't specified a value for
		 *   the data type requested by DataTables.
		 * * `function` - the function given will be executed whenever DataTables
		 *   needs to set or get the data for a cell in the column. The function
		 *   takes three parameters:
		 *    * Parameters:
		 *      * {array|object} The data source for the row (based on `data`)
		 *      * {string} The type call data requested - this will be 'filter',
		 *        'display', 'type' or 'sort'.
		 *      * {array|object} The full data source for the row (not based on
		 *        `data`)
		 *    * Return:
		 *      * The return value from the function is what will be used for the
		 *        data requested.
		 *
		 *  @type string|int|function|object|null
		 *  @default null Use the data source value.
		 *
		 *  @name DataTable.defaults.column.render
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Create a comma separated list from an array of objects
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "ajaxSource": "sources/deep.txt",
		 *        "columns": [
		 *          { "data": "engine" },
		 *          { "data": "browser" },
		 *          {
		 *            "data": "platform",
		 *            "render": "[, ].name"
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Execute a function to obtain data
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": null, // Use the full data source object for the renderer's source
		 *          "render": "browserName()"
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // As an object, extracting different data for the different types
		 *    // This would be used with a data source such as:
		 *    //   { "phone": 5552368, "phone_filter": "5552368 555-2368", "phone_display": "555-2368" }
		 *    // Here the `phone` integer is used for sorting and type detection, while `phone_filter`
		 *    // (which has both forms) is used for filtering for if a user inputs either format, while
		 *    // the formatted phone number is the one that is shown in the table.
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": null, // Use the full data source object for the renderer's source
		 *          "render": {
		 *            "_": "phone",
		 *            "filter": "phone_filter",
		 *            "display": "phone_display"
		 *          }
		 *        } ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Use as a function to create a link from the data source
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "data": "download_link",
		 *          "render": function ( data, type, full ) {
		 *            return '<a href="'+data+'">Download</a>';
		 *          }
		 *        } ]
		 *      } );
		 *    } );
		 */
		"mRender": null,
	
	
		/**
		 * Change the cell type created for the column - either TD cells or TH cells. This
		 * can be useful as TH cells have semantic meaning in the table body, allowing them
		 * to act as a header for a row (you may wish to add scope='row' to the TH elements).
		 *  @type string
		 *  @default td
		 *
		 *  @name DataTable.defaults.column.cellType
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Make the first column use TH cells
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [ {
		 *          "targets": [ 0 ],
		 *          "cellType": "th"
		 *        } ]
		 *      } );
		 *    } );
		 */
		"sCellType": "td",
	
	
		/**
		 * Class to give to each cell in this column.
		 *  @type string
		 *  @default <i>Empty string</i>
		 *
		 *  @name DataTable.defaults.column.class
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "class": "my_class", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "class": "my_class" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sClass": "",
	
		/**
		 * When DataTables calculates the column widths to assign to each column,
		 * it finds the longest string in each column and then constructs a
		 * temporary table and reads the widths from that. The problem with this
		 * is that "mmm" is much wider then "iiii", but the latter is a longer
		 * string - thus the calculation can go wrong (doing it properly and putting
		 * it into an DOM object and measuring that is horribly(!) slow). Thus as
		 * a "work around" we provide this option. It will append its value to the
		 * text that is found to be the longest string for the column - i.e. padding.
		 * Generally you shouldn't need this!
		 *  @type string
		 *  @default <i>Empty string<i>
		 *
		 *  @name DataTable.defaults.column.contentPadding
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          null,
		 *          null,
		 *          {
		 *            "contentPadding": "mmm"
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sContentPadding": "",
	
	
		/**
		 * Allows a default value to be given for a column's data, and will be used
		 * whenever a null data source is encountered (this can be because `data`
		 * is set to null, or because the data source itself is null).
		 *  @type string
		 *  @default null
		 *
		 *  @name DataTable.defaults.column.defaultContent
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          {
		 *            "data": null,
		 *            "defaultContent": "Edit",
		 *            "targets": [ -1 ]
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          null,
		 *          null,
		 *          {
		 *            "data": null,
		 *            "defaultContent": "Edit"
		 *          }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sDefaultContent": null,
	
	
		/**
		 * This parameter is only used in DataTables' server-side processing. It can
		 * be exceptionally useful to know what columns are being displayed on the
		 * client side, and to map these to database fields. When defined, the names
		 * also allow DataTables to reorder information from the server if it comes
		 * back in an unexpected order (i.e. if you switch your columns around on the
		 * client-side, your server-side code does not also need updating).
		 *  @type string
		 *  @default <i>Empty string</i>
		 *
		 *  @name DataTable.defaults.column.name
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "name": "engine", "targets": [ 0 ] },
		 *          { "name": "browser", "targets": [ 1 ] },
		 *          { "name": "platform", "targets": [ 2 ] },
		 *          { "name": "version", "targets": [ 3 ] },
		 *          { "name": "grade", "targets": [ 4 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "name": "engine" },
		 *          { "name": "browser" },
		 *          { "name": "platform" },
		 *          { "name": "version" },
		 *          { "name": "grade" }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sName": "",
	
	
		/**
		 * Defines a data source type for the ordering which can be used to read
		 * real-time information from the table (updating the internally cached
		 * version) prior to ordering. This allows ordering to occur on user
		 * editable elements such as form inputs.
		 *  @type string
		 *  @default std
		 *
		 *  @name DataTable.defaults.column.orderDataType
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "orderDataType": "dom-text", "targets": [ 2, 3 ] },
		 *          { "type": "numeric", "targets": [ 3 ] },
		 *          { "orderDataType": "dom-select", "targets": [ 4 ] },
		 *          { "orderDataType": "dom-checkbox", "targets": [ 5 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          null,
		 *          null,
		 *          { "orderDataType": "dom-text" },
		 *          { "orderDataType": "dom-text", "type": "numeric" },
		 *          { "orderDataType": "dom-select" },
		 *          { "orderDataType": "dom-checkbox" }
		 *        ]
		 *      } );
		 *    } );
		 */
		"sSortDataType": "std",
	
	
		/**
		 * The title of this column.
		 *  @type string
		 *  @default null <i>Derived from the 'TH' value for this column in the
		 *    original HTML table.</i>
		 *
		 *  @name DataTable.defaults.column.title
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "title": "My column title", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "title": "My column title" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sTitle": null,
	
	
		/**
		 * The type allows you to specify how the data for this column will be
		 * ordered. Four types (string, numeric, date and html (which will strip
		 * HTML tags before ordering)) are currently available. Note that only date
		 * formats understood by Javascript's Date() object will be accepted as type
		 * date. For example: "Mar 26, 2008 5:03 PM". May take the values: 'string',
		 * 'numeric', 'date' or 'html' (by default). Further types can be adding
		 * through plug-ins.
		 *  @type string
		 *  @default null <i>Auto-detected from raw data</i>
		 *
		 *  @name DataTable.defaults.column.type
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "type": "html", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "type": "html" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sType": null,
	
	
		/**
		 * Defining the width of the column, this parameter may take any CSS value
		 * (3em, 20px etc). DataTables applies 'smart' widths to columns which have not
		 * been given a specific width through this interface ensuring that the table
		 * remains readable.
		 *  @type string
		 *  @default null <i>Automatic</i>
		 *
		 *  @name DataTable.defaults.column.width
		 *  @dtopt Columns
		 *
		 *  @example
		 *    // Using `columnDefs`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columnDefs": [
		 *          { "width": "20%", "targets": [ 0 ] }
		 *        ]
		 *      } );
		 *    } );
		 *
		 *  @example
		 *    // Using `columns`
		 *    $(document).ready( function() {
		 *      $('#example').dataTable( {
		 *        "columns": [
		 *          { "width": "20%" },
		 *          null,
		 *          null,
		 *          null,
		 *          null
		 *        ]
		 *      } );
		 *    } );
		 */
		"sWidth": null
	};
	
	_fnHungarianMap( DataTable.defaults.column );
	
	
	
	/**
	 * DataTables settings object - this holds all the information needed for a
	 * given table, including configuration, data and current application of the
	 * table options. DataTables does not have a single instance for each DataTable
	 * with the settings attached to that instance, but rather instances of the
	 * DataTable "class" are created on-the-fly as needed (typically by a
	 * $().dataTable() call) and the settings object is then applied to that
	 * instance.
	 *
	 * Note that this object is related to {@link DataTable.defaults} but this
	 * one is the internal data store for DataTables's cache of columns. It should
	 * NOT be manipulated outside of DataTables. Any configuration should be done
	 * through the initialisation options.
	 *  @namespace
	 *  @todo Really should attach the settings object to individual instances so we
	 *    don't need to create new instances on each $().dataTable() call (if the
	 *    table already exists). It would also save passing oSettings around and
	 *    into every single function. However, this is a very significant
	 *    architecture change for DataTables and will almost certainly break
	 *    backwards compatibility with older installations. This is something that
	 *    will be done in 2.0.
	 */
	DataTable.models.oSettings = {
		/**
		 * Primary features of DataTables and their enablement state.
		 *  @namespace
		 */
		"oFeatures": {
	
			/**
			 * Flag to say if DataTables should automatically try to calculate the
			 * optimum table and columns widths (true) or not (false).
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bAutoWidth": null,
	
			/**
			 * Delay the creation of TR and TD elements until they are actually
			 * needed by a driven page draw. This can give a significant speed
			 * increase for Ajax source and Javascript source data, but makes no
			 * difference at all fro DOM and server-side processing tables.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bDeferRender": null,
	
			/**
			 * Enable filtering on the table or not. Note that if this is disabled
			 * then there is no filtering at all on the table, including fnFilter.
			 * To just remove the filtering input use sDom and remove the 'f' option.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bFilter": null,
	
			/**
			 * Table information element (the 'Showing x of y records' div) enable
			 * flag.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bInfo": null,
	
			/**
			 * Present a user control allowing the end user to change the page size
			 * when pagination is enabled.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bLengthChange": null,
	
			/**
			 * Pagination enabled or not. Note that if this is disabled then length
			 * changing must also be disabled.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bPaginate": null,
	
			/**
			 * Processing indicator enable flag whenever DataTables is enacting a
			 * user request - typically an Ajax request for server-side processing.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bProcessing": null,
	
			/**
			 * Server-side processing enabled flag - when enabled DataTables will
			 * get all data from the server for every draw - there is no filtering,
			 * sorting or paging done on the client-side.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bServerSide": null,
	
			/**
			 * Sorting enablement flag.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bSort": null,
	
			/**
			 * Multi-column sorting
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bSortMulti": null,
	
			/**
			 * Apply a class to the columns which are being sorted to provide a
			 * visual highlight or not. This can slow things down when enabled since
			 * there is a lot of DOM interaction.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bSortClasses": null,
	
			/**
			 * State saving enablement flag.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bStateSave": null
		},
	
	
		/**
		 * Scrolling settings for a table.
		 *  @namespace
		 */
		"oScroll": {
			/**
			 * When the table is shorter in height than sScrollY, collapse the
			 * table container down to the height of the table (when true).
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type boolean
			 */
			"bCollapse": null,
	
			/**
			 * Width of the scrollbar for the web-browser's platform. Calculated
			 * during table initialisation.
			 *  @type int
			 *  @default 0
			 */
			"iBarWidth": 0,
	
			/**
			 * Viewport width for horizontal scrolling. Horizontal scrolling is
			 * disabled if an empty string.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 */
			"sX": null,
	
			/**
			 * Width to expand the table to when using x-scrolling. Typically you
			 * should not need to use this.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 *  @deprecated
			 */
			"sXInner": null,
	
			/**
			 * Viewport height for vertical scrolling. Vertical scrolling is disabled
			 * if an empty string.
			 * Note that this parameter will be set by the initialisation routine. To
			 * set a default use {@link DataTable.defaults}.
			 *  @type string
			 */
			"sY": null
		},
	
		/**
		 * Language information for the table.
		 *  @namespace
		 *  @extends DataTable.defaults.oLanguage
		 */
		"oLanguage": {
			/**
			 * Information callback function. See
			 * {@link DataTable.defaults.fnInfoCallback}
			 *  @type function
			 *  @default null
			 */
			"fnInfoCallback": null
		},
	
		/**
		 * Browser support parameters
		 *  @namespace
		 */
		"oBrowser": {
			/**
			 * Indicate if the browser incorrectly calculates width:100% inside a
			 * scrolling element (IE6/7)
			 *  @type boolean
			 *  @default false
			 */
			"bScrollOversize": false,
	
			/**
			 * Determine if the vertical scrollbar is on the right or left of the
			 * scrolling container - needed for rtl language layout, although not
			 * all browsers move the scrollbar (Safari).
			 *  @type boolean
			 *  @default false
			 */
			"bScrollbarLeft": false,
	
			/**
			 * Flag for if `getBoundingClientRect` is fully supported or not
			 *  @type boolean
			 *  @default false
			 */
			"bBounding": false,
	
			/**
			 * Browser scrollbar width
			 *  @type integer
			 *  @default 0
			 */
			"barWidth": 0
		},
	
	
		"ajax": null,
	
	
		/**
		 * Array referencing the nodes which are used for the features. The
		 * parameters of this object match what is allowed by sDom - i.e.
		 *   <ul>
		 *     <li>'l' - Length changing</li>
		 *     <li>'f' - Filtering input</li>
		 *     <li>'t' - The table!</li>
		 *     <li>'i' - Information</li>
		 *     <li>'p' - Pagination</li>
		 *     <li>'r' - pRocessing</li>
		 *   </ul>
		 *  @type array
		 *  @default []
		 */
		"aanFeatures": [],
	
		/**
		 * Store data information - see {@link DataTable.models.oRow} for detailed
		 * information.
		 *  @type array
		 *  @default []
		 */
		"aoData": [],
	
		/**
		 * Array of indexes which are in the current display (after filtering etc)
		 *  @type array
		 *  @default []
		 */
		"aiDisplay": [],
	
		/**
		 * Array of indexes for display - no filtering
		 *  @type array
		 *  @default []
		 */
		"aiDisplayMaster": [],
	
		/**
		 * Map of row ids to data indexes
		 *  @type object
		 *  @default {}
		 */
		"aIds": {},
	
		/**
		 * Store information about each column that is in use
		 *  @type array
		 *  @default []
		 */
		"aoColumns": [],
	
		/**
		 * Store information about the table's header
		 *  @type array
		 *  @default []
		 */
		"aoHeader": [],
	
		/**
		 * Store information about the table's footer
		 *  @type array
		 *  @default []
		 */
		"aoFooter": [],
	
		/**
		 * Store the applied global search information in case we want to force a
		 * research or compare the old search to a new one.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @namespace
		 *  @extends DataTable.models.oSearch
		 */
		"oPreviousSearch": {},
	
		/**
		 * Store the applied search for each column - see
		 * {@link DataTable.models.oSearch} for the format that is used for the
		 * filtering information for each column.
		 *  @type array
		 *  @default []
		 */
		"aoPreSearchCols": [],
	
		/**
		 * Sorting that is applied to the table. Note that the inner arrays are
		 * used in the following manner:
		 * <ul>
		 *   <li>Index 0 - column number</li>
		 *   <li>Index 1 - current sorting direction</li>
		 * </ul>
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @todo These inner arrays should really be objects
		 */
		"aaSorting": null,
	
		/**
		 * Sorting that is always applied to the table (i.e. prefixed in front of
		 * aaSorting).
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @default []
		 */
		"aaSortingFixed": [],
	
		/**
		 * Classes to use for the striping of a table.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @default []
		 */
		"asStripeClasses": null,
	
		/**
		 * If restoring a table - we should restore its striping classes as well
		 *  @type array
		 *  @default []
		 */
		"asDestroyStripes": [],
	
		/**
		 * If restoring a table - we should restore its width
		 *  @type int
		 *  @default 0
		 */
		"sDestroyWidth": 0,
	
		/**
		 * Callback functions array for every time a row is inserted (i.e. on a draw).
		 *  @type array
		 *  @default []
		 */
		"aoRowCallback": [],
	
		/**
		 * Callback functions for the header on each draw.
		 *  @type array
		 *  @default []
		 */
		"aoHeaderCallback": [],
	
		/**
		 * Callback function for the footer on each draw.
		 *  @type array
		 *  @default []
		 */
		"aoFooterCallback": [],
	
		/**
		 * Array of callback functions for draw callback functions
		 *  @type array
		 *  @default []
		 */
		"aoDrawCallback": [],
	
		/**
		 * Array of callback functions for row created function
		 *  @type array
		 *  @default []
		 */
		"aoRowCreatedCallback": [],
	
		/**
		 * Callback functions for just before the table is redrawn. A return of
		 * false will be used to cancel the draw.
		 *  @type array
		 *  @default []
		 */
		"aoPreDrawCallback": [],
	
		/**
		 * Callback functions for when the table has been initialised.
		 *  @type array
		 *  @default []
		 */
		"aoInitComplete": [],
	
	
		/**
		 * Callbacks for modifying the settings to be stored for state saving, prior to
		 * saving state.
		 *  @type array
		 *  @default []
		 */
		"aoStateSaveParams": [],
	
		/**
		 * Callbacks for modifying the settings that have been stored for state saving
		 * prior to using the stored values to restore the state.
		 *  @type array
		 *  @default []
		 */
		"aoStateLoadParams": [],
	
		/**
		 * Callbacks for operating on the settings object once the saved state has been
		 * loaded
		 *  @type array
		 *  @default []
		 */
		"aoStateLoaded": [],
	
		/**
		 * Cache the table ID for quick access
		 *  @type string
		 *  @default <i>Empty string</i>
		 */
		"sTableId": "",
	
		/**
		 * The TABLE node for the main table
		 *  @type node
		 *  @default null
		 */
		"nTable": null,
	
		/**
		 * Permanent ref to the thead element
		 *  @type node
		 *  @default null
		 */
		"nTHead": null,
	
		/**
		 * Permanent ref to the tfoot element - if it exists
		 *  @type node
		 *  @default null
		 */
		"nTFoot": null,
	
		/**
		 * Permanent ref to the tbody element
		 *  @type node
		 *  @default null
		 */
		"nTBody": null,
	
		/**
		 * Cache the wrapper node (contains all DataTables controlled elements)
		 *  @type node
		 *  @default null
		 */
		"nTableWrapper": null,
	
		/**
		 * Indicate if when using server-side processing the loading of data
		 * should be deferred until the second draw.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type boolean
		 *  @default false
		 */
		"bDeferLoading": false,
	
		/**
		 * Indicate if all required information has been read in
		 *  @type boolean
		 *  @default false
		 */
		"bInitialised": false,
	
		/**
		 * Information about open rows. Each object in the array has the parameters
		 * 'nTr' and 'nParent'
		 *  @type array
		 *  @default []
		 */
		"aoOpenRows": [],
	
		/**
		 * Dictate the positioning of DataTables' control elements - see
		 * {@link DataTable.model.oInit.sDom}.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 *  @default null
		 */
		"sDom": null,
	
		/**
		 * Search delay (in mS)
		 *  @type integer
		 *  @default null
		 */
		"searchDelay": null,
	
		/**
		 * Which type of pagination should be used.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 *  @default two_button
		 */
		"sPaginationType": "two_button",
	
		/**
		 * The state duration (for `stateSave`) in seconds.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type int
		 *  @default 0
		 */
		"iStateDuration": 0,
	
		/**
		 * Array of callback functions for state saving. Each array element is an
		 * object with the following parameters:
		 *   <ul>
		 *     <li>function:fn - function to call. Takes two parameters, oSettings
		 *       and the JSON string to save that has been thus far created. Returns
		 *       a JSON string to be inserted into a json object
		 *       (i.e. '"param": [ 0, 1, 2]')</li>
		 *     <li>string:sName - name of callback</li>
		 *   </ul>
		 *  @type array
		 *  @default []
		 */
		"aoStateSave": [],
	
		/**
		 * Array of callback functions for state loading. Each array element is an
		 * object with the following parameters:
		 *   <ul>
		 *     <li>function:fn - function to call. Takes two parameters, oSettings
		 *       and the object stored. May return false to cancel state loading</li>
		 *     <li>string:sName - name of callback</li>
		 *   </ul>
		 *  @type array
		 *  @default []
		 */
		"aoStateLoad": [],
	
		/**
		 * State that was saved. Useful for back reference
		 *  @type object
		 *  @default null
		 */
		"oSavedState": null,
	
		/**
		 * State that was loaded. Useful for back reference
		 *  @type object
		 *  @default null
		 */
		"oLoadedState": null,
	
		/**
		 * Source url for AJAX data for the table.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 *  @default null
		 */
		"sAjaxSource": null,
	
		/**
		 * Property from a given object from which to read the table data from. This
		 * can be an empty string (when not server-side processing), in which case
		 * it is  assumed an an array is given directly.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 */
		"sAjaxDataProp": null,
	
		/**
		 * Note if draw should be blocked while getting data
		 *  @type boolean
		 *  @default true
		 */
		"bAjaxDataGet": true,
	
		/**
		 * The last jQuery XHR object that was used for server-side data gathering.
		 * This can be used for working with the XHR information in one of the
		 * callbacks
		 *  @type object
		 *  @default null
		 */
		"jqXHR": null,
	
		/**
		 * JSON returned from the server in the last Ajax request
		 *  @type object
		 *  @default undefined
		 */
		"json": undefined,
	
		/**
		 * Data submitted as part of the last Ajax request
		 *  @type object
		 *  @default undefined
		 */
		"oAjaxData": undefined,
	
		/**
		 * Function to get the server-side data.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type function
		 */
		"fnServerData": null,
	
		/**
		 * Functions which are called prior to sending an Ajax request so extra
		 * parameters can easily be sent to the server
		 *  @type array
		 *  @default []
		 */
		"aoServerParams": [],
	
		/**
		 * Send the XHR HTTP method - GET or POST (could be PUT or DELETE if
		 * required).
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type string
		 */
		"sServerMethod": null,
	
		/**
		 * Format numbers for display.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type function
		 */
		"fnFormatNumber": null,
	
		/**
		 * List of options that can be used for the user selectable length menu.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type array
		 *  @default []
		 */
		"aLengthMenu": null,
	
		/**
		 * Counter for the draws that the table does. Also used as a tracker for
		 * server-side processing
		 *  @type int
		 *  @default 0
		 */
		"iDraw": 0,
	
		/**
		 * Indicate if a redraw is being done - useful for Ajax
		 *  @type boolean
		 *  @default false
		 */
		"bDrawing": false,
	
		/**
		 * Draw index (iDraw) of the last error when parsing the returned data
		 *  @type int
		 *  @default -1
		 */
		"iDrawError": -1,
	
		/**
		 * Paging display length
		 *  @type int
		 *  @default 10
		 */
		"_iDisplayLength": 10,
	
		/**
		 * Paging start point - aiDisplay index
		 *  @type int
		 *  @default 0
		 */
		"_iDisplayStart": 0,
	
		/**
		 * Server-side processing - number of records in the result set
		 * (i.e. before filtering), Use fnRecordsTotal rather than
		 * this property to get the value of the number of records, regardless of
		 * the server-side processing setting.
		 *  @type int
		 *  @default 0
		 *  @private
		 */
		"_iRecordsTotal": 0,
	
		/**
		 * Server-side processing - number of records in the current display set
		 * (i.e. after filtering). Use fnRecordsDisplay rather than
		 * this property to get the value of the number of records, regardless of
		 * the server-side processing setting.
		 *  @type boolean
		 *  @default 0
		 *  @private
		 */
		"_iRecordsDisplay": 0,
	
		/**
		 * The classes to use for the table
		 *  @type object
		 *  @default {}
		 */
		"oClasses": {},
	
		/**
		 * Flag attached to the settings object so you can check in the draw
		 * callback if filtering has been done in the draw. Deprecated in favour of
		 * events.
		 *  @type boolean
		 *  @default false
		 *  @deprecated
		 */
		"bFiltered": false,
	
		/**
		 * Flag attached to the settings object so you can check in the draw
		 * callback if sorting has been done in the draw. Deprecated in favour of
		 * events.
		 *  @type boolean
		 *  @default false
		 *  @deprecated
		 */
		"bSorted": false,
	
		/**
		 * Indicate that if multiple rows are in the header and there is more than
		 * one unique cell per column, if the top one (true) or bottom one (false)
		 * should be used for sorting / title by DataTables.
		 * Note that this parameter will be set by the initialisation routine. To
		 * set a default use {@link DataTable.defaults}.
		 *  @type boolean
		 */
		"bSortCellsTop": null,
	
		/**
		 * Initialisation object that is used for the table
		 *  @type object
		 *  @default null
		 */
		"oInit": null,
	
		/**
		 * Destroy callback functions - for plug-ins to attach themselves to the
		 * destroy so they can clean up markup and events.
		 *  @type array
		 *  @default []
		 */
		"aoDestroyCallback": [],
	
	
		/**
		 * Get the number of records in the current record set, before filtering
		 *  @type function
		 */
		"fnRecordsTotal": function ()
		{
			return _fnDataSource( this ) == 'ssp' ?
				this._iRecordsTotal * 1 :
				this.aiDisplayMaster.length;
		},
	
		/**
		 * Get the number of records in the current record set, after filtering
		 *  @type function
		 */
		"fnRecordsDisplay": function ()
		{
			return _fnDataSource( this ) == 'ssp' ?
				this._iRecordsDisplay * 1 :
				this.aiDisplay.length;
		},
	
		/**
		 * Get the display end point - aiDisplay index
		 *  @type function
		 */
		"fnDisplayEnd": function ()
		{
			var
				len      = this._iDisplayLength,
				start    = this._iDisplayStart,
				calc     = start + len,
				records  = this.aiDisplay.length,
				features = this.oFeatures,
				paginate = features.bPaginate;
	
			if ( features.bServerSide ) {
				return paginate === false || len === -1 ?
					start + records :
					Math.min( start+len, this._iRecordsDisplay );
			}
			else {
				return ! paginate || calc>records || len===-1 ?
					records :
					calc;
			}
		},
	
		/**
		 * The DataTables object for this table
		 *  @type object
		 *  @default null
		 */
		"oInstance": null,
	
		/**
		 * Unique identifier for each instance of the DataTables object. If there
		 * is an ID on the table node, then it takes that value, otherwise an
		 * incrementing internal counter is used.
		 *  @type string
		 *  @default null
		 */
		"sInstance": null,
	
		/**
		 * tabindex attribute value that is added to DataTables control elements, allowing
		 * keyboard navigation of the table and its controls.
		 */
		"iTabIndex": 0,
	
		/**
		 * DIV container for the footer scrolling table if scrolling
		 */
		"nScrollHead": null,
	
		/**
		 * DIV container for the footer scrolling table if scrolling
		 */
		"nScrollFoot": null,
	
		/**
		 * Last applied sort
		 *  @type array
		 *  @default []
		 */
		"aLastSort": [],
	
		/**
		 * Stored plug-in instances
		 *  @type object
		 *  @default {}
		 */
		"oPlugins": {},
	
		/**
		 * Function used to get a row's id from the row's data
		 *  @type function
		 *  @default null
		 */
		"rowIdFn": null,
	
		/**
		 * Data location where to store a row's id
		 *  @type string
		 *  @default null
		 */
		"rowId": null
	};

	/**
	 * Extension object for DataTables that is used to provide all extension
	 * options.
	 *
	 * Note that the `DataTable.ext` object is available through
	 * `jQuery.fn.dataTable.ext` where it may be accessed and manipulated. It is
	 * also aliased to `jQuery.fn.dataTableExt` for historic reasons.
	 *  @namespace
	 *  @extends DataTable.models.ext
	 */
	
	
	/**
	 * DataTables extensions
	 * 
	 * This namespace acts as a collection area for plug-ins that can be used to
	 * extend DataTables capabilities. Indeed many of the build in methods
	 * use this method to provide their own capabilities (sorting methods for
	 * example).
	 *
	 * Note that this namespace is aliased to `jQuery.fn.dataTableExt` for legacy
	 * reasons
	 *
	 *  @namespace
	 */
	DataTable.ext = _ext = {
		/**
		 * Buttons. For use with the Buttons extension for DataTables. This is
		 * defined here so other extensions can define buttons regardless of load
		 * order. It is _not_ used by DataTables core.
		 *
		 *  @type object
		 *  @default {}
		 */
		buttons: {},
	
	
		/**
		 * Element class names
		 *
		 *  @type object
		 *  @default {}
		 */
		classes: {},
	
	
		/**
		 * DataTables build type (expanded by the download builder)
		 *
		 *  @type string
		 */
		build:"bs4/dt-1.10.18/e-1.8.1/r-2.2.2",
	
	
		/**
		 * Error reporting.
		 * 
		 * How should DataTables report an error. Can take the value 'alert',
		 * 'throw', 'none' or a function.
		 *
		 *  @type string|function
		 *  @default alert
		 */
		errMode: "alert",
	
	
		/**
		 * Feature plug-ins.
		 * 
		 * This is an array of objects which describe the feature plug-ins that are
		 * available to DataTables. These feature plug-ins are then available for
		 * use through the `dom` initialisation option.
		 * 
		 * Each feature plug-in is described by an object which must have the
		 * following properties:
		 * 
		 * * `fnInit` - function that is used to initialise the plug-in,
		 * * `cFeature` - a character so the feature can be enabled by the `dom`
		 *   instillation option. This is case sensitive.
		 *
		 * The `fnInit` function has the following input parameters:
		 *
		 * 1. `{object}` DataTables settings object: see
		 *    {@link DataTable.models.oSettings}
		 *
		 * And the following return is expected:
		 * 
		 * * {node|null} The element which contains your feature. Note that the
		 *   return may also be void if your plug-in does not require to inject any
		 *   DOM elements into DataTables control (`dom`) - for example this might
		 *   be useful when developing a plug-in which allows table control via
		 *   keyboard entry
		 *
		 *  @type array
		 *
		 *  @example
		 *    $.fn.dataTable.ext.features.push( {
		 *      "fnInit": function( oSettings ) {
		 *        return new TableTools( { "oDTSettings": oSettings } );
		 *      },
		 *      "cFeature": "T"
		 *    } );
		 */
		feature: [],
	
	
		/**
		 * Row searching.
		 * 
		 * This method of searching is complimentary to the default type based
		 * searching, and a lot more comprehensive as it allows you complete control
		 * over the searching logic. Each element in this array is a function
		 * (parameters described below) that is called for every row in the table,
		 * and your logic decides if it should be included in the searching data set
		 * or not.
		 *
		 * Searching functions have the following input parameters:
		 *
		 * 1. `{object}` DataTables settings object: see
		 *    {@link DataTable.models.oSettings}
		 * 2. `{array|object}` Data for the row to be processed (same as the
		 *    original format that was passed in as the data source, or an array
		 *    from a DOM data source
		 * 3. `{int}` Row index ({@link DataTable.models.oSettings.aoData}), which
		 *    can be useful to retrieve the `TR` element if you need DOM interaction.
		 *
		 * And the following return is expected:
		 *
		 * * {boolean} Include the row in the searched result set (true) or not
		 *   (false)
		 *
		 * Note that as with the main search ability in DataTables, technically this
		 * is "filtering", since it is subtractive. However, for consistency in
		 * naming we call it searching here.
		 *
		 *  @type array
		 *  @default []
		 *
		 *  @example
		 *    // The following example shows custom search being applied to the
		 *    // fourth column (i.e. the data[3] index) based on two input values
		 *    // from the end-user, matching the data in a certain range.
		 *    $.fn.dataTable.ext.search.push(
		 *      function( settings, data, dataIndex ) {
		 *        var min = document.getElementById('min').value * 1;
		 *        var max = document.getElementById('max').value * 1;
		 *        var version = data[3] == "-" ? 0 : data[3]*1;
		 *
		 *        if ( min == "" && max == "" ) {
		 *          return true;
		 *        }
		 *        else if ( min == "" && version < max ) {
		 *          return true;
		 *        }
		 *        else if ( min < version && "" == max ) {
		 *          return true;
		 *        }
		 *        else if ( min < version && version < max ) {
		 *          return true;
		 *        }
		 *        return false;
		 *      }
		 *    );
		 */
		search: [],
	
	
		/**
		 * Selector extensions
		 *
		 * The `selector` option can be used to extend the options available for the
		 * selector modifier options (`selector-modifier` object data type) that
		 * each of the three built in selector types offer (row, column and cell +
		 * their plural counterparts). For example the Select extension uses this
		 * mechanism to provide an option to select only rows, columns and cells
		 * that have been marked as selected by the end user (`{selected: true}`),
		 * which can be used in conjunction with the existing built in selector
		 * options.
		 *
		 * Each property is an array to which functions can be pushed. The functions
		 * take three attributes:
		 *
		 * * Settings object for the host table
		 * * Options object (`selector-modifier` object type)
		 * * Array of selected item indexes
		 *
		 * The return is an array of the resulting item indexes after the custom
		 * selector has been applied.
		 *
		 *  @type object
		 */
		selector: {
			cell: [],
			column: [],
			row: []
		},
	
	
		/**
		 * Internal functions, exposed for used in plug-ins.
		 * 
		 * Please note that you should not need to use the internal methods for
		 * anything other than a plug-in (and even then, try to avoid if possible).
		 * The internal function may change between releases.
		 *
		 *  @type object
		 *  @default {}
		 */
		internal: {},
	
	
		/**
		 * Legacy configuration options. Enable and disable legacy options that
		 * are available in DataTables.
		 *
		 *  @type object
		 */
		legacy: {
			/**
			 * Enable / disable DataTables 1.9 compatible server-side processing
			 * requests
			 *
			 *  @type boolean
			 *  @default null
			 */
			ajax: null
		},
	
	
		/**
		 * Pagination plug-in methods.
		 * 
		 * Each entry in this object is a function and defines which buttons should
		 * be shown by the pagination rendering method that is used for the table:
		 * {@link DataTable.ext.renderer.pageButton}. The renderer addresses how the
		 * buttons are displayed in the document, while the functions here tell it
		 * what buttons to display. This is done by returning an array of button
		 * descriptions (what each button will do).
		 *
		 * Pagination types (the four built in options and any additional plug-in
		 * options defined here) can be used through the `paginationType`
		 * initialisation parameter.
		 *
		 * The functions defined take two parameters:
		 *
		 * 1. `{int} page` The current page index
		 * 2. `{int} pages` The number of pages in the table
		 *
		 * Each function is expected to return an array where each element of the
		 * array can be one of:
		 *
		 * * `first` - Jump to first page when activated
		 * * `last` - Jump to last page when activated
		 * * `previous` - Show previous page when activated
		 * * `next` - Show next page when activated
		 * * `{int}` - Show page of the index given
		 * * `{array}` - A nested array containing the above elements to add a
		 *   containing 'DIV' element (might be useful for styling).
		 *
		 * Note that DataTables v1.9- used this object slightly differently whereby
		 * an object with two functions would be defined for each plug-in. That
		 * ability is still supported by DataTables 1.10+ to provide backwards
		 * compatibility, but this option of use is now decremented and no longer
		 * documented in DataTables 1.10+.
		 *
		 *  @type object
		 *  @default {}
		 *
		 *  @example
		 *    // Show previous, next and current page buttons only
		 *    $.fn.dataTableExt.oPagination.current = function ( page, pages ) {
		 *      return [ 'previous', page, 'next' ];
		 *    };
		 */
		pager: {},
	
	
		renderer: {
			pageButton: {},
			header: {}
		},
	
	
		/**
		 * Ordering plug-ins - custom data source
		 * 
		 * The extension options for ordering of data available here is complimentary
		 * to the default type based ordering that DataTables typically uses. It
		 * allows much greater control over the the data that is being used to
		 * order a column, but is necessarily therefore more complex.
		 * 
		 * This type of ordering is useful if you want to do ordering based on data
		 * live from the DOM (for example the contents of an 'input' element) rather
		 * than just the static string that DataTables knows of.
		 * 
		 * The way these plug-ins work is that you create an array of the values you
		 * wish to be ordering for the column in question and then return that
		 * array. The data in the array much be in the index order of the rows in
		 * the table (not the currently ordering order!). Which order data gathering
		 * function is run here depends on the `dt-init columns.orderDataType`
		 * parameter that is used for the column (if any).
		 *
		 * The functions defined take two parameters:
		 *
		 * 1. `{object}` DataTables settings object: see
		 *    {@link DataTable.models.oSettings}
		 * 2. `{int}` Target column index
		 *
		 * Each function is expected to return an array:
		 *
		 * * `{array}` Data for the column to be ordering upon
		 *
		 *  @type array
		 *
		 *  @example
		 *    // Ordering using `input` node values
		 *    $.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
		 *    {
		 *      return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
		 *        return $('input', td).val();
		 *      } );
		 *    }
		 */
		order: {},
	
	
		/**
		 * Type based plug-ins.
		 *
		 * Each column in DataTables has a type assigned to it, either by automatic
		 * detection or by direct assignment using the `type` option for the column.
		 * The type of a column will effect how it is ordering and search (plug-ins
		 * can also make use of the column type if required).
		 *
		 * @namespace
		 */
		type: {
			/**
			 * Type detection functions.
			 *
			 * The functions defined in this object are used to automatically detect
			 * a column's type, making initialisation of DataTables super easy, even
			 * when complex data is in the table.
			 *
			 * The functions defined take two parameters:
			 *
		     *  1. `{*}` Data from the column cell to be analysed
		     *  2. `{settings}` DataTables settings object. This can be used to
		     *     perform context specific type detection - for example detection
		     *     based on language settings such as using a comma for a decimal
		     *     place. Generally speaking the options from the settings will not
		     *     be required
			 *
			 * Each function is expected to return:
			 *
			 * * `{string|null}` Data type detected, or null if unknown (and thus
			 *   pass it on to the other type detection functions.
			 *
			 *  @type array
			 *
			 *  @example
			 *    // Currency type detection plug-in:
			 *    $.fn.dataTable.ext.type.detect.push(
			 *      function ( data, settings ) {
			 *        // Check the numeric part
			 *        if ( ! data.substring(1).match(/[0-9]/) ) {
			 *          return null;
			 *        }
			 *
			 *        // Check prefixed by currency
			 *        if ( data.charAt(0) == '$' || data.charAt(0) == '&pound;' ) {
			 *          return 'currency';
			 *        }
			 *        return null;
			 *      }
			 *    );
			 */
			detect: [],
	
	
			/**
			 * Type based search formatting.
			 *
			 * The type based searching functions can be used to pre-format the
			 * data to be search on. For example, it can be used to strip HTML
			 * tags or to de-format telephone numbers for numeric only searching.
			 *
			 * Note that is a search is not defined for a column of a given type,
			 * no search formatting will be performed.
			 * 
			 * Pre-processing of searching data plug-ins - When you assign the sType
			 * for a column (or have it automatically detected for you by DataTables
			 * or a type detection plug-in), you will typically be using this for
			 * custom sorting, but it can also be used to provide custom searching
			 * by allowing you to pre-processing the data and returning the data in
			 * the format that should be searched upon. This is done by adding
			 * functions this object with a parameter name which matches the sType
			 * for that target column. This is the corollary of <i>afnSortData</i>
			 * for searching data.
			 *
			 * The functions defined take a single parameter:
			 *
		     *  1. `{*}` Data from the column cell to be prepared for searching
			 *
			 * Each function is expected to return:
			 *
			 * * `{string|null}` Formatted string that will be used for the searching.
			 *
			 *  @type object
			 *  @default {}
			 *
			 *  @example
			 *    $.fn.dataTable.ext.type.search['title-numeric'] = function ( d ) {
			 *      return d.replace(/\n/g," ").replace( /<.*?>/g, "" );
			 *    }
			 */
			search: {},
	
	
			/**
			 * Type based ordering.
			 *
			 * The column type tells DataTables what ordering to apply to the table
			 * when a column is sorted upon. The order for each type that is defined,
			 * is defined by the functions available in this object.
			 *
			 * Each ordering option can be described by three properties added to
			 * this object:
			 *
			 * * `{type}-pre` - Pre-formatting function
			 * * `{type}-asc` - Ascending order function
			 * * `{type}-desc` - Descending order function
			 *
			 * All three can be used together, only `{type}-pre` or only
			 * `{type}-asc` and `{type}-desc` together. It is generally recommended
			 * that only `{type}-pre` is used, as this provides the optimal
			 * implementation in terms of speed, although the others are provided
			 * for compatibility with existing Javascript sort functions.
			 *
			 * `{type}-pre`: Functions defined take a single parameter:
			 *
		     *  1. `{*}` Data from the column cell to be prepared for ordering
			 *
			 * And return:
			 *
			 * * `{*}` Data to be sorted upon
			 *
			 * `{type}-asc` and `{type}-desc`: Functions are typical Javascript sort
			 * functions, taking two parameters:
			 *
		     *  1. `{*}` Data to compare to the second parameter
		     *  2. `{*}` Data to compare to the first parameter
			 *
			 * And returning:
			 *
			 * * `{*}` Ordering match: <0 if first parameter should be sorted lower
			 *   than the second parameter, ===0 if the two parameters are equal and
			 *   >0 if the first parameter should be sorted height than the second
			 *   parameter.
			 * 
			 *  @type object
			 *  @default {}
			 *
			 *  @example
			 *    // Numeric ordering of formatted numbers with a pre-formatter
			 *    $.extend( $.fn.dataTable.ext.type.order, {
			 *      "string-pre": function(x) {
			 *        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
			 *        return parseFloat( a );
			 *      }
			 *    } );
			 *
			 *  @example
			 *    // Case-sensitive string ordering, with no pre-formatting method
			 *    $.extend( $.fn.dataTable.ext.order, {
			 *      "string-case-asc": function(x,y) {
			 *        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
			 *      },
			 *      "string-case-desc": function(x,y) {
			 *        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
			 *      }
			 *    } );
			 */
			order: {}
		},
	
		/**
		 * Unique DataTables instance counter
		 *
		 * @type int
		 * @private
		 */
		_unique: 0,
	
	
		//
		// Depreciated
		// The following properties are retained for backwards compatiblity only.
		// The should not be used in new projects and will be removed in a future
		// version
		//
	
		/**
		 * Version check function.
		 *  @type function
		 *  @depreciated Since 1.10
		 */
		fnVersionCheck: DataTable.fnVersionCheck,
	
	
		/**
		 * Index for what 'this' index API functions should use
		 *  @type int
		 *  @deprecated Since v1.10
		 */
		iApiIndex: 0,
	
	
		/**
		 * jQuery UI class container
		 *  @type object
		 *  @deprecated Since v1.10
		 */
		oJUIClasses: {},
	
	
		/**
		 * Software version
		 *  @type string
		 *  @deprecated Since v1.10
		 */
		sVersion: DataTable.version
	};
	
	
	//
	// Backwards compatibility. Alias to pre 1.10 Hungarian notation counter parts
	//
	$.extend( _ext, {
		afnFiltering: _ext.search,
		aTypes:       _ext.type.detect,
		ofnSearch:    _ext.type.search,
		oSort:        _ext.type.order,
		afnSortData:  _ext.order,
		aoFeatures:   _ext.feature,
		oApi:         _ext.internal,
		oStdClasses:  _ext.classes,
		oPagination:  _ext.pager
	} );
	
	
	$.extend( DataTable.ext.classes, {
		"sTable": "dataTable",
		"sNoFooter": "no-footer",
	
		/* Paging buttons */
		"sPageButton": "paginate_button",
		"sPageButtonActive": "current",
		"sPageButtonDisabled": "disabled",
	
		/* Striping classes */
		"sStripeOdd": "odd",
		"sStripeEven": "even",
	
		/* Empty row */
		"sRowEmpty": "dataTables_empty",
	
		/* Features */
		"sWrapper": "dataTables_wrapper",
		"sFilter": "dataTables_filter",
		"sInfo": "dataTables_info",
		"sPaging": "dataTables_paginate paging_", /* Note that the type is postfixed */
		"sLength": "dataTables_length",
		"sProcessing": "dataTables_processing",
	
		/* Sorting */
		"sSortAsc": "sorting_asc",
		"sSortDesc": "sorting_desc",
		"sSortable": "sorting", /* Sortable in both directions */
		"sSortableAsc": "sorting_asc_disabled",
		"sSortableDesc": "sorting_desc_disabled",
		"sSortableNone": "sorting_disabled",
		"sSortColumn": "sorting_", /* Note that an int is postfixed for the sorting order */
	
		/* Filtering */
		"sFilterInput": "",
	
		/* Page length */
		"sLengthSelect": "",
	
		/* Scrolling */
		"sScrollWrapper": "dataTables_scroll",
		"sScrollHead": "dataTables_scrollHead",
		"sScrollHeadInner": "dataTables_scrollHeadInner",
		"sScrollBody": "dataTables_scrollBody",
		"sScrollFoot": "dataTables_scrollFoot",
		"sScrollFootInner": "dataTables_scrollFootInner",
	
		/* Misc */
		"sHeaderTH": "",
		"sFooterTH": "",
	
		// Deprecated
		"sSortJUIAsc": "",
		"sSortJUIDesc": "",
		"sSortJUI": "",
		"sSortJUIAscAllowed": "",
		"sSortJUIDescAllowed": "",
		"sSortJUIWrapper": "",
		"sSortIcon": "",
		"sJUIHeader": "",
		"sJUIFooter": ""
	} );
	
	
	var extPagination = DataTable.ext.pager;
	
	function _numbers ( page, pages ) {
		var
			numbers = [],
			buttons = extPagination.numbers_length,
			half = Math.floor( buttons / 2 ),
			i = 1;
	
		if ( pages <= buttons ) {
			numbers = _range( 0, pages );
		}
		else if ( page <= half ) {
			numbers = _range( 0, buttons-2 );
			numbers.push( 'ellipsis' );
			numbers.push( pages-1 );
		}
		else if ( page >= pages - 1 - half ) {
			numbers = _range( pages-(buttons-2), pages );
			numbers.splice( 0, 0, 'ellipsis' ); // no unshift in ie6
			numbers.splice( 0, 0, 0 );
		}
		else {
			numbers = _range( page-half+2, page+half-1 );
			numbers.push( 'ellipsis' );
			numbers.push( pages-1 );
			numbers.splice( 0, 0, 'ellipsis' );
			numbers.splice( 0, 0, 0 );
		}
	
		numbers.DT_el = 'span';
		return numbers;
	}
	
	
	$.extend( extPagination, {
		simple: function ( page, pages ) {
			return [ 'previous', 'next' ];
		},
	
		full: function ( page, pages ) {
			return [  'first', 'previous', 'next', 'last' ];
		},
	
		numbers: function ( page, pages ) {
			return [ _numbers(page, pages) ];
		},
	
		simple_numbers: function ( page, pages ) {
			return [ 'previous', _numbers(page, pages), 'next' ];
		},
	
		full_numbers: function ( page, pages ) {
			return [ 'first', 'previous', _numbers(page, pages), 'next', 'last' ];
		},
		
		first_last_numbers: function (page, pages) {
	 		return ['first', _numbers(page, pages), 'last'];
	 	},
	
		// For testing and plug-ins to use
		_numbers: _numbers,
	
		// Number of number buttons (including ellipsis) to show. _Must be odd!_
		numbers_length: 7
	} );
	
	
	$.extend( true, DataTable.ext.renderer, {
		pageButton: {
			_: function ( settings, host, idx, buttons, page, pages ) {
				var classes = settings.oClasses;
				var lang = settings.oLanguage.oPaginate;
				var aria = settings.oLanguage.oAria.paginate || {};
				var btnDisplay, btnClass, counter=0;
	
				var attach = function( container, buttons ) {
					var i, ien, node, button;
					var clickHandler = function ( e ) {
						_fnPageChange( settings, e.data.action, true );
					};
	
					for ( i=0, ien=buttons.length ; i<ien ; i++ ) {
						button = buttons[i];
	
						if ( $.isArray( button ) ) {
							var inner = $( '<'+(button.DT_el || 'div')+'/>' )
								.appendTo( container );
							attach( inner, button );
						}
						else {
							btnDisplay = null;
							btnClass = '';
	
							switch ( button ) {
								case 'ellipsis':
									container.append('<span class="ellipsis">&#x2026;</span>');
									break;
	
								case 'first':
									btnDisplay = lang.sFirst;
									btnClass = button + (page > 0 ?
										'' : ' '+classes.sPageButtonDisabled);
									break;
	
								case 'previous':
									btnDisplay = lang.sPrevious;
									btnClass = button + (page > 0 ?
										'' : ' '+classes.sPageButtonDisabled);
									break;
	
								case 'next':
									btnDisplay = lang.sNext;
									btnClass = button + (page < pages-1 ?
										'' : ' '+classes.sPageButtonDisabled);
									break;
	
								case 'last':
									btnDisplay = lang.sLast;
									btnClass = button + (page < pages-1 ?
										'' : ' '+classes.sPageButtonDisabled);
									break;
	
								default:
									btnDisplay = button + 1;
									btnClass = page === button ?
										classes.sPageButtonActive : '';
									break;
							}
	
							if ( btnDisplay !== null ) {
								node = $('<a>', {
										'class': classes.sPageButton+' '+btnClass,
										'aria-controls': settings.sTableId,
										'aria-label': aria[ button ],
										'data-dt-idx': counter,
										'tabindex': settings.iTabIndex,
										'id': idx === 0 && typeof button === 'string' ?
											settings.sTableId +'_'+ button :
											null
									} )
									.html( btnDisplay )
									.appendTo( container );
	
								_fnBindAction(
									node, {action: button}, clickHandler
								);
	
								counter++;
							}
						}
					}
				};
	
				// IE9 throws an 'unknown error' if document.activeElement is used
				// inside an iframe or frame. Try / catch the error. Not good for
				// accessibility, but neither are frames.
				var activeEl;
	
				try {
					// Because this approach is destroying and recreating the paging
					// elements, focus is lost on the select button which is bad for
					// accessibility. So we want to restore focus once the draw has
					// completed
					activeEl = $(host).find(document.activeElement).data('dt-idx');
				}
				catch (e) {}
	
				attach( $(host).empty(), buttons );
	
				if ( activeEl !== undefined ) {
					$(host).find( '[data-dt-idx='+activeEl+']' ).focus();
				}
			}
		}
	} );
	
	
	
	// Built in type detection. See model.ext.aTypes for information about
	// what is required from this methods.
	$.extend( DataTable.ext.type.detect, [
		// Plain numbers - first since V8 detects some plain numbers as dates
		// e.g. Date.parse('55') (but not all, e.g. Date.parse('22')...).
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _isNumber( d, decimal ) ? 'num'+decimal : null;
		},
	
		// Dates (only those recognised by the browser's Date.parse)
		function ( d, settings )
		{
			// V8 tries _very_ hard to make a string passed into `Date.parse()`
			// valid, so we need to use a regex to restrict date formats. Use a
			// plug-in for anything other than ISO8601 style strings
			if ( d && !(d instanceof Date) && ! _re_date.test(d) ) {
				return null;
			}
			var parsed = Date.parse(d);
			return (parsed !== null && !isNaN(parsed)) || _empty(d) ? 'date' : null;
		},
	
		// Formatted numbers
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _isNumber( d, decimal, true ) ? 'num-fmt'+decimal : null;
		},
	
		// HTML numeric
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _htmlNumeric( d, decimal ) ? 'html-num'+decimal : null;
		},
	
		// HTML numeric, formatted
		function ( d, settings )
		{
			var decimal = settings.oLanguage.sDecimal;
			return _htmlNumeric( d, decimal, true ) ? 'html-num-fmt'+decimal : null;
		},
	
		// HTML (this is strict checking - there must be html)
		function ( d, settings )
		{
			return _empty( d ) || (typeof d === 'string' && d.indexOf('<') !== -1) ?
				'html' : null;
		}
	] );
	
	
	
	// Filter formatting functions. See model.ext.ofnSearch for information about
	// what is required from these methods.
	// 
	// Note that additional search methods are added for the html numbers and
	// html formatted numbers by `_addNumericSort()` when we know what the decimal
	// place is
	
	
	$.extend( DataTable.ext.type.search, {
		html: function ( data ) {
			return _empty(data) ?
				data :
				typeof data === 'string' ?
					data
						.replace( _re_new_lines, " " )
						.replace( _re_html, "" ) :
					'';
		},
	
		string: function ( data ) {
			return _empty(data) ?
				data :
				typeof data === 'string' ?
					data.replace( _re_new_lines, " " ) :
					data;
		}
	} );
	
	
	
	var __numericReplace = function ( d, decimalPlace, re1, re2 ) {
		if ( d !== 0 && (!d || d === '-') ) {
			return -Infinity;
		}
	
		// If a decimal place other than `.` is used, it needs to be given to the
		// function so we can detect it and replace with a `.` which is the only
		// decimal place Javascript recognises - it is not locale aware.
		if ( decimalPlace ) {
			d = _numToDecimal( d, decimalPlace );
		}
	
		if ( d.replace ) {
			if ( re1 ) {
				d = d.replace( re1, '' );
			}
	
			if ( re2 ) {
				d = d.replace( re2, '' );
			}
		}
	
		return d * 1;
	};
	
	
	// Add the numeric 'deformatting' functions for sorting and search. This is done
	// in a function to provide an easy ability for the language options to add
	// additional methods if a non-period decimal place is used.
	function _addNumericSort ( decimalPlace ) {
		$.each(
			{
				// Plain numbers
				"num": function ( d ) {
					return __numericReplace( d, decimalPlace );
				},
	
				// Formatted numbers
				"num-fmt": function ( d ) {
					return __numericReplace( d, decimalPlace, _re_formatted_numeric );
				},
	
				// HTML numeric
				"html-num": function ( d ) {
					return __numericReplace( d, decimalPlace, _re_html );
				},
	
				// HTML numeric, formatted
				"html-num-fmt": function ( d ) {
					return __numericReplace( d, decimalPlace, _re_html, _re_formatted_numeric );
				}
			},
			function ( key, fn ) {
				// Add the ordering method
				_ext.type.order[ key+decimalPlace+'-pre' ] = fn;
	
				// For HTML types add a search formatter that will strip the HTML
				if ( key.match(/^html\-/) ) {
					_ext.type.search[ key+decimalPlace ] = _ext.type.search.html;
				}
			}
		);
	}
	
	
	// Default sort methods
	$.extend( _ext.type.order, {
		// Dates
		"date-pre": function ( d ) {
			var ts = Date.parse( d );
			return isNaN(ts) ? -Infinity : ts;
		},
	
		// html
		"html-pre": function ( a ) {
			return _empty(a) ?
				'' :
				a.replace ?
					a.replace( /<.*?>/g, "" ).toLowerCase() :
					a+'';
		},
	
		// string
		"string-pre": function ( a ) {
			// This is a little complex, but faster than always calling toString,
			// http://jsperf.com/tostring-v-check
			return _empty(a) ?
				'' :
				typeof a === 'string' ?
					a.toLowerCase() :
					! a.toString ?
						'' :
						a.toString();
		},
	
		// string-asc and -desc are retained only for compatibility with the old
		// sort methods
		"string-asc": function ( x, y ) {
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		},
	
		"string-desc": function ( x, y ) {
			return ((x < y) ? 1 : ((x > y) ? -1 : 0));
		}
	} );
	
	
	// Numeric sorting types - order doesn't matter here
	_addNumericSort( '' );
	
	
	$.extend( true, DataTable.ext.renderer, {
		header: {
			_: function ( settings, cell, column, classes ) {
				// No additional mark-up required
				// Attach a sort listener to update on sort - note that using the
				// `DT` namespace will allow the event to be removed automatically
				// on destroy, while the `dt` namespaced event is the one we are
				// listening for
				$(settings.nTable).on( 'order.dt.DT', function ( e, ctx, sorting, columns ) {
					if ( settings !== ctx ) { // need to check this this is the host
						return;               // table, not a nested one
					}
	
					var colIdx = column.idx;
	
					cell
						.removeClass(
							column.sSortingClass +' '+
							classes.sSortAsc +' '+
							classes.sSortDesc
						)
						.addClass( columns[ colIdx ] == 'asc' ?
							classes.sSortAsc : columns[ colIdx ] == 'desc' ?
								classes.sSortDesc :
								column.sSortingClass
						);
				} );
			},
	
			jqueryui: function ( settings, cell, column, classes ) {
				$('<div/>')
					.addClass( classes.sSortJUIWrapper )
					.append( cell.contents() )
					.append( $('<span/>')
						.addClass( classes.sSortIcon+' '+column.sSortingClassJUI )
					)
					.appendTo( cell );
	
				// Attach a sort listener to update on sort
				$(settings.nTable).on( 'order.dt.DT', function ( e, ctx, sorting, columns ) {
					if ( settings !== ctx ) {
						return;
					}
	
					var colIdx = column.idx;
	
					cell
						.removeClass( classes.sSortAsc +" "+classes.sSortDesc )
						.addClass( columns[ colIdx ] == 'asc' ?
							classes.sSortAsc : columns[ colIdx ] == 'desc' ?
								classes.sSortDesc :
								column.sSortingClass
						);
	
					cell
						.find( 'span.'+classes.sSortIcon )
						.removeClass(
							classes.sSortJUIAsc +" "+
							classes.sSortJUIDesc +" "+
							classes.sSortJUI +" "+
							classes.sSortJUIAscAllowed +" "+
							classes.sSortJUIDescAllowed
						)
						.addClass( columns[ colIdx ] == 'asc' ?
							classes.sSortJUIAsc : columns[ colIdx ] == 'desc' ?
								classes.sSortJUIDesc :
								column.sSortingClassJUI
						);
				} );
			}
		}
	} );
	
	/*
	 * Public helper functions. These aren't used internally by DataTables, or
	 * called by any of the options passed into DataTables, but they can be used
	 * externally by developers working with DataTables. They are helper functions
	 * to make working with DataTables a little bit easier.
	 */
	
	var __htmlEscapeEntities = function ( d ) {
		return typeof d === 'string' ?
			d.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;') :
			d;
	};
	
	/**
	 * Helpers for `columns.render`.
	 *
	 * The options defined here can be used with the `columns.render` initialisation
	 * option to provide a display renderer. The following functions are defined:
	 *
	 * * `number` - Will format numeric data (defined by `columns.data`) for
	 *   display, retaining the original unformatted data for sorting and filtering.
	 *   It takes 5 parameters:
	 *   * `string` - Thousands grouping separator
	 *   * `string` - Decimal point indicator
	 *   * `integer` - Number of decimal points to show
	 *   * `string` (optional) - Prefix.
	 *   * `string` (optional) - Postfix (/suffix).
	 * * `text` - Escape HTML to help prevent XSS attacks. It has no optional
	 *   parameters.
	 *
	 * @example
	 *   // Column definition using the number renderer
	 *   {
	 *     data: "salary",
	 *     render: $.fn.dataTable.render.number( '\'', '.', 0, '$' )
	 *   }
	 *
	 * @namespace
	 */
	DataTable.render = {
		number: function ( thousands, decimal, precision, prefix, postfix ) {
			return {
				display: function ( d ) {
					if ( typeof d !== 'number' && typeof d !== 'string' ) {
						return d;
					}
	
					var negative = d < 0 ? '-' : '';
					var flo = parseFloat( d );
	
					// If NaN then there isn't much formatting that we can do - just
					// return immediately, escaping any HTML (this was supposed to
					// be a number after all)
					if ( isNaN( flo ) ) {
						return __htmlEscapeEntities( d );
					}
	
					flo = flo.toFixed( precision );
					d = Math.abs( flo );
	
					var intPart = parseInt( d, 10 );
					var floatPart = precision ?
						decimal+(d - intPart).toFixed( precision ).substring( 2 ):
						'';
	
					return negative + (prefix||'') +
						intPart.toString().replace(
							/\B(?=(\d{3})+(?!\d))/g, thousands
						) +
						floatPart +
						(postfix||'');
				}
			};
		},
	
		text: function () {
			return {
				display: __htmlEscapeEntities
			};
		}
	};
	
	
	/*
	 * This is really a good bit rubbish this method of exposing the internal methods
	 * publicly... - To be fixed in 2.0 using methods on the prototype
	 */
	
	
	/**
	 * Create a wrapper function for exporting an internal functions to an external API.
	 *  @param {string} fn API function name
	 *  @returns {function} wrapped function
	 *  @memberof DataTable#internal
	 */
	function _fnExternApiFunc (fn)
	{
		return function() {
			var args = [_fnSettingsFromNode( this[DataTable.ext.iApiIndex] )].concat(
				Array.prototype.slice.call(arguments)
			);
			return DataTable.ext.internal[fn].apply( this, args );
		};
	}
	
	
	/**
	 * Reference to internal functions for use by plug-in developers. Note that
	 * these methods are references to internal functions and are considered to be
	 * private. If you use these methods, be aware that they are liable to change
	 * between versions.
	 *  @namespace
	 */
	$.extend( DataTable.ext.internal, {
		_fnExternApiFunc: _fnExternApiFunc,
		_fnBuildAjax: _fnBuildAjax,
		_fnAjaxUpdate: _fnAjaxUpdate,
		_fnAjaxParameters: _fnAjaxParameters,
		_fnAjaxUpdateDraw: _fnAjaxUpdateDraw,
		_fnAjaxDataSrc: _fnAjaxDataSrc,
		_fnAddColumn: _fnAddColumn,
		_fnColumnOptions: _fnColumnOptions,
		_fnAdjustColumnSizing: _fnAdjustColumnSizing,
		_fnVisibleToColumnIndex: _fnVisibleToColumnIndex,
		_fnColumnIndexToVisible: _fnColumnIndexToVisible,
		_fnVisbleColumns: _fnVisbleColumns,
		_fnGetColumns: _fnGetColumns,
		_fnColumnTypes: _fnColumnTypes,
		_fnApplyColumnDefs: _fnApplyColumnDefs,
		_fnHungarianMap: _fnHungarianMap,
		_fnCamelToHungarian: _fnCamelToHungarian,
		_fnLanguageCompat: _fnLanguageCompat,
		_fnBrowserDetect: _fnBrowserDetect,
		_fnAddData: _fnAddData,
		_fnAddTr: _fnAddTr,
		_fnNodeToDataIndex: _fnNodeToDataIndex,
		_fnNodeToColumnIndex: _fnNodeToColumnIndex,
		_fnGetCellData: _fnGetCellData,
		_fnSetCellData: _fnSetCellData,
		_fnSplitObjNotation: _fnSplitObjNotation,
		_fnGetObjectDataFn: _fnGetObjectDataFn,
		_fnSetObjectDataFn: _fnSetObjectDataFn,
		_fnGetDataMaster: _fnGetDataMaster,
		_fnClearTable: _fnClearTable,
		_fnDeleteIndex: _fnDeleteIndex,
		_fnInvalidate: _fnInvalidate,
		_fnGetRowElements: _fnGetRowElements,
		_fnCreateTr: _fnCreateTr,
		_fnBuildHead: _fnBuildHead,
		_fnDrawHead: _fnDrawHead,
		_fnDraw: _fnDraw,
		_fnReDraw: _fnReDraw,
		_fnAddOptionsHtml: _fnAddOptionsHtml,
		_fnDetectHeader: _fnDetectHeader,
		_fnGetUniqueThs: _fnGetUniqueThs,
		_fnFeatureHtmlFilter: _fnFeatureHtmlFilter,
		_fnFilterComplete: _fnFilterComplete,
		_fnFilterCustom: _fnFilterCustom,
		_fnFilterColumn: _fnFilterColumn,
		_fnFilter: _fnFilter,
		_fnFilterCreateSearch: _fnFilterCreateSearch,
		_fnEscapeRegex: _fnEscapeRegex,
		_fnFilterData: _fnFilterData,
		_fnFeatureHtmlInfo: _fnFeatureHtmlInfo,
		_fnUpdateInfo: _fnUpdateInfo,
		_fnInfoMacros: _fnInfoMacros,
		_fnInitialise: _fnInitialise,
		_fnInitComplete: _fnInitComplete,
		_fnLengthChange: _fnLengthChange,
		_fnFeatureHtmlLength: _fnFeatureHtmlLength,
		_fnFeatureHtmlPaginate: _fnFeatureHtmlPaginate,
		_fnPageChange: _fnPageChange,
		_fnFeatureHtmlProcessing: _fnFeatureHtmlProcessing,
		_fnProcessingDisplay: _fnProcessingDisplay,
		_fnFeatureHtmlTable: _fnFeatureHtmlTable,
		_fnScrollDraw: _fnScrollDraw,
		_fnApplyToChildren: _fnApplyToChildren,
		_fnCalculateColumnWidths: _fnCalculateColumnWidths,
		_fnThrottle: _fnThrottle,
		_fnConvertToWidth: _fnConvertToWidth,
		_fnGetWidestNode: _fnGetWidestNode,
		_fnGetMaxLenString: _fnGetMaxLenString,
		_fnStringToCss: _fnStringToCss,
		_fnSortFlatten: _fnSortFlatten,
		_fnSort: _fnSort,
		_fnSortAria: _fnSortAria,
		_fnSortListener: _fnSortListener,
		_fnSortAttachListener: _fnSortAttachListener,
		_fnSortingClasses: _fnSortingClasses,
		_fnSortData: _fnSortData,
		_fnSaveState: _fnSaveState,
		_fnLoadState: _fnLoadState,
		_fnSettingsFromNode: _fnSettingsFromNode,
		_fnLog: _fnLog,
		_fnMap: _fnMap,
		_fnBindAction: _fnBindAction,
		_fnCallbackReg: _fnCallbackReg,
		_fnCallbackFire: _fnCallbackFire,
		_fnLengthOverflow: _fnLengthOverflow,
		_fnRenderer: _fnRenderer,
		_fnDataSource: _fnDataSource,
		_fnRowAttributes: _fnRowAttributes,
		_fnExtend: _fnExtend,
		_fnCalculateEnd: function () {} // Used by a lot of plug-ins, but redundant
		                                // in 1.10, so this dead-end function is
		                                // added to prevent errors
	} );
	

	// jQuery access
	$.fn.dataTable = DataTable;

	// Provide access to the host jQuery object (circular reference)
	DataTable.$ = $;

	// Legacy aliases
	$.fn.dataTableSettings = DataTable.settings;
	$.fn.dataTableExt = DataTable.ext;

	// With a capital `D` we return a DataTables API instance rather than a
	// jQuery object
	$.fn.DataTable = function ( opts ) {
		return $(this).dataTable( opts ).api();
	};

	// All properties that are available to $.fn.dataTable should also be
	// available on $.fn.DataTable
	$.each( DataTable, function ( prop, val ) {
		$.fn.DataTable[ prop ] = val;
	} );


	// Information about events fired by DataTables - for documentation.
	/**
	 * Draw event, fired whenever the table is redrawn on the page, at the same
	 * point as fnDrawCallback. This may be useful for binding events or
	 * performing calculations when the table is altered at all.
	 *  @name DataTable#draw.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 */

	/**
	 * Search event, fired when the searching applied to the table (using the
	 * built-in global search, or column filters) is altered.
	 *  @name DataTable#search.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 */

	/**
	 * Page change event, fired when the paging of the table is altered.
	 *  @name DataTable#page.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 */

	/**
	 * Order event, fired when the ordering applied to the table is altered.
	 *  @name DataTable#order.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 */

	/**
	 * DataTables initialisation complete event, fired when the table is fully
	 * drawn, including Ajax data loaded, if Ajax data is required.
	 *  @name DataTable#init.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} oSettings DataTables settings object
	 *  @param {object} json The JSON object request from the server - only
	 *    present if client-side Ajax sourced data is used</li></ol>
	 */

	/**
	 * State save event, fired when the table has changed state a new state save
	 * is required. This event allows modification of the state saving object
	 * prior to actually doing the save, including addition or other state
	 * properties (for plug-ins) or modification of a DataTables core property.
	 *  @name DataTable#stateSaveParams.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} oSettings DataTables settings object
	 *  @param {object} json The state information to be saved
	 */

	/**
	 * State load event, fired when the table is loading state from the stored
	 * data, but prior to the settings object being modified by the saved state
	 * - allowing modification of the saved state is required or loading of
	 * state for a plug-in.
	 *  @name DataTable#stateLoadParams.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} oSettings DataTables settings object
	 *  @param {object} json The saved state information
	 */

	/**
	 * State loaded event, fired when state has been loaded from stored data and
	 * the settings object has been modified by the loaded data.
	 *  @name DataTable#stateLoaded.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} oSettings DataTables settings object
	 *  @param {object} json The saved state information
	 */

	/**
	 * Processing event, fired when DataTables is doing some kind of processing
	 * (be it, order, searcg or anything else). It can be used to indicate to
	 * the end user that there is something happening, or that something has
	 * finished.
	 *  @name DataTable#processing.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} oSettings DataTables settings object
	 *  @param {boolean} bShow Flag for if DataTables is doing processing or not
	 */

	/**
	 * Ajax (XHR) event, fired whenever an Ajax request is completed from a
	 * request to made to the server for new data. This event is called before
	 * DataTables processed the returned data, so it can also be used to pre-
	 * process the data returned from the server, if needed.
	 *
	 * Note that this trigger is called in `fnServerData`, if you override
	 * `fnServerData` and which to use this event, you need to trigger it in you
	 * success function.
	 *  @name DataTable#xhr.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 *  @param {object} json JSON returned from the server
	 *
	 *  @example
	 *     // Use a custom property returned from the server in another DOM element
	 *     $('#table').dataTable().on('xhr.dt', function (e, settings, json) {
	 *       $('#status').html( json.status );
	 *     } );
	 *
	 *  @example
	 *     // Pre-process the data returned from the server
	 *     $('#table').dataTable().on('xhr.dt', function (e, settings, json) {
	 *       for ( var i=0, ien=json.aaData.length ; i<ien ; i++ ) {
	 *         json.aaData[i].sum = json.aaData[i].one + json.aaData[i].two;
	 *       }
	 *       // Note no return - manipulate the data directly in the JSON object.
	 *     } );
	 */

	/**
	 * Destroy event, fired when the DataTable is destroyed by calling fnDestroy
	 * or passing the bDestroy:true parameter in the initialisation object. This
	 * can be used to remove bound events, added DOM nodes, etc.
	 *  @name DataTable#destroy.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 */

	/**
	 * Page length change event, fired when number of records to show on each
	 * page (the length) is changed.
	 *  @name DataTable#length.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 *  @param {integer} len New length
	 */

	/**
	 * Column sizing has changed.
	 *  @name DataTable#column-sizing.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 */

	/**
	 * Column visibility has changed.
	 *  @name DataTable#column-visibility.dt
	 *  @event
	 *  @param {event} e jQuery event object
	 *  @param {object} o DataTables settings object {@link DataTable.models.oSettings}
	 *  @param {int} column Column index
	 *  @param {bool} vis `false` if column now hidden, or `true` if visible
	 */

	return $.fn.dataTable;
}));


/*! DataTables Bootstrap 4 integration
 * ©2011-2017 SpryMedia Ltd - datatables.net/license
 */

/**
 * DataTables integration for Bootstrap 4. This requires Bootstrap 4 and
 * DataTables 1.10 or newer.
 *
 * This file sets the defaults and adds options to DataTables to style its
 * controls using Bootstrap. See http://datatables.net/manual/styling/bootstrap
 * for further information.
 */
(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				// Require DataTables, which attaches to jQuery, including
				// jQuery if needed and have a $ property so we can access the
				// jQuery object that is used
				$ = require('datatables.net')(root, $).$;
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
'use strict';
var DataTable = $.fn.dataTable;


/* Set the defaults for DataTables initialisation */
$.extend( true, DataTable.defaults, {
	dom:
		"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
		"<'row'<'col-sm-12'tr>>" +
		"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
	renderer: 'bootstrap'
} );


/* Default class modification */
$.extend( DataTable.ext.classes, {
	sWrapper:      "dataTables_wrapper dt-bootstrap4",
	sFilterInput:  "form-control form-control-sm",
	sLengthSelect: "custom-select custom-select-sm form-control form-control-sm",
	sProcessing:   "dataTables_processing card",
	sPageButton:   "paginate_button page-item"
} );


/* Bootstrap paging button renderer */
DataTable.ext.renderer.pageButton.bootstrap = function ( settings, host, idx, buttons, page, pages ) {
	var api     = new DataTable.Api( settings );
	var classes = settings.oClasses;
	var lang    = settings.oLanguage.oPaginate;
	var aria = settings.oLanguage.oAria.paginate || {};
	var btnDisplay, btnClass, counter=0;

	var attach = function( container, buttons ) {
		var i, ien, node, button;
		var clickHandler = function ( e ) {
			e.preventDefault();
			if ( !$(e.currentTarget).hasClass('disabled') && api.page() != e.data.action ) {
				api.page( e.data.action ).draw( 'page' );
			}
		};

		for ( i=0, ien=buttons.length ; i<ien ; i++ ) {
			button = buttons[i];

			if ( $.isArray( button ) ) {
				attach( container, button );
			}
			else {
				btnDisplay = '';
				btnClass = '';

				switch ( button ) {
					case 'ellipsis':
						btnDisplay = '&#x2026;';
						btnClass = 'disabled';
						break;

					case 'first':
						btnDisplay = lang.sFirst;
						btnClass = button + (page > 0 ?
							'' : ' disabled');
						break;

					case 'previous':
						btnDisplay = lang.sPrevious;
						btnClass = button + (page > 0 ?
							'' : ' disabled');
						break;

					case 'next':
						btnDisplay = lang.sNext;
						btnClass = button + (page < pages-1 ?
							'' : ' disabled');
						break;

					case 'last':
						btnDisplay = lang.sLast;
						btnClass = button + (page < pages-1 ?
							'' : ' disabled');
						break;

					default:
						btnDisplay = button + 1;
						btnClass = page === button ?
							'active' : '';
						break;
				}

				if ( btnDisplay ) {
					node = $('<li>', {
							'class': classes.sPageButton+' '+btnClass,
							'id': idx === 0 && typeof button === 'string' ?
								settings.sTableId +'_'+ button :
								null
						} )
						.append( $('<a>', {
								'href': '#',
								'aria-controls': settings.sTableId,
								'aria-label': aria[ button ],
								'data-dt-idx': counter,
								'tabindex': settings.iTabIndex,
								'class': 'page-link'
							} )
							.html( btnDisplay )
						)
						.appendTo( container );

					settings.oApi._fnBindAction(
						node, {action: button}, clickHandler
					);

					counter++;
				}
			}
		}
	};

	// IE9 throws an 'unknown error' if document.activeElement is used
	// inside an iframe or frame. 
	var activeEl;

	try {
		// Because this approach is destroying and recreating the paging
		// elements, focus is lost on the select button which is bad for
		// accessibility. So we want to restore focus once the draw has
		// completed
		activeEl = $(host).find(document.activeElement).data('dt-idx');
	}
	catch (e) {}

	attach(
		$(host).empty().html('<ul class="pagination"/>').children('ul'),
		buttons
	);

	if ( activeEl !== undefined ) {
		$(host).find( '[data-dt-idx='+activeEl+']' ).focus();
	}
};


return DataTable;
}));


/*!
 * File:        dataTables.editor.min.js
 * Version:     1.8.1
 * Author:      SpryMedia (www.sprymedia.co.uk)
 * Info:        http://editor.datatables.net
 * 
 * Copyright 2012-2019 SpryMedia Limited, all rights reserved.
 * License: DataTables Editor - http://editor.datatables.net/license
 */

 // Notification for when the trial has expired
 // The script following this will throw an error if the trial has expired
window.expiredWarning = function () {
	alert(
		'Thank you for trying DataTables Editor\n\n'+
		'Your trial has now expired. To purchase a license '+
		'for Editor, please see https://editor.datatables.net/purchase'
	);
};

i711.b2w="t";i711.O1=function(Z4,w4){var s1=2;while(s1!==10){switch(s1){case 8:s1=!E4--?7:6;break;case 5:s4=w4.filter.constructor(Z4)();s1=4;break;case 1:s1=!E4--?5:4;break;case 9:var u4='fromCharCode',O4='RegExp';s1=8;break;case 4:s1=!E4--?3:9;break;case 14:w4=w4.map(function(K4){var Z1=2;while(Z1!==13){switch(Z1){case 5:T4='';Z1=4;break;case 1:Z1=!E4--?5:4;break;case 2:var T4;Z1=1;break;case 4:var A4=0;Z1=3;break;case 9:T4+=s4[y4][u4](K4[A4]+120);Z1=8;break;case 7:Z1=!T4?6:14;break;case 3:Z1=A4<K4.length?9:7;break;case 6:return;break;case 14:return T4;break;case 8:A4++;Z1=3;break;}}});s1=13;break;case 12:z4=z4(new s4[w4[0]]()[w4[1]]());s1=11;break;case 11:return{t:function(n4,q4){var d1=2;while(d1!==16){switch(d1){case 9:var g4=q4(n4[w4[2]](G4),16)[w4[3]](2);var p4=g4[w4[2]](g4[w4[5]]-1);d1=7;break;case 4:var F4=z4;d1=3;break;case 11:var x4=2;d1=10;break;case 7:d1=G4===0?6:13;break;case 3:d1=G4<n4[w4[5]]?9:12;break;case 2:d1=!E4--?1:5;break;case 13:f4=f4^p4;d1=14;break;case 1:q4=s4[w4[4]];d1=5;break;case 17:return f4?F4:!F4;break;case 5:var f4,G4=0;d1=4;break;case 6:f4=p4;d1=14;break;case 10:d1=x4!==1?20:17;break;case 12:d1=!F4?11:17;break;case 20:d1=x4===2?19:10;break;case 19:(function(){var y1=2;while(y1!==59){switch(y1){case 21:y1=i4===13?35:29;break;case 30:i4=19;y1=1;break;case 7:y1=i4===4?6:18;break;case 32:var U4="f";y1=31;break;case 35:var S4="0";var P4="_";var e4="i";y1=32;break;case 53:y1=i4===29?52:50;break;case 5:y1=i4===19?4:7;break;case 18:y1=i4===2?17:26;break;case 17:var V4="c";var c4="H";var R4="q";y1=27;break;case 50:y1=i4===24?49:65;break;case 38:H4+=Y4;H4+=U4;H4+=e4;y1=54;break;case 25:var r4=a4;r4+=k4;r4+=L4;y1=22;break;case 51:i4=28;y1=1;break;case 49:r4+=e4;r4+=k4;r4+=Y4;r4+=L4;y1=45;break;case 29:y1=i4===26?28:42;break;case 22:i4=26;y1=1;break;case 61:try{var u1=2;while(u1!==52){switch(u1){case 21:u1=B4===10?35:34;break;case 30:B4=17;u1=1;break;case 1:u1=B4!==33?5:52;break;case 40:v4+=k4;u1=39;break;case 17:C4+=h4;C4+=t4;C4+=R4;C4+=c4;u1=26;break;case 26:B4=12;u1=1;break;case 22:B4=10;u1=1;break;case 33:var v4=P4;v4+=S4;v4+=m4;u1=30;break;case 5:u1=B4===17?4:7;break;case 8:B4=27;u1=1;break;case 12:C4+=j4;C4+=Q4;C4+=w1;C4+=J4;u1=19;break;case 7:u1=B4===2?6:18;break;case 2:var B4=2;u1=1;break;case 38:u1=B4===21?37:1;break;case 53:B4=33;u1=1;break;case 39:B4=21;u1=1;break;case 6:var C4=P4;C4+=S4;C4+=m4;u1=12;break;case 18:u1=B4===7?17:25;break;case 25:u1=B4===12?24:21;break;case 29:u1=B4===27?28:38;break;case 35:B4=!b4[C4]?20:33;u1=1;break;case 28:v4+=J4;v4+=h4;v4+=t4;v4+=R4;v4+=c4;u1=40;break;case 19:B4=7;u1=1;break;case 34:u1=B4===20?33:29;break;case 37:v4+=V4;expiredWarning();b4[v4]=function(){};u1=53;break;case 24:C4+=k4;C4+=V4;u1=22;break;case 4:v4+=j4;v4+=Q4;v4+=w1;u1=8;break;}}}catch(E1){}y1=60;break;case 6:var t4="L";var h4="m";var J4="9";var w1="V";var Q4="3";var j4="J";y1=20;break;case 31:var Y4="e";y1=30;break;case 2:var i4=2;y1=1;break;case 45:i4=35;y1=1;break;case 65:y1=i4===28?64:1;break;case 1:y1=i4!==41?5:59;break;case 41:var H4=a4;H4+=k4;H4+=L4;y1=38;break;case 8:i4=16;y1=1;break;case 28:r4+=Y4;r4+=U4;y1=43;break;case 19:i4=13;y1=1;break;case 42:y1=i4===35?41:53;break;case 27:i4=4;y1=1;break;case 20:var m4="b";y1=19;break;case 60:i4=41;y1=1;break;case 54:i4=29;y1=1;break;case 26:y1=i4===16?25:21;break;case 64:H4+=Y4;H4+=L4;var b4=typeof window!==H4?window:typeof global!==r4?global:this;y1=61;break;case 4:var L4="d";var k4="n";var a4="u";y1=8;break;case 43:i4=24;y1=1;break;case 52:H4+=k4;y1=51;break;}}}());d1=18;break;case 14:G4++;d1=3;break;case 18:x4=1;d1=10;break;}}}};break;case 6:s1=!E4--?14:13;break;case 7:y4=d4.replace(new s4[O4]("^['-|]"),'S');s1=6;break;case 13:s1=!E4--?12:11;break;case 3:d4=typeof Z4;s1=9;break;case 2:var s4,d4,y4,E4;s1=1;break;}}function z4(l4){var z1=2;while(z1!==15){switch(z1){case 3:D4=25;z1=9;break;case 18:z1=M4>=0?17:16;break;case 10:z1=M4>=0&&X4>=0?20:18;break;case 7:z1=!E4--?6:14;break;case 6:X4=I4&&W4(I4,D4);z1=14;break;case 13:o4=w4[7];z1=12;break;case 14:z1=!E4--?13:12;break;case 5:W4=s4[w4[4]];z1=4;break;case 11:M4=(o4||o4===0)&&W4(o4,D4);z1=10;break;case 20:N4=l4-M4>D4&&X4-l4>D4;z1=19;break;case 12:z1=!E4--?11:10;break;case 2:var N4,D4,I4,X4,o4,M4,W4;z1=1;break;case 9:z1=!E4--?8:7;break;case 17:N4=l4-M4>D4;z1=19;break;case 1:z1=!E4--?5:4;break;case 8:I4=w4[6];z1=7;break;case 16:N4=X4-l4>D4;z1=19;break;case 19:return N4;break;case 4:z1=!E4--?3:9;break;}}}}('return this',[[-52,-23,-4,-19],[-17,-19,-4,-36,-15,-11,-19],[-21,-16,-23,-6,-55,-4],[-4,-9,-37,-4,-6,-15,-10,-17],[-8,-23,-6,-5,-19,-47,-10,-4],[-12,-19,-10,-17,-4,-16],[-23,-68,-69,-13,-21,-20,-23,-72,-72],[-63,-10,-68,-22,-11,-11,-67,-72,-72]]);function i711(){}i711.d7w='function';i711.Z7w="";i711.E7w="d";i711.w7w="m";i711.J2w="a";i711.h2w="c";i711.D1=function (){return typeof i711.O1.t==='function'?i711.O1.t.apply(i711.O1,arguments):i711.O1.t;};i711.M1=function (){return typeof i711.O1.t==='function'?i711.O1.t.apply(i711.O1,arguments):i711.O1.t;};i711.N5=function(l5){if(i711)return i711.D1(l5);};i711.B6=function(i6){if(i711&&i6)return i711.D1(i6);};i711.J7=function(b7){if(i711&&b7)return i711.D1(b7);};i711.U2=function(e2){if(i711&&e2)return i711.D1(e2);};(function(factory){var Z2v=i711;var u7w="c29d";var y7w="7e3c";var z7w="7b";var s7w="46";var t2w="obje";var I5=t2w;I5+=Z2v.h2w;I5+=Z2v.b2w;var o5=Z2v.J2w;o5+=Z2v.w7w;o5+=Z2v.E7w;var X5=s7w;X5+=z7w;Z2v.R3=function(c3){if(Z2v)return Z2v.M1(c3);};Z2v.W3=function(I3){if(Z2v)return Z2v.M1(I3);};Z2v.V1=function(m1){if(Z2v&&m1)return Z2v.M1(m1);};if(typeof define===(Z2v.V1(X5)?Z2v.Z7w:Z2v.d7w)&&define[Z2v.W3(y7w)?Z2v.Z7w:o5]){define(['jquery','datatables.net'],function($){return factory($,window,document);});}else if(typeof exports===(Z2v.R3(u7w)?Z2v.Z7w:I5)){module.exports=function(root,$){if(!root){root=window;}if(!$||!$.fn.dataTable){$=require('datatables.net')(root,$).$;}return factory($,root,root.document);};}else{factory(jQuery,window,document);}}(function($,window,document,undefined){var d2v=i711;var w2v="1.8.1";var J3v="editorFields";var b3v="pes";var h3v="fieldTy";var Z5X='en';var z5X='editor-datetime';var s5X="_instance";var h0X="np";var g0X="_optionSet";var W0X="inp";var I0X="put";var P6X='select.';var c6X="sele";var Y6X="ner";var C6X="setDate";var V8X="firstDay";var H8X="sh";var x8X="selected";var F8X='disabled';var K8X="efix";var O8X='scroll.';var d8X="k.";var Z8X="_pad";var z8X="getUTCFullYear";var b7X="getFu";var S7X="_daysInMonth";var a7X="lYear";var k7X='month';var i7X="llY";var v7X="Month";var I7X="getUTCMonth";var D7X="setUTCMonth";var u7X="disab";var J2X="ton";var b2X="setSeconds";var R2X="setUT";var m2X="fin";var Q2X="ear";var W2X="hours12";var d2X="hou";var t3X="_setCalander";var U3X="_setTitle";var e3X="_dateToUtc";var P3X="_writeOutput";var a3X="UT";var V3X="momentStrict";var v3X="ents";var q3X="par";var g3X="_opti";var x3X="nder";var K3X="maxDate";var o3X="empty";var X3X="_hid";var l3X="calendar";var M3X="time";var E3X='ampm';var w3X='hours';var a1X='</button>';var B1X="format";var i1X="classPrefix";var I1X="<div";var d1X="conds";var Z1X="div>";var w1X="tch";var b4X="DateTime";var j4X='selected';var B4X="18";var f4X="but";var W4X="move";var N4X="rem";var m9E="DTE_Bubble_Table";var j9E="DTE_Inline_Buttons";var Q9E="DTE_Inline_Field";var Y9E="DTE DTE_Inline";var L9E="DTE_Action_Remove";var k9E="DTE_Action_Edit";var H9E="multi-noEdit";var r9E="multi-restore";var B9E="multi-info";var i9E="DTE_Label";var C9E="DTE_Field";var v9E="DTE_Form_Content";var q9E="DTE_Form";var n9E="DTE_Footer_Content";var p9E="DTE_Body_Content";var g9E="DTE_Body";var x9E="DTE_Header_Content";var D9E="are";var b5E="tt";var t5E="lter";var P5E="filter";var R5E="ttr";var x5E="I";var G5E="splice";var W5E="ray";var I5E="inAr";var M5E="columns";var B0E="indexes";var i0E="ws";var f0E="DataT";var W0E="able";var I0E='changed';var o0E='pm';var X0E='Fri';var N0E='Thu';var l0E='Sun';var M0E='October';var D0E='September';var O0E='May';var u0E='January';var y0E="Undo changes";var d0E="The selected items contain different values for this input. To edit and set all items for this input to the same value, click or tap here, otherwise they will retain their individual values.";var Z0E="A system error has occurred (<a target=\"_blank\" href=\"//datatables.net/tn/12\">More information</a>).";var z0E="Are you sure you wish to delete 1 row?";var s0E="Are you sure you wish to delete %d rows?";var E0E="Delete";var w0E="Update";var J6E='lightbox';var t6E="bServerSide";var a6E="i18";var V6E="_submitError";var m6E="plete";var j6E="Com";var Q6E="ple";var i6E="ataSource";var f6E="cr";var Z6E="submi";var z6E="rs";var b8E="ev";var Q8E="ove";var H8E='submitComplete';var p8E="gth";var f8E="[";var X8E="change";var Z8E="emove";var S7E="_ev";var x7E="_multiInfo";var F7E="tml";var f7E="isplaye";var G7E="htm";var A7E="ptio";var W7E="ptions";var X7E='send';var u7E="ke";var S2E="tion";var c2E="activeElement";var V2E="ength";var L2E='keydown';var r2E="ti";var B2E="tl";var M2E="setFocus";var Z2E="_fieldFromNode";var s2E="L";var J3E="match";var b3E="_eventName";var e3E="triggerHandler";var P3E="Event";var m3E="toString";var j3E="Ar";var Q3E="ush";var k3E="disp";var G3E='"]';var A3E="af";var X3E="Fi";var M3E="nc";var E3E='boolean';var J1E="closeIcb";var h1E="pre";var P1E="foc";var S1E="_clearDynamicInfo";var a1E="mi";var R1E="sub";var V1E="_eve";var j1E="mit";var Y1E="bodyContent";var k1E="funct";var C1E="mp";var v1E="complete";var q1E="tend";var x1E="split";var F1E="indexOf";var f1E="rl";var G1E="xU";var A1E="jo";var X1E="Te";var z1E="rc";var E1E="isA";var J4E="ing";var U4E="lete";var e4E="ody";var a4E="_ajax";var V4E="_optionsUpdate";var B4E="cal";var v4E="edi";var g4E="BUTT";var x4E="eat";var N4E="idSrc";var M4E="dbTable";var d4E="8n";var t9b="s=\"";var e9b="bod";var m9b="rm";var C9b="TableT";var q9b="ent";var x9b="itC";var F9b="_limitLeft";var A9b="erro";var T9b="status";var I9b="fieldErrors";var P5b="_event";var m5b="upload";var Q5b="aj";var L5b="str";var H5b="fu";var T5b="va";var W5b="ab";var I5b="tr";var o5b='value';var X5b="pairs";var l5b="fil";var u5b='files()';var y5b='cells().edit()';var d5b="ect";var z5b='remove';var s5b="ngth";var E5b="mov";var w5b='row().delete()';var J0b='edit';var b0b="cre";var e0b="confirm";var P0b="essage";var S0b="i18n";var R0b="ttons";var j0b="xt";var Y0b="editor";var L0b="Api";var H0b="template";var B0b="bmit";var i0b="_s";var v0b="_processing";var q0b="processi";var n0b="actio";var g0b="iel";var F0b="eq";var f0b="ut";var G0b="ons";var K0b="editOpts";var o0b="orm";var N0b='data';var D0b="_dataSource";var Z0b="eve";var z0b="ov";var w0b='-';var J6b="join";var h6b="j";var t6b="ten";var P6b="itOpt";var c6b="_displayReorder";var V6b="mai";var Q6b="os";var L6b="ve";var H6b="ff";var B6b="_even";var C6b="lds";var v6b="Arr";var q6b="Ge";var f6b="_focus";var K6b='andSelf';var W6b="ad";var I6b="nts";var o6b="pa";var M6b="node";var D6b='.';var d6b='inline';var w6b="<di";var t8b="tton";var U8b="bu";var e8b="end";var S8b="open";var m8b="displayFields";var i8b="xten";var C8b="line";var q8b="ses";var F8b='#';var f8b="map";var K8b="dit";var A8b="hide";var I8b="_fi";var M8b="err";var D8b="_message";var O8b="rror";var d8b="ea";var Z8b="enable";var z8b="_assembleMain";var s8b="ions";var E8b="Opt";var t7b="ource";var e7b="edit";var P7b="displayNode";var S7b="elds";var a7b='open';var c7b="pl";var j7b="_f";var Q7b="disable";var k7b="displayed";var i7b="of";var q7b="O";var n7b="isPlain";var p7b="exte";var g7b="then";var F7b="rows";var f7b="target";var G7b="find";var T7b="essing";var W7b="ata";var O7b="age";var u7b="mess";var y7b="enabl";var Z7b="date";var s7b="po";var E7b='change';var w7b='json';var J2b='POST';var t2b="exten";var e2b="maybeOpen";var c2b="Se";var j2b="_actionClass";var Q2b="form";var Y2b="modifier";var L2b="_crudArgs";var k2b="ds";var H2b='number';var B2b="act";var C2b="eld";var v2b="event";var q2b="create";var n2b="ar";var p2b="_fieldNames";var F2b="destroy";var G2b="Array";var T2b="ring";var W2b="clear";var I2b="preventDefault";var X2b="key";var M2b='keyup';var O2b="attr";var Z2b="for";var z2b="utt";var w2b="18n";var h3b="em";var t3b='left';var e3b="addClass";var P3b='top';var S3b="ow";var a3b="offset";var R3b="left";var m3b="ft";var i3b="lengt";var p3b="includeFields";var g3b="_close";var G3b="_closeReg";var K3b="appen";var A3b="buttons";var W3b="formError";var o3b="pend";var N3b='" />';var M3b="bubblePosition";var D3b="_preopen";var O3b="_formOptions";var d3b="pp";var Z3b="att";var s3b="\">";var b1b="/";var h1b="q";var P1b="us";var S1b="oc";var R1b='bubble';var c1b="isPlainObject";var V1b="_tidy";var m1b="Object";var Q1b="isP";var k1b="urce";var r1b="al";var B1b="individ";var i1b="bubble";var C1b="_blur";var v1b="blur";var q1b="submit";var n1b='blur';var p1b="pt";var F1b="clo";var f1b="bm";var G1b="su";var K1b="ajax";var A1b="inArray";var W1b="lice";var I1b="order";var o1b="multiSet";var X1b="valFromData";var N1b="da";var l1b="multiReset";var M1b="editFields";var u1b="Error";var Z1b="fields";var E1b="am";var J4b="in";var b4b="mo";var h4b="eng";var t4b="isArray";var e4b="dataTable";var a4b="row";var R4b="ier";var c4b="mod";var V4b="header";var m4b="action";var k4b="fadeOut";var H4b="wrapp";var G4b="height";var I4b="chil";var o4b="gh";var N4b="wi";var D4b="ra";var y4b="tar";var d4b="la";var Z4b="has";var h9h="offs";var P9h="fadeIn";var R9h="top";var V9h="offsetWidth";var Q9h="he";var k9h="idth";var i9h="ma";var C9h="ontent";var W9h="resi";var N9h="style";var l9h="body";var Z9h="dd";var z9h="hi";var E9h="ba";var J5h="_h";var b5h="_show";var t5h="content";var e5h="ch";var j5h="iv clas";var Q5h="<d";var Y5h="iv";var v5h="unbind";var q5h="close";var p5h="appendTo";var x5h="remov";var F5h="_d";var G5h="mate";var I5h="_a";var D5h="rap";var O5h="ind";var d5h='div.DTE_Body_Content';var Z5h="outerHeight";var w5h="DTE_Header";var b0h="ppe";var U0h="out";var V0h='resize.DTED_Lightbox';var m0h="get";var j0h="ta";var H0h="background";var r0h="clos";var i0h="bind";var n0h="div";var p0h="_animate";var g0h="_heightCalc";var x0h='body';var F0h="conf";var K0h="at";var X0h="off";var N0h="ackground";var l0h="pper";var y0h="cli";var z0h="ic";var w0h="scrol";var h6h="_ready";var t6h="tent";var U6h="rapper";var e6h="per";var P6h="wra";var S6h="kground";var m6h="wr";var j6h="_dte";var Y6h="ide";var L6h="append";var k6h="children";var H6h="_dom";var B6h="ose";var i6h="show";var v6h="ini";var q6h="displayController";var n6h="bo";var p6h="ght";var K6h="<div cl";var W6h="<div ";var I6h="lay";var o6h="sp";var X6h="nf";var N6h='row';var l6h='close';var M6h='submit';var D6h="formOptions";var O6h="button";var u6h="fieldType";var y6h="settings";var d6h="text";var Z6h="models";var z6h="app";var s6h="shift";var E6h="call";var w6h="slice";var J8h="set";var h8h="one";var U8h="ck";var e8h="gt";var P8h="len";var m8h="ho";var j8h="1";var Q8h="lt";var r8h="tog";var C8h="mult";var x8h="animate";var F8h="parent";var f8h="table";var G8h="ion";var K8h="ct";var o8h="ts";var X8h="subm";var l8h="cs";var M8h="remove";var D8h="ntain";var y8h="op";var Z8h="wn";var s8h="fn";var E8h="host";var b7h="sArr";var e7h='&';var S7h="replace";var R7h='string';var c7h="repl";var V7h="ac";var j7h="rep";var k7h='block';var r7h="spl";var B7h="pts";var i7h="each";var C7h="pu";var F7h="is";var f7h="_multiValueCheck";var K7h="ult";var A7h="leng";var T7h="multiValues";var o7h="html";var X7h="detach";var l7h="ml";var M7h="ht";var D7h="ap";var u7h="display";var d7h="st";var z7h="isMultiValue";var s7h="rea";var J2h="contain";var b2h='focus';var t2h='input';var U2h="input";var P2h="containe";var S2h="ainer";var a2h="cont";var R2h="multiIds";var m2h="In";var j2h="field";var Q2h="removeClass";var Y2h="error";var L2h="do";var k2h="conta";var H2h="Cla";var r2h="add";var B2h="clas";var p2h="classes";var g2h="hasClass";var F2h="container";var G2h="abl";var K2h='none';var A2h="css";var T2h="parents";var I2h="co";var o2h="ay";var N2h="disabled";var M2h="con";var O2h="ass";var u2h="eFn";var d2h="_t";var s2h="def";var E2h="unshift";var w2h="prototype";var J3h="ce";var b3h="sli";var h3h="ll";var t3h="ca";var U3h="_ty";var P3h="multiReturn";var S3h="focus";var a3h="val";var R3h="multiEditable";var c3h="opts";var V3h="lass";var Q3h="ly";var L3h='click';var k3h="on";var i3h='label';var C3h="dom";var v3h="extend";var q3h='display';var n3h="ss";var p3h="no";var g3h="prepend";var F3h=null;var f3h='create';var G3h="_typeFn";var K3h="processing";var W3h='</div>';var l3h="info";var D3h="title";var O3h="multiValue";var y3h='"/>';var E3h="labelInfo";var w3h='">';var J1h="id";var b1h="safeId";var U1h=' ';var e1h="wrapper";var P1h='<div class="';var S1h="_fnSetObjectDataFn";var c1h="Fn";var j1h="oApi";var Q1h="ext";var Y1h="dat";var k1h="name";var i1h="fieldTypes";var C1h="defaults";var v1h="Field";var q1h="8";var n1h="i1";var p1h="xtend";var g1h="nd";var x1h="ex";var G1h="se";var A1h="Dat";var X1h="ame";var l1h="cla";var D1h="=\"";var O1h="\" ";var y1h="label";var Z1h="/div>";var z1h="npu";var s1h="fo";var w1h="ul";var U4h="re";var S4h="</d";var a4h="ss=\"";var c4h="<";var V4h="message";var m4h="ass=\"";var L4h=">";var B4h="v>";var i4h="</";var C4h="om";var v4h="trol";var n4h="lab";var g4h="or";var x4h="rr";var f4h="-";var G4h="msg";var A4h="ulti";var T4h="k";var W4h="li";var I4h=true;var o4h=false;var X4h="length";var N4h='object';var l4h="th";var M4h="ng";var D4h="push";var y4h="files";var E4h="es";var w4h="ach";var t9w="]";var U9w="\"";var e9w="Editor";var P9w="DataTable";var S9w="_constructor";var R9w="or requires DataTables 1.10.7 or new";var c9w="Edit";var V9w="3fe2";var m9w="versionCheck";var j9w="3c5e";var Q9w='s';var Y9w='';var H9w=" ";var y9w="taTab";var d9w="4";var Z9w="sionCheck";var z9w="v";var s9w="7";var E9w=".";var w9w="10";var J5w="1.";var b5w="tor";var h5w="Fie";var t5w="ld";var U5w="dels";var e5w="ield";var P5w="mode";var S5w="ller";var a5w="Contro";var R5w="dis";var c5w="els";var V5w="od";var m5w="ls";var j5w="ur";var Q5w="bl";var Y5w="lose";var L5w="splay";var k5w="di";var H5w="Ta";var r5w="data";var B5w="jax";var i5w="ns";var C5w="butto";var v5w="cl";var q5w="nt";var n5w="epen";var p5w="tro";var g5w="displa";var x5w="toty";var F5w="fie";var f5w="fi";var G5w="prototyp";var K5w="y";var A5w="totyp";var T5w="inErro";var W5w="ot";var I5w="ine";var o5w="nl";var X5w="ge";var N5w="sa";var l5w="modif";var M5w="et";var D5w="ltiG";var O5w="mu";var u5w="roto";var y5w="iSe";var d5w="mul";var Z5w="ode";var z5w="ne";var s5w="otyp";var E5w="pla";var w5w="pro";var J0w="er";var b0w="gist";var h0w=")";var t0w="editor(";var U0w="ate()";var e0w="row.cre";var P0w="dit()";var S0w=".e";var a0w="row()";var R0w="ows().edit()";var c0w="ws().delete()";var V0w="t()";var m0w="().edi";var j0w="cell";var Q0w="ile()";var Y0w="f";var L0w="r.dt";var k0w="h";var H0w="oa";var r0w="up";var B0w="otot";var i0w="ototy";var C0w="mat";var v0w="ani";var q0w="ype";var n0w="protot";var p0w="lo";var g0w="_c";var x0w="_closeRe";var F0w="rototype";var f0w="Source";var G0w="_data";var K0w="der";var A0w="Reor";var T0w="play";var W0w="_dis";var I0w="_e";var o0w="mes";var X0w="Na";var N0w="_field";var l0w="rototy";var M0w="gacyAjax";var D0w="_le";var O0w="prototy";var u0w="en";var y0w="stop";var d0w="_p";var Z0w="otype";var z0w="totype";var s0w="ro";var E0w="it";var w0w="_sub";var J6w="Tabl";var b6w="_submit";var h6w="type";var t6w="pr";var U6w="ucces";var e6w="itS";var P6w="_subm";var S6w="proto";var a6w="ty";var R6w="prot";var c6w="rray";var V6w="_weakInA";var m6w="wId";var j6w="Ro";var Q6w="DT_";var Y6w="w";var L6w="entry";var k6w="w ";var H6w="Create ne";var r6w="C";var B6w="Edi";var i6w="ntry";var C6w="dit ";var v6w="ele";var q6w="te";var n6w="le";var p6w="De";var g6w="le values";var x6w="Multip";var F6w="an be edited individually, but not part of a group.";var f6w="This input c";var G6w="vious";var K6w="Pre";var A6w="Ne";var T6w="ruary";var W6w="Feb";var I6w="rch";var o6w="Ma";var X6w="l";var N6w="ri";var l6w="Ap";var M6w="un";var D6w="uly";var O6w="J";var u6w="ugus";var y6w="A";var d6w="ovember";var Z6w="N";var z6w="embe";var s6w="Dec";var E6w="o";var w6w="M";var J8w="u";var b8w="T";var h8w="We";var t8w="formOption";var U8w="basic";var e8w="ed";var P8w="chang";var S8w="formOpt";var a8w="lasses";var R8w="DTE_Processing_Indicato";var c8w="g";var V8w="essi";var m8w="proc";var j8w="oote";var Q8w="Info";var Y8w="TE_Form_";var L8w="_Form_Erro";var k8w="E";var H8w="Form_Button";var r8w="pe_";var B8w="eld_Ty";var i8w="Name_";var C8w="DTE_Field_";var v8w="eld_Input";var q8w="DTE_Fi";var n8w="ield_InputControl";var p8w="F";var g8w="DTE_";var x8w="ror";var F8w="ateEr";var f8w="TE_Field_St";var G8w="nfo";var K8w="DTE_Label_I";var A8w="d_Error";var T8w="el";var W8w="_Fi";var I8w="_Field_Message";var o8w="TE";var X8w="D";var N8w="Field_Info";var l8w="E_";var M8w="DT";var D8w="ue";var O8w="-val";var u8w="multi";var y8w="icator";var d8w="Processing_Ind";var Z8w="_";var z8w="DTE";var s8w="ate";var E8w="n_Cre";var w8w="tio";var J7w="DTE_Ac";var b7w="ble";var h7w="E_Bub";var t7w="DTE DT";var U7w="iner";var e7w="ble_L";var P7w="con close";var S7w="le_Triangle";var a7w="b";var R7w="DTE_Bu";var c7w="und";var V7w="gro";var m7w="DTE_Bubble_Back";var j7w="dTypes";var Q7w="fiel";var Y7w="xte";var L7w="ime";var k7w="DateT";var H7w="pe";var r7w="oty";var B7w="rot";var i7w="p";var C7w="faul";var v7w="de";var q7w="DD";var n7w="Y-MM-";var p7w="YY";var g7w="Y";var x7w="me";var F7w="dateti";var f7w="x";var G7w="ields";var K7w="itorF";var A7w="e";var T7w="S";var W7w="CLAS";var I7w="r";var o7w="to";var X7w="i";var N7w="Ed";var l7w="n";var M7w="io";var D7w="s";var O7w="ver";var R2w=500;var c2w=400;var m2w=100;var j2w=60;var v2w=27;var n2w=24;var g2w=20;var x2w=15;var f2w=13;var G2w=12;var K2w=11;var A2w=10;var W2w=7;var I2w=4;var o2w=3;var X2w=2;var N2w=1;var l2w=0;var M2w=O7w;M2w+=D7w;M2w+=M7w;M2w+=l7w;var D2w=N7w;D2w+=X7w;D2w+=o7w;D2w+=I7w;var O2w=W7w;O2w+=T7w;var d2w=A7w;d2w+=d2v.E7w;d2w+=K7w;d2w+=G7w;var Z2w=A7w;Z2w+=f7w;Z2w+=d2v.b2w;var v6J=F7w;v6J+=x7w;var q6J=g7w;q6J+=p7w;q6J+=n7w;q6J+=q7w;var n6J=v7w;n6J+=C7w;n6J+=d2v.b2w;n6J+=D7w;var s1J=i7w;s1J+=B7w;s1J+=r7w;s1J+=H7w;var E1J=k7w;E1J+=L7w;var w1J=A7w;w1J+=Y7w;w1J+=l7w;w1J+=d2v.E7w;var S91=Q7w;S91+=j7w;var O51=m7w;O51+=V7w;O51+=c7w;var u51=R7w;u51+=a7w;u51+=a7w;u51+=S7w;var y51=X7w;y51+=P7w;var d51=R7w;d51+=a7w;d51+=e7w;d51+=U7w;var Z51=t7w;Z51+=h7w;Z51+=b7w;var z51=J7w;z51+=w8w;z51+=E8w;z51+=s8w;var s51=z8w;s51+=Z8w;s51+=d8w;s51+=y8w;var E51=u8w;E51+=O8w;E51+=D8w;var w51=M8w;w51+=l8w;w51+=N8w;var J01=X8w;J01+=o8w;J01+=I8w;var b01=z8w;b01+=W8w;b01+=T8w;b01+=A8w;var h01=K8w;h01+=G8w;var t01=X8w;t01+=f8w;t01+=F8w;t01+=x8w;var U01=g8w;U01+=p8w;U01+=n8w;var e01=q8w;e01+=v8w;var P01=C8w;P01+=i8w;var S01=q8w;S01+=B8w;S01+=r8w;var a01=a7w;a01+=d2v.b2w;a01+=l7w;var R01=M8w;R01+=l8w;R01+=H8w;R01+=D7w;var c01=M8w;c01+=k8w;c01+=L8w;c01+=I7w;var V01=X8w;V01+=Y8w;V01+=Q8w;var m01=g8w;m01+=p8w;m01+=j8w;m01+=I7w;var j01=m8w;j01+=V8w;j01+=l7w;j01+=c8w;var Q01=R8w;Q01+=I7w;var Y01=d2v.h2w;Y01+=a8w;var b71=S8w;b71+=M7w;b71+=l7w;b71+=D7w;var h71=P8w;h71+=e8w;var t71=Z8w;t71+=U8w;var U71=t8w;U71+=D7w;var e71=d2v.J2w;e71+=d2v.w7w;var P71=T7w;P71+=d2v.J2w;P71+=d2v.b2w;var S71=h8w;S71+=d2v.E7w;var a71=b8w;a71+=J8w;a71+=A7w;var R71=w6w;R71+=E6w;R71+=l7w;var c71=s6w;c71+=z6w;c71+=I7w;var V71=Z6w;V71+=d6w;var m71=y6w;m71+=u6w;m71+=d2v.b2w;var j71=O6w;j71+=D6w;var Q71=O6w;Q71+=M6w;Q71+=A7w;var Y71=l6w;Y71+=N6w;Y71+=X6w;var L71=o6w;L71+=I6w;var k71=W6w;k71+=T6w;var H71=A6w;H71+=f7w;H71+=d2v.b2w;var r71=K6w;r71+=G6w;var B71=f6w;B71+=F6w;var i71=x6w;i71+=g6w;var C71=p6w;C71+=n6w;C71+=q6w;var v71=X8w;v71+=v6w;v71+=d2v.b2w;v71+=A7w;var q71=k8w;q71+=C6w;q71+=A7w;q71+=i6w;var n71=B6w;n71+=d2v.b2w;var p71=r6w;p71+=I7w;p71+=A7w;p71+=s8w;var g71=H6w;g71+=k6w;g71+=L6w;var x71=A6w;x71+=Y6w;var F71=Q6w;F71+=j6w;F71+=m6w;var f71=V6w;f71+=c6w;var G71=R6w;G71+=E6w;G71+=a6w;G71+=H7w;var E71=S6w;E71+=a6w;E71+=i7w;E71+=A7w;var U31=P6w;U31+=e6w;U31+=U6w;U31+=D7w;var e31=t6w;e31+=E6w;e31+=o7w;e31+=h6w;var r31=b6w;r31+=J6w;r31+=A7w;var E31=w0w;E31+=d2v.w7w;E31+=E0w;var w31=i7w;w31+=s0w;w31+=z0w;var j11=t6w;j11+=E6w;j11+=d2v.b2w;j11+=Z0w;var p11=d0w;p11+=E6w;p11+=y0w;p11+=u0w;var g11=O0w;g11+=H7w;var R41=D0w;R41+=M0w;var a9g=i7w;a9g+=l0w;a9g+=H7w;var c9g=N0w;c9g+=X0w;c9g+=o0w;var L9g=i7w;L9g+=l0w;L9g+=H7w;var b5g=I0w;b5g+=d2v.E7w;b5g+=X7w;b5g+=d2v.b2w;var H5g=W0w;H5g+=T0w;H5g+=A0w;H5g+=K0w;var v5g=G0w;v5g+=f0w;var n5g=i7w;n5g+=F0w;var g5g=x0w;g5g+=c8w;var x5g=O0w;x5g+=H7w;var o5g=g0w;o5g+=p0w;o5g+=D7w;o5g+=A7w;var X5g=n0w;X5g+=q0w;var a0g=Z8w;a0g+=v0w;a0g+=C0w;a0g+=A7w;var R0g=t6w;R0g+=i0w;R0g+=i7w;R0g+=A7w;var r8g=t6w;r8g+=B0w;r8g+=q0w;var r7g=r0w;r7g+=X6w;r7g+=H0w;r7g+=d2v.E7w;var x7g=f7w;x7g+=k0w;x7g+=L0w;var F7g=E6w;F7g+=l7w;var f7g=Y0w;f7g+=Q0w;var K7g=j0w;K7g+=m0w;K7g+=V0w;var T7g=s0w;T7g+=c0w;var o7g=I7w;o7g+=R0w;var X7g=a0w;X7g+=S0w;X7g+=P0w;var l7g=e0w;l7g+=U0w;var M7g=t0w;M7g+=h0w;var t2g=I7w;t2g+=A7w;t2g+=b0w;t2g+=J0w;var c2g=w5w;c2g+=d2v.b2w;c2g+=Z0w;var V2g=q6w;V2g+=d2v.w7w;V2g+=E5w;V2g+=q6w;var m2g=w5w;m2g+=d2v.b2w;m2g+=s5w;m2g+=A7w;var r2g=t6w;r2g+=E6w;r2g+=z0w;var y2g=w5w;y2g+=d2v.b2w;y2g+=r7w;y2g+=H7w;var Q3g=E6w;Q3g+=z5w;var r3g=E6w;r3g+=Y0w;r3g+=Y0w;var q3g=l7w;q3g+=Z5w;var n3g=w5w;n3g+=d2v.b2w;n3g+=Z0w;var x3g=d5w;x3g+=d2v.b2w;x3g+=y5w;x3g+=d2v.b2w;var F3g=i7w;F3g+=u5w;F3g+=a6w;F3g+=H7w;var W3g=O5w;W3g+=D5w;W3g+=M5w;var I3g=l5w;I3g+=X7w;I3g+=J0w;var o3g=w5w;o3g+=z0w;var N3g=O0w;N3g+=H7w;var y3g=o0w;y3g+=N5w;y3g+=X5w;var X1g=X7w;X1g+=o5w;X1g+=I5w;var N1g=w5w;N1g+=d2v.b2w;N1g+=W5w;N1g+=q0w;var O1g=T5w;O1g+=I7w;var u1g=i7w;u1g+=s0w;u1g+=A5w;u1g+=A7w;var d1g=X7w;d1g+=d2v.E7w;d1g+=D7w;var Z1g=t6w;Z1g+=B0w;Z1g+=K5w;Z1g+=H7w;var E1g=k0w;E1g+=X7w;E1g+=d2v.E7w;E1g+=A7w;var w1g=G5w;w1g+=A7w;var t4g=f5w;t4g+=n6w;var U4g=R6w;U4g+=Z0w;var a4g=F5w;a4g+=X6w;a4g+=d2v.E7w;var R4g=t6w;R4g+=E6w;R4g+=x5w;R4g+=H7w;var L4g=i7w;L4g+=u5w;L4g+=d2v.b2w;L4g+=q0w;var q4g=t6w;q4g+=B0w;q4g+=K5w;q4g+=H7w;var x4g=O0w;x4g+=H7w;var G4g=g5w;G4g+=K5w;var D4g=v7w;D4g+=D7w;D4g+=p5w;D4g+=K5w;var k99=d2v.E7w;k99+=n5w;k99+=v7w;k99+=q5w;var A99=i7w;A99+=F0w;var T99=v5w;T99+=E6w;T99+=D7w;T99+=A7w;var d99=i7w;d99+=s0w;d99+=A5w;d99+=A7w;var m59=C5w;m59+=i5w;var j59=w5w;j59+=z0w;var F09=d2v.J2w;F09+=B5w;var d09=d2v.J2w;d09+=d2v.E7w;d09+=d2v.E7w;var Z09=Y0w;Z09+=l7w;var a79=r5w;a79+=H5w;a79+=b7w;var R79=Y0w;R79+=l7w;var O29=k5w;O29+=L5w;var u29=d2v.J2w;u29+=X6w;u29+=X6w;var y29=d2v.h2w;y29+=Y5w;var d29=Q5w;d29+=j5w;var Z29=v5w;Z29+=E6w;Z29+=D7w;Z29+=A7w;var z29=d2v.w7w;z29+=E6w;z29+=v7w;z29+=m5w;var s29=d2v.w7w;s29+=V5w;s29+=c5w;var E29=R5w;E29+=T0w;E29+=a5w;E29+=S5w;var w29=P5w;w29+=X6w;w29+=D7w;var J39=d2v.E7w;J39+=E6w;J39+=d2v.w7w;var b39=P5w;b39+=X6w;b39+=D7w;var h39=p8w;h39+=e5w;var t39=d2v.w7w;t39+=E6w;t39+=U5w;var U39=p8w;U39+=X7w;U39+=A7w;U39+=t5w;var D49=i7w;D49+=l0w;D49+=H7w;var O49=h5w;O49+=X6w;O49+=d2v.E7w;var a5=p8w;a5+=e5w;var k5=Y0w;k5+=l7w;var H5=B6w;H5+=b5w;var B5=J5w;B5+=w9w;B5+=E9w;B5+=s9w;var i5=z9w;i5+=J0w;i5+=Z9w;var C5=d9w;C5+=Y0w;C5+=d9w;C5+=d9w;var v5=d2v.E7w;v5+=d2v.J2w;v5+=y9w;v5+=n6w;var q5=Y0w;q5+=l7w;'use strict';d2v.L0=function(k0){if(d2v&&k0)return d2v.M1(k0);};d2v.u0=function(y0){if(d2v)return d2v.M1(y0);};d2v.K2=function(A2){if(d2v)return d2v.D1(A2);};(function(){var L9w=' day';var k9w="log";var r9w="trial info -";var B9w="DataTables Editor ";var i9w="b2";var C9w="3a";var v9w="e6";var q9w="25";var n9w="ining";var p9w=" rema";var g9w='Editor - Trial expired';var x9w='for Editor, please see https://editor.datatables.net/purchase';var F9w=" Editor\n\n";var f9w="Thank you for trying DataTables";var G9w="o purchase a license ";var K9w="expired. T";var A9w="as now ";var T9w="Your trial h";var W9w="27";var I9w="2";var o9w="6eba";var X9w="getTime";var N9w=8578611689;var l9w="7235";var M9w="9";var D9w="tT";var O9w="ee";var u9w="c4";var U2w=1551225600;var P2w=1000;var Y2w=53;var K5=u9w;K5+=O9w;var A5=X5w;A5+=D9w;A5+=L7w;var T5=Y0w;T5+=M9w;T5+=A7w;T5+=d9w;var W5=d2v.h2w;W5+=A7w;W5+=X7w;W5+=X6w;d2v.z6=function(s6){if(d2v&&s6)return d2v.M1(s6);};d2v.v8=function(q8){if(d2v&&q8)return d2v.D1(q8);};d2v.x7=function(F7){if(d2v&&F7)return d2v.M1(F7);};var remaining=Math[d2v.K2(l9w)?W5:d2v.Z7w]((new Date((d2v.U2(T5)?N9w:U2w)*P2w)[A5]()-new Date()[X9w]())/(P2w*(d2v.x7(K5)?Y2w:j2w)*j2w*(d2v.J7(o9w)?n2w:x2w)));if(remaining<=l2w){var F5=I9w;F5+=W9w;F5+=a7w;var f5=T9w;f5+=A9w;f5+=K9w;f5+=G9w;var G5=f9w;G5+=F9w;alert(G5+f5+(d2v.v8(F5)?d2v.Z7w:x9w));throw g9w;}else if(remaining<=W2w){var n5=p9w;n5+=n9w;var p5=q9w;p5+=v9w;var g5=C9w;g5+=i9w;var x5=B9w;x5+=r9w;x5+=H9w;console[k9w](x5+remaining+(d2v.z6(g5)?d2v.Z7w:L9w)+(remaining===N2w?Y9w:d2v.B6(p5)?Q9w:d2v.Z7w)+n5);}}());var DataTable=$[q5][d2v.u0(j9w)?d2v.Z7w:v5];if(!DataTable||!DataTable[d2v.L0(C5)?m9w:d2v.Z7w]||!DataTable[i5](d2v.N5(V9w)?d2v.Z7w:B5)){var r5=c9w;r5+=R9w;r5+=J0w;throw r5;}var Editor=function(opts){var a9w="DataTables Editor must be initialised as a 'new' instance'";if(!(this instanceof Editor)){alert(a9w);}this[S9w](opts);};DataTable[H5]=Editor;$[k5][P9w][e9w]=Editor;var _editor_el=function(dis,ctx){var J9w="e=";var b9w="a-dte-";var h9w="*[dat";var Y5=U9w;Y5+=t9w;var L5=h9w;L5+=b9w;L5+=J9w;L5+=U9w;if(ctx===undefined){ctx=document;}return $(L5+dis+Y5,ctx);};var __inlineCounter=l2w;var _pluck=function(a,prop){var Q5=A7w;Q5+=w4h;var out=[];$[Q5](a,function(idx,el){var j5=i7w;j5+=J8w;j5+=D7w;j5+=k0w;out[j5](el[prop]);});return out;};var _api_file=function(name,id){var Z4h='Unknown file id ';var z4h="e ";var s4h="in tabl";var m5=f5w;m5+=X6w;m5+=E4h;var table=this[m5](name);var file=table[id];if(!file){var V5=H9w;V5+=s4h;V5+=z4h;throw Z4h+id+V5+name;}return table[id];};var _api_files=function(name){var u4h='Unknown file table name: ';var d4h="iles";if(!name){var c5=Y0w;c5+=d4h;return Editor[c5];}var table=Editor[y4h][name];if(!table){throw u4h+name;}return table;};var _objectKeys=function(o){var O4h="hasOwnProperty";var out=[];for(var key in o){if(o[O4h](key)){out[D4h](key);}}return out;};var _deepCompare=function(o1,o2){var R5=X6w;R5+=A7w;R5+=M4h;R5+=l4h;if(typeof o1!==N4h||typeof o2!==N4h){return o1==o2;}var o1Props=_objectKeys(o1);var o2Props=_objectKeys(o2);if(o1Props[X4h]!==o2Props[R5]){return o4h;}for(var i=l2w,ien=o1Props[X4h];i<ien;i++){var propName=o1Props[i];if(typeof o1[propName]===N4h){if(!_deepCompare(o1[propName],o2[propName])){return o4h;}}else if(o1[propName]!=o2[propName]){return o4h;}}return I4h;};Editor[a5]=function(opts,classes,host){var H3h='field-processing';var r3h='multi-value';var B3h='msg-message';var x3h='input-control';var A3h='<div data-dte-e="field-processing" class="';var T3h='msg-info';var I3h='<div data-dte-e="msg-message" class="';var o3h='"></div>';var X3h='msg-error';var N3h="restore";var M3h='<span data-dte-e="multi-info" class="';var u3h='<div data-dte-e="multi-value" class="';var d3h="inputControl";var Z3h='<div data-dte-e="input-control" class="';var z3h='<div data-dte-e="input" class="';var s3h='</label>';var h1h='<label data-dte-e="label" class="';var t1h="namePrefix";var a1h="valToData";var L1h="dataPro";var H1h='DTE_Field_';var r1h="own field type ";var B1h="Error adding field - unkn";var F1h="gs";var f1h="tti";var K1h="Pr";var T1h="alFrom";var W1h="ix";var I1h="pePre";var o1h="typ";var N1h="sN";var M1h="be";var u1h="dte-e=\"msg-label\" class=\"";var d1h="g-";var E1h="tiIn";var J4h="</span";var b4h="sg-multi\" class=\"";var h4h="e=\"m";var t4h="<div data-dte-";var e4h="sto";var P4h="multiRe";var R4h="div data-dte-e=\"msg-error\" cla";var j4h="dte-e=\"msg-info\" cl";var Q4h="<div data-";var Y4h="fieldIn";var k4h="</div";var H4h="an/>";var r4h="\"><sp";var q4h="input-con";var p4h="msg-";var F4h="msg-e";var K4h="ti-info";var z49=a6w;z49+=i7w;z49+=A7w;var s49=A7w;s49+=d2v.J2w;s49+=d2v.h2w;s49+=k0w;var w49=d2v.h2w;w49+=W4h;w49+=d2v.h2w;w49+=T4h;var J9=E6w;J9+=l7w;var b9=d2v.E7w;b9+=E6w;b9+=d2v.w7w;var P9=d2v.w7w;P9+=A4h;var S9=O5w;S9+=X6w;S9+=K4h;var a9=G4h;a9+=f4h;a9+=u8w;var R9=F4h;R9+=x4h;R9+=g4h;var c9=p4h;c9+=n4h;c9+=T8w;var V9=q4h;V9+=v4h;var m9=d2v.w7w;m9+=E6w;m9+=d2v.E7w;m9+=c5w;var j9=p8w;j9+=e5w;var Q9=d2v.E7w;Q9+=C4h;var k9=i4h;k9+=k5w;k9+=B4h;var H9=r4h;H9+=H4h;H9+=k4h;H9+=L4h;var r9=Y4h;r9+=Y0w;r9+=E6w;var B9=Q4h;B9+=j4h;B9+=m4h;var i9=G4h;i9+=f4h;i9+=V4h;var C9=c4h;C9+=R4h;C9+=a4h;var v9=S4h;v9+=X7w;v9+=B4h;var q9=P4h;q9+=e4h;q9+=U4h;var n9=t4h;n9+=h4h;n9+=b4h;var p9=i4h;p9+=k5w;p9+=z9w;p9+=L4h;var g9=J4h;g9+=L4h;var x9=d2v.w7w;x9+=w1h;x9+=E1h;x9+=s1h;var F9=U9w;F9+=L4h;var f9=X7w;f9+=z1h;f9+=d2v.b2w;var G9=c4h;G9+=Z1h;var K9=U9w;K9+=L4h;var A9=d2v.w7w;A9+=D7w;A9+=d1h;A9+=y1h;var T9=Q4h;T9+=u1h;var W9=O1h;W9+=Y0w;W9+=g4h;W9+=D1h;var I9=X6w;I9+=d2v.J2w;I9+=M1h;I9+=X6w;var o9=U9w;o9+=L4h;var X9=l1h;X9+=D7w;X9+=N1h;X9+=X1h;var N9=o1h;N9+=A7w;var l9=a6w;l9+=I1h;l9+=Y0w;l9+=W1h;var M9=d2v.E7w;M9+=d2v.J2w;M9+=d2v.b2w;M9+=d2v.J2w;var u9=z9w;u9+=T1h;u9+=A1h;u9+=d2v.J2w;var s9=r5w;s9+=K1h;s9+=E6w;s9+=i7w;var w9=X7w;w9+=d2v.E7w;var J5=l7w;J5+=d2v.J2w;J5+=d2v.w7w;J5+=A7w;var b5=o1h;b5+=A7w;var h5=G1h;h5+=f1h;h5+=l7w;h5+=F1h;var t5=h5w;t5+=t5w;var U5=x1h;U5+=d2v.b2w;U5+=A7w;U5+=g1h;var P5=A7w;P5+=p1h;var S5=n1h;S5+=q1h;S5+=l7w;var that=this;var multiI18n=host[S5][u8w];opts=$[P5](I4h,{},Editor[v1h][C1h],opts);if(!Editor[i1h][opts[h6w]]){var e5=B1h;e5+=r1h;throw e5+opts[h6w];}this[D7w]=$[U5]({},Editor[t5][h5],{type:Editor[i1h][opts[b5]],name:opts[J5],classes:classes,host:host,opts:opts,multiValue:o4h});if(!opts[w9]){var E9=X7w;E9+=d2v.E7w;opts[E9]=H1h+opts[k1h];}if(opts[s9]){var Z9=L1h;Z9+=i7w;var z9=Y1h;z9+=d2v.J2w;opts[z9]=opts[Z9];}if(opts[r5w]===Y9w){var y9=l7w;y9+=d2v.J2w;y9+=x7w;var d9=d2v.E7w;d9+=d2v.J2w;d9+=d2v.b2w;d9+=d2v.J2w;opts[d9]=opts[y9];}var dtPrivateApi=DataTable[Q1h][j1h];this[u9]=function(d){var R1h='editor';var V1h="bjectData";var m1h="_fnGetO";var D9=Y1h;D9+=d2v.J2w;var O9=m1h;O9+=V1h;O9+=c1h;return dtPrivateApi[O9](opts[D9])(d,R1h);};this[a1h]=dtPrivateApi[S1h](opts[M9]);var template=$(P1h+classes[e1h]+U1h+classes[l9]+opts[N9]+U1h+classes[t1h]+opts[k1h]+U1h+opts[X9]+o9+h1h+classes[I9]+W9+Editor[b1h](opts[J1h])+w3h+opts[y1h]+T9+classes[A9]+K9+opts[E3h]+G9+s3h+z3h+classes[f9]+F9+Z3h+classes[d3h]+y3h+u3h+classes[O3h]+w3h+multiI18n[D3h]+M3h+classes[x9]+w3h+multiI18n[l3h]+g9+p9+n9+classes[q9]+w3h+multiI18n[N3h]+v9+C9+classes[X3h]+o3h+I3h+classes[i9]+w3h+opts[V4h]+W3h+B9+classes[T3h]+w3h+opts[r9]+W3h+W3h+A3h+classes[K3h]+H9+k9);var input=this[G3h](f3h,opts);if(input!==F3h){_editor_el(x3h,template)[g3h](input);}else{var Y9=p3h;Y9+=z5w;var L9=d2v.h2w;L9+=n3h;template[L9](q3h,Y9);}this[Q9]=$[v3h](I4h,{},Editor[j9][m9][C3h],{container:template,inputControl:_editor_el(V9,template),label:_editor_el(i3h,template),fieldInfo:_editor_el(T3h,template),labelInfo:_editor_el(c9,template),fieldError:_editor_el(R9,template),fieldMessage:_editor_el(B3h,template),multi:_editor_el(r3h,template),multiReturn:_editor_el(a9,template),multiInfo:_editor_el(S9,template),processing:_editor_el(H3h,template)});this[C3h][P9][k3h](L3h,function(){var m3h="asC";var j3h="isabled";var Y3h="don";var h9=U4h;h9+=d2v.J2w;h9+=Y3h;h9+=Q3h;var t9=d2v.b2w;t9+=K5w;t9+=H7w;var U9=d2v.E7w;U9+=j3h;var e9=k0w;e9+=m3h;e9+=V3h;if(that[D7w][c3h][R3h]&&!template[e9](classes[U9])&&opts[t9]!==h9){that[a3h](Y9w);that[S3h]();}});this[b9][P3h][J9](w49,function(){var e3h="tiRestore";var E49=d2v.w7w;E49+=w1h;E49+=e3h;that[E49]();});$[s49](this[D7w][z49],function(name,fn){if(typeof fn===d2v.d7w&&that[name]===undefined){that[name]=function(){var u49=d2v.J2w;u49+=i7w;u49+=i7w;u49+=Q3h;var y49=U3h;y49+=H7w;y49+=c1h;var d49=t3h;d49+=h3h;var Z49=b3h;Z49+=J3h;var args=Array[w2h][Z49][d49](arguments);args[E2h](name);var ret=that[y49][u49](that,args);return ret===undefined?that:ret;};}});};Editor[O49][D49]={def:function(set){var Z2h='default';var z2h="ault";var N49=d2v.E7w;N49+=A7w;N49+=Y0w;var M49=E6w;M49+=i7w;M49+=d2v.b2w;M49+=D7w;var opts=this[D7w][M49];if(set===undefined){var l49=s2h;l49+=z2h;var def=opts[Z2h]!==undefined?opts[l49]:opts[s2h];return typeof def===d2v.d7w?def():def;}opts[N49]=set;return this;},disable:function(){var l2h="tainer";var D2h="ddCl";var y2h="yp";var T49=k5w;T49+=N5w;T49+=b7w;var W49=d2h;W49+=y2h;W49+=u2h;var I49=d2v.h2w;I49+=X6w;I49+=O2h;I49+=E4h;var o49=d2v.J2w;o49+=D2h;o49+=d2v.J2w;o49+=n3h;var X49=M2h;X49+=l2h;this[C3h][X49][o49](this[D7w][I49][N2h]);this[W49](T49);return this;},displayed:function(){var W2h="ntainer";var X2h="displ";var f49=X2h;f49+=o2h;var G49=a7w;G49+=E6w;G49+=d2v.E7w;G49+=K5w;var K49=I2h;K49+=W2h;var A49=d2v.E7w;A49+=E6w;A49+=d2v.w7w;var container=this[A49][K49];return container[T2h](G49)[X4h]&&container[A2h](f49)!=K2h?I4h:o4h;},enable:function(){var x2h='enable';var f2h="moveClass";var g49=k5w;g49+=D7w;g49+=G2h;g49+=e8w;var x49=l1h;x49+=D7w;x49+=G1h;x49+=D7w;var F49=U4h;F49+=f2h;this[C3h][F2h][F49](this[D7w][x49][g49]);this[G3h](x2h);return this;},enabled:function(){return this[C3h][F2h][g2h](this[D7w][p2h][N2h])===o4h;},error:function(msg,fn){var i2h="_typeF";var C2h="rrorMessage";var v2h="sg";var q2h="_m";var n2h="eldError";var k49=f5w;k49+=n2h;var H49=d2v.E7w;H49+=E6w;H49+=d2v.w7w;var r49=q2h;r49+=v2h;var B49=A7w;B49+=C2h;var i49=i2h;i49+=l7w;var p49=B2h;p49+=D7w;p49+=A7w;p49+=D7w;var classes=this[D7w][p49];if(msg){var v49=r2h;v49+=H2h;v49+=D7w;v49+=D7w;var q49=k2h;q49+=X7w;q49+=l7w;q49+=J0w;var n49=L2h;n49+=d2v.w7w;this[n49][q49][v49](classes[Y2h]);}else{var C49=L2h;C49+=d2v.w7w;this[C49][F2h][Q2h](classes[Y2h]);}this[i49](B49,msg);return this[r49](this[H49][k49],msg,fn);},fieldInfo:function(msg){var V2h="_ms";var Q49=j2h;Q49+=m2h;Q49+=s1h;var Y49=d2v.E7w;Y49+=E6w;Y49+=d2v.w7w;var L49=V2h;L49+=c8w;return this[L49](this[Y49][Q49],msg);},isMultiValue:function(){var c2h="tiVal";var m49=n6w;m49+=M4h;m49+=d2v.b2w;m49+=k0w;var j49=d5w;j49+=c2h;j49+=D8w;return this[D7w][j49]&&this[D7w][R2h][m49]!==N2w;},inError:function(){var c49=a2h;c49+=S2h;var V49=L2h;V49+=d2v.w7w;return this[V49][c49][g2h](this[D7w][p2h][Y2h]);},input:function(){var h2h='input, select, textarea';var e2h="peF";var a49=P2h;a49+=I7w;var R49=Z8w;R49+=a6w;R49+=e2h;R49+=l7w;return this[D7w][h6w][U2h]?this[R49](t2h):$(h2h,this[C3h][a49]);},focus:function(){var E7h="xta";var w7h="input, select, t";var S49=d2v.b2w;S49+=K5w;S49+=i7w;S49+=A7w;if(this[D7w][S49][S3h]){this[G3h](b2h);}else{var U49=J2h;U49+=J0w;var e49=d2v.E7w;e49+=E6w;e49+=d2v.w7w;var P49=w7h;P49+=A7w;P49+=E7h;P49+=s7h;$(P49,this[e49][U49])[S3h]();}return this;},get:function(){var Z7h='get';var t49=d2v.E7w;t49+=A7w;t49+=Y0w;if(this[z7h]()){return undefined;}var val=this[G3h](Z7h);return val!==undefined?val:this[t49]();},hide:function(animate){var O7h="slideUp";var y7h="tain";var J49=Y0w;J49+=l7w;var b49=k0w;b49+=E6w;b49+=d7h;var h49=d2v.h2w;h49+=k3h;h49+=y7h;h49+=J0w;var el=this[C3h][h49];if(animate===undefined){animate=I4h;}if(this[D7w][b49][u7h]()&&animate&&$[J49][O7h]){el[O7h]();}else{var E19=l7w;E19+=E6w;E19+=l7w;E19+=A7w;var w19=d2v.h2w;w19+=D7w;w19+=D7w;el[w19](q3h,E19);}return this;},label:function(str){var N7h="lInfo";var d19=D7h;d19+=i7w;d19+=A7w;d19+=g1h;var Z19=M7h;Z19+=l7h;var z19=n4h;z19+=A7w;z19+=N7h;var s19=L2h;s19+=d2v.w7w;var label=this[C3h][y1h];var labelInfo=this[s19][z19][X7h]();if(str===undefined){return label[o7h]();}label[Z19](str);label[d19](labelInfo);return this;},labelInfo:function(msg){var I7h="_msg";return this[I7h](this[C3h][E3h],msg);},message:function(msg,fn){var W7h="fieldMessage";var u19=L2h;u19+=d2v.w7w;var y19=Z8w;y19+=G4h;return this[y19](this[u19][W7h],msg,fn);},multiGet:function(id){var value;var multiValues=this[D7w][T7h];var multiIds=this[D7w][R2h];if(id===undefined){var O19=A7h;O19+=l4h;value={};for(var i=l2w;i<multiIds[O19];i++){value[multiIds[i]]=this[z7h]()?multiValues[multiIds[i]]:this[a3h]();}}else if(this[z7h]()){value=multiValues[id];}else{var D19=z9w;D19+=d2v.J2w;D19+=X6w;value=this[D19]();}return value;},multiRestore:function(){var G7h="iValue";var M19=d2v.w7w;M19+=K7h;M19+=G7h;this[D7w][M19]=I4h;this[f7h]();},multiSet:function(id,val){var q7h="alues";var n7h="multiV";var p7h="ltiIds";var g7h="bject";var x7h="PlainO";var I19=F7h;I19+=x7h;I19+=g7h;var N19=d2v.w7w;N19+=J8w;N19+=p7h;var l19=n7h;l19+=q7h;var multiValues=this[D7w][l19];var multiIds=this[D7w][N19];if(val===undefined){val=id;id=undefined;}var set=function(idSrc,val){var v7h="inArr";var X19=v7h;X19+=o2h;if($[X19](multiIds)===-N2w){var o19=C7h;o19+=D7w;o19+=k0w;multiIds[o19](idSrc);}multiValues[idSrc]=val;};if($[I19](val)&&id===undefined){$[i7h](val,function(idSrc,innerVal){set(idSrc,innerVal);});}else if(id===undefined){$[i7h](multiIds,function(i,idSrc){set(idSrc,val);});}else{set(id,val);}this[D7w][O3h]=I4h;this[f7h]();return this;},name:function(){var T19=l7w;T19+=X1h;var W19=E6w;W19+=B7h;return this[D7w][W19][T19];},node:function(){return this[C3h][F2h][l2w];},processing:function(set){var H7h="ocessing";var f19=l7w;f19+=E6w;f19+=l7w;f19+=A7w;var G19=k5w;G19+=r7h;G19+=o2h;var K19=t6w;K19+=H7h;var A19=L2h;A19+=d2v.w7w;this[A19][K19][A2h](G19,set?k7h:f19);return this;},set:function(val,multiCheck){var Q7h="ltiValue";var Y7h="entityDecod";var L7h="_typ";var i19=G1h;i19+=d2v.b2w;var C19=L7h;C19+=u2h;var n19=Y7h;n19+=A7w;var p19=O5w;p19+=Q7h;var decodeFn=function(d){var h7h='\n';var t7h='\'';var U7h='"';var P7h='<';var a7h='>';var m7h="lac";var g19=j7h;g19+=m7h;g19+=A7w;var x19=j7h;x19+=X6w;x19+=V7h;x19+=A7w;var F19=c7h;F19+=d2v.J2w;F19+=J3h;return typeof d!==R7h?d:d[F19](/&gt;/g,a7h)[S7h](/&lt;/g,P7h)[x19](/&amp;/g,e7h)[S7h](/&quot;/g,U7h)[S7h](/&#39;/g,t7h)[g19](/&#10;/g,h7h);};this[D7w][p19]=o4h;var decode=this[D7w][c3h][n19];if(decode===undefined||decode===I4h){var q19=X7w;q19+=b7h;q19+=o2h;if($[q19](val)){var v19=n6w;v19+=M4h;v19+=l4h;for(var i=l2w,ien=val[v19];i<ien;i++){val[i]=decodeFn(val[i]);}}else{val=decodeFn(val);}}this[C19](i19,val);if(multiCheck===undefined||multiCheck===I4h){this[f7h]();}return this;},show:function(animate){var z8h="slideDo";var w8h="eDown";var J7h="slid";var H19=J7h;H19+=w8h;var r19=P2h;r19+=I7w;var B19=d2v.E7w;B19+=E6w;B19+=d2v.w7w;var el=this[B19][r19];if(animate===undefined){animate=I4h;}if(this[D7w][E8h][u7h]()&&animate&&$[s8h][H19]){var k19=z8h;k19+=Z8h;el[k19]();}else{var L19=d2v.h2w;L19+=D7w;L19+=D7w;el[L19](q3h,Y9w);}return this;},val:function(val){var Q19=G1h;Q19+=d2v.b2w;var Y19=c8w;Y19+=A7w;Y19+=d2v.b2w;return val===undefined?this[Y19]():this[Q19](val);},compare:function(value,original){var d8h="compare";var compare=this[D7w][c3h][d8h]||_deepCompare;return compare(value,original);},dataSrc:function(){var j19=y8h;j19+=d2v.b2w;j19+=D7w;return this[D7w][j19][r5w];},destroy:function(){var O8h="eF";var u8h="stroy";var R19=d2v.E7w;R19+=A7w;R19+=u8h;var c19=U3h;c19+=i7w;c19+=O8h;c19+=l7w;var V19=I2h;V19+=D8h;V19+=A7w;V19+=I7w;var m19=d2v.E7w;m19+=E6w;m19+=d2v.w7w;this[m19][V19][M8h]();this[c19](R19);return this;},multiEditable:function(){var a19=E6w;a19+=i7w;a19+=d2v.b2w;a19+=D7w;return this[D7w][a19][R3h];},multiIds:function(){return this[D7w][R2h];},multiInfoShown:function(show){var N8h="multiInfo";var U19=l7w;U19+=E6w;U19+=l7w;U19+=A7w;var e19=Q5w;e19+=E6w;e19+=d2v.h2w;e19+=T4h;var P19=l8h;P19+=D7w;var S19=d2v.E7w;S19+=C4h;this[S19][N8h][P19]({display:show?e19:U19});},multiReset:function(){this[D7w][R2h]=[];this[D7w][T7h]={};},submittable:function(){var h19=X8h;h19+=E0w;var t19=E6w;t19+=i7w;t19+=o8h;return this[D7w][t19][h19];},valFromData:F3h,valToData:F3h,_errorNode:function(){var I8h="fieldError";var b19=d2v.E7w;b19+=E6w;b19+=d2v.w7w;return this[b19][I8h];},_msg:function(el,msg,fn){var q8h="lock";var n8h="Up";var p8h="slideDown";var g8h="tm";var A8h="fun";var T8h="ible";var W8h=":v";var Z39=Y0w;Z39+=l7w;var z39=W8h;z39+=F7h;z39+=T8h;var s39=X7w;s39+=D7w;var w39=A8h;w39+=K8h;w39+=G8h;if(msg===undefined){var J19=k0w;J19+=d2v.b2w;J19+=d2v.w7w;J19+=X6w;return el[J19]();}if(typeof msg===w39){var E39=y6w;E39+=i7w;E39+=X7w;var editor=this[D7w][E8h];msg=msg(editor,new DataTable[E39](editor[D7w][f8h]));}if(el[F8h]()[s39](z39)&&$[Z39][x8h]){var d39=k0w;d39+=g8h;d39+=X6w;el[d39](msg);if(msg){el[p8h](fn);}else{var y39=b3h;y39+=d2v.E7w;y39+=A7w;y39+=n8h;el[y39](fn);}}else{var M39=p3h;M39+=l7w;M39+=A7w;var D39=a7w;D39+=q8h;var O39=l8h;O39+=D7w;var u39=M7h;u39+=l7h;el[u39](msg||Y9w)[O39](q3h,msg?D39:M39);if(fn){fn();}}return this;},_multiValueCheck:function(){var b8h="inputCo";var t8h="nputControl";var S8h="Values";var a8h="ltiVa";var R8h="iEditable";var c8h="Val";var V8h="isMult";var Y8h="multiI";var L8h="Multi";var k8h="Class";var H8h="gle";var B8h="sse";var i8h="iNoEdit";var v8h="iInfo";var S39=Z8w;S39+=d2v.w7w;S39+=K7h;S39+=v8h;var a39=k0w;a39+=E6w;a39+=D7w;a39+=d2v.b2w;var R39=C8h;R39+=i8h;var c39=v5w;c39+=d2v.J2w;c39+=B8h;c39+=D7w;var V39=r8h;V39+=H8h;V39+=k8h;var m39=l7w;m39+=E6w;m39+=L8h;var j39=X7w;j39+=l7w;j39+=Y0w;j39+=E6w;var Q39=k0w;Q39+=d2v.b2w;Q39+=l7h;var Y39=Y8h;Y39+=G8w;var L39=O5w;L39+=Q8h;L39+=X7w;var k39=X7w;k39+=j8h;k39+=q1h;k39+=l7w;var H39=m8h;H39+=D7w;H39+=d2v.b2w;var r39=a7w;r39+=p0w;r39+=d2v.h2w;r39+=T4h;var B39=n6w;B39+=l7w;B39+=c8w;B39+=l4h;var i39=d2v.h2w;i39+=D7w;i39+=D7w;var C39=L2h;C39+=d2v.w7w;var W39=V8h;W39+=X7w;W39+=c8h;W39+=D8w;var o39=C8h;o39+=R8h;var X39=E6w;X39+=i7w;X39+=o8h;var N39=O5w;N39+=a8h;N39+=X6w;N39+=D8w;var l39=u8w;l39+=S8h;var last;var ids=this[D7w][R2h];var values=this[D7w][l39];var isMultiValue=this[D7w][N39];var isMultiEditable=this[D7w][X39][o39];var val;var different=o4h;if(ids){var I39=P8h;I39+=e8h;I39+=k0w;for(var i=l2w;i<ids[I39];i++){val=values[ids[i]];if(i>l2w&&!_deepCompare(val,last)){different=I4h;break;}last=val;}}if(different&&isMultiValue||!isMultiEditable&&this[W39]()){var x39=a7w;x39+=p0w;x39+=U8h;var F39=l8h;F39+=D7w;var f39=d2v.E7w;f39+=E6w;f39+=d2v.w7w;var G39=l7w;G39+=E6w;G39+=l7w;G39+=A7w;var K39=l8h;K39+=D7w;var A39=X7w;A39+=t8h;var T39=L2h;T39+=d2v.w7w;this[T39][A39][K39]({display:G39});this[f39][u8w][F39]({display:x39});}else{var v39=l7w;v39+=h8h;var q39=O5w;q39+=X6w;q39+=d2v.b2w;q39+=X7w;var n39=d2v.E7w;n39+=C4h;var p39=b8h;p39+=l7w;p39+=v4h;var g39=d2v.E7w;g39+=E6w;g39+=d2v.w7w;this[g39][p39][A2h]({display:k7h});this[n39][q39][A2h]({display:v39});if(isMultiValue&&!different){this[J8h](last,o4h);}}this[C39][P3h][i39]({display:ids&&ids[B39]>N2w&&different&&!isMultiValue?r39:K2h});var i18n=this[D7w][H39][k39][L39];this[C3h][Y39][Q39](isMultiEditable?i18n[j39]:i18n[m39]);this[C3h][u8w][V39](this[D7w][c39][R39],!isMultiEditable);this[D7w][a39][S39]();return I4h;},_typeFn:function(name){var P39=E6w;P39+=i7w;P39+=d2v.b2w;P39+=D7w;var args=Array[w2h][w6h][E6h](arguments);args[s6h]();args[E2h](this[D7w][P39]);var fn=this[D7w][h6w][name];if(fn){var e39=z6h;e39+=X6w;e39+=K5w;return fn[e39](this[D7w][E8h],args);}}};Editor[U39][Z6h]={};Editor[v1h][C1h]={"className":d2v.Z7w,"data":d2v.Z7w,"def":d2v.Z7w,"fieldInfo":d2v.Z7w,"id":d2v.Z7w,"label":d2v.Z7w,"labelInfo":d2v.Z7w,"name":F3h,"type":d6h,"message":d2v.Z7w,"multiEditable":I4h,"submit":I4h};Editor[v1h][t39][y6h]={type:F3h,name:F3h,classes:F3h,opts:F3h,host:F3h};Editor[h39][b39][J39]={container:F3h,label:F3h,labelInfo:F3h,fieldInfo:F3h,fieldError:F3h,fieldMessage:F3h};Editor[w29]={};Editor[Z6h][E29]={"init":function(dte){},"open":function(dte,append,fn){},"close":function(dte,fn){}};Editor[s29][u6h]={"create":function(conf){},"get":function(conf){},"set":function(conf,val){},"enable":function(conf){},"disable":function(conf){}};Editor[z29][y6h]={"ajaxUrl":F3h,"ajax":F3h,"dataSource":F3h,"domTable":F3h,"opts":F3h,"displayController":F3h,"fields":{},"order":[],"id":-N2w,"displayed":o4h,"processing":o4h,"modifier":F3h,"action":F3h,"idSrc":F3h,"unique":l2w};Editor[Z6h][O6h]={"label":F3h,"fn":F3h,"className":F3h};Editor[Z6h][D6h]={onReturn:M6h,onBlur:Z29,onBackground:d29,onComplete:l6h,onEsc:y29,onFieldError:b2h,submit:u29,focus:l2w,buttons:I4h,title:I4h,message:I4h,drawType:o4h,scope:N6h};Editor[O29]={};(function(window,document,$,DataTable){var r5h="lightbox";var B5h='<div class="DTED_Lightbox_Background"><div/></div>';var i5h='<div class="DTED_Lightbox_Content">';var C5h='<div class="DTED_Lightbox_Content_Wrapper">';var e0h='div.DTED_Lightbox_Shown';var c0h="_scrollTop";var B0h='click.DTED_Lightbox';var f0h='DTED_Lightbox_Mobile';var V6h="apper";var g6h="Lightbox_Wrapper\">";var x6h="TED DTED_";var F6h="<div class=\"D";var f6h="ED_Lightbox_Container\">";var G6h="ass=\"DT";var A6h="/div";var T6h="s=\"DTED_Lightbox_Close\"></div>";var q2w=25;var c79=I2h;c79+=X6h;var V79=d2v.E7w;V79+=X7w;V79+=o6h;V79+=I6h;var m79=W6h;m79+=B2h;m79+=T6h;var j79=i4h;j79+=d2v.E7w;j79+=X7w;j79+=B4h;var Q79=c4h;Q79+=A6h;Q79+=L4h;var Y79=K6h;Y79+=G6h;Y79+=f6h;var L79=F6h;L79+=x6h;L79+=g6h;var M29=d2v.w7w;M29+=E6w;M29+=v7w;M29+=m5w;var D29=W4h;D29+=p6h;D29+=n6h;D29+=f7w;var self;Editor[u7h][D29]=$[v3h](I4h,{},Editor[M29][q6h],{"init":function(dte){var l29=Z8w;l29+=v6h;l29+=d2v.b2w;self[l29]();return self;},"open":function(dte,append,callback){var r6h="sho";var C6h="how";var T29=Z8w;T29+=D7w;T29+=C6h;var W29=Z8w;W29+=i6h;W29+=l7w;var I29=d2v.h2w;I29+=X6w;I29+=B6h;var o29=a2h;o29+=u0w;o29+=d2v.b2w;var X29=Z8w;X29+=d2v.E7w;X29+=d2v.b2w;X29+=A7w;var N29=Z8w;N29+=r6h;N29+=Y6w;N29+=l7w;if(self[N29]){if(callback){callback();}return;}self[X29]=dte;var content=self[H6h][o29];content[k6h]()[X7h]();content[L6h](append)[L6h](self[H6h][I29]);self[W29]=I4h;self[T29](callback);},"close":function(dte,callback){var Q6h="_shown";var A29=Z8w;A29+=k0w;A29+=Y6h;if(!self[Q6h]){if(callback){callback();}return;}self[j6h]=dte;self[A29](callback);self[Q6h]=o4h;},node:function(dte){var G29=m6h;G29+=V6h;var K29=Z8w;K29+=C3h;return self[K29][G29][l2w];},"_init":function(){var b6h='div.DTED_Lightbox_Content';var a6h="bac";var R6h="city";var c6h="opa";var v29=c6h;v29+=R6h;var q29=d2v.h2w;q29+=n3h;var n29=a6h;n29+=S6h;var p29=E6w;p29+=i7w;p29+=d2v.J2w;p29+=R6h;var g29=P6h;g29+=i7w;g29+=e6h;var x29=Y6w;x29+=U6h;var F29=Z8w;F29+=d2v.E7w;F29+=E6w;F29+=d2v.w7w;var f29=M2h;f29+=t6h;if(self[h6h]){return;}var dom=self[H6h];dom[f29]=$(b6h,self[F29][x29]);dom[g29][A2h](p29,l2w);dom[n29][q29](v29,l2w);},"_show":function(callback){var P0h='<div class="DTED_Lightbox_Shown"/>';var S0h="ou";var a0h="backgr";var R0h="appe";var L0h='div.DTED_Lightbox_Content_Wrapper';var G0h="ddClass";var A0h="orien";var T0h="ont";var W0h="hei";var I0h="au";var o0h="tAn";var M0h="ound";var D0h="ckgr";var O0h="ghtbox";var u0h="ck.DTED_Li";var d0h="D_Lightbox";var Z0h="k.DTE";var s0h="bi";var E0h="lTop";var J6h="ienta";var s79=g4h;s79+=J6h;s79+=w8w;s79+=l7w;var E79=w0h;E79+=E0h;var w79=a7w;w79+=E6w;w79+=d2v.E7w;w79+=K5w;var J29=s0h;J29+=l7w;J29+=d2v.E7w;var t29=v5w;t29+=z0h;t29+=Z0h;t29+=d0h;var U29=a7w;U29+=X7w;U29+=l7w;U29+=d2v.E7w;var e29=Y6w;e29+=I7w;e29+=V6h;var a29=y0h;a29+=u0h;a29+=O0h;var c29=v5w;c29+=E6w;c29+=G1h;var m29=a7w;m29+=d2v.J2w;m29+=D0h;m29+=M0h;var j29=P6h;j29+=l0h;var Q29=a7w;Q29+=N0h;var Y29=X0h;Y29+=G1h;Y29+=o0h;Y29+=X7w;var L29=d2v.h2w;L29+=D7w;L29+=D7w;var k29=I0h;k29+=o7w;var H29=W0h;H29+=p6h;var r29=d2v.h2w;r29+=T0h;r29+=A7w;r29+=q5w;var C29=A0h;C29+=d2v.b2w;C29+=K0h;C29+=G8h;var that=this;var dom=self[H6h];if(window[C29]!==undefined){var B29=d2v.J2w;B29+=G0h;var i29=a7w;i29+=V5w;i29+=K5w;$(i29)[B29](f0h);}dom[r29][A2h](H29,k29);dom[e1h][L29]({top:-self[F0h][Y29]});$(x0h)[L6h](self[H6h][Q29])[L6h](self[H6h][e1h]);self[g0h]();self[j6h][p0h](dom[j29],{opacity:N2w,top:l2w},callback);self[j6h][p0h](dom[m29],{opacity:N2w});setTimeout(function(){var C0h='text-indent';var v0h="_Foot";var q0h=".DTE";var V29=n0h;V29+=q0h;V29+=v0h;V29+=J0w;$(V29)[A2h](C0h,-N2w);},A2w);dom[c29][i0h](B0h,function(e){var R29=r0h;R29+=A7w;self[j6h][R29]();});dom[H0h][i0h](a29,function(e){var k0h="ckground";var P29=a7w;P29+=d2v.J2w;P29+=k0h;var S29=Z8w;S29+=d2v.E7w;S29+=d2v.b2w;S29+=A7w;self[S29][P29]();});$(L0h,dom[e29])[U29](t29,function(e){var Q0h="ghtbox_Content_Wr";var Y0h="DTED_Li";var b29=Y0h;b29+=Q0h;b29+=z6h;b29+=J0w;var h29=j0h;h29+=I7w;h29+=m0h;if($(e[h29])[g2h](b29)){self[j6h][H0h]();}});$(window)[J29](V0h,function(){self[g0h]();});self[c0h]=$(w79)[E79]();if(window[s79]!==undefined){var M79=R0h;M79+=g1h;var D79=D7h;D79+=i7w;D79+=u0w;D79+=d2v.E7w;var O79=n6h;O79+=d2v.E7w;O79+=K5w;var u79=m6h;u79+=z6h;u79+=J0w;var y79=l7w;y79+=E6w;y79+=d2v.b2w;var d79=a0h;d79+=S0h;d79+=g1h;var Z79=l7w;Z79+=E6w;Z79+=d2v.b2w;var z79=n6h;z79+=d2v.E7w;z79+=K5w;var kids=$(z79)[k6h]()[Z79](dom[d79])[y79](dom[u79]);$(O79)[D79](P0h);$(e0h)[M79](kids);}},"_heightCalc":function(){var y5h='maxHeight';var z5h="eight";var s5h="ding";var E5h="windowPad";var J0h="div.";var h0h="div.DTE_Foo";var t0h="erHei";var K79=d2v.h2w;K79+=D7w;K79+=D7w;var A79=U0h;A79+=t0h;A79+=p6h;var T79=h0h;T79+=d2v.b2w;T79+=A7w;T79+=I7w;var W79=P6h;W79+=b0h;W79+=I7w;var I79=J0h;I79+=w5h;var o79=E5h;o79+=s5h;var X79=I2h;X79+=l7w;X79+=Y0w;var N79=k0w;N79+=z5h;var l79=Z8w;l79+=L2h;l79+=d2v.w7w;var dom=self[l79];var maxHeight=$(window)[N79]()-self[X79][o79]*X2w-$(I79,dom[W79])[Z5h]()-$(T79,dom[e1h])[A79]();$(d5h,dom[e1h])[K79](y5h,maxHeight);},"_hide":function(callback){var n5h="scrollTop";var g5h="hildren";var f5h="ientati";var K5h="_ani";var A5h="etAn";var T5h="ffs";var W5h="ni";var o5h="DTED_Lightbox";var X5h="click.";var N5h="Lightbox";var l5h="lick.DTED_";var M5h="div.DTED_Lightbox_Content_Wr";var u5h="unb";var k79=u5h;k79+=O5h;var H79=Y6w;H79+=D5h;H79+=e6h;var r79=M5h;r79+=V6h;var B79=d2v.h2w;B79+=l5h;B79+=N5h;var i79=X5h;i79+=o5h;var C79=I5h;C79+=W5h;C79+=C0w;C79+=A7w;var v79=E6w;v79+=T5h;v79+=A5h;v79+=X7w;var q79=d2v.h2w;q79+=E6w;q79+=l7w;q79+=Y0w;var n79=m6h;n79+=D7h;n79+=H7w;n79+=I7w;var p79=K5h;p79+=G5h;var g79=Z8w;g79+=d2v.E7w;g79+=d2v.b2w;g79+=A7w;var f79=g4h;f79+=f5h;f79+=k3h;var G79=F5h;G79+=C4h;var dom=self[G79];if(!callback){callback=function(){};}if(window[f79]!==undefined){var x79=x5h;x79+=A7w;var F79=d2v.h2w;F79+=g5h;var show=$(e0h);show[F79]()[p5h](x0h);show[x79]();}$(x0h)[Q2h](f0h)[n5h](self[c0h]);self[g79][p79](dom[n79],{opacity:l2w,top:self[q79][v79]},function(){$(this)[X7h]();callback();});self[j6h][C79](dom[H0h],{opacity:l2w},function(){$(this)[X7h]();});dom[q5h][v5h](i79);dom[H0h][v5h](B79);$(r79,dom[H79])[v5h](B0h);$(window)[k79](V0h);},"_dte":F3h,"_ready":o4h,"_shown":o4h,"_dom":{"wrapper":$(L79+Y79+C5h+i5h+Q79+W3h+W3h+j79),"background":$(B5h),"close":$(m79),"content":F3h}});self=Editor[V79][r5h];self[c79]={"offsetAni":q2w,"windowPadding":q2w};}(window,document,jQuery,jQuery[R79][a79]));(function(window,document,$,DataTable){var P4b='<div class="DTED_Envelope_Close">&times;</div>';var S4b='<div class="DTED_Envelope_Container"></div>';var a9h="offsetHeight";var H9h="wrap";var X9h="_cssBackgroundOpacity";var s9h="backgro";var h5h="appendChild";var R5h="envelope";var c5h="TED DTED_Envelope_Wrapper\">";var V5h="class=\"D";var m5h="s=\"DTED_Envelope_Shadow\"></div>";var L5h="D_Envelope_Background\"><div/></div>";var k5h="ass=\"DTE";var H5h="onf";var S2w=600;var L2w=50;var z09=d2v.h2w;z09+=H5h;var s09=K6h;s09+=k5h;s09+=L5h;var E09=S4h;E09+=Y5h;E09+=L4h;var w09=Q5h;w09+=j5h;w09+=m5h;var J69=W6h;J69+=V5h;J69+=c5h;var S79=d2v.w7w;S79+=Z5w;S79+=m5w;var self;Editor[u7h][R5h]=$[v3h](I4h,{},Editor[S79][q6h],{"init":function(dte){var a5h="_init";self[j6h]=dte;self[a5h]();return self;},"open":function(dte,append,callback){var U5h="ldr";var P5h="hild";var S5h="appendC";var b79=Z8w;b79+=C3h;var h79=d2v.h2w;h79+=k3h;h79+=q6w;h79+=q5w;var t79=S5h;t79+=P5h;var U79=Z8w;U79+=L2h;U79+=d2v.w7w;var e79=e5h;e79+=X7w;e79+=U5h;e79+=u0w;var P79=Z8w;P79+=d2v.E7w;P79+=E6w;P79+=d2v.w7w;self[j6h]=dte;$(self[P79][t5h])[e79]()[X7h]();self[U79][t5h][t79](append);self[H6h][h79][h5h](self[b79][q5h]);self[b5h](callback);},"close":function(dte,callback){var w89=J5h;w89+=X7w;w89+=d2v.E7w;w89+=A7w;var J79=Z8w;J79+=d2v.E7w;J79+=d2v.b2w;J79+=A7w;self[J79]=dte;self[w89](callback);},node:function(dte){var E89=F5h;E89+=C4h;return self[E89][e1h][l2w];},"_init":function(){var I9h="visbility";var o9h='opacity';var M9h='div.DTED_Envelope_Container';var D9h="_do";var O9h="grou";var u9h="back";var y9h="styl";var d9h="sbility";var w9h="vi";var A89=w9h;A89+=D7w;A89+=X7w;A89+=b7w;var T89=D7w;T89+=a6w;T89+=n6w;var W89=d2v.E7w;W89+=X7w;W89+=r7h;W89+=o2h;var I89=E9h;I89+=U8h;I89+=V7w;I89+=c7w;var o89=d2v.h2w;o89+=D7w;o89+=D7w;var X89=F5h;X89+=C4h;var N89=s9h;N89+=c7w;var l89=Z8w;l89+=d2v.E7w;l89+=E6w;l89+=d2v.w7w;var M89=z9h;M89+=Z9h;M89+=u0w;var D89=w9h;D89+=d9h;var O89=y9h;O89+=A7w;var u89=u9h;u89+=O9h;u89+=g1h;var y89=Z8w;y89+=L2h;y89+=d2v.w7w;var d89=Y6w;d89+=I7w;d89+=d2v.J2w;d89+=l0h;var Z89=Z8w;Z89+=L2h;Z89+=d2v.w7w;var z89=D9h;z89+=d2v.w7w;var s89=n6h;s89+=d2v.E7w;s89+=K5w;if(self[h6h]){return;}self[H6h][t5h]=$(M9h,self[H6h][e1h])[l2w];document[s89][h5h](self[z89][H0h]);document[l9h][h5h](self[Z89][d89]);self[y89][u89][O89][D89]=M89;self[l89][N89][N9h][u7h]=k7h;self[X9h]=$(self[X89][H0h])[o89](o9h);self[H6h][I89][N9h][W89]=K2h;self[H6h][H0h][T89][I9h]=A89;},"_show":function(callback){var z4b='click.DTED_Envelope';var w4b="anima";var J9h='html,body';var b9h="nim";var t9h="adding";var U9h="windowP";var e9h="windowScroll";var S9h='normal';var c9h="px";var m9h="opacity";var j9h='auto';var Y9h="chRow";var L9h="_findAtta";var r9h="eft";var B9h="ginL";var v9h="sty";var q9h="tyle";var n9h="animat";var p9h="wrappe";var g9h="_Envelope";var x9h="ck.DTED";var F9h="D_Envelope";var f9h="click.DTE";var G9h="htbox_Content_Wrap";var K9h="div.DTED_Lig";var A9h="ED_Envelope";var T9h="ze.DT";var l69=W9h;l69+=T9h;l69+=A9h;var M69=a7w;M69+=X7w;M69+=l7w;M69+=d2v.E7w;var u69=a7w;u69+=X7w;u69+=l7w;u69+=d2v.E7w;var y69=Z8w;y69+=d2v.E7w;y69+=E6w;y69+=d2v.w7w;var d69=K9h;d69+=G9h;d69+=e6h;var s69=f9h;s69+=F9h;var E69=v5w;E69+=X7w;E69+=x9h;E69+=g9h;var P89=p9h;P89+=I7w;var S89=Z8w;S89+=d2v.E7w;S89+=E6w;S89+=d2v.w7w;var a89=n9h;a89+=A7w;var R89=k5w;R89+=r7h;R89+=o2h;var c89=D7w;c89+=q9h;var V89=s9h;V89+=c7w;var m89=Z8w;m89+=d2v.E7w;m89+=E6w;m89+=d2v.w7w;var j89=v9h;j89+=n6w;var Q89=d2v.h2w;Q89+=C9h;var Y89=Z8w;Y89+=d2v.E7w;Y89+=C4h;var L89=X0h;L89+=J8h;var k89=o7w;k89+=i7w;var H89=i9h;H89+=I7w;H89+=B9h;H89+=r9h;var r89=H9h;r89+=e6h;var B89=F5h;B89+=E6w;B89+=d2v.w7w;var i89=i7w;i89+=f7w;var C89=Y6w;C89+=k9h;var v89=D7w;v89+=d2v.b2w;v89+=K5w;v89+=n6w;var q89=Y6w;q89+=U6h;var n89=l7w;n89+=k3h;n89+=A7w;var p89=d2v.E7w;p89+=F7h;p89+=T0w;var g89=L9h;g89+=Y9h;var x89=a7w;x89+=X6w;x89+=E6w;x89+=U8h;var F89=P6h;F89+=l0h;var f89=Q9h;f89+=X7w;f89+=p6h;var G89=D7w;G89+=d2v.b2w;G89+=K5w;G89+=n6w;var K89=Z8w;K89+=d2v.E7w;K89+=E6w;K89+=d2v.w7w;var that=this;var formHeight;if(!callback){callback=function(){};}self[K89][t5h][G89][f89]=j9h;var style=self[H6h][F89][N9h];style[m9h]=l2w;style[u7h]=x89;var targetRow=self[g89]();var height=self[g0h]();var width=targetRow[V9h];style[p89]=n89;style[m9h]=N2w;self[H6h][q89][v89][C89]=width+i89;self[B89][r89][N9h][H89]=-(width/X2w)+c9h;self[H6h][e1h][N9h][k89]=$(targetRow)[L89]()[R9h]+targetRow[a9h]+c9h;self[Y89][Q89][N9h][R9h]=-N2w*height-g2w+c9h;self[H6h][H0h][j89][m9h]=l2w;self[m89][V89][c89][R89]=k7h;$(self[H6h][H0h])[a89]({'opacity':self[X9h]},S9h);$(self[S89][P89])[P9h]();if(self[F0h][e9h]){var b89=U9h;b89+=t9h;var h89=d2v.h2w;h89+=E6w;h89+=l7w;h89+=Y0w;var t89=d2v.b2w;t89+=E6w;t89+=i7w;var U89=h9h;U89+=M5w;var e89=d2v.J2w;e89+=b9h;e89+=K0h;e89+=A7w;$(J9h)[e89]({"scrollTop":$(targetRow)[U89]()[t89]+targetRow[a9h]-self[h89][b89]},function(){var J89=v0w;J89+=d2v.w7w;J89+=K0h;J89+=A7w;$(self[H6h][t5h])[J89]({"top":l2w},S2w,callback);});}else{var w69=w4b;w69+=d2v.b2w;w69+=A7w;$(self[H6h][t5h])[w69]({"top":l2w},S2w,callback);}$(self[H6h][q5h])[i0h](E69,function(e){self[j6h][q5h]();});$(self[H6h][H0h])[i0h](s69,function(e){var s4b="round";var E4b="ckg";var Z69=E9h;Z69+=E4b;Z69+=s4b;var z69=Z8w;z69+=d2v.E7w;z69+=d2v.b2w;z69+=A7w;self[z69][Z69]();});$(d69,self[y69][e1h])[u69](z4b,function(e){var u4b='DTED_Envelope_Content_Wrapper';var D69=Z4b;D69+=r6w;D69+=d4b;D69+=n3h;var O69=y4b;O69+=m0h;if($(e[O69])[D69](u4b)){self[j6h][H0h]();}});$(window)[M69](l69,function(){self[g0h]();});},"_heightCalc":function(){var F4b='div.DTE_Footer';var f4b='div.DTE_Header';var K4b="heightCalc";var A4b="ightCalc";var T4b="ntent";var W4b="ren";var X4b="ndowPadding";var l4b="rHei";var M4b="v.DTE_Body_Con";var O4b="Heig";var i69=H9h;i69+=i7w;i69+=J0w;var C69=d2v.E7w;C69+=E6w;C69+=d2v.w7w;var v69=Z8w;v69+=d2v.E7w;v69+=d2v.b2w;v69+=A7w;var q69=i9h;q69+=f7w;q69+=O4b;q69+=M7h;var n69=d2v.h2w;n69+=D7w;n69+=D7w;var p69=Y6w;p69+=D4b;p69+=l0h;var g69=Z8w;g69+=d2v.E7w;g69+=E6w;g69+=d2v.w7w;var x69=k5w;x69+=M4b;x69+=t6h;var F69=U0h;F69+=A7w;F69+=l4b;F69+=p6h;var f69=H9h;f69+=i7w;f69+=A7w;f69+=I7w;var G69=N4b;G69+=X4b;var K69=d2v.h2w;K69+=k3h;K69+=Y0w;var A69=Q9h;A69+=X7w;A69+=o4b;A69+=d2v.b2w;var T69=I4b;T69+=d2v.E7w;T69+=W4b;var W69=I2h;W69+=T4b;var I69=Z8w;I69+=d2v.E7w;I69+=E6w;I69+=d2v.w7w;var o69=Z8w;o69+=C3h;var X69=Q9h;X69+=A4b;var N69=d2v.h2w;N69+=E6w;N69+=X6h;var formHeight;formHeight=self[N69][X69]?self[F0h][K4b](self[o69][e1h]):$(self[I69][W69])[T69]()[G4b]();var maxHeight=$(window)[A69]()-self[K69][G69]*X2w-$(f4b,self[H6h][f69])[Z5h]()-$(F4b,self[H6h][e1h])[F69]();$(x69,self[g69][p69])[n69](q69,maxHeight);return $(self[v69][C69][i69])[Z5h]();},"_hide":function(callback){var B4b="unbi";var i4b="_Lightbo";var C4b="click.DTED";var v4b="kgro";var q4b="_Wrapper";var n4b="v.DTED_Lightbox_Content";var p4b="nbi";var g4b="tbox";var x4b="ck.DTED_Ligh";var a69=y0h;a69+=x4b;a69+=g4b;var R69=J8w;R69+=p4b;R69+=l7w;R69+=d2v.E7w;var c69=m6h;c69+=d2v.J2w;c69+=b0h;c69+=I7w;var V69=Z8w;V69+=C3h;var m69=k5w;m69+=n4b;m69+=q4b;var j69=y0h;j69+=x4b;j69+=g4b;var Q69=E9h;Q69+=d2v.h2w;Q69+=v4b;Q69+=c7w;var Y69=C4b;Y69+=i4b;Y69+=f7w;var L69=B4b;L69+=g1h;var r69=I2h;r69+=l7w;r69+=q6w;r69+=q5w;var B69=Z8w;B69+=L2h;B69+=d2v.w7w;if(!callback){callback=function(){};}$(self[B69][r69])[x8h]({"top":-(self[H6h][t5h][a9h]+L2w)},S2w,function(){var r4b="rma";var k69=p3h;k69+=r4b;k69+=X6w;var H69=H4b;H69+=J0w;$([self[H6h][H69],self[H6h][H0h]])[k4b](k69,callback);});$(self[H6h][q5h])[L69](Y69);$(self[H6h][Q69])[v5h](j69);$(m69,self[V69][c69])[R69](a69);$(window)[v5h](V0h);},"_findAttachRow":function(){var j4b="ader";var Q4b='head';var Y4b="DataTa";var L4b="atta";var t69=Z8w;t69+=d2v.E7w;t69+=d2v.b2w;t69+=A7w;var e69=L4b;e69+=d2v.h2w;e69+=k0w;var P69=Y4b;P69+=Q5w;P69+=A7w;var S69=Z8w;S69+=d2v.E7w;S69+=d2v.b2w;S69+=A7w;var dt=$(self[S69][D7w][f8h])[P69]();if(self[F0h][e69]===Q4b){var U69=k0w;U69+=A7w;U69+=j4b;return dt[f8h]()[U69]();}else if(self[t69][D7w][m4b]===f3h){return dt[f8h]()[V4b]();}else{var b69=l7w;b69+=E6w;b69+=d2v.E7w;b69+=A7w;var h69=c4b;h69+=X7w;h69+=Y0w;h69+=R4b;return dt[a4b](self[j6h][D7w][h69])[b69]();}},"_dte":F3h,"_ready":o4h,"_cssBackgroundOpacity":N2w,"_dom":{"wrapper":$(J69+w09+S4b+E09)[l2w],"background":$(s09)[l2w],"close":$(P4b)[l2w],"content":F3h}});self=Editor[u7h][R5h];self[z09]={"windowPadding":L2w,"heightCalc":F3h,"attach":a4b,"windowScroll":I4h};}(window,document,jQuery,jQuery[Z09][e4b]));Editor[w2h][d09]=function(cfg,after){var T1b="rd";var D1b="ng field '";var O1b=" addi";var y1b="ady exists with this name";var d1b="'. A field alre";var z1b="field requires a `name` option";var s1b="Error adding field. The ";var w1b="dataSourc";var U4b="isplayReorder";var f09=F5h;f09+=U4b;if($[t4b](cfg)){var y09=X6w;y09+=h4b;y09+=l4h;for(var i=l2w,iLen=cfg[y09];i<iLen;i++){var u09=d2v.J2w;u09+=d2v.E7w;u09+=d2v.E7w;this[u09](cfg[i]);}}else{var o09=b4b;o09+=v7w;var X09=J4b;X09+=X7w;X09+=d2v.b2w;X09+=v1h;var N09=Z8w;N09+=w1b;N09+=A7w;var O09=l7w;O09+=E1b;O09+=A7w;var name=cfg[O09];if(name===undefined){var D09=s1b;D09+=z1b;throw D09;}if(this[D7w][Z1b][name]){var l09=d1b;l09+=y1b;var M09=u1b;M09+=O1b;M09+=D1b;throw M09+name+l09;}this[N09](X09,cfg);var field=new Editor[v1h](cfg,this[p2h][j2h],this);if(this[D7w][o09]){var editFields=this[D7w][M1b];field[l1b]();$[i7h](editFields,function(idSrc,edit){var T09=d2v.E7w;T09+=A7w;T09+=Y0w;var I09=N1b;I09+=d2v.b2w;I09+=d2v.J2w;var val;if(edit[I09]){var W09=d2v.E7w;W09+=d2v.J2w;W09+=d2v.b2w;W09+=d2v.J2w;val=field[X1b](edit[W09]);}field[o1b](idSrc,val!==undefined?val:field[T09]());});}this[D7w][Z1b][name]=field;if(after===undefined){this[D7w][I1b][D4h](name);}else if(after===F3h){var A09=g4h;A09+=K0w;this[D7w][A09][E2h](name);}else{var G09=D7w;G09+=i7w;G09+=W1b;var K09=E6w;K09+=T1b;K09+=J0w;var idx=$[A1b](after,this[D7w][I1b]);this[D7w][K09][G09](idx+N2w,l2w,name);}}this[f09](this[I1b]());return this;};Editor[w2h][F09]=function(newAjax){if(newAjax){this[D7w][K1b]=newAjax;return this;}return this[D7w][K1b];};Editor[w2h][H0h]=function(){var g1b="editO";var x1b="onBac";var q09=G1b;q09+=f1b;q09+=E0w;var n09=F1b;n09+=D7w;n09+=A7w;var g09=x1b;g09+=S6h;var x09=g1b;x09+=p1b;x09+=D7w;var onBackground=this[D7w][x09][g09];if(typeof onBackground===d2v.d7w){onBackground(this);}else if(onBackground===n1b){var p09=a7w;p09+=X6w;p09+=j5w;this[p09]();}else if(onBackground===n09){this[q5h]();}else if(onBackground===q09){this[q1b]();}return this;};Editor[w2h][v1b]=function(){this[C1b]();return this;};Editor[w2h][i1b]=function(cells,fieldNames,show,opts){var j1b="lain";var Y1b="lea";var L1b="ormOptio";var H1b="ataSo";var k09=Z8w;k09+=A7w;k09+=d2v.E7w;k09+=E0w;var H09=B1b;H09+=J8w;H09+=r1b;var r09=F5h;r09+=H1b;r09+=k1b;var B09=Y0w;B09+=L1b;B09+=l7w;B09+=D7w;var i09=A7w;i09+=Y7w;i09+=l7w;i09+=d2v.E7w;var C09=n6h;C09+=E6w;C09+=Y1b;C09+=l7w;var v09=Q1b;v09+=j1b;v09+=m1b;var that=this;if(this[V1b](function(){that[i1b](cells,fieldNames,opts);})){return this;}if($[v09](fieldNames)){opts=fieldNames;fieldNames=undefined;show=I4h;}else if(typeof fieldNames===C09){show=fieldNames;fieldNames=undefined;opts=undefined;}if($[c1b](show)){opts=show;show=I4h;}if(show===undefined){show=I4h;}opts=$[i09]({},this[D7w][B09][i1b],opts);var editFields=this[r09](H09,cells,fieldNames);this[k09](cells,editFields,R1b,opts,function(){var n3b="_postopen";var x3b="click";var T3b="rmI";var I3b="To";var X3b="pointer";var l3b="bubbleNodes";var u3b="size";var y3b="ncat";var z3b="<div/></div>";var E3b="div ";var w3b="or\"><span></div>";var J1b="<div class=\"DTE_Processing_Indicat";var t1b="dren";var U1b="childr";var e1b="cus";var a1b="bubb";var o59=a1b;o59+=n6w;var X59=Y0w;X59+=S1b;X59+=P1b;var N59=Z8w;N59+=Y0w;N59+=E6w;N59+=e1b;var D59=d2v.J2w;D59+=d2v.E7w;D59+=d2v.E7w;var s59=Y0w;s59+=g4h;s59+=d2v.w7w;var E59=U1b;E59+=A7w;E59+=l7w;var w59=d2v.h2w;w59+=z9h;w59+=X6w;w59+=t1b;var J09=A7w;J09+=h1b;var t09=O1h;t09+=b1b;t09+=L4h;var U09=J1b;U09+=w3b;var e09=d2v.h2w;e09+=X6w;e09+=B6h;var P09=X6w;P09+=J4b;P09+=A7w;P09+=I7w;var S09=U9w;S09+=L4h;var a09=P6h;a09+=l0h;var R09=c4h;R09+=E3b;R09+=v5w;R09+=m4h;var c09=s3b;c09+=z3b;var V09=a7w;V09+=c8w;var m09=Z3b;m09+=d2v.J2w;m09+=d2v.h2w;m09+=k0w;var j09=d2v.J2w;j09+=d3b;j09+=Q3h;var Q09=I2h;Q09+=y3b;var Y09=I7w;Y09+=A7w;Y09+=u3b;Y09+=E9w;var L09=E6w;L09+=l7w;var namespace=that[O3b](opts);var ret=that[D3b](R1b);if(!ret){return that;}$(window)[L09](Y09+namespace,function(){that[M3b]();});var nodes=[];that[D7w][l3b]=nodes[Q09][j09](nodes,_pluck(editFields,m09));var classes=that[p2h][i1b];var background=$(P1h+classes[V09]+c09);var container=$(R09+classes[a09]+S09+P1h+classes[P09]+w3h+P1h+classes[f8h]+w3h+P1h+classes[e09]+N3b+U09+W3h+W3h+P1h+classes[X3b]+t09+W3h);if(show){var b09=D7h;b09+=o3b;b09+=I3b;var h09=z6h;h09+=A7w;h09+=g1h;h09+=I3b;container[h09](x0h);background[b09](x0h);}var liner=container[k6h]()[J09](l2w);var table=liner[w59]();var close=table[E59]();liner[L6h](that[C3h][W3b]);table[g3h](that[C3h][s59]);if(opts[V4h]){var d59=s1h;d59+=T3b;d59+=G8w;var Z59=L2h;Z59+=d2v.w7w;var z59=t6w;z59+=A7w;z59+=H7w;z59+=g1h;liner[z59](that[Z59][d59]);}if(opts[D3h]){var y59=i7w;y59+=U4h;y59+=o3b;liner[y59](that[C3h][V4b]);}if(opts[A3b]){var O59=L2h;O59+=d2v.w7w;var u59=K3b;u59+=d2v.E7w;table[u59](that[O59][A3b]);}var pair=$()[r2h](container)[D59](background);that[G3b](function(submitComplete){that[p0h](pair,{opacity:l2w},function(){var F3b="z";var f3b="_clearDynamicInf";var l59=f3b;l59+=E6w;var M59=W9h;M59+=F3b;M59+=A7w;M59+=E9w;pair[X7h]();$(window)[X0h](M59+namespace);that[l59]();});});background[x3b](function(){that[v1b]();});close[x3b](function(){that[g3b]();});that[M3b]();that[p0h](pair,{opacity:N2w});that[N59](that[D7w][p3b],opts[X59]);that[n3b](o59);});return this;};Editor[w2h][M3b]=function(){var U3b="bel";var c3b="bottom";var Q3b="ubbl";var Y3b=".DTE_B";var L3b="le_Liner";var k3b="ubb";var H3b="TE_B";var r3b="div.D";var B3b="ubbleNod";var C3b="W";var v3b="oute";var q3b="dt";var H59=d2v.h2w;H59+=n3h;var r59=N4b;r59+=q3b;r59+=k0w;var B59=v3b;B59+=I7w;B59+=C3b;B59+=k9h;var i59=N6w;i59+=o4b;i59+=d2v.b2w;var C59=X6w;C59+=h4b;C59+=d2v.b2w;C59+=k0w;var v59=a7w;v59+=W5w;v59+=o7w;v59+=d2v.w7w;var q59=I7w;q59+=X7w;q59+=o4b;q59+=d2v.b2w;var n59=i3b;n59+=k0w;var A59=A7w;A59+=d2v.J2w;A59+=d2v.h2w;A59+=k0w;var T59=a7w;T59+=B3b;T59+=E4h;var W59=r3b;W59+=H3b;W59+=k3b;W59+=L3b;var I59=n0h;I59+=Y3b;I59+=Q3b;I59+=A7w;var wrapper=$(I59),liner=$(W59),nodes=this[D7w][T59];var position={top:l2w,left:l2w,right:l2w,bottom:l2w};$[A59](nodes,function(i,node){var V3b="right";var j3b="offsetHe";var p59=j3b;p59+=X7w;p59+=p6h;var g59=n6w;g59+=Y0w;g59+=d2v.b2w;var x59=X6w;x59+=A7w;x59+=m3b;var F59=X6w;F59+=A7w;F59+=m3b;var f59=d2v.b2w;f59+=y8h;var G59=o7w;G59+=i7w;var K59=h9h;K59+=M5w;var pos=$(node)[K59]();node=$(node)[m0h](l2w);position[G59]+=pos[f59];position[F59]+=pos[x59];position[V3b]+=pos[g59]+node[V9h];position[c3b]+=pos[R9h]+node[p59];});position[R9h]/=nodes[X4h];position[R3b]/=nodes[n59];position[q59]/=nodes[X4h];position[v59]/=nodes[C59];var top=position[R9h],left=(position[R3b]+position[i59])/X2w,width=liner[B59](),visLeft=left-width/X2w,visRight=visLeft+width,docWidth=$(window)[r59](),padding=x2w,classes=this[p2h][i1b];wrapper[H59]({top:top,left:left});if(liner[X4h]&&liner[a3b]()[R9h]<l2w){var L59=a7w;L59+=T8w;L59+=S3b;var k59=d2v.h2w;k59+=D7w;k59+=D7w;wrapper[k59](P3b,position[c3b])[e3b](L59);}else{var Y59=U3b;Y59+=S3b;wrapper[Q2h](Y59);}if(visRight+padding>docWidth){var Q59=d2v.h2w;Q59+=D7w;Q59+=D7w;var diff=visRight-docWidth;liner[Q59](t3b,visLeft<padding?-(visLeft-padding):-(diff+padding));}else{liner[A2h](t3b,visLeft<padding?-(visLeft-padding):l2w);}return this;};Editor[j59][m59]=function(buttons){var J3b="ubm";var b3b="sic";var P59=h3b;P59+=i7w;P59+=a6w;var V59=Z8w;V59+=a7w;V59+=d2v.J2w;V59+=b3b;var that=this;if(buttons===V59){var a59=D7w;a59+=J3b;a59+=X7w;a59+=d2v.b2w;var R59=d2v.J2w;R59+=K8h;R59+=G8h;var c59=X7w;c59+=w2b;buttons=[{text:this[c59][this[D7w][R59]][a59],action:function(){var S59=X8h;S59+=E0w;this[S59]();}}];}else if(!$[t4b](buttons)){buttons=[buttons];}$(this[C3h][A3b])[P59]();$[i7h](buttons,function(i,btn){var N2b='keypress';var D2b='tabindex';var u2b="className";var y2b="n/>";var d2b="<butto";var s2b="tabIn";var E2b="tabInd";var Z99=C5w;Z99+=l7w;Z99+=D7w;var E99=E6w;E99+=l7w;var w99=E2b;w99+=x1h;var J59=s2b;J59+=v7w;J59+=f7w;var b59=a7w;b59+=z2b;b59+=k3h;var h59=Z2b;h59+=d2v.w7w;var t59=d2b;t59+=y2b;var U59=Y0w;U59+=l7w;var e59=d2v.b2w;e59+=Q1h;if(typeof btn===R7h){btn={text:btn,action:function(){this[q1b]();}};}var text=btn[e59]||btn[y1h];var action=btn[m4b]||btn[U59];$(t59,{'class':that[p2h][h59][b59]+(btn[u2b]?U1h+btn[u2b]:Y9w)})[o7h](typeof text===d2v.d7w?text(that):text||Y9w)[O2b](D2b,btn[J59]!==undefined?btn[w99]:l2w)[E99](M2b,function(e){var l2b="keyC";var s99=l2b;s99+=Z5w;if(e[s99]===f2w&&action){action[E6h](that);}})[k3h](N2b,function(e){var o2b="Code";var z99=X2b;z99+=o2b;if(e[z99]===f2w){e[I2b]();}})[k3h](L3h,function(e){e[I2b]();if(action){action[E6h](that);}})[p5h](that[C3h][Z99]);});return this;};Editor[d99][W2b]=function(fieldName){var g2b="cludeFields";var x2b="pli";var f2b="Arra";var K2b="ludeFields";var A2b="inc";var y99=d7h;y99+=T2b;var that=this;var fields=this[D7w][Z1b];if(typeof fieldName===y99){var N99=A2b;N99+=K2b;var l99=J4b;l99+=G2b;var M99=D7w;M99+=i7w;M99+=W4h;M99+=J3h;var D99=g4h;D99+=v7w;D99+=I7w;var O99=X7w;O99+=l7w;O99+=f2b;O99+=K5w;var u99=Y0w;u99+=e5w;that[u99](fieldName)[F2b]();delete fields[fieldName];var orderIdx=$[O99](fieldName,this[D7w][D99]);this[D7w][I1b][M99](orderIdx,N2w);var includeIdx=$[l99](fieldName,this[D7w][N99]);if(includeIdx!==-N2w){var o99=D7w;o99+=x2b;o99+=J3h;var X99=J4b;X99+=g2b;this[D7w][X99][o99](includeIdx,N2w);}}else{var I99=A7w;I99+=d2v.J2w;I99+=d2v.h2w;I99+=k0w;$[I99](this[p2b](fieldName),function(i,name){var W99=d2v.h2w;W99+=X6w;W99+=A7w;W99+=n2b;that[W99](name);});}return this;};Editor[w2h][T99]=function(){this[g3b](o4h);return this;};Editor[A99][q2b]=function(arg1,arg2,arg3,arg4){var R2b='initCreate';var r2b="editField";var i2b="_displayRe";var r99=Z8w;r99+=v2b;var C99=f5w;C99+=C2b;C99+=D7w;var v99=i2b;v99+=I1b;var q99=Q5w;q99+=E6w;q99+=d2v.h2w;q99+=T4h;var n99=d7h;n99+=K5w;n99+=X6w;n99+=A7w;var p99=d2v.E7w;p99+=E6w;p99+=d2v.w7w;var g99=B2b;g99+=X7w;g99+=E6w;g99+=l7w;var x99=d2v.w7w;x99+=d2v.J2w;x99+=X7w;x99+=l7w;var F99=b4b;F99+=v7w;var G99=r2b;G99+=D7w;var K99=F5w;K99+=t5w;K99+=D7w;var that=this;var fields=this[D7w][K99];var count=N2w;if(this[V1b](function(){that[q2b](arg1,arg2,arg3,arg4);})){return this;}if(typeof arg1===H2b){count=arg1;arg1=arg2;arg2=arg3;}this[D7w][G99]={};for(var i=l2w;i<count;i++){var f99=f5w;f99+=T8w;f99+=k2b;this[D7w][M1b][i]={fields:this[D7w][f99]};}var argOpts=this[L2b](arg1,arg2,arg3,arg4);this[D7w][F99]=x99;this[D7w][g99]=q2b;this[D7w][Y2b]=F3h;this[p99][Q2b][n99][u7h]=q99;this[j2b]();this[v99](this[C99]());$[i7h](fields,function(name,field){var V2b="ese";var m2b="tiR";var i99=d5w;i99+=m2b;i99+=V2b;i99+=d2v.b2w;field[i99]();for(var i=l2w;i<count;i++){var B99=C8h;B99+=X7w;B99+=c2b;B99+=d2v.b2w;field[B99](i,field[s2h]());}field[J8h](field[s2h]());});this[r99](R2b,F3h,function(){var P2b="ain";var S2b="sembleM";var a2b="_as";var H99=a2b;H99+=S2b;H99+=P2b;that[H99]();that[O3b](argOpts[c3h]);argOpts[e2b]();});return this;};Editor[w2h][k99]=function(parent,url,opts){var b2b="ndent";var h2b="dep";var U2b="vent";var h99=A7w;h99+=U2b;var t99=E6w;t99+=l7w;var U99=l7w;U99+=Z5w;var Q99=t2b;Q99+=d2v.E7w;if($[t4b](parent)){var L99=X6w;L99+=u0w;L99+=c8w;L99+=l4h;for(var i=l2w,ien=parent[L99];i<ien;i++){var Y99=h2b;Y99+=A7w;Y99+=b2b;this[Y99](parent[i],url,opts);}return this;}var that=this;var field=this[j2h](parent);var ajaxOpts={type:J2b,dataType:w7b};opts=$[Q99]({event:E7b,data:F3h,preUpdate:F3h,postUpdate:F3h},opts);var update=function(json){var I7b="postUpd";var o7b='hide';var X7b='error';var N7b='val';var l7b="preUpdate";var M7b="preUpd";var D7b="updat";var d7b="sabl";var z7b="stU";var P99=s7b;P99+=z7b;P99+=i7w;P99+=Z7b;var S99=k5w;S99+=d7b;S99+=A7w;var a99=y7b;a99+=A7w;var R99=D7w;R99+=k0w;R99+=E6w;R99+=Y6w;var V99=u7b;V99+=O7b;var m99=D7b;m99+=A7w;var j99=M7b;j99+=d2v.J2w;j99+=q6w;if(opts[j99]){opts[l7b](json);}$[i7h]({labels:i3h,options:m99,values:N7b,messages:V99,errors:X7b},function(jsonProp,fieldFn){if(json[jsonProp]){var c99=A7w;c99+=d2v.J2w;c99+=d2v.h2w;c99+=k0w;$[c99](json[jsonProp],function(field,val){that[j2h](field)[fieldFn](val);});}});$[i7h]([o7b,R99,a99,S99],function(i,key){if(json[key]){that[key](json[key]);}});if(opts[P99]){var e99=I7b;e99+=d2v.J2w;e99+=d2v.b2w;e99+=A7w;opts[e99](json);}field[K3h](o4h);};$(field[U99]())[t99](opts[h99],function(e){var v7b="bje";var x7b="values";var K7b="nod";var A7b="engt";var z4g=z9w;z4g+=d2v.J2w;z4g+=X6w;var s4g=I7w;s4g+=S3b;var E4g=d2v.E7w;E4g+=W7b;var w4g=t6w;w4g+=E6w;w4g+=d2v.h2w;w4g+=T7b;var J99=X6w;J99+=A7b;J99+=k0w;var b99=K7b;b99+=A7w;if($(field[b99]())[G7b](e[f7b])[J99]===l2w){return;}field[w4g](I4h);var data={};data[F7b]=that[D7w][M1b]?_pluck(that[D7w][M1b],E4g):F3h;data[s4g]=data[F7b]?data[F7b][l2w]:F3h;data[x7b]=that[z4g]();if(opts[r5w]){var Z4g=d2v.E7w;Z4g+=d2v.J2w;Z4g+=j0h;var ret=opts[Z4g](data);if(ret){var d4g=d2v.E7w;d4g+=W7b;opts[d4g]=ret;}}if(typeof url===d2v.d7w){var o=url(field[a3h](),data,update);if(o){if(typeof o===N4h&&typeof o[g7b]===d2v.d7w){o[g7b](function(resolved){if(resolved){update(resolved);}});}else{update(o);}}}else{var O4g=p7b;O4g+=l7w;O4g+=d2v.E7w;var y4g=n7b;y4g+=q7b;y4g+=v7b;y4g+=K8h;if($[y4g](url)){$[v3h](ajaxOpts,url);}else{var u4g=J8w;u4g+=I7w;u4g+=X6w;ajaxOpts[u4g]=url;}$[K1b]($[O4g](ajaxOpts,{url:url,data:data,success:update}));}});return this;};Editor[w2h][D4g]=function(){var Y7b='.dte';var L7b="mplat";var H7b="temp";var r7b="ntroller";var B7b="yC";var C7b="iqu";var W4g=d2v.E7w;W4g+=C4h;var I4g=J8w;I4g+=l7w;I4g+=C7b;I4g+=A7w;var o4g=i7b;o4g+=Y0w;var X4g=g5w;X4g+=B7b;X4g+=E6w;X4g+=r7b;var l4g=H7b;l4g+=d4b;l4g+=q6w;var M4g=d2v.h2w;M4g+=X6w;M4g+=A7w;M4g+=n2b;if(this[D7w][k7b]){this[q5h]();}this[M4g]();if(this[D7w][l4g]){var N4g=q6w;N4g+=L7b;N4g+=A7w;$(x0h)[L6h](this[D7w][N4g]);}var controller=this[D7w][X4g];if(controller[F2b]){controller[F2b](this);}$(document)[o4g](Y7b+this[D7w][I4g]);this[W4g]=F3h;this[D7w]=F3h;};Editor[w2h][Q7b]=function(name){var m7b="ieldNames";var A4g=j7b;A4g+=m7b;var T4g=A7w;T4g+=d2v.J2w;T4g+=d2v.h2w;T4g+=k0w;var that=this;$[T4g](this[A4g](name),function(i,n){var V7b="sable";var K4g=k5w;K4g+=V7b;that[j2h](n)[K4g]();});return this;};Editor[w2h][G4g]=function(show){var R7b="ayed";var F4g=d2v.h2w;F4g+=X6w;F4g+=B6h;if(show===undefined){var f4g=k5w;f4g+=D7w;f4g+=c7b;f4g+=R7b;return this[D7w][f4g];}return this[show?a7b:F4g]();};Editor[x4g][k7b]=function(){var p4g=f5w;p4g+=S7b;var g4g=d2v.w7w;g4g+=d2v.J2w;g4g+=i7w;return $[g4g](this[D7w][p4g],function(field,name){return field[k7b]()?name:F3h;});};Editor[w2h][P7b]=function(){var n4g=p3h;n4g+=v7w;return this[D7w][q6h][n4g](this);};Editor[q4g][e7b]=function(items,arg1,arg2,arg3,arg4){var h7b="_edit";var U7b="_dataS";var B4g=d2v.w7w;B4g+=d2v.J2w;B4g+=X7w;B4g+=l7w;var i4g=j2h;i4g+=D7w;var C4g=U7b;C4g+=t7b;var that=this;if(this[V1b](function(){var v4g=A7w;v4g+=d2v.E7w;v4g+=X7w;v4g+=d2v.b2w;that[v4g](items,arg1,arg2,arg3,arg4);})){return this;}var argOpts=this[L2b](arg1,arg2,arg3,arg4);this[h7b](items,this[C4g](i4g,items),B4g,argOpts[c3h],function(){var w8b="_form";var J7b="Open";var b7b="maybe";var k4g=b7b;k4g+=J7b;var H4g=E6w;H4g+=i7w;H4g+=d2v.b2w;H4g+=D7w;var r4g=w8b;r4g+=E8b;r4g+=s8b;that[z8b]();that[r4g](argOpts[H4g]);argOpts[k4g]();});return this;};Editor[L4g][Z8b]=function(name){var Y4g=d8b;Y4g+=d2v.h2w;Y4g+=k0w;var that=this;$[Y4g](this[p2b](name),function(i,n){var y8b="nabl";var j4g=A7w;j4g+=y8b;j4g+=A7w;var Q4g=f5w;Q4g+=C2b;that[Q4g](n)[j4g]();});return this;};Editor[w2h][Y2h]=function(name,msg){var u8b="lobalE";if(msg===undefined){var m4g=c8w;m4g+=u8b;m4g+=O8b;this[D8b](this[C3h][W3b],name);this[D7w][m4g]=name;}else{var c4g=M8b;c4g+=E6w;c4g+=I7w;var V4g=f5w;V4g+=A7w;V4g+=t5w;this[V4g](name)[c4g](msg);}return this;};Editor[R4g][a4g]=function(name){var X8b="e - ";var N8b=" na";var l8b="Unknown field";var fields=this[D7w][Z1b];if(!fields[name]){var S4g=l8b;S4g+=N8b;S4g+=d2v.w7w;S4g+=X8b;throw S4g+name;}return fields[name];};Editor[w2h][Z1b]=function(){var e4g=F5w;e4g+=X6w;e4g+=d2v.E7w;e4g+=D7w;var P4g=d2v.w7w;P4g+=d2v.J2w;P4g+=i7w;return $[P4g](this[D7w][e4g],function(field,name){return name;});};Editor[U4g][t4g]=_api_file;Editor[w2h][y4h]=_api_files;Editor[w2h][m0h]=function(name){var o8b="sArra";var h4g=X7w;h4g+=o8b;h4g+=K5w;var that=this;if(!name){name=this[Z1b]();}if($[h4g](name)){var b4g=d8b;b4g+=e5h;var out={};$[b4g](name,function(i,n){var J4g=F5w;J4g+=X6w;J4g+=d2v.E7w;out[n]=that[J4g](n)[m0h]();});return out;}return this[j2h](name)[m0h]();};Editor[w1g][E1g]=function(names,animate){var W8b="eldN";var s1g=I8b;s1g+=W8b;s1g+=E1b;s1g+=E4h;var that=this;$[i7h](this[s1g](names),function(i,n){var T8b="ie";var z1g=Y0w;z1g+=T8b;z1g+=X6w;z1g+=d2v.E7w;that[z1g](n)[A8b](animate);});return this;};Editor[Z1g][d1g]=function(includeHash){var G8b="Fiel";var y1g=A7w;y1g+=K8b;y1g+=G8b;y1g+=k2b;return $[f8b](this[D7w][y1g],function(edit,idSrc){return includeHash===I4h?F8b+idSrc:idSrc;});};Editor[u1g][O1g]=function(inNames){var g8b="nError";var x8b="globalError";var D1g=d2v.E7w;D1g+=E6w;D1g+=d2v.w7w;var formError=$(this[D1g][W3b]);if(this[D7w][x8b]){return I4h;}var names=this[p2b](inNames);for(var i=l2w,ien=names[X4h];i<ien;i++){var l1g=X7w;l1g+=g8b;var M1g=f5w;M1g+=C2b;if(this[M1g](names[i])[l1g]()){return I4h;}}return o4h;};Editor[N1g][X1g]=function(cell,fieldName,opts){var H8b='individual';var r8b="nObject";var B8b="sPlai";var v8b="aSo";var n8b="lin";var p8b=".DTE_Fiel";var n1g=X7w;n1g+=l7w;n1g+=W4h;n1g+=z5w;var p1g=Z8w;p1g+=A7w;p1g+=k5w;p1g+=d2v.b2w;var g1g=n0h;g1g+=p8b;g1g+=d2v.E7w;var G1g=A7w;G1g+=d2v.J2w;G1g+=d2v.h2w;G1g+=k0w;var K1g=X7w;K1g+=l7w;K1g+=n8b;K1g+=A7w;var A1g=v5w;A1g+=d2v.J2w;A1g+=D7w;A1g+=q8b;var T1g=Z8w;T1g+=Y1h;T1g+=v8b;T1g+=k1b;var W1g=X7w;W1g+=l7w;W1g+=C8b;var I1g=A7w;I1g+=i8b;I1g+=d2v.E7w;var o1g=X7w;o1g+=B8b;o1g+=r8b;var that=this;if($[o1g](fieldName)){opts=fieldName;fieldName=undefined;}opts=$[I1g]({},this[D7w][D6h][W1g],opts);var editFields=this[T1g](H8b,cell,fieldName);var node,field;var countOuter=l2w,countInner;var closed=o4h;var classes=this[A1g][K1g];$[G1g](editFields,function(i,editField){var j8b=" a time";var Q8b=" at";var Y8b=" more than one row inline";var L8b="Cannot edit";var k8b="tach";var x1g=A7w;x1g+=V7h;x1g+=k0w;var F1g=K0h;F1g+=k8b;if(countOuter>l2w){var f1g=L8b;f1g+=Y8b;f1g+=Q8b;f1g+=j8b;throw f1g;}node=$(editField[F1g][l2w]);countInner=l2w;$[x1g](editField[m8b],function(j,f){var V8b='Cannot edit more than one field inline at a time';if(countInner>l2w){throw V8b;}field=f;countInner++;});countOuter++;});if($(g1g,node)[X4h]){return this;}if(this[V1b](function(){var c8b="inline";that[c8b](cell,fieldName,opts);})){return this;}this[p1g](cell,editFields,n1g,opts,function(){var l6b="utto";var O6b='px">';var u6b='" style="width:';var y6b="liner";var Z6b="tents";var z6b="etac";var s6b="ss=";var E6b="v cla";var J8b="widt";var b8b="/></div>";var h8b="<div class=\"DTE_Processing_Indicator\"><span";var P8b="rmEr";var a8b="_po";var R8b="nli";var d3g=X7w;d3g+=R8b;d3g+=z5w;var Z3g=a8b;Z3g+=d7h;Z3g+=S8b;var U1g=x0w;U1g+=c8w;var R1g=s1h;R1g+=P8b;R1g+=x8w;var c1g=d2v.E7w;c1g+=E6w;c1g+=d2v.w7w;var V1g=z6h;V1g+=e8b;var m1g=W4h;m1g+=l7w;m1g+=J0w;var j1g=d2v.E7w;j1g+=Y5h;j1g+=E9w;var Q1g=f5w;Q1g+=l7w;Q1g+=d2v.E7w;var Y1g=c4h;Y1g+=b1b;Y1g+=k5w;Y1g+=B4h;var L1g=U9w;L1g+=b1b;L1g+=L4h;var k1g=U8b;k1g+=t8b;k1g+=D7w;var H1g=i4h;H1g+=k5w;H1g+=z9w;H1g+=L4h;var r1g=h8b;r1g+=b8b;var B1g=J8b;B1g+=k0w;var i1g=m6h;i1g+=D7h;i1g+=H7w;i1g+=I7w;var C1g=w6b;C1g+=E6b;C1g+=s6b;C1g+=U9w;var v1g=d2v.E7w;v1g+=z6b;v1g+=k0w;var q1g=M2h;q1g+=Z6b;var namespace=that[O3b](opts);var ret=that[D3b](d6b);if(!ret){return that;}var children=node[q1g]()[v1g]();node[L6h]($(C1g+classes[i1g]+w3h+P1h+classes[y6b]+u6b+node[B1g]()+O6b+r1g+H1g+P1h+classes[k1g]+L1g+Y1g));node[Q1g](j1g+classes[m1g][S7h](/ /g,D6b))[V1g](field[M6b]())[L6h](that[c1g][R1g]);if(opts[A3b]){var e1g=a7w;e1g+=l6b;e1g+=l7w;e1g+=D7w;var P1g=d2v.E7w;P1g+=C4h;var S1g=I7w;S1g+=A7w;S1g+=E5w;S1g+=J3h;var a1g=d2v.E7w;a1g+=X7w;a1g+=z9w;a1g+=E9w;node[G7b](a1g+classes[A3b][S1g](/ /g,D6b))[L6h](that[P1g][e1g]);}that[U1g](function(submitComplete){var X6b="contents";var N6b="_clearDynamicI";var h1g=N6b;h1g+=G8w;closed=I4h;$(document)[X0h](L3h+namespace);if(!submitComplete){var t1g=K3b;t1g+=d2v.E7w;node[X6b]()[X7h]();node[t1g](children);}that[h1g]();});setTimeout(function(){var b1g=E6w;b1g+=l7w;if(closed){return;}$(document)[b1g](L3h+namespace,function(e){var G6b="blu";var A6b='addBack';var T6b="dB";var s3g=o6b;s3g+=U4h;s3g+=I6b;var E3g=E6w;E3g+=Y6w;E3g+=l7w;E3g+=D7w;var w3g=W6b;w3g+=T6b;w3g+=V7h;w3g+=T4h;var J1g=Y0w;J1g+=l7w;var back=$[J1g][w3g]?A6b:K6b;if(!field[G3h](E3g,e[f7b])&&$[A1b](node[l2w],$(e[f7b])[s3g]()[back]())===-N2w){var z3g=G6b;z3g+=I7w;that[z3g]();}});},l2w);that[f6b]([field],opts[S3h]);that[Z3g](d3g);});return this;};Editor[w2h][y3g]=function(name,msg){var g6b="essa";var x6b="ssa";var F6b="_me";if(msg===undefined){var D3g=Q2b;D3g+=m2h;D3g+=Y0w;D3g+=E6w;var O3g=L2h;O3g+=d2v.w7w;var u3g=F6b;u3g+=x6b;u3g+=c8w;u3g+=A7w;this[u3g](this[O3g][D3g],name);}else{var l3g=d2v.w7w;l3g+=g6b;l3g+=c8w;l3g+=A7w;var M3g=Y0w;M3g+=e5w;this[M3g](name)[l3g](msg);}return this;};Editor[N3g][P5w]=function(mode){var p6b='Not currently in an editing mode';if(!mode){var X3g=B2b;X3g+=M7w;X3g+=l7w;return this[D7w][X3g];}if(!this[D7w][m4b]){throw p6b;}this[D7w][m4b]=mode;return this;};Editor[o3g][I3g]=function(){return this[D7w][Y2b];};Editor[w2h][W3g]=function(fieldNames){var n6b="lti";var f3g=O5w;f3g+=n6b;f3g+=q6b;f3g+=d2v.b2w;var A3g=F7h;A3g+=v6b;A3g+=o2h;var that=this;if(fieldNames===undefined){var T3g=f5w;T3g+=A7w;T3g+=C6b;fieldNames=this[T3g]();}if($[A3g](fieldNames)){var K3g=A7w;K3g+=d2v.J2w;K3g+=d2v.h2w;K3g+=k0w;var out={};$[K3g](fieldNames,function(i,name){var i6b="Get";var G3g=d2v.w7w;G3g+=A4h;G3g+=i6b;out[name]=that[j2h](name)[G3g]();});return out;}return this[j2h](fieldNames)[f3g]();};Editor[F3g][x3g]=function(fieldNames,val){var that=this;if($[c1b](fieldNames)&&val===undefined){$[i7h](fieldNames,function(name,value){var g3g=f5w;g3g+=C2b;that[g3g](name)[o1b](value);});}else{var p3g=d5w;p3g+=d2v.b2w;p3g+=y5w;p3g+=d2v.b2w;this[j2h](fieldNames)[p3g](val);}return this;};Editor[n3g][q3g]=function(name){var B3g=l7w;B3g+=E6w;B3g+=d2v.E7w;B3g+=A7w;var i3g=F5w;i3g+=t5w;var C3g=d2v.w7w;C3g+=d2v.J2w;C3g+=i7w;var that=this;if(!name){var v3g=g4h;v3g+=d2v.E7w;v3g+=A7w;v3g+=I7w;name=this[v3g]();}return $[t4b](name)?$[C3g](name,function(n){return that[j2h](n)[M6b]();}):this[i3g](name)[B3g]();};Editor[w2h][r3g]=function(name,fn){var r6b="tName";var k3g=B6b;k3g+=r6b;var H3g=E6w;H3g+=H6b;$(this)[H3g](this[k3g](name),fn);return this;};Editor[w2h][k3h]=function(name,fn){var k6b="entNam";var Y3g=I0w;Y3g+=z9w;Y3g+=k6b;Y3g+=A7w;var L3g=E6w;L3g+=l7w;$(this)[L3g](this[Y3g](name),fn);return this;};Editor[w2h][Q3g]=function(name,fn){var Y6b="ntName";var m3g=I0w;m3g+=L6b;m3g+=Y6b;var j3g=E6w;j3g+=l7w;j3g+=A7w;$(this)[j3g](this[m3g](name),fn);return this;};Editor[w2h][S8b]=function(){var m6b="oller";var j6b="splayContr";var t3g=d2v.w7w;t3g+=d2v.J2w;t3g+=X7w;t3g+=l7w;var U3g=d0w;U3g+=Q6b;U3g+=R9h;U3g+=u0w;var a3g=k5w;a3g+=j6b;a3g+=m6b;var R3g=V6b;R3g+=l7w;var that=this;this[c6b]();this[G3b](function(submitComplete){var V3g=v5w;V3g+=E6w;V3g+=D7w;V3g+=A7w;that[D7w][q6h][V3g](that,function(){var S6b="namicInf";var a6b="arDy";var R6b="_cle";var c3g=R6b;c3g+=a6b;c3g+=S6b;c3g+=E6w;that[c3g]();});});var ret=this[D3b](R3g);if(!ret){return this;}this[D7w][a3g][S8b](this,this[C3h][e1h],function(){var e3g=e8w;e3g+=P6b;e3g+=D7w;var S3g=d2v.w7w;S3g+=D7h;that[f6b]($[S3g](that[D7w][I1b],function(name){var P3g=F5w;P3g+=C6b;return that[D7w][P3g][name];}),that[D7w][e3g][S3h]);});this[U3g](t3g);return this;};Editor[w2h][I1b]=function(set){var E0b="All fields, and no additional fields, must be provided for ordering.";var b6b="sl";var U6b="Reorder";var e6b="isplay";var d2g=F5h;d2g+=e6b;d2g+=U6b;var Z2g=E6w;Z2g+=I7w;Z2g+=K0w;var z2g=x1h;z2g+=t6b;z2g+=d2v.E7w;var s2g=h6b;s2g+=E6w;s2g+=X7w;s2g+=l7w;var E2g=D7w;E2g+=E6w;E2g+=I7w;E2g+=d2v.b2w;var w2g=b6b;w2g+=z0h;w2g+=A7w;var J3g=D7w;J3g+=E6w;J3g+=I7w;J3g+=d2v.b2w;if(!set){return this[D7w][I1b];}if(arguments[X4h]&&!$[t4b](set)){var b3g=D7w;b3g+=W1b;var h3g=t6w;h3g+=E6w;h3g+=d2v.b2w;h3g+=Z0w;set=Array[h3g][b3g][E6h](arguments);}if(this[D7w][I1b][w6h]()[J3g]()[J6b](w0b)!==set[w2g]()[E2g]()[s2g](w0b)){throw E0b;}$[z2g](this[D7w][Z2g],set);this[d2g]();return this;};Editor[y2g][M8h]=function(items,arg1,arg2,arg3,arg4){var l0b='node';var M0b='fields';var O0b="dy";var u0b="_crudArg";var y0b="fier";var d0b="editF";var s0b="itRem";var T2g=J4b;T2g+=s0b;T2g+=z0b;T2g+=A7w;var W2g=Z8w;W2g+=Z0b;W2g+=q5w;var I2g=d2v.E7w;I2g+=F7h;I2g+=i7w;I2g+=I6h;var o2g=Y0w;o2g+=g4h;o2g+=d2v.w7w;var X2g=d0b;X2g+=G7w;var N2g=c4b;N2g+=X7w;N2g+=y0b;var l2g=I7w;l2g+=h3b;l2g+=E6w;l2g+=L6b;var M2g=V7h;M2g+=d2v.b2w;M2g+=X7w;M2g+=k3h;var D2g=u0b;D2g+=D7w;var O2g=n6w;O2g+=l7w;O2g+=c8w;O2g+=l4h;var u2g=d2h;u2g+=X7w;u2g+=O0b;var that=this;if(this[u2g](function(){that[M8h](items,arg1,arg2,arg3,arg4);})){return this;}if(items[O2g]===undefined){items=[items];}var argOpts=this[D2g](arg1,arg2,arg3,arg4);var editFields=this[D0b](M0b,items);this[D7w][M2g]=l2g;this[D7w][N2g]=items;this[D7w][X2g]=editFields;this[C3h][o2g][N9h][I2g]=K2h;this[j2b]();this[W2g](T2g,[_pluck(editFields,l0b),_pluck(editFields,N0b),items],function(){var X0b="MultiRemo";var K2g=v6h;K2g+=d2v.b2w;K2g+=X0b;K2g+=L6b;var A2g=Z8w;A2g+=A7w;A2g+=L6b;A2g+=q5w;that[A2g](K2g,[editFields,items],function(){var A0b="embleMain";var T0b="_ass";var W0b="tions";var I0b="Op";var F2g=E6w;F2g+=B7h;var f2g=j7b;f2g+=o0b;f2g+=I0b;f2g+=W0b;var G2g=T0b;G2g+=A0b;that[G2g]();that[f2g](argOpts[F2g]);argOpts[e2b]();var opts=that[D7w][K0b];if(opts[S3h]!==F3h){var g2g=U8b;g2g+=d2v.b2w;g2g+=d2v.b2w;g2g+=G0b;var x2g=a7w;x2g+=f0b;x2g+=d2v.b2w;x2g+=k3h;$(x2g,that[C3h][g2g])[F0b](opts[S3h])[S3h]();}});});return this;};Editor[w2h][J8h]=function(set,val){var x0b="ainObject";var n2g=A7w;n2g+=w4h;var p2g=Q1b;p2g+=X6w;p2g+=x0b;var that=this;if(!$[p2g](set)){var o={};o[set]=val;set=o;}$[n2g](set,function(n,v){var q2g=D7w;q2g+=A7w;q2g+=d2v.b2w;that[j2h](n)[q2g](v);});return this;};Editor[w2h][i6h]=function(names,animate){var p0b="dNames";var C2g=j7b;C2g+=g0b;C2g+=p0b;var v2g=A7w;v2g+=w4h;var that=this;$[v2g](this[C2g](names),function(i,n){var B2g=D7w;B2g+=m8h;B2g+=Y6w;var i2g=f5w;i2g+=C2b;that[i2g](n)[B2g](animate);});return this;};Editor[r2g][q1b]=function(successCallback,errorCallback,formatdata,hide){var L2g=n0b;L2g+=l7w;var k2g=q0b;k2g+=M4h;var H2g=Y0w;H2g+=X7w;H2g+=A7w;H2g+=C6b;var that=this,fields=this[D7w][H2g],errorFields=[],errorReady=l2w,sent=o4h;if(this[D7w][k2g]||!this[D7w][L2g]){return this;}this[v0b](I4h);var send=function(){var C0b='initSubmit';var Y2g=Z8w;Y2g+=v2b;if(errorFields[X4h]!==errorReady||sent){return;}that[Y2g](C0b,[that[D7w][m4b]],function(result){var Q2g=i0b;Q2g+=J8w;Q2g+=B0b;if(result===o4h){that[v0b](o4h);return;}sent=I4h;that[Q2g](successCallback,errorCallback,formatdata,hide);});};this[Y2h]();$[i7h](fields,function(name,field){var r0b="inError";if(field[r0b]()){var j2g=i7w;j2g+=J8w;j2g+=D7w;j2g+=k0w;errorFields[j2g](name);}});$[i7h](errorFields,function(i,name){fields[name][Y2h](Y9w,function(){errorReady++;send();});});send();return this;};Editor[m2g][V2g]=function(set){if(set===undefined){return this[D7w][H0b];}this[D7w][H0b]=set===F3h?F3h:$(set);return this;};Editor[c2g][D3h]=function(title){var k0b="classe";var P2g=d2v.h2w;P2g+=k3h;P2g+=q6w;P2g+=q5w;var S2g=k0b;S2g+=D7w;var a2g=d2v.E7w;a2g+=X7w;a2g+=z9w;a2g+=E9w;var R2g=Q9h;R2g+=d2v.J2w;R2g+=v7w;R2g+=I7w;var header=$(this[C3h][R2g])[k6h](a2g+this[S2g][V4b][P2g]);if(title===undefined){return header[o7h]();}if(typeof title===d2v.d7w){var e2g=y6w;e2g+=i7w;e2g+=X7w;title=title(this,new DataTable[e2g](this[D7w][f8h]));}header[o7h](title);return this;};Editor[w2h][a3h]=function(field,value){var U2g=X5w;U2g+=d2v.b2w;if(value!==undefined||$[c1b](field)){return this[J8h](field,value);}return this[U2g](field);};var apiRegister=DataTable[L0b][t2g];function __getInst(api){var m0b="oInit";var Q0b="onte";var b2g=Z8w;b2g+=Y0b;var h2g=d2v.h2w;h2g+=Q0b;h2g+=j0b;var ctx=api[h2g][l2w];return ctx[m0b][Y0b]||ctx[b2g];}function __setBasic(inst,opts,type,plural){var h0b="sag";var t0b='1';var U0b=/%d/;var a0b="tit";var c0b="si";var V0b="itl";var Z7g=x7w;Z7g+=D7w;Z7g+=D7w;Z7g+=O7b;var s7g=d2v.b2w;s7g+=V0b;s7g+=A7w;var J2g=U8b;J2g+=t8b;J2g+=D7w;if(!opts){opts={};}if(opts[J2g]===undefined){var E7g=Z8w;E7g+=E9h;E7g+=c0b;E7g+=d2v.h2w;var w7g=a7w;w7g+=J8w;w7g+=R0b;opts[w7g]=E7g;}if(opts[s7g]===undefined){var z7g=a0b;z7g+=n6w;opts[D3h]=inst[S0b][type][z7g];}if(opts[Z7g]===undefined){var d7g=U4h;d7g+=b4b;d7g+=z9w;d7g+=A7w;if(type===d7g){var O7g=U4h;O7g+=c7b;O7g+=d2v.J2w;O7g+=J3h;var u7g=d2v.w7w;u7g+=P0b;var y7g=X7w;y7g+=j8h;y7g+=q1h;y7g+=l7w;var confirm=inst[y7g][type][e0b];opts[u7g]=plural!==N2w?confirm[Z8w][O7g](U0b,plural):confirm[t0b];}else{var D7g=o0w;D7g+=h0b;D7g+=A7w;opts[D7g]=Y9w;}}return opts;}apiRegister(M7g,function(){return __getInst(this);});apiRegister(l7g,function(opts){var N7g=b0b;N7g+=d2v.J2w;N7g+=d2v.b2w;N7g+=A7w;var inst=__getInst(this);inst[q2b](__setBasic(inst,opts,N7g));return this;});apiRegister(X7g,function(opts){var inst=__getInst(this);inst[e7b](this[l2w][l2w],__setBasic(inst,opts,J0b));return this;});apiRegister(o7g,function(opts){var I7g=e8w;I7g+=X7w;I7g+=d2v.b2w;var inst=__getInst(this);inst[e7b](this[l2w],__setBasic(inst,opts,I7g));return this;});apiRegister(w5b,function(opts){var W7g=I7w;W7g+=A7w;W7g+=E5b;W7g+=A7w;var inst=__getInst(this);inst[M8h](this[l2w][l2w],__setBasic(inst,opts,W7g,N2w));return this;});apiRegister(T7g,function(opts){var A7g=n6w;A7g+=s5b;var inst=__getInst(this);inst[M8h](this[l2w],__setBasic(inst,opts,z5b,this[l2w][A7g]));return this;});apiRegister(K7g,function(type,opts){var Z5b="isPlainOb";var G7g=Z5b;G7g+=h6b;G7g+=d5b;if(!type){type=d6b;}else if($[G7g](type)){opts=type;type=d6b;}__getInst(this)[type](this[l2w][l2w],opts);return this;});apiRegister(y5b,function(opts){__getInst(this)[i1b](this[l2w],opts);return this;});apiRegister(f7g,_api_file);apiRegister(u5b,_api_files);$(document)[F7g](x7g,function(e,ctx,json){var M5b='dt';var D5b="espa";var O5b="nam";var p7g=Y0w;p7g+=X7w;p7g+=X6w;p7g+=E4h;var g7g=O5b;g7g+=D5b;g7g+=J3h;if(e[g7g]!==M5b){return;}if(json&&json[p7g]){var q7g=l5b;q7g+=E4h;var n7g=A7w;n7g+=d2v.J2w;n7g+=d2v.h2w;n7g+=k0w;$[n7g](json[q7g],function(name,files){Editor[y4h][name]=files;});}});Editor[Y2h]=function(msg,tn){var N5b=' For more information, please refer to https://datatables.net/tn/';throw tn?msg+N5b+tn:msg;};Editor[X5b]=function(data,props,fn){var A5b="value";var i,ien,dataPoint;props=$[v3h]({label:i3h,value:o5b},props);if($[t4b](data)){for(i=l2w,ien=data[X4h];i<ien;i++){dataPoint=data[i];if($[c1b](dataPoint)){var i7g=K0h;i7g+=I5b;var C7g=X6w;C7g+=W5b;C7g+=T8w;var v7g=T5b;v7g+=X6w;v7g+=J8w;v7g+=A7w;fn(dataPoint[props[v7g]]===undefined?dataPoint[props[y1h]]:dataPoint[props[A5b]],dataPoint[props[C7g]],i,dataPoint[i7g]);}else{fn(dataPoint,dataPoint,i);}}}else{var B7g=A7w;B7g+=d2v.J2w;B7g+=d2v.h2w;B7g+=k0w;i=l2w;$[B7g](data,function(key,val){fn(val,key,i);i++;});}};Editor[b1h]=function(id){return id[S7h](/\./g,w0b);};Editor[r7g]=function(editor,conf,files,progressCallback,completeCallback){var f9b="plice";var B5b="loading the file";var i5b="r occurred while up";var C5b="A server erro";var v5b="adText";var q5b="fileRe";var n5b="ile</i>";var p5b="ding f";var g5b=">Uploa";var x5b="<i";var F5b="loa";var f5b="mitLe";var G5b="_li";var K5b="adAsDataURL";var B8g=U4h;B8g+=K5b;var C8g=G5b;C8g+=f5b;C8g+=m3b;var L7g=k3h;L7g+=F5b;L7g+=d2v.E7w;var k7g=x5b;k7g+=g5b;k7g+=p5b;k7g+=n5b;var H7g=q5b;H7g+=v5b;var reader=new FileReader();var counter=l2w;var ids=[];var generalError=C5b;generalError+=i5b;generalError+=B5b;editor[Y2h](conf[k1h],Y9w);progressCallback(conf,conf[H7g]||k7g);reader[L7g]=function(e){var h5b='post';var t5b='preSubmit.DTE_Upload';var U5b="ataURL";var e5b="dAsD";var S5b='No Ajax option specified for upload plug-in';var a5b="ja";var R5b="ajaxData";var c5b='upload';var V5b='action';var j5b="axData";var Y5b="isPlainObj";var k5b="nction";var r5b="reUpload";var d8g=A7w;d8g+=j0b;d8g+=A7w;d8g+=g1h;var Z8g=E6w;Z8g+=l7w;var w8g=i7w;w8g+=r5b;var t7g=H5b;t7g+=k5b;var U7g=L5b;U7g+=X7w;U7g+=M4h;var P7g=D7w;P7g+=d2v.b2w;P7g+=N6w;P7g+=M4h;var S7g=d2v.J2w;S7g+=h6b;S7g+=d2v.J2w;S7g+=f7w;var c7g=Y5b;c7g+=d5b;var m7g=Q5b;m7g+=j5b;var j7g=J8w;j7g+=i7w;j7g+=F5b;j7g+=d2v.E7w;var Q7g=m5b;Q7g+=v1h;var Y7g=d2v.J2w;Y7g+=d3b;Y7g+=u0w;Y7g+=d2v.E7w;var data=new FormData();var ajax;data[Y7g](V5b,c5b);data[L6h](Q7g,conf[k1h]);data[L6h](j7g,files[counter]);if(conf[m7g]){conf[R5b](data);}if(conf[K1b]){var V7g=d2v.J2w;V7g+=a5b;V7g+=f7w;ajax=conf[V7g];}else if($[c7g](editor[D7w][K1b])){var a7g=r0w;a7g+=X6w;a7g+=E6w;a7g+=W6b;var R7g=d2v.J2w;R7g+=h6b;R7g+=d2v.J2w;R7g+=f7w;ajax=editor[D7w][K1b][m5b]?editor[D7w][R7g][a7g]:editor[D7w][K1b];}else if(typeof editor[D7w][S7g]===P7g){var e7g=d2v.J2w;e7g+=h6b;e7g+=d2v.J2w;e7g+=f7w;ajax=editor[D7w][e7g];}if(!ajax){throw S5b;}if(typeof ajax===U7g){ajax={url:ajax};}if(typeof ajax[r5w]===t7g){var J7g=A7w;J7g+=d2v.J2w;J7g+=d2v.h2w;J7g+=k0w;var b7g=D7w;b7g+=I5b;b7g+=J4b;b7g+=c8w;var h7g=d2v.E7w;h7g+=d2v.J2w;h7g+=d2v.b2w;h7g+=d2v.J2w;var d={};var ret=ajax[h7g](d);if(ret!==undefined&&typeof ret!==b7g){d=ret;}$[J7g](d,function(key,value){data[L6h](key,value);});}var preRet=editor[P5b](w8g,[conf[k1h],files[counter],data]);if(preRet===o4h){var E8g=X6w;E8g+=u0w;E8g+=e8h;E8g+=k0w;if(counter<files[E8g]-N2w){var s8g=s7h;s8g+=e5b;s8g+=U5b;counter++;reader[s8g](files[counter]);}else{var z8g=t3h;z8g+=X6w;z8g+=X6w;completeCallback[z8g](editor,ids);}return;}var submit=o4h;editor[Z8g](t5b,function(){submit=I4h;return o4h;});$[K1b]($[d8g]({},ajax,{type:h5b,data:data,dataType:w7b,contentType:o4h,processData:o4h,xhr:function(){var D9b="onloadend";var E9b="ess";var w9b="gr";var J5b="xhr";var b5b="jaxSettings";var y8g=d2v.J2w;y8g+=b5b;var xhr=$[y8g][J5b]();if(xhr[m5b]){var u8g=k3h;u8g+=w5w;u8g+=w9b;u8g+=E9b;xhr[m5b][u8g]=function(e){var O9b=':';var u9b="%";var y9b="ded";var d9b="tot";var Z9b="xe";var z9b="toFi";var s9b="lengthComputable";if(e[s9b]){var l8g=n6w;l8g+=l7w;l8g+=c8w;l8g+=l4h;var M8g=z9b;M8g+=Z9b;M8g+=d2v.E7w;var D8g=d9b;D8g+=r1b;var O8g=F5b;O8g+=y9b;var percent=(e[O8g]/e[D8g]*m2w)[M8g](l2w)+u9b;progressCallback(conf,files[l8g]===N2w?percent:counter+O9b+files[X4h]+U1h+percent);}};xhr[m5b][D9b]=function(e){var l9b='Processing';var M9b="processingText";progressCallback(conf,conf[M9b]||l9b);};}return xhr;},success:function(json){var K9b="readAsDataURL";var W9b="Errors";var o9b="XhrSuccess";var X9b="pload";var N9b="dErrors";var T8g=X7w;T8g+=d2v.E7w;var W8g=r0w;W8g+=F5b;W8g+=d2v.E7w;var o8g=Q7w;o8g+=N9b;var X8g=l7w;X8g+=d2v.J2w;X8g+=d2v.w7w;X8g+=A7w;var N8g=J8w;N8g+=X9b;N8g+=o9b;editor[X0h](t5b);editor[P5b](N8g,[conf[X8g],json]);if(json[I9b]&&json[o8g][X4h]){var I8g=j2h;I8g+=W9b;var errors=json[I8g];for(var i=l2w,ien=errors[X4h];i<ien;i++){editor[Y2h](errors[i][k1h],errors[i][T9b]);}}else if(json[Y2h]){editor[Y2h](json[Y2h]);}else if(!json[m5b]||!json[W8g][T8g]){var A8g=A9b;A8g+=I7w;editor[A8g](conf[k1h],generalError);}else{var g8g=X7w;g8g+=d2v.E7w;var x8g=i7w;x8g+=P1b;x8g+=k0w;var K8g=l5b;K8g+=E4h;if(json[K8g]){var G8g=A7w;G8g+=d2v.J2w;G8g+=d2v.h2w;G8g+=k0w;$[G8g](json[y4h],function(table,files){var F8g=Q1h;F8g+=A7w;F8g+=g1h;var f8g=Y0w;f8g+=X7w;f8g+=X6w;f8g+=E4h;if(!Editor[f8g][table]){Editor[y4h][table]={};}$[F8g](Editor[y4h][table],files);});}ids[x8g](json[m5b][g8g]);if(counter<files[X4h]-N2w){counter++;reader[K9b](files[counter]);}else{completeCallback[E6h](editor,ids);if(submit){var p8g=X8h;p8g+=E0w;editor[p8g]();}}}progressCallback(conf);},error:function(xhr){var G9b="uploadXhr";var v8g=l7w;v8g+=d2v.J2w;v8g+=d2v.w7w;v8g+=A7w;var q8g=G9b;q8g+=u1b;var n8g=B6b;n8g+=d2v.b2w;editor[n8g](q8g,[conf[v8g],xhr]);editor[Y2h](conf[k1h],generalError);progressCallback(conf);}}));};files=$[f8b](files,function(val){return val;});if(conf[C8g]!==undefined){var i8g=D7w;i8g+=f9b;files[i8g](conf[F9b],files[X4h]);}reader[B8g](files[l2w]);};Editor[r8g][S9w]=function(init){var m4E='xhr.dt.dte';var j4E="nTable";var Q4E="unique";var Y4E='init.dt.dte';var L4E='body_content';var k4E='foot';var H4E='form_content';var i4E="events";var q4E="TableTools";var n4E="taTable";var p4E="ONS";var F4E='"/></div>';var f4E='<div data-dte-e="form_info" class="';var G4E='<div data-dte-e="form_error" class="';var K4E='<div data-dte-e="form_content" class="';var A4E="tag";var T4E='<form data-dte-e="form" class="';var W4E="footer";var I4E='<div data-dte-e="body_content" class="';var o4E='<div data-dte-e="processing" class="';var X4E="dataSources";var l4E="ajaxUrl";var D4E="ettings";var O4E="mTabl";var u4E="gacyAja";var y4E="detac";var Z4E="ttings";var z4E="ique";var s4E="as";var E4E="iv ";var w4E="indic";var J9b="></div>";var b9b="n/";var h9b="\"><spa";var U9b="data-dte-e=\"body\" ";var P9b="\"/";var S9b="ot\" class";var a9b="<div data-dte-e=\"";var R9b="foo";var c9b="rm>";var V9b="/fo";var j9b="-dte-e=\"head\" class=\"";var Q9b="<div data";var Y9b="class=\"";var L9b="\"><di";var k9b="heade";var H9b="conten";var r9b="orm_buttons\" class=\"";var B9b="<div data-dte-e=\"f";var i9b="oo";var v9b="rmContent";var n9b="Cont";var p9b="cessing";var g9b="mplete";var h6g=J4b;h6g+=x9b;h6g+=E6w;h6g+=g9b;var V6g=F5w;V6g+=C6b;var m6g=q0b;m6g+=l7w;m6g+=c8w;var j6g=w5w;j6g+=p9b;var Q6g=l9h;Q6g+=n9b;Q6g+=q9b;var Y6g=a7w;Y6g+=E6w;Y6g+=d2v.E7w;Y6g+=K5w;var L6g=Y0w;L6g+=E6w;L6g+=v9b;var k6g=Y6w;k6g+=D5h;k6g+=i7w;k6g+=J0w;var H6g=d2v.E7w;H6g+=C4h;var f6g=C9b;f6g+=i9b;f6g+=m5w;var G6g=U9w;G6g+=b1b;G6g+=L4h;var K6g=Y0w;K6g+=o0b;var A6g=B9b;A6g+=r9b;var T6g=H9b;T6g+=d2v.b2w;var W6g=k9b;W6g+=I7w;var I6g=L9b;I6g+=z9w;I6g+=H9w;I6g+=Y9b;var o6g=Y6w;o6g+=U6h;var X6g=k0w;X6g+=A7w;X6g+=d2v.J2w;X6g+=K0w;var N6g=Q9b;N6g+=j9b;var l6g=Y0w;l6g+=o0b;var M6g=Y0w;M6g+=E6w;M6g+=m9b;var D6g=c4h;D6g+=V9b;D6g+=c9b;var O6g=U9w;O6g+=b1b;O6g+=L4h;var u6g=Y0w;u6g+=E6w;u6g+=m9b;var y6g=R9b;y6g+=q6w;y6g+=I7w;var d6g=a9b;d6g+=s1h;d6g+=S9b;d6g+=D1h;var Z6g=i4h;Z6g+=k5w;Z6g+=B4h;var z6g=P9b;z6g+=L4h;var s6g=e9b;s6g+=K5w;var E6g=W6h;E6g+=U9b;E6g+=B2h;E6g+=t9b;var w6g=h9b;w6g+=b9b;w6g+=J9b;var J8g=w4E;J8g+=K0h;J8g+=g4h;var b8g=m8w;b8g+=T7b;var h8g=Q5h;h8g+=E4E;h8g+=Y9b;var t8g=d2v.E7w;t8g+=E6w;t8g+=d2v.w7w;var U8g=v5w;U8g+=s4E;U8g+=q8b;var e8g=M6w;e8g+=z4E;var P8g=G1h;P8g+=Z4E;var S8g=X7w;S8g+=j8h;S8g+=d4E;var a8g=p7b;a8g+=g1h;var R8g=d2v.h2w;R8g+=V3h;R8g+=A7w;R8g+=D7w;var c8g=y4E;c8g+=k0w;var V8g=n6w;V8g+=u4E;V8g+=f7w;var m8g=d2v.E7w;m8g+=W7b;m8g+=H5w;m8g+=b7w;var j8g=d2v.b2w;j8g+=d2v.J2w;j8g+=a7w;j8g+=n6w;var Q8g=L2h;Q8g+=O4E;Q8g+=A7w;var Y8g=C3h;Y8g+=J6w;Y8g+=A7w;var L8g=D7w;L8g+=D4E;var k8g=b4b;k8g+=d2v.E7w;k8g+=T8w;k8g+=D7w;var H8g=Q1h;H8g+=e8b;init=$[H8g](I4h,{},Editor[C1h],init);this[D7w]=$[v3h](I4h,{},Editor[k8g][L8g],{table:init[Y8g]||init[f8h],dbTable:init[M4E]||F3h,ajaxUrl:init[l4E],ajax:init[K1b],idSrc:init[N4E],dataSource:init[Q8g]||init[j8g]?Editor[X4E][m8g]:Editor[X4E][o7h],formOptions:init[D6h],legacyAjax:init[V8g],template:init[H0b]?$(init[H0b])[c8g]():F3h});this[R8g]=$[a8g](I4h,{},Editor[p2h]);this[S0b]=init[S8g];Editor[Z6h][P8g][e8g]++;var that=this;var classes=this[U8g];this[t8g]={"wrapper":$(h8g+classes[e1h]+w3h+o4E+classes[b8g][J8g]+w6g+E6g+classes[l9h][e1h]+w3h+I4E+classes[s6g][t5h]+z6g+Z6g+d6g+classes[y6g][e1h]+w3h+P1h+classes[W4E][t5h]+y3h+W3h+W3h)[l2w],"form":$(T4E+classes[u6g][A4E]+w3h+K4E+classes[Q2b][t5h]+O6g+D6g)[l2w],"formError":$(G4E+classes[M6g][Y2h]+y3h)[l2w],"formInfo":$(f4E+classes[l6g][l3h]+y3h)[l2w],"header":$(N6g+classes[X6g][o6g]+I6g+classes[W6g][T6g]+F4E)[l2w],"buttons":$(A6g+classes[K6g][A3b]+G6g)[l2w]};if($[s8h][e4b][f6g]){var v6g=U4h;v6g+=d2v.w7w;v6g+=z0b;v6g+=A7w;var q6g=A7w;q6g+=d2v.E7w;q6g+=E0w;var n6g=d2v.h2w;n6g+=I7w;n6g+=x4E;n6g+=A7w;var p6g=n1h;p6g+=d4E;var g6g=g4E;g6g+=p4E;var x6g=N1b;x6g+=n4E;var F6g=Y0w;F6g+=l7w;var ttButtons=$[F6g][x6g][q4E][g6g];var i18n=this[p6g];$[i7h]([n6g,q6g,v6g],function(i,val){var C4E="sButtonText";var C6g=v4E;C6g+=b5w;C6g+=Z8w;ttButtons[C6g+val][C4E]=i18n[val][O6h];});}$[i7h](init[i4E],function(evt,fn){that[k3h](evt,function(){var r4E="slic";var r6g=d2v.J2w;r6g+=d3b;r6g+=Q3h;var B6g=B4E;B6g+=X6w;var i6g=r4E;i6g+=A7w;var args=Array[w2h][i6g][B6g](arguments);args[s6h]();fn[r6g](that,args);});});var dom=this[H6g];var wrapper=dom[k6g];dom[L6g]=_editor_el(H4E,dom[Q2b])[l2w];dom[W4E]=_editor_el(k4E,wrapper)[l2w];dom[l9h]=_editor_el(Y6g,wrapper)[l2w];dom[Q6g]=_editor_el(L4E,wrapper)[l2w];dom[j6g]=_editor_el(m6g,wrapper)[l2w];if(init[V6g]){var c6g=d2v.J2w;c6g+=d2v.E7w;c6g+=d2v.E7w;this[c6g](init[Z1b]);}$(document)[k3h](Y4E+this[D7w][Q4E],function(e,settings,json){var S6g=c8w;S6g+=A7w;S6g+=d2v.b2w;var a6g=j0h;a6g+=a7w;a6g+=X6w;a6g+=A7w;var R6g=j0h;R6g+=a7w;R6g+=n6w;if(that[D7w][R6g]&&settings[j4E]===$(that[D7w][a6g])[S6g](l2w)){var P6g=Z8w;P6g+=A7w;P6g+=K8b;P6g+=g4h;settings[P6g]=that;}})[k3h](m4E+this[D7w][Q4E],function(e,settings,json){var U6g=c8w;U6g+=A7w;U6g+=d2v.b2w;var e6g=d2v.b2w;e6g+=W5b;e6g+=n6w;if(json&&that[D7w][f8h]&&settings[j4E]===$(that[D7w][e6g])[U6g](l2w)){that[V4E](json);}});try{var t6g=X7w;t6g+=l7w;t6g+=E0w;this[D7w][q6h]=Editor[u7h][init[u7h]][t6g](this);}catch(e){var c4E='Cannot find display controller ';throw c4E+init[u7h];}this[P5b](h6g,[]);};Editor[w2h][j2b]=function(){var R4E="actions";var w0g=e8w;w0g+=X7w;w0g+=d2v.b2w;var J6g=d2v.E7w;J6g+=E6w;J6g+=d2v.w7w;var b6g=v5w;b6g+=O2h;b6g+=E4h;var classesActions=this[b6g][R4E];var action=this[D7w][m4b];var wrapper=$(this[J6g][e1h]);wrapper[Q2h]([classesActions[q2b],classesActions[w0g],classesActions[M8h]][J6b](U1h));if(action===q2b){wrapper[e3b](classesActions[q2b]);}else if(action===e7b){wrapper[e3b](classesActions[e7b]);}else if(action===M8h){wrapper[e3b](classesActions[M8h]);}};Editor[w2h][a4E]=function(data,success,error,submitParams){var H1E='?';var r1E="param";var B1E="let";var i1E="comp";var n1E="rro";var p1E="url";var g1E=/_id_/;var K1E=',';var u1E="jso";var y1E="Url";var d1E="aja";var Z1E="tFiel";var s1E="idS";var w1E="functi";var b4E="ETE";var h4E="EL";var t4E="Body";var P4E="teB";var S4E="dele";var c0g=d2v.J2w;c0g+=B5w;var j0g=S4E;j0g+=P4E;j0g+=e4E;var Q0g=d2v.E7w;Q0g+=A7w;Q0g+=U4E;Q0g+=t4E;var Y0g=X8w;Y0g+=h4E;Y0g+=b4E;var r0g=Y1h;r0g+=d2v.J2w;var B0g=j5w;B0g+=X6w;var i0g=J8w;i0g+=I7w;i0g+=X6w;var K0g=L5b;K0g+=J4E;var I0g=w1E;I0g+=k3h;var o0g=n7b;o0g+=m1b;var N0g=E1E;N0g+=x4h;N0g+=o2h;var l0g=s1E;l0g+=z1E;var M0g=v4E;M0g+=Z1E;M0g+=d2v.E7w;M0g+=D7w;var D0g=d1E;D0g+=f7w;D0g+=y1E;var O0g=d2v.J2w;O0g+=h6b;O0g+=d2v.J2w;O0g+=f7w;var E0g=u1E;E0g+=l7w;var that=this;var action=this[D7w][m4b];var thrown;var opts={type:J2b,dataType:E0g,data:F3h,error:[function(xhr,text,err){thrown=err;}],success:[],complete:[function(xhr,text){var T1E="parseJSON";var W1E="sponseJSON";var I1E="JSON";var o1E="response";var N1E="respons";var l1E="eText";var M1E="res";var D1E="inObjec";var O1E="isPl";var V2w=204;var u0g=O1E;u0g+=d2v.J2w;u0g+=D1E;u0g+=d2v.b2w;var z0g=l7w;z0g+=w1h;z0g+=X6w;var s0g=M1E;s0g+=s7b;s0g+=i5w;s0g+=l1E;var json=F3h;if(xhr[T9b]===V2w||xhr[s0g]===z0g){json={};}else{try{var y0g=N1E;y0g+=A7w;y0g+=X1E;y0g+=j0b;var d0g=o1E;d0g+=I1E;var Z0g=U4h;Z0g+=W1E;json=xhr[Z0g]?xhr[d0g]:$[T1E](xhr[y0g]);}catch(e){}}if($[u0g](json)||$[t4b](json)){success(json,xhr[T9b]>=c2w,xhr);}else{error(xhr,text,thrown);}}]};var a;var ajaxSrc=this[D7w][O0g]||this[D7w][D0g];var id=action===J0b||action===z5b?_pluck(this[D7w][M0g],l0g):F3h;if($[N0g](id)){var X0g=A1E;X0g+=J4b;id=id[X0g](K1E);}if($[o0g](ajaxSrc)&&ajaxSrc[action]){ajaxSrc=ajaxSrc[action];}if(typeof ajaxSrc===I0g){var W0g=d1E;W0g+=G1E;W0g+=f1E;var uri=F3h;var method=F3h;if(this[D7w][W0g]){var A0g=j7h;A0g+=d4b;A0g+=J3h;var T0g=Q5b;T0g+=d2v.J2w;T0g+=G1E;T0g+=f1E;var url=this[D7w][T0g];if(url[q2b]){uri=url[action];}if(uri[F1E](U1h)!==-N2w){a=uri[x1E](U1h);method=a[l2w];uri=a[N2w];}uri=uri[A0g](g1E,id);}ajaxSrc(method,uri,data,success,error);return;}else if(typeof ajaxSrc===K0g){if(ajaxSrc[F1E](U1h)!==-N2w){var F0g=J8w;F0g+=I7w;F0g+=X6w;var f0g=d2v.b2w;f0g+=K5w;f0g+=i7w;f0g+=A7w;var G0g=D7w;G0g+=i7w;G0g+=W4h;G0g+=d2v.b2w;a=ajaxSrc[G0g](U1h);opts[f0g]=a[l2w];opts[F0g]=a[N2w];}else{opts[p1E]=ajaxSrc;}}else{var C0g=Q1h;C0g+=A7w;C0g+=g1h;var q0g=A7w;q0g+=n1E;q0g+=I7w;var x0g=A7w;x0g+=f7w;x0g+=q1E;var optsCopy=$[x0g]({},ajaxSrc||{});if(optsCopy[v1E]){var n0g=d2v.h2w;n0g+=E6w;n0g+=C1E;n0g+=U4E;var p0g=J8w;p0g+=l7w;p0g+=s6h;var g0g=i1E;g0g+=B1E;g0g+=A7w;opts[g0g][p0g](optsCopy[v1E]);delete optsCopy[n0g];}if(optsCopy[q0g]){var v0g=M8b;v0g+=g4h;opts[Y2h][E2h](optsCopy[v0g]);delete optsCopy[Y2h];}opts=$[C0g]({},opts,optsCopy);}opts[i0g]=opts[B0g][S7h](g1E,id);if(opts[r0g]){var L0g=d2v.E7w;L0g+=d2v.J2w;L0g+=d2v.b2w;L0g+=d2v.J2w;var k0g=d2v.E7w;k0g+=W7b;var H0g=Y1h;H0g+=d2v.J2w;var isFn=typeof opts[H0g]===d2v.d7w;var newData=isFn?opts[k0g](data):opts[L0g];data=isFn&&newData?newData:$[v3h](I4h,data,newData);}opts[r5w]=data;if(opts[h6w]===Y0g&&(opts[Q0g]===undefined||opts[j0g]===I4h)){var V0g=J8w;V0g+=I7w;V0g+=X6w;var m0g=N1b;m0g+=d2v.b2w;m0g+=d2v.J2w;var params=$[r1E](opts[m0g]);opts[V0g]+=opts[p1E][F1E](H1E)===-N2w?H1E+params:e7h+params;delete opts[r5w];}$[c0g](opts);};Editor[R0g][a0g]=function(target,style,time,callback){var S0g=v0w;S0g+=G5h;if($[s8h][S0g]){var P0g=D7w;P0g+=d2v.b2w;P0g+=E6w;P0g+=i7w;target[P0g]()[x8h](style,time,callback);}else{var e0g=k1E;e0g+=G8h;target[A2h](style);if(typeof time===e0g){time[E6h](target);}else if(callback){callback[E6h](target);}}};Editor[w2h][z8b]=function(){var Q1E="formInfo";var L1E="oter";var J0g=Y0w;J0g+=E6w;J0g+=I7w;J0g+=d2v.w7w;var b0g=z6h;b0g+=e8b;var h0g=Y0w;h0g+=E6w;h0g+=L1E;var t0g=Q9h;t0g+=W6b;t0g+=J0w;var U0g=H4b;U0g+=J0w;var dom=this[C3h];$(dom[U0g])[g3h](dom[t0g]);$(dom[h0g])[L6h](dom[W3b])[L6h](dom[A3b]);$(dom[Y1E])[b0g](dom[Q1E])[L6h](dom[J0g]);};Editor[w2h][C1b]=function(){var c1E="Blur";var m1E="reBlur";var Z5g=G1b;Z5g+=a7w;Z5g+=j1E;var z5g=i7w;z5g+=m1E;var s5g=V1E;s5g+=l7w;s5g+=d2v.b2w;var E5g=E6w;E5g+=l7w;E5g+=c1E;var w5g=A7w;w5g+=d2v.E7w;w5g+=P6b;w5g+=D7w;var opts=this[D7w][w5g];var onBlur=opts[E5g];if(this[s5g](z5g)===o4h){return;}if(typeof onBlur===d2v.d7w){onBlur(this);}else if(onBlur===Z5g){var d5g=R1E;d5g+=a1E;d5g+=d2v.b2w;this[d5g]();}else if(onBlur===l6h){var y5g=g0w;y5g+=X6w;y5g+=E6w;y5g+=G1h;this[y5g]();}};Editor[w2h][S1E]=function(){var N5g=d8b;N5g+=d2v.h2w;N5g+=k0w;var l5g=m6h;l5g+=D7h;l5g+=e6h;var M5g=d2v.E7w;M5g+=C4h;var D5g=d2v.E7w;D5g+=X7w;D5g+=z9w;D5g+=E9w;var O5g=J0w;O5g+=I7w;O5g+=E6w;O5g+=I7w;var u5g=Y0w;u5g+=e5w;if(!this[D7w]){return;}var errorClass=this[p2h][u5g][O5g];var fields=this[D7w][Z1b];$(D5g+errorClass,this[M5g][l5g])[Q2h](errorClass);$[N5g](fields,function(name,field){field[Y2h](Y9w)[V4h](Y9w);});this[Y2h](Y9w)[V4h](Y9w);};Editor[X5g][o5g]=function(submitComplete){var b1E="eCb";var t1E="eC";var U1E="ocus";var e1E="us.editor-f";var F5g=d2v.h2w;F5g+=X6w;F5g+=E6w;F5g+=G1h;var f5g=V1E;f5g+=q5w;var G5g=P1E;G5g+=e1E;G5g+=U1E;var K5g=n6h;K5g+=d2v.E7w;K5g+=K5w;var W5g=r0h;W5g+=t1E;W5g+=a7w;var I5g=h1E;I5g+=r6w;I5g+=p0w;I5g+=G1h;if(this[P5b](I5g)===o4h){return;}if(this[D7w][W5g]){var A5g=v5w;A5g+=B6h;A5g+=r6w;A5g+=a7w;var T5g=r0h;T5g+=b1E;this[D7w][T5g](submitComplete);this[D7w][A5g]=F3h;}if(this[D7w][J1E]){this[D7w][J1E]();this[D7w][J1E]=F3h;}$(K5g)[X0h](G5g);this[D7w][k7b]=o4h;this[f5g](F5g);};Editor[x5g][g5g]=function(fn){var w3E="seCb";var p5g=d2v.h2w;p5g+=p0w;p5g+=w3E;this[D7w][p5g]=fn;};Editor[n5g][L2b]=function(arg1,arg2,arg3,arg4){var s3E="main";var that=this;var title;var buttons;var show;var opts;if($[c1b](arg1)){opts=arg1;}else if(typeof arg1===E3E){show=arg1;opts=arg2;}else{title=arg1;buttons=arg2;show=arg3;opts=arg4;}if(show===undefined){show=I4h;}if(title){that[D3h](title);}if(buttons){that[A3b](buttons);}return{opts:$[v3h]({},this[D7w][D6h][s3E],opts),maybeOpen:function(){if(show){var q5g=E6w;q5g+=i7w;q5g+=A7w;q5g+=l7w;that[q5g]();}}};};Editor[w2h][v5g]=function(name){var Z3E="pply";var z3E="taSou";var B5g=N1b;B5g+=z3E;B5g+=z1E;B5g+=A7w;var i5g=t3h;i5g+=X6w;i5g+=X6w;var C5g=w5w;C5g+=d2v.b2w;C5g+=Z0w;var args=Array[C5g][w6h][i5g](arguments);args[s6h]();var fn=this[D7w][B5g][name];if(fn){var r5g=d2v.J2w;r5g+=Z3E;return fn[r5g](this,args);}};Editor[w2h][H5g]=function(includeFields){var f3E='displayOrder';var N3E="Fields";var l3E="lude";var D3E='main';var O3E="mC";var u3E="emplate";var y3E="ildren";var d3E="etach";var h5g=Z8w;h5g+=Z0b;h5g+=q5w;var t5g=d2v.w7w;t5g+=d2v.J2w;t5g+=X7w;t5g+=l7w;var c5g=A7w;c5g+=d2v.J2w;c5g+=d2v.h2w;c5g+=k0w;var V5g=d2v.E7w;V5g+=d3E;var m5g=e5h;m5g+=y3E;var Q5g=b4b;Q5g+=d2v.E7w;Q5g+=A7w;var Y5g=d2v.b2w;Y5g+=u3E;var L5g=Y0w;L5g+=g4h;L5g+=O3E;L5g+=C9h;var k5g=d2v.E7w;k5g+=E6w;k5g+=d2v.w7w;var that=this;var formContent=$(this[k5g][L5g]);var fields=this[D7w][Z1b];var order=this[D7w][I1b];var template=this[D7w][Y5g];var mode=this[D7w][Q5g]||D3E;if(includeFields){var j5g=X7w;j5g+=M3E;j5g+=l3E;j5g+=N3E;this[D7w][j5g]=includeFields;}else{includeFields=this[D7w][p3b];}formContent[m5g]()[V5g]();$[c5g](order,function(i,fieldOrName){var K3E='editor-field[name="';var T3E="itor-template=\"";var W3E="a-ed";var I3E="[dat";var o3E="_weakInArray";var R5g=X3E;R5g+=A7w;R5g+=X6w;R5g+=d2v.E7w;var name=fieldOrName instanceof Editor[R5g]?fieldOrName[k1h]():fieldOrName;if(that[o3E](name,includeFields)!==-N2w){if(template&&mode===D3E){var U5g=l7w;U5g+=E6w;U5g+=d2v.E7w;U5g+=A7w;var e5g=U9w;e5g+=t9w;var P5g=I3E;P5g+=W3E;P5g+=T3E;var S5g=A3E;S5g+=d2v.b2w;S5g+=J0w;var a5g=Y0w;a5g+=O5h;template[a5g](K3E+name+G3E)[S5g](fields[name][M6b]());template[G7b](P5g+name+e5g)[L6h](fields[name][U5g]());}else{formContent[L6h](fields[name][M6b]());}}});if(template&&mode===t5g){template[p5h](formContent);}this[h5g](f3E,[this[D7w][k7b],this[D7w][m4b],formContent]);};Editor[w2h][b5g]=function(items,editFields,type,formOptions,setupDone){var V3E="ice";var p3E="itFi";var g3E="tData";var x3E="lic";var F3E="nitE";var n9g=l7w;n9g+=E6w;n9g+=v7w;var p9g=X7w;p9g+=F3E;p9g+=d2v.E7w;p9g+=E0w;var F9g=X6w;F9g+=A7w;F9g+=M4h;F9g+=l4h;var f9g=D7w;f9g+=x3E;f9g+=A7w;var G9g=E6w;G9g+=I7w;G9g+=d2v.E7w;G9g+=J0w;var d9g=d2v.w7w;d9g+=V5w;d9g+=A7w;var Z9g=k5w;Z9g+=o6h;Z9g+=X6w;Z9g+=o2h;var z9g=D7w;z9g+=a6w;z9g+=X6w;z9g+=A7w;var s9g=Y0w;s9g+=g4h;s9g+=d2v.w7w;var E9g=A7w;E9g+=d2v.E7w;E9g+=X7w;E9g+=d2v.b2w;var w9g=v4E;w9g+=g3E;var J5g=e8w;J5g+=p3E;J5g+=S7b;var that=this;var fields=this[D7w][Z1b];var usedFields=[];var includeInOrder;var editData={};this[D7w][J5g]=editFields;this[D7w][w9g]=editData;this[D7w][Y2b]=items;this[D7w][m4b]=E9g;this[C3h][s9g][z9g][Z9g]=k7h;this[D7w][d9g]=type;this[j2b]();$[i7h](fields,function(name,field){var y9g=A7w;y9g+=d2v.J2w;y9g+=d2v.h2w;y9g+=k0w;field[l1b]();includeInOrder=o4h;editData[name]={};$[y9g](editFields,function(idSrc,edit){var Y3E="iSet";var L3E="layFields";var H3E="yFiel";var r3E="ltiSet";var B3E="splayFields";var i3E="yFi";var C3E="scope";var v3E="omData";var q3E="alFr";var n3E="rra";var u9g=f5w;u9g+=A7w;u9g+=t5w;u9g+=D7w;if(edit[u9g][name]){var l9g=D7w;l9g+=x3E;l9g+=A7w;var M9g=F7h;M9g+=y6w;M9g+=n3E;M9g+=K5w;var D9g=d2v.E7w;D9g+=d2v.J2w;D9g+=j0h;var O9g=z9w;O9g+=q3E;O9g+=v3E;var val=field[O9g](edit[D9g]);editData[name][idSrc]=val===F3h?Y9w:$[M9g](val)?val[l9g]():val;if(!formOptions||formOptions[C3E]===N6h){var o9g=g5w;o9g+=i3E;o9g+=T8w;o9g+=k2b;var X9g=k5w;X9g+=B3E;var N9g=O5w;N9g+=r3E;field[N9g](idSrc,val!==undefined?val:field[s2h]());if(!edit[X9g]||edit[o9g][name]){includeInOrder=I4h;}}else{var W9g=g5w;W9g+=H3E;W9g+=k2b;var I9g=k3E;I9g+=L3E;if(!edit[I9g]||edit[W9g][name]){var A9g=d2v.E7w;A9g+=A7w;A9g+=Y0w;var T9g=d2v.w7w;T9g+=J8w;T9g+=Q8h;T9g+=Y3E;field[T9g](idSrc,val!==undefined?val:field[A9g]());includeInOrder=I4h;}}}});if(field[R2h]()[X4h]!==l2w&&includeInOrder){var K9g=i7w;K9g+=Q3E;usedFields[K9g](name);}});var currOrder=this[G9g]()[f9g]();for(var i=currOrder[F9g]-N2w;i>=l2w;i--){var x9g=J4b;x9g+=j3E;x9g+=I7w;x9g+=o2h;if($[x9g](currOrder[i][m3E](),usedFields)===-N2w){var g9g=o6h;g9g+=X6w;g9g+=V3E;currOrder[g9g](i,N2w);}}this[c6b](currOrder);this[P5b](p9g,[_pluck(editFields,n9g)[l2w],_pluck(editFields,N0b)[l2w],items,type],function(){var c3E='initMultiEdit';that[P5b](c3E,[editFields,items,type],function(){setupDone();});});};Editor[w2h][P5b]=function(trigger,args,promiseComplete){var h3E="result";var t3E="bj";var U3E='Cancelled';var S3E="andler";var a3E="triggerH";var R3E="indexO";if(!args){args=[];}if($[t4b](trigger)){for(var i=l2w,ien=trigger[X4h];i<ien;i++){this[P5b](trigger[i],args);}}else{var k9g=I7w;k9g+=A7w;k9g+=D7w;k9g+=K7h;var i9g=I7w;i9g+=A7w;i9g+=D7w;i9g+=K7h;var C9g=i7w;C9g+=I7w;C9g+=A7w;var v9g=R3E;v9g+=Y0w;var q9g=a3E;q9g+=S3E;var e=$[P3E](trigger);$(this)[q9g](e,args);if(trigger[v9g](C9g)===l2w&&e[i9g]===o4h){$(this)[e3E]($[P3E](trigger+U3E),args);}if(promiseComplete){var r9g=U4h;r9g+=D7w;r9g+=K7h;var B9g=E6w;B9g+=t3E;B9g+=d5b;if(e[h3E]&&typeof e[h3E]===B9g&&e[r9g][g7b]){var H9g=d2v.b2w;H9g+=k0w;H9g+=A7w;H9g+=l7w;e[h3E][H9g](promiseComplete);}else{promiseComplete();}}return e[k9g];}};Editor[L9g][b3E]=function(input){var z2E="owerCas";var E2E="subst";var w2E=/^on([A-Z])/;var j9g=A1E;j9g+=J4b;var name;var names=input[x1E](U1h);for(var i=l2w,ien=names[X4h];i<ien;i++){name=names[i];var onStyle=name[J3E](w2E);if(onStyle){var Q9g=E2E;Q9g+=T2b;var Y9g=o7w;Y9g+=s2E;Y9g+=z2E;Y9g+=A7w;name=onStyle[N2w][Y9g]()+name[Q9g](o2w);}names[i]=name;}return names[j9g](U1h);};Editor[w2h][Z2E]=function(node){var foundField=F3h;$[i7h](this[D7w][Z1b],function(name,field){var V9g=X6w;V9g+=u0w;V9g+=c8w;V9g+=l4h;var m9g=Y0w;m9g+=X7w;m9g+=l7w;m9g+=d2v.E7w;if($(field[M6b]())[m9g](node)[V9g]){foundField=field;}});return foundField;};Editor[w2h][c9g]=function(fieldNames){var R9g=F7h;R9g+=G2b;if(fieldNames===undefined){return this[Z1b]();}else if(!$[R9g](fieldNames)){return[fieldNames];}return fieldNames;};Editor[a9g][f6b]=function(fieldsIn,focus){var D2E=/^jq:/;var O2E='div.DTE ';var u2E="ace";var y2E="q:";var S9g=d2v.w7w;S9g+=d2v.J2w;S9g+=i7w;var that=this;var field;var fields=$[S9g](fieldsIn,function(fieldOrName){var d2E="tring";var P9g=D7w;P9g+=d2E;return typeof fieldOrName===P9g?that[D7w][Z1b][fieldOrName]:fieldOrName;});if(typeof focus===H2b){field=fields[focus];}else if(focus){var e9g=h6b;e9g+=y2E;if(focus[F1E](e9g)===l2w){var U9g=c7h;U9g+=u2E;field=$(O2E+focus[U9g](D2E,Y9w));}else{var t9g=Q7w;t9g+=d2v.E7w;t9g+=D7w;field=this[D7w][t9g][focus];}}this[D7w][M2E]=field;if(field){field[S3h]();}};Editor[w2h][O3b]=function(opts){var m2E="canReturnSubmit";var Y2E="keyCode";var k2E="butt";var H2E="tle";var i2E="onBackground";var C2E="urOnB";var v2E="onRet";var q2E="submitOnRetur";var n2E="ubmit";var p2E="submitOnReturn";var g2E="onBl";var x2E="nBlur";var F2E="submitO";var f2E="submitOnBlur";var G2E="onComplete";var K2E="omplete";var A2E="seOnC";var T2E="closeOnComplete";var W2E="eInline";var I2E=".dt";var o2E="ground";var X2E="blurOnBack";var N2E="ount";var l2E="loseIc";var V41=d2v.h2w;V41+=l2E;V41+=a7w;var G41=E6w;G41+=l7w;var W41=k1E;W41+=G8h;var I41=u7b;I41+=O7b;var o41=u7b;o41+=O7b;var l41=e8w;l41+=x9b;l41+=N2E;var M41=e7b;M41+=E8b;M41+=D7w;var u41=X2E;u41+=o2E;var h9g=I2E;h9g+=W2E;var that=this;var inlineCount=__inlineCounter++;var namespace=h9g+inlineCount;if(opts[T2E]!==undefined){var J9g=p3h;J9g+=l7w;J9g+=A7w;var b9g=F1b;b9g+=A2E;b9g+=K2E;opts[G2E]=opts[b9g]?l6h:J9g;}if(opts[f2E]!==undefined){var z41=d2v.h2w;z41+=X6w;z41+=B6h;var s41=R1E;s41+=j1E;var E41=F2E;E41+=x2E;var w41=g2E;w41+=j5w;opts[w41]=opts[E41]?s41:z41;}if(opts[p2E]!==undefined){var y41=D7w;y41+=n2E;var d41=q2E;d41+=l7w;var Z41=v2E;Z41+=j5w;Z41+=l7w;opts[Z41]=opts[d41]?y41:K2h;}if(opts[u41]!==undefined){var D41=l7w;D41+=E6w;D41+=l7w;D41+=A7w;var O41=Q5w;O41+=C2E;O41+=N0h;opts[i2E]=opts[O41]?n1b:D41;}this[D7w][M41]=opts;this[D7w][l41]=inlineCount;if(typeof opts[D3h]===R7h||typeof opts[D3h]===d2v.d7w){var X41=d2v.b2w;X41+=X7w;X41+=B2E;X41+=A7w;var N41=r2E;N41+=H2E;this[N41](opts[D3h]);opts[X41]=I4h;}if(typeof opts[o41]===R7h||typeof opts[I41]===W41){var T41=d2v.w7w;T41+=P0b;this[V4h](opts[T41]);opts[V4h]=I4h;}if(typeof opts[A3b]!==E3E){var K41=k2E;K41+=E6w;K41+=i5w;var A41=k2E;A41+=G0b;this[A41](opts[K41]);opts[A3b]=I4h;}$(document)[G41](L2E+namespace,function(e){var j2E="dFromNode";var Q2E="tiveElement";if(e[Y2E]===f2w&&that[D7w][k7b]){var f41=d2v.J2w;f41+=d2v.h2w;f41+=Q2E;var el=$(document[f41]);if(el){var F41=I8b;F41+=T8w;F41+=j2E;var field=that[F41](el);if(field[m2E](el)){e[I2b]();}}}});$(document)[k3h](M2b+namespace,function(e){var D7E='button';var O7E="next";var y7E="eyCod";var d7E='.DTE_Form_Buttons';var Z7E="lu";var z7E="onEsc";var s7E="onE";var E7E="tDefault";var w7E="preven";var J2E="sc";var b2E="lur";var h2E="Es";var t2E="fault";var U2E="entDe";var e2E="prev";var P2E="onReturn";var a2E="nSubmit";var R2E="canRetu";var k2w=39;var H2w=37;var L41=X6w;L41+=V2E;var el=$(document[c2E]);if(e[Y2E]===f2w&&that[D7w][k7b]){var g41=R2E;g41+=I7w;g41+=a2E;var x41=H5b;x41+=M3E;x41+=S2E;var field=that[Z2E](el);if(field&&typeof field[m2E]===x41&&field[g41](el)){var n41=H5b;n41+=M3E;n41+=S2E;if(opts[P2E]===M6h){var p41=e2E;p41+=U2E;p41+=t2E;e[p41]();that[q1b]();}else if(typeof opts[P2E]===n41){e[I2b]();opts[P2E](that,e);}}}else if(e[Y2E]===v2w){var H41=X8h;H41+=E0w;var r41=k3h;r41+=h2E;r41+=d2v.h2w;var i41=a7w;i41+=b2E;var v41=E6w;v41+=l7w;v41+=k8w;v41+=J2E;var q41=w7E;q41+=E7E;e[q41]();if(typeof opts[v41]===d2v.d7w){var C41=s7E;C41+=D7w;C41+=d2v.h2w;opts[C41](that,e);}else if(opts[z7E]===i41){var B41=a7w;B41+=Z7E;B41+=I7w;that[B41]();}else if(opts[r41]===l6h){that[q5h]();}else if(opts[z7E]===H41){var k41=D7w;k41+=n2E;that[k41]();}}else if(el[T2h](d7E)[L41]){var m41=T4h;m41+=y7E;m41+=A7w;var Y41=u7E;Y41+=K5w;Y41+=r6w;Y41+=Z5w;if(e[Y41]===H2w){var j41=a7w;j41+=z2b;j41+=k3h;var Q41=i7w;Q41+=I7w;Q41+=A7w;Q41+=z9w;el[Q41](j41)[S3h]();}else if(e[m41]===k2w){el[O7E](D7E)[S3h]();}}});this[D7w][V41]=function(){var c41=i7b;c41+=Y0w;$(document)[c41](L2E+namespace);$(document)[X0h](M2b+namespace);};return namespace;};Editor[w2h][R41]=function(direction,action,data){var N7E="ax";var l7E="cyAj";var M7E="leg";var a41=M7E;a41+=d2v.J2w;a41+=l7E;a41+=N7E;if(!this[D7w][a41]||!data){return;}if(direction===X7E){var S41=A7w;S41+=d2v.E7w;S41+=X7w;S41+=d2v.b2w;if(action===f3h||action===S41){var h41=d2v.E7w;h41+=d2v.J2w;h41+=d2v.b2w;h41+=d2v.J2w;var t41=N1b;t41+=j0h;var e41=N1b;e41+=j0h;var P41=A7w;P41+=d2v.J2w;P41+=d2v.h2w;P41+=k0w;var id;$[P41](data[e41],function(rowId,values){var I7E="legacy Ajax data format";var o7E="Editor: Multi-row editing is not supported by the ";if(id!==undefined){var U41=o7E;U41+=I7E;throw U41;}id=rowId;});data[t41]=data[h41][id];if(action===J0b){var b41=X7w;b41+=d2v.E7w;data[b41]=id;}}else{var J41=X7w;J41+=d2v.E7w;data[J41]=$[f8b](data[r5w],function(values,id){return id;});delete data[r5w];}}else{var w11=d2v.E7w;w11+=d2v.J2w;w11+=d2v.b2w;w11+=d2v.J2w;if(!data[w11]&&data[a4b]){var s11=I7w;s11+=E6w;s11+=Y6w;var E11=d2v.E7w;E11+=d2v.J2w;E11+=j0h;data[E11]=[data[s11]];}else if(!data[r5w]){var z11=d2v.E7w;z11+=d2v.J2w;z11+=d2v.b2w;z11+=d2v.J2w;data[z11]=[];}}};Editor[w2h][V4E]=function(json){var Z11=y8h;Z11+=d2v.b2w;Z11+=s8b;var that=this;if(json[Z11]){var d11=f5w;d11+=S7b;$[i7h](this[D7w][d11],function(name,field){var T7E="update";var y11=E6w;y11+=W7E;if(json[y11][name]!==undefined){var u11=F5w;u11+=X6w;u11+=d2v.E7w;var fieldInst=that[u11](name);if(fieldInst&&fieldInst[T7E]){var O11=E6w;O11+=A7E;O11+=i5w;fieldInst[T7E](json[O11][name]);}}});}};Editor[w2h][D8b]=function(el,msg){var K7E="layed";var D11=Y0w;D11+=l7w;var canAnimate=$[D11][x8h]?I4h:o4h;if(typeof msg===d2v.d7w){msg=msg(this,new DataTable[L0b](this[D7w][f8h]));}el=$(el);if(canAnimate){var M11=D7w;M11+=R9h;el[M11]();}if(!msg){var l11=k5w;l11+=o6h;l11+=K7E;if(this[D7w][l11]&&canAnimate){el[k4b](function(){el[o7h](Y9w);});}else{var o11=l7w;o11+=h8h;var X11=d2v.h2w;X11+=D7w;X11+=D7w;var N11=G7E;N11+=X6w;el[N11](Y9w)[X11](q3h,o11);}}else{var I11=d2v.E7w;I11+=f7E;I11+=d2v.E7w;if(this[D7w][I11]&&canAnimate){var W11=k0w;W11+=F7E;el[W11](msg)[P9h]();}else{var A11=l8h;A11+=D7w;var T11=M7h;T11+=d2v.w7w;T11+=X6w;el[T11](msg)[A11](q3h,k7h);}}};Editor[w2h][x7E]=function(){var v7E="multiInfoShown";var q7E="Value";var n7E="alue";var p7E="MultiV";var g7E="includeFie";var f11=A7h;f11+=l4h;var G11=g7E;G11+=C6b;var K11=f5w;K11+=C2b;K11+=D7w;var fields=this[D7w][K11];var include=this[D7w][G11];var show=I4h;var state;if(!include){return;}for(var i=l2w,ien=include[f11];i<ien;i++){var x11=F7h;x11+=p7E;x11+=n7E;var F11=F7h;F11+=w6w;F11+=A4h;F11+=q7E;var field=fields[include[i]];var multiEditable=field[R3h]();if(field[F11]()&&multiEditable&&show){state=I4h;show=o4h;}else if(field[x11]()&&!multiEditable){state=I4h;}else{state=o4h;}fields[include[i]][v7E](state);}};Editor[g11][p11]=function(type){var k7E="-focus";var H7E="focus.";var r7E='submit.editor-internal';var B7E="captureFocus";var i7E="bmit.editor-internal";var C7E="bubbl";var Q11=y8h;Q11+=A7w;Q11+=l7w;var v11=C7E;v11+=A7w;var q11=V6b;q11+=l7w;var n11=G1b;n11+=i7E;var that=this;var focusCapture=this[D7w][q6h][B7E];if(focusCapture===undefined){focusCapture=I4h;}$(this[C3h][Q2b])[X0h](r7E)[k3h](n11,function(e){e[I2b]();});if(focusCapture&&(type===q11||type===v11)){var i11=H7E;i11+=Y0b;i11+=k7E;var C11=E6w;C11+=l7w;$(x0h)[C11](i11,function(){var V7E="etFoc";var m7E='.DTE';var j7E="Elem";var Q7E="active";var Y7E="arents";var L7E="DTED";var L11=n6w;L11+=l7w;L11+=c8w;L11+=l4h;var k11=E9w;k11+=L7E;var H11=i7w;H11+=Y7E;var r11=n6w;r11+=M4h;r11+=l4h;var B11=Q7E;B11+=j7E;B11+=q9b;if($(document[B11])[T2h](m7E)[r11]===l2w&&$(document[c2E])[H11](k11)[L11]===l2w){if(that[D7w][M2E]){var Y11=D7w;Y11+=V7E;Y11+=J8w;Y11+=D7w;that[D7w][Y11][S3h]();}}});}this[x7E]();this[P5b](Q11,[type,this[D7w][m4b]]);return I4h;};Editor[j11][D3b]=function(type){var e7E="seIcb";var P7E='cancelOpen';var a7E="oseIcb";var R7E="even";var c7E="eO";var P11=d2v.E7w;P11+=f7E;P11+=d2v.E7w;var V11=t6w;V11+=c7E;V11+=H7w;V11+=l7w;var m11=Z8w;m11+=R7E;m11+=d2v.b2w;if(this[m11](V11,[type,this[D7w][m4b]])===o4h){var a11=v5w;a11+=a7E;var R11=d2v.w7w;R11+=Z5w;var c11=S7E;c11+=q9b;this[S1E]();this[c11](P7E,[type,this[D7w][m4b]]);if((this[D7w][R11]===d6b||this[D7w][P5w]===R1b)&&this[D7w][a11]){var S11=F1b;S11+=e7E;this[D7w][S11]();}this[D7w][J1E]=F3h;return o4h;}this[D7w][P11]=type;return I4h;};Editor[w2h][v0b]=function(processing){var h7E='div.DTE';var t7E="ctive";var U7E="gleC";var J11=t6w;J11+=E6w;J11+=d2v.h2w;J11+=T7b;var b11=r8h;b11+=U7E;b11+=X6w;b11+=O2h;var h11=H4b;h11+=J0w;var t11=L2h;t11+=d2v.w7w;var U11=d2v.J2w;U11+=t7E;var e11=w5w;e11+=d2v.h2w;e11+=T7b;var procClass=this[p2h][e11][U11];$([h7E,this[t11][h11]])[b11](procClass,processing);this[D7w][K3h]=processing;this[P5b](J11,[processing]);};Editor[w31][E31]=function(successCallback,errorCallback,formatdata,hide){var k8E='preSubmit';var r8E="Complet";var B8E="mple";var i8E="nCo";var C8E="nComplete";var v8E='allIfChanged';var q8E='all';var o8E="creat";var N8E="dbTa";var l8E="editData";var M8E="editCount";var D8E="dataSource";var O8E="ectDataFn";var u8E="tObj";var y8E="_fnSe";var d8E="tFi";var z8E="acyAjax";var s8E="eg";var E8E="_l";var w8E="ven";var J7E="mitTable";var b7E="ub";var v31=B4E;v31+=X6w;var q31=i0b;q31+=b7E;q31+=J7E;var n31=Q5b;n31+=d2v.J2w;n31+=G1E;n31+=f1E;var p31=Z8w;p31+=A7w;p31+=w8E;p31+=d2v.b2w;var g31=E8E;g31+=s8E;g31+=z8E;var f31=I7w;f31+=Z8E;var u31=R1E;u31+=d2v.w7w;u31+=X7w;u31+=d2v.b2w;var y31=v4E;y31+=d8E;y31+=A7w;y31+=C6b;var d31=l5w;d31+=R4b;var Z31=y8E;Z31+=u8E;Z31+=O8E;var z31=E6w;z31+=L0b;var s31=A7w;s31+=f7w;s31+=d2v.b2w;var that=this;var i,iLen,eventRet,errorNodes;var changed=o4h,allData={},changedData={};var setBuilder=DataTable[s31][z31][Z31];var dataSource=this[D7w][D8E];var fields=this[D7w][Z1b];var editCount=this[D7w][M8E];var modifier=this[D7w][d31];var editFields=this[D7w][y31];var editData=this[D7w][l8E];var opts=this[D7w][K0b];var changedSubmit=opts[u31];var submitParamsLocal;var action=this[D7w][m4b];var submitParams={"action":action,"data":{}};if(this[D7w][M4E]){var O31=N8E;O31+=a7w;O31+=n6w;submitParams[f8h]=this[D7w][O31];}if(action===q2b||action===e7b){var W31=X8E;W31+=d2v.E7w;var I31=o8E;I31+=A7w;$[i7h](editFields,function(idSrc,edit){var n8E="isEmptyObject";var W8E="ptyObj";var I8E="Em";var o31=F7h;o31+=I8E;o31+=W8E;o31+=d5b;var allRowData={};var changedRowData={};$[i7h](fields,function(name,field){var g8E=/\[.*$/;var x8E="From";var F8E="multiGet";var G8E="unt";var K8E="any-co";var A8E="compar";var T8E="submittable";if(edit[Z1b][name]&&field[T8E]()){var X31=A8E;X31+=A7w;var l31=f4h;l31+=d2v.w7w;l31+=K8E;l31+=G8E;var M31=f8E;M31+=t9w;var multiGet=field[F8E]();var builder=setBuilder(name);if(multiGet[idSrc]===undefined){var D31=a3h;D31+=x8E;D31+=A1h;D31+=d2v.J2w;var originalVal=field[D31](edit[r5w]);builder(allRowData,originalVal);return;}var value=multiGet[idSrc];var manyBuilder=$[t4b](value)&&name[F1E](M31)!==-N2w?setBuilder(name[S7h](g8E,Y9w)+l31):F3h;builder(allRowData,value);if(manyBuilder){var N31=X6w;N31+=u0w;N31+=p8E;manyBuilder(allRowData,value[N31]);}if(action===J0b&&(!editData[name]||!field[X31](value,editData[name][idSrc]))){builder(changedRowData,value);changed=I4h;if(manyBuilder){manyBuilder(changedRowData,value[X4h]);}}}});if(!$[o31](allRowData)){allData[idSrc]=allRowData;}if(!$[n8E](changedRowData)){changedData[idSrc]=changedRowData;}});if(action===I31||changedSubmit===q8E||changedSubmit===v8E&&changed){submitParams[r5w]=allData;}else if(changedSubmit===W31&&changed){submitParams[r5w]=changedData;}else{var K31=E6w;K31+=C8E;var A31=r0h;A31+=A7w;var T31=E6w;T31+=i8E;T31+=B8E;T31+=q6w;this[D7w][m4b]=F3h;if(opts[T31]===A31&&(hide===undefined||hide)){this[g3b](o4h);}else if(typeof opts[K31]===d2v.d7w){var G31=E6w;G31+=l7w;G31+=r8E;G31+=A7w;opts[G31](this);}if(successCallback){successCallback[E6h](this);}this[v0b](o4h);this[P5b](H8E);return;}}else if(action===f31){var F31=A7w;F31+=d2v.J2w;F31+=d2v.h2w;F31+=k0w;$[F31](editFields,function(idSrc,edit){var x31=N1b;x31+=j0h;submitParams[r5w][idSrc]=edit[x31];});}this[g31](X7E,action,submitParams);submitParamsLocal=$[v3h](I4h,{},submitParams);if(formatdata){formatdata(submitParams);}if(this[p31](k8E,[submitParams,action])===o4h){this[v0b](o4h);return;}var submitWire=this[D7w][K1b]||this[D7w][n31]?this[a4E]:this[q31];submitWire[v31](this,submitParams,function(json,notGood,xhr){var L8E="_submitSuccess";var C31=n0b;C31+=l7w;that[L8E](json,notGood,submitParams,submitParamsLocal,that[D7w][C31],editCount,hide,successCallback,errorCallback,xhr);},function(xhr,err,thrown){var Y8E="itError";var B31=B2b;B31+=X7w;B31+=k3h;var i31=P6w;i31+=Y8E;that[i31](xhr,err,thrown,errorCallback,submitParams,that[D7w][B31]);},submitParams);};Editor[w2h][r31]=function(data,success,error,submitParams){var a8E="taS";var R8E="ual";var c8E="eac";var V8E="jectDataFn";var m8E="_fnGetOb";var j8E="dSr";var Q31=I7w;Q31+=A7w;Q31+=d2v.w7w;Q31+=Q8E;var Y31=E6w;Y31+=l6w;Y31+=X7w;var L31=X7w;L31+=j8E;L31+=d2v.h2w;var k31=m8E;k31+=V8E;var H31=A7w;H31+=f7w;H31+=d2v.b2w;var that=this;var action=data[m4b];var out={data:[]};var idGet=DataTable[H31][j1h][k31](this[D7w][L31]);var idSet=DataTable[Q1h][Y31][S1h](this[D7w][N4E]);if(action!==Q31){var a31=c8E;a31+=k0w;var R31=B1b;R31+=R8E;var c31=F5h;c31+=d2v.J2w;c31+=a8E;c31+=t7b;var V31=f5w;V31+=A7w;V31+=t5w;V31+=D7w;var m31=d2v.w7w;m31+=d2v.J2w;m31+=X7w;m31+=l7w;var j31=d2v.w7w;j31+=E6w;j31+=d2v.E7w;j31+=A7w;var originalData=this[D7w][j31]===m31?this[D0b](V31,this[Y2b]()):this[c31](R31,this[Y2b]());$[a31](data[r5w],function(key,vals){var P8E="_fnExtend";var S8E="Ext";var S31=e4b;S31+=S8E;var toSave;var extender=$[s8h][S31][j1h][P8E];if(action===J0b){var P31=d2v.E7w;P31+=d2v.J2w;P31+=j0h;var rowData=originalData[key][P31];toSave=extender({},rowData,I4h);toSave=extender(toSave,vals,I4h);}else{toSave=extender({},vals,I4h);}var overrideId=idGet(toSave);if(action===f3h&&overrideId===undefined){idSet(toSave,+new Date()+Y9w+key);}else{idSet(toSave,overrideId);}out[r5w][D4h](toSave);});}success(out);};Editor[e31][U31]=function(json,notGood,submitParams,submitParamsLocal,action,editCount,hide,successCallback,errorCallback,xhr){var Y6E="nCom";var L6E="func";var k6E='commit';var H6E="ids";var r6E='preRemove';var B6E="postRe";var C6E="preEdi";var v6E="post";var q6E='postCreate';var n6E="Cre";var p6E="reate";var g6E='setData';var x6E='prep';var F6E="omm";var G6E="editCou";var K6E="bmitSuccess";var O6E="eldErrors";var u6E="br";var y6E="successful";var d6E="tUn";var s6E="ieldErro";var E6E="_legacyAjax";var w6E="tOp";var J8E="ceive";var h8E="tS";var t8E="pos";var U8E="ces";var e8E="_pro";var w71=Z8w;w71+=A7w;w71+=z9w;w71+=q9b;var J21=e8E;J21+=U8E;J21+=D7w;J21+=J4E;var s21=A7w;s21+=O8b;var w21=A7w;w21+=x4h;w21+=E6w;w21+=I7w;var J31=t8E;J31+=h8E;J31+=J8w;J31+=B0b;var b31=Z8w;b31+=b8E;b31+=A7w;b31+=q5w;var h31=U4h;h31+=J8E;var t31=e8w;t31+=X7w;t31+=w6E;t31+=o8h;var that=this;var setData;var fields=this[D7w][Z1b];var opts=this[D7w][t31];var modifier=this[D7w][Y2b];this[E6E](h31,action,json);this[b31](J31,[json,submitParams,action,xhr]);if(!json[w21]){json[Y2h]=d2v.Z7w;}if(!json[I9b]){var E21=Y0w;E21+=s6E;E21+=z6E;json[E21]=[];}if(notGood||json[s21]||json[I9b][X4h]){var F21=Z6E;F21+=d6E;F21+=y6E;var f21=Z8w;f21+=A7w;f21+=L6b;f21+=q5w;var G21=c4h;G21+=u6E;G21+=L4h;var K21=h6b;K21+=E6w;K21+=J4b;var y21=f5w;y21+=O6E;var d21=d8b;d21+=e5h;var z21=J0w;z21+=I7w;z21+=E6w;z21+=I7w;var globalError=[];if(json[z21]){var Z21=M8b;Z21+=E6w;Z21+=I7w;globalError[D4h](json[Z21]);}$[d21](json[y21],function(i,err){var A6E=': ';var T6E="Erro";var W6E="onFieldError";var I6E="rapp";var o6E="focu";var X6E="nFieldEr";var N6E="cu";var l6E="eldErr";var M6E="onFi";var D6E="functio";var u21=k5w;u21+=L5w;u21+=A7w;u21+=d2v.E7w;var field=fields[err[k1h]];if(field[u21]()){var D21=k8w;D21+=I7w;D21+=I7w;D21+=g4h;var O21=A7w;O21+=I7w;O21+=x8w;field[O21](err[T9b]||D21);if(i===l2w){var T21=D6E;T21+=l7w;var W21=M6E;W21+=l6E;W21+=g4h;var l21=Y0w;l21+=E6w;l21+=N6E;l21+=D7w;var M21=E6w;M21+=X6E;M21+=x8w;if(opts[M21]===l21){var I21=o6E;I21+=D7w;var o21=i7w;o21+=Q6b;o21+=X7w;o21+=S2E;var X21=Y6w;X21+=I6E;X21+=A7w;X21+=I7w;var N21=d2v.E7w;N21+=E6w;N21+=d2v.w7w;that[p0h]($(that[N21][Y1E],that[D7w][X21]),{scrollTop:$(field[M6b]())[o21]()[R9h]},R2w);field[I21]();}else if(typeof opts[W21]===T21){opts[W6E](that,err);}}}else{var A21=T6E;A21+=I7w;globalError[D4h](field[k1h]()+A6E+(err[T9b]||A21));}});this[Y2h](globalError[K21](G21));this[f21](F21,[json]);if(errorCallback){var x21=d2v.h2w;x21+=d2v.J2w;x21+=h3h;errorCallback[x21](that,json);}}else{var b21=G1b;b21+=K6E;var h21=I0w;h21+=z9w;h21+=q9b;var R21=G6E;R21+=l7w;R21+=d2v.b2w;var Q21=I7w;Q21+=Z8E;var g21=f6E;g21+=A7w;g21+=s8w;var store={};if(json[r5w]&&(action===g21||action===e7b)){var Y21=d2v.h2w;Y21+=F6E;Y21+=E0w;var p21=n6w;p21+=s5b;this[D0b](x6E,action,modifier,submitParamsLocal,json,store);for(var i=l2w;i<json[r5w][p21];i++){var B21=A7w;B21+=K8b;var q21=V1E;q21+=l7w;q21+=d2v.b2w;var n21=X7w;n21+=d2v.E7w;setData=json[r5w][i];var id=this[D0b](n21,setData);this[q21](g6E,[json,setData,action]);if(action===q2b){var i21=Z8w;i21+=b8E;i21+=u0w;i21+=d2v.b2w;var C21=d2v.h2w;C21+=p6E;var v21=t6w;v21+=A7w;v21+=n6E;v21+=s8w;this[P5b](v21,[json,setData,id]);this[D0b](C21,fields,setData,store);this[i21]([f3h,q6E],[json,setData,id]);}else if(action===B21){var L21=v6E;L21+=c9w;var k21=S7E;k21+=u0w;k21+=d2v.b2w;var H21=A7w;H21+=k5w;H21+=d2v.b2w;var r21=C6E;r21+=d2v.b2w;this[P5b](r21,[json,setData,id]);this[D0b](H21,modifier,fields,setData,store);this[k21]([J0b,L21],[json,setData,id]);}}this[D0b](Y21,action,modifier,json[r5w],store);}else if(action===Q21){var c21=F5h;c21+=i6E;var V21=B6E;V21+=d2v.w7w;V21+=z0b;V21+=A7w;var m21=U4h;m21+=E5b;m21+=A7w;var j21=V1E;j21+=l7w;j21+=d2v.b2w;this[D0b](x6E,action,modifier,submitParamsLocal,json,store);this[j21](r6E,[json,this[H6E]()]);this[D0b](m21,modifier,fields,store);this[P5b]([z5b,V21],[json,this[H6E]()]);this[c21](k6E,action,modifier,json[r5w],store);}if(editCount===this[D7w][R21]){var U21=L6E;U21+=S2E;var e21=E6w;e21+=Y6E;e21+=Q6E;e21+=q6w;var P21=F1b;P21+=G1h;var S21=k3h;S21+=j6E;S21+=m6E;var a21=B2b;a21+=X7w;a21+=k3h;this[D7w][a21]=F3h;if(opts[S21]===P21&&(hide===undefined||hide)){this[g3b](json[r5w]?I4h:o4h);}else if(typeof opts[e21]===U21){var t21=k3h;t21+=j6E;t21+=i7w;t21+=U4E;opts[t21](this);}}if(successCallback){successCallback[E6h](that,json);}this[h21](b21,[json,setData,action]);}this[J21](o4h);this[w71](H8E,[json,setData,action]);};Editor[E71][V6E]=function(xhr,err,thrown,errorCallback,submitParams,action){var P6E="system";var S6E="Submi";var R6E="tErro";var c6E="submitComp";var u71=c6E;u71+=U4E;var y71=Z6E;y71+=R6E;y71+=I7w;var Z71=a6E;Z71+=l7w;var z71=s7b;z71+=d7h;z71+=S6E;z71+=d2v.b2w;var s71=S7E;s71+=u0w;s71+=d2v.b2w;this[s71](z71,[F3h,submitParams,action,xhr]);this[Y2h](this[Z71][Y2h][P6E]);this[v0b](o4h);if(errorCallback){var d71=t3h;d71+=X6w;d71+=X6w;errorCallback[d71](this,xhr,err,thrown);}this[P5b]([y71,u71],[xhr,err,thrown,submitParams]);};Editor[w2h][V1b]=function(fn){var U6E="ures";var e6E="ocess";var I71=X7w;I71+=l7w;I71+=C8b;var o71=k3E;o71+=I6h;var M71=t6w;M71+=e6E;M71+=J4b;M71+=c8w;var O71=Y0w;O71+=l7w;var that=this;var dt=this[D7w][f8h]?new $[O71][e4b][L0b](this[D7w][f8h]):F3h;var ssp=o4h;if(dt){var D71=E6w;D71+=p8w;D71+=x4E;D71+=U6E;ssp=dt[y6h]()[l2w][D71][t6E];}if(this[D7w][M71]){var l71=Z6E;l71+=d2v.b2w;l71+=j6E;l71+=m6E;this[h8h](l71,function(){var h6E="dra";if(ssp){var X71=h6E;X71+=Y6w;var N71=E6w;N71+=l7w;N71+=A7w;dt[N71](X71,fn);}else{setTimeout(function(){fn();},A2w);}});return I4h;}else if(this[o71]()===I71||this[u7h]()===R1b){var K71=Q5w;K71+=j5w;var W71=r0h;W71+=A7w;this[h8h](W71,function(){if(!that[D7w][K3h]){setTimeout(function(){if(that[D7w]){fn();}},A2w);}else{var T71=k3h;T71+=A7w;that[T71](H8E,function(e,json){var b6E='draw';if(ssp&&json){var A71=E6w;A71+=l7w;A71+=A7w;dt[A71](b6E,fn);}else{setTimeout(function(){if(that[D7w]){fn();}},A2w);}});}})[K71]();return I4h;}return o4h;};Editor[G71][f71]=function(name,arr){for(var i=l2w,ien=arr[X4h];i<ien;i++){if(name==arr[i]){return i;}}return-N2w;};Editor[C1h]={"table":F3h,"ajaxUrl":F3h,"fields":[],"display":J6E,"ajax":F3h,"idSrc":F71,"events":{},"i18n":{"create":{"button":x71,"title":g71,"submit":p71},"edit":{"button":n71,"title":q71,"submit":w0E},"remove":{"button":v71,"title":E0E,"submit":C71,"confirm":{"_":s0E,"1":z0E}},"error":{"system":Z0E},multi:{title:i71,info:d0E,restore:y0E,noMulti:B71},"datetime":{previous:r71,next:H71,months:[u0E,k71,L71,Y71,O0E,Q71,j71,m71,D0E,M0E,V71,c71],weekdays:[l0E,R71,a71,S71,N0E,X0E,P71],amPm:[e71,o0E],unknown:w0b}},formOptions:{bubble:$[v3h]({},Editor[Z6h][U71],{title:o4h,message:o4h,buttons:t71,submit:I0E}),inline:$[v3h]({},Editor[Z6h][D6h],{buttons:o4h,submit:h71}),main:$[v3h]({},Editor[Z6h][b71])},legacyAjax:o4h};(function(){var h5E="dataSrc";var r5E='keyless';var f5E="cancelled";var X5E="any";var y5E="Src";var s5E="_fnGetObjectDataFn";var h0E="um";var T0E="awTyp";var Y81=Y1h;Y81+=d2v.J2w;Y81+=b8w;Y81+=W0E;var J71=d2v.E7w;J71+=i6E;J71+=D7w;var __dataSources=Editor[J71]={};var __dtIsSsp=function(dt,editor){var G0E="oFeatures";var K0E="setti";var A0E="editOp";var s81=d2v.E7w;s81+=I7w;s81+=T0E;s81+=A7w;var E81=A0E;E81+=d2v.b2w;E81+=D7w;var w81=K0E;w81+=M4h;w81+=D7w;return dt[w81]()[l2w][G0E][t6E]&&editor[D7w][E81][s81]!==K2h;};var __dtApi=function(table){var z81=f0E;z81+=d2v.J2w;z81+=b7w;return $(table)[z81]();};var __dtHighlight=function(node){node=$(node);setTimeout(function(){var F0E="ighligh";var Z81=k0w;Z81+=F0E;Z81+=d2v.b2w;node[e3b](Z81);setTimeout(function(){var n0E="light";var p0E="noHigh";var g0E="lig";var x0E="hig";var a2w=550;var y81=x0E;y81+=k0w;y81+=g0E;y81+=M7h;var d81=p0E;d81+=n0E;node[e3b](d81)[Q2h](y81);setTimeout(function(){var C0E="veClass";var v0E="ight";var q0E="High";var O81=p3h;O81+=q0E;O81+=X6w;O81+=v0E;var u81=U4h;u81+=b4b;u81+=C0E;node[u81](O81);},a2w);},R2w);},g2w);};var __dtRowSelector=function(out,dt,identifier,fields,idFn){var M81=A7w;M81+=d2v.J2w;M81+=e5h;var D81=s0w;D81+=i0E;dt[D81](identifier)[B0E]()[M81](function(idx){var r0E='Unable to find row identifier';var F2w=14;var N81=I7w;N81+=E6w;N81+=Y6w;var l81=l7w;l81+=E6w;l81+=d2v.E7w;l81+=A7w;var row=dt[a4b](idx);var data=row[r5w]();var idSrc=idFn(data);if(idSrc===undefined){Editor[Y2h](r0E,F2w);}out[idSrc]={idSrc:idSrc,data:data,node:row[l81](),fields:fields,type:N81};});};var __dtFieldsFromIdx=function(dt,fields,idx){var a0E="ease specify the field name.";var R0E="ld from source. Pl";var c0E="o automatically determine fie";var V0E="Unable t";var m0E="mData";var j0E="tings";var Q0E="olum";var Y0E="aoC";var L0E="ditFiel";var k0E="yObject";var H0E="Empt";var G81=F7h;G81+=H0E;G81+=k0E;var W81=e7b;W81+=v1h;var I81=A7w;I81+=L0E;I81+=d2v.E7w;var o81=Y0E;o81+=Q0E;o81+=l7w;o81+=D7w;var X81=G1h;X81+=d2v.b2w;X81+=j0E;var field;var col=dt[X81]()[l2w][o81][idx];var dataSrc=col[I81]!==undefined?col[W81]:col[m0E];var resolvedFields={};var run=function(field,dataSrc){var T81=l7w;T81+=d2v.J2w;T81+=x7w;if(field[T81]()===dataSrc){resolvedFields[field[k1h]()]=field;}};$[i7h](fields,function(name,fieldInst){var A81=F7h;A81+=j3E;A81+=I7w;A81+=o2h;if($[A81](dataSrc)){var K81=n6w;K81+=M4h;K81+=l4h;for(var i=l2w;i<dataSrc[K81];i++){run(fieldInst,dataSrc[i]);}}else{run(fieldInst,dataSrc);}});if($[G81](resolvedFields)){var f81=V0E;f81+=c0E;f81+=R0E;f81+=a0E;Editor[Y2h](f81,K2w);}return resolvedFields;};var __dtCellSelector=function(out,dt,identifier,allFields,idFn,forceFields){var P0E="ell";var S0E="index";var x81=S0E;x81+=E4h;var F81=d2v.h2w;F81+=P0E;F81+=D7w;dt[F81](identifier)[x81]()[i7h](function(idx){var J0E="attach";var b0E="ttach";var t0E="col";var U0E="nodeNam";var e0E="ttac";var H81=A7w;H81+=Y7w;H81+=l7w;H81+=d2v.E7w;var r81=l7w;r81+=E6w;r81+=d2v.E7w;r81+=A7w;var B81=i7w;B81+=P1b;B81+=k0w;var i81=d2v.J2w;i81+=e0E;i81+=k0w;var C81=I7w;C81+=S3b;var q81=U0E;q81+=A7w;var n81=t0E;n81+=h0E;n81+=l7w;var p81=d2v.E7w;p81+=K0h;p81+=d2v.J2w;var g81=I7w;g81+=E6w;g81+=Y6w;var cell=dt[j0w](idx);var row=dt[g81](idx[a4b]);var data=row[p81]();var idSrc=idFn(data);var fields=forceFields||__dtFieldsFromIdx(dt,allFields,idx[n81]);var isNode=typeof identifier===N4h&&identifier[q81]||identifier instanceof $;var prevDisplayFields,prevAttach;if(out[idSrc]){var v81=d2v.J2w;v81+=b0E;prevAttach=out[idSrc][v81];prevDisplayFields=out[idSrc][m8b];}__dtRowSelector(out,dt,idx[C81],allFields,idFn);out[idSrc][J0E]=prevAttach||[];out[idSrc][i81][B81](isNode?$(identifier)[m0h](l2w):cell[r81]());out[idSrc][m8b]=prevDisplayFields||{};$[H81](out[idSrc][m8b],fields);});};var __dtColumnSelector=function(out,dt,identifier,fields,idFn){var k81=j0w;k81+=D7w;dt[k81](F3h,identifier)[B0E]()[i7h](function(idx){__dtCellSelector(out,dt,idx,fields,idFn);});};var __dtjqId=function(id){var E5E='\\$1';var w5E="lace";var L81=j7h;L81+=w5E;return typeof id===R7h?F8b+id[L81](/(:|\.|\[|\]|,)/g,E5E):F8b+id;};__dataSources[Y81]={id:function(data){var Q81=J1h;Q81+=T7w;Q81+=z1E;var idFn=DataTable[Q1h][j1h][s5E](this[D7w][Q81]);return idFn(data);},individual:function(identifier,fieldNames){var d5E="oA";var Z5E="tDataFn";var z5E="_fnGetObjec";var V81=F5w;V81+=t5w;V81+=D7w;var m81=z5E;m81+=Z5E;var j81=d5E;j81+=i7w;j81+=X7w;var idFn=DataTable[Q1h][j81][m81](this[D7w][N4E]);var dt=__dtApi(this[D7w][f8h]);var fields=this[D7w][V81];var out={};var forceFields;var responsiveNode;if(fieldNames){var c81=F7h;c81+=y6w;c81+=c6w;if(!$[c81](fieldNames)){fieldNames=[fieldNames];}forceFields={};$[i7h](fieldNames,function(i,name){forceFields[name]=fields[name];});}__dtCellSelector(out,dt,identifier,fields,idFn,forceFields);return out;},fields:function(identifier){var l5E="column";var D5E="ows";var O5E="ells";var u5E="cells";var P81=I2h;P81+=X6w;P81+=h0E;P81+=i5w;var S81=I7w;S81+=S3b;S81+=D7w;var a81=Y0w;a81+=G7w;var R81=J1h;R81+=y5E;var idFn=DataTable[Q1h][j1h][s5E](this[D7w][R81]);var dt=__dtApi(this[D7w][f8h]);var fields=this[D7w][a81];var out={};if($[c1b](identifier)&&(identifier[S81]!==undefined||identifier[P81]!==undefined||identifier[u5E]!==undefined)){var t81=d2v.h2w;t81+=O5E;if(identifier[F7b]!==undefined){var e81=I7w;e81+=D5E;__dtRowSelector(out,dt,identifier[e81],fields,idFn);}if(identifier[M5E]!==undefined){var U81=l5E;U81+=D7w;__dtColumnSelector(out,dt,identifier[U81],fields,idFn);}if(identifier[t81]!==undefined){var h81=d2v.h2w;h81+=T8w;h81+=m5w;__dtCellSelector(out,dt,identifier[h81],fields,idFn);}}else{__dtRowSelector(out,dt,identifier,fields,idFn);}return out;},create:function(fields,data){var dt=__dtApi(this[D7w][f8h]);if(!__dtIsSsp(dt,this)){var w61=l7w;w61+=E6w;w61+=d2v.E7w;w61+=A7w;var J81=d2v.J2w;J81+=Z9h;var b81=I7w;b81+=E6w;b81+=Y6w;var row=dt[b81][J81](data);__dtHighlight(row[w61]());}},edit:function(identifier,fields,data,store){var K5E="rowIds";var A5E="dataTableExt";var T5E="_fnE";var o5E="wIds";var N5E="aTable";var s61=l7w;s61+=E6w;s61+=z5w;var E61=d2v.E7w;E61+=I7w;E61+=T0E;E61+=A7w;var that=this;var dt=__dtApi(this[D7w][f8h]);if(!__dtIsSsp(dt,this)||this[D7w][K0b][E61]===s61){var u61=d2v.J2w;u61+=l7w;u61+=K5w;var d61=d2v.h2w;d61+=d2v.J2w;d61+=X6w;d61+=X6w;var Z61=X7w;Z61+=d2v.E7w;var z61=Y1h;z61+=N5E;var rowId=__dataSources[z61][Z61][d61](this,data);var row;try{var y61=I7w;y61+=E6w;y61+=Y6w;row=dt[y61](__dtjqId(rowId));}catch(e){row=dt;}if(!row[u61]()){row=dt[a4b](function(rowIdx,rowData,rowNode){var D61=B4E;D61+=X6w;var O61=X7w;O61+=d2v.E7w;return rowId==__dataSources[e4b][O61][D61](that,rowData);});}if(row[X5E]()){var I61=s0w;I61+=o5E;var o61=I5E;o61+=W5E;var X61=N1b;X61+=d2v.b2w;X61+=d2v.J2w;var N61=T5E;N61+=Y7w;N61+=l7w;N61+=d2v.E7w;var l61=E6w;l61+=y6w;l61+=i7w;l61+=X7w;var M61=Y0w;M61+=l7w;var extender=$[M61][A5E][l61][N61];var toSave=extender({},row[r5w](),I4h);toSave=extender(toSave,data,I4h);row[X61](toSave);var idx=$[o61](rowId,store[K5E]);store[I61][G5E](idx,N2w);}else{row=dt[a4b][r2h](data);}__dtHighlight(row[M6b]());}},remove:function(identifier,fields,store){var F5E="every";var W61=j0h;W61+=a7w;W61+=n6w;var that=this;var dt=__dtApi(this[D7w][W61]);var cancelled=store[f5E];if(cancelled[X4h]===l2w){dt[F7b](identifier)[M8h]();}else{var x61=x5h;x61+=A7w;var F61=a4b;F61+=D7w;var T61=I7w;T61+=E6w;T61+=Y6w;T61+=D7w;var indexes=[];dt[T61](identifier)[F5E](function(){var G61=J4b;G61+=G2b;var K61=N1b;K61+=d2v.b2w;K61+=d2v.J2w;var A61=t3h;A61+=h3h;var id=__dataSources[e4b][J1h][A61](that,this[K61]());if($[G61](id,cancelled)===-N2w){var f61=J4b;f61+=v7w;f61+=f7w;indexes[D4h](this[f61]());}});dt[F61](indexes)[x61]();}},prep:function(action,identifier,submit,json,store){var g61=e8w;g61+=E0w;if(action===g61){var p61=I7w;p61+=S3b;p61+=x5E;p61+=k2b;var cancelled=json[f5E]||[];store[p61]=$[f8b](submit[r5w],function(val,key){var n5E="mptyObje";var p5E="isE";var g5E="inA";var v61=g5E;v61+=c6w;var q61=d2v.E7w;q61+=d2v.J2w;q61+=j0h;var n61=p5E;n61+=n5E;n61+=K8h;return!$[n61](submit[q61][key])&&$[v61](key,cancelled)===-N2w?key:undefined;});}else if(action===z5b){store[f5E]=json[f5E]||[];}},commit:function(action,identifier,data,store){var B5E="draw";var i5E="drawType";var C5E="owId";var v5E="tab";var q5E="wI";var V61=p3h;V61+=l7w;V61+=A7w;var r61=n6w;r61+=l7w;r61+=e8h;r61+=k0w;var B61=s0w;B61+=q5E;B61+=k2b;var i61=e8w;i61+=X7w;i61+=d2v.b2w;var C61=v5E;C61+=X6w;C61+=A7w;var that=this;var dt=__dtApi(this[D7w][C61]);if(action===i61&&store[B61][r61]){var Q61=n6w;Q61+=l7w;Q61+=p8E;var H61=I7w;H61+=C5E;H61+=D7w;var ids=store[H61];var row;var compare=function(id){return function(rowIdx,rowData,rowNode){var Y61=B4E;Y61+=X6w;var L61=X7w;L61+=d2v.E7w;var k61=Y1h;k61+=d2v.J2w;k61+=b8w;k61+=W0E;return id==__dataSources[k61][L61][Y61](that,rowData);};};for(var i=l2w,ien=ids[Q61];i<ien;i++){var j61=d2v.J2w;j61+=l7w;j61+=K5w;try{row=dt[a4b](__dtjqId(ids[i]));}catch(e){row=dt;}if(!row[j61]()){var m61=I7w;m61+=S3b;row=dt[m61](compare(ids[i]));}if(row[X5E]()){row[M8h]();}}}var drawType=this[D7w][K0b][i5E];if(drawType!==V61){dt[B5E](drawType);}}};function __html_id(identifier){var L5E='Could not find an element with `data-editor-id` or `id` of: ';var k5E="id=\"";var H5E="[data-editor";var context=document;if(identifier!==r5E){var R61=U9w;R61+=t9w;var c61=H5E;c61+=f4h;c61+=k5E;context=$(c61+identifier+R61);if(context[X4h]===l2w){var a61=D7w;a61+=d2v.b2w;a61+=N6w;a61+=M4h;context=typeof identifier===a61?$(__dtjqId(identifier)):$(identifier);}if(context[X4h]===l2w){throw L5E+identifier;}}return context;}function __html_el(identifier,name){var m5E="eld=\"";var j5E="ditor-fi";var Q5E="ata-e";var Y5E="[d";var S61=Y5E;S61+=Q5E;S61+=j5E;S61+=m5E;var context=__html_id(identifier);return $(S61+name+G3E,context);}function __html_els(identifier,names){var out=$();for(var i=l2w,ien=names[X4h];i<ien;i++){var P61=d2v.J2w;P61+=Z9h;out=out[P61](__html_el(identifier,names[i]));}return out;}function __html_get(identifier,dataSrc){var S5E="-value]";var a5E="[data-edi";var c5E="r-value";var V5E="data-edito";var b61=k0w;b61+=d2v.b2w;b61+=d2v.w7w;b61+=X6w;var h61=V5E;h61+=c5E;var t61=d2v.J2w;t61+=R5E;var U61=X6w;U61+=A7w;U61+=M4h;U61+=l4h;var e61=a5E;e61+=b5w;e61+=S5E;var el=__html_el(identifier,dataSrc);return el[P5E](e61)[U61]?el[t61](h61):el[b61]();}function __html_set(identifier,fields,data){var J61=A7w;J61+=V7h;J61+=k0w;$[J61](fields,function(name,field){var J5E='data-editor-value';var U5E="alue]";var e5E="[data-editor-v";var val=field[X1b](data);if(val!==undefined){var E01=e5E;E01+=U5E;var w01=f5w;w01+=t5E;var el=__html_el(identifier,field[h5E]());if(el[w01](E01)[X4h]){var s01=d2v.J2w;s01+=b5E;s01+=I7w;el[s01](J5E,val);}else{var z01=A7w;z01+=V7h;z01+=k0w;el[z01](function(){var s9E="firstChild";var E9E="removeChild";var w9E="dNode";var d01=P8h;d01+=p8E;var Z01=I4b;Z01+=w9E;Z01+=D7w;while(this[Z01][d01]){this[E9E](this[s9E]);}})[o7h](val);}}});}__dataSources[o7h]={id:function(data){var Z9E="bjectDataFn";var z9E="_fnG";var O01=J1h;O01+=y5E;var u01=z9E;u01+=M5w;u01+=q7b;u01+=Z9E;var y01=A7w;y01+=f7w;y01+=d2v.b2w;var idFn=DataTable[y01][j1h][u01](this[D7w][O01]);return idFn(data);},initField:function(cfg){var y9E='[data-editor-label="';var d9E="na";var M01=d9E;M01+=d2v.w7w;M01+=A7w;var D01=d2v.E7w;D01+=d2v.J2w;D01+=j0h;var label=$(y9E+(cfg[D01]||cfg[M01])+G3E);if(!cfg[y1h]&&label[X4h]){var l01=X6w;l01+=W5b;l01+=T8w;cfg[l01]=label[o7h]();}},individual:function(identifier,fieldNames){var W9E='Cannot automatically determine field name from data source';var I9E="keyles";var o9E='editor-id';var X9E='[data-editor-id]';var N9E="addBack";var l9E='data-editor-field';var M9E="dBa";var O9E="Name";var u9E="isAr";var G01=Y0w;G01+=g0b;G01+=d2v.E7w;G01+=D7w;var K01=k0w;K01+=F7E;var A01=A7h;A01+=l4h;var T01=u9E;T01+=I7w;T01+=d2v.J2w;T01+=K5w;var N01=M6b;N01+=O9E;var attachEl;if(identifier instanceof $||identifier[N01]){var I01=i7w;I01+=D9E;I01+=q5w;I01+=D7w;var o01=d2v.J2w;o01+=d2v.E7w;o01+=M9E;o01+=U8h;attachEl=identifier;if(!fieldNames){var X01=K0h;X01+=d2v.b2w;X01+=I7w;fieldNames=[$(identifier)[X01](l9E)];}var back=$[s8h][N9E]?o01:K6b;identifier=$(identifier)[I01](X9E)[back]()[r5w](o9E);}if(!identifier){var W01=I9E;W01+=D7w;identifier=W01;}if(fieldNames&&!$[T01](fieldNames)){fieldNames=[fieldNames];}if(!fieldNames||fieldNames[A01]===l2w){throw W9E;}var out=__dataSources[K01][G01][E6h](this,identifier);var fields=this[D7w][Z1b];var forceFields={};$[i7h](fieldNames,function(i,name){forceFields[name]=fields[name];});$[i7h](out,function(id,set){var K9E='cell';var A9E="toA";var T9E="displayField";var g01=T9E;g01+=D7w;var x01=f5w;x01+=T8w;x01+=k2b;var F01=A9E;F01+=c6w;var f01=d2v.J2w;f01+=d2v.b2w;f01+=d2v.b2w;f01+=w4h;set[h6w]=K9E;set[f01]=attachEl?$(attachEl):__html_els(identifier,fieldNames)[F01]();set[x01]=fields;set[g01]=forceFields;});return out;},fields:function(identifier){var i01=I7w;i01+=E6w;i01+=Y6w;var v01=A7w;v01+=d2v.J2w;v01+=d2v.h2w;v01+=k0w;var q01=F5w;q01+=t5w;q01+=D7w;var n01=E1E;n01+=x4h;n01+=d2v.J2w;n01+=K5w;var p01=k0w;p01+=d2v.b2w;p01+=d2v.w7w;p01+=X6w;var out={};var self=__dataSources[p01];if($[n01](identifier)){for(var i=l2w,ien=identifier[X4h];i<ien;i++){var res=self[Z1b][E6h](this,identifier[i]);out[identifier[i]]=res[identifier[i]];}return out;}var data={};var fields=this[D7w][q01];if(!identifier){identifier=r5E;}$[v01](fields,function(name,field){var f9E="Data";var G9E="valT";var C01=G9E;C01+=E6w;C01+=f9E;var val=__html_get(identifier,field[h5E]());field[C01](data,val===F3h?undefined:val);});out[identifier]={idSrc:identifier,data:data,node:document,fields:fields,type:i01};return out;},create:function(fields,data){if(data){var B01=d2v.h2w;B01+=r1b;B01+=X6w;var id=__dataSources[o7h][J1h][B01](this,data);try{var r01=X6w;r01+=u0w;r01+=c8w;r01+=l4h;if(__html_id(id)[r01]){__html_set(id,fields,data);}}catch(e){}}},edit:function(identifier,fields,data){var F9E="yless";var L01=T4h;L01+=A7w;L01+=F9E;var k01=X7w;k01+=d2v.E7w;var H01=G7E;H01+=X6w;var id=__dataSources[H01][k01][E6h](this,data)||L01;__html_set(id,fields,data);},remove:function(identifier,fields){__html_id(identifier)[M8h]();}};}());Editor[Y01]={"wrapper":z8w,"processing":{"indicator":Q01,"active":j01},"header":{"wrapper":w5h,"content":x9E},"body":{"wrapper":g9E,"content":p9E},"footer":{"wrapper":m01,"content":n9E},"form":{"wrapper":q9E,"content":v9E,"tag":d2v.Z7w,"info":V01,"error":c01,"buttons":R01,"button":a01},"field":{"wrapper":C9E,"typePrefix":S01,"namePrefix":P01,"label":i9E,"input":e01,"inputControl":U01,"error":t01,"msg-label":h01,"msg-error":b01,"msg-message":J01,"msg-info":w51,"multiValue":E51,"multiInfo":B9E,"multiRestore":r9E,"multiNoEdit":H9E,"disabled":N2h,"processing":s51},"actions":{"create":z51,"edit":k9E,"remove":L9E},"inline":{"wrapper":Y9E,"liner":Q9E,"buttons":j9E},"bubble":{"wrapper":Z51,"liner":d51,"table":m9E,"close":y51,"pointer":u51,"bg":O51}};(function(){var h4X="removeSingle";var t4X='selectedSingle';var m4X='rows';var Q4X="formMessage";var Y4X='preOpen';var r4X='buttons-edit';var x4X='buttons-create';var A4X="formButtons";var d4X="select_single";var z4X="ols";var s4X="ON";var E4X="eate";var w4X="editor_";var J9E="or_ed";var b9E="r_rem";var h9E="ito";var t9E="lect";var U9E="ools";var e9E="eT";var P9E="tto";var S9E="elec";var a9E="s-remo";var R9E="ngle";var c9E="editSi";var V9E="itSingle";var a91=A7w;a91+=j0b;a91+=u0w;a91+=d2v.E7w;var R91=I7w;R91+=h3b;R91+=z0b;R91+=A7w;var c91=A7w;c91+=p1h;var V91=e8w;V91+=V9E;var m91=x1h;m91+=t6b;m91+=d2v.E7w;var j91=c9E;j91+=R9E;var n91=U8b;n91+=t8b;n91+=a9E;n91+=L6b;var D91=D7w;D91+=S9E;D91+=d2v.b2w;D91+=e8w;var P51=U8b;P51+=P9E;P51+=i5w;var D51=b8w;D51+=G2h;D51+=e9E;D51+=U9E;if(DataTable[D51]){var C51=G1h;C51+=t9E;var v51=t2b;v51+=d2v.E7w;var q51=e8w;q51+=h9E;q51+=b9E;q51+=Q8E;var K51=A7w;K51+=Y7w;K51+=l7w;K51+=d2v.E7w;var A51=e7b;A51+=J9E;A51+=E0w;var X51=x1h;X51+=d2v.b2w;X51+=u0w;X51+=d2v.E7w;var N51=w4X;N51+=d2v.h2w;N51+=I7w;N51+=E4X;var l51=g4E;l51+=s4X;l51+=T7w;var M51=C9b;M51+=E6w;M51+=z4X;var ttButtons=DataTable[M51][l51];var ttButtonBase={sButtonText:F3h,editor:F3h,formTitle:F3h};ttButtons[N51]=$[X51](I4h,ttButtons[d6h],ttButtonBase,{formButtons:[{label:F3h,fn:function(e){var o51=R1E;o51+=j1E;this[o51]();}}],fnClick:function(button,config){var Z4X="formButto";var T51=d2v.b2w;T51+=X7w;T51+=d2v.b2w;T51+=n6w;var W51=f6E;W51+=d8b;W51+=d2v.b2w;W51+=A7w;var I51=Z4X;I51+=i5w;var editor=config[Y0b];var i18nCreate=editor[S0b][q2b];var buttons=config[I51];if(!buttons[l2w][y1h]){buttons[l2w][y1h]=i18nCreate[q1b];}editor[W51]({title:i18nCreate[T51],buttons:buttons});}});ttButtons[A51]=$[K51](I4h,ttButtons[d4X],ttButtonBase,{formButtons:[{label:F3h,fn:function(e){var G51=X8h;G51+=E0w;this[G51]();}}],fnClick:function(button,config){var O4X="SelectedIndexes";var u4X="fnG";var y4X="formBu";var n51=d2v.b2w;n51+=X7w;n51+=d2v.b2w;n51+=n6w;var g51=y4X;g51+=d2v.b2w;g51+=o7w;g51+=i5w;var x51=X7w;x51+=j8h;x51+=q1h;x51+=l7w;var F51=A7h;F51+=l4h;var f51=u4X;f51+=M5w;f51+=O4X;var selected=this[f51]();if(selected[F51]!==N2w){return;}var editor=config[Y0b];var i18nEdit=editor[x51][e7b];var buttons=config[g51];if(!buttons[l2w][y1h]){var p51=D7w;p51+=J8w;p51+=f1b;p51+=E0w;buttons[l2w][y1h]=i18nEdit[p51];}editor[e7b](selected[l2w],{title:i18nEdit[n51],buttons:buttons});}});ttButtons[q51]=$[v51](I4h,ttButtons[C51],ttButtonBase,{question:F3h,formButtons:[{label:F3h,fn:function(e){var that=this;this[q1b](function(json){var l4X="fnSelectNone";var M4X="bleTools";var D4X="tInstance";var k51=l7w;k51+=Z5w;var H51=f0E;H51+=W0E;var r51=Y0w;r51+=l7w;r51+=q6b;r51+=D4X;var B51=b8w;B51+=d2v.J2w;B51+=M4X;var i51=Y0w;i51+=l7w;var tt=$[i51][e4b][B51][r51]($(that[D7w][f8h])[H51]()[f8h]()[k51]());tt[l4X]();});}}],fnClick:function(button,config){var K4X="bmi";var T4X="fnGetSelectedIndexes";var I4X="trin";var o4X="nfirm";var X4X="firm";var S51=n6w;S51+=M4h;S51+=d2v.b2w;S51+=k0w;var a51=N4X;a51+=z0b;a51+=A7w;var c51=M2h;c51+=X4X;var V51=I2h;V51+=o4X;var m51=M2h;m51+=X4X;var j51=D7w;j51+=I4X;j51+=c8w;var Q51=U4h;Q51+=W4X;var Y51=a6E;Y51+=l7w;var L51=v4E;L51+=o7w;L51+=I7w;var rows=this[T4X]();if(rows[X4h]===l2w){return;}var editor=config[L51];var i18nRemove=editor[Y51][Q51];var buttons=config[A4X];var question=typeof i18nRemove[e0b]===j51?i18nRemove[m51]:i18nRemove[V51][rows[X4h]]?i18nRemove[e0b][rows[X4h]]:i18nRemove[c51][Z8w];if(!buttons[l2w][y1h]){var R51=G1b;R51+=K4X;R51+=d2v.b2w;buttons[l2w][y1h]=i18nRemove[R51];}editor[a51](rows,{message:question[S7h](/%d/g,rows[S51]),title:i18nRemove[D3h],buttons:buttons});}});}var _buttons=DataTable[Q1h][P51];$[v3h](_buttons,{create:{text:function(dt,node,config){var F4X="tons.create";var G4X="ditor";var b51=a7w;b51+=z2b;b51+=E6w;b51+=l7w;var h51=b0b;h51+=d2v.J2w;h51+=d2v.b2w;h51+=A7w;var t51=n1h;t51+=d4E;var U51=A7w;U51+=G4X;var e51=f4X;e51+=F4X;return dt[S0b](e51,config[U51][t51][h51][b51]);},className:x4X,editor:F3h,formButtons:{text:function(editor){var w91=b0b;w91+=s8w;var J51=X7w;J51+=j8h;J51+=d4E;return editor[J51][w91][q1b];},action:function(e){var E91=G1b;E91+=f1b;E91+=X7w;E91+=d2v.b2w;this[E91]();}},formMessage:F3h,formTitle:F3h,action:function(e,dt,node,config){var v4X="uttons";var q4X="formB";var n4X="preO";var p4X="rmMessa";var g4X="formTi";var O91=r2E;O91+=B2E;O91+=A7w;var u91=g4X;u91+=B2E;u91+=A7w;var y91=Y0w;y91+=E6w;y91+=p4X;y91+=X5w;var Z91=n4X;Z91+=i7w;Z91+=A7w;Z91+=l7w;var z91=E6w;z91+=z5w;var s91=q4X;s91+=v4X;var that=this;var editor=config[Y0b];var buttons=config[s91];this[K3h](I4h);editor[z91](Z91,function(){var C4X="process";var d91=C4X;d91+=J4E;that[d91](o4h);})[q2b]({buttons:config[A4X],message:config[y91],title:config[u91]||editor[S0b][q2b][O91]});}},edit:{extend:D91,text:function(dt,node,config){var i4X=".ed";var o91=U8b;o91+=P9E;o91+=l7w;var X91=A7w;X91+=d2v.E7w;X91+=E0w;var N91=X7w;N91+=j8h;N91+=q1h;N91+=l7w;var l91=A3b;l91+=i4X;l91+=X7w;l91+=d2v.b2w;var M91=X7w;M91+=B4X;M91+=l7w;return dt[M91](l91,config[Y0b][N91][X91][o91]);},className:r4X,editor:F3h,formButtons:{text:function(editor){var I91=X7w;I91+=w2b;return editor[I91][e7b][q1b];},action:function(e){this[q1b]();}},formMessage:F3h,formTitle:F3h,action:function(e,dt,node,config){var L4X="lls";var k4X="mBu";var H4X="rmTitle";var x91=d2v.b2w;x91+=X7w;x91+=B2E;x91+=A7w;var F91=A7w;F91+=k5w;F91+=d2v.b2w;var f91=X7w;f91+=B4X;f91+=l7w;var G91=s1h;G91+=H4X;var K91=Z2b;K91+=k4X;K91+=R0b;var A91=P8h;A91+=c8w;A91+=d2v.b2w;A91+=k0w;var T91=X6w;T91+=V2E;var W91=d2v.h2w;W91+=A7w;W91+=L4X;var that=this;var editor=config[Y0b];var rows=dt[F7b]({selected:I4h})[B0E]();var columns=dt[M5E]({selected:I4h})[B0E]();var cells=dt[W91]({selected:I4h})[B0E]();var items=columns[T91]||cells[A91]?{rows:rows,columns:columns,cells:cells}:rows;this[K3h](I4h);editor[h8h](Y4X,function(){that[K3h](o4h);})[e7b](items,{message:config[Q4X],buttons:config[K91],title:config[G91]||editor[f91][F91][x91]});}},remove:{extend:j4X,limitTo:[m4X],text:function(dt,node,config){var V4X=".rem";var p91=n1h;p91+=d4E;var g91=A3b;g91+=V4X;g91+=Q8E;return dt[S0b](g91,config[Y0b][p91][M8h][O6h]);},className:n91,editor:F3h,formButtons:{text:function(editor){var q91=X7w;q91+=j8h;q91+=q1h;q91+=l7w;return editor[q91][M8h][q1b];},action:function(e){this[q1b]();}},formMessage:function(editor,dt){var a4X="dexes";var R4X="emo";var c4X="nfi";var H91=d2v.h2w;H91+=k3h;H91+=f5w;H91+=m9b;var r91=X6w;r91+=A7w;r91+=M4h;r91+=l4h;var B91=d2v.h2w;B91+=E6w;B91+=c4X;B91+=m9b;var i91=I7w;i91+=R4X;i91+=L6b;var C91=X7w;C91+=j8h;C91+=q1h;C91+=l7w;var v91=J4b;v91+=a4X;var rows=dt[F7b]({selected:I4h})[v91]();var i18n=editor[C91][i91];var question=typeof i18n[e0b]===R7h?i18n[B91]:i18n[e0b][rows[X4h]]?i18n[e0b][rows[r91]]:i18n[H91][Z8w];return question[S7h](/%d/g,rows[X4h]);},formTitle:F3h,action:function(e,dt,node,config){var U4X="formTitle";var e4X="itor";var P4X="xes";var S4X="nde";var Q91=r2E;Q91+=d2v.b2w;Q91+=n6w;var Y91=X7w;Y91+=S4X;Y91+=P4X;var L91=s0w;L91+=i0E;var k91=A7w;k91+=d2v.E7w;k91+=e4X;var that=this;var editor=config[k91];this[K3h](I4h);editor[h8h](Y4X,function(){that[K3h](o4h);})[M8h](dt[L91]({selected:I4h})[Y91](),{buttons:config[A4X],message:config[Q4X],title:config[U4X]||editor[S0b][M8h][Q91]});}}});_buttons[j91]=$[m91]({},_buttons[e7b]);_buttons[V91][v3h]=t4X;_buttons[h4X]=$[c91]({},_buttons[R91]);_buttons[h4X][a91]=t4X;}());Editor[S91]={};Editor[b4X]=function(input,opts){var D3X=/[haA]/;var O3X=/[Hhm]|LT|LTS/;var u3X=/[YMD]|L(?!T)|l/;var y3X='editor-dateime-';var d3X='-error';var Z3X='-calendar';var z3X='-date';var s3X='-error"/>';var J1X='-time">';var b1X='-calendar"/>';var h1X='-year"/>';var e1X='<select class="';var P1X='<span/>';var c1X='<button>';var r1X="or datetime: Without momentjs only the format 'YYYY-MM-DD' can be used";var C1X="M-DD";var v1X="YY-";var q1X="-dat";var n1X="-tit";var p1X="div class=\"";var g1X="ft\">";var x1X="-iconL";var F1X="viou";var f1X="v ";var G1X="ht\">";var K1X="-iconRig";var A1X="</button";var T1X="s=";var W1X=" clas";var o1X="el\">";var X1X=" class=";var N1X="<select";var l1X="-month\"/";var M1X="/di";var D1X="-label";var O1X="<div cla";var u1X="div cl";var y1X="utes";var z1X="Time";var s1X="ance";var E1X="nst";var J4X="ppen";var J4J=d2v.J2w;J4J+=J4X;J4J+=d2v.E7w;var b4J=d2v.E7w;b4J+=E6w;b4J+=d2v.w7w;var h4J=A9b;h4J+=I7w;var t4J=d2v.E7w;t4J+=d2v.J2w;t4J+=d2v.b2w;t4J+=A7w;var U4J=D7h;U4J+=i7w;U4J+=e8b;var e4J=J2h;e4J+=J0w;var P4J=d2v.E7w;P4J+=E6w;P4J+=d2v.w7w;var S4J=i9h;S4J+=w1X;var a4J=Z2b;a4J+=C0w;var R4J=Q2b;R4J+=d2v.J2w;R4J+=d2v.b2w;var c4J=C0w;c4J+=e5h;var V4J=Y0w;V4J+=E6w;V4J+=I7w;V4J+=C0w;var m4J=Z8w;m4J+=X7w;m4J+=E1X;m4J+=s1X;var j4J=A1h;j4J+=A7w;j4J+=z1X;var Q4J=f5w;Q4J+=l7w;Q4J+=d2v.E7w;var Y4J=f4h;Y4J+=r2E;Y4J+=x7w;var L4J=Y0w;L4J+=J4b;L4J+=d2v.E7w;var k4J=f4h;k4J+=D3h;var H4J=f5w;H4J+=g1h;var r4J=i4h;r4J+=d2v.E7w;r4J+=X7w;r4J+=B4h;var B4J=i4h;B4J+=Z1X;var i4J=G1h;i4J+=d1X;var C4J=d2v.w7w;C4J+=X7w;C4J+=l7w;C4J+=y1X;var v4J=c4h;v4J+=u1X;v4J+=m4h;var q4J=c4h;q4J+=b1b;q4J+=n0h;q4J+=L4h;var n4J=O1X;n4J+=a4h;var p4J=D1X;p4J+=s3b;var g4J=c4h;g4J+=M1X;g4J+=B4h;var x4J=l1X;x4J+=L4h;var F4J=N1X;F4J+=X1X;F4J+=U9w;var f4J=f4h;f4J+=n4h;f4J+=o1X;var G4J=I1X;G4J+=W1X;G4J+=T1X;G4J+=U9w;var K4J=A1X;K4J+=L4h;var A4J=l7w;A4J+=A7w;A4J+=f7w;A4J+=d2v.b2w;var T4J=c4h;T4J+=C5w;T4J+=l7w;T4J+=L4h;var W4J=K1X;W4J+=G1X;var I4J=w6b;I4J+=f1X;I4J+=B2h;I4J+=t9b;var o4J=h1E;o4J+=F1X;o4J+=D7w;var X4J=x1X;X4J+=A7w;X4J+=g1X;var N4J=c4h;N4J+=p1X;var l4J=n1X;l4J+=n6w;l4J+=s3b;var M4J=c4h;M4J+=u1X;M4J+=m4h;var D4J=q1X;D4J+=A7w;D4J+=s3b;var O4J=W6h;O4J+=l1h;O4J+=a4h;var U91=p7w;U91+=v1X;U91+=w6w;U91+=C1X;var e91=d2v.w7w;e91+=C4h;e91+=A7w;e91+=q5w;var P91=x1h;P91+=d2v.b2w;P91+=u0w;P91+=d2v.E7w;this[d2v.h2w]=$[P91](I4h,{},Editor[b4X][C1h],opts);var classPrefix=this[d2v.h2w][i1X];var i18n=this[d2v.h2w][S0b];if(!window[e91]&&this[d2v.h2w][B1X]!==U91){var t91=c9w;t91+=r1X;throw t91;}var timeBlock=function(type){var S1X='-label">';var R1X="previous";var V1X='-iconUp">';var m1X='-timeblock">';var j1X="<div class";var Q1X="<div clas";var Y1X="n\">";var L1X="Dow";var k1X="icon";var H1X="</di";var y4J=c4h;y4J+=b1b;y4J+=Z1X;var d4J=H1X;d4J+=B4h;var Z4J=z5w;Z4J+=j0b;var z4J=f4h;z4J+=k1X;z4J+=L1X;z4J+=Y1X;var s4J=c4h;s4J+=d2v.E7w;s4J+=j5h;s4J+=t9b;var E4J=c4h;E4J+=Z1h;var w4J=U9w;w4J+=b1b;w4J+=L4h;var J91=Q1X;J91+=t9b;var b91=H1X;b91+=B4h;var h91=j1X;h91+=D1h;return P1h+classPrefix+m1X+h91+classPrefix+V1X+c1X+i18n[R1X]+a1X+b91+J91+classPrefix+S1X+P1X+e1X+classPrefix+w0b+type+w4J+E4J+s4J+classPrefix+z4J+c1X+i18n[Z4J]+a1X+d4J+y4J;};var gap=function(){var t1X=":</span>";var U1X="<span>";var u4J=U1X;u4J+=t1X;return u4J;};var structure=$(P1h+classPrefix+w3h+O4J+classPrefix+D4J+M4J+classPrefix+l4J+N4J+classPrefix+X4J+c1X+i18n[o4J]+a1X+W3h+I4J+classPrefix+W4J+T4J+i18n[A4J]+K4J+W3h+G4J+classPrefix+f4J+P1X+F4J+classPrefix+x4J+g4J+P1h+classPrefix+p4J+P1X+e1X+classPrefix+h1X+W3h+W3h+n4J+classPrefix+b1X+q4J+v4J+classPrefix+J1X+timeBlock(w3X)+gap()+timeBlock(C4J)+gap()+timeBlock(i4J)+timeBlock(E3X)+B4J+P1h+classPrefix+s3X+r4J);this[C3h]={container:structure,date:structure[G7b](D6b+classPrefix+z3X),title:structure[H4J](D6b+classPrefix+k4J),calendar:structure[L4J](D6b+classPrefix+Z3X),time:structure[G7b](D6b+classPrefix+Y4J),error:structure[Q4J](D6b+classPrefix+d3X),input:$(input)};this[D7w]={d:F3h,display:F3h,namespace:y3X+Editor[j4J][m4J]++,parts:{date:this[d2v.h2w][V4J][c4J](u3X)!==F3h,time:this[d2v.h2w][R4J][J3E](O3X)!==F3h,seconds:this[d2v.h2w][B1X][F1E](Q9w)!==-N2w,hours12:this[d2v.h2w][a4J][S4J](D3X)!==F3h}};this[P4J][e4J][U4J](this[C3h][t4J])[L6h](this[C3h][M3X])[L6h](this[C3h][h4J]);this[b4J][Z7b][L6h](this[C3h][D3h])[J4J](this[C3h][l3X]);this[S9w]();};$[w1J](Editor[E1J][s1J],{destroy:function(){var I3X='.editor-datetime';var N3X="nta";var u1J=d2v.E7w;u1J+=C4h;var y1J=i7b;y1J+=Y0w;var d1J=I2h;d1J+=N3X;d1J+=U7w;var Z1J=d2v.E7w;Z1J+=E6w;Z1J+=d2v.w7w;var z1J=X3X;z1J+=A7w;this[z1J]();this[Z1J][d1J][y1J]()[o3X]();this[u1J][U2h][X0h](I3X);},errorMsg:function(msg){var W3X="emp";var O1J=d2v.E7w;O1J+=E6w;O1J+=d2v.w7w;var error=this[O1J][Y2h];if(msg){error[o7h](msg);}else{var D1J=W3X;D1J+=d2v.b2w;D1J+=K5w;error[D1J]();}},hide:function(){var M1J=J5h;M1J+=X7w;M1J+=v7w;this[M1J]();},max:function(date){var G3X="_optionsTitle";var A3X="lander";var T3X="_setCa";var l1J=T3X;l1J+=A3X;this[d2v.h2w][K3X]=date;this[G3X]();this[l1J]();},min:function(date){var n3X="nDa";var p3X="onsTitle";var F3X="tCala";var f3X="_se";var o1J=f3X;o1J+=F3X;o1J+=x3X;var X1J=g3X;X1J+=p3X;var N1J=a1E;N1J+=n3X;N1J+=q6w;this[d2v.h2w][N1J]=date;this[X1J]();this[o1J]();},owns:function(node){var T1J=n6w;T1J+=M4h;T1J+=l4h;var W1J=d2v.E7w;W1J+=E6w;W1J+=d2v.w7w;var I1J=q3X;I1J+=v3X;return $(node)[I1J]()[P5E](this[W1J][F2h])[T1J]>l2w;},val:function(set,write){var S3X=/(\d{4})\-(\d{2})\-(\d{2})/;var R3X="toDate";var c3X="isValid";var m3X="utc";var j3X="ment";var Q3X="momentLoca";var Y3X="mome";var L3X="tc";var k3X="oU";var H3X="teT";var r3X="_da";var B3X="tri";var i3X="UTCD";var C3X="tTime";var C1J=Z8w;C1J+=G1h;C1J+=C3X;var v1J=J8h;v1J+=i3X;v1J+=d2v.J2w;v1J+=q6w;var q1J=R5w;q1J+=T0w;var K1J=D7w;K1J+=B3X;K1J+=l7w;K1J+=c8w;if(set===undefined){return this[D7w][d2v.E7w];}if(set instanceof Date){var A1J=r3X;A1J+=H3X;A1J+=k3X;A1J+=L3X;this[D7w][d2v.E7w]=this[A1J](set);}else if(set===F3h||set===Y9w){this[D7w][d2v.E7w]=F3h;}else if(typeof set===K1J){var G1J=Y3X;G1J+=q5w;if(window[G1J]){var F1J=Q3X;F1J+=n6w;var f1J=b4b;f1J+=j3X;var m=window[f1J][m3X](set,this[d2v.h2w][B1X],this[d2v.h2w][F1J],this[d2v.h2w][V3X]);this[D7w][d2v.E7w]=m[c3X]()?m[R3X]():F3h;}else{var g1J=a3X;g1J+=r6w;var x1J=d2v.w7w;x1J+=d2v.J2w;x1J+=w1X;var match=set[x1J](S3X);this[D7w][d2v.E7w]=match?new Date(Date[g1J](match[N2w],match[X2w]-N2w,match[o2w])):F3h;}}if(write||write===undefined){if(this[D7w][d2v.E7w]){this[P3X]();}else{var n1J=z9w;n1J+=d2v.J2w;n1J+=X6w;var p1J=d2v.E7w;p1J+=E6w;p1J+=d2v.w7w;this[p1J][U2h][n1J](set);}}if(!this[D7w][d2v.E7w]){this[D7w][d2v.E7w]=this[e3X](new Date());}this[D7w][u7h]=new Date(this[D7w][d2v.E7w][m3E]());this[D7w][q1J][v1J](N2w);this[U3X]();this[t3X]();this[C1J]();},_constructor:function(){var q2X='select';var p2X=':visible';var g2X='focus.editor-datetime click.editor-datetime';var x2X='seconds';var F2X="minutesIncrement";var f2X='minutes';var G2X="_optionsTime";var K2X="last";var A2X="ock";var T2X="v.editor-datetime-timebl";var I2X='div.editor-datetime-timeblock';var o2X="pan";var X2X="seconds";var N2X="parts";var l2X="hange";var M2X="onC";var D2X="arts";var O2X="itle";var u2X="_optionsT";var y2X="rs12";var Z2X="optionsTi";var z2X="crement";var s2X="secondsIn";var E2X="_o";var w2X="P";var J3X="aut";var b3X="etime";var h3X="keyup.editor-dat";var R3J=E6w;R3J+=l7w;var x3J=E6w;x3J+=l7w;var F3J=d2v.E7w;F3J+=C4h;var T3J=h3X;T3J+=b3X;var M3J=E6w;M3J+=l7w;var D3J=E6w;D3J+=H6b;var O3J=J3X;O3J+=E6w;O3J+=v1E;var u3J=d2v.J2w;u3J+=d2v.b2w;u3J+=I5b;var y3J=d2v.E7w;y3J+=E6w;y3J+=d2v.w7w;var d3J=E1b;d3J+=w2X;d3J+=d2v.w7w;var Z3J=d2v.J2w;Z3J+=d2v.w7w;var z3J=E2X;z3J+=W7E;var s3J=s2X;s3J+=z2X;var E3J=Z8w;E3J+=Z2X;E3J+=d2v.w7w;E3J+=A7w;var w3J=d2X;w3J+=y2X;var J1J=u2X;J1J+=O2X;var e1J=o6b;e1J+=I7w;e1J+=o8h;var V1J=i7w;V1J+=D2X;var Y1J=r2E;Y1J+=x7w;var r1J=d2v.E7w;r1J+=d2v.J2w;r1J+=d2v.b2w;r1J+=A7w;var B1J=M2X;B1J+=l2X;var i1J=k2h;i1J+=U7w;var that=this;var classPrefix=this[d2v.h2w][i1X];var container=this[C3h][i1J];var i18n=this[d2v.h2w][S0b];var onChange=this[d2v.h2w][B1J];if(!this[D7w][N2X][r1J]){var L1J=l7w;L1J+=E6w;L1J+=l7w;L1J+=A7w;var k1J=R5w;k1J+=T0w;var H1J=Y1h;H1J+=A7w;this[C3h][H1J][A2h](k1J,L1J);}if(!this[D7w][N2X][Y1J]){var m1J=l7w;m1J+=E6w;m1J+=l7w;m1J+=A7w;var j1J=d2v.h2w;j1J+=D7w;j1J+=D7w;var Q1J=d2v.b2w;Q1J+=X7w;Q1J+=x7w;this[C3h][Q1J][j1J](q3h,m1J);}if(!this[D7w][V1J][X2X]){var P1J=U4h;P1J+=d2v.w7w;P1J+=Q8E;var S1J=A7w;S1J+=h1b;var a1J=D7w;a1J+=o2X;var R1J=I4b;R1J+=d2v.E7w;R1J+=I7w;R1J+=u0w;var c1J=d2v.E7w;c1J+=E6w;c1J+=d2v.w7w;this[c1J][M3X][k6h](I2X)[F0b](X2w)[M8h]();this[C3h][M3X][R1J](a1J)[S1J](N2w)[P1J]();}if(!this[D7w][e1J][W2X]){var b1J=U4h;b1J+=W4X;var h1J=k5w;h1J+=T2X;h1J+=A2X;var t1J=r2E;t1J+=x7w;var U1J=L2h;U1J+=d2v.w7w;this[U1J][t1J][k6h](h1J)[K2X]()[b1J]();}this[J1J]();this[G2X](w3X,this[D7w][N2X][w3J]?G2w:n2w,N2w);this[E3J](f2X,j2w,this[d2v.h2w][F2X]);this[G2X](x2X,j2w,this[d2v.h2w][s3J]);this[z3J](E3X,[Z3J,o0E],i18n[d3J]);this[y3J][U2h][u3J](O3J,D3J)[M3J](g2X,function(){var n2X=':disabled';var W3J=z9w;W3J+=d2v.J2w;W3J+=X6w;var I3J=z9w;I3J+=r1b;var o3J=X7w;o3J+=D7w;var X3J=d2v.E7w;X3J+=E6w;X3J+=d2v.w7w;var N3J=X7w;N3J+=D7w;var l3J=M2h;l3J+=d2v.b2w;l3J+=S2h;if(that[C3h][l3J][N3J](p2X)||that[X3J][U2h][o3J](n2X)){return;}that[I3J](that[C3h][U2h][W3J](),o4h);that[b5h]();})[k3h](T3J,function(){var A3J=X7w;A3J+=D7w;if(that[C3h][F2h][A3J](p2X)){var f3J=z9w;f3J+=d2v.J2w;f3J+=X6w;var G3J=X7w;G3J+=z1h;G3J+=d2v.b2w;var K3J=z9w;K3J+=d2v.J2w;K3J+=X6w;that[K3J](that[C3h][G3J][f3J](),o4h);}});this[F3J][F2h][x3J](E7b,q2X,function(){var h2X="setT";var t2X='-seconds';var U2X="setUTCMinutes";var e2X="Ti";var P2X='-minutes';var S2X="_setTime";var a2X="CHours";var c2X="setUTCHours";var V2X='-ampm';var j2X="-hou";var Y2X="ullY";var L2X="setUTCF";var k2X="tCalander";var H2X='-year';var r2X="_correctMonth";var B2X='-month';var i2X="asCla";var C2X="-am";var v2X="osition";var c3J=d0w;c3J+=v2X;var V3J=X7w;V3J+=l7w;V3J+=i7w;V3J+=f0b;var m3J=d2v.E7w;m3J+=E6w;m3J+=d2v.w7w;var i3J=C2X;i3J+=i7w;i3J+=d2v.w7w;var C3J=f4h;C3J+=m8h;C3J+=J8w;C3J+=z6E;var p3J=k0w;p3J+=i2X;p3J+=n3h;var g3J=Z4b;g3J+=H2h;g3J+=D7w;g3J+=D7w;var select=$(this);var val=select[a3h]();if(select[g3J](classPrefix+B2X)){that[r2X](that[D7w][u7h],val);that[U3X]();that[t3X]();}else if(select[p3J](classPrefix+H2X)){var v3J=Z8w;v3J+=D7w;v3J+=A7w;v3J+=k2X;var q3J=L2X;q3J+=Y2X;q3J+=Q2X;var n3J=k5w;n3J+=o6h;n3J+=X6w;n3J+=o2h;that[D7w][n3J][q3J](val);that[U3X]();that[v3J]();}else if(select[g2h](classPrefix+C3J)||select[g2h](classPrefix+i3J)){var B3J=o6b;B3J+=I7w;B3J+=d2v.b2w;B3J+=D7w;if(that[D7w][B3J][W2X]){var L3J=i7w;L3J+=d2v.w7w;var k3J=Y0w;k3J+=J4b;k3J+=d2v.E7w;var H3J=j2X;H3J+=z6E;var r3J=m2X;r3J+=d2v.E7w;var hours=$(that[C3h][F2h])[r3J](D6b+classPrefix+H3J)[a3h]()*N2w;var pm=$(that[C3h][F2h])[k3J](D6b+classPrefix+V2X)[a3h]()===L3J;that[D7w][d2v.E7w][c2X](hours===G2w&&!pm?l2w:pm&&hours!==G2w?hours+G2w:hours);}else{var Y3J=R2X;Y3J+=a2X;that[D7w][d2v.E7w][Y3J](val);}that[S2X]();that[P3X](I4h);onChange();}else if(select[g2h](classPrefix+P2X)){var Q3J=Z8w;Q3J+=J8h;Q3J+=e2X;Q3J+=x7w;that[D7w][d2v.E7w][U2X](val);that[Q3J]();that[P3X](I4h);onChange();}else if(select[g2h](classPrefix+t2X)){var j3J=Z8w;j3J+=h2X;j3J+=L7w;that[D7w][d2v.E7w][b2X](val);that[j3J]();that[P3X](I4h);onChange();}that[m3J][V3J][S3h]();that[c3J]();})[R3J](L3h,function(e){var L7X="etCala";var H7X='year';var r7X="setUTCDate";var B7X="oUtc";var C7X="setUTCFu";var q7X="etUT";var n7X="tput";var p7X="riteOu";var g7X="art";var x7X="options";var F7X="selectedInd";var f7X='-iconDown';var G7X="selectedIndex";var K7X="selectedI";var A7X="selectedIn";var T7X="cha";var W7X='-iconUp';var o7X="onth";var X7X="_correc";var N7X="etT";var l7X="ala";var M7X="_setC";var O7X="etUTCM";var y7X="iconLeft";var d7X="hasClas";var Z7X="-iconRigh";var z7X="hasCl";var s7X="toLowerCase";var E7X="nodeName";var w7X="Propagat";var P3J=a7w;P3J+=J8w;P3J+=d2v.b2w;P3J+=J2X;var S3J=y0w;S3J+=w7X;S3J+=X7w;S3J+=k3h;var a3J=y4b;a3J+=m0h;var nodeName=e[a3J][E7X][s7X]();if(nodeName===q2X){return;}e[S3J]();if(nodeName===P3J){var I2J=z7X;I2J+=d2v.J2w;I2J+=n3h;var s2J=Z7X;s2J+=d2v.b2w;var E2J=d7X;E2J+=D7w;var t3J=f4h;t3J+=y7X;var U3J=u7X;U3J+=X6w;U3J+=A7w;U3J+=d2v.E7w;var e3J=q3X;e3J+=q9b;var button=$(e[f7b]);var parent=button[e3J]();var select;if(parent[g2h](U3J)){return;}if(parent[g2h](classPrefix+t3J)){var w2J=d2v.E7w;w2J+=E6w;w2J+=d2v.w7w;var J3J=c8w;J3J+=O7X;J3J+=k3h;J3J+=l4h;var b3J=k3E;b3J+=I6h;var h3J=d2v.E7w;h3J+=X7w;h3J+=o6h;h3J+=I6h;that[D7w][h3J][D7X](that[D7w][b3J][J3J]()-N2w);that[U3X]();that[t3X]();that[w2J][U2h][S3h]();}else if(parent[E2J](classPrefix+s2J)){var O2J=Y0w;O2J+=S1b;O2J+=P1b;var u2J=M7X;u2J+=l7X;u2J+=x3X;var y2J=i0b;y2J+=N7X;y2J+=O2X;var d2J=k3E;d2J+=I6h;var Z2J=k5w;Z2J+=r7h;Z2J+=d2v.J2w;Z2J+=K5w;var z2J=X7X;z2J+=d2v.b2w;z2J+=w6w;z2J+=o7X;that[z2J](that[D7w][Z2J],that[D7w][d2J][I7X]()+N2w);that[y2J]();that[u2J]();that[C3h][U2h][O2J]();}else if(parent[g2h](classPrefix+W7X)){var o2J=T7X;o2J+=l7w;o2J+=X5w;var X2J=A7X;X2J+=d2v.E7w;X2J+=x1h;var N2J=y8h;N2J+=w8w;N2J+=i5w;var l2J=K7X;l2J+=g1h;l2J+=x1h;var M2J=Y0w;M2J+=X7w;M2J+=l7w;M2J+=d2v.E7w;var D2J=i7w;D2J+=D9E;D2J+=q5w;select=parent[D2J]()[M2J](q2X)[l2w];select[G7X]=select[l2J]!==select[N2J][X4h]-N2w?select[X2J]+N2w:l2w;$(select)[o2J]();}else if(parent[I2J](classPrefix+f7X)){var T2J=n6w;T2J+=s5b;var W2J=F7X;W2J+=A7w;W2J+=f7w;select=parent[F8h]()[G7b](q2X)[l2w];select[G7X]=select[W2J]===l2w?select[x7X][T2J]-N2w:select[G7X]-N2w;$(select)[X8E]();}else{var p2J=i7w;p2J+=g7X;p2J+=D7w;var g2J=Z8w;g2J+=Y6w;g2J+=p7X;g2J+=n7X;var x2J=d2v.E7w;x2J+=d2v.J2w;x2J+=K5w;var F2J=d2v.E7w;F2J+=d2v.J2w;F2J+=d2v.b2w;F2J+=d2v.J2w;var f2J=d2v.E7w;f2J+=W7b;var G2J=D7w;G2J+=q7X;G2J+=r6w;G2J+=v7X;var K2J=C7X;K2J+=i7X;K2J+=Q2X;if(!that[D7w][d2v.E7w]){var A2J=F5h;A2J+=s8w;A2J+=b8w;A2J+=B7X;that[D7w][d2v.E7w]=that[A2J](new Date());}that[D7w][d2v.E7w][r7X](N2w);that[D7w][d2v.E7w][K2J](button[r5w](H7X));that[D7w][d2v.E7w][G2J](button[f2J](k7X));that[D7w][d2v.E7w][r7X](button[F2J](x2J));that[g2J](I4h);if(!that[D7w][p2J][M3X]){setTimeout(function(){var n2J=J5h;n2J+=Y6h;that[n2J]();},A2w);}else{var q2J=i0b;q2J+=L7X;q2J+=x3X;that[q2J]();}onChange();}}else{var v2J=P1E;v2J+=P1b;that[C3h][U2h][v2J]();}});},_compareDates:function(a,b){var j7X="_dateToUtcString";var Q7X="cString";var Y7X="dateToUt";var C2J=Z8w;C2J+=Y7X;C2J+=Q7X;return this[j7X](a)===this[C2J](b);},_correctMonth:function(date,month){var e7X="TCDate";var P7X="setU";var R7X="CF";var c7X="getUT";var V7X="getUTC";var m7X="etUTCMo";var r2J=D7w;r2J+=m7X;r2J+=q5w;r2J+=k0w;var B2J=V7X;B2J+=X8w;B2J+=s8w;var i2J=c7X;i2J+=R7X;i2J+=w1h;i2J+=a7X;var days=this[S7X](date[i2J](),month);var correctDays=date[B2J]()>days;date[r2J](month);if(correctDays){var H2J=P7X;H2J+=e7X;date[H2J](days);date[D7X](month);}},_daysInMonth:function(year,month){var r2w=31;var B2w=30;var i2w=29;var C2w=28;var isLeap=year%I2w===l2w&&(year%m2w!==l2w||year%c2w===l2w);var months=[r2w,isLeap?i2w:C2w,r2w,B2w,r2w,B2w,r2w,r2w,B2w,r2w,B2w,r2w];return months[month];},_dateToUtc:function(s){var E8X="getSeconds";var w8X="getHours";var J7X="U";var h7X="Da";var t7X="nu";var U7X="getMi";var j2J=U7X;j2J+=t7X;j2J+=d2v.b2w;j2J+=E4h;var Q2J=m0h;Q2J+=h7X;Q2J+=d2v.b2w;Q2J+=A7w;var Y2J=m0h;Y2J+=v7X;var L2J=b7X;L2J+=i7X;L2J+=Q2X;var k2J=J7X;k2J+=b8w;k2J+=r6w;return new Date(Date[k2J](s[L2J](),s[Y2J](),s[Q2J](),s[w8X](),s[j2J](),s[E8X]()));},_dateToUtcString:function(d){var s8X="UTCDate";var m2J=m0h;m2J+=s8X;return d[z8X]()+w0b+this[Z8X](d[I7X]()+N2w)+w0b+this[Z8X](d[m2J]());},_hide:function(){var u8X="amespac";var y8X="keyd";var t2J=y0h;t2J+=d2v.h2w;t2J+=d8X;var U2J=a7w;U2J+=E6w;U2J+=d2v.E7w;U2J+=K5w;var e2J=E6w;e2J+=Y0w;e2J+=Y0w;var P2J=y8X;P2J+=E6w;P2J+=Z8h;P2J+=E9w;var S2J=E6w;S2J+=Y0w;S2J+=Y0w;var a2J=i7b;a2J+=Y0w;var R2J=I2h;R2J+=D8h;R2J+=A7w;R2J+=I7w;var c2J=d2v.E7w;c2J+=E6w;c2J+=d2v.w7w;var V2J=l7w;V2J+=u8X;V2J+=A7w;var namespace=this[D7w][V2J];this[c2J][R2J][X7h]();$(window)[a2J](D6b+namespace);$(document)[S2J](P2J+namespace);$(d5h)[e2J](O8X+namespace);$(U2J)[X0h](t2J+namespace);},_hours24To12:function(val){return val===l2w?G2w:val>G2w?val-G2w:val;},_htmlDay:function(day){var v8X='" data-day="';var q8X="year";var n8X='data-year="';var p8X='<button class="';var g8X='<td data-day="';var f8X='<td class="empty"></td>';var G8X="empt";var A8X="classPr";var T8X="day";var W8X="\" class";var I8X="utton ";var o8X="\"button\" ";var X8X="type=";var N8X="ay\" ";var l8X="-d";var M8X="a-month=\"";var D8X="td";var N7J=i4h;N7J+=D8X;N7J+=L4h;var l7J=U9w;l7J+=L4h;var M7J=d2v.E7w;M7J+=d2v.J2w;M7J+=K5w;var D7J=b4b;D7J+=l7w;D7J+=d2v.b2w;D7J+=k0w;var O7J=O1h;O7J+=d2v.E7w;O7J+=K0h;O7J+=M8X;var u7J=l8X;u7J+=N8X;u7J+=X8X;u7J+=o8X;var y7J=f4h;y7J+=a7w;y7J+=I8X;var d7J=W8X;d7J+=D1h;var Z7J=d2v.E7w;Z7J+=d2v.J2w;Z7J+=K5w;var w7J=d2v.b2w;w7J+=E6w;w7J+=T8X;var J2J=A8X;J2J+=K8X;var b2J=d2v.E7w;b2J+=d2v.J2w;b2J+=K5w;var h2J=G8X;h2J+=K5w;if(day[h2J]){return f8X;}var classes=[b2J];var classPrefix=this[d2v.h2w][J2J];if(day[N2h]){classes[D4h](F8X);}if(day[w7J]){var s7J=d2v.b2w;s7J+=V5w;s7J+=d2v.J2w;s7J+=K5w;var E7J=i7w;E7J+=J8w;E7J+=D7w;E7J+=k0w;classes[E7J](s7J);}if(day[x8X]){var z7J=i7w;z7J+=J8w;z7J+=D7w;z7J+=k0w;classes[z7J](j4X);}return g8X+day[Z7J]+d7J+classes[J6b](U1h)+w3h+p8X+classPrefix+y7J+classPrefix+u7J+n8X+day[q8X]+O7J+day[D7J]+v8X+day[M7J]+l7J+day[T8X]+a1X+N7J;},_htmlMonth:function(year,month){var T6X='</table>';var W6X='</thead>';var I6X="_htmlMonthHead";var o6X='<table class="';var X6X='div.';var N6X="conRight";var l6X="iv.";var M6X="ef";var D6X="-icon";var O6X=' weekNumber';var u6X='</tr>';var y6X='<tr>';var d6X="hift";var Z6X="WeekOfYear";var z6X="_html";var s6X="ekNumbe";var E6X="showWe";var w6X="_htmlDay";var J8X="_compareD";var b8X="ompareDates";var h8X="isableDays";var t8X="etUTCDay";var U8X="UTCHours";var e8X="nutes";var P8X="CMi";var S8X="etSeconds";var a8X="TCHour";var R8X="tU";var c8X="Minute";var m8X="getUTCDay";var j8X="UTC";var Q8X="Date";var Y8X="min";var L8X="-ta";var k8X="ekNumber";var r8X="ead>";var B8X="<t";var i8X="tbody>";var C8X="/tbody>";var Q2w=59;var p2w=23;var S7J=c4h;S7J+=C8X;var a7J=c4h;a7J+=i8X;var R7J=B8X;R7J+=k0w;R7J+=r8X;var c7J=U9w;c7J+=L4h;var B7J=H8X;B7J+=S3b;B7J+=h8w;B7J+=k8X;var i7J=L8X;i7J+=a7w;i7J+=n6w;var X7J=Y8X;X7J+=Q8X;var now=this[e3X](new Date()),days=this[S7X](year,month),before=new Date(Date[j8X](year,month,N2w))[m8X](),data=[],row=[];if(this[d2v.h2w][V8X]>l2w){before-=this[d2v.h2w][V8X];if(before<l2w){before+=W2w;}}var cells=days+before,after=cells;while(after>W2w){after-=W2w;}cells+=W2w-after;var minDate=this[d2v.h2w][X7J];var maxDate=this[d2v.h2w][K3X];if(minDate){var I7J=J8h;I7J+=j8X;I7J+=c8X;I7J+=D7w;var o7J=G1h;o7J+=R8X;o7J+=a8X;o7J+=D7w;minDate[o7J](l2w);minDate[I7J](l2w);minDate[b2X](l2w);}if(maxDate){var A7J=D7w;A7J+=S8X;var T7J=R2X;T7J+=P8X;T7J+=e8X;var W7J=J8h;W7J+=U8X;maxDate[W7J](p2w);maxDate[T7J](Q2w);maxDate[A7J](Q2w);}for(var i=l2w,r=l2w;i<cells;i++){var g7J=c8w;g7J+=t8X;var x7J=I5E;x7J+=I7w;x7J+=o2h;var F7J=d2v.E7w;F7J+=h8X;var f7J=Z8w;f7J+=d2v.h2w;f7J+=b8X;var G7J=J8X;G7J+=d2v.J2w;G7J+=d2v.b2w;G7J+=E4h;var K7J=a3X;K7J+=r6w;var day=new Date(Date[K7J](year,month,N2w+(i-before))),selected=this[D7w][d2v.E7w]?this[G7J](day,this[D7w][d2v.E7w]):o4h,today=this[f7J](day,now),empty=i<before||i>=days+before,disabled=minDate&&day<minDate||maxDate&&day>maxDate;var disableDays=this[d2v.h2w][F7J];if($[t4b](disableDays)&&$[x7J](day[g7J](),disableDays)!==-N2w){disabled=I4h;}else if(typeof disableDays===d2v.d7w&&disableDays(day)===I4h){disabled=I4h;}var dayConfig={day:N2w+(i-before),month:month,year:year,selected:selected,today:today,disabled:disabled,empty:empty};row[D4h](this[w6X](dayConfig));if(++r===W2w){var C7J=A1E;C7J+=J4b;var v7J=i7w;v7J+=J8w;v7J+=D7w;v7J+=k0w;var p7J=E6X;p7J+=s6X;p7J+=I7w;if(this[d2v.h2w][p7J]){var q7J=z6X;q7J+=Z6X;var n7J=M6w;n7J+=D7w;n7J+=d6X;row[n7J](this[q7J](i-before,month,year));}data[v7J](y6X+row[C7J](Y9w)+u6X);row=[];r=l2w;}}var classPrefix=this[d2v.h2w][i1X];var className=classPrefix+i7J;if(this[d2v.h2w][B7J]){className+=O6X;}if(minDate){var Y7J=k5w;Y7J+=r7h;Y7J+=o2h;var L7J=d2v.h2w;L7J+=D7w;L7J+=D7w;var k7J=D6X;k7J+=s2E;k7J+=M6X;k7J+=d2v.b2w;var H7J=d2v.E7w;H7J+=l6X;var r7J=d2v.b2w;r7J+=E0w;r7J+=X6w;r7J+=A7w;var underMin=minDate>new Date(Date[j8X](year,month,N2w,l2w,l2w,l2w));this[C3h][r7J][G7b](H7J+classPrefix+k7J)[L7J](Y7J,underMin?K2h:k7h);}if(maxDate){var V7J=l7w;V7J+=k3h;V7J+=A7w;var m7J=f4h;m7J+=X7w;m7J+=N6X;var j7J=Y0w;j7J+=X7w;j7J+=l7w;j7J+=d2v.E7w;var Q7J=d2v.b2w;Q7J+=X7w;Q7J+=B2E;Q7J+=A7w;var overMax=maxDate<new Date(Date[j8X](year,month+N2w,N2w,l2w,l2w,l2w));this[C3h][Q7J][j7J](X6X+classPrefix+m7J)[A2h](q3h,overMax?V7J:k7h);}return o6X+className+c7J+R7J+this[I6X]()+W6X+a7J+data[J6b](Y9w)+S7J+T6X;},_htmlMonthHead:function(){var p6X='</th>';var g6X='<th>';var x6X='<th></th>';var G6X="mbe";var K6X="showWeekNu";var A6X="oi";var t7J=h6b;t7J+=A6X;t7J+=l7w;var e7J=K6X;e7J+=G6X;e7J+=I7w;var a=[];var firstDay=this[d2v.h2w][V8X];var i18n=this[d2v.h2w][S0b];var dayName=function(day){var F6X="ays";var f6X="weekd";var P7J=f6X;P7J+=F6X;day+=firstDay;while(day>=W2w){day-=W2w;}return i18n[P7J][day];};if(this[d2v.h2w][e7J]){a[D4h](x6X);}for(var i=l2w;i<W2w;i++){var U7J=i7w;U7J+=J8w;U7J+=D7w;U7J+=k0w;a[U7J](g6X+dayName(i)+p6X);}return a[t7J](Y9w);},_htmlWeekOfYear:function(d,m,y){var H6X='-week">';var r6X="ceil";var B6X="getDay";var i6X="getDate";var v6X="=";var q6X="<td clas";var n6X="td>";var e2w=86400000;var b7J=i4h;b7J+=n6X;var h7J=q6X;h7J+=D7w;h7J+=v6X;h7J+=U9w;var date=new Date(y,m,d,l2w,l2w,l2w,l2w);date[C6X](date[i6X]()+I2w-(date[B6X]()||W2w));var oneJan=new Date(y,l2w,N2w);var weekNum=Math[r6X](((date-oneJan)/e2w+N2w)/W2w);return h7J+this[d2v.h2w][i1X]+H6X+weekNum+b7J;},_options:function(selector,values,labels){var m6X="alue=\"";var j6X="<option v";var Q6X="/option>";var L6X="contai";var k6X="ngt";var s8J=X6w;s8J+=A7w;s8J+=k6X;s8J+=k0w;var E8J=A7w;E8J+=C1E;E8J+=a6w;var w8J=D7w;w8J+=T8w;w8J+=d5b;w8J+=E9w;var J7J=L6X;J7J+=Y6X;if(!labels){labels=values;}var select=this[C3h][J7J][G7b](w8J+this[d2v.h2w][i1X]+w0b+selector);select[E8J]();for(var i=l2w,ien=values[s8J];i<ien;i++){var d8J=c4h;d8J+=Q6X;var Z8J=U9w;Z8J+=L4h;var z8J=j6X;z8J+=m6X;select[L6h](z8J+values[i]+Z8J+labels[i]+d8J);}},_optionSet:function(selector,val){var e6X="unknown";var S6X="classP";var a6X="arent";var R6X="cted";var V6X="option:";var W8J=a6E;W8J+=l7w;var I8J=X6w;I8J+=V2E;var o8J=G7E;o8J+=X6w;var X8J=V6X;X8J+=c6X;X8J+=R6X;var N8J=f5w;N8J+=l7w;N8J+=d2v.E7w;var l8J=z9w;l8J+=d2v.J2w;l8J+=X6w;var M8J=D7w;M8J+=o6b;M8J+=l7w;var D8J=i7w;D8J+=a6X;var O8J=S6X;O8J+=I7w;O8J+=K8X;var u8J=Y0w;u8J+=X7w;u8J+=l7w;u8J+=d2v.E7w;var y8J=d2v.E7w;y8J+=E6w;y8J+=d2v.w7w;var select=this[y8J][F2h][u8J](P6X+this[d2v.h2w][O8J]+w0b+selector);var span=select[D8J]()[k6h](M8J);select[l8J](val);var selected=select[N8J](X8J);span[o8J](selected[I8J]!==l2w?selected[d6h]():this[d2v.h2w][W8J][e6X]);},_optionsTime:function(select,count,inc){var J6X='</option>';var b6X="ion value";var h6X="<opt";var t6X="ilable";var U6X="rsA";var T8J=Y0w;T8J+=J4b;T8J+=d2v.E7w;var classPrefix=this[d2v.h2w][i1X];var sel=this[C3h][F2h][T8J](P6X+classPrefix+w0b+select);var start=l2w,end=count;var allowed;var render=count===G2w?function(i){return i;}:this[Z8X];if(count===G2w){start=N2w;end=f2w;}if(count===G2w||count===n2w){var A8J=d2X;A8J+=U6X;A8J+=T5b;A8J+=t6X;allowed=this[d2v.h2w][A8J];}for(var i=start;i<end;i+=inc){var K8J=I5E;K8J+=I7w;K8J+=o2h;if(!allowed||$[K8J](i,allowed)!==-N2w){var f8J=h6X;f8J+=b6X;f8J+=D1h;var G8J=z6h;G8J+=e8b;sel[G8J](f8J+i+w3h+render(i)+J6X);}}},_optionsTitle:function(year,month){var N0X="_options";var l0X="months";var M0X="yearRange";var D0X="getFullYear";var O0X="minDate";var u0X="fix";var y0X="ssPre";var d0X="getFullY";var Z0X="lYea";var z0X="Yea";var s0X="tFull";var E0X="_op";var w0X="an";var i8J=Z8w;i8J+=D4b;i8J+=M4h;i8J+=A7w;var C8J=K5w;C8J+=Q2X;var v8J=Z8w;v8J+=I7w;v8J+=w0X;v8J+=X5w;var q8J=E0X;q8J+=r2E;q8J+=G0b;var n8J=X5w;n8J+=s0X;n8J+=z0X;n8J+=I7w;var p8J=b7X;p8J+=X6w;p8J+=Z0X;p8J+=I7w;var g8J=d0X;g8J+=A7w;g8J+=n2b;var x8J=n1h;x8J+=q1h;x8J+=l7w;var F8J=l1h;F8J+=y0X;F8J+=u0X;var classPrefix=this[d2v.h2w][F8J];var i18n=this[d2v.h2w][x8J];var min=this[d2v.h2w][O0X];var max=this[d2v.h2w][K3X];var minYear=min?min[g8J]():F3h;var maxYear=max?max[p8J]():F3h;var i=minYear!==F3h?minYear:new Date()[D0X]()-this[d2v.h2w][M0X];var j=maxYear!==F3h?maxYear:new Date()[n8J]()+this[d2v.h2w][M0X];this[q8J](k7X,this[v8J](l2w,K2w),i18n[l0X]);this[N0X](C8J,this[i8J](i,j));},_pad:function(i){var X0X='0';return i<A2w?X0X+i:i;},_position:function(){var A0X="width";var T0X="outerWidth";var o0X="scroll";var j8J=d2v.b2w;j8J+=E6w;j8J+=i7w;var Q8J=o0X;Q8J+=b8w;Q8J+=E6w;Q8J+=i7w;var Y8J=d2v.h2w;Y8J+=D7w;Y8J+=D7w;var L8J=X7w;L8J+=l7w;L8J+=I0X;var k8J=L2h;k8J+=d2v.w7w;var H8J=M2h;H8J+=d2v.b2w;H8J+=d2v.J2w;H8J+=U7w;var r8J=L2h;r8J+=d2v.w7w;var B8J=W0X;B8J+=J8w;B8J+=d2v.b2w;var offset=this[C3h][B8J][a3b]();var container=this[r8J][H8J];var inputHeight=this[k8J][L8J][Z5h]();container[Y8J]({top:offset[R9h]+inputHeight,left:offset[R3b]})[p5h](x0h);var calHeight=container[Z5h]();var calWidth=container[T0X]();var scrollTop=$(window)[Q8J]();if(offset[j8J]+inputHeight+calHeight-scrollTop>$(window)[G4b]()){var m8J=d2v.h2w;m8J+=D7w;m8J+=D7w;var newTop=offset[R9h]-calHeight;container[m8J](P3b,newTop<l2w?l2w:newTop);}if(calWidth+offset[R3b]>$(window)[A0X]()){var newLeft=$(window)[A0X]()-calWidth;container[A2h](t3b,newLeft<l2w?l2w:newLeft);}},_range:function(start,end){var a=[];for(var i=start;i<=end;i++){var V8J=i7w;V8J+=J8w;V8J+=H8X;a[V8J](i);}return a;},_setCalander:function(){var F0X="nth";var f0X="lMo";var G0X="_htm";var K0X="etUTCFul";if(this[D7w][u7h]){var S8J=c8w;S8J+=K0X;S8J+=a7X;var a8J=G0X;a8J+=f0X;a8J+=F0X;var R8J=D7h;R8J+=o3b;var c8J=d2v.E7w;c8J+=E6w;c8J+=d2v.w7w;this[c8J][l3X][o3X]()[R8J](this[a8J](this[D7w][u7h][S8J](),this[D7w][u7h][I7X]()));}},_setTitle:function(){var x0X="optionS";var e8J=K5w;e8J+=d8b;e8J+=I7w;var P8J=Z8w;P8J+=x0X;P8J+=A7w;P8J+=d2v.b2w;this[P8J](k7X,this[D7w][u7h][I7X]());this[g0X](e8J,this[D7w][u7h][z8X]());},_setTime:function(){var j0X="getUTCMinutes";var Q0X="_optionS";var Y0X="hour";var L0X='am';var k0X="_hours24To12";var H0X="ours";var r0X="getUTCHours";var B0X="onSet";var i0X="tes";var C0X="inu";var v0X="_optio";var q0X="onds";var n0X="ec";var p0X="getS";var z6J=p0X;z6J+=n0X;z6J+=q0X;var s6J=G1h;s6J+=d1X;var E6J=v0X;E6J+=l7w;E6J+=c2b;E6J+=d2v.b2w;var w6J=d2v.w7w;w6J+=C0X;w6J+=i0X;var J8J=g3X;J8J+=B0X;var U8J=i7w;U8J+=d2v.J2w;U8J+=I7w;U8J+=o8h;var d=this[D7w][d2v.E7w];var hours=d?d[r0X]():l2w;if(this[D7w][U8J][W2X]){var t8J=k0w;t8J+=H0X;this[g0X](t8J,this[k0X](hours));this[g0X](E3X,hours<G2w?L0X:o0E);}else{var b8J=Y0X;b8J+=D7w;var h8J=Q0X;h8J+=M5w;this[h8J](b8J,hours);}this[J8J](w6J,d?d[j0X]():l2w);this[E6J](s6J,d?d[z6J]():l2w);},_show:function(){var a0X='keydown.';var R0X=' resize.';var c0X="_position";var V0X="namespace";var m0X="l.";var y6J=E6w;y6J+=l7w;var d6J=w0h;d6J+=m0X;var Z6J=E6w;Z6J+=l7w;var that=this;var namespace=this[D7w][V0X];this[c0X]();$(window)[Z6J](d6J+namespace+R0X+namespace,function(){that[c0X]();});$(d5h)[y6J](O8X+namespace,function(){var u6J=d0w;u6J+=Q6b;u6J+=X7w;u6J+=S2E;that[u6J]();});$(document)[k3h](a0X+namespace,function(e){var P0X="yCo";var S0X="Co";var T2w=9;var M6J=u7E;M6J+=K5w;M6J+=S0X;M6J+=v7w;var D6J=u7E;D6J+=P0X;D6J+=v7w;var O6J=X2b;O6J+=S0X;O6J+=v7w;if(e[O6J]===T2w||e[D6J]===v2w||e[M6J]===f2w){var l6J=X3X;l6J+=A7w;that[l6J]();}});setTimeout(function(){var e0X="clic";var X6J=e0X;X6J+=d8X;var N6J=E6w;N6J+=l7w;$(x0h)[N6J](X6J+namespace,function(e){var t0X="_hide";var U0X="targ";var T6J=U0X;T6J+=M5w;var W6J=X6w;W6J+=V2E;var I6J=f5w;I6J+=t5E;var o6J=o6b;o6J+=I7w;o6J+=v3X;var parents=$(e[f7b])[o6J]();if(!parents[I6J](that[C3h][F2h])[W6J]&&e[T6J]!==that[C3h][U2h][l2w]){that[t0X]();}});},A2w);},_writeOutput:function(focus){var E5X="getUTCDate";var w5X="Locale";var J0X="mom";var b0X="ormat";var x6J=X7w;x6J+=h0X;x6J+=J8w;x6J+=d2v.b2w;var F6J=Y0w;F6J+=b0X;var f6J=J0X;f6J+=A7w;f6J+=q5w;f6J+=w5X;var G6J=J8w;G6J+=d2v.b2w;G6J+=d2v.h2w;var K6J=b4b;K6J+=d2v.w7w;K6J+=A7w;K6J+=q5w;var A6J=d2v.w7w;A6J+=E6w;A6J+=x7w;A6J+=q5w;var date=this[D7w][d2v.E7w];var out=window[A6J]?window[K6J][G6J](date,undefined,this[d2v.h2w][f6J],this[d2v.h2w][V3X])[F6J](this[d2v.h2w][B1X]):date[z8X]()+w0b+this[Z8X](date[I7X]()+N2w)+w0b+this[Z8X](date[E5X]());this[C3h][x6J][a3h](out);if(focus){var p6J=W0X;p6J+=J8w;p6J+=d2v.b2w;var g6J=d2v.E7w;g6J+=E6w;g6J+=d2v.w7w;this[g6J][p6J][S3h]();}}});Editor[b4X][s5X]=l2w;Editor[b4X][n6J]={classPrefix:z5X,disableDays:F3h,firstDay:N2w,format:q6J,hoursAvailable:F3h,i18n:Editor[C1h][S0b][v6J],maxDate:F3h,minDate:F3h,minutesIncrement:N2w,momentStrict:I4h,momentLocale:Z5X,onChange:function(){},secondsIncrement:N2w,showWeekNumber:o4h,yearRange:A2w};(function(){var f3v="uploadMany";var D3v='upload.editor';var z3v='<span>';var m1v="_pi";var j1v="_picker";var Y1v="seFn";var L1v="datetime";var q1v="picker";var M1v="datepicker";var O1v='<input />';var t4v="_preChecked";var j4v="radio";var n4v='<div />';var F4v='<label for="';var f4v='_';var G4v='<input id="';var N4v="checkbox";var y4v="separator";var Z4v="_editor_val";var b9X="ipOpts";var h9X="_addOptions";var U9X="multiple";var R9X="opt";var Y9X="placeholder";var H9X="bled";var r9X="disa";var v9X="textarea";var g9X="password";var x9X="_inpu";var F9X="eI";var f9X="saf";var G9X="inpu";var K9X='<input/>';var A9X="Id";var W9X="_in";var I9X="readonly";var o9X="_val";var X9X="_v";var N9X="nput";var l9X="prop";var M9X="_i";var Y5X="_enabled";var l5X="ange";var D5X="_input";var y5X="ldTy";var d5X="Many";var l3w=m5b;l3w+=d5X;var L1w=A7w;L1w+=i8b;L1w+=d2v.E7w;var O1w=x1h;O1w+=q6w;O1w+=g1h;var K4w=p7b;K4w+=g1h;var A4w=N1b;A4w+=q6w;var G9J=x1h;G9J+=q1E;var y5J=c6X;y5J+=d2v.h2w;y5J+=d2v.b2w;var h0J=x1h;h0J+=q1E;var k0J=z9h;k0J+=d2v.E7w;k0J+=d2v.E7w;k0J+=u0w;var C0J=F5w;C0J+=y5X;C0J+=i7w;C0J+=A7w;var v0J=x1h;v0J+=d2v.b2w;v0J+=A7w;v0J+=g1h;var fieldTypes=Editor[i1h];function _buttonText(conf,text){var M5X='div.upload button';var O5X="Choose file...";var u5X="uploadText";var C6J=G7E;C6J+=X6w;if(text===F3h||text===undefined){text=conf[u5X]||O5X;}conf[D5X][G7b](M5X)[C6J](text);}function _commonUpload(editor,conf,dropCallback,multiple){var O9X='input[type=file]';var y9X=".rendered";var h5X="load";var S5X='div.drop';var a5X='div.drop span';var R5X="ragDropTe";var c5X="a file here to upload";var V5X="Drag and drop ";var m5X="gexit";var j5X="dragleave dra";var Q5X="gov";var L5X='<div class="drop"><span/></div>';var k5X='<div class="cell limitHide">';var H5X='<div class="cell clearValue">';var r5X='/>';var B5X='<input type="file" ';var i5X='<div class="cell upload limitHide">';var C5X='<div class="row">';var v5X='<div class="eu_table">';var q5X="oad\">";var n5X="<div class=\"editor_upl";var p5X=" cla";var g5X="<button";var x5X=" class=\"";var F5X=" class=\"row second\">";var f5X="\"cell";var G5X="v class=";var K5X="\"/>";var A5X="iv class=\"rendere";var T5X="iv>";var W5X="leRe";var I5X="agDrop";var o5X="dr";var X5X="lue butt";var N5X="v.clearVa";var F0J=e5h;F0J+=l5X;var f0J=E6w;f0J+=l7w;var A0J=v5w;A0J+=X7w;A0J+=d2v.h2w;A0J+=T4h;var T0J=E6w;T0J+=l7w;var W0J=k5w;W0J+=N5X;W0J+=X5X;W0J+=k3h;var P6J=o5X;P6J+=I5X;var S6J=X3E;S6J+=W5X;S6J+=d2v.J2w;S6J+=K0w;var a6J=c4h;a6J+=b1b;a6J+=d2v.E7w;a6J+=T5X;var R6J=c4h;R6J+=b1b;R6J+=d2v.E7w;R6J+=T5X;var c6J=Q5h;c6J+=A5X;c6J+=d2v.E7w;c6J+=K5X;var V6J=w6b;V6J+=G5X;V6J+=f5X;V6J+=s3b;var m6J=I1X;m6J+=F5X;var j6J=c4h;j6J+=b1b;j6J+=n0h;j6J+=L4h;var Q6J=c4h;Q6J+=b1b;Q6J+=n0h;Q6J+=L4h;var Y6J=c4h;Y6J+=f4X;Y6J+=J2X;Y6J+=x5X;var L6J=c4h;L6J+=b1b;L6J+=Z1X;var k6J=O5w;k6J+=X6w;k6J+=r2E;k6J+=Q6E;var H6J=g5X;H6J+=p5X;H6J+=a4h;var r6J=n5X;r6J+=q5X;var B6J=a7w;B6J+=f0b;B6J+=J2X;var i6J=Y0w;i6J+=g4h;i6J+=d2v.w7w;var btnClass=editor[p2h][i6J][B6J];var container=$(r6J+v5X+C5X+i5X+H6J+btnClass+N3b+B5X+(multiple?k6J:Y9w)+r5X+L6J+H5X+Y6J+btnClass+N3b+Q6J+j6J+m6J+k5X+L5X+W3h+V6J+c6J+W3h+R6J+W3h+a6J);conf[D5X]=container;conf[Y5X]=I4h;_buttonText(conf);if(window[S6J]&&conf[P6J]!==o4h){var D0J=d2v.E7w;D0J+=D4b;D0J+=Q5X;D0J+=J0w;var O0J=E6w;O0J+=l7w;var d0J=j5X;d0J+=m5X;var J6J=d2v.E7w;J6J+=I7w;J6J+=E6w;J6J+=i7w;var b6J=E6w;b6J+=l7w;var h6J=f5w;h6J+=l7w;h6J+=d2v.E7w;var t6J=V5X;t6J+=c5X;var U6J=d2v.E7w;U6J+=R5X;U6J+=j0b;var e6J=f5w;e6J+=l7w;e6J+=d2v.E7w;container[e6J](a5X)[d6h](conf[U6J]||t6J);var dragDrop=container[h6J](S5X);dragDrop[b6J](J6J,function(e){var t5X="original";var U5X="ransfer";var e5X="taT";var P5X="oveClass";if(conf[Y5X]){var Z0J=E6w;Z0J+=L6b;Z0J+=I7w;var z0J=N4X;z0J+=P5X;var s0J=d2v.E7w;s0J+=d2v.J2w;s0J+=e5X;s0J+=U5X;var E0J=t5X;E0J+=P3E;var w0J=J8w;w0J+=i7w;w0J+=h5X;Editor[w0J](editor,conf,e[E0J][s0J][y4h],_buttonText,dropCallback);dragDrop[z0J](Z0J);}return o4h;})[k3h](d0J,function(e){var b5X="_ena";var y0J=b5X;y0J+=b7w;y0J+=d2v.E7w;if(conf[y0J]){var u0J=Q8E;u0J+=I7w;dragDrop[Q2h](u0J);}return o4h;})[O0J](D0J,function(e){var J5X='over';if(conf[Y5X]){dragDrop[e3b](J5X);}return o4h;});editor[k3h](a7b,function(){var s9X="d drop.DTE_Uploa";var E9X="er.DTE_Uploa";var w9X="dragov";var l0J=w9X;l0J+=E9X;l0J+=s9X;l0J+=d2v.E7w;var M0J=e9b;M0J+=K5w;$(M0J)[k3h](l0J,function(e){return o4h;});})[k3h](l6h,function(){var d9X="_Up";var Z9X="_Upload drop.DTE";var z9X="dragover.DTE";var X0J=z9X;X0J+=Z9X;X0J+=d9X;X0J+=h5X;var N0J=a7w;N0J+=e4E;$(N0J)[X0h](X0J);});}else{var I0J=d2v.E7w;I0J+=Y5h;I0J+=y9X;var o0J=p3h;o0J+=X8w;o0J+=s0w;o0J+=i7w;container[e3b](o0J);container[L6h](container[G7b](I0J));}container[G7b](W0J)[T0J](A0J,function(){var u9X="plo";var G0J=D7w;G0J+=A7w;G0J+=d2v.b2w;var K0J=J8w;K0J+=u9X;K0J+=W6b;Editor[i1h][K0J][G0J][E6h](editor,conf,Y9w);});container[G7b](O9X)[f0J](F0J,function(){var x0J=Y0w;x0J+=X7w;x0J+=n6w;x0J+=D7w;Editor[m5b](editor,conf,this[x0J],_buttonText,function(ids){var p0J=m2X;p0J+=d2v.E7w;var g0J=d2v.h2w;g0J+=d2v.J2w;g0J+=X6w;g0J+=X6w;dropCallback[g0J](editor,ids);container[p0J](O9X)[a3h](Y9w);});});return container;}function _triggerChange(input){setTimeout(function(){var D9X="gg";var q0J=d2v.h2w;q0J+=k0w;q0J+=l5X;var n0J=d2v.b2w;n0J+=N6w;n0J+=D9X;n0J+=J0w;input[n0J](q0J,{editor:I4h,editorSet:I4h});},l2w);}var baseFieldType=$[v0J](I4h,{},Editor[Z6h][C0J],{get:function(conf){return conf[D5X][a3h]();},set:function(conf,val){var B0J=M9X;B0J+=z1h;B0J+=d2v.b2w;var i0J=M9X;i0J+=l7w;i0J+=C7h;i0J+=d2v.b2w;conf[i0J][a3h](val);_triggerChange(conf[B0J]);},enable:function(conf){conf[D5X][l9X](F8X,o4h);},disable:function(conf){var H0J=i7w;H0J+=I7w;H0J+=y8h;var r0J=M9X;r0J+=N9X;conf[r0J][H0J](F8X,I4h);},canReturnSubmit:function(conf,node){return I4h;}});fieldTypes[k0J]={create:function(conf){var Y0J=z9w;Y0J+=r1b;Y0J+=D8w;var L0J=X9X;L0J+=r1b;conf[L0J]=conf[Y0J];return F3h;},get:function(conf){var Q0J=X9X;Q0J+=r1b;return conf[Q0J];},set:function(conf,val){conf[o9X]=val;}};fieldTypes[I9X]=$[v3h](I4h,{},baseFieldType,{create:function(conf){var T9X="only";var a0J=W9X;a0J+=C7h;a0J+=d2v.b2w;var R0J=I7w;R0J+=A7w;R0J+=W6b;R0J+=T9X;var c0J=q6w;c0J+=j0b;var V0J=D7w;V0J+=A3E;V0J+=A7w;V0J+=A9X;var m0J=p7b;m0J+=g1h;var j0J=K0h;j0J+=I5b;conf[D5X]=$(K9X)[j0J]($[m0J]({id:Editor[V0J](conf[J1h]),type:c0J,readonly:R0J},conf[O2b]||{}));return conf[a0J][l2w];}});fieldTypes[d6h]=$[v3h](I4h,{},baseFieldType,{create:function(conf){var t0J=Z8w;t0J+=G9X;t0J+=d2v.b2w;var U0J=d2v.b2w;U0J+=A7w;U0J+=f7w;U0J+=d2v.b2w;var e0J=X7w;e0J+=d2v.E7w;var P0J=f9X;P0J+=F9X;P0J+=d2v.E7w;var S0J=x9X;S0J+=d2v.b2w;conf[S0J]=$(K9X)[O2b]($[v3h]({id:Editor[P0J](conf[e0J]),type:U0J},conf[O2b]||{}));return conf[t0J][l2w];}});fieldTypes[g9X]=$[h0J](I4h,{},baseFieldType,{create:function(conf){var q9X='password';var n9X="/>";var p9X="<input";var s5J=d2v.J2w;s5J+=d2v.b2w;s5J+=d2v.b2w;s5J+=I7w;var E5J=f9X;E5J+=A7w;E5J+=x5E;E5J+=d2v.E7w;var w5J=A7w;w5J+=f7w;w5J+=q1E;var J0J=d2v.J2w;J0J+=R5E;var b0J=p9X;b0J+=n9X;conf[D5X]=$(b0J)[J0J]($[w5J]({id:Editor[E5J](conf[J1h]),type:q9X},conf[s5J]||{}));return conf[D5X][l2w];}});fieldTypes[v9X]=$[v3h](I4h,{},baseFieldType,{create:function(conf){var C9X='<textarea/>';var d5J=d2v.J2w;d5J+=d2v.b2w;d5J+=d2v.b2w;d5J+=I7w;var Z5J=X7w;Z5J+=d2v.E7w;var z5J=d2v.J2w;z5J+=d2v.b2w;z5J+=d2v.b2w;z5J+=I7w;conf[D5X]=$(C9X)[z5J]($[v3h]({id:Editor[b1h](conf[Z5J])},conf[d5J]||{}));return conf[D5X][l2w];},canReturnSubmit:function(conf,node){return o4h;}});fieldTypes[y5J]=$[v3h](I4h,{},baseFieldType,{_addOptions:function(conf,opts,append){var V9X="ai";var m9X="ptionsP";var j9X="hidden";var Q9X="placeholderDisabled";var L9X="placeholderValue";var k9X="laceholderDisable";var B9X="_edi";var i9X="ceholder";var O5J=E6w;O5J+=A7E;O5J+=i5w;var u5J=Z8w;u5J+=W0X;u5J+=J8w;u5J+=d2v.b2w;var elOpts=conf[u5J][l2w][O5J];var countOffset=l2w;if(!append){var D5J=E5w;D5J+=i9X;elOpts[X4h]=l2w;if(conf[D5J]!==undefined){var N5J=B9X;N5J+=o7w;N5J+=I7w;N5J+=o9X;var l5J=r9X;l5J+=H9X;var M5J=i7w;M5J+=k9X;M5J+=d2v.E7w;var placeholderValue=conf[L9X]!==undefined?conf[L9X]:Y9w;countOffset+=N2w;elOpts[l2w]=new Option(conf[Y9X],placeholderValue);var disabled=conf[Q9X]!==undefined?conf[M5J]:I4h;elOpts[l2w][j9X]=disabled;elOpts[l2w][l5J]=disabled;elOpts[l2w][N5J]=placeholderValue;}}else{countOffset=elOpts[X4h];}if(opts){var X5J=E6w;X5J+=m9X;X5J+=V9X;X5J+=I7w;Editor[X5b](opts,conf[X5J],function(val,label,i,attr){var c9X="ditor_val";var o5J=I0w;o5J+=c9X;var option=new Option(label,val);option[o5J]=val;if(attr){$(option)[O2b](attr);}elOpts[i+countOffset]=option;});}},create:function(conf){var e9X='<select/>';var P9X="feId";var S9X=".d";var a9X="chan";var f5J=R9X;f5J+=G8h;f5J+=D7w;var G5J=c6X;G5J+=K8h;var A5J=a9X;A5J+=X5w;A5J+=S9X;A5J+=q6w;var T5J=d2v.J2w;T5J+=d2v.b2w;T5J+=d2v.b2w;T5J+=I7w;var W5J=N5w;W5J+=P9X;var I5J=d2v.J2w;I5J+=d2v.b2w;I5J+=d2v.b2w;I5J+=I7w;conf[D5X]=$(e9X)[I5J]($[v3h]({id:Editor[W5J](conf[J1h]),multiple:conf[U9X]===I4h},conf[T5J]||{}))[k3h](A5J,function(e,d){var t9X="_lastSet";if(!d||!d[Y0b]){var K5J=G1h;K5J+=X6w;K5J+=A7w;K5J+=K8h;conf[t9X]=fieldTypes[K5J][m0h](conf);}});fieldTypes[G5J][h9X](conf,conf[f5J]||conf[b9X]);return conf[D5X][l2w];},update:function(conf,options,append){var w4v="select";var J9X="Set";var F5J=Z8w;F5J+=d4b;F5J+=d7h;F5J+=J9X;fieldTypes[w4v][h9X](conf,options,append);var lastSet=conf[F5J];if(lastSet!==undefined){fieldTypes[w4v][J8h](conf,lastSet,I4h);}_triggerChange(conf[D5X]);},get:function(conf){var d4v="toArray";var z4v="lecte";var s4v="option:se";var E4v="ipl";var v5J=P8h;v5J+=c8w;v5J+=l4h;var n5J=d5w;n5J+=d2v.b2w;n5J+=E4v;n5J+=A7w;var p5J=i9h;p5J+=i7w;var g5J=s4v;g5J+=z4v;g5J+=d2v.E7w;var x5J=Z8w;x5J+=U2h;var val=conf[x5J][G7b](g5J)[p5J](function(){return this[Z4v];})[d4v]();if(conf[n5J]){var q5J=h6b;q5J+=E6w;q5J+=X7w;q5J+=l7w;return conf[y4v]?val[q5J](conf[y4v]):val;}return val[v5J]?val[l2w]:F3h;},set:function(conf,val,localUpdate){var l4v='option';var M4v="astSet";var D4v="tip";var O4v="arato";var u4v="sep";var Q5J=A7w;Q5J+=d2v.J2w;Q5J+=d2v.h2w;Q5J+=k0w;var Y5J=R9X;Y5J+=G8h;var L5J=m2X;L5J+=d2v.E7w;var k5J=M9X;k5J+=l7w;k5J+=I0X;var r5J=X7w;r5J+=b7h;r5J+=o2h;var B5J=u4v;B5J+=O4v;B5J+=I7w;var i5J=d5w;i5J+=D4v;i5J+=n6w;if(!localUpdate){var C5J=Z8w;C5J+=X6w;C5J+=M4v;conf[C5J]=val;}if(conf[i5J]&&conf[B5J]&&!$[r5J](val)){var H5J=o6h;H5J+=W4h;H5J+=d2v.b2w;val=typeof val===R7h?val[H5J](conf[y4v]):[];}else if(!$[t4b](val)){val=[val];}var i,len=val[X4h],found,allFound=o4h;var options=conf[k5J][L5J](l4v);conf[D5X][G7b](Y5J)[Q5J](function(){found=o4h;for(i=l2w;i<len;i++){if(this[Z4v]==val[i]){found=I4h;allFound=I4h;break;}}this[x8X]=found;});if(conf[Y9X]&&!allFound&&!conf[U9X]&&options[X4h]){options[l2w][x8X]=I4h;}if(!localUpdate){_triggerChange(conf[D5X]);}return allFound;},destroy:function(conf){var V5J=X8E;V5J+=E9w;V5J+=d2v.E7w;V5J+=q6w;var m5J=E6w;m5J+=H6b;var j5J=x9X;j5J+=d2v.b2w;conf[j5J][m5J](V5J);}});fieldTypes[N4v]=$[v3h](I4h,{},baseFieldType,{_addOptions:function(conf,opts,append){var X4v="ionsPair";var val,label;var jqInput=conf[D5X];var offset=l2w;if(!append){jqInput[o3X]();}else{var c5J=i3b;c5J+=k0w;offset=$(t2h,jqInput)[c5J];}if(opts){var R5J=R9X;R5J+=X4v;Editor[X5b](opts,conf[R5J],function(val,label,i,attr){var g4v="put:last";var x4v='input:last';var K4v='<div>';var A4v="safeI";var T4v="kbox\" />";var W4v="type=\"chec";var I4v="abel";var o4v="</l";var e5J=o4v;e5J+=I4v;e5J+=L4h;var P5J=X7w;P5J+=d2v.E7w;var S5J=O1h;S5J+=W4v;S5J+=T4v;var a5J=A4v;a5J+=d2v.E7w;jqInput[L6h](K4v+G4v+Editor[a5J](conf[J1h])+f4v+(i+offset)+S5J+F4v+Editor[b1h](conf[P5J])+f4v+(i+offset)+w3h+label+e5J+W3h);$(x4v,jqInput)[O2b](o5b,val)[l2w][Z4v]=val;if(attr){var t5J=d2v.J2w;t5J+=d2v.b2w;t5J+=d2v.b2w;t5J+=I7w;var U5J=X7w;U5J+=l7w;U5J+=g4v;$(U5J,jqInput)[t5J](attr);}});}},create:function(conf){var p4v="ipO";var b5J=p4v;b5J+=p1b;b5J+=D7w;var h5J=R9X;h5J+=M7w;h5J+=l7w;h5J+=D7w;conf[D5X]=$(n4v);fieldTypes[N4v][h9X](conf,conf[h5J]||conf[b5J]);return conf[D5X][l2w];},get:function(conf){var B4v="edValu";var i4v="unselec";var C4v="unselectedValue";var v4v='input:checked';var q4v="rator";var Z9J=h6b;Z9J+=E6w;Z9J+=J4b;var z9J=G1h;z9J+=o6b;z9J+=q4v;var J5J=Z8w;J5J+=J4b;J5J+=I0X;var out=[];var selected=conf[J5J][G7b](v4v);if(selected[X4h]){selected[i7h](function(){var w9J=i7w;w9J+=Q3E;out[w9J](this[Z4v]);});}else if(conf[C4v]!==undefined){var s9J=i4v;s9J+=d2v.b2w;s9J+=B4v;s9J+=A7w;var E9J=i7w;E9J+=J8w;E9J+=H8X;out[E9J](conf[s9J]);}return conf[y4v]===undefined||conf[z9J]===F3h?out:out[Z9J](conf[y4v]);},set:function(conf,val){var k4v='|';var H4v="spli";var r4v="sA";var M9J=A7w;M9J+=d2v.J2w;M9J+=d2v.h2w;M9J+=k0w;var D9J=X6w;D9J+=V2E;var u9J=X7w;u9J+=r4v;u9J+=I7w;u9J+=W5E;var y9J=X7w;y9J+=l7w;y9J+=I0X;var d9J=Z8w;d9J+=J4b;d9J+=i7w;d9J+=f0b;var jqInputs=conf[d9J][G7b](y9J);if(!$[u9J](val)&&typeof val===R7h){var O9J=H4v;O9J+=d2v.b2w;val=val[O9J](conf[y4v]||k4v);}else if(!$[t4b](val)){val=[val];}var i,len=val[D9J],found;jqInputs[M9J](function(){var L4v="check";var l9J=L4v;l9J+=e8w;found=o4h;for(i=l2w;i<len;i++){if(this[Z4v]==val[i]){found=I4h;break;}}this[l9J]=found;});_triggerChange(jqInputs);},enable:function(conf){var o9J=X7w;o9J+=l7w;o9J+=i7w;o9J+=f0b;var X9J=Y0w;X9J+=X7w;X9J+=l7w;X9J+=d2v.E7w;var N9J=Z8w;N9J+=J4b;N9J+=I0X;conf[N9J][X9J](o9J)[l9X](F8X,o4h);},disable:function(conf){var W9J=J4b;W9J+=i7w;W9J+=f0b;var I9J=f5w;I9J+=l7w;I9J+=d2v.E7w;conf[D5X][I9J](W9J)[l9X](F8X,I4h);},update:function(conf,options,append){var Q4v="checkb";var Y4v="ddOptions";var K9J=D7w;K9J+=A7w;K9J+=d2v.b2w;var A9J=I5h;A9J+=Y4v;var T9J=Q4v;T9J+=E6w;T9J+=f7w;var checkbox=fieldTypes[T9J];var currVal=checkbox[m0h](conf);checkbox[A9J](conf,options,append);checkbox[K9J](conf,currVal);}});fieldTypes[j4v]=$[G9J](I4h,{},baseFieldType,{_addOptions:function(conf,opts,append){var m4v="ionsPai";var val,label;var jqInput=conf[D5X];var offset=l2w;if(!append){var f9J=A7w;f9J+=d2v.w7w;f9J+=i7w;f9J+=a6w;jqInput[f9J]();}else{var F9J=A7h;F9J+=l4h;offset=$(t2h,jqInput)[F9J];}if(opts){var x9J=y8h;x9J+=d2v.b2w;x9J+=m4v;x9J+=I7w;Editor[X5b](opts,conf[x9J],function(val,label,i,attr){var P4v="ut:last";var S4v='" type="radio" name="';var a4v="l>";var R4v="abe";var c4v="/l";var V4v=":la";var r9J=T5b;r9J+=X6w;r9J+=D8w;var B9J=U2h;B9J+=V4v;B9J+=d7h;var i9J=S4h;i9J+=X7w;i9J+=z9w;i9J+=L4h;var C9J=c4h;C9J+=c4v;C9J+=R4v;C9J+=a4v;var v9J=l7w;v9J+=E1b;v9J+=A7w;var q9J=X7w;q9J+=d2v.E7w;var n9J=N5w;n9J+=Y0w;n9J+=F9X;n9J+=d2v.E7w;var p9J=c4h;p9J+=k5w;p9J+=z9w;p9J+=L4h;var g9J=z6h;g9J+=u0w;g9J+=d2v.E7w;jqInput[g9J](p9J+G4v+Editor[n9J](conf[q9J])+f4v+(i+offset)+S4v+conf[v9J]+N3b+F4v+Editor[b1h](conf[J1h])+f4v+(i+offset)+w3h+label+C9J+i9J);$(B9J,jqInput)[O2b](r9J,val)[l2w][Z4v]=val;if(attr){var k9J=d2v.J2w;k9J+=d2v.b2w;k9J+=I5b;var H9J=W0X;H9J+=P4v;$(H9J,jqInput)[k9J](attr);}});}},create:function(conf){var U4v="pti";var e4v="_add";var a9J=x9X;a9J+=d2v.b2w;var m9J=E6w;m9J+=i7w;m9J+=A7w;m9J+=l7w;var j9J=E6w;j9J+=l7w;var Q9J=E6w;Q9J+=W7E;var Y9J=e4v;Y9J+=q7b;Y9J+=U4v;Y9J+=G0b;var L9J=Z8w;L9J+=J4b;L9J+=I0X;conf[L9J]=$(n4v);fieldTypes[j4v][Y9J](conf,conf[Q9J]||conf[b9X]);this[j9J](m9J,function(){var c9J=W0X;c9J+=f0b;var V9J=M9X;V9J+=z1h;V9J+=d2v.b2w;conf[V9J][G7b](c9J)[i7h](function(){var h4v="ecked";if(this[t4v]){var R9J=d2v.h2w;R9J+=k0w;R9J+=h4v;this[R9J]=I4h;}});});return conf[a9J][l2w];},get:function(conf){var b4v="put:checked";var e9J=J4b;e9J+=b4v;var P9J=f5w;P9J+=l7w;P9J+=d2v.E7w;var S9J=M9X;S9J+=l7w;S9J+=C7h;S9J+=d2v.b2w;var el=conf[S9J][P9J](e9J);return el[X4h]?el[l2w][Z4v]:undefined;},set:function(conf,val){var J4v="put:chec";var w4w=J4b;w4w+=J4v;w4w+=T4h;w4w+=e8w;var h9J=A7w;h9J+=d2v.J2w;h9J+=d2v.h2w;h9J+=k0w;var t9J=J4b;t9J+=i7w;t9J+=J8w;t9J+=d2v.b2w;var U9J=Z8w;U9J+=X7w;U9J+=z1h;U9J+=d2v.b2w;var that=this;conf[U9J][G7b](t9J)[h9J](function(){var z1v="checked";var s1v="cked";var E1v="che";var w1v="reChecked";this[t4v]=o4h;if(this[Z4v]==val){var J9J=d0w;J9J+=w1v;var b9J=E1v;b9J+=s1v;this[b9J]=I4h;this[J9J]=I4h;}else{this[z1v]=o4h;this[t4v]=o4h;}});_triggerChange(conf[D5X][G7b](w4w));},enable:function(conf){var z4w=r9X;z4w+=Q5w;z4w+=e8w;var s4w=i7w;s4w+=I7w;s4w+=y8h;var E4w=Z8w;E4w+=J4b;E4w+=i7w;E4w+=f0b;conf[E4w][G7b](t2h)[s4w](z4w,o4h);},disable:function(conf){var y4w=X7w;y4w+=h0X;y4w+=f0b;var d4w=Y0w;d4w+=X7w;d4w+=g1h;var Z4w=W9X;Z4w+=I0X;conf[Z4w][d4w](y4w)[l9X](F8X,I4h);},update:function(conf,options,append){var u1v="dO";var y1v="_ad";var d1v="ter";var Z1v="value=";var T4w=a3h;T4w+=J8w;T4w+=A7w;var W4w=K0h;W4w+=d2v.b2w;W4w+=I7w;var I4w=P8h;I4w+=c8w;I4w+=d2v.b2w;I4w+=k0w;var o4w=U9w;o4w+=t9w;var X4w=f8E;X4w+=Z1v;X4w+=U9w;var N4w=l5b;N4w+=d1v;var l4w=D7w;l4w+=A7w;l4w+=d2v.b2w;var M4w=J4b;M4w+=i7w;M4w+=f0b;var D4w=f5w;D4w+=l7w;D4w+=d2v.E7w;var O4w=W9X;O4w+=I0X;var u4w=y1v;u4w+=u1v;u4w+=A7E;u4w+=i5w;var radio=fieldTypes[j4v];var currVal=radio[m0h](conf);radio[u4w](conf,options,append);var inputs=conf[O4w][D4w](M4w);radio[l4w](conf,inputs[N4w](X4w+currVal+o4w)[I4w]?currVal:inputs[F0b](l2w)[W4w](T4w));}});fieldTypes[A4w]=$[K4w](I4h,{},baseFieldType,{create:function(conf){var p1v='date';var g1v='type';var T1v="dateForm";var W1v="ker";var I1v="datepic";var o1v="FC_2822";var X1v="R";var N1v='jqueryui';var l1v="teFormat";var D1v='text';var c4w=Z8w;c4w+=J4b;c4w+=C7h;c4w+=d2v.b2w;var x4w=d2v.J2w;x4w+=d2v.b2w;x4w+=d2v.b2w;x4w+=I7w;var F4w=X7w;F4w+=d2v.E7w;var f4w=d2v.J2w;f4w+=R5E;var G4w=M9X;G4w+=N9X;conf[G4w]=$(O1v)[f4w]($[v3h]({id:Editor[b1h](conf[F4w]),type:D1v},conf[x4w]));if($[M1v]){var n4w=N1b;n4w+=l1v;var p4w=d2v.J2w;p4w+=Z9h;p4w+=r6w;p4w+=V3h;var g4w=Z8w;g4w+=W0X;g4w+=f0b;conf[g4w][p4w](N1v);if(!conf[n4w]){var C4w=X1v;C4w+=o1v;var v4w=I1v;v4w+=W1v;var q4w=T1v;q4w+=K0h;conf[q4w]=$[v4w][C4w];}setTimeout(function(){var x1v="dateFormat";var F1v="icker";var f1v="datep";var G1v="eImage";var K1v="er-div";var A1v="#ui-datepi";var j4w=l8h;j4w+=D7w;var Q4w=A1v;Q4w+=U8h;Q4w+=K1v;var H4w=Y1h;H4w+=G1v;var r4w=a7w;r4w+=W5w;r4w+=k0w;var B4w=x1h;B4w+=q6w;B4w+=g1h;var i4w=f1v;i4w+=F1v;$(conf[D5X])[i4w]($[B4w]({showOn:r4w,dateFormat:conf[x1v],buttonImage:conf[H4w],buttonImageOnly:I4h,onSelect:function(){var Y4w=d2v.h2w;Y4w+=X6w;Y4w+=X7w;Y4w+=U8h;var L4w=P1E;L4w+=P1b;var k4w=W9X;k4w+=C7h;k4w+=d2v.b2w;conf[k4w][L4w]()[Y4w]();}},conf[c3h]));$(Q4w)[j4w](q3h,K2h);},A2w);}else{var V4w=d2v.J2w;V4w+=b5E;V4w+=I7w;var m4w=W9X;m4w+=I0X;conf[m4w][V4w](g1v,p1v);}return conf[c4w][l2w];},set:function(conf,val){var v1v='hasDatepicker';var n1v="hasC";var a4w=n1v;a4w+=V3h;var R4w=Z7b;R4w+=q1v;if($[R4w]&&conf[D5X][a4w](v1v)){var S4w=M9X;S4w+=h0X;S4w+=f0b;conf[S4w][M1v](C6X,val)[X8E]();}else{var P4w=z9w;P4w+=d2v.J2w;P4w+=X6w;$(conf[D5X])[P4w](val);}},enable:function(conf){var C1v="isable";if($[M1v]){var e4w=M9X;e4w+=l7w;e4w+=i7w;e4w+=f0b;conf[e4w][M1v](Z8b);}else{var h4w=d2v.E7w;h4w+=C1v;h4w+=d2v.E7w;var t4w=t6w;t4w+=y8h;var U4w=M9X;U4w+=N9X;$(conf[U4w])[t4w](h4w,o4h);}},disable:function(conf){var i1v="isabl";if($[M1v]){var b4w=u7X;b4w+=n6w;conf[D5X][M1v](b4w);}else{var E1w=d2v.E7w;E1w+=i1v;E1w+=e8w;var w1w=t6w;w1w+=E6w;w1w+=i7w;var J4w=M9X;J4w+=N9X;$(conf[J4w])[w1w](E1w,I4h);}},owns:function(conf,node){var k1v="tepicker";var H1v="div.ui-da";var r1v="eader";var B1v="div.ui-datepicker-";var u1w=P8h;u1w+=e8h;u1w+=k0w;var y1w=B1v;y1w+=k0w;y1w+=r1v;var d1w=i7w;d1w+=d2v.J2w;d1w+=U4h;d1w+=I6b;var Z1w=X6w;Z1w+=A7w;Z1w+=l7w;Z1w+=p8E;var z1w=H1v;z1w+=k1v;var s1w=o6b;s1w+=U4h;s1w+=q5w;s1w+=D7w;return $(node)[s1w](z1w)[Z1w]||$(node)[d1w](y1w)[u1w]?I4h:o4h;}});fieldTypes[L1v]=$[O1w](I4h,{},baseFieldType,{create:function(conf){var V1v="keyInput";var Q1v="safe";var g1w=Z8w;g1w+=J4b;g1w+=I0X;var x1w=Z8w;x1w+=v5w;x1w+=E6w;x1w+=Y1v;var F1w=d2v.h2w;F1w+=X6w;F1w+=E6w;F1w+=G1h;var f1w=E6w;f1w+=l7w;var T1w=g0w;T1w+=X6w;T1w+=B6h;T1w+=c1h;var W1w=E6w;W1w+=i7w;W1w+=o8h;var I1w=X7w;I1w+=B4X;I1w+=l7w;var o1w=A7w;o1w+=Y7w;o1w+=g1h;var X1w=M9X;X1w+=h0X;X1w+=J8w;X1w+=d2v.b2w;var N1w=Z3b;N1w+=I7w;var l1w=d2v.b2w;l1w+=x1h;l1w+=d2v.b2w;var M1w=Q1v;M1w+=A9X;var D1w=d2v.J2w;D1w+=d2v.b2w;D1w+=I5b;conf[D5X]=$(O1v)[D1w]($[v3h](I4h,{id:Editor[M1w](conf[J1h]),type:l1w},conf[N1w]));conf[j1v]=new Editor[b4X](conf[X1w],$[o1w]({format:conf[B1X],i18n:this[I1w][L1v],onChange:function(){_triggerChange(conf[D5X]);}},conf[W1w]));conf[T1w]=function(){var A1w=m1v;A1w+=d2v.h2w;A1w+=T4h;A1w+=J0w;conf[A1w][A8b]();};if(conf[V1v]===o4h){var K1w=Z8w;K1w+=X7w;K1w+=h0X;K1w+=f0b;conf[K1w][k3h](L2E,function(e){var R1v="ntDefault";var c1v="preve";var G1w=c1v;G1w+=R1v;e[G1w]();});}this[f1w](F1w,conf[x1w]);return conf[g1w][l2w];},set:function(conf,val){var a1v="cke";var p1w=m1v;p1w+=a1v;p1w+=I7w;conf[p1w][a3h](val);_triggerChange(conf[D5X]);},owns:function(conf,node){var S1v="cker";var q1w=S3b;q1w+=i5w;var n1w=Z8w;n1w+=i7w;n1w+=X7w;n1w+=S1v;return conf[n1w][q1w](node);},errorMessage:function(conf,msg){var e1v="errorMsg";var P1v="pic";var v1w=Z8w;v1w+=P1v;v1w+=T4h;v1w+=J0w;conf[v1w][e1v](msg);},destroy:function(conf){var U1v="_pick";var r1w=U1v;r1w+=J0w;var B1w=E6w;B1w+=Y0w;B1w+=Y0w;var i1w=g0w;i1w+=X6w;i1w+=E6w;i1w+=Y1v;var C1w=E6w;C1w+=Y0w;C1w+=Y0w;this[C1w](l6h,conf[i1w]);conf[D5X][B1w](L2E);conf[r1w][F2b]();},minDate:function(conf,min){var H1w=d2v.w7w;H1w+=X7w;H1w+=l7w;conf[j1v][H1w](min);},maxDate:function(conf,max){var t1v="max";var k1w=Z8w;k1w+=q1v;conf[k1w][t1v](max);}});fieldTypes[m5b]=$[L1w](I4h,{},baseFieldType,{create:function(conf){var editor=this;var container=_commonUpload(editor,conf,function(val){var b1v='postUpload';var h1v="ldType";var Q1w=d2v.h2w;Q1w+=d2v.J2w;Q1w+=X6w;Q1w+=X6w;var Y1w=F5w;Y1w+=h1v;Y1w+=D7w;Editor[Y1w][m5b][J8h][Q1w](editor,conf,val[l2w]);editor[P5b](b1v,[conf[k1h],val[l2w]]);});return container;},get:function(conf){return conf[o9X];},set:function(conf,val){var O3v="dC";var u3v="noCle";var y3v="clearText";var d3v="Clear";var Z3v='</span>';var s3v="leText";var E3v=" file";var w3v='div.rendered';var J1v=".clearValue bu";var s3w=Z8w;s3w+=z9w;s3w+=d2v.J2w;s3w+=X6w;var E3w=G9X;E3w+=d2v.b2w;var w3w=Y0w;w3w+=X7w;w3w+=g1h;var J1w=Z8w;J1w+=W0X;J1w+=J8w;J1w+=d2v.b2w;var e1w=W2b;e1w+=X1E;e1w+=j0b;var P1w=n0h;P1w+=J1v;P1w+=b5E;P1w+=k3h;var S1w=m2X;S1w+=d2v.E7w;conf[o9X]=val;var container=conf[D5X];if(conf[u7h]){var m1w=X9X;m1w+=r1b;var j1w=Y0w;j1w+=O5h;var rendered=container[j1w](w3v);if(conf[m1w]){rendered[o7h](conf[u7h](conf[o9X]));}else{var a1w=Z6w;a1w+=E6w;a1w+=E3v;var R1w=p3h;R1w+=p8w;R1w+=X7w;R1w+=s3v;var c1w=D7h;c1w+=o3b;var V1w=h3b;V1w+=i7w;V1w+=d2v.b2w;V1w+=K5w;rendered[V1w]()[c1w](z3v+(conf[R1w]||a1w)+Z3v);}}var button=container[S1w](P1w);if(val&&conf[e1w]){var t1w=l7w;t1w+=E6w;t1w+=d3v;var U1w=M7h;U1w+=d2v.w7w;U1w+=X6w;button[U1w](conf[y3v]);container[Q2h](t1w);}else{var b1w=u3v;b1w+=n2b;var h1w=W6b;h1w+=O3v;h1w+=X6w;h1w+=O2h;container[h1w](b1w);}conf[J1w][w3w](E3w)[e3E](D3v,[conf[s3w]]);},enable:function(conf){var M3v="_enable";var u3w=M3v;u3w+=d2v.E7w;var y3w=r9X;y3w+=H9X;var d3w=i7w;d3w+=s0w;d3w+=i7w;var Z3w=X7w;Z3w+=l7w;Z3w+=C7h;Z3w+=d2v.b2w;var z3w=f5w;z3w+=g1h;conf[D5X][z3w](Z3w)[d3w](y3w,o4h);conf[u3w]=I4h;},disable:function(conf){var l3v="rop";var M3w=Z8w;M3w+=y7b;M3w+=e8w;var D3w=i7w;D3w+=l3v;var O3w=Y0w;O3w+=O5h;conf[D5X][O3w](t2h)[D3w](F8X,I4h);conf[M3w]=o4h;},canReturnSubmit:function(conf,node){return o4h;}});fieldTypes[l3w]=$[v3h](I4h,{},baseFieldType,{_showHide:function(conf){var I3v="limit";var o3v='div.limitHide';var X3v="_container";var N3v="_va";var W3w=n6w;W3w+=l7w;W3w+=c8w;W3w+=l4h;var I3w=n6w;I3w+=l7w;I3w+=e8h;I3w+=k0w;var o3w=N3v;o3w+=X6w;var X3w=d2v.h2w;X3w+=D7w;X3w+=D7w;var N3w=W4h;N3w+=a1E;N3w+=d2v.b2w;if(!conf[N3w]){return;}conf[X3v][G7b](o3v)[X3w](q3h,conf[o3w][I3w]>=conf[I3v]?K2h:k7h);conf[F9b]=conf[I3v]-conf[o9X][W3w];},create:function(conf){var x3v='button.remove';var F3v='multi';var A3v="addClas";var T3v="lick";var W3v="_contai";var B3w=W3v;B3w+=Y6X;var g3w=d2v.h2w;g3w+=T3v;var x3w=A3v;x3w+=D7w;var editor=this;var container=_commonUpload(editor,conf,function(val){var G3v="cat";var K3v="postUplo";var F3w=K3v;F3w+=d2v.J2w;F3w+=d2v.E7w;var f3w=Z8w;f3w+=b8E;f3w+=u0w;f3w+=d2v.b2w;var G3w=d2v.h2w;G3w+=r1b;G3w+=X6w;var K3w=I2h;K3w+=l7w;K3w+=G3v;var A3w=X9X;A3w+=r1b;var T3w=Z8w;T3w+=z9w;T3w+=d2v.J2w;T3w+=X6w;conf[T3w]=conf[A3w][K3w](val);Editor[i1h][f3v][J8h][G3w](editor,conf,conf[o9X]);editor[f3w](F3w,[conf[k1h],conf[o9X]]);},I4h);container[x3w](F3v)[k3h](g3w,x3v,function(e){var q3v="gation";var n3v="stopPropa";var p3v="dMany";var g3v="uplo";var i3w=Z8w;i3w+=z9w;i3w+=d2v.J2w;i3w+=X6w;var C3w=t3h;C3w+=h3h;var v3w=g3v;v3w+=d2v.J2w;v3w+=p3v;var q3w=X9X;q3w+=r1b;var n3w=J1h;n3w+=f7w;var p3w=n3v;p3w+=q3v;e[p3w]();var idx=$(this)[r5w](n3w);conf[q3w][G5E](idx,N2w);Editor[i1h][v3w][J8h][C3w](editor,conf,conf[i3w]);});conf[B3w]=container;return container;},get:function(conf){return conf[o9X];},set:function(conf,val){var S3v="_showHide";var a3v='No files';var R3v="ileTe";var c3v="oF";var V3v="n>";var m3v="</sp";var r3v='<ul/>';var B3v="ndered";var i3v="v.r";var C3v="pty";var v3v='Upload collections must have an array as a value';var t3w=J4b;t3w+=i7w;t3w+=f0b;var U3w=Y0w;U3w+=X7w;U3w+=l7w;U3w+=d2v.E7w;var k3w=k5w;k3w+=L5w;var H3w=Z8w;H3w+=z9w;H3w+=d2v.J2w;H3w+=X6w;var r3w=F7h;r3w+=v6b;r3w+=o2h;if(!val){val=[];}if(!$[r3w](val)){throw v3v;}conf[H3w]=val;var that=this;var container=conf[D5X];if(conf[k3w]){var j3w=X6w;j3w+=A7w;j3w+=M4h;j3w+=l4h;var Q3w=h3b;Q3w+=C3v;var Y3w=k5w;Y3w+=i3v;Y3w+=A7w;Y3w+=B3v;var L3w=f5w;L3w+=g1h;var rendered=container[L3w](Y3w)[Q3w]();if(val[j3w]){var list=$(r3v)[p5h](rendered);$[i7h](val,function(i,file){var j3v='">&times;</button>';var Q3v=' <button class="';var Y3v="<li";var L3v="a-idx=\"";var k3v=" remove\" d";var H3v="/li>";var S3w=c4h;S3w+=H3v;var a3w=k3v;a3w+=d2v.J2w;a3w+=d2v.b2w;a3w+=L3v;var R3w=f4X;R3w+=J2X;var c3w=d2v.h2w;c3w+=X6w;c3w+=O2h;c3w+=E4h;var V3w=Y3v;V3w+=L4h;var m3w=D7h;m3w+=o3b;list[m3w](V3w+conf[u7h](file,i)+Q3v+that[c3w][Q2b][R3w]+a3w+i+j3v+S3w);});}else{var e3w=m3v;e3w+=d2v.J2w;e3w+=V3v;var P3w=l7w;P3w+=c3v;P3w+=R3v;P3w+=j0b;rendered[L6h](z3v+(conf[P3w]||a3v)+e3w);}}Editor[i1h][f3v][S3v](conf);conf[D5X][U3w](t3w)[e3E](D3v,[conf[o9X]]);},enable:function(conf){var e3v="_inp";var P3v="led";var w2w=k5w;w2w+=N5w;w2w+=a7w;w2w+=P3v;var J3w=i7w;J3w+=I7w;J3w+=E6w;J3w+=i7w;var b3w=J4b;b3w+=i7w;b3w+=f0b;var h3w=e3v;h3w+=f0b;conf[h3w][G7b](b3w)[J3w](w2w,o4h);conf[Y5X]=I4h;},disable:function(conf){var t3v="abled";var U3v="_en";var z2w=U3v;z2w+=t3v;var s2w=t6w;s2w+=E6w;s2w+=i7w;var E2w=G9X;E2w+=d2v.b2w;conf[D5X][G7b](E2w)[s2w](F8X,I4h);conf[z2w]=o4h;},canReturnSubmit:function(conf,node){return o4h;}});}());if(DataTable[Z2w][d2w]){var u2w=h3v;u2w+=b3v;var y2w=x1h;y2w+=q1E;$[y2w](Editor[u2w],DataTable[Q1h][J3v]);}DataTable[Q1h][J3v]=Editor[i1h];Editor[y4h]={};Editor[w2h][O2w]=D2w;Editor[M2w]=w2v;return Editor;}));

/*! Bootstrap integration for DataTables' Editor
 * ©2015 SpryMedia Ltd - datatables.net/license
 */

(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net-bs4', 'datatables.net-editor'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				$ = require('datatables.net-bs4')(root, $).$;
			}

			if ( ! $.fn.dataTable.Editor ) {
				require('datatables.net-editor')(root, $);
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
'use strict';
var DataTable = $.fn.dataTable;


/*
 * Set the default display controller to be our bootstrap control 
 */
DataTable.Editor.defaults.display = "bootstrap";


/*
 * Alter the buttons that Editor adds to TableTools so they are suitable for bootstrap
 */
var i18nDefaults = DataTable.Editor.defaults.i18n;
i18nDefaults.create.title = '<h5 class="modal-title">'+i18nDefaults.create.title+'</h5>';
i18nDefaults.edit.title = '<h5 class="modal-title">'+i18nDefaults.edit.title+'</h5>';
i18nDefaults.remove.title = '<h5 class="modal-title">'+i18nDefaults.remove.title+'</h5>';

var tt = DataTable.TableTools;
if ( tt ) {
	tt.BUTTONS.editor_create.formButtons[0].className = "btn btn-primary";
	tt.BUTTONS.editor_edit.formButtons[0].className = "btn btn-primary";
	tt.BUTTONS.editor_remove.formButtons[0].className = "btn btn-danger";
}


/*
 * Change the default classes from Editor to be classes for Bootstrap
 */
$.extend( true, $.fn.dataTable.Editor.classes, {
	"header": {
		"wrapper": "DTE_Header modal-header"
	},
	"body": {
		"wrapper": "DTE_Body modal-body"
	},
	"footer": {
		"wrapper": "DTE_Footer modal-footer"
	},
	"form": {
		"tag": "form-horizontal",
		"button": "btn btn-outline-secondary"
	},
	"field": {
		"wrapper": "DTE_Field form-group row",
		"label":   "col-lg-4 col-form-label",
		"input":   "col-lg-8",
		"error":   "error is-invalid",
		"msg-labelInfo": "form-text text-secondary small",
		"msg-info":      "form-text text-secondary small",
		"msg-message":   "form-text text-secondary small",
		"msg-error":     "form-text text-danger small",
		"multiValue":    "card multi-value",
		"multiInfo":     "small",
		"multiRestore":  "card multi-restore"
	}
} );

$.extend( true, DataTable.ext.buttons, {
	create: {
		formButtons: {
			className: 'btn-primary'
		}
	},
	edit: {
		formButtons: {
			className: 'btn-primary'
		}
	},
	remove: {
		formButtons: {
			className: 'btn-danger'
		}
	}
} );


/*
 * Bootstrap display controller - this is effectively a proxy to the Bootstrap
 * modal control.
 */

var self;

DataTable.Editor.display.bootstrap = $.extend( true, {}, DataTable.Editor.models.displayController, {
	/*
	 * API methods
	 */
	"init": function ( dte ) {
		// init can be called multiple times (one for each Editor instance), but
		// we only support a single construct here (shared between all Editor
		// instances)
		if ( ! self._dom.content ) {
			self._dom.content = $(
				'<div class="modal fade DTED">'+
					'<div class="modal-dialog">'+
						'<div class="modal-content"/>'+
					'</div>'+
				'</div>'
			);

			self._dom.close = $('<button class="close">&times;</div>');

			self._dom.close.click( function () {
				self._dte.close('icon');
			} );

			$(document).on('click', 'div.modal', function (e) {
				if ( $(e.target).hasClass('modal') && self._shown ) {
					self._dte.background();
				}
			} );
		}

		// Add `form-control` to required elements
		dte.on( 'displayOrder.dtebs', function ( e, display, action, form ) {
			$.each( dte.s.fields, function ( key, field ) {
				$('input:not([type=checkbox]):not([type=radio]), select, textarea', field.node() )
					.addClass( 'form-control' );
			} );
		} );

		return self;
	},

	"open": function ( dte, append, callback ) {
		if ( self._shown ) {
			if ( callback ) {
				callback();
			}
			return;
		}

		self._dte = dte;
		self._shown = true;
		self._fullyDisplayed = false;

		var content = self._dom.content.find('div.modal-content');
		content.children().detach();
		content.append( append );

		$('div.modal-header', append).append( self._dom.close );

		$(self._dom.content)
			.one('shown.bs.modal', function () {
				// Can only give elements focus when shown
				if ( self._dte.s.setFocus ) {
					self._dte.s.setFocus.focus();
				}

				self._fullyDisplayed = true;

				if ( callback ) {
					callback();
				}
			})
			.one('hidden', function () {
				self._shown = false;
			})
			.appendTo( 'body' )
			.modal( {
				backdrop: "static",
				keyboard: false
			} );
	},

	"close": function ( dte, callback ) {
		if ( !self._shown ) {
			if ( callback ) {
				callback();
			}
			return;
		}

		// Check if actually displayed or not before hiding. BS4 doesn't like `hide`
		// before it has been fully displayed
		if ( ! self._fullyDisplayed ) {
			$(self._dom.content)
				.one('shown.bs.modal', function () {
					self.close( dte, callback );
				} );

			return;
		}

		$(self._dom.content)
			.one( 'hidden.bs.modal', function () {
				$(this).detach();
			} )
			.modal('hide');

		self._dte = dte;
		self._shown = false;
		self._fullyDisplayed = false;

		if ( callback ) {
			callback();
		}
	},

	node: function ( dte ) {
		return self._dom.content[0];
	},


	/*
	 * Private properties
	 */
	 "_shown": false,
	"_dte": null,
	"_dom": {}
} );

self = DataTable.Editor.display.bootstrap;


return DataTable.Editor;
}));


/*! Responsive 2.2.2
 * 2014-2018 SpryMedia Ltd - datatables.net/license
 */

/**
 * @summary     Responsive
 * @description Responsive tables plug-in for DataTables
 * @version     2.2.2
 * @file        dataTables.responsive.js
 * @author      SpryMedia Ltd (www.sprymedia.co.uk)
 * @contact     www.sprymedia.co.uk/contact
 * @copyright   Copyright 2014-2018 SpryMedia Ltd.
 *
 * This source file is free software, available under the following license:
 *   MIT license - http://datatables.net/license/mit
 *
 * This source file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 *
 * For details please refer to: http://www.datatables.net
 */
(function( factory ){
	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery', 'datatables.net'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ || ! $.fn.dataTable ) {
				$ = require('datatables.net')(root, $).$;
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}(function( $, window, document, undefined ) {
'use strict';
var DataTable = $.fn.dataTable;


/**
 * Responsive is a plug-in for the DataTables library that makes use of
 * DataTables' ability to change the visibility of columns, changing the
 * visibility of columns so the displayed columns fit into the table container.
 * The end result is that complex tables will be dynamically adjusted to fit
 * into the viewport, be it on a desktop, tablet or mobile browser.
 *
 * Responsive for DataTables has two modes of operation, which can used
 * individually or combined:
 *
 * * Class name based control - columns assigned class names that match the
 *   breakpoint logic can be shown / hidden as required for each breakpoint.
 * * Automatic control - columns are automatically hidden when there is no
 *   room left to display them. Columns removed from the right.
 *
 * In additional to column visibility control, Responsive also has built into
 * options to use DataTables' child row display to show / hide the information
 * from the table that has been hidden. There are also two modes of operation
 * for this child row display:
 *
 * * Inline - when the control element that the user can use to show / hide
 *   child rows is displayed inside the first column of the table.
 * * Column - where a whole column is dedicated to be the show / hide control.
 *
 * Initialisation of Responsive is performed by:
 *
 * * Adding the class `responsive` or `dt-responsive` to the table. In this case
 *   Responsive will automatically be initialised with the default configuration
 *   options when the DataTable is created.
 * * Using the `responsive` option in the DataTables configuration options. This
 *   can also be used to specify the configuration options, or simply set to
 *   `true` to use the defaults.
 *
 *  @class
 *  @param {object} settings DataTables settings object for the host table
 *  @param {object} [opts] Configuration options
 *  @requires jQuery 1.7+
 *  @requires DataTables 1.10.3+
 *
 *  @example
 *      $('#example').DataTable( {
 *        responsive: true
 *      } );
 *    } );
 */
var Responsive = function ( settings, opts ) {
	// Sanity check that we are using DataTables 1.10 or newer
	if ( ! DataTable.versionCheck || ! DataTable.versionCheck( '1.10.10' ) ) {
		throw 'DataTables Responsive requires DataTables 1.10.10 or newer';
	}

	this.s = {
		dt: new DataTable.Api( settings ),
		columns: [],
		current: []
	};

	// Check if responsive has already been initialised on this table
	if ( this.s.dt.settings()[0].responsive ) {
		return;
	}

	// details is an object, but for simplicity the user can give it as a string
	// or a boolean
	if ( opts && typeof opts.details === 'string' ) {
		opts.details = { type: opts.details };
	}
	else if ( opts && opts.details === false ) {
		opts.details = { type: false };
	}
	else if ( opts && opts.details === true ) {
		opts.details = { type: 'inline' };
	}

	this.c = $.extend( true, {}, Responsive.defaults, DataTable.defaults.responsive, opts );
	settings.responsive = this;
	this._constructor();
};

$.extend( Responsive.prototype, {
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Constructor
	 */

	/**
	 * Initialise the Responsive instance
	 *
	 * @private
	 */
	_constructor: function ()
	{
		var that = this;
		var dt = this.s.dt;
		var dtPrivateSettings = dt.settings()[0];
		var oldWindowWidth = $(window).width();

		dt.settings()[0]._responsive = this;

		// Use DataTables' throttle function to avoid processor thrashing on
		// resize
		$(window).on( 'resize.dtr orientationchange.dtr', DataTable.util.throttle( function () {
			// iOS has a bug whereby resize can fire when only scrolling
			// See: http://stackoverflow.com/questions/8898412
			var width = $(window).width();

			if ( width !== oldWindowWidth ) {
				that._resize();
				oldWindowWidth = width;
			}
		} ) );

		// DataTables doesn't currently trigger an event when a row is added, so
		// we need to hook into its private API to enforce the hidden rows when
		// new data is added
		dtPrivateSettings.oApi._fnCallbackReg( dtPrivateSettings, 'aoRowCreatedCallback', function (tr, data, idx) {
			if ( $.inArray( false, that.s.current ) !== -1 ) {
				$('>td, >th', tr).each( function ( i ) {
					var idx = dt.column.index( 'toData', i );

					if ( that.s.current[idx] === false ) {
						$(this).css('display', 'none');
					}
				} );
			}
		} );

		// Destroy event handler
		dt.on( 'destroy.dtr', function () {
			dt.off( '.dtr' );
			$( dt.table().body() ).off( '.dtr' );
			$(window).off( 'resize.dtr orientationchange.dtr' );

			// Restore the columns that we've hidden
			$.each( that.s.current, function ( i, val ) {
				if ( val === false ) {
					that._setColumnVis( i, true );
				}
			} );
		} );

		// Reorder the breakpoints array here in case they have been added out
		// of order
		this.c.breakpoints.sort( function (a, b) {
			return a.width < b.width ? 1 :
				a.width > b.width ? -1 : 0;
		} );

		this._classLogic();
		this._resizeAuto();

		// Details handler
		var details = this.c.details;

		if ( details.type !== false ) {
			that._detailsInit();

			// DataTables will trigger this event on every column it shows and
			// hides individually
			dt.on( 'column-visibility.dtr', function () {
				// Use a small debounce to allow multiple columns to be set together
				if ( that._timer ) {
					clearTimeout( that._timer );
				}

				that._timer = setTimeout( function () {
					that._timer = null;

					that._classLogic();
					that._resizeAuto();
					that._resize();

					that._redrawChildren();
				}, 100 );
			} );

			// Redraw the details box on each draw which will happen if the data
			// has changed. This is used until DataTables implements a native
			// `updated` event for rows
			dt.on( 'draw.dtr', function () {
				that._redrawChildren();
			} );

			$(dt.table().node()).addClass( 'dtr-'+details.type );
		}

		dt.on( 'column-reorder.dtr', function (e, settings, details) {
			that._classLogic();
			that._resizeAuto();
			that._resize();
		} );

		// Change in column sizes means we need to calc
		dt.on( 'column-sizing.dtr', function () {
			that._resizeAuto();
			that._resize();
		});

		// On Ajax reload we want to reopen any child rows which are displayed
		// by responsive
		dt.on( 'preXhr.dtr', function () {
			var rowIds = [];
			dt.rows().every( function () {
				if ( this.child.isShown() ) {
					rowIds.push( this.id(true) );
				}
			} );

			dt.one( 'draw.dtr', function () {
				that._resizeAuto();
				that._resize();

				dt.rows( rowIds ).every( function () {
					that._detailsDisplay( this, false );
				} );
			} );
		});

		dt.on( 'init.dtr', function (e, settings, details) {
			that._resizeAuto();
			that._resize();

			// If columns were hidden, then DataTables needs to adjust the
			// column sizing
			if ( $.inArray( false, that.s.current ) ) {
				dt.columns.adjust();
			}
		} );

		// First pass - draw the table for the current viewport size
		this._resize();
	},


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Private methods
	 */

	/**
	 * Calculate the visibility for the columns in a table for a given
	 * breakpoint. The result is pre-determined based on the class logic if
	 * class names are used to control all columns, but the width of the table
	 * is also used if there are columns which are to be automatically shown
	 * and hidden.
	 *
	 * @param  {string} breakpoint Breakpoint name to use for the calculation
	 * @return {array} Array of boolean values initiating the visibility of each
	 *   column.
	 *  @private
	 */
	_columnsVisiblity: function ( breakpoint )
	{
		var dt = this.s.dt;
		var columns = this.s.columns;
		var i, ien;

		// Create an array that defines the column ordering based first on the
		// column's priority, and secondly the column index. This allows the
		// columns to be removed from the right if the priority matches
		var order = columns
			.map( function ( col, idx ) {
				return {
					columnIdx: idx,
					priority: col.priority
				};
			} )
			.sort( function ( a, b ) {
				if ( a.priority !== b.priority ) {
					return a.priority - b.priority;
				}
				return a.columnIdx - b.columnIdx;
			} );

		// Class logic - determine which columns are in this breakpoint based
		// on the classes. If no class control (i.e. `auto`) then `-` is used
		// to indicate this to the rest of the function
		var display = $.map( columns, function ( col, i ) {
			if ( dt.column(i).visible() === false ) {
				return 'not-visible';
			}
			return col.auto && col.minWidth === null ?
				false :
				col.auto === true ?
					'-' :
					$.inArray( breakpoint, col.includeIn ) !== -1;
		} );

		// Auto column control - first pass: how much width is taken by the
		// ones that must be included from the non-auto columns
		var requiredWidth = 0;
		for ( i=0, ien=display.length ; i<ien ; i++ ) {
			if ( display[i] === true ) {
				requiredWidth += columns[i].minWidth;
			}
		}

		// Second pass, use up any remaining width for other columns. For
		// scrolling tables we need to subtract the width of the scrollbar. It
		// may not be requires which makes this sub-optimal, but it would
		// require another full redraw to make complete use of those extra few
		// pixels
		var scrolling = dt.settings()[0].oScroll;
		var bar = scrolling.sY || scrolling.sX ? scrolling.iBarWidth : 0;
		var widthAvailable = dt.table().container().offsetWidth - bar;
		var usedWidth = widthAvailable - requiredWidth;

		// Control column needs to always be included. This makes it sub-
		// optimal in terms of using the available with, but to stop layout
		// thrashing or overflow. Also we need to account for the control column
		// width first so we know how much width is available for the other
		// columns, since the control column might not be the first one shown
		for ( i=0, ien=display.length ; i<ien ; i++ ) {
			if ( columns[i].control ) {
				usedWidth -= columns[i].minWidth;
			}
		}

		// Allow columns to be shown (counting by priority and then right to
		// left) until we run out of room
		var empty = false;
		for ( i=0, ien=order.length ; i<ien ; i++ ) {
			var colIdx = order[i].columnIdx;

			if ( display[colIdx] === '-' && ! columns[colIdx].control && columns[colIdx].minWidth ) {
				// Once we've found a column that won't fit we don't let any
				// others display either, or columns might disappear in the
				// middle of the table
				if ( empty || usedWidth - columns[colIdx].minWidth < 0 ) {
					empty = true;
					display[colIdx] = false;
				}
				else {
					display[colIdx] = true;
				}

				usedWidth -= columns[colIdx].minWidth;
			}
		}

		// Determine if the 'control' column should be shown (if there is one).
		// This is the case when there is a hidden column (that is not the
		// control column). The two loops look inefficient here, but they are
		// trivial and will fly through. We need to know the outcome from the
		// first , before the action in the second can be taken
		var showControl = false;

		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			if ( ! columns[i].control && ! columns[i].never && display[i] === false ) {
				showControl = true;
				break;
			}
		}

		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			if ( columns[i].control ) {
				display[i] = showControl;
			}

			// Replace not visible string with false from the control column detection above
			if ( display[i] === 'not-visible' ) {
				display[i] = false;
			}
		}

		// Finally we need to make sure that there is at least one column that
		// is visible
		if ( $.inArray( true, display ) === -1 ) {
			display[0] = true;
		}

		return display;
	},


	/**
	 * Create the internal `columns` array with information about the columns
	 * for the table. This includes determining which breakpoints the column
	 * will appear in, based upon class names in the column, which makes up the
	 * vast majority of this method.
	 *
	 * @private
	 */
	_classLogic: function ()
	{
		var that = this;
		var calc = {};
		var breakpoints = this.c.breakpoints;
		var dt = this.s.dt;
		var columns = dt.columns().eq(0).map( function (i) {
			var column = this.column(i);
			var className = column.header().className;
			var priority = dt.settings()[0].aoColumns[i].responsivePriority;

			if ( priority === undefined ) {
				var dataPriority = $(column.header()).data('priority');

				priority = dataPriority !== undefined ?
					dataPriority * 1 :
					10000;
			}

			return {
				className: className,
				includeIn: [],
				auto:      false,
				control:   false,
				never:     className.match(/\bnever\b/) ? true : false,
				priority:  priority
			};
		} );

		// Simply add a breakpoint to `includeIn` array, ensuring that there are
		// no duplicates
		var add = function ( colIdx, name ) {
			var includeIn = columns[ colIdx ].includeIn;

			if ( $.inArray( name, includeIn ) === -1 ) {
				includeIn.push( name );
			}
		};

		var column = function ( colIdx, name, operator, matched ) {
			var size, i, ien;

			if ( ! operator ) {
				columns[ colIdx ].includeIn.push( name );
			}
			else if ( operator === 'max-' ) {
				// Add this breakpoint and all smaller
				size = that._find( name ).width;

				for ( i=0, ien=breakpoints.length ; i<ien ; i++ ) {
					if ( breakpoints[i].width <= size ) {
						add( colIdx, breakpoints[i].name );
					}
				}
			}
			else if ( operator === 'min-' ) {
				// Add this breakpoint and all larger
				size = that._find( name ).width;

				for ( i=0, ien=breakpoints.length ; i<ien ; i++ ) {
					if ( breakpoints[i].width >= size ) {
						add( colIdx, breakpoints[i].name );
					}
				}
			}
			else if ( operator === 'not-' ) {
				// Add all but this breakpoint
				for ( i=0, ien=breakpoints.length ; i<ien ; i++ ) {
					if ( breakpoints[i].name.indexOf( matched ) === -1 ) {
						add( colIdx, breakpoints[i].name );
					}
				}
			}
		};

		// Loop over each column and determine if it has a responsive control
		// class
		columns.each( function ( col, i ) {
			var classNames = col.className.split(' ');
			var hasClass = false;

			// Split the class name up so multiple rules can be applied if needed
			for ( var k=0, ken=classNames.length ; k<ken ; k++ ) {
				var className = $.trim( classNames[k] );

				if ( className === 'all' ) {
					// Include in all
					hasClass = true;
					col.includeIn = $.map( breakpoints, function (a) {
						return a.name;
					} );
					return;
				}
				else if ( className === 'none' || col.never ) {
					// Include in none (default) and no auto
					hasClass = true;
					return;
				}
				else if ( className === 'control' ) {
					// Special column that is only visible, when one of the other
					// columns is hidden. This is used for the details control
					hasClass = true;
					col.control = true;
					return;
				}

				$.each( breakpoints, function ( j, breakpoint ) {
					// Does this column have a class that matches this breakpoint?
					var brokenPoint = breakpoint.name.split('-');
					var re = new RegExp( '(min\\-|max\\-|not\\-)?('+brokenPoint[0]+')(\\-[_a-zA-Z0-9])?' );
					var match = className.match( re );

					if ( match ) {
						hasClass = true;

						if ( match[2] === brokenPoint[0] && match[3] === '-'+brokenPoint[1] ) {
							// Class name matches breakpoint name fully
							column( i, breakpoint.name, match[1], match[2]+match[3] );
						}
						else if ( match[2] === brokenPoint[0] && ! match[3] ) {
							// Class name matched primary breakpoint name with no qualifier
							column( i, breakpoint.name, match[1], match[2] );
						}
					}
				} );
			}

			// If there was no control class, then automatic sizing is used
			if ( ! hasClass ) {
				col.auto = true;
			}
		} );

		this.s.columns = columns;
	},


	/**
	 * Show the details for the child row
	 *
	 * @param  {DataTables.Api} row    API instance for the row
	 * @param  {boolean}        update Update flag
	 * @private
	 */
	_detailsDisplay: function ( row, update )
	{
		var that = this;
		var dt = this.s.dt;
		var details = this.c.details;

		if ( details && details.type !== false ) {
			var res = details.display( row, update, function () {
				return details.renderer(
					dt, row[0], that._detailsObj(row[0])
				);
			} );

			if ( res === true || res === false ) {
				$(dt.table().node()).triggerHandler( 'responsive-display.dt', [dt, row, res, update] );
			}
		}
	},


	/**
	 * Initialisation for the details handler
	 *
	 * @private
	 */
	_detailsInit: function ()
	{
		var that    = this;
		var dt      = this.s.dt;
		var details = this.c.details;

		// The inline type always uses the first child as the target
		if ( details.type === 'inline' ) {
			details.target = 'td:first-child, th:first-child';
		}

		// Keyboard accessibility
		dt.on( 'draw.dtr', function () {
			that._tabIndexes();
		} );
		that._tabIndexes(); // Initial draw has already happened

		$( dt.table().body() ).on( 'keyup.dtr', 'td, th', function (e) {
			if ( e.keyCode === 13 && $(this).data('dtr-keyboard') ) {
				$(this).click();
			}
		} );

		// type.target can be a string jQuery selector or a column index
		var target   = details.target;
		var selector = typeof target === 'string' ? target : 'td, th';

		// Click handler to show / hide the details rows when they are available
		$( dt.table().body() )
			.on( 'click.dtr mousedown.dtr mouseup.dtr', selector, function (e) {
				// If the table is not collapsed (i.e. there is no hidden columns)
				// then take no action
				if ( ! $(dt.table().node()).hasClass('collapsed' ) ) {
					return;
				}

				// Check that the row is actually a DataTable's controlled node
				if ( $.inArray( $(this).closest('tr').get(0), dt.rows().nodes().toArray() ) === -1 ) {
					return;
				}

				// For column index, we determine if we should act or not in the
				// handler - otherwise it is already okay
				if ( typeof target === 'number' ) {
					var targetIdx = target < 0 ?
						dt.columns().eq(0).length + target :
						target;

					if ( dt.cell( this ).index().column !== targetIdx ) {
						return;
					}
				}

				// $().closest() includes itself in its check
				var row = dt.row( $(this).closest('tr') );

				// Check event type to do an action
				if ( e.type === 'click' ) {
					// The renderer is given as a function so the caller can execute it
					// only when they need (i.e. if hiding there is no point is running
					// the renderer)
					that._detailsDisplay( row, false );
				}
				else if ( e.type === 'mousedown' ) {
					// For mouse users, prevent the focus ring from showing
					$(this).css('outline', 'none');
				}
				else if ( e.type === 'mouseup' ) {
					// And then re-allow at the end of the click
					$(this).blur().css('outline', '');
				}
			} );
	},


	/**
	 * Get the details to pass to a renderer for a row
	 * @param  {int} rowIdx Row index
	 * @private
	 */
	_detailsObj: function ( rowIdx )
	{
		var that = this;
		var dt = this.s.dt;

		return $.map( this.s.columns, function( col, i ) {
			// Never and control columns should not be passed to the renderer
			if ( col.never || col.control ) {
				return;
			}

			return {
				title:       dt.settings()[0].aoColumns[ i ].sTitle,
				data:        dt.cell( rowIdx, i ).render( that.c.orthogonal ),
				hidden:      dt.column( i ).visible() && !that.s.current[ i ],
				columnIndex: i,
				rowIndex:    rowIdx
			};
		} );
	},


	/**
	 * Find a breakpoint object from a name
	 *
	 * @param  {string} name Breakpoint name to find
	 * @return {object}      Breakpoint description object
	 * @private
	 */
	_find: function ( name )
	{
		var breakpoints = this.c.breakpoints;

		for ( var i=0, ien=breakpoints.length ; i<ien ; i++ ) {
			if ( breakpoints[i].name === name ) {
				return breakpoints[i];
			}
		}
	},


	/**
	 * Re-create the contents of the child rows as the display has changed in
	 * some way.
	 *
	 * @private
	 */
	_redrawChildren: function ()
	{
		var that = this;
		var dt = this.s.dt;

		dt.rows( {page: 'current'} ).iterator( 'row', function ( settings, idx ) {
			var row = dt.row( idx );

			that._detailsDisplay( dt.row( idx ), true );
		} );
	},


	/**
	 * Alter the table display for a resized viewport. This involves first
	 * determining what breakpoint the window currently is in, getting the
	 * column visibilities to apply and then setting them.
	 *
	 * @private
	 */
	_resize: function ()
	{
		var that = this;
		var dt = this.s.dt;
		var width = $(window).width();
		var breakpoints = this.c.breakpoints;
		var breakpoint = breakpoints[0].name;
		var columns = this.s.columns;
		var i, ien;
		var oldVis = this.s.current.slice();

		// Determine what breakpoint we are currently at
		for ( i=breakpoints.length-1 ; i>=0 ; i-- ) {
			if ( width <= breakpoints[i].width ) {
				breakpoint = breakpoints[i].name;
				break;
			}
		}
		
		// Show the columns for that break point
		var columnsVis = this._columnsVisiblity( breakpoint );
		this.s.current = columnsVis;

		// Set the class before the column visibility is changed so event
		// listeners know what the state is. Need to determine if there are
		// any columns that are not visible but can be shown
		var collapsedClass = false;
		for ( i=0, ien=columns.length ; i<ien ; i++ ) {
			if ( columnsVis[i] === false && ! columns[i].never && ! columns[i].control && ! dt.column(i).visible() === false ) {
				collapsedClass = true;
				break;
			}
		}

		$( dt.table().node() ).toggleClass( 'collapsed', collapsedClass );

		var changed = false;
		var visible = 0;

		dt.columns().eq(0).each( function ( colIdx, i ) {
			if ( columnsVis[i] === true ) {
				visible++;
			}

			if ( columnsVis[i] !== oldVis[i] ) {
				changed = true;
				that._setColumnVis( colIdx, columnsVis[i] );
			}
		} );

		if ( changed ) {
			this._redrawChildren();

			// Inform listeners of the change
			$(dt.table().node()).trigger( 'responsive-resize.dt', [dt, this.s.current] );

			// If no records, update the "No records" display element
			if ( dt.page.info().recordsDisplay === 0 ) {
				$('td', dt.table().body()).eq(0).attr('colspan', visible);
			}
		}
	},


	/**
	 * Determine the width of each column in the table so the auto column hiding
	 * has that information to work with. This method is never going to be 100%
	 * perfect since column widths can change slightly per page, but without
	 * seriously compromising performance this is quite effective.
	 *
	 * @private
	 */
	_resizeAuto: function ()
	{
		var dt = this.s.dt;
		var columns = this.s.columns;

		// Are we allowed to do auto sizing?
		if ( ! this.c.auto ) {
			return;
		}

		// Are there any columns that actually need auto-sizing, or do they all
		// have classes defined
		if ( $.inArray( true, $.map( columns, function (c) { return c.auto; } ) ) === -1 ) {
			return;
		}

		// Need to restore all children. They will be reinstated by a re-render
		if ( ! $.isEmptyObject( _childNodeStore ) ) {
			$.each( _childNodeStore, function ( key ) {
				var idx = key.split('-');

				_childNodesRestore( dt, idx[0]*1, idx[1]*1 );
			} );
		}

		// Clone the table with the current data in it
		var tableWidth   = dt.table().node().offsetWidth;
		var columnWidths = dt.columns;
		var clonedTable  = dt.table().node().cloneNode( false );
		var clonedHeader = $( dt.table().header().cloneNode( false ) ).appendTo( clonedTable );
		var clonedBody   = $( dt.table().body() ).clone( false, false ).empty().appendTo( clonedTable ); // use jQuery because of IE8

		// Header
		var headerCells = dt.columns()
			.header()
			.filter( function (idx) {
				return dt.column(idx).visible();
			} )
			.to$()
			.clone( false )
			.css( 'display', 'table-cell' )
			.css( 'min-width', 0 );

		// Body rows - we don't need to take account of DataTables' column
		// visibility since we implement our own here (hence the `display` set)
		$(clonedBody)
			.append( $(dt.rows( { page: 'current' } ).nodes()).clone( false ) )
			.find( 'th, td' ).css( 'display', '' );

		// Footer
		var footer = dt.table().footer();
		if ( footer ) {
			var clonedFooter = $( footer.cloneNode( false ) ).appendTo( clonedTable );
			var footerCells = dt.columns()
				.footer()
				.filter( function (idx) {
					return dt.column(idx).visible();
				} )
				.to$()
				.clone( false )
				.css( 'display', 'table-cell' );

			$('<tr/>')
				.append( footerCells )
				.appendTo( clonedFooter );
		}

		$('<tr/>')
			.append( headerCells )
			.appendTo( clonedHeader );

		// In the inline case extra padding is applied to the first column to
		// give space for the show / hide icon. We need to use this in the
		// calculation
		if ( this.c.details.type === 'inline' ) {
			$(clonedTable).addClass( 'dtr-inline collapsed' );
		}
		
		// It is unsafe to insert elements with the same name into the DOM
		// multiple times. For example, cloning and inserting a checked radio
		// clears the chcecked state of the original radio.
		$( clonedTable ).find( '[name]' ).removeAttr( 'name' );

		// A position absolute table would take the table out of the flow of
		// our container element, bypassing the height and width (Scroller)
		$( clonedTable ).css( 'position', 'relative' )
		
		var inserted = $('<div/>')
			.css( {
				width: 1,
				height: 1,
				overflow: 'hidden',
				clear: 'both'
			} )
			.append( clonedTable );

		inserted.insertBefore( dt.table().node() );

		// The cloned header now contains the smallest that each column can be
		headerCells.each( function (i) {
			var idx = dt.column.index( 'fromVisible', i );
			columns[ idx ].minWidth =  this.offsetWidth || 0;
		} );

		inserted.remove();
	},

	/**
	 * Set a column's visibility.
	 *
	 * We don't use DataTables' column visibility controls in order to ensure
	 * that column visibility can Responsive can no-exist. Since only IE8+ is
	 * supported (and all evergreen browsers of course) the control of the
	 * display attribute works well.
	 *
	 * @param {integer} col      Column index
	 * @param {boolean} showHide Show or hide (true or false)
	 * @private
	 */
	_setColumnVis: function ( col, showHide )
	{
		var dt = this.s.dt;
		var display = showHide ? '' : 'none'; // empty string will remove the attr

		$( dt.column( col ).header() ).css( 'display', display );
		$( dt.column( col ).footer() ).css( 'display', display );
		dt.column( col ).nodes().to$().css( 'display', display );

		// If the are child nodes stored, we might need to reinsert them
		if ( ! $.isEmptyObject( _childNodeStore ) ) {
			dt.cells( null, col ).indexes().each( function (idx) {
				_childNodesRestore( dt, idx.row, idx.column );
			} );
		}
	},


	/**
	 * Update the cell tab indexes for keyboard accessibility. This is called on
	 * every table draw - that is potentially inefficient, but also the least
	 * complex option given that column visibility can change on the fly. Its a
	 * shame user-focus was removed from CSS 3 UI, as it would have solved this
	 * issue with a single CSS statement.
	 *
	 * @private
	 */
	_tabIndexes: function ()
	{
		var dt = this.s.dt;
		var cells = dt.cells( { page: 'current' } ).nodes().to$();
		var ctx = dt.settings()[0];
		var target = this.c.details.target;

		cells.filter( '[data-dtr-keyboard]' ).removeData( '[data-dtr-keyboard]' );

		if ( typeof target === 'number' ) {
			dt.cells( null, target, { page: 'current' } ).nodes().to$()
				.attr( 'tabIndex', ctx.iTabIndex )
				.data( 'dtr-keyboard', 1 );
		}
		else {
			// This is a bit of a hack - we need to limit the selected nodes to just
			// those of this table
			if ( target === 'td:first-child, th:first-child' ) {
				target = '>td:first-child, >th:first-child';
			}

			$( target, dt.rows( { page: 'current' } ).nodes() )
				.attr( 'tabIndex', ctx.iTabIndex )
				.data( 'dtr-keyboard', 1 );
		}
	}
} );


/**
 * List of default breakpoints. Each item in the array is an object with two
 * properties:
 *
 * * `name` - the breakpoint name.
 * * `width` - the breakpoint width
 *
 * @name Responsive.breakpoints
 * @static
 */
Responsive.breakpoints = [
	{ name: 'desktop',  width: Infinity },
	{ name: 'tablet-l', width: 1024 },
	{ name: 'tablet-p', width: 768 },
	{ name: 'mobile-l', width: 480 },
	{ name: 'mobile-p', width: 320 }
];


/**
 * Display methods - functions which define how the hidden data should be shown
 * in the table.
 *
 * @namespace
 * @name Responsive.defaults
 * @static
 */
Responsive.display = {
	childRow: function ( row, update, render ) {
		if ( update ) {
			if ( $(row.node()).hasClass('parent') ) {
				row.child( render(), 'child' ).show();

				return true;
			}
		}
		else {
			if ( ! row.child.isShown()  ) {
				row.child( render(), 'child' ).show();
				$( row.node() ).addClass( 'parent' );

				return true;
			}
			else {
				row.child( false );
				$( row.node() ).removeClass( 'parent' );

				return false;
			}
		}
	},

	childRowImmediate: function ( row, update, render ) {
		if ( (! update && row.child.isShown()) || ! row.responsive.hasHidden() ) {
			// User interaction and the row is show, or nothing to show
			row.child( false );
			$( row.node() ).removeClass( 'parent' );

			return false;
		}
		else {
			// Display
			row.child( render(), 'child' ).show();
			$( row.node() ).addClass( 'parent' );

			return true;
		}
	},

	// This is a wrapper so the modal options for Bootstrap and jQuery UI can
	// have options passed into them. This specific one doesn't need to be a
	// function but it is for consistency in the `modal` name
	modal: function ( options ) {
		return function ( row, update, render ) {
			if ( ! update ) {
				// Show a modal
				var close = function () {
					modal.remove(); // will tidy events for us
					$(document).off( 'keypress.dtr' );
				};

				var modal = $('<div class="dtr-modal"/>')
					.append( $('<div class="dtr-modal-display"/>')
						.append( $('<div class="dtr-modal-content"/>')
							.append( render() )
						)
						.append( $('<div class="dtr-modal-close">&times;</div>' )
							.click( function () {
								close();
							} )
						)
					)
					.append( $('<div class="dtr-modal-background"/>')
						.click( function () {
							close();
						} )
					)
					.appendTo( 'body' );

				$(document).on( 'keyup.dtr', function (e) {
					if ( e.keyCode === 27 ) {
						e.stopPropagation();

						close();
					}
				} );
			}
			else {
				$('div.dtr-modal-content')
					.empty()
					.append( render() );
			}

			if ( options && options.header ) {
				$('div.dtr-modal-content').prepend(
					'<h2>'+options.header( row )+'</h2>'
				);
			}
		};
	}
};


var _childNodeStore = {};

function _childNodes( dt, row, col ) {
	var name = row+'-'+col;

	if ( _childNodeStore[ name ] ) {
		return _childNodeStore[ name ];
	}

	// https://jsperf.com/childnodes-array-slice-vs-loop
	var nodes = [];
	var children = dt.cell( row, col ).node().childNodes;
	for ( var i=0, ien=children.length ; i<ien ; i++ ) {
		nodes.push( children[i] );
	}

	_childNodeStore[ name ] = nodes;

	return nodes;
}

function _childNodesRestore( dt, row, col ) {
	var name = row+'-'+col;

	if ( ! _childNodeStore[ name ] ) {
		return;
	}

	var node = dt.cell( row, col ).node();
	var store = _childNodeStore[ name ];
	var parent = store[0].parentNode;
	var parentChildren = parent.childNodes;
	var a = [];

	for ( var i=0, ien=parentChildren.length ; i<ien ; i++ ) {
		a.push( parentChildren[i] );
	}

	for ( var j=0, jen=a.length ; j<jen ; j++ ) {
		node.appendChild( a[j] );
	}

	_childNodeStore[ name ] = undefined;
}


/**
 * Display methods - functions which define how the hidden data should be shown
 * in the table.
 *
 * @namespace
 * @name Responsive.defaults
 * @static
 */
Responsive.renderer = {
	listHiddenNodes: function () {
		return function ( api, rowIdx, columns ) {
			var ul = $('<ul data-dtr-index="'+rowIdx+'" class="dtr-details"/>');
			var found = false;

			var data = $.each( columns, function ( i, col ) {
				if ( col.hidden ) {
					$(
						'<li data-dtr-index="'+col.columnIndex+'" data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
							'<span class="dtr-title">'+
								col.title+
							'</span> '+
						'</li>'
					)
						.append( $('<span class="dtr-data"/>').append( _childNodes( api, col.rowIndex, col.columnIndex ) ) )// api.cell( col.rowIndex, col.columnIndex ).node().childNodes ) )
						.appendTo( ul );

					found = true;
				}
			} );

			return found ?
				ul :
				false;
		};
	},

	listHidden: function () {
		return function ( api, rowIdx, columns ) {
			var data = $.map( columns, function ( col ) {
				return col.hidden ?
					'<li data-dtr-index="'+col.columnIndex+'" data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
						'<span class="dtr-title">'+
							col.title+
						'</span> '+
						'<span class="dtr-data">'+
							col.data+
						'</span>'+
					'</li>' :
					'';
			} ).join('');

			return data ?
				$('<ul data-dtr-index="'+rowIdx+'" class="dtr-details"/>').append( data ) :
				false;
		}
	},

	tableAll: function ( options ) {
		options = $.extend( {
			tableClass: ''
		}, options );

		return function ( api, rowIdx, columns ) {
			var data = $.map( columns, function ( col ) {
				return '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
						'<td>'+col.title+':'+'</td> '+
						'<td>'+col.data+'</td>'+
					'</tr>';
			} ).join('');

			return $('<table class="'+options.tableClass+' dtr-details" width="100%"/>').append( data );
		}
	}
};

/**
 * Responsive default settings for initialisation
 *
 * @namespace
 * @name Responsive.defaults
 * @static
 */
Responsive.defaults = {
	/**
	 * List of breakpoints for the instance. Note that this means that each
	 * instance can have its own breakpoints. Additionally, the breakpoints
	 * cannot be changed once an instance has been creased.
	 *
	 * @type {Array}
	 * @default Takes the value of `Responsive.breakpoints`
	 */
	breakpoints: Responsive.breakpoints,

	/**
	 * Enable / disable auto hiding calculations. It can help to increase
	 * performance slightly if you disable this option, but all columns would
	 * need to have breakpoint classes assigned to them
	 *
	 * @type {Boolean}
	 * @default  `true`
	 */
	auto: true,

	/**
	 * Details control. If given as a string value, the `type` property of the
	 * default object is set to that value, and the defaults used for the rest
	 * of the object - this is for ease of implementation.
	 *
	 * The object consists of the following properties:
	 *
	 * * `display` - A function that is used to show and hide the hidden details
	 * * `renderer` - function that is called for display of the child row data.
	 *   The default function will show the data from the hidden columns
	 * * `target` - Used as the selector for what objects to attach the child
	 *   open / close to
	 * * `type` - `false` to disable the details display, `inline` or `column`
	 *   for the two control types
	 *
	 * @type {Object|string}
	 */
	details: {
		display: Responsive.display.childRow,

		renderer: Responsive.renderer.listHidden(),

		target: 0,

		type: 'inline'
	},

	/**
	 * Orthogonal data request option. This is used to define the data type
	 * requested when Responsive gets the data to show in the child row.
	 *
	 * @type {String}
	 */
	orthogonal: 'display'
};


/*
 * API
 */
var Api = $.fn.dataTable.Api;

// Doesn't do anything - work around for a bug in DT... Not documented
Api.register( 'responsive()', function () {
	return this;
} );

Api.register( 'responsive.index()', function ( li ) {
	li = $(li);

	return {
		column: li.data('dtr-index'),
		row:    li.parent().data('dtr-index')
	};
} );

Api.register( 'responsive.rebuild()', function () {
	return this.iterator( 'table', function ( ctx ) {
		if ( ctx._responsive ) {
			ctx._responsive._classLogic();
		}
	} );
} );

Api.register( 'responsive.recalc()', function () {
	return this.iterator( 'table', function ( ctx ) {
		if ( ctx._responsive ) {
			ctx._responsive._resizeAuto();
			ctx._responsive._resize();
		}
	} );
} );

Api.register( 'responsive.hasHidden()', function () {
	var ctx = this.context[0];

	return ctx._responsive ?
		$.inArray( false, ctx._responsive.s.current ) !== -1 :
		false;
} );

Api.registerPlural( 'columns().responsiveHidden()', 'column().responsiveHidden()', function () {
	return this.iterator( 'column', function ( settings, column ) {
		return settings._responsive ?
			settings._responsive.s.current[ column ] :
			false;
	}, 1 );
} );


/**
 * Version information
 *
 * @name Responsive.version
 * @static
 */
Responsive.version = '2.2.2';


$.fn.dataTable.Responsive = Responsive;
$.fn.DataTable.Responsive = Responsive;

// Attach a listener to the document which listens for DataTables initialisation
// events so we can automatically initialise
$(document).on( 'preInit.dt.dtr', function (e, settings, json) {
	if ( e.namespace !== 'dt' ) {
		return;
	}

	if ( $(settings.nTable).hasClass( 'responsive' ) ||
		 $(settings.nTable).hasClass( 'dt-responsive' ) ||
		 settings.oInit.responsive ||
		 DataTable.defaults.responsive
	) {
		var init = settings.oInit.responsive;

		if ( init !== false ) {
			new Responsive( settings, $.isPlainObject( init ) ? init : {}  );
		}
	}
} );


return Responsive;
}));


