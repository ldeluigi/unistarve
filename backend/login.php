<?php
require("base.php");
$e = isset($_GET["email"]) ? $_GET["email"] : false;
$result = [];
if ($e and is_string($e) and strlen($e) <= $EMAX and filter_var($e, FILTER_VALIDATE_EMAIL)) {
    $p = isset($_GET["pword"]) ? $_GET["pword"] : false;
    if ($p and is_string($p) and strlen($p) <= $PMAX){
        $conn = connectDB();
        if ($conn->connect_error) {
            $result[OK] = 50;
            $result[ERROR] = "Connessione fallita: " . $conn->connect_error;
        } else {
            $stmt = $conn->prepare("SELECT ID, productor, transporter, consumer, administrator FROM ACCOUNTS WHERE email=? AND pword=?");
            if (!$stmt){
                $result[OK] = 51;
                $result[ERROR] = "Errore nel database";
            } else {
                if ($stmt->bind_param("ss", $e, $p) and $stmt->execute()) {
                    $account = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                    if (count($account) === 1) {
                        $aID = $account[0]["ID"];
                        session_start();
                        if (isset($_SESSION[ACCOUNT]) and $aID === $_SESSION[ACCOUNT]) {
                            refresh_session();
                        }
                        if ($account[0]["administrator"] == 1) {
                            $result[RESULT][REDIRECT] = "/admin/admin.html";
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 52;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "administrator";
                            }
                        } else if ($account[0]["productor"] != null) {
                            $result[OK] = true;
                            $result[RESULT][REDIRECT] = "/productor/productor.html";
                            $result[RESULT][ID] = $account[0]["productor"];
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 53;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "productor";
                                $_SESSION[ID] = $account[0]["productor"];
                            }
                        } else if ($account[0]["transporter"] != null) {
                            $result[OK] = true;
                            $result[RESULT][REDIRECT] = "/transporter/transporter.html";
                            $result[RESULT][ID] = $account[0]["transporter"];
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 54;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "transporter";
                                $_SESSION[ID] = $account[0]["transporter"];
                            }
                        } else if ($account[0]["consumer"] != null) {
                            $result[OK] = true;
                            $result[RESULT][REDIRECT] = "/consumer/consumer.html";
                            $result[RESULT][ID] = $account[0]["consumer"];
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 55;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "consumer";
                                $_SESSION[ID] = $account[0]["consumer"];
                            }
                        } else {
                            $result[OK] = 56;
                            $result[ERROR] = "ruolo account assente";
                        }
                    } else if (count($account) === 0) {
                        $result[OK] = 57;
                        $result[ERROR] = "Account inesistente o password errata";    
                    } else {
                        $result[OK] = 58;
                        $result[ERROR] = "Errore nel DataBase";
                    }
                } else {
                    $result[OK] = 59;
                    $result[ERROR] = "Errore nella query: " + $stmt->error;
                }
            }
        }
    } else {
        $result[OK] = 60;
        $result[ERROR] = "Password  non valida (lunghezza massima = $PMAX)";
    }
} else {
    session_start();
    if (isset($_SESSION[ACCOUNT])) {
        refresh_session();
        $conn = connectDB();
        if ($conn->connect_error) {
            $result[OK] = 71;
            $result[ERROR] = "Connessione fallita: " . $conn->connect_error;
        } else {
            $stmt = $conn->prepare("SELECT ID, productor, transporter, consumer, administrator FROM ACCOUNTS WHERE ID=?");
            if (!$stmt) {
                $result[OK] = 61;
                $result[ERROR] = "Errore nel database";
            } else {
                if ($stmt->bind_param("i", $_SESSION[ACCOUNT]) and $stmt->execute()) {
                    $account = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                    if (count($account) === 1) {
                        $aID = $account[0]["ID"];
                        if ($account[0]["administrator"] == 1) {
                            $result[RESULT][REDIRECT] = "/admin/admin.html";
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 62;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "administrator";
                            }
                        } elseif ($account[0]["productor"] != null) {
                            $result[OK] = true;
                            $result[RESULT][REDIRECT] = "/productor/productor.html";
                            $result[RESULT][ID] = $account[0]["productor"];
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 63;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "productor";
                                $_SESSION[ID] = $account[0]["productor"];
                            }
                        } elseif ($account[0]["transporter"] != null) {
                            $result[OK] = true;
                            $result[RESULT][REDIRECT] = "/transporter/transporter.html";
                            $result[RESULT][ID] = $account[0]["transporter"];
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 64;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "transporter";
                                $_SESSION[ID] = $account[0]["transporter"];
                            }
                        } elseif ($account[0]["consumer"] != null) {
                            $result[OK] = true;
                            $result[RESULT][REDIRECT] = "/consumer/consumer.html";
                            $result[RESULT][ID] = $account[0]["consumer"];
                            if (!$conn->query("UPDATE ACCOUNTS SET last_access=NOW() WHERE ID=$aID")) {
                                $result[OK] = 65;
                                $result[ERROR] = "Errore durante l'accesso";
                            } else {
                                $result[OK] = true;
                                $_SESSION[ACCOUNT] = $aID;
                                $_SESSION[TOKEN] = "consumer";
                                $_SESSION[ID] = $account[0]["consumer"];
                            }
                        } else {
                            $result[OK] = 66;
                            $result[ERROR] = "ruolo account assente";
                        }
                    } elseif (count($account) === 0) {
                        $result[OK] = 67;
                        $result[ERROR] = "Account inesistente o password errata";
                    } else {
                        $result[OK] = 68;
                        $result[ERROR] = "Errore nel DataBase";
                    }
                } else {
                    $result[OK] = 69;
                    $result[ERROR] = "Errore nella query: " + $stmt->error;
                }
            }
        }
    } else {
        $result[OK] = 70;
        if (!($e and is_string($e)))
            $result[ERROR] = "Mail  non inserita";
        else
            $result[ERROR] = "Formato mail non valido (lunghezza massima = $EMAX)";
    }
}
if ($result[OK] === true) {
    $result["timeout"] = $SESSION_DURATION;
}
header('Content-Type: application/json');
echo(json_encode($result));
?>