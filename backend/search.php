<?php
require("base.php");
$result = [];
session_start();
$conn = connectDB();
if ($conn->connect_error) {
    $result[OK] = 301;
    $result[ERROR] = "Connessione al DB fallita";
} else {
    if (isset($_GET["q"]) and is_string($_GET["q"])) {
        $stmt = $conn->prepare("SELECT O.ID AS ID, O.img_link AS img, O.name as name, O.short_description AS sd, O.cost AS cost, P.name AS productor, SUM(C.quantity) AS total, O.available AS av FROM ORDINABLES AS O
        JOIN PRODUCTORS AS P ON P.ID=O.productor
        JOIN ACCOUNTS AS A ON A.productor=P.ID
        LEFT JOIN COMPRISE AS C ON O.ID=C.product
        WHERE (O.elimination_datetime IS NULL) AND (P.unsubscription IS NULL) AND".(isset($_GET["p"]) ? " A.ID=? AND" : "")." (O.name LIKE CONCAT('%', ?, '%') OR O.short_description LIKE CONCAT('%', ?, '%') OR P.name LIKE CONCAT('%', ?, '%') OR O.description LIKE CONCAT('%', ?, '%'))
        GROUP BY O.ID
        ORDER BY total DESC, cost DESC, name");
        if (!$stmt) {
            $result[OK] = 303;
            $result[ERROR] = "Errore nella preparazione della query di ricerca";
        } else {
            if ((isset($_GET["p"]) and isset($_SESSION[ACCOUNT]) and $stmt->bind_param("issss", $_SESSION[ACCOUNT], $_GET["q"], $_GET["q"], $_GET["q"], $_GET["q"])) or $stmt->bind_param("ssss", $_GET["q"], $_GET["q"], $_GET["q"], $_GET["q"])) {
                if ($stmt->execute() === false) {
                    $result[OK] = 304;
                    $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                } else {
                    $res = $stmt->get_result();
                    if ($res === false) {
                        $result[OK] = 305;
                        $result[ERROR] = "Errore nella query per i top: ".$conn->error;
                    } else {
                        $RESULT_NUM = 50;
                        $res = generate($res, $RESULT_NUM, (isset($_GET["n"]) and is_numeric($_GET["n"])) ? $_GET["n"] + 1 : 1, (isset($_GET["p"]) and is_numeric($_GET["p"])) ? $_GET["p"] : 0);
                        $result[OK] = true;
                        $result[HTML] = $res;
                    }
                }
            } else {
                $result[OK] = 306;
                $result[ERROR] = "Errore nella preparazione della query di ricerca per ".$_GET["q"];
            }
        }
    } else {
        if (isset($_GET["productor"]) and isset($_SESSION[ACCOUNT])) {
            $stmt = $conn->prepare("SELECT O.ID AS ID, O.img_link AS img, O.name as name, O.short_description AS sd, O.cost AS cost, P.name AS productor, SUM(C.quantity) AS total, O.available AS av FROM ORDINABLES AS O
                                        JOIN PRODUCTORS AS P ON P.ID=O.productor
                                        JOIN ACCOUNTS AS A ON A.productor=P.ID
                                        LEFT JOIN COMPRISE AS C ON O.ID=C.product
                                        WHERE (O.elimination_datetime IS NULL) AND (P.unsubscription IS NULL) AND A.ID=?
                                        GROUP BY O.ID
                                        ORDER BY total DESC, cost DESC, name");
            if ($stmt !== false and $stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                if ($stmt->execute()) {
                    $result[OK] = true;
                    $result[HTML] = generate($stmt->get_result(), PHP_INT_MAX);
                } else {
                    $result[OK] = 308;
                    $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                }
            } else {
                $result[OK] = 307;
                $result[ERROR] = "Errore nella query per i prodotti: ".$conn->error;
            }
        } else {
            $res = $conn->query("SELECT O.ID AS ID, O.img_link AS img, O.name as name, O.short_description AS sd, O.cost AS cost, P.name AS productor, SUM(C.quantity) AS total, O.available AS av FROM ORDINABLES AS O
                                    JOIN PRODUCTORS AS P ON P.ID=O.productor
                                    LEFT JOIN COMPRISE AS C ON O.ID=C.product
                                    WHERE (O.elimination_datetime IS NULL) AND (O.available=1) AND (P.unsubscription IS NULL)
                                    GROUP BY O.ID
                                    ORDER BY total DESC, cost DESC, name");
            if ($res === false) {
                $result[OK] = 302;
                $result[ERROR] = "Errore nella query per i top: ".$conn->error;
            } else {
                $RESULT_NUM = 20;
                $res = generate($res, $RESULT_NUM, (isset($_GET["n"]) and is_numeric($_GET["n"])) ? (int)$_GET["n"] : 1, (isset($_GET["p"]) and is_numeric($_GET["p"])) ? ((int)$_GET["p"]) : 0);
                $result[OK] = true;
                $result[HTML] = $res;
            }
        }
    }
}
header('Content-Type: application/json');
echo(json_encode($result));


function generate($res, $RESULT_NUM, $resultMultiplier = 1, $pageIndex = 0) {
    $page = new DOMDocument();
    $page->normalizeDocument();
    $page->formatOutput = true;
    $productorMode = false;
    if (isset($_GET["productor"]) and isset($_SESSION[ACCOUNT])) {
        $productorMode = true;
    }
    $list = $page->createElement('ul');
    $list->setAttribute('class', 'products list-unstyled');
    $moreResults = $res->num_rows > ($pageIndex + 1) * $RESULT_NUM * $resultMultiplier;
    if ($pageIndex > 0 and $RESULT_NUM * $resultMultiplier > 0) {
        $res->data_seek($pageIndex * $RESULT_NUM * $resultMultiplier);
    }
    for ($i = 0; $i < $RESULT_NUM * $resultMultiplier and $elem = $res->fetch_assoc(); $i++) {
        $onProductClick = 'if (typeof showProduct == "function") {
                            showProduct('.$elem["ID"].'); 
                           } else {
                               window.location.href="/product/product.html?id=' . $elem["ID"].'";
                           }';

        $el = $page->createElement('li');
        $el->setAttribute('class', 'product media border rounded w-100 mb-1 prod-'.$elem["ID"]);

        $h = $page->createElement('div');
        $h->setAttribute('class', 'm-3 media-body w-50');
        $h->setAttribute('onclick', $onProductClick);
        
        $ee = $page->createElement('h5');
        $ee->setAttribute('class', 'mt-0 mb-1');
        
        $id = $page->createElement('p');
        $id->setAttribute('class', 'font-weight-light cost text-right float-right mr-2 align-middle text-nowrap pt-5 pr-4');
        $id->appendChild($page->createTextNode($elem["cost"]."€"));
        
        $rdiv = $page->createElement('div');
        $rdiv->setAttribute('class', 'float-right my-auto rdiv w-25 text-nowrap');

        if ($elem["img"] and strlen($elem["img"]) > 0) {
            $dimg = $page->createElement('div');
            $dimg->setAttribute('class', 'ml-2 my-auto prodImageC position-relative');
            $dimg->setAttribute('onclick', $onProductClick);
            $img = $page->createElement('img');
            $img->setAttribute('class', 'my-auto prodImage img-thumbnail rounded mw-25 lazy');
            $img->setAttribute('alt', $elem["name"]);
            $img->setAttribute('src', '/img/unibologostarve.gif');
            $img->setAttribute('data-src', $elem["img"]);
            $icon = $page->createElement("i");
            $icon->setAttribute("class", "fas fa-search-plus position-absolute");
            $icon->setAttribute("aria-hidden", "true");
            $dimg->appendChild($icon);

            $dimg->appendChild($img);
            $el->appendChild($dimg);
        }
        

        $descrip = $page->createElement('div');
        $descrip->setAttribute('class', 'description mb-0 text-truncate mw-100');
        $pd = $page->createElement('p');
        $pd->appendChild($page->createTextNode($elem["sd"]));
        $pd->setAttribute('class', 'text-truncate');
        $descrip->appendChild($pd);
        

        $text = $page->createElement('small');
        $text->setAttribute('class', 'productor');
        $text->appendChild($page->createTextNode($elem["productor"]));
        

        $ee->appendChild($page->createTextNode($elem["name"]));
        $h->appendChild($ee);
        $h->appendChild($descrip);
        $h->appendChild($text);
        

        $el->appendChild($h);
        if ($elem["av"] == 1 or $productorMode) {
            $del = $page->createElement('button');
            $del->setAttribute('class', 'add edit float-right btn btn-outline-light mt-4 '.($productorMode ? 'mr-1 ' : 'mr-4'));
            $del->setAttribute('type', 'button');
            $icon = $page->createElement('i');
            if ($productorMode) {
                $del->setAttribute('onclick', 'editProduct('.$elem["ID"].');');
                $icon->setAttribute('class', "fas fa-pen");
                $del->setAttribute('title', 'Modifica '.$elem["name"]);
                $dele = $page->createElement('button');
                $dele->setAttribute('class', 'add edit float-right btn btn-outline-light mt-4 mr-4');
                $dele->setAttribute('type', 'button');
                $icon2 = $page->createElement('i');
                $dele->setAttribute('onclick', 'deleteItem('.$elem["ID"].');');
                $icon2->setAttribute('class', "fas fa-trash");
                $dele->setAttribute('title', 'Elimina '.$elem["name"]);
                $dele->appendChild($icon2);
                $rdiv->appendChild($dele);
            } else {
                $del->setAttribute('onclick', isset($_SESSION[ACCOUNT]) ? 'addToCart('.$elem["ID"].');' : 'window.location="/login/login.html";');
                $icon->setAttribute('class', "fas fa-plus");
                $del->setAttribute('title', 'Aggiungi '.$elem["name"].' al carrello');
            }
            $icon->setAttribute('aria-hidden', "true");
            $del->appendChild($icon);
            $rdiv->appendChild($del);
        }
        $rdiv->appendChild($id);
        $el->appendChild($rdiv);

        $list->appendChild($el);
    }
    if ($i === 0) {
        $p = $page->createElement('p');
        $p->setAttribute('class', 'no-messages font-italic text-center lead');
        $p->appendChild($page->createTextNode("Nessun risultato"));
        $page->appendChild($p);
    } else {
        $page->appendChild($list);
        if ($moreResults) {
            $mDiv = $page->createElement('div');
            $mDiv->setAttribute('class', 'mx-auto w-50 my-2 text-center');
            $more = $page->createElement('button');
            $more->setAttribute('class', 'btn btn-outline-light mx-auto');
            $more->setAttribute('type', 'button');
            $more->setAttribute('onclick', 'moreItems('.($pageIndex + 1).','.($resultMultiplier + 1).');');
            $more->appendChild($page->createTextNode('Altri Risultati'));
            $mDiv->appendChild($more);
            $page->appendChild($mDiv);
        }
    }
    $res = html_entity_decode($page->saveHTML());
    return $res;
}
?>