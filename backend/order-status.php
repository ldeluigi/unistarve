<?php
require("base.php");
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 751;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        if (isset($_GET["id"]) and is_numeric($_GET["id"])) {
            $stmt = $conn -> prepare("UPDATE ORDERS AS O SET delivered=IF(EXISTS(SELECT * FROM ACCOUNTS WHERE ID=? AND transporter IS NOT NULL), NOW(), delivered), pickedup=IF(EXISTS(SELECT * FROM ACCOUNTS WHERE ID=? AND productor IS NOT NULL), NOW(), pickedup)
                                        WHERE O.ID=? AND (EXISTS(SELECT * FROM PRODUCTORS AS P JOIN ACCOUNTS AS A ON A.productor=P.ID WHERE O.destinated_to=P.ID AND A.ID=?)
                                            OR EXISTS(SELECT * FROM TRANSPORTERS AS P JOIN ACCOUNTS AS A ON A.transporter=P.ID WHERE O.transportation=P.ID AND A.ID=?))");
            if ($stmt === false) {
                $result[OK] = 752;
                $result[ERROR] = "Errore durante la preparazione della query.";
            } else {
                if ($stmt -> bind_param ("iiiii", $_SESSION[ACCOUNT], $_SESSION[ACCOUNT], $_GET["id"], $_SESSION[ACCOUNT], $_SESSION[ACCOUNT])) {
                    if ($stmt->execute() and $stmt->affected_rows > 0) {
                        $result[OK] = true;
                        $result[RESULT] = "Ordine aggiornato";
                        sendMessage($_GET["id"], $conn);
                    } else {
                        $result[OK] = 753;
                        $result[ERROR] = "Errore durante l'esecuzione della query. [".$stmt->num_rows." righe modificate] ".$stmt->error;
                    }
                } else {
                    $result[OK] = 754;
                    $result[ERROR] = "Errore durante la preparazione della query.";
                }
            }
        }
    }
} else {
    $result[OK] = 757;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));

function sendMessage(int $orderid, mysqli $conn) {
    $text = "L'ordine ".$orderid." è stato aggiornato.";
    $stmt = $conn->prepare("INSERT INTO MESSAGES (time, text, Rec_ID) SELECT DISTINCT NOW(), ?, A.ID FROM ACCOUNTS AS A LEFT JOIN TRANSPORTERS AS T ON A.transporter=T.ID
                            LEFT JOIN CONSUMERS AS C ON C.CF=A.consumer
                            LEFT JOIN ORDINATIONS AS O ON O.client=C.CF
                            JOIN ORDERS AS ORD ON ORD.ordination=O.ID OR ORD.transportation=T.ID
                            WHERE (A.transporter IS NOT NULL OR A.consumer IS NOT NULL) AND ORD.ID=?");
    return $stmt !== false && $stmt->bind_param("si", $text, $orderid) && $stmt->execute();
}
?>