<?php
require("base.php");
$result = [];
session_start();
refresh_session();
$conn = connectDB();
if ($conn->connect_error) {
    $result[OK] = 600;
    $result[ERROR] = "Connessione al DB fallita";
} else {
    $go = true;
    // if (isset($_GET["suggest"])) {
    //     $go = false;
    //     // maybe we should make evryone able to add them as personal, or maybe not
    // }
    if ($go) {
        $locs = $conn->query("SELECT ID, address, indications FROM locations");
        if ($locs === false) {
            $result[OK] = 601;
            $result[ERROR] = "Errore durante l'esecuzione della query ".$conn->error;
        } else {
            $page = new DOMDocument();
            $page->normalizeDocument();
            $page->formatOutput = true;
            if ($locs->num_rows == 0) {
                $p = $page->createElement('p');
                $p->setAttribute('class', 'no-locations font-italic text-center lead border rounded');
                $p->appendChild($page->createTextNode("Nessun luogo di consegna disponibile"));
                $page->appendChild($p);
            } else {
                $list = $page->createElement('ul');
                $list->setAttribute('class', 'messages list-group');
                $exR = [];
                foreach ($locs->fetch_all(MYSQLI_ASSOC) as $row) {
                    $exR[] = $row;
                    $el = $page->createElement('li');
                    $el->setAttribute('class', 'list-group-item flex-column align-items-start loc-'.$row["ID"]);

                    $h = $page->createElement('div');
                    $h->setAttribute('class', 'messagetime d-flex justify-content-between mt-3');

                    $addr = $page->createElement('h5');
                    $addr->setAttribute('class', 'mb-1');
                    $addr->appendChild($page->createTextNode($row["address"]));
                    $h->appendChild($addr);

                    $el->appendChild($h);

                    $text = $page->createElement('p');
                    $text->setAttribute('class', 'messagetext text-md-left w-100 p-3');
                    $text->appendChild($page->createTextNode($row["indications"]));
                    $el->appendChild($text);                    

                    $del = $page->createElement('button');
                    $del->setAttribute('class', 'selectLoc float-right btn btn-outline-light');
                    $del->setAttribute('type', 'button');
                    $del->setAttribute('onclick', 'selectLocation('.$row["ID"].');');
                    $icon = $page->createElement('i');
                    $icon->setAttribute('class', "fas fa-check");
                    $icon->setAttribute('aria-hidden', "true");
                    $del->appendChild($icon);
                    $el->appendChild($del);

                    $list->appendChild($el);
                }
                $page->appendChild($list);
            }
            $res = html_entity_decode($page->saveHTML());
            $result[OK] = true;
            $result[HTML] = $res;
            $result[RESULT] = $exR;
        }
    }
}
header('Content-Type: application/json');
echo(json_encode($result));


?>