<?php
require("base.php");
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 851;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $go = true;
        if ((isset($_GET["on"]) or isset($_GET["off"])) and isset($_GET["prodid"]) and is_numeric($_GET["prodid"])) {
            $go = false;
            $query = isset($_GET["on"]) ? "INSERT INTO COVERS (transporter, area) SELECT T.ID, ? FROM TRANSPORTERS AS T
                                    JOIN ACCOUNTS AS A ON A.transporter=T.ID
                                    WHERE A.ID=?"
            : "DELETE FROM COVERS WHERE area=? AND transporter IN (SELECT T.ID FROM TRANSPORTERS AS T JOIN ACCOUNTS AS A ON A.transporter=T.ID WHERE A.ID=?)";
            $stmt = $conn -> prepare($query);
            if ($stmt === false) {
                $result[OK] = 852;
                $result[ERROR] = "Errore durante la preparazione della query. ".$conn->error;
            } else {
                if ($stmt -> bind_param ("ii", $_GET["prodid"], $_SESSION[ACCOUNT])) {
                    if ($stmt->execute() and $stmt->affected_rows > 0) {
                        $go = true;
                    } else {
                        $result[OK] = 853;
                        $result[ERROR] = "Errore durante l'esecuzione della query.".$stmt->error."(".$stmt->affected_rows." righe modificate)";
                    }
                } else {
                    $result[OK] = 854;
                    $result[ERROR] = "Errore durante la preparazione della query.";
                }
            }
        
        }
        if ($go) {
            $stmt = $conn->prepare("SELECT P.ID AS PID, P.name AS Pname, P.address AS Paddress, COUNT(C.area) AS av
                                    FROM PRODUCTORS AS P
                                    CROSS JOIN TRANSPORTERS AS T
                                    JOIN ACCOUNTS AS A ON A.transporter=T.ID
                                    LEFT JOIN COVERS AS C ON C.transporter=T.ID AND C.area=P.ID
                                    WHERE A.ID=? AND P.unsubscription IS NULL AND T.unsubscription IS NULL
                                    GROUP BY P.ID, P.name, P.address");
            if (!$stmt) {
                $result[OK] = 855;
                $result[ERROR] = "Errore durante la preparazione della query";
            } else {
                if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                    if ($stmt->execute()) {
                        $res = $stmt->get_result();
                        if ($res->num_rows === 0) {
                            $page = new DOMDocument();
                            $page->normalizeDocument();
                            $page->formatOutput = true;
                            $p = $page->createElement('p');
                            $p->setAttribute('class', 'no-messages font-italic text-center lead w-50 mx-auto border rounded');
                            $p->appendChild($page->createTextNode("Nessun produttore disponibile"));
                            $page->appendChild($p);
                            $res = html_entity_decode($page->saveHTML());
                        } else {
                            $res = generate($res);
                        }
                        $result[OK] = true;
                        $result[HTML] = $res;
                    } else {
                        $result[OK] = 856;
                        $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                    } 
                } else {
                    $result[OK] = 857;
                    $result[ERROR] = "Errore nella creazione della query per i messaggi";
                }
            }
        }
    }
} else {
    $result[OK] = 858;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));


function generate($res) {
    $page = new DOMDocument();
    $page->normalizeDocument();
    $page->formatOutput = true;

    $list = $page->createElement('ul');
    $list->setAttribute('class', 'areas list-unstyled');
    for ($i = 0; $elem = $res->fetch_assoc(); $i++) {

        $el = $page->createElement('li');
        $el->setAttribute('class', 'switchRow media border rounded w-100 mb-1 p-2');
        
        $ee = $page->createElement('span');
        $ee->setAttribute('class', 'my-1');
        $ee->appendChild($page->createTextNode($elem["Pname"]."  <>  ".$elem["Paddress"]));

        $switchDiv = $page->createElement('div');
        $switchDiv->setAttribute('class', 'ml-2 switch round '.($elem["av"] == 0 ? 'Off' : 'On' ));
        $switchDiv->setAttribute('data-productorID', $elem["PID"]);
        $switchDiv->setAttribute('id', 'Pavailable');
        $avToggle = $page->createElement('div');
        $avToggle->setAttribute('class', 'toggle');
        $avLabel = $page->createElement('div');
        $avLabel->setAttribute('class', 'align-middle my-4 ml-auto text-right');
        $avLabel->appendChild($page->createTextNode('Disponibilità: '));
        $switchDiv->appendChild($avToggle);
        $avLabel->appendChild($switchDiv);


        $el->appendChild($ee);
        $el->appendChild($avLabel);


        $list->appendChild($el);
    }
    if ($i === 0) {
        $p = $page->createElement('p');
        $p->setAttribute('class', 'no-messages font-italic text-center lead');
        $p->appendChild($page->createTextNode("Nessun produttore"));
        $page->appendChild($p);
    } else {
        $page->appendChild($list);
        $okDiv = $page->createElement('div');
        $okDiv->setAttribute('class', 'mx-auto w-100 my-2 text-right');
        $returnB = $page->createElement('button');
        $returnB->setAttribute('class', 'btn btn-outline-light');
        $returnB->setAttribute('type', 'button');
        $returnB->setAttribute('onclick', 'backToPerson();');
        $returnB->appendChild($page->createTextNode('Indietro'));
        $okDiv->appendChild($returnB);
        $page->appendChild($okDiv);
    }
    $res = html_entity_decode($page->saveHTML());
    return $res;
}
?>