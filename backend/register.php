<?php
require("base.php");
$e = isset($_GET["email"]) ? $_GET["email"] : false;
$p = isset($_GET["pword"]) ? $_GET["pword"] : false;
$role = isset($_GET["role"]) ? $_GET["role"] : false;
$result = [];
$null = null;
$con_id = false;
$prod_id = false;
$tran_id = false;
$cart_id = false;
if ($e and is_string($e) and strlen($e) <= $EMAX and filter_var($e, FILTER_VALIDATE_EMAIL)) {
    if ($p and is_string($p) and strlen($p) <= $PMAX) {
        if ($role and is_string($role) and (in_array($role, array("c", "t", "p")))) {
            $conn = connectDB();
            if ($conn->connect_error) {
                $result[OK] = 103;
                $result[ERROR] = "Connessione fallita: " . $conn->connect_error;
            } else {
                $stmt = $conn->prepare("INSERT INTO ACCOUNTS (productor, transporter, consumer, last_access, registration, email, pword) VALUES (?, ?, ?, NULL, NOW(), ?, ?)");
                if (!$stmt){
                    $result[OK] = 106;
                    $result[ERROR] = "Errore durante la preparazione della query - ".$conn->error;
                } else {
                    if ($role == "c") {
                        $surname = isset($_GET["surname"]) ? $_GET["surname"] : false;
                        $name = isset($_GET["name"]) ? $_GET["name"] : false;
                        $cf = isset($_GET["fc"]) ? $_GET["fc"] : false;
                        if ($surname and $name and $cf and is_string($surname) and is_string($name) and is_string($cf)) {
                            if (strlen($name) > $NMAX) {
                                $result[OK] = 114;
                                $result[ERROR] = "Nome troppo lungo ($NMAX)";
                            } else if (strlen($surname) > $CMAX) {
                                $result[OK] = 115;
                                $result[ERROR] = "Cognome troppo lungo ($CMAX)";
                            } else if (strlen($cf) > $CFMAX) {
                                $result[OK] = 116;
                                $result[ERROR] = "CF troppo lungo ($CFMAX)";
                            } else {
                                $stmt_cc = $conn->prepare("INSERT INTO CART () VALUES ()");
                                if (!$stmt_cc) {
                                    $result[OK] = 110;
                                    $result[ERROR] = "Errore durante la preparazione della query";
                                } else {
                                    $stmt_c = $conn->prepare("INSERT INTO CONSUMERS (surname, name, CF, cart) VALUES (?, ?, ?, ?)");
                                    if (!$stmt_c) {
                                        $result[OK] = 111;
                                        $result[ERROR] = "Errore durante la preparazione della query";
                                    } else {
                                        if (!$stmt_cc->execute() || !$stmt_cc->insert_id) {
                                            $result[OK] = 112;
                                            $result[ERROR] = "Errore durante l'esecuzione della query: ".$stmt_cc->error;
                                        } else {
                                            $cart_id = $stmt_cc->insert_id;
                                            if ($stmt_c->bind_param("sssi", $surname, $name, $cf, $cart_id)) {
                                                if ($stmt_c->execute()) {
                                                    $con_id = $cf;
                                                    if ($stmt->bind_param("iisss", $null, $null, $con_id, $e, $p)) {
                                                        if ($stmt->execute()) {
                                                            $result[OK] = true;
                                                            sendMessage("Salve $name e benvenuto in Unistarve. Da questa sezione puoi visualizzare le notifiche, mentre cliccando sui bottoni in alto puoi navigare i menu e gestire il tuo carrello e le tue ordinazioni. Ricorda che per poter effettuare un ordine è necessario aver aggiunto almeno un metodo di pagamento. E' possibile aggiungerne uno dal bottone apposito nella pagina per la gestione del profilo. Saluti dal team e buon appetito!", $stmt->insert_id, $conn);
                                                        } else {
                                                            $result[OK] = 120;
                                                            $result[ERROR] = "Errore nell'esecuzione della query per l'account: ".$stmt->error;
                                                        }
                                                    } else {
                                                        $result[OK] = 119;
                                                        $result[ERROR] = "Errore nella creazione della query per il cliente";
                                                    }
                                                } else {
                                                    $result[OK] = 118;
                                                    $result[ERROR] = "Errore nell'esecuzione della query per il cliente: ".$stmt_c->error;
                                                }
                                            } else {
                                                $result[OK] = 117;
                                                $result[ERROR] = "Errore nella creazione della query per il cliente";
                                            }                                            
                                        }
                                    }
                                }
                            }
                        } else {
                            $result[OK] = 113;
                            $result[ERROR] = "Parametri errati. Richiesto nome, cognome, cf";
                        }
                    } else if ($role == "p") {
                        $piva = isset($_GET["piva"]) ? $_GET["piva"] : false;
                        $address = isset($_GET["address"]) ? $_GET["address"] : false;
                        $name = isset($_GET["name"]) ? $_GET["name"] : false;
                        $description = isset($_GET["d"]) ? $_GET["d"] : false;
                        if ($piva and $address and $name and $description and is_string($piva) and is_string($address) and is_string($name) and is_string($description)) {
                            if (strlen($name) > $PNMAX) {
                                $result[OK] = 121;
                                $result[ERROR] = "Nome produttore troppo lungo";
                            } else if (strlen($piva) > $PIVAMAX) {
                                $result[OK] = 122;
                                $result[ERROR] = "Partita iva troppo lunga";
                            } else if (strlen($address) > $AMAX) {
                                $result[OK] = 123;
                                $result[ERROR] = "Indirizzo troppo lungo";
                            } else if (strlen($description) > $DMAX) {
                                $result[OK] = 124;
                                $result[ERROR] = "Descrizione troppo lunga";
                            } else {
                                $stmt_p = $conn->prepare("INSERT INTO PRODUCTORS (P_IVA, address, name, description) VALUES (?, ?, ?, ?)");
                                if (!$stmt_p || !$stmt_p->bind_param("ssss", $piva, $address, $name, $description)) {
                                    $result[OK] = 125;
                                    $result[ERROR] = "Errore nella creazione della query produttore";
                                } else {
                                    if ($stmt_p->execute()) {
                                        $prod_id = $stmt_p->insert_id;
                                        if ($stmt->bind_param("iisss", $prod_id, $null, $null, $e, $p)) {
                                            if ($stmt->execute()) {
                                                $result[OK] = true;
                                                sendMessage("Salve $name, il team vi da il benvenuto. Come produttori avrete il compito di soddisfare gli ordini che compariranno nell'omonima sezione, corrispondenti a prodotti che voi metterete a disposizione dei clienti inserendone i dati nei form. Il software vi permetterà di aggiungere rimuovere o modificare prodotti a vostro piacimento, e di rendere non disponibili alcuni di questi. Note importanti: Nella schermata principale, i prodotti saranno ordinati per popolarità decrescente.", $stmt->insert_id, $conn);
                                            } else {
                                                $result[OK] = 128;
                                                $result[ERROR] = "Errore nell'esecuzione della query per l'account: ".$stmt->error;
                                            }
                                        } else {
                                            $result[OK] = 127;
                                            $result[ERROR] = "Errore nella creazione della query per il produttore";
                                        }
                                    } else {
                                        $result[OK] = 126;
                                        $result[ERROR] = "Errore nell'esecuzione della query per il produttore: ".$stmt_p->error;
                                    }
                                }
                            }
                        } else {
                            $result[OK] = 138;
                            $result[ERROR] = "Parametri produttore invalidi";
                        }
                    } else if ($role == "t") {
                        $piva = isset($_GET["piva"]) ? $_GET["piva"] : false;
                        $address = isset($_GET["address"]) ? $_GET["address"] : false;
                        $name = isset($_GET["name"]) ? $_GET["name"] : false;
                        $telephone = isset($_GET["telephone"]) ? $_GET["telephone"] : false;
                        if ($piva and $address and $name and $telephone and is_string($piva) and is_string($address) and is_string($name)) {
                            if (strlen($name) > $TNMAX) {
                                $result[OK] = 129;
                                $result[ERROR] = "Nome trasporto troppo lungo ($TNMAX)";
                            } else if (strlen($piva) > $PIVAMAX) {
                                $result[OK] = 130;
                                $result[ERROR] = "Partita iva troppo lunga ($PIVAMAX)";
                            } else if (strlen($address) > $AMAX) {
                                $result[OK] = 131;
                                $result[ERROR] = "Indirizzo troppo lungo ($AMAX)";
                            } else if (strlen($telephone) > $TMAX or strlen($telephone) < $TMIN) {
                                $result[OK] = 132;
                                $result[ERROR] = "Numero di telefono troppo lungo o troppo corto ($TMIN - $TMAX)";
                            } else {
                                $stmt_t = $conn->prepare("INSERT INTO TRANSPORTERS (address, name, P_IVA, active) VALUES (?, ?, ?, 1)");
                                $stmt_cn = $conn->prepare("INSERT INTO CELL_NUMBERS (phone_number, owner) VALUES (?, ?)");
                                if (!$stmt_t || !$stmt_t->bind_param("sss", $address, $name, $piva) || !$stmt_cn) {
                                    $result[OK] = 133;
                                    $result[ERROR] = "Errore nella creazione della query trasportatore";
                                } else {
                                    if ($stmt_t->execute()) {
                                        $tran_id = $stmt_t->insert_id;
                                        if ($tran_id and $stmt_cn->bind_param("si", $telephone, $tran_id) and $stmt->bind_param("iisss", $null, $tran_id, $null, $e, $p)) {
                                            if ($stmt_cn->execute()) {
                                                if ($stmt->execute()) {
                                                    $result[OK] = true;
                                                    sendMessage("Benvenuto $name nel team di Unistarve! Come ente di trasporto, avrai il compito di prendere i prodotti dai fornitori e portarli ai consumatori che li hanno ordinati. La sezione ordini conterrà tutte le informazioni necessarie, nonchè i bottoni per confermare la consegna dei suddetti. La schermata principale contiene una mappa utile per trovare la destinazione. Pro tip: cliccando su un ordine, la mappa si aggiornerà per puntare alla destinazione di quest'ultimo. Inoltre, nella sezione personale è possibile attivare o disattivare la propria disponibilità a ricevere ordini. Nota importante: gli ordini sono assegnati randomicamente tra i trasportatori disponibili.", $stmt->insert_id, $conn);
                                                } else {
                                                    $result[OK] = 137;
                                                    $result[ERROR] = "Errore nell'esecuzione della query per l'account: ".$stmt->error;
                                                }
                                            } else {
                                                $result[OK] = 136;
                                                $result[ERROR] = "Errore nell'esecuzione della query per il trasportatore: ".$stmt_cn->error;
                                            }
                                        } else {
                                            $result[OK] = 135;
                                            $result[ERROR] = "Errore nella creazione della query per il trasportatore";
                                        }
                                    } else {
                                        $tran_id = $stmt_t->insert_id;
                                        $result[OK] = 134;
                                        $result[ERROR] = "Errore nell'esecuzione della query per il trasportatore: ".$stmt_t->error;
                                    }
                                }
                            }
                        } else {
                            $result[OK] = 139;
                            $result[ERROR] = "Parametri produttore invalidi";
                        }
                    }
                }
            }
        } else {
            $result[OK] = 108;
            $result[ERROR] = "Ruolo non riconosciuto";
        }
    } else {
        $result[OK] = 102;
        $result[ERROR] = "Password  non valida (lunghezza massima = $PMAX)";
    }
} else {
    $result[OK] = 101;
    if (!($e and is_string($e)))
        $result[ERROR] = "Mail  non inserita";
    else
        $result[ERROR] = "Formato mail non valido (lunghezza massima = $EMAX)";
}
header('Content-Type: application/json');
echo(json_encode($result));
if ($result[OK] !== true and isset($con_id) and is_string($con_id)) {
    $conn->query("DELETE FROM CONSUMERS WHERE CF=\"$con_id\"");
}
if ($result[OK] !== true and isset($cart_id) and is_numeric($cart_id)) {
    $conn->query("DELETE FROM CART WHERE ID=$cart_id");
}
if ($result[OK] !== true and isset($prod_id) and is_numeric($prod_id)) {
    $conn->query("DELETE FROM PRODUCTORS WHERE ID=$prod_id");
}
if ($result[OK] !== true and isset($tran_id) and is_numeric($tran_id)) {
    $conn->query("DELETE FROM CELL_NUMBERS WHERE owner=$tran_id");
    $conn->query("DELETE FROM TRANSPORTERS WHERE ID=$tran_id");
}


function sendMessage(string $text, int $id, mysqli $conn) {
    $stmt = $conn->prepare("INSERT INTO MESSAGES (time, text, Rec_ID) VALUES (NOW(), ?, ?)");
    return $stmt !== false && $stmt->bind_param("si", $text, $id) && $stmt->execute();
}
?>