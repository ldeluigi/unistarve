
var url = window.location.href;
var alertM;

function showProduct(id) {
    $.ajax({
        type: "get",
        url: "/api/product.php?prodid=" + id,
        success: function (result, status, xhr) {
            if (result.ok && result.ok === true) {
                $("#modal-container").html(result.html);
                url = window.location.href;
                $('[data-toggle="tooltip"]').on('shown.bs.tooltip', function() {
                    report_tooltip_shown = true;
                });
                $('[data-toggle="tooltip"]').on('hide.bs.tooltip', function() {
                    report_tooltip_shown = false;
                });
                $('[data-toggle="tooltip"]').tooltip();
                var modal = $('#productModal');
                modal.on('hide.bs.modal', function (e) {
                    history.pushState(null, '', url);
                });
                modal.on('show.bs.modal', function (e) {
                    history.pushState(null, '', '/product/product.html?id='+id);
                });
                modal.modal();
                alertM = $(".bannerM").html();
                $(".bannerM").html("");
            } else {
                $(".banner").html(alert);
                $(".alert").removeClass("alert-success");
                $(".alert").addClass("alert-danger");
                $(".msg").text("Errore: " + result.message+" ("+result.ok+")");
            }
        },
        error: function (xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
        }
    });
}



var report_tooltip_shown = false;

function report(id) {
    if (report_tooltip_shown) {
        $('[data-toggle="tooltip"]').tooltip('hide');
        $.ajax({
            type: "get",
            url: "/api/report-image.php?prodid="+id,
            success: function (result, status, xhr) {
                if (result.ok && result.ok === true) {
                    $(".bannerM").html(alertM);
                    $(".alertM").removeClass("alert-danger");
                    $(".alertM").addClass("alert-success");
                    $(".msgM").text("Segnalazione inviata!");
                } else if(result.ok === 500){
                    window.location.replace('/login/login.html');
                } else {
                    $(".bannerM").html(alertM);
                    $(".alertM").removeClass("alert-success");
                    $(".alertM").addClass("alert-danger");
                    $(".msgM").text("Errore: " + result.message+" ("+result.ok+")");
                }
            },
            error: function (xhr, status, error) {
                $(".bannerM").html(alertM);
                $(".alertM").removeClass("alert-success");
                $(".alertM").addClass("alert-danger");
                $(".msgM").text("Errore nella risoluzione della richiesta AJAX");
            }
        });
    }
}



