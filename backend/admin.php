<?php
require("base.php");
$result = [];
session_start();
refresh_session();
$json = true;
if (isset($_SESSION[ACCOUNT]))  {
    $conn = $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 730;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $stmt = $conn->prepare("SELECT administrator, last_access, email, ID FROM ACCOUNTS WHERE ID=? AND administrator=1");
        if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
            if ($stmt->execute()) {
                $admin = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                if (count($admin) === 1) {
                    $h = $admin[0];
                    if (isset($_GET["table"]) && $_GET["table"] !== "Selezionare una tabella...") {
                        $json = false;
                        $table = $_GET["table"];
                        $res = $conn->query("SHOW KEYS FROM $table WHERE Key_name='PRIMARY'");
                        if(!$res) {
                            $result[OK] = 731;
                            $result[ERROR] = "Errore durante l'esecuzione della query per l'ottenimento delle chiavi";                    
                        } else {
                            $tabKey = array_column($res->fetch_all(MYSQLI_ASSOC), "Column_name");
                            ?>
                            <html> 
                                <head>
                                    <?php if (!isset($_GET["DG_ajaxid"])) { ?>
                                        <link
                                            rel="stylesheet"
                                            type="text/css"
                                            href="/commons/bootstrap/css/bootstrap.min.css"
                                        />
                                        <link rel="stylesheet" type="text/css" href="/commons/unistyle.css" />
                                        <link rel="stylesheet" type="text/css" href="/admin/style.css" />
                                        <script src="/commons/jquery.min.js"></script>
                                        <script src="/commons/popper.min.js"></script>
                                        <script src="/commons/bootstrap/js/bootstrap.min.js"></script>
                                        <?php      
                                    }
                                    include ("./libraries/phpMyDataGrid/phpmydatagrid.class.php");
                                    $objGrid = new datagrid;                       
                                    $objGrid -> friendlyHTML();                  
                                    $objGrid -> pathtoimages("/commons/phpMyDataGrid/images/");                  
                                    $objGrid -> closeTags(true);                         
                                    $objGrid -> form($table, true);
                                    $objGrid -> methodForm("get"); 
                                    $objGrid -> linkparam("&table={$table}");                      
                                    $objGrid -> conectadb($servername, $username, $password, $dbname);                        
                                    $objGrid -> tabla ($table);                  
                                    $objGrid -> buttons(true,true,true,true);
                                    foreach ($tabKey as $key) {
                                        $objGrid -> keyfield($key);               
                                    }                       
                                    $objGrid -> TituloGrid($table);
                                    $objGrid -> salt("Some Code4Stronger(Protection)"); 
                                    $objGrid -> datarows(10);
                                    $objGrid -> paginationmode('links');
                                    $objGrid -> orderby($tabKey[0]);
                                    $res = $conn->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='$table' AND table_schema='$dbname'");
                                    if(!$res) {
                                        $result[OK] = 732;
                                        $result[ERROR] = "Errore durante la preparazione della query";                    
                                    } else {
                                        $tabName = $res->fetch_all(MYSQLI_ASSOC);
                                        foreach (array_column($tabName, "COLUMN_NAME") as $name) {
                                            $objGrid -> FormatColumn($name, $name, 0, 100, 0, "120", "center");
                                        }
                                    }
                                    $objGrid -> setHeader('admin.php', '/commons/phpMyDataGrid/js/dgscripts.js', '/commons/phpMyDataGrid/css/dgstyle.css' );
                                    ?>
                                </head>
                                <body>
                                    <?php
                                     if (!isset($_GET["DG_ajaxid"])) { ?>
                                        <div class="background-image"></div>
                                    <?php 
                                    }
                                    $objGrid -> ajax("silent");
                                    $objGrid -> grid();
                                    $objGrid -> desconectar();
                                    ?>
                                    <button type="button" class="btn btn-secondary btn-outline-light ml-1" onclick="goBack()">Indietro</button>
                                    <script>
                                        function goBack() {
                                            window.location.href = "/admin/admin.html";
                                        }
                                    </script>
                                </body>
                            </html> 
                        <?php
                        }
                    } else {             
                        $res = $conn->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_SCHEMA='my_unistarve'");
                        if ($res !== false) {
                            $table = $res->fetch_all(MYSQLI_ASSOC);
                            $c = $table[0];
                            class br extends DOMElement {
                                function __construct() {
                                    parent::__construct('br');
                                }
                            }
                            $page = new DOMDocument();
                            $page->normalizeDocument();
                            $page->formatOutput = true;
                            $option = $page->createElement('option');
                            $option->setAttribute('value', "");
                            $option->appendChild($page->createTextNode("Selezionare una tabella..."));
                            $page->appendChild($option);
                            foreach ($table as $row) {
                                $option = $page->createElement('option');
                                $option->setAttribute('value', $row["TABLE_NAME"]);
                                $option->appendChild($page->createTextNode($row["TABLE_NAME"]));
                                $page->appendChild($option);
                            }                  
                            $res = html_entity_decode($page->saveHTML());
                            $result[OK] = true;
                            $result[HTML] = $res;
                        } else {
                            $result[OK] = 734;
                            $result[ERROR] = "Errore nella esecuzione della query di ottenimento tabella";
                        }
                    }                     
                } else {
                    $result[OK] = 735;
                    $result[ERROR] = "amministratore non unico";
                }
            } else {
                $result[OK] = 736;
                $result[ERROR] = "Errore nell'esecuzione della query per l'amministratore";
            }
        } else {
            $result[OK] = 737;
            $result[ERROR] = "Errore nella preparazione della query per l'amministratore";
        }
    }
} else {
    $result[OK] = 738;
    $result[ERROR] = "Sessione scaduta";
}
if ($json) {
    header('Content-Type: application/json');
    echo(json_encode($result));
}
?>