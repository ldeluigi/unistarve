var hoveringButton;
var addForm;
var formOn=false;
$(document).ready(function () {
    hoveringButton = $("#payment-container").html();
    addForm = $("#form-container").html();
    $("#form-container").html("");
    $("#payment-container").html("");
});

function paymentMethods() {
    $.ajax({
        type: 'GET',
        url: "/api/payments.php",
        success: function (result, status, xhr) {
            if (result.ok && result.ok === true) {
                $("#personal-container").html(result.html);
                $("#payment-container").html(hoveringButton);
                mediaQuery();
            } else {
                $(".banner").html(alert);
                $(".alert").removeClass("alert-success");
                $(".alert").addClass("alert-danger");
                $(".msg").text("Errore: " + result.message+" ("+result.ok+")");
            }
        },
        error: function (xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
        }
    });

}

function deletePayCard(id) {
    $.ajax({
        type: 'GET',
        url: "/api/payments.php?delete&id="+id,
        success: function (result, status, xhr) {
            if (result.ok && result.ok === true) {
                $(".card").finish();
                $(".card-" + id).fadeOut(function () {
                    $("#personal-container").html(result.html);
                });
                $("#payment-container").html(hoveringButton);
                mediaQuery();
            } else {
                $(".banner").html(alert);
                $(".alert").removeClass("alert-success");
                $(".alert").addClass("alert-danger");
                $(".msg").text("Errore: " + result.message+" ("+result.ok+")");
            }
        },
        error: function (xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
        }
    });
}

function addPayCard() {
    $("#form-container").html(addForm);
    $(".block").finish().animate({scrollTop: $("#addCard").offset().top},'slow');
    $("#hover-icon").removeClass("fa-plus");
    $("#hover-icon").addClass("fa-check");
    formOn = true;
}

function noAddCard() {
    $(".block").finish().animate({scrollTop: $("#personal-container").offset().top},'slow');
    $("#addCard").finish();
    $("#addCard").fadeOut("slow", function(){
        $("#form-container").html("");
    });
    $("#hover-icon").removeClass("fa-check");
    $("#hover-icon").addClass("fa-plus");
    formOn = false;
}

function hoverHandler() {
    if (formOn) {
        confirm();
    } else {
        addPayCard();
    }
}

function confirm() {
    var code = $("#code").val();
    var type = $("#data").val();
    var pin = $("#pin").val();
    $.ajax({
        type: 'GET',
        url: "/api/payments.php?insert&code="+code+"&payment_type="+type+"&pin="+pin,
        success: function (result, status, xhr) {
            if (result.ok && result.ok === true) {
                $("#personal-container").html(result.html);
                $("#payment-container").html(hoveringButton);
                noAddCard();
                $(".banner").html(alert);
                $(".alert").removeClass("alert-danger");
                $(".alert").addClass("alert-success");
                $(".msg").text("Carta aggiunta");
            } else {
                $(".banner").html(alert);
                $(".alert").removeClass("alert-success");
                $(".alert").addClass("alert-danger");
                $(".msg").text("Errore: " + result.message+" ("+result.ok+")");
                noAddCard();
            }
        },
        error: function (xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
            noAddCard();
        }
    });
}