var mainShown = false
var messageShown = false;
var messageLoop = null;
var pagetitle = document.title;

$(document).ready(function() {
  $("#notifiche-on").hide();
  $("#notifiche-off").show();
  $.ajax({
    type: "get",
    url: "/api/message.php?count",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        setInterval(function() {
          if (messageShown) {
            $("span.num").hide();
            document.title = pagetitle;
            return;
          }
          $.ajax({
            type: "get",
            url: "/api/message.php?count",
            success: function(result, status, xhr) {
              if (messageShown) return;
              if (result.ok && result.ok === true) {
                if (result.count > 0) {
                  $("span.num").text(result.count);
                  document.title = "(" + result.count + ") " + pagetitle;
                  $("span.num").show();
                }
              } else {
                $("span.num").hide();
              }
            },
            error: function(xhr, status, error) {}
          });
        }, 1000);
      } else {
        console.log(
          "Impossibile caricare notifiche: " +
            result.message +
            " (" +
            result.ok +
            ")"
        );
        if (result.ok === 205) {
          window.location.replace("/login/login.html");
        }
      }
    },
    error: function(xhr, status, error) {
      console.log(
        "Errore nella risoluzione della richiesta AJAX, impossibile caricare notifiche"
      );
    }
  });

  getItems();
  showMain();
  
});

function showMain() {
  if (mainShown) {
    $("#main-container").hide();
    mainShown = false;
  } else {
    if (messageShown) {
      showMessages();
    }
    $("#main-container").show();
    mainShown = true;
  }
}

function getItems() {
  $.ajax({
    type: "get",
    url: "/api/admin.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
          $("#table-select").html(result.html);
          $("#table-select").change(function(e) {
            if ($("select#table-select option:selected").text() !== "Selezionare una tabella...") {
              window.location.href = "/api/admin.php?table="+ $("select#table-select option:selected").text();
            }
          });
        } else if (result.ok && result.ok === 735) {
          window.location.replace("/login/login.html");
        } else {
          console.log("Errore: " + result.message + " (" + result.ok + ")");
        }
    },
    error: function(xhr, status, error) {
      console.log("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function showMessages() {
  $("#notifiche").toggleClass("active");
  if (messageShown) {
    $("#notifiche i").finish();
    $("#notifiche-on").hide();
    $("#notifiche-off").fadeIn();
    $("#message-container").html("");
    messageShown = false;
    if (messageLoop != null) {
      clearInterval(messageLoop);
      messageLoop = null;
    }
    if (!mainShown) {
      showMain();
    }
  } else {
    $("span.num").hide();
    if (mainShown) {
      showMain();
    }
    $.ajax({
      type: "get",
      url: "/api/message.php",
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $("#notifiche i").finish();
          $("#notifiche-off").hide();
          $("#notifiche-on").fadeIn();
          $("#message-container").html(result.html);
          messageShown = true;
          if (messageLoop == null) {
            messageLoop = setInterval(function() {
              $.ajax({
                type: "get",
                url: "/api/message.php",
                success: function(result, status, xhr) {
                  if (!messageShown) return;
                  if (result.ok && result.ok === true) {
                    $("#message-container").html(result.html);
                  } else if (result.ok && result.ok === 735) {
                    window.location.replace("/login/login.html");
                  } else {
                    console.log("Errore: " + result.message + " (" + result.ok + ")");
                  }
                },
                error: function(xhr, status, error) {}
              });
            }, 1000);
          }
        } else if (result.ok && result.ok === 735) {
          window.location.replace("/login/login.html");
        } else {
          console.log("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        console.log("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function deleteMessage(id) {
  if (id) {
    $(".del-" + id)
      .prop("onclick", null)
      .off("click");
    $.ajax({
      type: "get",
      url: "/api/message.php?delete=" + id,
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $(".message").finish();
          $(".msg-" + id).fadeOut(function() {
            $("#message-container").html(result.html);
          });
        } else if (result.ok && result.ok === 205) {
          window.location.replace("/login/login.html");
        } else {
          console.log("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        console.log("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}