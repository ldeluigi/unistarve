<?php
require("base.php");
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 451;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $stmt = $conn->prepare("SELECT consumer, last_access, email, ID FROM ACCOUNTS WHERE ID=? AND consumer IS NOT NULL");
        if (!$stmt) {
            $result[OK] = 452;
            $result[ERROR] = "Errore durante la preparazione della query";
        } else {
            if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                if ($stmt->execute()) {
                    $account = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                    if (count($account) === 1) {
                        $h = $account[0];
                        if ($h["consumer"]) {
                            $go=true;
                            if (isset($_GET["insert"])) {
                                $go=false;
                                if (isset($_GET["code"]) and isset($_GET["payment_type"]) and isset($_GET["pin"]) and
                                    strlen($_GET["code"]) <= $COMAX and strlen($_GET["payment_type"]) <= $PTMAX and strlen($_GET["pin"]) <= $PIMAX) {
                                    $stmt = $conn->prepare("INSERT INTO PAYMENT_DATA (PIN) VALUES (?)");
                                    $stmt2 = $conn->prepare("INSERT INTO PAYMENT_METHODS (payment_data, code, payment_type, owner) VALUES (?, ?, ? ,?)");
                                    if (!$stmt or !$stmt2) {
                                        $result[OK] = 460;
                                        $result[ERROR] = "Errore durante la preparazione della query di inserimento della carta";
                                    } else {
                                        if ($stmt->bind_param("s", $_GET["pin"])) {
                                            if (($stmt->execute() and ($pin_id = $stmt->insert_id)) and $stmt2->bind_param("isss", $pin_id, $_GET["code"], $_GET["payment_type"], $h["consumer"])) {
                                                if ($stmt2->execute()){
                                                    $go=true;
                                                } else{
                                                    $result[OK] = 463;
                                                    $result[ERROR] = "Errore durante la esecuzione della query di inserimento della carta ". $stmt2->error."Lines changed: ".$stmt2->affected_rows;
                                                }
                                            } else {
                                                $result[OK] = 462;
                                                $result[ERROR] = "Errore durante la esecuzione della query di inserimento dei codici carta ". $stmt->error."Lines changed: ".$stmt->affected_rows;
                                            }
                                        } else {
                                            $result[OK] = 461;
                                            $result[ERROR] = "Errore durante la preparazione della query di inserimento dei codici carta";
                                        }
                                    }
                                } else {
                                    $result[OK] = 464;
                                    $result[ERROR] = "Errore input";
                                }
                            } else {
                                if (isset($_GET["delete"])) {
                                    $go=false;
                                    if (isset($_GET["id"])){
                                        $stmt = $conn->prepare("SELECT payment_data FROM PAYMENT_METHODS WHERE ID=? AND owner=?");
                                        $stmt2 = $conn->prepare("UPDATE PAYMENT_METHODS SET payment_data=NULL WHERE ID=? AND owner=?");
                                        $stmt3 = $conn->prepare("DELETE FROM PAYMENT_DATA WHERE ID=?");
                                        if (!$stmt or !$stmt2 or !$stmt3) {
                                            $result[OK] = 466;
                                            $result[ERROR] = "Errore durante la creazione della query di estrazione dati della carta";
                                        } else {
                                            if ($stmt->bind_param("is", $_GET["id"], $h["consumer"])) {
                                                if ($stmt->execute()) {
                                                    $credit = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                                                    if (count($credit)===1 and $stmt2->bind_param("is", $_GET["id"], $h["consumer"])) {
                                                        if ($stmt2->execute()){
                                                            if ($stmt3->bind_param("i", $credit[0]["payment_data"])) {
                                                                if ($stmt3->execute()){
                                                                    $go=true;
                                                                } else {
                                                                    $result[OK] = 472;
                                                                    $result[ERROR] = "Errore durante la esecuzione della query di cancellazione della carta ". $stmt2->error;
                                                                }  
                                                            } else {
                                                                $result[OK] = 471;
                                                                $result[ERROR] = "Errore durante la preparazione della query di cancellazione dati della carta";
                                                            }
                                                        } else {
                                                            $result[OK] = 470;
                                                            $result[ERROR] = "Errore durante la esecuzione della query di modifica della carta ". $stmt2->error;
                                                        }                                                        
                                                    } else {
                                                        $result[OK] = 469;
                                                        $result[ERROR] = "Errore durante la preparazione della query di modifica dati della carta";
                                                    }
                                                } else {
                                                    $result[OK] = 468;
                                                    $result[ERROR] = "Errore durante la esecuzione della query di estrazione dati della carta ". $stmt->error;
                                                }
                                            } else {
                                                $result[OK] = 467;
                                                $result[ERROR] = "Errore durante la preparazione della query di estrazione dati della carta";
                                            }
                                        }
                                    } else {
                                        $result[OK] = 465;
                                        $result[ERROR] = "Errore input";
                                    } 
                                }
                            }
                            if ($go) {
                                $stmt = $conn->prepare('SELECT PD.PIN AS pin, P.code AS code, P.payment_type AS ptype, P.ID AS ID FROM PAYMENT_DATA AS PD 
                                                        JOIN PAYMENT_METHODS AS P ON P.payment_data=PD.ID
                                                        WHERE P.owner=?');
                                if (!$stmt) {
                                    $result[OK] = 453;
                                    $result[ERROR] = "Errore durante la preparazione della query di pagamento";
                                } else {
                                    if ( $stmt->bind_param("s", $h["consumer"])) {
                                        if ($stmt->execute()) {
                                            $payments = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                                            class br extends DOMElement {
                                                function __construct() {
                                                    parent::__construct('br');
                                                }
                                            }
                                            $page = new DOMDocument();
                                            $page->normalizeDocument();
                                            $page->formatOutput = true;
                                            if (count($payments) === 0) {
                                                $p = $page->createElement('p');
                                                $p->setAttribute('class', 'no-messages font-italic text-center lead w-50 mx-auto border rounded');
                                                $p->appendChild($page->createTextNode("Nessun metodo di pagamento registrato"));
                                                $page->appendChild($p);
                                                $result[RESULT] = [];
                                            } else {
                                                foreach ($payments as $i => $payment) {
                                                    $result[RESULT][] = array("code"=>$payment["code"], "payment_type"=>$payment["ptype"], "ID" =>$payment["ID"]);
                                                    $form = $page->createElement('form');
                                                    $div1 = $page->createElement('div');
                                                    $div2 = $page->createElement('div');
                                                    $div3 = $page->createElement('div');
                                                    $code = $page->createElement('input');
                                                    $data = $page->createElement('input');
                                                    $pin = $page->createElement('input');
                                                    
                                                    $form->setAttribute('class', 'card border border-secondary rounded p-3 mb-2 container card-'.$payment["ID"]);
                                                    $form->setAttribute('onsubmit', 'modifyPayment(); return false;');

                                                    $div1->setAttribute('class', 'form-group row w-100 align-items-center');
                                                    $div2->setAttribute('class', 'form-group row w-100 align-items-center');
                                                    $div3->setAttribute('class', 'form-group row w-100 align-items-center');
                                                    
                                                    $code->setAttribute('class', 'form-control col');
                                                    $code->setAttribute('type', 'text');
                                                    $code->setAttribute('name', 'code');
                                                    $code->setAttribute('disabled', '');
                                                    $code->setAttribute('value', $payment["code"]);
                                                    
                                                    $data->setAttribute('class', 'form-control col');
                                                    $data->setAttribute('type', 'text');
                                                    $data->setAttribute('name', 'data');
                                                    $data->setAttribute('disabled', '');
                                                    $data->setAttribute('value', $payment["ptype"]);

                                                    $pin->setAttribute('class', 'form-control col');
                                                    $pin->setAttribute('type', 'password');
                                                    $pin->setAttribute('name', 'pin');
                                                    $pin->setAttribute('disabled', '');
                                                    $pin->setAttribute('value', '');

                                                    $delete = $page->createElement('button');
                                                    $delete->setAttribute('class', 'btn btn-secondary btn-outline-light col-auto ml-3');
                                                    $trashIcon = $page->createElement('i');
                                                    $trashIcon->setAttribute('class', 'fas fa-trash-alt');
                                                    $delete->setAttribute('onclick', 'deletePayCard('.$payment["ID"].'); return false;');
                                                    $delete->appendChild($trashIcon);

                                                    $labelCode = $page->createElement('label');
                                                    $labelCode->setAttribute('class', 'col-auto mb-1');
                                                    $labelCode->appendChild($page->createTextNode('Codice: '));
                                                    $div1->appendChild($labelCode);
                                                    $div1->appendChild($code);
                                                    $labelCode = $page->createElement('label');
                                                    $labelCode->setAttribute('class', 'col-auto mb-1');
                                                    $labelCode->appendChild($page->createTextNode('Tipo: '));
                                                    $div2->appendChild($labelCode);
                                                    $div2->appendChild($data);
                                                    $labelCode = $page->createElement('label');
                                                    $labelCode->setAttribute('class', 'col-auto mb-1');
                                                    $labelCode->appendChild($page->createTextNode('PIN: '));
                                                    $div3->appendChild($labelCode);
                                                    $div3->appendChild($pin);
                                                    $div3->appendChild($delete);

                                                    $form->appendChild($div1);
                                                    $form->appendChild($div2);
                                                    $form->appendChild($div3);
                                                    $page->appendChild($form);
                                                }
                                            }
                                            $res = html_entity_decode($page->saveHTML());
                                            $result[OK] = true;
                                            $result[HTML] = $res;
                                        } else {
                                            $result[OK] = 454;
                                            $result[ERROR] = "Errore nell'esecuzione della query per il metodo di pagamento";
                                        }   
                                    } else {
                                        $result[OK] = 455;
                                        $result[ERROR] = "Errore nella creazione della query per il metodo di pagamento";                    
                                    }
                                }
                            }
                        } else {
                            $result[OK] = 456;
                            $result[ERROR] = "Account non consumatore";
                        }
                    } else {
                        $result[OK] = 457;
                        $result[ERROR] = "Errore nell'esecuzione della query per il cliente";
                    }
                } else {
                    $result[OK] = 458;
                    $result[ERROR] = "Errore nella creazione della query per il cliente";
                } 
            } else {
                $result[OK] = 459;
                $result[ERROR] = "Errore account non unico";
            }
        }
    }
} else {
    $result[OK] = 450;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));
if ($result[OK] !== true and isset($pin_id) and is_numeric($pin_id)) {
    $conn->query("DELETE FROM PAYMENT_DATA WHERE ID=$pin_id");
}
?>