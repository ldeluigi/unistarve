var alert;
var alertM;
$(document).ready(function() {
  alert = $(".banner").html();
  $(".banner").html("");
  $.ajax({
    type: "get",
    url: "/api/product.php?prodid=" + getQueryVariable("id"),
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#product-container").html(result.html);
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="tooltip"]').on("shown.bs.tooltip", function() {
          report_tooltip_shown = true;
        });
        $('[data-toggle="tooltip"]').on("hide.bs.tooltip", function() {
          report_tooltip_shown = false;
        });
        var modal = $("#productModal");
        modal.on("hidden.bs.modal", function(e) {
          window.history.back();
        });
        modal.modal();
        alertM = $(".bannerM").html();
        $(".bannerM").html("");
        mediaQuery();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
});

function mediaQuery() {
  if (
    window.matchMedia("(min-width: 768px)").matches ||
    $(window).width() > 768
  ) {
    $(".hide-sm").show();
    $("#imgDiv").addClass("ml-3");
  } else {
    $(".hide-sm").hide();
    $("#imgDiv").removeClass("ml-3");
  }
}

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (decodeURIComponent(pair[0]) == variable) {
      return decodeURIComponent(pair[1]);
    }
  }
}

var report_tooltip_shown = false;

function report(id) {
  if (report_tooltip_shown) {
    $('[data-toggle="tooltip"]').tooltip("hide");
    $.ajax({
      type: "get",
      url: "/api/report-image.php?prodid=" + getQueryVariable("id"),
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $(".bannerM").html(alertM);
          $(".alertM").removeClass("alert-danger");
          $(".alertM").addClass("alert-success");
          $(".msgM").text("Segnalazione inviata!");
        } else if (result.ok === 500) {
          window.location.replace("/login/login.html");
        } else {
          $(".bannerM").html(alertM);
          $(".alertM").removeClass("alert-success");
          $(".alertM").addClass("alert-danger");
          $(".msgM").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".bannerM").html(alertM);
        $(".alertM").removeClass("alert-success");
        $(".alertM").addClass("alert-danger");
        $(".msgM").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}
