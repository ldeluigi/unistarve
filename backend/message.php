<?php
require("base.php");
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 201;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $go = true;
        if (isset($_GET["delete"])) {
            $go = false;
            $stmt = $conn->prepare("DELETE FROM MESSAGES WHERE ID=? AND Rec_ID=?");
            if (!$stmt) {
                $result[OK] = 206;
                $result[ERROR] = "Errore durante la preparazione della query";
            } else {
                if ($stmt->bind_param("ii", $_GET["delete"], $_SESSION[ACCOUNT])) {
                    if ($stmt->execute()) {
                        if ($stmt->affected_rows > 0) {
                            $go = true;
                        } else {
                            $result[OK] = 208;
                            $result[ERROR] = "Nessun messaggio cancellato con questo ID";
                        }
                    } else {
                        $result[OK] = 207;
                        $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                    } 
                } else {
                    $result[OK] = 208;
                    $result[ERROR] = "Errore nella creazione della query per i messaggi, parametro invalido";
                }
            }
        }
        if ($go) {
            $stmt = $conn->prepare("SELECT time, text, ID FROM MESSAGES WHERE Rec_ID=? ORDER BY time DESC");
            if (!$stmt) {
                $result[OK] = 202;
                $result[ERROR] = "Errore durante la preparazione della query";
            } else {
                if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                    if ($stmt->execute()) {
                        if (isset($_GET["count"])) {
                            $result[OK] = true;
                            $result["count"] = $stmt->get_result()->num_rows;
                        }  else {
                            $messages = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                            $page = new DOMDocument();
                            $page->normalizeDocument();
                            $page->formatOutput = true;
                            if (count($messages) == 0) {
                                $p = $page->createElement('p');
                                $p->setAttribute('class', 'no-messages font-italic text-center lead w-50 mx-auto border rounded');
                                $p->appendChild($page->createTextNode("Nessun messaggio"));
                                $page->appendChild($p);
                            } else {
                                $list = $page->createElement('ul');
                                $list->setAttribute('class', 'messages list-group');
                                foreach ($messages as $message) {
                                    $el = $page->createElement('li');
                                    $el->setAttribute('class', 'message list-group-item flex-column align-items-start msg-'.$message["ID"]);

                                    $h = $page->createElement('div');
                                    $h->setAttribute('class', 'messagetime d-flex justify-content-between');


                                    $time = $page->createElement('h5');
                                    $time->setAttribute('class', 'mb-1');
                                    $time->appendChild($page->createTextNode($message["time"]));
                                    $h->appendChild($time);

                                    $id = $page->createElement('small');
                                    $id->setAttribute('class', 'messageid');
                                    $id->appendChild($page->createTextNode($message["ID"]));
                                    $h->appendChild($id);

                                    $el->appendChild($h);
            
                                    $text = $page->createElement('p');
                                    $text->setAttribute('class', 'messagetext text-md-left w-100 p-3');
                                    $text->appendChild($page->createTextNode($message["text"]));
                                    $el->appendChild($text);
            
                                    
            
                                    $del = $page->createElement('button');
                                    $del->setAttribute('class', 'delmessage float-right btn btn-outline-light del-'.$message["ID"]);
                                    $del->setAttribute('type', 'button');
                                    $del->setAttribute('onclick', 'deleteMessage('.$message["ID"].');');
                                    $icon = $page->createElement('i');
                                    $icon->setAttribute('class', "fas fa-trash-alt");
                                    $icon->setAttribute('aria-hidden', "true");
                                    $del->appendChild($icon);
                                    $el->appendChild($del);
            
                                    $list->appendChild($el);
                                }
                                $page->appendChild($list);
                            }
                            $res = html_entity_decode($page->saveHTML());
                            $result[OK] = true;
                            $result[HTML] = $res;
                        }
                    } else {
                        $result[OK] = 203;
                        $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                    } 
                } else {
                    $result[OK] = 204;
                    $result[ERROR] = "Errore nella creazione della query per i messaggi";
                }
            }
        }
    }
} else {
    $result[OK] = 205;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));
?>