document.addEventListener(
  "DOMContentLoaded",
  function() {
    var noscriptContainer = document.getElementById("async-div");
    if (noscriptContainer != null) {
      var container = document.createElement("div");
      container.innerHTML = noscriptContainer.textContent;
      document.body.appendChild(container);
    }
  },
  false
);

var lazyI;
if (typeof jQuery != "undefined" && typeof jQuery != "undefined") {
  $(document).ready(function() {
    if ($(".carousel").length > 0) {
      lazyI = new LazyLoad({
        element_selector: ".lazy"
      });
      return $(".carousel").on("slid.bs.carousel", function(ev) {
        lazyI.update();
      });
    }
  });
} else {
  console.log("Error in carousel");
}

window.lazyLoadOptions = {
  elements_selector: ".lazy"
};
/* window.addEventListener('LazyLoad::Initialized', function (e) {
  console.log(e.detail.instance);
}, false); DEBUG */
