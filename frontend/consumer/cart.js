function addToCart(id) {
  $.ajax({
    type: "GET",
    url: "/api/cart.php?update&quantity=1&prodid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        mediaQuery();
        $("div.block.overflow-auto")
          .finish()
          .animate({ scrollTop: 0 }, "slow");
        $(".banner").html(alert);
        $(".alert").removeClass("alert-danger");
        $(".alert").addClass("alert-success");
        $(".msg").text("Oggetto aggiunto al carrello");
      } else if (result.ok === 307) {
        $.ajax({
          type: "GET",
          url: "/api/cart.php?set&quantity=1&prodid=" + id,
          success: function(result, status, xhr) {
            if (result.ok && result.ok === true) {
              mediaQuery();
              $("div.block.overflow-auto")
                .finish()
                .animate({ scrollTop: 0 }, "slow");
              $(".banner").html(alert);
              $(".alert").removeClass("alert-danger");
              $(".alert").addClass("alert-success");
              $(".msg").text("Oggetto aggiunto al carrello");
            } else {
              $(".banner").html(alert);
              $(".alert").removeClass("alert-success");
              $(".alert").addClass("alert-danger");
              $(".msg").text(
                "Errore: " + result.message + " (" + result.ok + ")"
              );
            }
          },
          error: function(xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
          }
        });
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function plusCart(id) {
  $.ajax({
    type: "GET",
    url: "/api/cart.php?update&quantity=1&prodid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#cart-container").html(result.html);
        mediaQuery();
        lazyI.update();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function minusCart(id) {
  $.ajax({
    type: "GET",
    url: "/api/cart.php?update&quantity=-1&prodid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#cart-container").html(result.html);
        mediaQuery();
        lazyI.update();
      } else {
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function removeCart(id) {
  $.ajax({
    type: "GET",
    url: "/api/cart.php?delete&prodid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $(".cartproduct").finish();
        $(".cartprod-" + id).fadeOut(function() {
          $("#cart-container").html(result.html);
        });
        mediaQuery();
        lazyI.update();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

validateCart = false;

function buyCart() {
  var dtToday = new Date();
  var month = dtToday.getMonth() + 1;
  var day = dtToday.getDate();
  var year = dtToday.getFullYear();
  if (month < 10) month = "0" + month.toString();
  if (day < 10) day = "0" + day.toString();
  var maxDate = year + "-" + month + "-" + day;
  $("#for-date").attr("min", maxDate);

  validateCart = false;
  var html = "";
  var html2 = "";
  $.ajax({
    type: "GET",
    url: "/api/locations.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        if (result.result.length == 0) {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: nessun luogo di consegna disponibile");
        } else {
          result.result.forEach(element => {
            html +=
              "<option value='" +
              element.ID +
              "'>" +
              element.address +
              "</option>";
          });
          $("#location-select").html(html);
          $.ajax({
            type: "GET",
            url: "/api/payments.php",
            success: function(result, status, xhr) {
              if (result.ok && result.ok === true) {
                if (
                  result.result == null ||
                  result.result == undefined ||
                  result.result.length == 0
                ) {
                  $(".banner").html(alert);
                  $(".alert").removeClass("alert-success");
                  $(".alert").addClass("alert-danger");
                  $(".msg").text(
                    "Errore: nessun metodo di pagamento disponibile. Aggiungine uno dalla sezione profilo personale."
                  );
                  $(".block")
                    .finish()
                    .animate(
                      { scrollTop: $("#order-container").offset().top },
                      "slow"
                    );
                } else {
                  result.result.forEach(element => {
                    html2 +=
                      "<option value='" +
                      element.ID +
                      "'>" +
                      element.payment_type +
                      " " +
                      element.code +
                      "</option>";
                  });
                  $("#payment-select").html(html2);
                  validateCart = true;
                  $("#location-modal").modal();
                }
              } else {
                $(".banner").html(alert);
                $(".alert").removeClass("alert-success");
                $(".alert").addClass("alert-danger");
                $(".msg").text(
                  "Errore: " + result.message + " (" + result.ok + ")"
                );
              }
            },
            error: function(xhr, status, error) {
              $(".banner").html(alert);
              $(".alert").removeClass("alert-success");
              $(".alert").addClass("alert-danger");
              $(".msg").text("Errore nella risoluzione della richiesta AJAX");
            }
          });
        }
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function convertDate(inputFormat) {
  function pad(s) {
    return s < 10 ? "0" + s : s;
  }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join("-");
}

function createOrder() {
  if (validateCart) {
    var location = $("#location-select").val();
    var payment = $("#payment-select").val();
    var time = $("#for-time").val() ? $("#for-time").val() : false;
    var date = $("#for-date").val() ? convertDate($("#for-date").val()) : false;
    if (!time || !date) {
      $("#location-modal").modal("toggle");
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text(
        "Non hai specificato l'ora o la data di consegna. Operazione annullata."
      );
      return;
    }
    $.ajax({
      type: "GET",
      url:
        "/api/order.php?order&location=" +
        location +
        "&pay=" +
        payment +
        (time ? "&time=" + time : "") +
        (date ? "&date=" + date : ""),
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          mediaQuery();
          $("#location-modal").modal("toggle");
          $(".banner").html(alert);
          $(".alert").removeClass("alert-danger");
          $(".alert").addClass("alert-success");
          $(".msg").text("Acquisto completato");
          if (typeof showCart != undefined) {
            showCart();
          }
        } else {
          $("#location-modal").modal("toggle");
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
          if (typeof showCart !== undefined) {
            showCart();
          }
        }
      },
      error: function(xhr, status, error) {
        $("#location-modal").modal("toggle");
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}
