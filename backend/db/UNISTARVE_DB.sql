-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.0              
-- * Generator datetime: Sep  6 2018              
-- * Generation datetime: Sun Jan  6 16:37:51 2019 
-- * LUN file: C:\Users\Utente\Desktop\Progettoni\Tecnologie Web\project\unistarve\docs\db\UNISTARVE_DB.lun 
-- * Schema: UNISTARVE/2 
-- ********************************************* 


-- Database Section
-- ________________ 

create database IF NOT EXISTS my_unistarve CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use my_unistarve;


-- Tables Section
-- _____________ 

create table ACCOUNTS (
     ID int not null auto_increment,
     productor int,
     transporter int,
     consumer varchar(20),
     last_access datetime,
     registration datetime not null,
     email varchar(50) not null,
     pword varchar(128) not null,
     administrator TINYINT(1) NOT NULL DEFAULT '0',
     constraint IDACCOUNT primary key (ID),
     constraint email_unique unique (email),
     constraint FKaccede_a_ID unique (productor),
     constraint FKaccede_p_1_ID unique (transporter),
     constraint FKaccede_p_ID unique (consumer));

create table MESSAGES (
     time datetime not null,
     text varchar(3072) not null,
     ID int not null auto_increment,
     Rec_ID int not null,
     constraint IDMESSAGE primary key (ID));


create table CART (
     ID int not null auto_increment,
     constraint IDCARRELLO_ID primary key (ID));

create table CELL_NUMBERS (
     phone_number varchar(20) not null,
     owner int not null,
     constraint IDNumero_cell primary key (phone_number));

create table CHOICES (
     product_group int not null,
     menu int not null,
     quantity int not null,
     constraint IDSCELTA primary key (menu, product_group));

create table COMPRISE (
     product int not null,
     orderID int not null,
     quantity int not null,
     price float(20) not null,
     constraint IDcomprende primary key (product, orderID));

create table CONSUMERS (
     surname char(30) not null,
     name char(30) not null,
     CF varchar(20) not null,
     cart int not null,
     constraint IDCONSUMATORE_SINGOLO_ID primary key (CF),
     constraint FKtiene_ID unique (cart));

create table COVERS (
     transporter int not null,
     area int not null,
     constraint IDcopre primary key (area, transporter));

create table INCLUDE (
     includes int not null,
     menu int not null,
     quantity int not null,
     constraint IDinclude primary key (includes, menu));

create table LOCATIONS (
     ID int not null auto_increment,
     address varchar(200) not null,
     indications varchar(2048) not null,
     constraint IDLOCATION primary key (ID));

create table ON_CART (
     product int not null,
     cart int not null,
     quantity int not null DEFAULT '1',
     constraint IDin_carrello primary key (cart, product));

create table OPTIONS (
     product int not null,
     product_group int not null,
     constraint IDOPZIONE primary key (product, product_group));

create table ORDERS (
     ID int not null auto_increment,
     delivered datetime,
     pickedup datetime,
     ordination int not null,
     destinated_to int not null,
     transportation int not null,
     constraint IDORDINE primary key (ID));

create table ORDINABLES (
     ID int not null auto_increment,
     description varchar(2048),
     img_link varchar(1024),
     elimination_datetime datetime,
     name varchar(200) not null,
     short_description varchar(300) not null,
     available tinyint(1) not null,
     cost float(20) not null,
     isMenu tinyint(1) not null,
     productor int not null,
     constraint IDORDINABILE_ID primary key (ID));

create table ORDINATIONS (
     ID int not null auto_increment,
     payed tinyint(1) not null,
     delivery_datetime datetime,
     ordination_datetime datetime not null,
     for_datetime datetime,
     client varchar(20) not null,
     location int not null,
     pay_with int not null,
     constraint IDORDINAZIONE primary key (ID));

create table PAYMENT_DATA (
     ID int not null auto_increment,
     PIN varchar(20) not null,
     constraint IDDATI_PAGAMENTO primary key (ID));

create table PAYMENT_METHODS (
     ID int not null auto_increment,
     payment_data int,
     code varchar(50) not null,
     payment_type char(20) not null,
     owner varchar(20) not null,
     constraint IDMETODO_PAGAMENTO primary key (ID),
     constraint FKpagamento_ID unique (payment_data));

create table PRODUCT_GROUPS (
     ID int not null auto_increment,
     optional tinyint(1) not null,
     constraint IDGRUPPO_PRODOTTI primary key (ID));

create table PRODUCTORS (
     P_IVA varchar(20) not null,
     address varchar(200) not null,
     name varchar(200) not null,
     ID int not null auto_increment,
     unsubscription datetime,
     description varchar(2048) not null,
     constraint IDPRODUTTORE_ID primary key (ID));

create table TRANSPORTERS (
     address varchar(200) not null,
     name varchar(200) not null,
     P_IVA varchar(20) not null,
     ID int not null auto_increment,
     unsubscription varchar(2048),
     active tinyint(1) not null,
     constraint IDTRASPORTATORE_ID primary key (ID));


-- Constraints Section
-- ___________________ 

alter table ACCOUNTS add constraint FKaccede_a_FK
     foreign key (productor)
     references PRODUCTORS (ID);

alter table ACCOUNTS add constraint FKaccede_p_1_FK
     foreign key (transporter)
     references TRANSPORTERS (ID);

alter table ACCOUNTS add constraint FKaccede_p_FK
     foreign key (consumer)
     references CONSUMERS (CF);

-- Not implemented
-- alter table CART add constraint IDCARRELLO_CHK
--     check(exists(select * from CONSUMERS
--                  where CONSUMERS.cart = ID)); 

alter table CELL_NUMBERS add constraint FKtelefoni
     foreign key (owner)
     references TRANSPORTERS (ID);

alter table CHOICES add constraint FKmenu2
     foreign key (menu)
     references ORDINABLES (ID);

alter table CHOICES add constraint FKgruppo
     foreign key (product_group)
     references PRODUCT_GROUPS (ID);

alter table COMPRISE add constraint FKordine
     foreign key (orderID)
     references ORDERS (ID);

alter table COMPRISE add constraint FKprodotto2
     foreign key (product)
     references ORDINABLES (ID);

-- Not implemented
-- alter table CONSUMERS add constraint IDCONSUMATORE_SINGOLO_CHK
--     check(exists(select * from ACCOUNTS
--                  where ACCOUNTS.consumer = CF)); 

alter table CONSUMERS add constraint FKtiene_FK
     foreign key (cart)
     references CART (ID);

alter table COVERS add constraint FKcop_PRO
     foreign key (area)
     references PRODUCTORS (ID);

alter table COVERS add constraint FKcop_TRA
     foreign key (transporter)
     references TRANSPORTERS (ID);

alter table INCLUDE add constraint FKmenu
     foreign key (menu)
     references ORDINABLES (ID);

alter table INCLUDE add constraint FKincluso
     foreign key (includes)
     references ORDINABLES (ID);

alter table ON_CART add constraint FKcarrello
     foreign key (cart)
     references CART (ID);

alter table ON_CART add constraint FKprodotto3
     foreign key (product)
     references ORDINABLES (ID);

alter table OPTIONS add constraint FKprodotto4
     foreign key (product_group)
     references PRODUCT_GROUPS (ID);

alter table OPTIONS add constraint FKgruppo2
     foreign key (product)
     references ORDINABLES (ID);

alter table ORDERS add constraint FKgenera
     foreign key (ordination)
     references ORDINATIONS (ID);

alter table ORDERS add constraint FKriguarda
     foreign key (destinated_to)
     references PRODUCTORS (ID);

-- Not implemented
-- alter table ORDINABLES add constraint IDORDINABILE_CHK
--     check(exists(select * from COMPRISE
--                  where COMPRISE.product = ID)); 

-- Not implemented
-- alter table ORDINABLES add constraint IDORDINABILE_CHK
--     check(exists(select * from OPTIONS
--                  where OPTIONS.product = ID)); 

alter table ORDINABLES add constraint FKdispose
     foreign key (productor)
     references PRODUCTORS (ID);

alter table ORDINATIONS add constraint FKexecute
     foreign key (client)
     references CONSUMERS (CF);

alter table ORDINATIONS add constraint FKdestination
     foreign key (location)
     references LOCATIONS (ID);

alter table ORDERS add constraint FKis_assigned_to
     foreign key (transportation)
     references TRANSPORTERS (ID);

alter table ORDINATIONS add constraint FKpays_with
     foreign key (pay_with)
     references PAYMENT_METHODS (ID);

alter table PAYMENT_METHODS add constraint FKpagamento_FK
     foreign key (payment_data)
     references PAYMENT_DATA (ID);

alter table PAYMENT_METHODS add constraint FKpays_with2
     foreign key (owner)
     references CONSUMERS (CF);

alter table MESSAGES add constraint FKReceives
     foreign key (Rec_ID)
     references ACCOUNTS (ID);

-- Not implemented
-- alter table PRODUCTORS add constraint IDPRODUTTORE_CHK
--     check(exists(select * from ACCOUNTS
--                  where ACCOUNTS.productor = ID)); 

-- Not implemented
-- alter table TRANSPORTERS add constraint IDTRASPORTATORE_CHK
--     check(exists(select * from ACCOUNTS
--                  where ACCOUNTS.transporter = ID)); 

-- Not implemented
-- alter table TRANSPORTERS add constraint IDTRASPORTATORE_CHK
--     check(exists(select * from CELL_NUMBERS
--                  where CELL_NUMBERS.owner = ID)); 


-- Index Section
-- _____________ 

