<?php
require("base.php");
function checkPword() {
    global $PMAX;
    return ( is_string($_GET["pword"]) and ( strlen($_GET["pword"]) <= $PMAX ) and ( strlen($_GET["pword"]) > 0 ) );
}
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 700;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $stmt = $conn->prepare("SELECT productor, last_access, email, ID FROM ACCOUNTS WHERE ID=? AND productor IS NOT NULL");
        if (!$stmt) {
            $result[OK] = 701;
            $result[ERROR] = "Errore durante la preparazione della query";
        } else {
            if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                if ($stmt->execute()) {
                    $account = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                    if (count($account) === 1) {
                        $h = $account[0];
                        if ($h["productor"]) {
                            $go=true;
                            if (isset($_GET["update"])) {
                                $go=false;
                                if (isset($_GET["name"]) and isset($_GET["address"]) and isset($_GET["email"]) and isset($_GET["pword"]) and isset($_GET["piva"]) and isset($_GET["d"]) and
                                    is_string($_GET["name"]) and strlen($_GET["name"]) <= $NMAX and 
                                    is_string($_GET["address"]) and strlen($_GET["address"]) <= $AMAX and 
                                    strlen((string) $_GET["piva"]) <= $PIVAMAX and 
                                    is_string($_GET["d"]) and strlen($_GET["d"]) <= $DMAX and 
                                    is_string($_GET["email"]) and strlen($_GET["email"]) <= $EMAX and filter_var($_GET["email"], FILTER_VALIDATE_EMAIL)) {
                                    $stmt = $conn->prepare("UPDATE ACCOUNTS SET email=?".(checkPword() ? ", pword=?" : "")." WHERE ID=?");
                                    $stmt2 = $conn->prepare("UPDATE PRODUCTORS SET name=?, address=?, P_IVA=?, description=? WHERE ID=?");
                                    if (!$stmt or !$stmt2) {
                                        $result[OK] = 702;
                                        $result[ERROR] = "Errore durante la preparazione della query di modifica produttore";
                                    } else {
                                        if ((checkPword() ? $stmt->bind_param("ssi", $_GET["email"], $_GET["pword"], $h["ID"]) : $stmt->bind_param("si", $_GET["email"], $h["ID"]) ) 
                                                        and $stmt2->bind_param("ssssi", $_GET["name"], $_GET["address"], $_GET["piva"], $_GET["d"], $h["productor"])) {
                                            if ($stmt->execute() and $stmt2->execute() and ($stmt->affected_rows > 0 or $stmt2->affected_rows > 0)) {
                                                $go=true;
                                            } else {
                                                $result[OK] = 703;
                                                $result[ERROR] = "Errore durante la esecuzione della query di modifica produttore ". $stmt->error. " ".$stmt2->error."Lines changed: ".($stmt->affected_rows + $stmt2->affected_rows);
                                            }
                                        } else {
                                            $result[OK] = 704;
                                            $result[ERROR] = "Errore durante la preparazione della query di modifica produttore";
                                        }
                                    }
                                } else {
                                    $result[OK] = 705;
                                    $result[ERROR] = "Errore input";
                                }
                            }
                            if ($go) {
                                $stmt = $conn->prepare("SELECT name, address, P_IVA, description FROM PRODUCTORS WHERE ID=?");
                                if (!$stmt) {
                                    $result[OK] = 706;
                                    $result[ERROR] = "Errore durante la preparazione della query consumatore";
                                } else {
                                    if ( $stmt->bind_param("s", $h["productor"])) {
                                        if ($stmt->execute()) {
                                            $productor = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                                            if (count($productor) === 1) {
                                                $c = $productor[0];
                                                class br extends DOMElement {
                                                    function __construct() {
                                                        parent::__construct('br');
                                                    }
                                                }
                                                
                                                $page = new DOMDocument();
                                                $page->normalizeDocument();
                                                $page->formatOutput = true;

                                                $form = $page->createElement('form');
                                                $divname = $page->createElement('div');
                                                $divaddress = $page->createElement('div');
                                                $divemail = $page->createElement('div');
                                                $divpword = $page->createElement('div');
                                                $divcode = $page->createElement('div');
                                                $divaccess = $page->createElement('div');
                                                $divdesc = $page->createElement('div');
                                                $name = $page->createElement('input');
                                                $address = $page->createElement('input');
                                                $email = $page->createElement('input');
                                                $pword = $page->createElement('input');
                                                $code = $page->createElement('input');
                                                $access = $page->createElement('input');
                                                $desc = $page->createElement('textarea');
                                                
                                                $modifica = $page->createElement('button');
                                                $annulla = $page->createElement('button');
                                                
                                                $form->setAttribute('class', 'border border-secondary rounded p-3');
                                                $form->setAttribute('id','form');
                                                $form->setAttribute('onsubmit', 'submitHandler(); return false;');

                                                $divname->setAttribute('class', 'form-group');
                                                $divaddress->setAttribute('class', 'form-group');
                                                $divemail->setAttribute('class', 'form-group');
                                                $divpword->setAttribute('class', 'form-group');
                                                $divdesc->setAttribute('class', 'form-group w-100');
                                                
                                                $name->setAttribute('class', 'form-control');
                                                $name->setAttribute('type', 'text');
                                                $name->setAttribute('id', 'name');
                                                $name->setAttribute('name', 'name');
                                                $name->setAttribute('value', $c["name"]);
                                                
                                                $address->setAttribute('class', 'form-control');
                                                $address->setAttribute('type', 'text');
                                                $address->setAttribute('id', 'address');
                                                $address->setAttribute('name', 'address');
                                                $address->setAttribute('value', $c["address"]);

                                                $email->setAttribute('class', 'form-control');
                                                $email->setAttribute('type', 'email');
                                                $email->setAttribute('id', 'email');
                                                $email->setAttribute('name', 'email');
                                                $email->setAttribute('value', (isset($_GET["email"]) ? $_GET["email"] : $h["email"]));

                                                $pword->setAttribute('class', 'form-control');
                                                $pword->setAttribute('type', 'password');
                                                $pword->setAttribute('id', 'pword');
                                                $pword->setAttribute('name', 'pword');
                                                $pword->setAttribute('value', '');
                                                $pword->setAttribute('placeholder', 'New password');
                                                $pword->setAttribute('minlength', '8');
                                                $pword->setAttribute('maxlength', '20');
                                                $pword->setAttribute('pattern', '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$+=!@%]).{8,20}$');

                                                $code->setAttribute('class', 'form-control');
                                                $code->setAttribute('id', 'piva');
                                                $code->setAttribute('type', 'text');
                                                $code->setAttribute('name', 'fcode');
                                                $code->setAttribute('value', $c["P_IVA"]);

                                                $desc->setAttribute('form', 'form');
                                                $desc->setAttribute('maxlength', $DMAX);
                                                $desc->appendChild($page->createTextNode($c["description"]));
                                                $desc->setAttribute('class', 'w-100 text-area-desc form-control');
                                                $desc->setAttribute('id', 'description');

                                                $access->setAttribute('class', 'form-control mb-2');
                                                $access->setAttribute('readonly','');
                                                $access->setAttribute('type', 'text');
                                                $access->setAttribute('name', 'last access');
                                                $access->setAttribute('value', $h["last_access"]);
                                                
                                                $modifica->setAttribute('class','btn btn-secondary');
                                                $modifica->setAttribute('type','submit');
                                                $modifica->appendChild($page->createTextNode('Modifica'));

                                                $annulla->setAttribute('class', 'btn btn-secondary');
                                                $annulla->setAttribute('onclick','annullaHandler(); return false;');
                                                $annulla->appendChild($page->createTextNode('Annulla'));
                                                
                                                $divname->appendChild($page->createTextNode('Nome: '));
                                                $divname->appendChild($name);
                                                $divaddress->appendChild($page->createTextNode('Indirizzo: '));
                                                $divaddress->appendChild($address);
                                                $divemail->appendChild($page->createTextNode('Email: '));
                                                $divemail->appendChild($email);
                                                $divpword->appendChild($page->createTextNode('Password: '));
                                                $divpword->appendChild($pword);
                                                $divdesc->appendChild($page->createTextNode('Descrizione: '));
                                                $divdesc->appendChild($desc);
                                                
                                                $divcode->appendChild($page->createTextNode('Partita IVA: '));
                                                $divcode->appendChild($code);

                                                $divaccess->appendChild($page->createTextNode('Ultimo Accesso: '));
                                                $divaccess->appendChild($access);
                                                
                                                $form->appendChild($divname);
                                                $form->appendChild($divaddress);
                                                $form->appendChild($divdesc);
                                                $form->appendChild($divemail);
                                                $form->appendChild($divpword);
                                                $form->appendChild($divcode);
                                                $form->appendChild($divaccess);
                                                $form->appendChild($modifica);
                                                $form->appendChild($annulla);
                                                
                                                $page->appendChild($form);
                                                
                                                $res = html_entity_decode($page->saveHTML());
                                                $result[OK] = true;
                                                $result[HTML] = $res;
                                            
                                            } else {
                                                $result[OK] = 707;
                                                $result[ERROR] = "Errore account non unico";
                                            }
                                        } else {
                                            $result[OK] = 708;
                                            $result[ERROR] = "Errore nell'esecuzione della query per il produttore";
                                        }   
                                    } else {
                                        $result[OK] = 709;
                                        $result[ERROR] = "Errore nella creazione della query per il produttore";                    
                                    }
                                }
                            }
                        } else {
                            $result[OK] = 710;
                            $result[ERROR] = "Account non consumatore";
                        }
                    } else {
                        $result[OK] = 711;
                        $result[ERROR] = "Errore nell'esecuzione della query per il produttore";
                    }
                } else {
                    $result[OK] = 712;
                    $result[ERROR] = "Errore nella creazione della query per il produttore";
                } 
            } else {
                $result[OK] = 713;
                $result[ERROR] = "Errore account non unico";
            }
        }
    }
} else {
    $result[OK] = 714;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));
?>