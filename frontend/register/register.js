$(document).ready(function() {
  var alert = $(".banner").html();
  var c = $(".c").html();
  var p = $(".p").html();
  var t = $(".t").html();
  $(".c").html("");
  $(".p").html("");
  $(".t").html("");
  $(".banner").html("");
  $("select").change(function() {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    if (valueSelected == "") {
      $(".c").html("");
      $(".p").html("");
      $(".t").html("");
    } else if (valueSelected == "c") {
      $(".c").html(c);
      $(".p").html("");
      $(".t").html("");
    } else if (valueSelected == "p") {
      $(".p").html(p);
      $(".c").html("");
      $(".t").html("");
    } else if (valueSelected == "t") {
      $(".t").html(t);
      $(".c").html("");
      $(".p").html("");
    }
  });
  $("#form1").submit(function() {
    var email = $("#email1").val();
    var password = $("#InputPassword").val();
    var role = $("#inlineFormCustomSelect").val();
    var name = $("#name1").val();
    var surname = $("#surname1").val();
    var cf = $("#CF1").val();
    var p_iva1 = $("#P_IVA1").val();
    var address1 = $("#address1").val();
    var description = $("#description1").val();
    var p_iva2 = $("#P_IVA2").val();
    var address2 = $("#address2").val();
    var telephone = $("#telephone1").val();
    $.ajax({
      type: "GET",
      url:
        "/api/register.php?email=" +
        email +
        "&pword=" +
        password +
        "&role=" +
        role +
        (name ? "&name=" + name : "") +
        (surname ? "&surname=" + surname : "") +
        (cf ? "&fc=" + cf : "") +
        (p_iva1 ? "&piva=" + p_iva1 : "") +
        (address1 ? "&address=" + address1 : "") +
        (description ? "&d=" + description : "") +
        (p_iva2 ? "&piva=" + p_iva2 : "") +
        (address2 ? "&address=" + address2 : "") +
        (telephone ? "&telephone=" + telephone : ""),

      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
    return false;
  });
});
