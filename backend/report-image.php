<?php
require("base.php");
$result = [];
session_start();
$conn = connectDB();
if (isset($_SESSION[ACCOUNT]))  {
    if (isset($_SESSION[REPORT]) and time() - $_SESSION[REPORT] < 3600) {
        $result[OK] = 507;
        $result[ERROR] = "Troppe segnalazioni. Aspetta ".date('i \m\i\n\u\t\i \e s \s\e\c\o\n\d\i', 3600 + $_SESSION[REPORT] - time());
    } else {      
        $conn = connectDB();
        if ($conn->connect_error) {
            $result[OK] = 501;
            $result[ERROR] = "Connessione al DB fallita";
        } else if (isset($_GET["prodid"]) and is_numeric($_GET["prodid"])) {
            $stmtCheck = $conn->prepare("SELECT COUNT(*) AS count FROM ORDINABLES WHERE ID=?");
            if ($stmtCheck !== false and $stmtCheck->bind_param("i", $_GET["prodid"]) and $stmtCheck->execute() and $stmtCheck->get_result()->fetch_assoc()["count"] > 0) {
                $stmt = $conn->prepare('INSERT INTO MESSAGES (time, text, Rec_ID)
                                    SELECT DISTINCT NOW(), CONCAT(?, O.name, " (", O.ID, ")", ?, O.img_link), A.ID FROM ACCOUNTS AS A
                                    JOIN ORDINABLES AS O ON O.ID=?
                                    WHERE A.administrator=1');
                if (!$stmt) {
                    $result[OK] = 502;
                    $result[ERROR] = "Errore durante la preparazione della query";
                } else {
                    $first = "L'utente con id ".$_SESSION[ACCOUNT]." ha segnalato il prodotto ";
                    $third = ". Link all'immagine segnalata: ";
                    if ($stmt->bind_param("ssi", $first, $third, $_GET["prodid"])) {
                        if ($stmt->execute()) {
                            $result[OK] = true;
                            $result[RESULT] = "Segnalazione effettuata.";
                            $_SESSION[REPORT] = time();
                        } else {
                            $result[OK] = 503;
                            $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                        } 
                    } else {
                        $result[OK] = 504;
                        $result[ERROR] = "Errore nella creazione della query per i messaggi, parametro invalido";
                    }
                }
            } else {
                $result[OK] = 505;
                $result[ERROR] = "Prodotto non trovato";
            }
        } else {
            $result[OK] = 506;
            $result[ERROR] = "ID del prodotto non specificato";
        }
    }
} else {
    $result[OK] = 500;
    $result[ERROR] = "Sessione scaduta";
} 
header('Content-Type: application/json');
echo(json_encode($result));
?>