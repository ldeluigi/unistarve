<?php
require("base.php");
function checkPword() {
    global $PMAX;
    return ( is_string($_GET["pword"]) and ( strlen($_GET["pword"]) <= $PMAX ) and ( strlen($_GET["pword"]) > 0 ) );
}
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 800;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $stmt = $conn->prepare("SELECT transporter, last_access, email, ID FROM ACCOUNTS WHERE ID=? AND transporter IS NOT NULL");
        if (!$stmt) {
            $result[OK] = 801;
            $result[ERROR] = "Errore durante la preparazione della query";
        } else {
            if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                if ($stmt->execute()) {
                    $account = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                    if (count($account) === 1) {
                        $h = $account[0];
                        if ($h["transporter"]) {
                            $go=true;
                            if (isset($_GET["update"])) {
                                $go=false;
                                if (isset($_GET["name"]) and isset($_GET["address"]) and isset($_GET["email"]) and isset($_GET["pword"])and isset($_GET["av"]) and
                                    is_string($_GET["name"]) and strlen($_GET["name"]) <= $TNMAX and 
                                    is_string($_GET["address"]) and strlen($_GET["address"]) <= $AMAX and 
                                    is_string($_GET["email"]) and strlen($_GET["email"]) <= $EMAX and filter_var($_GET["email"], FILTER_VALIDATE_EMAIL) and
                                    ($_GET["av"]) == 1 or $_GET["av"] == 0) {
                                    $stmt = $conn->prepare("UPDATE ACCOUNTS SET email=?".(checkPword() ? ", pword=?" : "")." WHERE ID=?");
                                    $stmt2 = $conn->prepare("UPDATE TRANSPORTERS SET name=?, address=?, active=? WHERE ID=?");
                                    if (!$stmt or !$stmt2) {
                                        $result[OK] = 802;
                                        $result[ERROR] = "Errore durante la preparazione della query di modifica trasportatore";
                                    } else {
                                        $av = $_GET["av"] == 1 ? 1 : 0;
                                        if ((checkPword() ? $stmt->bind_param("ssi", $_GET["email"], $_GET["pword"], $h["ID"]) : $stmt->bind_param("si", $_GET["email"], $h["ID"]) ) 
                                                        and $stmt2->bind_param("ssii", $_GET["name"], $_GET["address"], $av,$h["transporter"])) {
                                            if ($stmt->execute() and $stmt2->execute() and ($stmt->affected_rows > 0 or $stmt2->affected_rows > 0)) {
                                                $go=true;
                                            } else {
                                                $result[OK] = 803;
                                                $result[ERROR] = "Errore durante la esecuzione della query di modifica trasportatore ". $stmt->error. " ".$stmt2->error."Lines changed: ".$stmt->affected_rows." ".$stmt2->affected_rows;
                                            }
                                        } else {
                                            $result[OK] = 804;
                                            $result[ERROR] = "Errore durante la preparazione della query di modifica trasportatore";
                                        }
                                    }
                                } else {
                                    $result[OK] = 805;
                                    $result[ERROR] = "Errore input";
                                }
                            }
                            if ($go) {
                                $stmt = $conn->prepare("SELECT name, address, ID, p_iva, active AS av FROM TRANSPORTERS WHERE ID=? AND unsubscription IS NULL");
                                if (!$stmt) {
                                    $result[OK] = 806;
                                    $result[ERROR] = "Errore durante la preparazione della query trasportatore";
                                } else {
                                    if ( $stmt->bind_param("i", $h["transporter"])) {
                                        if ($stmt->execute()) {
                                            $transporters = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                                            if (count($transporters) === 1) {
                                                $c = $transporters[0];
                                                class br extends DOMElement {
                                                    function __construct() {
                                                        parent::__construct('br');
                                                    }
                                                }
                                                
                                                $page = new DOMDocument();
                                                $page->normalizeDocument();
                                                $page->formatOutput = true;

                                                $form = $page->createElement('form');
                                                $divname = $page->createElement('div');
                                                $divsurname = $page->createElement('div');
                                                $divemail = $page->createElement('div');
                                                $divpword = $page->createElement('div');
                                                $divcode = $page->createElement('div');
                                                $divaccess = $page->createElement('div');
                                                $name = $page->createElement('input');
                                                $surname = $page->createElement('input');
                                                $email = $page->createElement('input');
                                                $pword = $page->createElement('input');
                                                $code = $page->createElement('input');
                                                $access = $page->createElement('input');
                                                $piva = $page->createElement('input');

                                                $modifica = $page->createElement('button');
                                                $annulla = $page->createElement('button');
                                                $mdp = $page->createElement('button');
                                                
                                                $form->setAttribute('class', 'border border-secondary rounded p-3');
                                                $form->setAttribute('id','form');
                                                $form->setAttribute('onsubmit', 'submitHandler(); return false;');

                                                $divname->setAttribute('class', 'form-group');
                                                $divsurname->setAttribute('class', 'form-group');
                                                $divemail->setAttribute('class', 'form-group');
                                                $divpword->setAttribute('class', 'form-group');
                                                
                                                $name->setAttribute('class', 'form-control');
                                                $name->setAttribute('type', 'text');
                                                $name->setAttribute('id', 'name');
                                                $name->setAttribute('name', 'name');
                                                $name->setAttribute('value', $c["name"]);
                                                
                                                $surname->setAttribute('class', 'form-control');
                                                $surname->setAttribute('type', 'text');
                                                $surname->setAttribute('id', 'surname');
                                                $surname->setAttribute('name', 'surname');
                                                $surname->setAttribute('value', $c["address"]);

                                                $email->setAttribute('class', 'form-control');
                                                $email->setAttribute('type', 'email');
                                                $email->setAttribute('id', 'email');
                                                $email->setAttribute('name', 'email');
                                                $email->setAttribute('value', (isset($_GET["email"]) ? $_GET["email"] : $h["email"]));
                                                
                                                $pword->setAttribute('class', 'form-control');
                                                $pword->setAttribute('type', 'password');
                                                $pword->setAttribute('id', 'pword');
                                                $pword->setAttribute('name', 'pword');
                                                $pword->setAttribute('value', '');
                                                $pword->setAttribute('placeholder', 'New password');
                                                $pword->setAttribute('minlength', '8');
                                                $pword->setAttribute('maxlength', '20');
                                                $pword->setAttribute('pattern', '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$+=!@%]).{8,20}$');

                                                $code->setAttribute('class', 'form-control');
                                                $code->setAttribute('readonly','');
                                                $code->setAttribute('type', 'text');
                                                $code->setAttribute('name', 'fcode');
                                                $code->setAttribute('value', $c["ID"]);

                                                $access->setAttribute('class', 'form-control mb-2');
                                                $access->setAttribute('readonly','');
                                                $access->setAttribute('type', 'text');
                                                $access->setAttribute('name', 'last access');
                                                $access->setAttribute('value', $h["last_access"]);

                                                $piva->setAttribute('class', 'form-control mb-2');
                                                $piva->setAttribute('readonly','');
                                                $piva->setAttribute('type', 'text');
                                                $piva->setAttribute('name', 'p_iva');
                                                $piva->setAttribute('value', $c["p_iva"]);
                                                
                                                $modifica->setAttribute('class','btn btn-secondary');
                                                $modifica->setAttribute('type','submit');
                                                $modifica->appendChild($page->createTextNode('Modifica'));

                                                $annulla->setAttribute('class', 'btn btn-secondary');
                                                $annulla->setAttribute('onclick','annullaHandler(); return false;');
                                                $annulla->appendChild($page->createTextNode('Annulla'));

                                                $mdp->setAttribute('class', 'btn btn-secondary float-right');
                                                $mdp->setAttribute('onclick','supportedProductors(); return false;');
                                                
                                                $cardIcon = $page->createElement('i');
                                                $cardIcon->setAttribute('class', 'fas fa-map-marked-alt');
                                                $cardIcon->setAttribute('aria-hidden', 'true');
                                                $cardText = $page->createElement('span');
                                                $cardText->appendChild($page->createTextNode(' Produttori supportati'));
                                                $cardText->setAttribute('class', 'hide-sm');

                                                $mdp->appendChild($cardIcon);
                                                $mdp->appendChild($cardText);
                                                                                                
                                                $divname->appendChild($page->createTextNode('Nome: '));
                                                $divname->appendChild($name);
                                                $divsurname->appendChild($page->createTextNode('Indirizzo: '));
                                                $divsurname->appendChild($surname);
                                                $divemail->appendChild($page->createTextNode('Email: '));
                                                $divemail->appendChild($email);
                                                $divpword->appendChild($page->createTextNode('Password: '));
                                                $divpword->appendChild($pword);
                                                

                                                $divaccess->appendChild($page->createTextNode('Ultimo Accesso: '));
                                                $divaccess->appendChild($access);
                                                $divaccess->appendChild($page->createTextNode('Partita IVA: '));
                                                $divaccess->appendChild($piva);
                                                
                                                $form->appendChild($divname);
                                                $form->appendChild($divsurname);
                                                $form->appendChild($divemail);
                                                $form->appendChild($divpword);
                                                $form->appendChild($divcode);
                                                $form->appendChild($divaccess);
                                                $switchDiv = $page->createElement('div');
                                                $switchDiv->setAttribute('class', 'float-right mr-5 switch round '.($c["av"] == 0 ? 'Off' : 'On' ));
                                                $switchDiv->setAttribute('id', 'Pavailable');
                                                $avToggle = $page->createElement('div');
                                                $avToggle->setAttribute('class', 'toggle');
                                                $avLabel = $page->createElement('div');
                                                $avLabel->setAttribute('class', 'w-100 align-middle my-4');
                                                $avLabel->appendChild($page->createTextNode('Disponibilità: '));
                                                $switchDiv->appendChild($avToggle);
                                                $avLabel->appendChild($switchDiv);
                                                $form->appendChild($avLabel);
                                                $form->appendChild($modifica);
                                                $form->appendChild($annulla);
                                                $form->appendChild($mdp);
                                                
                                                
                                                $page->appendChild($form);
                                                
                                                $res = html_entity_decode($page->saveHTML());
                                                $result[OK] = true;
                                                $result[HTML] = $res;
                                            
                                            } else {
                                                $result[OK] = 807;
                                                $result[ERROR] = "Errore account non unico";
                                            }
                                        } else {
                                            $result[OK] = 808;
                                            $result[ERROR] = "Errore nell'esecuzione della query per il trasportatore";
                                        }   
                                    } else {
                                        $result[OK] = 809;
                                        $result[ERROR] = "Errore nella creazione della query per il cliente";                    
                                    }
                                }
                            }
                        } else {
                            $result[OK] = 810;
                            $result[ERROR] = "Account non trasportatore";
                        }
                    } else {
                        $result[OK] = 811;
                        $result[ERROR] = "Errore nell'esecuzione della query per il cliente";
                    }
                } else {
                    $result[OK] = 812;
                    $result[ERROR] = "Errore nella creazione della query per il cliente";
                } 
            } else {
                $result[OK] = 813;
                $result[ERROR] = "Errore account non unico";
            }
        }
    }
} else {
    $result[OK] = 814;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));
?>