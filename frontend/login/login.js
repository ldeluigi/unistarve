var counter = 0;
$(document).ready(function() {
  var alert = $(".banner").html();
  $.ajax({
    type: "GET",
    url: "/api/login.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        window.location.replace(".." + result.result.redirect);
      }
    },
    error: function(xhr, status, error) {}
  });
  $(".banner").html("");
  $("#form").submit(function() {
    var f = $("#form");
    var email = $("#InputEmail").val();
    var password = $("#InputPassword").val();
    $.ajax({
      type: "GET",
      url: "/api/login.php?email=" + email + "&pword=" + password,
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          window.location.replace(".." + result.result.redirect);
        } else if (result.ok === 57) {
          if (counter < 3) {
            counter++;
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text(
              "Errore: " + result.message + " (" + result.ok + ")"
            );
          } else {
            window.location.replace("../register/register.html");
          }
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
    return false;
  });
});
