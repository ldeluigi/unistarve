<?php
require("base.php");
function checkPword() {
    global $PMAX;
    return ( is_string($_GET["pword"]) and ( strlen($_GET["pword"]) <= $PMAX ) and ( strlen($_GET["pword"]) > 0 ) );
}
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 152;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $stmt = $conn->prepare("SELECT consumer, last_access, email, ID FROM ACCOUNTS WHERE ID=? AND consumer IS NOT NULL");
        if (!$stmt) {
            $result[OK] = 153;
            $result[ERROR] = "Errore durante la preparazione della query";
        } else {
            if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                if ($stmt->execute()) {
                    $account = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                    if (count($account) === 1) {
                        $h = $account[0];
                        if ($h["consumer"]) {
                            $go=true;
                            if (isset($_GET["update"])) {
                                $go=false;
                                if (isset($_GET["name"]) and isset($_GET["surname"]) and isset($_GET["email"]) and isset($_GET["pword"]) and
                                    is_string($_GET["name"]) and strlen($_GET["name"]) <= $NMAX and 
                                    is_string($_GET["surname"]) and strlen($_GET["surname"]) <= $CMAX and 
                                    is_string($_GET["email"]) and strlen($_GET["email"]) <= $EMAX and filter_var($_GET["email"], FILTER_VALIDATE_EMAIL)) {
                                    $stmt = $conn->prepare("UPDATE ACCOUNTS SET email=?".(checkPword() ? ", pword=?" : "")." WHERE ID=?");
                                    $stmt2 = $conn->prepare("UPDATE CONSUMERS SET name=?, surname=? WHERE CF=?");
                                    if (!$stmt or !$stmt2) {
                                        $result[OK] = 159;
                                        $result[ERROR] = "Errore durante la preparazione della query di modifica consumatore";
                                    } else {
                                        if ((checkPword() ? $stmt->bind_param("ssi", $_GET["email"], $_GET["pword"], $h["ID"]) : $stmt->bind_param("si", $_GET["email"], $h["ID"]) ) 
                                                        and $stmt2->bind_param("sss", $_GET["name"], $_GET["surname"], $h["consumer"])) {
                                            if ($stmt->execute() and $stmt2->execute() and ($stmt->affected_rows > 0 or $stmt2->affected_rows > 0)) {
                                                $go=true;
                                            } else {
                                                $result[OK] = 161;
                                                $result[ERROR] = "Errore durante la esecuzione della query di modifica consumatore ". $stmt->error. " ".$stmt2->error."Lines changed: ".$stmt->affected_rows." ".$stmt2->affected_rows;
                                            }
                                        } else {
                                            $result[OK] = 160;
                                            $result[ERROR] = "Errore durante la preparazione della query di modifica consumatore";
                                        }
                                    }
                                } else {
                                    $result[OK] = 170;
                                    $result[ERROR] = "Errore input";
                                }
                            }
                            if ($go) {
                                $stmt = $conn->prepare("SELECT name, surname, CF, cart FROM CONSUMERS WHERE CF=?");
                                if (!$stmt) {
                                    $result[OK] = 158;
                                    $result[ERROR] = "Errore durante la preparazione della query consumatore";
                                } else {
                                    if ( $stmt->bind_param("s", $h["consumer"])) {
                                        if ($stmt->execute()) {
                                            $consumer = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                                            if (count($consumer) === 1) {
                                                $c = $consumer[0];
                                                class br extends DOMElement {
                                                    function __construct() {
                                                        parent::__construct('br');
                                                    }
                                                }
                                                
                                                $page = new DOMDocument();
                                                $page->normalizeDocument();
                                                $page->formatOutput = true;

                                                $form = $page->createElement('form');
                                                $divname = $page->createElement('div');
                                                $divsurname = $page->createElement('div');
                                                $divemail = $page->createElement('div');
                                                $divpword = $page->createElement('div');
                                                $divcode = $page->createElement('div');
                                                $divaccess = $page->createElement('div');
                                                $name = $page->createElement('input');
                                                $surname = $page->createElement('input');
                                                $email = $page->createElement('input');
                                                $pword = $page->createElement('input');
                                                $code = $page->createElement('input');
                                                $access = $page->createElement('input');
                                                
                                                $modifica = $page->createElement('button');
                                                $annulla = $page->createElement('button');
                                                $mdp = $page->createElement('button');
                                                
                                                $form->setAttribute('class', 'border border-secondary rounded p-3');
                                                $form->setAttribute('id','form');
                                                $form->setAttribute('onsubmit', 'submitHandler(); return false;');

                                                $divname->setAttribute('class', 'form-group');
                                                $divsurname->setAttribute('class', 'form-group');
                                                $divemail->setAttribute('class', 'form-group');
                                                $divpword->setAttribute('class', 'form-group');
                                                
                                                $name->setAttribute('class', 'form-control');
                                                $name->setAttribute('type', 'text');
                                                $name->setAttribute('id', 'name');
                                                $name->setAttribute('name', 'name');
                                                $name->setAttribute('value', $c["name"]);
                                                
                                                $surname->setAttribute('class', 'form-control');
                                                $surname->setAttribute('type', 'text');
                                                $surname->setAttribute('id', 'surname');
                                                $surname->setAttribute('name', 'surname');
                                                $surname->setAttribute('value', $c["surname"]);

                                                $email->setAttribute('class', 'form-control');
                                                $email->setAttribute('type', 'email');
                                                $email->setAttribute('id', 'email');
                                                $email->setAttribute('name', 'email');
                                                $email->setAttribute('value', (isset($_GET["email"]) ? $_GET["email"] : $h["email"]));
                                                
                                                $pword->setAttribute('class', 'form-control');
                                                $pword->setAttribute('type', 'password');
                                                $pword->setAttribute('id', 'pword');
                                                $pword->setAttribute('name', 'pword');
                                                $pword->setAttribute('value', '');
                                                $pword->setAttribute('placeholder', 'New password');
                                                $pword->setAttribute('minlength', '8');
                                                $pword->setAttribute('maxlength', '20');
                                                $pword->setAttribute('pattern', '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$+=!@%]).{8,20}$');

                                                $code->setAttribute('class', 'form-control');
                                                $code->setAttribute('readonly','');
                                                $code->setAttribute('type', 'text');
                                                $code->setAttribute('name', 'fcode');
                                                $code->setAttribute('value', $c["CF"]);

                                                $access->setAttribute('class', 'form-control mb-2');
                                                $access->setAttribute('readonly','');
                                                $access->setAttribute('type', 'text');
                                                $access->setAttribute('name', 'last access');
                                                $access->setAttribute('value', $h["last_access"]);
                                                
                                                $modifica->setAttribute('class','btn btn-secondary');
                                                $modifica->setAttribute('type','submit');
                                                $modifica->appendChild($page->createTextNode('Modifica'));

                                                $annulla->setAttribute('class', 'btn btn-secondary');
                                                $annulla->setAttribute('onclick','annullaHandler(); return false;');
                                                $annulla->appendChild($page->createTextNode('Annulla'));
                                                
                                                $mdp->setAttribute('class', 'btn btn-secondary float-right');
                                                $mdp->setAttribute('onclick','paymentMethods(); return false;');

                                                $cardIcon = $page->createElement('i');
                                                $cardIcon->setAttribute('class', 'fas fa-credit-card');
                                                $cardIcon->setAttribute('aria-hidden', 'true');
                                                $cardText = $page->createElement('span');
                                                $cardText->appendChild($page->createTextNode(' Metodi di Pagamento'));
                                                $cardText->setAttribute('class', 'hide-sm');

                                                $mdp->appendChild($cardIcon);
                                                $mdp->appendChild($cardText);
                                                
                                                $divname->appendChild($page->createTextNode('Nome: '));
                                                $divname->appendChild($name);
                                                $divsurname->appendChild($page->createTextNode('Cognome: '));
                                                $divsurname->appendChild($surname);
                                                $divemail->appendChild($page->createTextNode('Email: '));
                                                $divemail->appendChild($email);
                                                $divpword->appendChild($page->createTextNode('Password: '));
                                                $divpword->appendChild($pword);
                                                
                                                $divcode->appendChild($page->createTextNode('Codice Fiscale: '));
                                                $divcode->appendChild($code);

                                                $divaccess->appendChild($page->createTextNode('Ultimo Accesso: '));
                                                $divaccess->appendChild($access);
                                                
                                                $form->appendChild($divname);
                                                $form->appendChild($divsurname);
                                                $form->appendChild($divemail);
                                                $form->appendChild($divpword);
                                                $form->appendChild($divcode);
                                                $form->appendChild($divaccess);
                                                $form->appendChild($modifica);
                                                $form->appendChild($annulla);
                                                
                                                $form->appendChild($mdp);
                                                
                                                $page->appendChild($form);
                                                
                                                $res = html_entity_decode($page->saveHTML());
                                                $result[OK] = true;
                                                $result[HTML] = $res;
                                            
                                            } else {
                                                $result[OK] = 159;
                                                $result[ERROR] = "Errore account non unico";
                                            }
                                        } else {
                                            $result[OK] = 160;
                                            $result[ERROR] = "Errore nell'esecuzione della query per il consumatore";
                                        }   
                                    } else {
                                        $result[OK] = 161;
                                        $result[ERROR] = "Errore nella creazione della query per il cliente";                    
                                    }
                                }
                            }
                        } else {
                            $result[OK] = 155;
                            $result[ERROR] = "Account non consumatore";
                        }
                    } else {
                        $result[OK] = 156;
                        $result[ERROR] = "Errore nell'esecuzione della query per il cliente";
                    }
                } else {
                    $result[OK] = 157;
                    $result[ERROR] = "Errore nella creazione della query per il cliente";
                } 
            } else {
                $result[OK] = 154;
                $result[ERROR] = "Errore account non unico";
            }
        }
    }
} else {
    $result[OK] = 151;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));
?>