function supportedProductors() {
  $.ajax({
    type: "GET",
    url: "/api/areas.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#personal-container").html(result.html);
        mediaQuery();
        $(".switch").click(function() {
          $(this)
            .toggleClass("On")
            .toggleClass("Off");
          changeSupported(
            $(this).attr("data-productorID"),
            $(this).hasClass("On") ? "on" : "off"
          );
        });
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function changeSupported(id, state) {
  $.ajax({
    type: "GET",
    url: "/api/areas.php?" + state + "&prodid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        mediaQuery();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function backToPerson() {
  $.ajax({
    type: "get",
    url: "/api/transporter.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#personal i").finish();
        $("#personal-off").hide();
        $("#personal-on").fadeIn();
        $("#personal-container").html(result.html);
        personShown = true;
        $(".switch").click(function() {
          $(this)
            .toggleClass("On")
            .toggleClass("Off");
        });
        mediaQuery();
      } else if (result.ok && result.ok === 151) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}
