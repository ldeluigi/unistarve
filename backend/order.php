<?php
require("base.php");
$result = [];
session_start();
refresh_session();
if (isset($_SESSION[ACCOUNT]))  {
    $conn = connectDB();
    if ($conn->connect_error) {
        $result[OK] = 651;
        $result[ERROR] = "Connessione al DB fallita";
    } else {
        $go = true;
        if (isset($_GET["delay"]) and isset($_GET["orderid"]) and is_numeric($_GET["orderid"])) {
            $go = false;
            $stmt = $conn->prepare("UPDATE ORDINATIONS AS O 
                                        JOIN ORDERS AS ORD ON O.ID=ORD.ordination
                                        LEFT JOIN TRANSPORTERS AS T ON ORD.transportation=T.ID
                                        LEFT JOIN PRODUCTORS AS P ON ORD.destinated_to=P.ID 
                                        JOIN ACCOUNTS AS A ON A.productor=P.ID OR A.transporter=T.ID
                                        SET O.for_datetime=O.for_datetime + INTERVAL 15 MINUTE
                                        WHERE ORD.ID=? AND A.ID=? AND O.delivery_datetime IS NULL");
            if ($stmt === false) {
                $result[OK] = 686;
                $result[ERROR] = "Errore durante la preparazione della query.";
            } else {
                if ($stmt -> bind_param ("ii", $_GET["orderid"], $_SESSION[ACCOUNT])) {
                    if ($stmt->execute() and $stmt->affected_rows > 0) {
                        sendMessage("L'ordine ".$_GET["orderid"]." ha subito un ritardo.", $_GET["orderid"], $conn);
                        $go = true;
                    } else {
                        $result[OK] = 687;
                        $result[ERROR] = "Errore durante l'esecuzione della query (".$stmt->error.")";
                    }
                } else {
                    $result[OK] = 688;
                    $result[ERROR] = "Errore durante la creazione della query.";
                }
            }
        } else if (isset($_GET["order"])) {
            $go = false;
            if (!(isset($_GET["location"]) and is_numeric($_GET["location"]) and isset($_GET["pay"]) and is_numeric($_GET["pay"]))) {
                $result[OK] = 651;
                $result[ERROR] = "I parametri non sono numerici o sono mancanti.";
            } else {
                $stmt = $conn -> prepare("SELECT O.productor AS prodID, O.ID AS ID, O.cost AS cost, OC.quantity AS quantity, C.ID AS cart FROM ORDINABLES AS O
                                            JOIN ON_CART AS OC ON O.ID=OC.product
                                            JOIN CART AS C ON C.ID=OC.cart
                                            JOIN CONSUMERS AS CO ON CO.cart=C.ID
                                            JOIN ACCOUNTS AS A ON A.consumer=CO.CF
                                            WHERE A.ID=?");
                if ($stmt === false) {
                    $result[OK] = 652;
                    $result[ERROR] = "Errore durante la preparazione della query.";
                } else {
                    if ($stmt -> bind_param ("i", $_SESSION[ACCOUNT])) {
                        if ($stmt->execute() and ($stmtr = $stmt->get_result())->num_rows > 0) {
                            if (isset($_GET["date"]) and isset($_GET["time"])) {
                                $datetime = $_GET["date"].' '.$_GET["time"];
                                $convertedData = strtotime($datetime);
                                if ($convertedData !== false) {
                                    $datetime = date("Y-m-d H:i:s", $convertedData);
                                    if ($datetime !== false) {
                                        $timeRequest = $datetime;
                                    } else {
                                        $timeRequest = null;
                                    }
                                } else {
                                    $timeRequest = null;
                                }
                            } else {
                                $timeRequest = null;
                            }
                            $ordination = $conn->prepare("INSERT INTO ORDINATIONS (payed, delivery_datetime, ordination_datetime, client, location, pay_with, for_datetime)
                                                        SELECT 1, NULL, NOW(), C.CF, ?, PM.ID, ?
                                                        FROM CONSUMERS AS C
                                                        JOIN ACCOUNTS AS A ON A.consumer=C.CF
                                                        JOIN PAYMENT_METHODS AS PM ON PM.owner=C.CF
                                                        JOIN PAYMENT_DATA AS PD ON PD.ID=PM.payment_data
                                                        WHERE PM.ID=? AND A.ID=?");
                            if ($ordination === false or !$ordination->bind_param("isii", $_GET["location"], $timeRequest, $_GET["pay"], $_SESSION[ACCOUNT])) {
                                $result[OK] = 655;
                                $result[ERROR] = "Errore durante la preparazione della query.";
                            } else if ($ordination->execute() and $ordination->affected_rows > 0) {
                                $ordinationID = $ordination->insert_id;
                                $products = $stmtr->fetch_all(MYSQLI_ASSOC);
                                $producers = array_unique(array_column($products, 'prodID'));
                                $end = count($producers);
                                $i = 0;
                                $totalCost = 0;
                                foreach ($producers as $producerID) {
                                    $stmtOrder = $conn->prepare("INSERT INTO ORDERS (delivered, ordination, destinated_to, transportation)
                                                                SELECT NULL, ?, ?, C.transporter FROM COVERS AS C
                                                                JOIN TRANSPORTERS AS T ON T.ID=C.transporter
                                                                WHERE C.area=? AND T.unsubscription IS NULL AND T.active=1 ORDER BY RAND() LIMIT 1");
                                    if ($stmtOrder === false or !$stmtOrder->bind_param("iii", $ordinationID, $producerID, $producerID)) {
                                        $result[OK] = 656;
                                        $result[ERROR] = "Errore durante la preparazione della query.";
                                        break;
                                    } else if ($stmtOrder->execute() and $stmtOrder->affected_rows > 0) {
                                        $orderID = $stmtOrder->insert_id;
                                        $prodCheck = $conn->prepare("SELECT * FROM PRODUCTORS WHERE ID=? AND UNSUBSCRIPTION IS NULL");
                                        if ($prodCheck === false or !$prodCheck->bind_param("i", $producerID)) {
                                            $result[OK] = 659;
                                            $result[ERROR] = "Preparazione check fallita al passso $i di $end";
                                            break;
                                        } else if ($prodCheck->execute() and $prodCheckR = $prodCheck->get_result()) {
                                            $notify = "INSERT INTO MESSAGES (time, text, Rec_ID) VALUES (NOW(), ?, ?)";
                                            $getProductor = $conn->prepare("SELECT A.ID AS A_ID, P.name AS name FROM PRODUCTORS AS P
                                                                                JOIN ACCOUNTS AS A ON A.productor=P.ID
                                                                                WHERE P.ID=?");
                                            if ($getProductor === false or !$getProductor->bind_param("i", $producerID)) {
                                                $result[OK] = 671;
                                                $result[ERROR] = "Preparazione check fallita al passo $i di $end. ".$conn->error;
                                                break;
                                            } else if (!$getProductor->execute()) {
                                                $result[OK] = 672;
                                                $result[ERROR] = "Esecuzione check fallita al passo $i di $end ".$getProductor->error;
                                                break;
                                            } else {
                                                $getProductorR = $getProductor->get_result()->fetch_assoc();
                                                if (!$notify = $conn->prepare($notify.", (NOW(), ?, (SELECT A.ID FROM ACCOUNTS AS A 
                                                                                        JOIN TRANSPORTERS AS T ON T.ID=A.transporter 
                                                                                        JOIN ORDERS AS O ON O.transportation=T.ID 
                                                                                        WHERE O.ID=?))")) {
                                                    $result[OK] = 678;
                                                    $result[ERROR] = "Preparazione notifica fallita al passo $i di $end ".$conn->error;
                                                    break;
                                                } else {
                                                    if ($prodCheckR->num_rows > 0) {
                                                        $prodAccID = $getProductorR["A_ID"];
                                                        $text = "Un nuovo ordine è stato elaborato e i prodotti saranno aggiunti in lista appena il pagamento è avvenuto.";
                                                        $text2 = "Un nuovo ordine è stato elaborato e i le informazioni sono state inserite in lista ordini.";
                                                        if (!$notify->bind_param("sisi", $text, $prodAccID, $text2, $orderID)) {
                                                            $result[OK] = 675;
                                                            $result[ERROR] = "Preparazione notifica fallita al passo $i di $end ";
                                                            break;
                                                        } else if (!$notify->execute()) {
                                                            $result[OK] = 676;
                                                            $result[ERROR] = "Esecuzione notifica fallita al passo $i di $end ".$notify->error;
                                                            break;
                                                        } else {
                                                            $j = 0;
                                                            $jend = count($products);
                                                            foreach ($products as $item) {
                                                                if ($item["prodID"] == $producerID) {
                                                                    $comprise = $conn->prepare("INSERT INTO COMPRISE (product, orderID, quantity, price) VALUES (?, ?, ?, ?)");
                                                                    if ($comprise === false or !$comprise->bind_param("iiid", $item["ID"], $orderID, $item["quantity"], $item["cost"])) {
                                                                        $result[OK] = 676;
                                                                        $result[ERROR] = "Preparazione oggetto fallita al passo $j di $jend del passo $i di $end ";
                                                                        break;
                                                                    } else if (!$comprise->execute()) {
                                                                        $result[OK] = 677;
                                                                        $result[ERROR] = "Esecuzione acquisto oggetto fallita al passo $j di $jend del passo $i di $end ".$comprise->error;
                                                                        break;
                                                                    } else {
                                                                        $totalCost += $item["quantity"] * $item["cost"];
                                                                        $deleteFromCart = $conn->prepare("DELETE FROM on_cart WHERE product=? AND cart=?");
                                                                        if ($deleteFromCart === false or !$deleteFromCart->bind_param("ii", $item["ID"], $item["cart"]) or !$deleteFromCart->execute()) {
                                                                            $result[OK] = 681;
                                                                            $result[ERROR] = "Eliminazione oggetto carrello dopo acquisto fallita al passo $j di $jend del passo $i di $end ".$deleteFromCart->error." ".$conn->error;
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                $j++;
                                                            }
                                                            if ($j < $jend) {
                                                                break;
                                                            }
                                                        }
                                                    } else {
                                                        $removedPName = $getProductorR["name"];
                                                        $text = "Il produttore $removedPName non è più disponibile, pertanto i prodotti ordinati per $removedPName sono stati rimossi dall'ordine (e dal conto).";
                                                        if (!$notify->bind_param("si", $text, $_SESSION[ACCOUNT])) {
                                                            $result[OK] = 673;
                                                            $result[ERROR] = "Preparazione notifica fallita al passo $i di $end ";
                                                            break;
                                                        } else if (!$notify->execute()) {
                                                            $result[OK] = 674;
                                                            $result[ERROR] = "Esecuzione notifica fallita al passo $i di $end ".$notify->error;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $result[OK] = 670;
                                            $result[ERROR] = "Esecuzione check fallita al passo $i di $end ".$prodCheck->error;
                                            break;
                                        }
                                        $i++;
                                    } else {
                                        $result[OK] = 658;
                                        $result[ERROR] = "Esecuzione fallita al passso $i di $end (".$stmtOrder->error."). Nessun trasportatore disponibile per il produttore $producerID. Controlla il carrello per eliminare questi prodotti.";
                                        break;
                                    }
                                }
                                $notify = $conn->prepare("INSERT INTO MESSAGES (time, text, Rec_ID) VALUES (NOW(), ?, ?)");
                                $text = "Ordine effettuato. Spesa totale: $totalCost €";
                                if ($notify === false or !$notify->bind_param("si", $text, $_SESSION[ACCOUNT])) {
                                    if ($i === $end) {
                                        $result[OK] = 679;
                                        $result[ERROR] = "Preparazione notifica fallita al passo $i di $end ";
                                    }
                                } else if (!$notify->execute()) {
                                    if ($i === $end) {
                                        $result[OK] = 680;
                                        $result[ERROR] = "Esecuzione notifica fallita al passo $i di $end ".$notify->error;
                                    }
                                } else {
                                    if ($i === $end) {
                                        $go = true;
                                    }
                                }
                            } else {
                                $result[OK] = 657;
                                $result[ERROR] = "Errore durante l'esecuzione della query ".$ordination->error;
                            }
                        } else {
                            $result[OK] = 653;
                            $result[ERROR] = "Errore durante l'esecuzione della query o carrello vuoto. [".$stmt->num_rows."] ".$stmt->error;
                        }
                    } else {
                        $result[OK] = 654;
                        $result[ERROR] = "Errore durante la preparazione della query.";
                    }
                }
            }
        }
        if ($go) {
            $history = isset($_GET["history"]);
             $stmt = $conn->prepare("SELECT OO.ID AS OrderID, O.name AS name, CO.quantity AS q, CO.price AS p, L.address AS location,
                                     ORD.ordination_datetime as time, ORD.payed AS payed, ORD.for_datetime AS f, OO.pickedup AS pick, O.ID AS ID,
                                     P.name AS Pname, P.address AS Paddress, T.name AS Tname, IF(A.consumer IS NULL, 0, 1) AS isConsumer, OO.delivered AS delivered
                                     FROM COMPRISE AS CO
                                     JOIN ORDINABLES AS O ON CO.product=O.ID
                                     JOIN ORDERS AS OO ON CO.orderID=OO.ID
                                     JOIN ORDINATIONS AS ORD ON OO.ordination=ORD.ID
                                     JOIN CONSUMERS AS C ON ORD.client=C.CF
                                     JOIN PRODUCTORS AS P ON P.ID=OO.destinated_to
                                     JOIN TRANSPORTERS AS T ON T.ID=OO.transportation
                                     JOIN ACCOUNTS AS A ON A.productor=P.ID OR A.consumer=C.CF OR A.transporter=T.ID
                                     JOIN LOCATIONS AS L ON L.ID=ORD.location
                                     WHERE A.ID=? ".(!$history ? "AND OO.delivered IS NULL " : "")."AND (".(!$history ? "OO.pickedup IS NULL OR " : "A.productor IS NOT NULL OR ")."A.transporter IS NOT NULL OR A.consumer IS NOT NULL)
                                     ORDER BY time DESC, OrderID");
            if (!$stmt) {
                $result[OK] = 682;
                $result[ERROR] = "Errore durante la preparazione della query";
            } else {
                if ($stmt->bind_param("i", $_SESSION[ACCOUNT])) {
                    if ($stmt->execute()) {
                        $cart = $stmt->get_result();
                        if ($cart->num_rows === 0) {
                            $page = new DOMDocument();
                            $page->normalizeDocument();
                            $page->formatOutput = true;
                            $p = $page->createElement('p');
                            $p->setAttribute('class', 'no-messages font-italic text-center lead w-50 mx-auto border rounded');
                            $p->appendChild($page->createTextNode("Nessun ordine presente"));
                            $confirm = $page->createElement('button');
                            $confirm->setAttribute('class', 'mt-3 text-right btn btn-secondary btn-outline-light');
                            $plus = $page->createElement('i');
                            $plus->setAttribute('class', 'fas fa-history');
                            $confirm->setAttribute('onclick', 'orderHistory();');
                            $confirm->appendChild($plus);
                            $confirm->appendChild($page->createTextNode(' Storico ordini'));
                            $btndiv = $page->createElement('div');
                            $btndiv->setAttribute('class', 'w-100 d-flex justify-content-center');
                            $btndiv->appendChild($confirm);
                            $page->appendChild($p);
                            $page->appendChild($btndiv);
                            $res = html_entity_decode($page->saveHTML());
                        } else {
                            $res = generate($cart, PHP_INT_MAX, 1, 0, $history);
                        }
                        $result[OK] = true;
                        $result[HTML] = $res;
                    } else {
                        $result[OK] = 683;
                        $result[ERROR] = "Errore nell'esecuzione della query: ".$stmt->error;
                    } 
                } else {
                    $result[OK] = 684;
                    $result[ERROR] = "Errore nella creazione della query per i messaggi";
                }
            }
        }
    }
} else {
    $result[OK] = 685;
    $result[ERROR] = "Sessione scaduta";
}  
header('Content-Type: application/json');
echo(json_encode($result));

function sendMessage(string $text, int $orderid, mysqli $conn) {
    $stmt = $conn->prepare("INSERT INTO MESSAGES (time, text, Rec_ID) SELECT DISTINCT NOW(), ?, A.ID FROM ACCOUNTS AS A LEFT JOIN TRANSPORTERS AS T ON A.transporter=T.ID
                            LEFT JOIN CONSUMERS AS C ON C.CF=A.consumer
                            LEFT JOIN ORDINATIONS AS O ON O.client=C.CF
                            JOIN ORDERS AS ORD ON ORD.ordination=O.ID OR ORD.transportation=T.ID
                            WHERE (A.transporter IS NOT NULL OR A.consumer IS NOT NULL) AND ORD.ID=?");
    return $stmt !== false && $stmt->bind_param("si", $text, $orderid) && $stmt->execute();
}

function generate($res, $RESULT_NUM, $resultMultiplier = 1, $pageIndex = 0, $history = false) {
    $page = new DOMDocument();
    $page->normalizeDocument();
    $page->formatOutput = true;
    $listattr = 'orders list-unstyled border rounded border-dark mb-1 p-1';
    $list = $page->createTextNode(""); 
    $moreResults = $res->num_rows > ($pageIndex + 1) * $RESULT_NUM * $resultMultiplier;
    if ($pageIndex > 0 and $RESULT_NUM * $resultMultiplier > 0) {
        $res->data_seek($pageIndex * $RESULT_NUM * $resultMultiplier);
    }
    $total = 0;
    $lastID = NULL;
    for ($i = 0; $i < $RESULT_NUM * $resultMultiplier and $elem = $res->fetch_assoc(); $i++) {
        $delayable = $elem["isConsumer"] == 0;
        if ( $elem["OrderID"] != $lastID ) {
            if ($lastID != NULL) {
                if (!$history) {
                    if ($delayable) {
                        $confirm = $page->createElement('button');
                        $confirm->setAttribute('class', 'mt-1 text-center btn btn-secondary btn-outline-light');
                        $plus = $page->createElement('i');
                        $plus->setAttribute('class', 'fas fa-check');
                        $confirm->setAttribute('onclick', 'orderCompleted('.$lastID.');');
                        $confirm->setAttribute('title', 'Conferma l\'ordine '.$lastID.' come finito di processare');
                        $confirm->appendChild($plus);
                        $btndiv = $page->createElement('div');
                        $btndiv->setAttribute('class', 'w-100 d-flex justify-content-center');
                        $delay = $page->createElement('button');
                        $delay->setAttribute('class', 'mt-1 text-center btn btn-secondary btn-outline-light');
                        $clock = $page->createElement('i');
                        $clock->setAttribute('class', 'fas fa-stopwatch');
                        $delay->setAttribute('onclick', 'delayOrder('.$lastID.');');
                        $delay->setAttribute('title', 'Ritarda la consegna dell\'ordine '.$lastID);
                        $delay->appendChild($clock);
                        $btndiv->appendChild($delay);
                        $btndiv->appendChild($confirm);
                        $list->appendChild($btndiv);
                    }
                }
                $page->appendChild($list);
            }
            $orderTitle = $page->createElement('h3');
            $orderTitle->setAttribute('class', 'w-50 text-center mx-auto mt-1');
            $orderTitle->appendChild($page->createTextNode('ID ordine: ' .$elem["OrderID"]));
            $list = $page->createElement('ul');
            $list->setAttribute('class', $listattr." orderID-".$elem["OrderID"]);
            $list->appendChild($orderTitle);
            $infoDivC = $page->createElement('div');
            $infoDivC->setAttribute('class', 'container w-100');
            $infoDiv = $page->createElement('div');
            $infoDiv->setAttribute('class', 'row w-100 justify-content-around');
            $prodInfoDiv = $page->createElement('div');
            $prodInfoDiv->setAttribute('class', 'col mx-2');
            $prodName = $page->createElement('h5');
            $prodName->setAttribute('class', 'row mt-1 mb-0');
            $prodName->appendChild($page->createTextNode("Nome fornitore: "));
            $prodInfoDiv->appendChild($prodName);
            $prodName = $page->createElement('span');
            $prodName->setAttribute('class', 'row mb-1');
            $prodName->appendChild($page->createTextNode($elem["Pname"]));
            $prodInfoDiv->appendChild($prodName);
            $prodAdd = $page->createElement('h5');
            $prodAdd->setAttribute('class', 'row mt-1 mb-0');
            $prodAdd->appendChild($page->createTextNode("Indirizzo di partenza: "));
            $prodInfoDiv->appendChild($prodAdd);
            $prodAdd = $page->createElement('span');
            $prodAdd->setAttribute('class', 'row mb-1');
            $prodAdd->appendChild($page->createTextNode($elem["Paddress"]));
            $prodInfoDiv->appendChild($prodAdd);
            $transpInfoDiv = $page->createElement('div');
            $transpInfoDiv->setAttribute('class', 'col mx-2');
            $transpName = $page->createElement('h5');
            $transpName->setAttribute('class', 'row text-right justify-content-end');
            $transpName->appendChild($page->createTextNode("Nome ente di trasporto: "));
            $transpInfoDiv->appendChild($transpName);
            $transpName = $page->createElement('span');
            $transpName->setAttribute('class', 'row text-right justify-content-end');
            $transpName->appendChild($page->createTextNode($elem["Tname"]));
            $transpInfoDiv->appendChild($transpName);
            if ((isset($elem["f"]) and $elem["f"] != null) or $history) {
                $transpTime = $page->createElement('h5');
                $transpTime->setAttribute('class', 'row text-right justify-content-end');
                if ($history) {
                    $transpTime->appendChild($page->createTextNode("Consegna effettuata:"));
                    $transpInfoDiv->appendChild($transpTime);
                    $transpTime = $page->createElement('span');
                    $transpTime->setAttribute('class', 'row text-right justify-content-end');
                    $transpTime->appendChild($page->createTextNode($elem["delivered"] ? $elem["delivered"] : "n/a"));
                } else {
                    $transpTime->appendChild($page->createTextNode("Consegna prevista:"));
                    $transpInfoDiv->appendChild($transpTime);
                    $transpTime = $page->createElement('span');
                    $transpTime->setAttribute('class', 'row text-right justify-content-end');
                    $transpTime->appendChild($page->createTextNode($elem["f"]));
                }
                $transpInfoDiv->appendChild($transpTime);
            }
            $infoDiv->appendChild($prodInfoDiv);
            $infoDiv->appendChild($transpInfoDiv);
            $infoDivC->appendChild($infoDiv);
            $list->appendChild($infoDivC);
            $lastID = $elem["OrderID"];
        }
        
        $onProductClick = 'if (typeof showProduct == "function") {
                            showProduct('.$elem["ID"].'); 
                           } else if (typeof moveMap == "function") {
                            moveMap("'.$elem["location"].'");
                           } else {
                               window.location.href="/product/product.html?id=' . $elem["ID"].'";
                           }';

        $el = $page->createElement('li');
        $el->setAttribute('class', 'product media border rounded w-100 prod-'.$elem["ID"]);

        $h = $page->createElement('div');
        $h->setAttribute('class', 'm-1 media-body w-50');
        $h->setAttribute('onclick', $onProductClick);
        
        $ee = $page->createElement('h5');
        $ee->setAttribute('class', 'mt-0 mb-1');


        $divP = $page->createElement('div');
        $divP->setAttribute('class', 'row w-100');

        $id = $page->createElement('p');
        $id->setAttribute('class', 'col m-0 p-0');
        $id->appendChild($page->createTextNode($elem["q"]." x ".$elem["p"]."€ = ".(number_format((float) $elem["q"] * $elem["p"], 2, ".", "") ."€")));

        $payedDiv = $page->createElement('div');
        $payedDiv->setAttribute('class', 'row w-100');

        $payedBox =  $page->createElement('input');
        $payedBox->setAttribute('class', 'col p-0 mt-2');
        $payedBox->setAttribute('type', 'checkbox');
        $payedBox->setAttribute('value', '');
        $payedBox->setAttribute('disabled', '');
        $payedBox->setAttribute('checked', '');
        
        $payed = $page->createElement('p');
        $payed->setAttribute('class', 'col m-0 p-0');
        $payed->appendChild($page->createTextNode("Pagato: "));

        $pickedDiv = $page->createElement('div');
        $pickedDiv->setAttribute('class', 'row w-100');
        
        $picked = $page->createElement('p');
        $picked->setAttribute('class', 'col m-0 p-0');
        $picked->appendChild($page->createTextNode("In viaggio dal: ".($elem["pick"] ? $elem["pick"] : "n/a")));

        $rdiv = $page->createElement('div');
        $rdiv->setAttribute('class', 'rdiv container w-50 my-auto mr-0 ml-2');
        

        $descrip = $page->createElement('div');
        $descrip->setAttribute('class', 'description mb-0 text-truncate mw-100');
        $pd = $page->createElement('p');
        $pd->appendChild($page->createTextNode($elem["time"]."  "));
        $pd->appendChild($page->createTextNode($elem["location"]));
        $pd->setAttribute('class', 'text-truncate');
        $descrip->appendChild($pd);

        $ee->appendChild($page->createTextNode($elem["q"]."x ".$elem["name"]));
        $h->appendChild($ee);
        $h->appendChild($descrip);

        $el->appendChild($h);

        $rdiv->appendChild($divP);
        $divP->appendChild($id);
        $payedDiv->appendChild($payed);
        $payedDiv->appendChild($payedBox);
        $pickedDiv->appendChild($picked);
        $rdiv->appendChild($payedDiv);
        $rdiv->appendChild($pickedDiv);
        $el->appendChild($rdiv);

        $list->appendChild($el);
        $total += $elem["q"] * $elem["p"];
    }
    if ($i === 0) {
        $p = $page->createElement('p');
        $p->setAttribute('class', 'no-messages font-italic text-center lead');
        $p->appendChild($page->createTextNode("Nessun ordine"));
        $page->appendChild($p);
        $confirm = $page->createElement('button');
        $confirm->setAttribute('class', 'mt-3 text-right btn btn-secondary btn-outline-light');
        $plus = $page->createElement('i');
        $plus->setAttribute('class', 'fas fa-history');
        $confirm->setAttribute('onclick', 'orderHistory();');
        $confirm->appendChild($plus);
        $confirm->appendChild($page->createTextNode(' Storico ordini'));
        $btndiv = $page->createElement('div');
        $btndiv->setAttribute('class', 'w-100 d-flex justify-content-center');
        $btndiv->appendChild($confirm);
        $page->appendChild($btndiv);
    } else {
        if (!$history) {
            if ($delayable) {
                $confirm = $page->createElement('button');
                $confirm->setAttribute('class', 'mt-1 text-center btn btn-secondary btn-outline-light');
                $plus = $page->createElement('i');
                $plus->setAttribute('class', 'fas fa-check');
                $confirm->setAttribute('onclick', 'orderCompleted('.$lastID.');');
                $confirm->setAttribute('title', 'Conferma l\'ordine '.$lastID.' come finito di processare');
                $confirm->appendChild($plus);
                $btndiv = $page->createElement('div');
                $btndiv->setAttribute('class', 'w-100 d-flex justify-content-center');
                $delay = $page->createElement('button');
                $delay->setAttribute('class', 'mt-1 text-center btn btn-secondary btn-outline-light');
                $clock = $page->createElement('i');
                $clock->setAttribute('class', 'fas fa-stopwatch');
                $delay->setAttribute('onclick', 'delayOrder('.$lastID.');');
                $delay->setAttribute('title', 'Ritarda la consegna dell\'ordine '.$lastID);
                $delay->appendChild($clock);
                $btndiv->appendChild($delay);
                $btndiv->appendChild($confirm);
                $list->appendChild($btndiv);
            }
        }
        $page->appendChild($list);
        if ($moreResults) {
            $mDiv = $page->createElement('div');
            $mDiv->setAttribute('class', 'mx-auto w-50 my-2 text-center');
            $more = $page->createElement('button');
            $more->setAttribute('class', 'btn btn-outline-light mx-auto');
            $more->setAttribute('type', 'button');
            $more->setAttribute('onclick', 'moreItems('.($pageIndex + 1).','.($resultMultiplier + 1).');');
            $more->appendChild($page->createTextNode('Altri Risultati'));
            $mDiv->appendChild($more);
            $page->appendChild($mDiv);
        }
        if (!$history) {
            $confirm = $page->createElement('button');
            $confirm->setAttribute('class', 'mt-3 text-right btn btn-secondary btn-outline-light');
            $plus = $page->createElement('i');
            $plus->setAttribute('class', 'fas fa-history');
            $confirm->setAttribute('onclick', 'orderHistory();');
            $confirm->appendChild($plus);
            $confirm->appendChild($page->createTextNode(' Storico ordini'));
            $btndiv = $page->createElement('div');
            $btndiv->setAttribute('class', 'w-100 d-flex justify-content-center');
            $btndiv->appendChild($confirm);
            $page->appendChild($btndiv);
        }
    }
    $res = html_entity_decode($page->saveHTML());
    return $res;
}

?>