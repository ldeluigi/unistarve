var messageShown = false;
var personShown = false;
var mainShown = false;
var orderShown = false;
var messageLoop = null;
var alert;

$(document).ready(function() {
  alert = $(".banner").html();
  mediaQuery();
  $(".banner").html("");
  $("#main-container").hide();
  $("#notifiche-on").hide();
  $("#notifiche-off").show();
  $("#personal-on").hide();
  $("#personal-off").show();
  $("#orders-on").hide();
  $("#orders-off").show();
  $("span.num").hide();
  $.ajax({
    type: "get",
    url: "/api/message.php?count",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        setInterval(function() {
          if (messageShown) {
            $("span.num").hide();
            return;
          }
          $.ajax({
            type: "get",
            url: "/api/message.php?count",
            success: function(result, status, xhr) {
              if (messageShown) return;
              if (result.ok && result.ok === true) {
                if (result.count > 0) {
                  $("span.num").text(result.count);
                  $("span.num").show();
                }
              } else {
                $("span.num").hide();
              }
            },
            error: function(xhr, status, error) {}
          });
        }, 1000);
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text(
          "Impossibile caricare notifiche: " +
            result.message +
            " (" +
            result.ok +
            ")"
        );
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text(
        "Errore nella risoluzione della richiesta AJAX, impossibile caricare notifiche"
      );
    }
  });
  showMain();
});

function submitHandler() {
  var name = $("#name").val();
  var address = $("#surname").val();
  var email = $("#email").val();
  var pword = $("#pword").val();
  $.ajax({
    type: "GET",
    url:
      "/api/transporter.php?update=true&name=" +
      name +
      "&address=" +
      address +
      "&email=" +
      email +
      "&pword=" +
      pword +
      "&av=" +
      ($(".switch").hasClass("On") ? 1 : 0),
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#personal-container").html(result.html);
        personShown = true;
        $(".banner").html(alert);
        $(".alert").removeClass("alert-danger");
        $(".alert").addClass("alert-success");
        $(".msg").text("Cambiamenti salvati");
        mediaQuery();
      } else if (result.ok && result.ok === 151) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function annullaHandler() {
  $.ajax({
    type: "get",
    url: "/api/transporter.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#personal-container").html(result.html);
        personShown = true;
        mediaQuery();
      } else if (result.ok && result.ok === 151) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}
$(window).on("orientationchange", function(event) {
  mediaQuery();
});

function showPerson() {
  $("#personal").toggleClass("active");
  if (personShown) {
    $("#personal i").finish();
    $("#personal-on").hide();
    $("#personal-off").fadeIn();
    $("#personal-container").html("");
    personShown = false;
    if (!mainShown) {
      showMain();
    }
  } else {
    if (orderShown) {
      showOrders();
    }
    if (messageShown) {
      showMessages();
    }
    if (mainShown) {
      showMain();
    }
    $.ajax({
      type: "get",
      url: "/api/transporter.php",
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $("#personal i").finish();
          $("#personal-off").hide();
          $("#personal-on").fadeIn();
          $("#personal-container").html(result.html);
          personShown = true;
          mediaQuery();
          $(".switch").click(function() {
            $(this)
              .toggleClass("On")
              .toggleClass("Off");
          });
        } else if (result.ok && result.ok === 151) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function showMessages() {
  $("#notifiche").toggleClass("active");
  if (messageShown) {
    $("#notifiche i").finish();
    $("#notifiche-on").hide();
    $("#notifiche-off").fadeIn();
    $("#message-container").html("");
    messageShown = false;
    if (!mainShown) {
      showMain();
    }
    if (messageLoop != null) {
      clearInterval(messageLoop);
      messageLoop = null;
    }
  } else {
    $("span.num").hide();
    if (orderShown) {
      showOrders();
    }
    if (personShown) {
      showPerson();
    }
    if (mainShown) {
      showMain();
    }
    $.ajax({
      type: "get",
      url: "/api/message.php",
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $("#notifiche i").finish();
          $("#notifiche-off").hide();
          $("#notifiche-on").fadeIn();
          $("#message-container").html(result.html);
          messageShown = true;
          if (messageLoop == null) {
            messageLoop = setInterval(function() {
              $.ajax({
                type: "get",
                url: "/api/message.php",
                success: function(result, status, xhr) {
                  if (!messageShown) return;
                  if (result.ok && result.ok === true) {
                    $("#message-container").html(result.html);
                  } else if (result.ok && result.ok === 205) {
                    window.location.replace("/login/login.html");
                  } else {
                  }
                },
                error: function(xhr, status, error) {}
              });
            }, 1000);
          }
        } else if (result.ok && result.ok === 205) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function mediaQuery() {
  if (window.matchMedia("(min-width: 768px)").matches) {
    $(".hide-sm").show();
  } else {
    $(".hide-sm").hide();
  }

  if (
    window.matchMedia("(min-width: 768px)").matches ||
    $(window).width() > 768
  ) {
    $(".hide-sm").show();
    $(".magical-width-100").removeClass("w-100");
    $(".magical-width-100").addClass("w-75");
    $(".magical-width-75").removeClass("w-75");
    $(".magical-width-75").addClass("w-50");
  } else {
    $(".hide-sm").hide();
    $(".magical-width-100").removeClass("w-75");
    $(".magical-width-100").addClass("w-100");
    $(".magical-width-75").removeClass("w-50");
    $(".magical-width-75").addClass("w-75");
  }
}

function deleteMessage(id) {
  if (id) {
    $(".del-" + id)
      .prop("onclick", null)
      .off("click");
    $.ajax({
      type: "get",
      url: "/api/message.php?delete=" + id,
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $(".message").finish();
          $(".msg-" + id).fadeOut(function() {
            $("#message-container").html(result.html);
          });
        } else if (result.ok && result.ok === 205) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function showMain() {
  if (mainShown) {
    $("#main-container").hide();
    mainShown = false;
  } else {
    if (orderShown) {
      showOrders();
    }
    if (messageShown) {
      showMessages();
    }
    if (personShown) {
      showPerson();
    }
    $("#main-container").show();
    mainShown = true;
  }
}

function orderCompleted(id) {
  $.ajax({
    type: "GET",
    url: "/api/order-status.php?id=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $.ajax({
          type: "get",
          url: "/api/order.php",
          success: function(result, status, xhr) {
            if (result.ok && result.ok === true) {
              $("#order i").finish();
              $("#orders-off").hide();
              $("#orders-on").fadeIn();
              $("#order-container").html(result.html);
              orderShown = true;
            } else if (result.ok && result.ok === 151) {
              window.location.replace("/login/login.html");
            } else {
              $(".banner").html(alert);
              $(".alert").removeClass("alert-success");
              $(".alert").addClass("alert-danger");
              $(".msg").text(
                "Errore: " + result.message + " (" + result.ok + ")"
              );
            }
          },
          error: function(xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
          }
        });
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message);
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function delayOrder(id) {
  $.ajax({
    type: "GET",
    url: "/api/order.php?delay&orderid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $.ajax({
          type: "get",
          url: "/api/order.php",
          success: function(result, status, xhr) {
            if (result.ok && result.ok === true) {
              $("#order i").finish();
              $("#orders-off").hide();
              $("#orders-on").fadeIn();
              $("#order-container").html(result.html);
              orderShown = true;
            } else {
              $(".banner").html(alert);
              $(".alert").removeClass("alert-success");
              $(".alert").addClass("alert-danger");
              $(".msg").text(
                "Errore: " + result.message + " (" + result.ok + ")"
              );
            }
          },
          error: function(xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
          }
        });
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message);
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function showOrders() {
  $("#orders").toggleClass("active");
  if (orderShown) {
    $("#orders i").finish();
    $("#orders-on").hide();
    $("#orders-off").fadeIn();
    $("#order-container").html("");
    orderShown = false;
    if (!mainShown) {
      showMain();
    }
  } else {
    if (messageShown) {
      showMessages();
    }
    if (personShown) {
      showPerson();
    }
    if (mainShown) {
      showMain();
    }
    $.ajax({
      type: "get",
      url: "/api/order.php",
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $("#order i").finish();
          $("#orders-off").hide();
          $("#orders-on").fadeIn();
          $("#order-container").html(result.html);
          orderShown = true;
        } else if (result.ok && result.ok === 151) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function orderHistory() {
  if (messageShown) {
    showMessages();
  }
  if (personShown) {
    showPerson();
  }
  if (mainShown) {
    showMain();
  }
  $.ajax({
    type: "get",
    url: "/api/order.php?history",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#order i").finish();
        $("#orders-off").hide();
        $("#orders-on").fadeIn();
        $("#order-container").html(result.html);
        orderShown = true;
      } else if (result.ok && result.ok === 151) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function moveMap(address) {
  var query = "https://maps.google.com/maps?width=700&height=440&hl=en&q=";
  var postQuery = "+(Titolo)&ie=UTF8&t=&z=10&iwloc=B&output=embed";
  $("iframe.map").attr("src", query + encodeURIComponent(address) + postQuery);
  showMain();
}
