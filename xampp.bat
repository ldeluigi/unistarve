
if Not Exist "./htdocs.txt" (
	@echo off && >htdocs.txt ( for /F %%d in ( 'wmic logicaldisk get name' ) do dir %%d\htdocs /AD /s /b )
)

@echo on

set /p FOLDER=<htdocs.txt
IF [%FOLDER%] == [] GOTO END
robocopy frontend %FOLDER% /E /LEV:1
for /F %%D in ( 'dir /b /a:d frontend' ) do robocopy frontend\%%D %FOLDER%\%%D /MIR
robocopy backend %FOLDER%\api\ "*.php" /MIR /S
:END
