var messageShown = false;
var personShown = false;
var cartShown = false;
var messageLoop = null;
var orderShown = false;
var mainShown = false;
var neverOpened = true;
var alert;
var pagetitle = document.title;

$(document).ready(function() {
  alert = $(".banner").html();
  mediaQuery();
  $(".banner").html("");
  $("#notifiche-on").hide();
  $("#notifiche-off").show();
  $("#orders-on").hide();
  $("#orders-off").show();
  $("#personal-on").hide();
  $("#personal-off").show();
  $("#cartbutton-on").hide();
  $("#cartbutton-off").show();
  $("span.num").hide();
  $.ajax({
    type: "get",
    url: "/api/message.php?count",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        setInterval(function() {
          if (messageShown) {
            $("span.num").hide();
            document.title = pagetitle;
            return;
          }
          $.ajax({
            type: "get",
            url: "/api/message.php?count",
            success: function(result, status, xhr) {
              if (messageShown) return;
              if (result.ok && result.ok === true) {
                if (result.count > 0) {
                  $("span.num").text(result.count);
                  document.title = "(" + result.count + ") " + pagetitle;
                  $("span.num").show();
                }
              } else {
                $("span.num").hide();
              }
            },
            error: function(xhr, status, error) {}
          });
        }, 1000);
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text(
          "Impossibile caricare notifiche: " +
            result.message +
            " (" +
            result.ok +
            ")"
        );
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text(
        "Errore nella risoluzione della richiesta AJAX, impossibile caricare notifiche"
      );
    }
  });
  showMain();
  $("#search-term").keyup(function(e) {
    if (e.keyCode == 13) {
      $(this).trigger("ENTER");
    }
  });
  $("#search-term").bind("ENTER", function(e) {
    searchQuery();
  });
  lazyI = new LazyLoad({
    element_selector: ".lazy",
    container: document.querySelector(".overflow-auto")
  });
  
  $(".switch#APavailable").click(function() {
    $(this)
      .toggleClass("On")
      .toggleClass("Off");
  });
});

function getItems() {
  $.ajax({
    type: "GET",
    url: "/api/search.php?productor",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#products-container").html(result.html);
        mediaQuery();
        lazyI.update();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message);
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function submitHandler() {
  var name = $("#name").val();
  var address = $("#address").val();
  var email = $("#email").val();
  var pword = $("#pword").val();
  var description = $("#description").val();
  var piva = $("#piva").val();
  $.ajax({
    type: "GET",
    url:
      "/api/productor.php?update=true&name=" +
      name +
      "&address=" +
      address +
      "&email=" +
      email +
      "&pword=" +
      pword +
      "&piva=" +
      piva +
      "&d=" +
      description,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#personal-container").html(result.html);
        $(".banner").html(alert);
        $(".alert").removeClass("alert-danger");
        $(".alert").addClass("alert-success");
        $(".msg").text("Dati modificati con successo");
        personShown = true;
        mediaQuery();
      } else if (result.ok && result.ok === 151) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function annullaHandler() {
  $.ajax({
    type: "get",
    url: "/api/productor.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#personal i").finish();
        $("#personal-off").hide();
        $("#personal-on").fadeIn();
        $("#personal-container").html(result.html);
        personShown = true;
        mediaQuery();
      } else if (result.ok && result.ok === 151) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}
$(window).on("orientationchange", function(event) {
  mediaQuery();
});

function mediaQuery() {
  if (window.matchMedia("(min-width: 768px)").matches) {
    $(".hide-sm").show();
  } else {
    $(".hide-sm").hide();
  }

  if (
    window.matchMedia("(min-width: 768px)").matches ||
    $(window).width() > 768
  ) {
    $(".hide-sm").show();
    $(".magical-width-100").removeClass("w-100");
    $(".magical-width-100").addClass("w-75");
    $(".magical-width-75").removeClass("w-75");
    $(".magical-width-75").addClass("w-50");
  } else {
    $(".hide-sm").hide();
    $(".magical-width-100").removeClass("w-75");
    $(".magical-width-100").addClass("w-100");
    $(".magical-width-75").removeClass("w-50");
    $(".magical-width-75").addClass("w-75");
  }
}

function deleteMessage(id) {
  if (id) {
    $(".del-" + id)
      .prop("onclick", null)
      .off("click");
    $.ajax({
      type: "get",
      url: "/api/message.php?delete=" + id,
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $(".message").finish();
          $(".msg-" + id).fadeOut(function() {
            $("#message-container").html(result.html);
          });
        } else if (result.ok && result.ok === 205) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function showPerson() {
  $("#personal").toggleClass("active");
  if (personShown) {
    $("#personal i").finish();
    $("#personal-on").hide();
    $("#personal-off").fadeIn();
    $("#personal-container").html("");
    personShown = false;
    if (!mainShown) {
      showMain();
    }
  } else {
    if (cartShown) {
      showCart();
    }
    if (messageShown) {
      showMessages();
    }
    if (orderShown) {
      showOrders();
    }
    if (mainShown) {
      showMain();
    }
    $.ajax({
      type: "get",
      url: "/api/productor.php",
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $("#personal i").finish();
          $("#personal-off").hide();
          $("#personal-on").fadeIn();
          $("#personal-container").html(result.html);
          personShown = true;
        } else if (result.ok && result.ok === 151) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function showMessages() {
  $("#notifiche").toggleClass("active");
  if (messageShown) {
    $("#notifiche i").finish();
    $("#notifiche-on").hide();
    $("#notifiche-off").fadeIn();
    $("#message-container").html("");
    messageShown = false;
    if (messageLoop != null) {
      clearInterval(messageLoop);
      messageLoop = null;
    }
    if (!mainShown) {
      showMain();
    }
  } else {
    $("span.num").hide();
    if (personShown) {
      showPerson();
    }
    if (cartShown) {
      showCart();
    }
    if (orderShown) {
      showOrders();
    }
    if (mainShown) {
      showMain();
    }
    $.ajax({
      type: "get",
      url: "/api/message.php",
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $("#notifiche i").finish();
          $("#notifiche-off").hide();
          $("#notifiche-on").fadeIn();
          $("#message-container").html(result.html);
          messageShown = true;
          if (messageLoop == null) {
            messageLoop = setInterval(function() {
              $.ajax({
                type: "get",
                url: "/api/message.php",
                success: function(result, status, xhr) {
                  if (!messageShown) return;
                  if (result.ok && result.ok === true) {
                    $("#message-container").html(result.html);
                  } else if (result.ok && result.ok === 205) {
                    window.location.replace("/login/login.html");
                  } else {
                  }
                },
                error: function(xhr, status, error) {}
              });
            }, 1000);
          }
        } else if (result.ok && result.ok === 205) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function showMain() {
  if (mainShown) {
    $("#main-container").hide();
    mainShown = false;
  } else {
    if (messageShown) {
      showMessages();
    }
    if (personShown) {
      showPerson();
    }
    if (cartShown) {
      showCart();
    }
    if (orderShown) {
      showOrders();
    }
    $("#main-container").show();
    if (neverOpened) {
      getItems();
      neverOpened = false;
    }
    mainShown = true;
  }
}

function showOrders() {
  $("#orders").toggleClass("active");
  if (orderShown) {
    $("#orders i").finish();
    $("#orders-on").hide();
    $("#orders-off").fadeIn();
    $("#order-container").html("");
    orderShown = false;
    if (!mainShown) {
      showMain();
    }
  } else {
    if (cartShown) {
      showCart();
    }
    if (messageShown) {
      showMessages();
    }
    if (personShown) {
      showPerson();
    }
    if (mainShown) {
      showMain();
    }
    $.ajax({
      type: "get",
      url: "/api/order.php",
      success: function(result, status, xhr) {
        if (result.ok && result.ok === true) {
          $("#order i").finish();
          $("#orders-off").hide();
          $("#orders-on").fadeIn();
          $("#order-container").html(result.html);
          orderShown = true;
        } else if (result.ok && result.ok === 151) {
          window.location.replace("/login/login.html");
        } else {
          $(".banner").html(alert);
          $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
          $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
        }
      },
      error: function(xhr, status, error) {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore nella risoluzione della richiesta AJAX");
      }
    });
  }
}

function orderHistory() {
  if (cartShown) {
    showCart();
  }
  if (messageShown) {
    showMessages();
  }
  if (personShown) {
    showPerson();
  }
  if (mainShown) {
    showMain();
  }
  $.ajax({
    type: "get",
    url: "/api/order.php?history",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#order i").finish();
        $("#orders-off").hide();
        $("#orders-on").fadeIn();
        $("#order-container").html(result.html);
        orderShown = true;
      } else if (result.ok && result.ok === 151) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function editProduct(id) {
  $.ajax({
    type: "get",
    url: "/api/product.php?edit&prodid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#modal-container").html(result.html);
        $('[data-toggle="tooltip"]').on("shown.bs.tooltip", function() {
          report_tooltip_shown = true;
        });
        $('[data-toggle="tooltip"]').on("hide.bs.tooltip", function() {
          report_tooltip_shown = false;
        });
        $('[data-toggle="tooltip"]').tooltip();
        var modal = $("#productModal");
        modal.modal();
        alertM = $(".bannerM").html();
        $(".bannerM").html("");
        $(".switch#Pavailable").click(function() {
          $(this)
            .toggleClass("On")
            .toggleClass("Off");
        });
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function updateItem(id) {
  var name = $("#Pname").val();
  var sd = $("#Psd").val();
  var cost = $("#Pcost").val();
  var d = $("#Pdescription").val();
  var link = $("#Pimg").val();
  var av = $("#Pavailable").hasClass("On") ? 1 : 0;

  $.ajax({
    type: "get",
    url:
      "/api/product.php?update&prodid=" +
      id +
      "&name=" +
      name +
      "&sd=" +
      sd +
      "&cost=" +
      cost +
      "&d=" +
      d +
      (link ? "&img=" + link : "") +
      "&av=" +
      av,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#productModal").modal("toggle");
        getItems();
      } else if (result.ok && result.ok === 205) {
        window.location.replace("/login/login.html");
      } else {
        $("#productModal .bannerM").html(alert);
        $("#productModal .alertM").removeClass("alert-success");
        $("#productModal .alertM").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $("#productModal .bannerM").html(alert);
      $("#productModal .alertM").removeClass("alert-success");
      $("#productModal .alertM").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function hoverHandler() {
  $("#addModal").modal();
}

function newItem() {
  var name = $("#APname").val();
  var sd = $("#APsd").val();
  var cost = $("#APcost").val();
  var av = $("#APavailable").hasClass("On") ? 1 : 0;
  var d = $("#APdescription").val();
  var link = $("#APimg").val();
  $.ajax({
    type: "get",
    url:
      "/api/product.php?new&name=" +
      name +
      "&sd=" +
      sd +
      "&cost=" +
      cost +
      "&av=" +
      av +
      (d ? "&d=" + d : "") +
      (link ? "&img=" + link : ""),
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#addModal").modal("toggle");
        getItems();
      } else if (result.ok && result.ok === 205) {
        window.location.replace("/login/login.html");
      } else {
        $(".bannerM").html(alert);
        $(".alertM").removeClass("alert-success");
        $(".alertM").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".bannerM").html(alert);
      $(".alertM").removeClass("alert-success");
      $(".alertM").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function deleteItem(id) {
  $.ajax({
    type: "get",
    url: "/api/product.php?delete&prodid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $(".prod-" + id).fadeOut("slow", function() {
          getItems();
          $(".banner").html(alert);
          $(".alert").removeClass("alert-danger");
          $(".alert").addClass("alert-success");
          $(".msg").text("Oggetto correttamente cancellato");
        });
      } else if (result.ok && result.ok === 205) {
        window.location.replace("/login/login.html");
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}
function searchQuery() {
  var bar = $("#search-term").val();
  $.ajax({
    type: "GET",
    url: "/api/search.php?productor&p&q=" + bar,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#products-container").html(result.html);
        mediaQuery();
        lazyI.update();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message);
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function orderCompleted(id) {
  $.ajax({
    type: "GET",
    url: "/api/order-status.php?id=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $.ajax({
          type: "get",
          url: "/api/order.php",
          success: function(result, status, xhr) {
            if (result.ok && result.ok === true) {
              $("#order i").finish();
              $("#orders-off").hide();
              $("#orders-on").fadeIn();
              $("#order-container").html(result.html);
              orderShown = true;
            } else if (result.ok && result.ok === 151) {
              window.location.replace("/login/login.html");
            } else {
              $(".banner").html(alert);
              $(".alert").removeClass("alert-success");
              $(".alert").addClass("alert-danger");
              $(".msg").text(
                "Errore: " + result.message + " (" + result.ok + ")"
              );
            }
          },
          error: function(xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
          }
        });
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message);
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function delayOrder(id) {
  $.ajax({
    type: "GET",
    url: "/api/order.php?delay&orderid=" + id,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $.ajax({
          type: "get",
          url: "/api/order.php",
          success: function(result, status, xhr) {
            if (result.ok && result.ok === true) {
              $("#order i").finish();
              $("#orders-off").hide();
              $("#orders-on").fadeIn();
              $("#order-container").html(result.html);
              orderShown = true;
            } else {
              $(".banner").html(alert);
              $(".alert").removeClass("alert-success");
              $(".alert").addClass("alert-danger");
              $(".msg").text(
                "Errore: " + result.message + " (" + result.ok + ")"
              );
            }
          },
          error: function(xhr, status, error) {
            $(".banner").html(alert);
            $(".alert").removeClass("alert-success");
            $(".alert").addClass("alert-danger");
            $(".msg").text("Errore nella risoluzione della richiesta AJAX");
          }
        });
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message);
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}
