var alert;
var lazyI;

$(document).ready(function() {
  alert = $(".banner").html();
  $(".banner").html("");
  start();
  $("#search-term").keyup(function(e) {
    if (e.keyCode == 13) {
      $(this).trigger("ENTER");
    }
  });
  $("#search-term").bind("ENTER", function(e) {
    searchQuery();
  });
  lazyI = new LazyLoad({
    element_selector: ".lazy",
    container: document.querySelector(".overflow-auto")
  });
});

function moreItems(page, num) {
  $.ajax({
    type: "GET",
    url: "/api/search.php?n=" + num,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#products-container").html(result.html);
        mediaQuery();
        lazyI.update();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

$(window).on("orientationchange", function(event) {
  mediaQuery();
});

function mediaQuery() {
  if (
    window.matchMedia("(min-width: 768px)").matches ||
    $(window).width() > 768
  ) {
    $(".hide-sm").show();
    $("#products-container").removeClass("w-100");
    $("#products-container").addClass("w-75");
    $(".search-bar").removeClass("w-75");
    $(".search-bar").addClass("w-50");
  } else {
    $(".hide-sm").hide();
    $("#products-container").removeClass("w-75");
    $("#products-container").addClass("w-100");
    $(".search-bar").removeClass("w-50");
    $(".search-bar").addClass("w-75");
  }
}

function searchQuery() {
  var bar = $("#search-term").val();
  $.ajax({
    type: "GET",
    url: "/api/search.php?q=" + bar,
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#products-container").html(result.html);
        mediaQuery();
        lazyI.update();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}

function start() {
  $.ajax({
    type: "GET",
    url: "/api/search.php",
    success: function(result, status, xhr) {
      if (result.ok && result.ok === true) {
        $("#products-container").html(result.html);
        mediaQuery();
        lazyI.update();
      } else {
        $(".banner").html(alert);
        $(".alert").removeClass("alert-success");
        $(".alert").addClass("alert-danger");
        $(".msg").text("Errore: " + result.message + " (" + result.ok + ")");
      }
    },
    error: function(xhr, status, error) {
      $(".banner").html(alert);
      $(".alert").removeClass("alert-success");
      $(".alert").addClass("alert-danger");
      $(".msg").text("Errore nella risoluzione della richiesta AJAX");
    }
  });
}
