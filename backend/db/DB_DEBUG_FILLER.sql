-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 18, 2019 alle 17:12
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_unistarve`
--
CREATE DATABASE IF NOT EXISTS `my_unistarve` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `my_unistarve`;

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts`
--

CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL,
  `productor` int(11) DEFAULT NULL,
  `transporter` int(11) DEFAULT NULL,
  `consumer` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_access` datetime DEFAULT NULL,
  `registration` datetime NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pword` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `administrator` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `accounts`:
--   `productor`
--       `productors` -> `ID`
--   `transporter`
--       `transporters` -> `ID`
--   `consumer`
--       `consumers` -> `CF`
--

--
-- Dump dei dati per la tabella `accounts`
--

INSERT INTO `accounts` (`ID`, `productor`, `transporter`, `consumer`, `last_access`, `registration`, `email`, `pword`, `administrator`) VALUES
(1, 1, NULL, NULL, '2019-02-18 17:09:04', '2019-02-18 16:39:38', 'produttore@produttore.produttore', 'Produttore@1', 0),
(2, 2, NULL, NULL, '2019-02-18 16:55:54', '2019-02-18 16:55:44', 'altro@produttore.produttore', 'Produttore@1', 0),
(3, NULL, 1, NULL, '2019-02-18 17:09:52', '2019-02-18 17:00:20', 't@t.t', 'Trasporto@1', 0),
(4, NULL, NULL, 'ADMIN123456', '2019-02-18 17:07:34', '2019-02-18 17:01:15', 'admin@admin.admin', 'Amministratore@1', 1),
(5, NULL, NULL, 'UTNTTST12345', '2019-02-18 17:10:38', '2019-02-18 17:03:01', 'utente@utente.utente', 'Utente@1', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `cart`
--

CREATE TABLE `cart` (
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `cart`:
--

--
-- Dump dei dati per la tabella `cart`
--

INSERT INTO `cart` (`ID`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Struttura della tabella `cell_numbers`
--

CREATE TABLE `cell_numbers` (
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `cell_numbers`:
--   `owner`
--       `transporters` -> `ID`
--

--
-- Dump dei dati per la tabella `cell_numbers`
--

INSERT INTO `cell_numbers` (`phone_number`, `owner`) VALUES
('1234567890', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `choices`
--

CREATE TABLE `choices` (
  `product_group` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `choices`:
--   `product_group`
--       `product_groups` -> `ID`
--   `menu`
--       `ordinables` -> `ID`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `comprise`
--

CREATE TABLE `comprise` (
  `product` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `comprise`:
--   `orderID`
--       `orders` -> `ID`
--   `product`
--       `ordinables` -> `ID`
--

--
-- Dump dei dati per la tabella `comprise`
--

INSERT INTO `comprise` (`product`, `orderID`, `quantity`, `price`) VALUES
(1, 1, 1, 4.5),
(3, 2, 2, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `consumers`
--

CREATE TABLE `consumers` (
  `surname` char(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` char(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CF` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `consumers`:
--   `cart`
--       `cart` -> `ID`
--

--
-- Dump dei dati per la tabella `consumers`
--

INSERT INTO `consumers` (`surname`, `name`, `CF`, `cart`) VALUES
('Admin', 'Admin', 'ADMIN123456', 1),
('Test', 'Utente', 'UTNTTST12345', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `covers`
--

CREATE TABLE `covers` (
  `transporter` int(11) NOT NULL,
  `area` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `covers`:
--   `area`
--       `productors` -> `ID`
--   `transporter`
--       `transporters` -> `ID`
--

--
-- Dump dei dati per la tabella `covers`
--

INSERT INTO `covers` (`transporter`, `area`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `include`
--

CREATE TABLE `include` (
  `includes` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `include`:
--   `includes`
--       `ordinables` -> `ID`
--   `menu`
--       `ordinables` -> `ID`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `locations`
--

CREATE TABLE `locations` (
  `ID` int(11) NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indications` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `locations`:
--

--
-- Dump dei dati per la tabella `locations`
--

INSERT INTO `locations` (`ID`, `address`, `indications`) VALUES
(1, 'Via Cesare Pavese, 50, 47521 Cesena FC', 'Dopo la stazione proseguire fino alla rotonda della Coop. Si trova ad un kilometro dalla rotonda.'),
(2, 'Via Alfredo Oriani 47521 Cesena FC', 'E\\\' il retro della sede universitaria'),
(3, 'Via NiccolÃ² Machiavelli Cesena FC', 'Ingresso secondario della sede di cesena');

-- --------------------------------------------------------

--
-- Struttura della tabella `messages`
--

CREATE TABLE `messages` (
  `time` datetime NOT NULL,
  `text` varchar(3072) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID` int(11) NOT NULL,
  `Rec_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `messages`:
--   `Rec_ID`
--       `accounts` -> `ID`
--

--
-- Dump dei dati per la tabella `messages`
--

INSERT INTO `messages` (`time`, `text`, `ID`, `Rec_ID`) VALUES
('2019-02-18 16:39:38', 'Salve Produttore, il team vi da il benvenuto. Come produttori avrete il compito di soddisfare gli ordini che compariranno nell\'omonima sezione, corrispondenti a prodotti che voi metterete a disposizione dei clienti inserendone i dati nei form. Il software vi permetterà di aggiungere rimuovere o modificare prodotti a vostro piacimento, e di rendere non disponibili alcuni di questi. Note importanti: Nella schermata principale, i prodotti saranno ordinati per popolarità decrescente.', 1, 1),
('2019-02-18 16:55:44', 'Salve Altro Produttore, il team vi da il benvenuto. Come produttori avrete il compito di soddisfare gli ordini che compariranno nell\'omonima sezione, corrispondenti a prodotti che voi metterete a disposizione dei clienti inserendone i dati nei form. Il software vi permetterà di aggiungere rimuovere o modificare prodotti a vostro piacimento, e di rendere non disponibili alcuni di questi. Note importanti: Nella schermata principale, i prodotti saranno ordinati per popolarità decrescente.', 2, 2),
('2019-02-18 17:00:20', 'Benvenuto Delivery SRL nel team di Unistarve! Come ente di trasporto, avrai il compito di prendere i prodotti dai fornitori e portarli ai consumatori che li hanno ordinati. La sezione ordini conterrà tutte le informazioni necessarie, nonchè i bottoni per confermare la consegna dei suddetti. La schermata principale contiene una mappa utile per trovare la destinazione. Pro tip: cliccando su un ordine, la mappa si aggiornerà per puntare alla destinazione di quest\'ultimo. Inoltre, nella sezione personale è possibile attivare o disattivare la propria disponibilità a ricevere ordini. Nota importante: gli ordini sono assegnati randomicamente tra i trasportatori disponibili.', 3, 3),
('2019-02-18 17:03:01', 'Salve Utente e benvenuto in Unistarve. Da questa sezione puoi visualizzare le notifiche, mentre cliccando sui bottoni in alto puoi navigare i menu e gestire il tuo carrello e le tue ordinazioni. Ricorda che per poter effettuare un ordine è necessario aver aggiunto almeno un metodo di pagamento. E\' possibile aggiungerne uno dal bottone apposito nella pagina per la gestione del profilo. Saluti dal team e buon appetito!', 5, 5),
('2019-02-18 17:08:42', 'Un nuovo ordine è stato elaborato e i prodotti saranno aggiunti in lista appena il pagamento è avvenuto.', 8, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `on_cart`
--

CREATE TABLE `on_cart` (
  `product` int(11) NOT NULL,
  `cart` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `on_cart`:
--   `cart`
--       `cart` -> `ID`
--   `product`
--       `ordinables` -> `ID`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `options`
--

CREATE TABLE `options` (
  `product` int(11) NOT NULL,
  `product_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `options`:
--   `product`
--       `ordinables` -> `ID`
--   `product_group`
--       `product_groups` -> `ID`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `orders`
--

CREATE TABLE `orders` (
  `ID` int(11) NOT NULL,
  `delivered` datetime DEFAULT NULL,
  `pickedup` datetime DEFAULT NULL,
  `ordination` int(11) NOT NULL,
  `destinated_to` int(11) NOT NULL,
  `transportation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `orders`:
--   `ordination`
--       `ordinations` -> `ID`
--   `transportation`
--       `transporters` -> `ID`
--   `destinated_to`
--       `productors` -> `ID`
--

--
-- Dump dei dati per la tabella `orders`
--

INSERT INTO `orders` (`ID`, `delivered`, `pickedup`, `ordination`, `destinated_to`, `transportation`) VALUES
(1, '2019-02-18 17:10:18', '2019-02-18 17:09:20', 1, 1, 1),
(2, '2019-02-18 17:10:19', NULL, 1, 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordinables`
--

CREATE TABLE `ordinables` (
  `ID` int(11) NOT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_link` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elimination_datetime` datetime DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available` tinyint(1) NOT NULL,
  `cost` float NOT NULL,
  `isMenu` tinyint(1) NOT NULL,
  `productor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `ordinables`:
--   `productor`
--       `productors` -> `ID`
--

--
-- Dump dei dati per la tabella `ordinables`
--

INSERT INTO `ordinables` (`ID`, `description`, `img_link`, `elimination_datetime`, `name`, `short_description`, `available`, `cost`, `isMenu`, `productor`) VALUES
(1, 'Lunga descrizione sul prodotto contenente tutte le informazioni utili....', 'http://www.sedanoallegro.it/sites/default/files/ricette/2017-10/club.jpeg', NULL, 'Panino di test', 'Descrizione breve che attira l\'attenzione...', 1, 4.5, 0, 1),
(2, 'Lunga descrizione sul prodotto contenente tutte le informazioni utili....', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-thanksgiving-leftover-sandwich-horizontal-1542326155.jpg', NULL, 'Panino di test non disponibile', 'Descrizione breve che attira l\'attenzione...', 0, 5, 0, 1),
(3, 'Descrizione lunga', NULL, NULL, 'Prodotto di test senza immagine', 'Descrizione breve', 1, 2, 0, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordinations`
--

CREATE TABLE `ordinations` (
  `ID` int(11) NOT NULL,
  `payed` tinyint(1) NOT NULL,
  `delivery_datetime` datetime DEFAULT NULL,
  `ordination_datetime` datetime NOT NULL,
  `for_datetime` datetime DEFAULT NULL,
  `client` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` int(11) NOT NULL,
  `pay_with` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `ordinations`:
--   `location`
--       `locations` -> `ID`
--   `client`
--       `consumers` -> `CF`
--   `pay_with`
--       `payment_methods` -> `ID`
--

--
-- Dump dei dati per la tabella `ordinations`
--

INSERT INTO `ordinations` (`ID`, `payed`, `delivery_datetime`, `ordination_datetime`, `for_datetime`, `client`, `location`, `pay_with`) VALUES
(1, 1, NULL, '2019-02-18 17:08:42', '2019-02-18 18:45:00', 'UTNTTST12345', 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `payment_data`
--

CREATE TABLE `payment_data` (
  `ID` int(11) NOT NULL,
  `PIN` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `payment_data`:
--

--
-- Dump dei dati per la tabella `payment_data`
--

INSERT INTO `payment_data` (`ID`, `PIN`) VALUES
(1, '7836487623');

-- --------------------------------------------------------

--
-- Struttura della tabella `payment_methods`
--

CREATE TABLE `payment_methods` (
  `ID` int(11) NOT NULL,
  `payment_data` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `payment_methods`:
--   `payment_data`
--       `payment_data` -> `ID`
--   `owner`
--       `consumers` -> `CF`
--

--
-- Dump dei dati per la tabella `payment_methods`
--

INSERT INTO `payment_methods` (`ID`, `payment_data`, `code`, `payment_type`, `owner`) VALUES
(1, 1, '27816387216871268487124', 'MasterCard', 'UTNTTST12345');

-- --------------------------------------------------------

--
-- Struttura della tabella `productors`
--

CREATE TABLE `productors` (
  `P_IVA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID` int(11) NOT NULL,
  `unsubscription` datetime DEFAULT NULL,
  `description` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `productors`:
--

--
-- Dump dei dati per la tabella `productors`
--

INSERT INTO `productors` (`P_IVA`, `address`, `name`, `ID`, `unsubscription`, `description`) VALUES
('PARTITAIVA12345', 'Via Fornaci, 41-37 47521 Cesena FC', 'Produttore', 1, NULL, 'Generico produttore di vivande, questa è la sua descrizione'),
('ALTRPRODIVA123', 'Viale Giacomo Matteotti, 280-390 47522 Cesena FC', 'Altro Produttore', 2, NULL, 'Una descrizione di questo produttore');

-- --------------------------------------------------------

--
-- Struttura della tabella `product_groups`
--

CREATE TABLE `product_groups` (
  `ID` int(11) NOT NULL,
  `optional` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `product_groups`:
--

-- --------------------------------------------------------

--
-- Struttura della tabella `transporters`
--

CREATE TABLE `transporters` (
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `P_IVA` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID` int(11) NOT NULL,
  `unsubscription` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- RELAZIONI PER TABELLA `transporters`:
--

--
-- Dump dei dati per la tabella `transporters`
--

INSERT INTO `transporters` (`address`, `name`, `P_IVA`, `ID`, `unsubscription`, `active`) VALUES
('Via L. C. Farini, 880', 'Delivery SRL', 'PIVATRSPRT1243', 1, NULL, 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD UNIQUE KEY `FKaccede_a_ID` (`productor`),
  ADD UNIQUE KEY `FKaccede_p_1_ID` (`transporter`),
  ADD UNIQUE KEY `FKaccede_p_ID` (`consumer`);

--
-- Indici per le tabelle `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `cell_numbers`
--
ALTER TABLE `cell_numbers`
  ADD PRIMARY KEY (`phone_number`),
  ADD KEY `FKtelefoni` (`owner`);

--
-- Indici per le tabelle `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`menu`,`product_group`),
  ADD KEY `FKgruppo` (`product_group`);

--
-- Indici per le tabelle `comprise`
--
ALTER TABLE `comprise`
  ADD PRIMARY KEY (`product`,`orderID`),
  ADD KEY `FKordine` (`orderID`);

--
-- Indici per le tabelle `consumers`
--
ALTER TABLE `consumers`
  ADD PRIMARY KEY (`CF`),
  ADD UNIQUE KEY `FKtiene_ID` (`cart`);

--
-- Indici per le tabelle `covers`
--
ALTER TABLE `covers`
  ADD PRIMARY KEY (`area`,`transporter`),
  ADD KEY `FKcop_TRA` (`transporter`);

--
-- Indici per le tabelle `include`
--
ALTER TABLE `include`
  ADD PRIMARY KEY (`includes`,`menu`),
  ADD KEY `FKmenu` (`menu`);

--
-- Indici per le tabelle `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKReceives` (`Rec_ID`);

--
-- Indici per le tabelle `on_cart`
--
ALTER TABLE `on_cart`
  ADD PRIMARY KEY (`cart`,`product`),
  ADD KEY `FKprodotto3` (`product`);

--
-- Indici per le tabelle `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`product`,`product_group`),
  ADD KEY `FKprodotto4` (`product_group`);

--
-- Indici per le tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKgenera` (`ordination`),
  ADD KEY `FKriguarda` (`destinated_to`),
  ADD KEY `FKis_assigned_to` (`transportation`);

--
-- Indici per le tabelle `ordinables`
--
ALTER TABLE `ordinables`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKdispose` (`productor`);

--
-- Indici per le tabelle `ordinations`
--
ALTER TABLE `ordinations`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKexecute` (`client`),
  ADD KEY `FKdestination` (`location`),
  ADD KEY `FKpays_with` (`pay_with`);

--
-- Indici per le tabelle `payment_data`
--
ALTER TABLE `payment_data`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `FKpagamento_ID` (`payment_data`),
  ADD KEY `FKpays_with2` (`owner`);

--
-- Indici per le tabelle `productors`
--
ALTER TABLE `productors`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `product_groups`
--
ALTER TABLE `product_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `transporters`
--
ALTER TABLE `transporters`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `accounts`
--
ALTER TABLE `accounts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `cart`
--
ALTER TABLE `cart`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `locations`
--
ALTER TABLE `locations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `messages`
--
ALTER TABLE `messages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT per la tabella `orders`
--
ALTER TABLE `orders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `ordinables`
--
ALTER TABLE `ordinables`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `ordinations`
--
ALTER TABLE `ordinations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `payment_data`
--
ALTER TABLE `payment_data`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `productors`
--
ALTER TABLE `productors`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `product_groups`
--
ALTER TABLE `product_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `transporters`
--
ALTER TABLE `transporters`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `FKaccede_a_FK` FOREIGN KEY (`productor`) REFERENCES `productors` (`ID`),
  ADD CONSTRAINT `FKaccede_p_1_FK` FOREIGN KEY (`transporter`) REFERENCES `transporters` (`ID`),
  ADD CONSTRAINT `FKaccede_p_FK` FOREIGN KEY (`consumer`) REFERENCES `consumers` (`CF`);

--
-- Limiti per la tabella `cell_numbers`
--
ALTER TABLE `cell_numbers`
  ADD CONSTRAINT `FKtelefoni` FOREIGN KEY (`owner`) REFERENCES `transporters` (`ID`);

--
-- Limiti per la tabella `choices`
--
ALTER TABLE `choices`
  ADD CONSTRAINT `FKgruppo` FOREIGN KEY (`product_group`) REFERENCES `product_groups` (`ID`),
  ADD CONSTRAINT `FKmenu2` FOREIGN KEY (`menu`) REFERENCES `ordinables` (`ID`);

--
-- Limiti per la tabella `comprise`
--
ALTER TABLE `comprise`
  ADD CONSTRAINT `FKordine` FOREIGN KEY (`orderID`) REFERENCES `orders` (`ID`),
  ADD CONSTRAINT `FKprodotto2` FOREIGN KEY (`product`) REFERENCES `ordinables` (`ID`);

--
-- Limiti per la tabella `consumers`
--
ALTER TABLE `consumers`
  ADD CONSTRAINT `FKtiene_FK` FOREIGN KEY (`cart`) REFERENCES `cart` (`ID`);

--
-- Limiti per la tabella `covers`
--
ALTER TABLE `covers`
  ADD CONSTRAINT `FKcop_PRO` FOREIGN KEY (`area`) REFERENCES `productors` (`ID`),
  ADD CONSTRAINT `FKcop_TRA` FOREIGN KEY (`transporter`) REFERENCES `transporters` (`ID`);

--
-- Limiti per la tabella `include`
--
ALTER TABLE `include`
  ADD CONSTRAINT `FKincluso` FOREIGN KEY (`includes`) REFERENCES `ordinables` (`ID`),
  ADD CONSTRAINT `FKmenu` FOREIGN KEY (`menu`) REFERENCES `ordinables` (`ID`);

--
-- Limiti per la tabella `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `FKReceives` FOREIGN KEY (`Rec_ID`) REFERENCES `accounts` (`ID`);

--
-- Limiti per la tabella `on_cart`
--
ALTER TABLE `on_cart`
  ADD CONSTRAINT `FKcarrello` FOREIGN KEY (`cart`) REFERENCES `cart` (`ID`),
  ADD CONSTRAINT `FKprodotto3` FOREIGN KEY (`product`) REFERENCES `ordinables` (`ID`);

--
-- Limiti per la tabella `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `FKgruppo2` FOREIGN KEY (`product`) REFERENCES `ordinables` (`ID`),
  ADD CONSTRAINT `FKprodotto4` FOREIGN KEY (`product_group`) REFERENCES `product_groups` (`ID`);

--
-- Limiti per la tabella `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKgenera` FOREIGN KEY (`ordination`) REFERENCES `ordinations` (`ID`),
  ADD CONSTRAINT `FKis_assigned_to` FOREIGN KEY (`transportation`) REFERENCES `transporters` (`ID`),
  ADD CONSTRAINT `FKriguarda` FOREIGN KEY (`destinated_to`) REFERENCES `productors` (`ID`);

--
-- Limiti per la tabella `ordinables`
--
ALTER TABLE `ordinables`
  ADD CONSTRAINT `FKdispose` FOREIGN KEY (`productor`) REFERENCES `productors` (`ID`);

--
-- Limiti per la tabella `ordinations`
--
ALTER TABLE `ordinations`
  ADD CONSTRAINT `FKdestination` FOREIGN KEY (`location`) REFERENCES `locations` (`ID`),
  ADD CONSTRAINT `FKexecute` FOREIGN KEY (`client`) REFERENCES `consumers` (`CF`),
  ADD CONSTRAINT `FKpays_with` FOREIGN KEY (`pay_with`) REFERENCES `payment_methods` (`ID`);

--
-- Limiti per la tabella `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD CONSTRAINT `FKpagamento_FK` FOREIGN KEY (`payment_data`) REFERENCES `payment_data` (`ID`),
  ADD CONSTRAINT `FKpays_with2` FOREIGN KEY (`owner`) REFERENCES `consumers` (`CF`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
